import re
from os import getenv

from setuptools import setup, find_packages


def build_version():
    major = '1'
    minor = '0'
    rev = getenv('BUILD_NUMBER', '0')  # This is provided as a Jenkins Global Variable in the build system
    branch = getenv('GIT_BRANCH', 'local')  # This is provided as a Jenkins Global Variable when using Pipeline SCM

    base_version = '.'.join([major, minor, rev])

    if branch.endswith('master'):
        return base_version
    else:
        sanitized_branch = re.sub(r'[^a-zA-Z0-9]+', '.', branch)
        return '+'.join([base_version, sanitized_branch])


setup(
    name='lng_md_to_conf',
    packages=["lng_md_to_conf"],
    version=build_version(),
    description='LNG Markdown to Confluence Publisher',
    entry_points={
        "console_scripts": [
            'lng_md_to_conf = lng_md_to_conf.md2conf:main',
            'md2conf = lng_md_to_conf.md2conf:main'
        ]
    },
    author='Brian Besl',
    author_email='Brian.Besl@lexisnexis.com',
    url='https://tfs-glo-lexisadvance.visualstudio.com/DefaultCollection/Content/_git/1939-CICD',
    keywords=['lng_md_to_conf'],  # arbitrary keywords
    install_requires=[
        "certifi==2019.3.9",
        "chardet==3.0.4",
        "idna==2.8",
        "Markdown==3.1.1",
        "requests==2.22.0",
        "urllib3==1.25.3"
    ]
)
