#!/usr/local/bin/tcsh
foreach url (  \
http://pypi.content.aws.lexis.com/bcslexis/api/pypi/pypi/packages/a9/55/270e211ef05e17e810cf37a62a26f3ed204aafe30424b74be9b5d952c9db/1020-nester-1.00.zip \
http://pypi.content.aws.lexis.com/bcslexis/api/pypi/pypi/packagesd8/3d/2c3204d0f180166cfea7b12c13effe2b49b4667d000411e32308c40c2f07/numpy-1.10.0.post2.zip )
set base=`basename $url`
/usr/bin/wget --timeout=60 --tries=1  \
-d $url \
-o wget_pypi.log -O $base
end

/bin/rm index.html*
/usr/bin/wget --timeout=60 --tries=1  \
-d http://pypi.content.aws.lexis.com/bcslexis/api/pypi/pypi/simple/ \
-o wget_simple.log
