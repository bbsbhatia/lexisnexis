{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "Creates proxy stack for Misc CICD Services",
    "Outputs": {
        "GLOBALProxyEndPoint": {
            "Description": "Content-CICD-Proxy ELB Endpoint",
            "Value": {
                "Fn::Sub": "${AssetGroup}|${AssetID}|proxy|${Elb.DNSName}"
            }
        }
    },
    "Parameters": {
        "AssetID": {
            "Description": "Asset id of GLITz Asset(to be used in tags)",
            "Type": "String",
            "Default": "1939"
        },
        "AssetName": {
            "Description": "Short name of the GLITz Asset(to be used in tags)",
            "Type": "String",
            "Default": "Artifactory"
        },
        "AssetGroup": {
            "Description": "AssetGroup Name",
            "Type": "String",
            "Default": "test"
        },
        "AmiId": {
            "Description": "AMI ID to build NGINX on",
            "Type": "String",
            "Default": "ami-0144056d91f5f893a"
        }
    },
    "Resources": {
        "CloudMeta": {
            "Type": "Custom::CloudMetadata",
            "Properties": {
                "ServiceToken": {
                    "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
                },
                "AssetID": {
                    "Ref": "AssetID"
                },
                "AssetGroup": {
                    "Ref": "AssetGroup"
                },
                "AssetAreaName": "proxy",
                "Version": "1",
                "LastUpdate": "1499548494"
            }
        },
        "NginxSG": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "GroupDescription": "CICD Proxy Security Group",
                "Tags": [
                    {
                        "Key": "Name",
                        "Value": "cicd-proxy"
                    },
                    {
                        "Key": "AssetGroup",
                        "Value": {
                            "Ref": "AssetGroup"
                        }
                    },
                    {
                        "Key": "AssetID",
                        "Value": {
                            "Ref": "AssetID"
                        }
                    },
                    {
                        "Key": "AssetName",
                        "Value": {
                            "Ref": "AssetName"
                        }
                    },
                    {
                        "Key": "AssetAreaName",
                        "Value": "proxy"
                    }
                ],
                "VpcId": {
                    "Fn::GetAtt": [
                        "CloudMeta",
                        "vpc.Id"
                    ]
                }
            }
        },
        "NginxSGIngressHttpFrom10": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "80",
                "ToPort": "80",
                "CidrIp": "10.0.0.0/8"
            }
        },
        "NginxSGIngressHttpFrom138": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "80",
                "ToPort": "80",
                "CidrIp": "138.12.0.0/16"
            }
        },
        "NginxSGIngressHttpFrom172": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "80",
                "ToPort": "80",
                "CidrIp": "172.29.0.0/16"
            }
        },
        "NginxSGIngressSiblings": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "SourceSecurityGroupId": {
                    "Ref": "NginxSG"
                }
            }
        },
        "NginxSGEgressSiblings": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "SourceSecurityGroupId": {
                    "Ref": "NginxSG"
                }
            }
        },
        "NginxSGEgressNtp1": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "udp",
                "FromPort": "123",
                "ToPort": "123",
                "CidrIp": "192.206.141.88/32"
            }
        },
        "NginxSGEgressNtp2": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "udp",
                "FromPort": "123",
                "ToPort": "123",
                "CidrIp": "198.206.141.208/32"
            }
        },
        "NginxSGEgressNtp3": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "udp",
                "FromPort": "123",
                "ToPort": "123",
                "CidrIp": "192.206.141.8/32"
            }
        },
        "NginxSGEgressTo172": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "172.29.0.0/16"
            }
        },
        "NginxSGEgressTo138": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "138.12.0.0/16"
            }
        },
        "NginxSGEgressTo10": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "NginxSG",
            "Properties": {
                "GroupId": {
                    "Ref": "NginxSG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "10.0.0.0/8"
            }
        },
        "ElbSG": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "GroupDescription": "CICD Proxy Security Group",
                "Tags": [
                    {
                        "Key": "Name",
                        "Value": "cicd-proxy"
                    },
                    {
                        "Key": "AssetGroup",
                        "Value": {
                            "Ref": "AssetGroup"
                        }
                    },
                    {
                        "Key": "AssetID",
                        "Value": {
                            "Ref": "AssetID"
                        }
                    },
                    {
                        "Key": "AssetName",
                        "Value": {
                            "Ref": "AssetName"
                        }
                    },
                    {
                        "Key": "AssetAreaName",
                        "Value": "proxy"
                    }
                ],
                "VpcId": {
                    "Fn::GetAtt": [
                        "CloudMeta",
                        "vpc.Id"
                    ]
                }
            }
        },
        "ElbSGIngressHttpsFrom10": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "443",
                "ToPort": "443",
                "CidrIp": "10.0.0.0/8"
            }
        },
        "ElbSGIngressHttpsFrom138": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "443",
                "ToPort": "443",
                "CidrIp": "138.12.0.0/16"
            }
        },
        "ElbSGIngressHttpsFrom172": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "443",
                "ToPort": "443",
                "CidrIp": "172.29.0.0/16"
            }
        },
        "ElbSGIngressHttpsFrom192": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "443",
                "ToPort": "443",
                "CidrIp": "192.168.0.0/16"
            }
        },
        "ElbSGIngressHttpFrom10": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "80",
                "ToPort": "80",
                "CidrIp": "10.0.0.0/8"
            }
        },
        "ElbSGIngressHttpFrom138": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "80",
                "ToPort": "80",
                "CidrIp": "138.12.0.0/16"
            }
        },
        "ElbSGIngressHttpFrom172": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "80",
                "ToPort": "80",
                "CidrIp": "172.29.0.0/16"
            }
        },
        "ElbSGIngressHttpFrom192": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "tcp",
                "FromPort": "80",
                "ToPort": "80",
                "CidrIp": "192.168.0.0/16"
            }
        },
        "ElbSGEgressHosts": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "ElbSG",
            "Properties": {
                "GroupId": {
                    "Ref": "ElbSG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "SourceSecurityGroupId": {
                    "Ref": "NginxSG"
                }
            }
        },
        "NginxServerGroup": {
            "Type": "AWS::AutoScaling::AutoScalingGroup",
            "Properties": {
                "LoadBalancerNames": [
                    {
                        "Ref": "Elb"
                    }
                ],
                "VPCZoneIdentifier": {
                    "Fn::GetAtt": [
                        "CloudMeta",
                        "vpc.PrivateSubnetList"
                    ]
                },
                "LaunchConfigurationName": {
                    "Ref": "NginxLaunchConfig"
                },
                "MaxSize": "2",
                "MinSize": "0",
                "DesiredCapacity": "1",
                "Cooldown": "400",
                "HealthCheckGracePeriod": "200",
                "HealthCheckType": "EC2",
                "MetricsCollection": [
                    {
                        "Granularity": "1Minute"
                    }
                ],
                "Tags": [
                    {
                        "Key": "AssetName",
                        "Value": {
                            "Ref": "AssetName"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetAreaName",
                        "Value": "nginx",
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "Name",
                        "Value": {
                            "Fn::Join": [
                                ":",
                                [
                                    {
                                        "Ref": "AssetGroup"
                                    },
                                    {
                                        "Ref": "AssetName"
                                    },
                                    "nginx"
                                ]
                            ]
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetGroup",
                        "Value": {
                            "Ref": "AssetGroup"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetID",
                        "Value": {
                            "Ref": "AssetID"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "Managed",
                        "Value": "True",
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "JoinDomain",
                        "Value": "True",
                        "PropagateAtLaunch": "true"
                    }
                ]
            },
            "UpdatePolicy": {
                "AutoScalingRollingUpdate": {}
            }
        },
        "NginxLaunchConfig": {
            "Type": "AWS::AutoScaling::LaunchConfiguration",
            "Properties": {
                "ImageId": {
                    "Ref": "AmiId"
                },
                "InstanceMonitoring": "True",
                "IamInstanceProfile": {
                    "Fn::GetAtt": [
                        "CloudMeta",
                        "iam.instanceprofile.BaseInstanceProfile"
                    ]
                },
                "AssociatePublicIpAddress": "False",
                "InstanceType": "t3.small",
                "SecurityGroups": [
                    {
                        "Ref": "NginxSG"
                    },
                    {
                        "Fn::GetAtt": [
                            "CloudMeta",
                            "vpc.SecurityGroups.SgBastionAccess"
                        ]
                    },
                    {
                        "Fn::GetAtt": [
                            "CloudMeta",
                            "vpc.SecurityGroups.SgInternetOut"
                        ]
                    }
                ],
                "UserData": {
                    "Fn::Base64": {
                        "Fn::Join": [
                            "",
                            [
                                "#!/bin/bash -xe\n",
                                "yum update -y\n",
                                "# Install the files and packages from the metadata\n",
                                "/opt/aws/bin/cfn-init -v ",
                                "  --stack ",
                                {
                                    "Ref": "AWS::StackName"
                                },
                                "  --resource NginxLaunchConfig ",
                                "  --configsets Main ",
                                "  --region ",
                                {
                                    "Ref": "AWS::Region"
                                },
                                " > /tmp/cfn-init.log 2>&1",
                                "\n"
                            ]
                        ]
                    }
                }
            },
            "Metadata": {
                "AWS::CloudFormation::Init": {
                    "configSets": {
                        "Main": [
                            "config"
                        ]
                    },
                    "config": {
                        "commands": {
                            "a-create-dns_resolver-conf": {
                                "command": "egrep -e'nameserver' /etc/resolv.conf | sed -e's/nameserver //' > /etc/nginx/opscontent_aws_dns_resolver.conf;sed -i '1 i resolver' /etc/nginx/opscontent_aws_dns_resolver.conf;sed -i '$ a valid=60s;\nresolver_timeout  10s;\n' /etc/nginx/opscontent_aws_dns_resolver.conf;"
                            },
                            "b-start-nginx": {
                                "command": "sudo nginx -c /etc/nginx/nginx.conf"
                            }
                        }
                    }
                }
            }
        },
        "Elb": {
            "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
            "Properties": {
                "CrossZone": "true",
                "ConnectionSettings": {
                    "IdleTimeout": 300
                },
                "HealthCheck": {
                    "Target": "TCP:80",
                    "HealthyThreshold": "3",
                    "Interval": "20",
                    "Timeout": "15",
                    "UnhealthyThreshold": "3"
                },
                "Listeners": [
                    {
                        "InstancePort": "80",
                        "InstanceProtocol": "HTTP",
                        "LoadBalancerPort": "443",
                        "Protocol": "HTTPS",
                        "SSLCertificateId": {
                            "Fn::GetAtt": [
                                "CloudMeta",
                                "network.sslArn.content.aws.lexis.com"
                            ]
                        }
                    }
                ],
                "AccessLoggingPolicy": {
                    "S3BucketName": "ln-content-elb-logs",
                    "S3BucketPrefix": "private",
                    "Enabled": "true",
                    "EmitInterval": "5"
                },
                "Scheme": "internal",
                "SecurityGroups": [
                    {
                        "Ref": "ElbSG"
                    }
                ],
                "Subnets": {
                    "Fn::GetAtt": [
                        "CloudMeta",
                        "vpc.PrivateSubnetList"
                    ]
                },
                "Tags": [
                    {
                        "Key": "Name",
                        "Value": "cicd-proxy"
                    },
                    {
                        "Key": "AssetGroup",
                        "Value": {
                            "Ref": "AssetGroup"
                        }
                    },
                    {
                        "Key": "AssetID",
                        "Value": {
                            "Ref": "AssetID"
                        }
                    },
                    {
                        "Key": "AssetName",
                        "Value": {
                            "Ref": "AssetName"
                        }
                    },
                    {
                        "Key": "AssetAreaName",
                        "Value": "proxy"
                    }
                ],
                "ConnectionDrainingPolicy": {
                    "Enabled": "true",
                    "Timeout": "300"
                }
            }
        },
        "PypiUrl": {
            "Type": "Custom::Route53ELBAliasRecord",
            "Version": "1.0",
            "DependsOn": "CloudMeta",
            "Properties": {
                "ServiceToken": {
                    "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
                },
                "AliasTarget": {
                    "Fn::GetAtt": [
                        "Elb",
                        "DNSName"
                    ]
                },
                "AliasRecord": {
                    "Fn::Sub": "${AssetGroup}-pypi.content.aws.lexis.com"
                },
                "AliasComment": {
                    "Fn::Sub": "Production -> $AssetGroup"
                }
            }
        },
        "NpmUrl": {
            "Type": "Custom::Route53ELBAliasRecord",
            "Version": "1.0",
            "DependsOn": "CloudMeta",
            "Properties": {
                "ServiceToken": {
                    "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
                },
                "AliasTarget": {
                    "Fn::GetAtt": [
                        "Elb",
                        "DNSName"
                    ]
                },
                "AliasRecord": {
                    "Fn::Sub": "${AssetGroup}-npm.content.aws.lexis.com"
                },
                "AliasComment": {
                    "Fn::Sub": "Production -> $AssetGroup"
                }
            }
        },
        "MavenUrl": {
            "Type": "Custom::Route53ELBAliasRecord",
            "Version": "1.0",
            "DependsOn": "CloudMeta",
            "Properties": {
                "ServiceToken": {
                    "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
                },
                "AliasTarget": {
                    "Fn::GetAtt": [
                        "Elb",
                        "DNSName"
                    ]
                },
                "AliasRecord": {
                    "Fn::Sub": "${AssetGroup}-maven.content.aws.lexis.com"
                },
                "AliasComment": {
                    "Fn::Sub": "Production -> $AssetGroup"
                }
            }
        },
        "ArtifactoryUrl": {
            "Type": "Custom::Route53ELBAliasRecord",
            "Version": "1.0",
            "DependsOn": "CloudMeta",
            "Properties": {
                "ServiceToken": {
                    "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
                },
                "AliasTarget": {
                    "Fn::GetAtt": [
                        "Elb",
                        "DNSName"
                    ]
                },
                "AliasRecord": {
                    "Fn::Sub": "${AssetGroup}-artifactory.content.aws.lexis.com"
                },
                "AliasComment": {
                    "Fn::Sub": "Production -> $AssetGroup"
                }
            }
        }
    }
}