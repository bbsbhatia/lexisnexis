```plantuml
@startuml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Container.puml
!define AWSPUML https://raw.githubusercontent.com/milo-minderbinder/AWS-PlantUML/release/18-2-22/dist
!includeurl AWSPUML/common.puml
!includeurl AWSPUML/ApplicationServices/AmazonAPIGateway/AmazonAPIGateway.puml
!includeurl AWSPUML/Analytics/AmazonES/AmazonES.puml
!includeurl AWSPUML/Analytics/AmazonKinesis/AmazonKinesisFirehose/AmazonKinesisFirehose.puml
title Wormhole CICD Jenkins Metrics Container Diagram


Person(developer, "Devloper", "Human User")

Container(jenkins, "Jenkins", "Web Server", "Core orchestration platform for integration and deployment")

System_Boundary(metrics, "Jenkins Metrics"){
    AMAZONAPIGATEWAY(gateway, "Recieves and processes events via Velocity Templates")
    AMAZONKINESISFIREHOSE(firehose)
    AMAZONES(es, "Stores Event Pages")
    Container(metrics_kibana, "Dashboard", "Kibana", "Frontend for metrics")
}

Rel(developer, metrics_kibana, "Uses", "HTTPS")
Rel(metrics_kibana, es, "Gets Documents", "API")
Rel(jenkins, gateway, "Sends Events", "API")
Rel(gateway, firehose, "Sends Events", "API")
Rel(firehose, es, "Stores Pages", "API")

@enduml
```