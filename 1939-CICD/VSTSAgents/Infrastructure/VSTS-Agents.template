{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "VSTS Build Agents",
    "Outputs": {},
    "Parameters": {
        "AssetID": {
            "Description": "Asset id of GLITz Asset(to be used in tags)",
            "Type": "String",
            "Default": "1939"
        },
        "AssetName": {
            "Description": "Short name of the GLITz Asset(to be used in tags)",
            "Type": "String",
            "Default": "CICD"
        },
        "AssetAreaName": {
            "Description": "Short name of the GLITz Asset(to be used in tags)",
            "Type": "String",
            "Default": "VSTS-Agent"
        },
        "AssetGroup": {
            "Description": "AssetGroup Name",
            "Type": "String",
            "Default": "prod"
        },
        "KeyName": {
            "Description": "Name of and existing EC2 KeyPair to enable SSH access to the instance",
            "Type": "AWS::EC2::KeyPair::KeyName",
            "Default": "operations-content-prod"
        },
        "InstanceType": {
            "Description": "Instance type",
            "Type": "String",
            "Default": "t3.nano",
            "ConstraintDescription": "must be a valid EC2 instance type."
        },
        "AmiId": {
            "Description": "AMI ID to build  on",
            "Type": "String",
            "Default": "ami-f973ab84"
        },
        "VstsPat": {
            "NoEcho": "true",
            "Type": "String",
            "Description": "VSTS Personal Access Token for Agent Management"
        }
    },
    "Resources": {
        "CloudMetadata": {
            "Type": "Custom::CloudMetadata",
            "Properties": {
                "ServiceToken": {
                    "Fn::Sub": "arn:aws:lambda:us-east-1:807841377931:function:cloud_metadata:STABLE"
                },
                "AssetID": {
                    "Ref": "AssetID"
                },
                "AssetGroup": {
                    "Ref": "AssetGroup"
                },
                "AssetAreaName": "proxy",
                "Version": "1",
                "LastUpdate": "1499334494"
            }
        },
        "SG": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "GroupDescription": "VSTS Agents Security Group",
                "Tags": [
                    {
                        "Key": "Name",
                        "Value": "cicd-proxy"
                    },
                    {
                        "Key": "AssetGroup",
                        "Value": {
                            "Ref": "AssetGroup"
                        }
                    },
                    {
                        "Key": "AssetID",
                        "Value": {
                            "Ref": "AssetID"
                        }
                    },
                    {
                        "Key": "AssetName",
                        "Value": {
                            "Ref": "AssetName"
                        }
                    },
                    {
                        "Key": "AssetAreaName",
                        "Value": "proxy"
                    }
                ],
                "VpcId": {
                    "Fn::GetAtt": [
                        "CloudMetadata",
                        "vpc.Id"
                    ]
                }
            }
        },
        "SGIngressHttpFrom10": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "10.0.0.0/8"
            }
        },
        "SGIngressHttpFrom138": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "138.12.0.0/16"
            }
        },
        "SGIngressHttpFrom172": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "172.29.0.0/16"
            }
        },
        "SGIngressSiblings": {
            "Type": "AWS::EC2::SecurityGroupIngress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "SourceSecurityGroupId": {
                    "Ref": "SG"
                }
            }
        },
        "SGEgressSiblings": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "SourceSecurityGroupId": {
                    "Ref": "SG"
                }
            }
        },
        "SGEgressNtp1": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "udp",
                "FromPort": "123",
                "ToPort": "123",
                "CidrIp": "192.206.141.88/32"
            }
        },
        "SGEgressNtp2": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "udp",
                "FromPort": "123",
                "ToPort": "123",
                "CidrIp": "198.206.141.208/32"
            }
        },
        "SGEgressNtp3": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "udp",
                "FromPort": "123",
                "ToPort": "123",
                "CidrIp": "192.206.141.8/32"
            }
        },
        "SGEgressTo172": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "172.29.0.0/16"
            }
        },
        "SGEgressTo138": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "138.12.0.0/16"
            }
        },
        "SGEgressTo10": {
            "Type": "AWS::EC2::SecurityGroupEgress",
            "DependsOn": "SG",
            "Properties": {
                "GroupId": {
                    "Ref": "SG"
                },
                "IpProtocol": "-1",
                "FromPort": "-1",
                "ToPort": "-1",
                "CidrIp": "10.0.0.0/8"
            }
        },
        "ServerGroup1": {
            "Type": "AWS::AutoScaling::AutoScalingGroup",
            "Properties": {
                "VPCZoneIdentifier": {
                    "Fn::GetAtt": [
                        "CloudMetadata",
                        "vpc.PrivateSubnetList"
                    ]
                },
                "LaunchConfigurationName": {
                    "Ref": "LaunchConfig"
                },
                "MaxSize": "2",
                "MinSize": "0",
                "DesiredCapacity": "1",
                "Cooldown": "400",
                "HealthCheckGracePeriod": "200",
                "HealthCheckType": "EC2",
                "MetricsCollection": [
                    {
                        "Granularity": "1Minute"
                    }
                ],
                "Tags": [
                    {
                        "Key": "AssetName",
                        "Value": {
                            "Ref": "AssetName"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetAreaName",
                        "Value": {
                            "Ref": "AssetAreaName"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "Name",
                        "Value": {
                            "Fn::Join": [
                                ":",
                                [
                                    {
                                        "Ref": "AssetGroup"
                                    },
                                    {
                                        "Ref": "AssetName"
                                    },
                                    {
                                        "Ref": "AssetAreaName"
                                    },"1"
                                ]
                            ]
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetGroup",
                        "Value": {
                            "Ref": "AssetGroup"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetID",
                        "Value": {
                            "Ref": "AssetID"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "Managed",
                        "Value": "True",
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "JoinDomain",
                        "Value": "True",
                        "PropagateAtLaunch": "true"
                    }
                    
                ]
            },
            "UpdatePolicy": {
                "AutoScalingRollingUpdate": {}
            }
        },
        "ServerGroup2": {
            "Type": "AWS::AutoScaling::AutoScalingGroup",
            "Properties": {
                "VPCZoneIdentifier": {
                    "Fn::GetAtt": [
                        "CloudMetadata",
                        "vpc.PrivateSubnetList"
                    ]
                },
                "LaunchConfigurationName": {
                    "Ref": "LaunchConfig2"
                },
                "MaxSize": "2",
                "MinSize": "0",
                "DesiredCapacity": "1",
                "Cooldown": "400",
                "HealthCheckGracePeriod": "200",
                "HealthCheckType": "EC2",
                "MetricsCollection": [
                    {
                        "Granularity": "1Minute"
                    }
                ],
                "Tags": [
                    {
                        "Key": "AssetName",
                        "Value": {
                            "Ref": "AssetName"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetAreaName",
                        "Value": {
                            "Ref": "AssetAreaName"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "Name",
                        "Value": {
                            "Fn::Join": [
                                ":",
                                [
                                    {
                                        "Ref": "AssetGroup"
                                    },
                                    {
                                        "Ref": "AssetName"
                                    },
                                    {
                                        "Ref": "AssetAreaName"
                                    },"2"
                                ]
                            ]
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetGroup",
                        "Value": {
                            "Ref": "AssetGroup"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "AssetID",
                        "Value": {
                            "Ref": "AssetID"
                        },
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "Managed",
                        "Value": "True",
                        "PropagateAtLaunch": "true"
                    },
                    {
                        "Key": "JoinDomain",
                        "Value": "True",
                        "PropagateAtLaunch": "true"
                    }
                    
                ]
            },
            "UpdatePolicy": {
                "AutoScalingRollingUpdate": {}
            }
        },
        "LaunchConfig": {
            "Type": "AWS::AutoScaling::LaunchConfiguration",
            "Properties": {
                "BlockDeviceMappings": [ 
                    {
                        "DeviceName" : "/dev/xvda",
                        "Ebs" : {
                            "VolumeSize" : 16
                        }
                    }
                ],
                "ImageId": {
                    "Ref": "AmiId"
                },
                "InstanceMonitoring": "True",
                "IamInstanceProfile": {"Fn::GetAtt": ["CloudMetadata", "iam.instanceprofile.BaseInstanceProfile"]},
                "AssociatePublicIpAddress": "False",
                "InstanceType": {
                    "Ref": "InstanceType"
                },
                "KeyName": {
                    "Ref": "KeyName"
                },
                "SecurityGroups": [
                    {
                        "Ref": "SG"
                    },
                    {
                        "Fn::GetAtt": [
                            "CloudMetadata",
                            "vpc.SecurityGroups.SgBastionAccess"
                        ]
                    },
                    {
                        "Fn::GetAtt": [
                            "CloudMetadata",
                            "vpc.SecurityGroups.SgInternetOut"
                        ]
                    }
                ],
                "UserData": {
                    "Fn::Base64": {
                        "Fn::Join": [
                            "",
                            [
                                "#!/bin/bash -xe\n",
                                "yum update -y\n",
                                "# Install the files and packages from the metadata\n",
                                "/opt/aws/bin/cfn-init -v ",
                                "  --stack ",
                                {
                                    "Ref": "AWS::StackName"
                                },
                                "  --resource LaunchConfig ",
                                "  --configsets Group1 ",
                                "  --region ",
                                {
                                    "Ref": "AWS::Region"
                                },
                                " > /tmp/cfn-init.log 2>&1",
                                "\n"
                            ]
                        ]
                    }
                }
            },
            "Metadata": {
                "AWS::CloudFormation::Init": {
                    "configSets": {
                        "Group1": [
                            "download",
                            "claim",
                            "config1",
                            "install",
                            "start"
                        ],
                        "Group2": [
                            "download",
                            "claim",
                            "config2",
                            "install",
                            "start"
                        ]
                    },
                    "download": {
                        "sources": {
                            "/home/ec2-user/vsts/agent/a": "https://vstsagentpackage.azureedge.net/agent/2.153.2/vsts-agent-linux-x64-2.153.2.tar.gz",
                            "/home/ec2-user/vsts/agent/b": "https://vstsagentpackage.azureedge.net/agent/2.153.2/vsts-agent-linux-x64-2.153.2.tar.gz",
                            "/home/ec2-user/vsts/agent/c": "https://vstsagentpackage.azureedge.net/agent/2.153.2/vsts-agent-linux-x64-2.153.2.tar.gz",
                            "/home/ec2-user/vsts/agent/d": "https://vstsagentpackage.azureedge.net/agent/2.153.2/vsts-agent-linux-x64-2.153.2.tar.gz"
                        },
                        "packages": {
                            "yum":{ 
                                "libunwind": [],
                                "git": []
                            }
                        }
                    },
                    "claim": {
                        "commands": {
                            "a-claim-agent-a": {
                                "command": "chown -R ec2-user:ec2-user .",
                                "cwd": "/home/ec2-user/vsts/agent/a"
                            },
                            "b-claim-agent-b": {
                                "command": "chown -R ec2-user:ec2-user .",
                                "cwd": "/home/ec2-user/vsts/agent/b"
                            },
                            "c-claim-agent-c": {
                                "command": "chown -R ec2-user:ec2-user .",
                                "cwd": "/home/ec2-user/vsts/agent/c"
                            },
                            "d-claim-agent-d": {
                                "command": "chown -R ec2-user:ec2-user .",
                                "cwd": "/home/ec2-user/vsts/agent/d"
                            }
                        }
                    },
                    "config1": {
                        "commands": {
                            "a-configure-agent-a": {
                                "command": {
                                    "Fn::Sub": "su ec2-user -c './bin/Agent.Listener configure --unattended --url https://tfs-glo-lexisadvance.visualstudio.com --auth pat --token ${VstsPat} --pool BCS-DevOps --acceptTeeEula --replace --agent Agent-1A'"
                                },
                                "cwd": "/home/ec2-user/vsts/agent/a"
                            },
                            "b-configure-agent-b": {
                                "command": {
                                    "Fn::Sub": "su ec2-user -c './bin/Agent.Listener configure --unattended --url https://tfs-glo-lexisadvance.visualstudio.com --auth pat --token ${VstsPat} --pool BCS-DevOps --acceptTeeEula --replace --agent Agent-1B'"
                                },
                                "cwd": "/home/ec2-user/vsts/agent/b"
                            },
                            "c-configure-agent-c": {
                                "command": {
                                    "Fn::Sub": "su ec2-user -c './bin/Agent.Listener configure --unattended --url https://tfs-glo-lexisadvance.visualstudio.com --auth pat --token ${VstsPat} --pool BCS-DevOps --acceptTeeEula --replace --agent Agent-1C'"
                                },
                                "cwd": "/home/ec2-user/vsts/agent/c"
                            },
                            "d-configure-agent-d": {
                                "command": {
                                    "Fn::Sub": "su ec2-user -c './bin/Agent.Listener configure --unattended --url https://tfs-glo-lexisadvance.visualstudio.com --auth pat --token ${VstsPat} --pool BCS-DevOps --acceptTeeEula --replace --agent Agent-1D'"
                                },
                                "cwd": "/home/ec2-user/vsts/agent/d"
                            }
                        }
                    },
                    "config2": {
                        "commands": {
                            "a-configure-agent-a": {
                                "command": {
                                    "Fn::Sub": "su ec2-user -c './bin/Agent.Listener configure --unattended --url https://tfs-glo-lexisadvance.visualstudio.com --auth pat --token ${VstsPat} --pool BCS-DevOps --acceptTeeEula --replace --agent Agent-2A'"
                                },
                                "cwd": "/home/ec2-user/vsts/agent/a"
                            },
                            "b-configure-agent-b": {
                                "command": {
                                    "Fn::Sub": "su ec2-user -c './bin/Agent.Listener configure --unattended --url https://tfs-glo-lexisadvance.visualstudio.com --auth pat --token ${VstsPat} --pool BCS-DevOps --acceptTeeEula --replace --agent Agent-2B'"
                                },
                                "cwd": "/home/ec2-user/vsts/agent/b"
                            },
                            "c-configure-agent-c": {
                                "command": {
                                    "Fn::Sub": "su ec2-user -c './bin/Agent.Listener configure --unattended --url https://tfs-glo-lexisadvance.visualstudio.com --auth pat --token ${VstsPat} --pool BCS-DevOps --acceptTeeEula --replace --agent Agent-2C'"
                                },
                                "cwd": "/home/ec2-user/vsts/agent/c"
                            },
                            "d-configure-agent-d": {
                                "command": {
                                    "Fn::Sub": "su ec2-user -c './bin/Agent.Listener configure --unattended --url https://tfs-glo-lexisadvance.visualstudio.com --auth pat --token ${VstsPat} --pool BCS-DevOps --acceptTeeEula --replace --agent Agent-2D'"
                                },
                                "cwd": "/home/ec2-user/vsts/agent/d"
                            }
                        }
                    },
                    "install": {
                        "commands": {
                            "a-install-agent-service-a": {
                                "command": "sudo ./svc.sh install",
                                "cwd": "/home/ec2-user/vsts/agent/a"
                            },
                            "b-install-agent-service-b": {
                                "command": "sudo ./svc.sh install",
                                "cwd": "/home/ec2-user/vsts/agent/b"
                            },
                            "c-install-agent-service-c": {
                                "command": "sudo ./svc.sh install",
                                "cwd": "/home/ec2-user/vsts/agent/c"
                            },
                            "d-install-agent-service-d": {
                                "command": "sudo ./svc.sh install",
                                "cwd": "/home/ec2-user/vsts/agent/d"
                            }
                        }
                    },
                    "start": {
                        "commands": {
                            "a-start-agent-service-a": {
                                "command": "sudo ./svc.sh start",
                                "cwd": "/home/ec2-user/vsts/agent/a"
                            },
                            "b-start-agent-service-b": {
                                "command": "sudo ./svc.sh start",
                                "cwd": "/home/ec2-user/vsts/agent/b"
                            },
                            "c-start-agent-service-c": {
                                "command": "sudo ./svc.sh start",
                                "cwd": "/home/ec2-user/vsts/agent/c"
                            },
                            "d-start-agent-service-d": {
                                "command": "sudo ./svc.sh start",
                                "cwd": "/home/ec2-user/vsts/agent/d"
                            }
                        }
                    }
                }
            }
        },
        "LaunchConfig2": {
            "Type": "AWS::AutoScaling::LaunchConfiguration",
            "Properties": {
                "ImageId": {
                    "Ref": "AmiId"
                },
                "InstanceMonitoring": "True",
                "IamInstanceProfile": {"Fn::GetAtt": ["CloudMetadata", "iam.instanceprofile.BaseInstanceProfile"]},
                "AssociatePublicIpAddress": "False",
                "InstanceType": {
                    "Ref": "InstanceType"
                },
                "KeyName": {
                    "Ref": "KeyName"
                },
                "SecurityGroups": [
                    {
                        "Ref": "SG"
                    },
                    {
                        "Fn::GetAtt": [
                            "CloudMetadata",
                            "vpc.SecurityGroups.SgBastionAccess"
                        ]
                    },
                    {
                        "Fn::GetAtt": [
                            "CloudMetadata",
                            "vpc.SecurityGroups.SgInternetOut"
                        ]
                    }
                ],
                "UserData": {
                    "Fn::Base64": {
                        "Fn::Join": [
                            "",
                            [
                                "#!/bin/bash -xe\n",
                                "yum update -y\n",
                                "# Install the files and packages from the metadata\n",
                                "/opt/aws/bin/cfn-init -v ",
                                "  --stack ",
                                {
                                    "Ref": "AWS::StackName"
                                },
                                "  --resource LaunchConfig ",
                                "  --configsets Group2 ",
                                "  --region ",
                                {
                                    "Ref": "AWS::Region"
                                },
                                " > /tmp/cfn-init.log 2>&1",
                                "\n"
                            ]
                        ]
                    }
                }
            }
        }
    }
}