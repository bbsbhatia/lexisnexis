def call(Map params = [:]){
    model_props = readProperties file: "${PROJECT_PATH}/classifiermodel.txt"                    
    verFile = """
JENKINS BUILD ID: [${BUILD_NUMBER}] ${BUILD_URL}

GIT LOCATION: ${GIT_URL}
GIT BRANCH:   ${GIT_BRANCH}
GIT COMMIT:   ${GIT_COMMIT}
MODEL SOURCE: ${S3_BUCKET}/${model_props.version}
"""
    dir(env.PROJECT_PATH) {
        writeFile file: 'version.txt', text: verFile
    }
    println(verFile)
}