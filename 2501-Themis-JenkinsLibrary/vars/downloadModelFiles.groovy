def call(Map params = [:]){
    model_props = readProperties file: "${PROJECT_PATH}/classifiermodel.txt"                    
    model_dir   = "${PROJECT_PATH}/classifiermodel"
    sh "mkdir -p ${model_dir}"
    dir(model_dir) {
        downloadS3Files(model_props)
    }
}

def downloadS3Files(modelprops) {    
    def modelversion = modelprops.version

    modelprops.each { name, value ->
        if(name=="version") return
        def file = value.replaceAll("\\s","")
        def key  = "${modelversion}/${file}".replaceAll("\\s","")
        boto3(
                account: AWS_ACCOUNT,
                arguments: ["Filename": file, "Bucket": S3_BUCKET, "Key": key],
                client: "s3",
                method: "download_file",
                region: "us-east-1"
        )
        println "Downloaded Model File: ${S3_BUCKET}/${key}"
    }
}