import boto3
import argparse

dynamodb = boto3.client("dynamodb")


def get_model_score(id_, version=None):
    params = {
        "IndexName": "id-score-index",
        "TableName": "2492-model-history",
        "ProjectionExpression": "score",
        "ScanIndexForward": False,
        "Limit": 1,
        "KeyConditionExpression": "id = :id",
        "ExpressionAttributeValues": {":id": {"S": id_}},
    }

    if version is not None:
        del params["IndexName"]
        del params["ScanIndexForward"]
        params["KeyConditionExpression"] += " AND version = :version"
        params["ExpressionAttributeValues"][":version"] = {"N": str(version)}

    try:
        response = dynamodb.query(**params)
    except Exception as e:
        print(f"DynamoDB query encountered an error.\n{e}")
        raise

    items = response.get("Items", [])
    score = None
    if len(items) != 0:
        score = float(items[0]["score"]["N"])
    return score


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model-id", dest="id", type=str, required=True)
    parser.add_argument("--model-version", dest="version", type=int, required=True)
    args = parser.parse_args()
    model_score = get_model_score(args.id, args.version)
    max_score = get_model_score(args.id)
    deployable = False
    if model_score and max_score and (model_score >= max_score):
        deployable = True

    print("Model score: ", model_score, "Max score (across model versions): ", max_score, "Deployable: ", deployable)

