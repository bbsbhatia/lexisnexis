import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput

def call(Map params=[:]){
    String command = params.get('command', null)
    def retVal = null

    switch (command) {
        case 'getLatestStableBuild':
            retVal = getLatestStableBuild(params)
        default:
            echo "You must specify a command, or specified command not yet implimented"
            break
    }

    return retVal
}

// workaround while waiting for permissions
// def getLatestStableBuild(Map params=[:]) {
//     def ecrRepoName = params.get('ecrRepoName')
//     def account = params.get('account')
//     def buildNum = null
    
//     withAwsAccount(account) {
//         def getBuildTag = """
//             aws ecr describe-images \
//                 --repository-name ${ecrRepoName} \
//                 --query "imageDetails[? imageTags[? contains(@, 'latest')] && imageTags[? contains(@, 'stable')]] | [0].imageTags[? starts_with(@, 'build')]" \
//                 --output text
//         """
//         def buildTag = sh(script: getBuildTag, returnStdout: true)
//         buildNum = buildTag.replace("build.", "").trim()
//     }
    
//     return buildNum
// }

def getLatestStableBuild(Map params=[:]) {
    def ecrRepoName = params.get('ecrRepoName')
    def account = params.get('account')
    def region = params.get('region')
    
    def buildNum = null
    def imageIds = [["imageTag":"stable"]]
    def tags = ["stable", "latest"]
    
    images = ecr(
        command: 'describeImages', 
        account: account,
        region: region, 
        ecrRepoName: ecrRepoName, 
        imageIds: imageIds, 
        tags: tags
    )

    try {
        assert images.size() == 1
        buildNum = images[0]["imageTags"].find{ it.startsWith('build.')}.substring(6)
    } catch (Exception e) {
        println "Error identifying latest stable build. ${e}"
    }

    return buildNum
}

def getRepoNames(Map params=[:]) {
    def projectPath = params.get('projectPath')
    def assetName = params.get('assetName')
    def assetId = params.get('assetId')
    def assetAreaName = params.get('assetAreaName')
    def repoName = null

    dir(projectPath) {
        def dockerFolder = ['DevOps', 'Docker'].join(File.separator)
        def composeFileLocation = [dockerFolder, 'docker-compose.yml'].join(File.separator)
        def composeFileContents = sh(script: "cat ${composeFileLocation}", returnStdout: true)
        def serviceBlockPattern = /(?s)services(.*?)^\w/
        def servicePattern = /.*?(\w*):\n.*?build:/
        
        if (!(composeFileContents ==~ serviceBlockPattern)) {
            throw new Exception('Unable to identify services block from compose file.')
        }

        def serviceBlockMatch = (composeFileContents =~ serviceBlockPattern)
        
        if (!(serviceMatch[0][1]) ==~ servicePattern) {
            throw new Exception('Unable to identify services from services block.')
        }
        def servicesMatch = (serviceMatch[0][1] =~ servicePattern)

        repoName = [
            assetId, 
            assetName.toLowerCase(), 
            assetAreaName.toLowerCase(), 
            serviceName.toLowerCase()
        ].join("/")
    }   
    return repoName 
}