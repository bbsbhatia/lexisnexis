def call(Map params = [:]){
    String command = params.get('command', null)
    def retVal = null

    switch (command) {
        case 'evaluateModel':
            retVal = evaluateModel(params)
            break
        default:
            echo "You must specify a command, or specified command not yet implimented"
            break
    }

    return retVal
}

def evaluateModel(Map params=[:]) {
    String region = params.get('region', 'us-east-1')
    String account = params.get('account')
    def deployable = false
    def pattern = /(.*)-(\d*)/

    if (!(params.trainingJobName ==~ pattern)) {
        println "Couldn't parse trainingJobName"
        return false
    }
    
    def match = (params.trainingJobName =~ pattern)
    def id = match[0][1]
    def version = match[0][2]    
    match = null
    // Obtain Library Resources
    def assumeRoleScript = libraryResource "aws/sts/assumeRole.py"
    writeFile file: "temp/assumeRole.py", text: assumeRoleScript

    def evalModelScript = libraryResource "org/2492-ModelUtilities/evaluateModel.py"
    writeFile file: "temp/evaluateModel.py", text: evalModelScript

    // Get AWS Credentials for desired account/region
    def assumeRole = "python36 -u temp/assumeRole.py -account $account -region $region"
    wrap([$class: 'BuildUser']) {
        envvars = sh(script: assumeRole, returnStdout: true).trim().tokenize(',')
    }

    // Inject AWS Tokens into Environment
    withEnv(envvars){
        wrap([$class: 'BuildUser']) {
            params['buildUserId'] = BUILD_USER_ID
            // Execute python script
            def evalModel = "python36 -u temp/evaluateModel.py --model-id ${id} --model-version ${version}"
            def result = sh(script: evalModel, returnStdout: true).trim()
            echo result
            if (result.contains("Deployable:  True")) {
                deployable = true
            }
        }
    }

    return deployable  
}