{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Creates a SageMaker EFS mount and Backup",
  "Outputs": {
    "FileSystemId": {
      "Description": "EFS File System ID",
      "Value": {
        "Ref": "SageMakerEfs"
      }
    },
    "MountIp1": {
      "Description": "EFS Mount Target 1",
      "Value": {
        "Fn::GetAtt": [
          "MountTarget1",
          "IpAddress"
        ]
      }
    },
    "MountIp2": {
      "Description": "EFS Mount Target 2",
      "Value": {
        "Fn::GetAtt": [
          "MountTarget2",
          "IpAddress"
        ]
      }
    },
    "MountIp3": {
      "Description": "EFS Mount Target 3",
      "Value": {
        "Fn::GetAtt": [
          "MountTarget3",
          "IpAddress"
        ]
      }
    }
  },
  "Parameters": {
    "BuildNumber": {
      "Description": "Jenkins Build Number",
      "Type": "String",
      "Default": "1"
    },
    "AssetID": {
      "Description": "Asset id of GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": ""
    },
    "AssetName": {
      "Description": "Short name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": ""
    },
    "AssetAreaName": {
      "Description": "Short asset area name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": ""
    },
    "AssetGroup": {
      "Description": "AssetGroup where this is deployed in",
      "Type": "String",
      "Default": ""
    }
  },
  "Resources": {
    "CloudMetadata": {
      "Type": "Custom::CloudMetadata",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "AssetID": {
          "Ref": "AssetID"
        },
        "AssetGroup": {
          "Ref": "AssetGroup"
        },
        "AssetAreaName": {
          "Ref": "AssetAreaName"
        },
        "Version": "1",
        "LastUpdate": {
          "Ref": "BuildNumber"
        }
      }
    },
    "SageMakerEfs": {
      "Type": "AWS::EFS::FileSystem",
      "Properties": {
        "PerformanceMode": "generalPurpose",
        "FileSystemTags": [
          {
            "Key": "Name",
            "Value": {
              "Fn::Sub": "${AssetGroup}-${AssetID}-${AssetAreaName}-SageMaker"
            }
          },
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          }
        ]
      }
    },
    "MountTarget1": {
      "Type": "AWS::EFS::MountTarget",
      "Properties": {
        "FileSystemId": {
          "Ref": "SageMakerEfs"
        },
        "SubnetId": {
          "Fn::Select": [
            0,
            {
              "Fn::GetAtt": [
                "CloudMetadata",
                "vpc.PrivateSubnetList"
              ]
            }
          ]
        },
        "SecurityGroups": [
          {
            "Ref": "EfsSg"
          }
        ]
      }
    },
    "MountTarget2": {
      "Type": "AWS::EFS::MountTarget",
      "Properties": {
        "FileSystemId": {
          "Ref": "SageMakerEfs"
        },
        "SubnetId": {
          "Fn::Select": [
            1,
            {
              "Fn::GetAtt": [
                "CloudMetadata",
                "vpc.PrivateSubnetList"
              ]
            }
          ]
        },
        "SecurityGroups": [
          {
            "Ref": "EfsSg"
          },
          {
            "Fn::GetAtt": [
              "CloudMetadata",
              "vpc.SecurityGroups.SgRemoteAccess"
            ]
          }
        ]
      }
    },
    "MountTarget3": {
      "Type": "AWS::EFS::MountTarget",
      "Properties": {
        "FileSystemId": {
          "Ref": "SageMakerEfs"
        },
        "SubnetId": {
          "Fn::Select": [
            2,
            {
              "Fn::GetAtt": [
                "CloudMetadata",
                "vpc.PrivateSubnetList"
              ]
            }
          ]
        },
        "SecurityGroups": [
          {
            "Ref": "EfsSg"
          }
        ]
      }
    },
    "EfsSg": {
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "GroupDescription": "Allows traffic on ports for EFS",
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          }
        ],
        "VpcId": {
          "Fn::GetAtt": [
            "CloudMetadata",
            "vpc.Id"
          ]
        }
      }
    },
    "EfsSgIngressFromVpcSgPort2049": {
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "Description": "Vpc",
        "GroupId": {
          "Ref": "EfsSg"
        },
        "IpProtocol": "tcp",
        "FromPort": "2049",
        "ToPort": "2049",
        "CidrIp": {
          "Fn::GetAtt": [
            "CloudMetadata",
            "vpc.Cidr"
          ]
        }
      }
    },
    "EfsSgEgressToVpcSgPort2049": {
      "Type": "AWS::EC2::SecurityGroupEgress",
      "Properties": {
        "Description": "Vpc",
        "GroupId": {
          "Ref": "EfsSg"
        },
        "IpProtocol": "tcp",
        "FromPort": "2049",
        "ToPort": "2049",
        "CidrIp": {
          "Fn::GetAtt": [
            "CloudMetadata",
            "vpc.Cidr"
          ]
        }
      }
    },
    "BackupRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": [
                  "backup.amazonaws.com"
                ]
              },
              "Action": [
                "sts:AssumeRole"
              ]
            }
          ]
        },
        "Path": {
          "Fn::Sub": "/AssetApplication_${AssetID}/"
        },
        "Policies": [
          {
            "PolicyName": "EfsBackupAccess",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowEfsBackupAccess",
                  "Effect": "Allow",
                  "Action": [
                    "elasticfilesystem:Backup"
                  ],
                  "Resource": {
                    "Fn::Sub": "arn:aws:elasticfilesystem:${AWS::Region}:${AWS::AccountId}:file-system/${SageMakerEfs}"
                  }
                }
              ]
            }
          }
        ]
      }
    },
    "BackupVault": {
      "Type": "AWS::Backup::BackupVault",
      "Properties": {
        "BackupVaultName": {
          "Fn::Sub": "${AssetGroup}-${AssetAreaName}-SageMaker"
        },
        "BackupVaultTags": {
          "AssetID": {
            "Ref": "AssetID"
          },
          "AssetGroup": {
            "Ref": "AssetGroup"
          },
          "AssetName": {
            "Ref": "AssetName"
          },
          "AssetAreaName": {
            "Ref": "AssetAreaName"
          }
        }
      }
    },
    "BackupPlan": {
      "Type": "AWS::Backup::BackupPlan",
      "Properties": {
        "BackupPlan": {
          "BackupPlanName": {
            "Fn::Sub": "${AssetGroup}-${AssetAreaName}"
          },
          "BackupPlanRule": [
            {
              "RuleName": "Daily",
              "CompletionWindowMinutes": 600,
              "Lifecycle": {
                "DeleteAfterDays": "7"
              },
              "RecoveryPointTags": {
                "AssetID": {
                  "Ref": "AssetID"
                },
                "AssetGroup": {
                  "Ref": "AssetGroup"
                },
                "AssetName": {
                  "Ref": "AssetName"
                },
                "AssetAreaName": {
                  "Ref": "AssetAreaName"
                }
              },
              "ScheduleExpression": "cron(0 3 ? * MON,TUE,WED,THU,FRI,SAT *)",
              "TargetBackupVault": {
                "Ref": "BackupVault"
              }
            }
          ]
        },
        "BackupPlanTags": {
          "AssetID": {
            "Ref": "AssetID"
          },
          "AssetGroup": {
            "Ref": "AssetGroup"
          },
          "AssetName": {
            "Ref": "AssetName"
          },
          "AssetAreaName": {
            "Ref": "AssetAreaName"
          }
        }
      }
    },
    "BackupSelection": {
      "Type": "AWS::Backup::BackupSelection",
      "Properties": {
        "BackupPlanId": {
          "Ref": "BackupPlan"
        },
        "BackupSelection": {
          "IamRoleArn": {
            "Fn::GetAtt": [
              "BackupRole",
              "Arn"
            ]
          },
          "Resources": [
            {
              "Fn::Sub": "arn:aws:elasticfilesystem:${AWS::Region}:${AWS::AccountId}:file-system/${SageMakerEfs}"
            }
          ],
          "SelectionName": "SageMaker EFS"
        }
      }
    }
  }
}