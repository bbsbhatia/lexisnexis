{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Creates a secrets manager secret to use with SageMaker Git Repo",
  "Outputs": {
    "GitSecretArn": {
      "Description": "Arn for the sagemaker git secret",
      "Value": {
        "Ref": "SageMakerGitSecret"
      }
    }
  },
  "Parameters": {
    "BuildNumber": {
      "Description": "Jenkins Build Number",
      "Type": "String",
      "Default": "1"
    },
    "AssetID": {
      "Description": "Asset id of GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": ""
    },
    "AssetName": {
      "Description": "Short name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": ""
    },
    "AssetAreaName": {
      "Description": "Short asset area name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": ""
    },
    "AssetGroup": {
      "Description": "AssetGroup where this is deployed in",
      "Type": "String",
      "Default": ""
    },
    "SecretName": {
      "Description": "[Required] Name of the secret",
      "Type": "String",
      "Default": ""
    },
    "GitUsername": {
      "Description": "[Required] Git username for the personal token. typically this is {legalid}@legal.regn.net",
      "Type": "String",
      "NoEcho": true,
      "Default": ""
    },
    "GitPersonalToken": {
      "Description": "[Required] Git repository associated with the notebook instance as its default code repository",
      "Type": "String",
      "NoEcho": true,
      "Default": ""
    }
  },
  "Resources": {
    "SageMakerGitSecret": {
      "Metadata": {
        "Comment": "The Name of this secret MUST start with AmazonSageMaker for sagemaker to pick it up properly"
      },
      "Type": "AWS::SecretsManager::Secret",
      "Properties": {
        "Description": "SageMaker Git",
        "Name": {
          "Fn::Sub": "AmazonSageMaker-${SecretName}"
        },
        "SecretString": {
          "Fn::Sub": "{\"username\": ${GitUsername},\"password\": ${GitPersonalToken}}"
        },
        "Tags": [
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          },
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          }
        ]
      }
    }
  }
}