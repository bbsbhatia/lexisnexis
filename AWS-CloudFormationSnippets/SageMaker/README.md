# Introduction 
These templates provide a secure way to configure and deploy SageMaker via CloudFormation.  It provides options to deploy with a Git Repo and also EFS setup

# Getting Started
##Git Repo Setup (Recommended but not required)

Note: If you already have a Git secret configured to go step 3.
1) Create your personal git access token https://tfs-glo-lexisadvance.visualstudio.com/_usersSettings/tokens
2) Launch the SageMakerGitSecret.template to configure your secret and note the Output Arn value
3) Create the Git Repo by using the SecretArn from step2 (or previously known) and deploy the SageMakerGitRepo.template
4) Note the CodeRepoName in the output for when you launch your notebook

## EFS Setup (Optional)
If you want to use EFS as a storage backing for your notebooks do the following.  It's recommended that you share the 
same EFS volume accross multiple notebooks rather than a 1 to 1 mapping.  The EFS volume is backed up to AWS Backup Vault.

1) Deploy the EFS template
2) Note the IP address output in the template for when you launch your notebook

## Notebook Deployment
*If you want to use a GitRepo be sure you have a Git Repo configured (as defined above) and then pass in the CodeRepoName to the DefaultGitRepo parameter.

*If you want to use an EFS volume, be sure you first have the EFS deployed (as defined above) and then pass in **one** of the IP addresses from the output to the EfsIpAddress. It will be mounted at /home/ec2-user/SageMaker/efs when the notebook is launched

1) Run the SageMakerNotebook.template and pass in the appropriate parameter. 
You may need to add permissions to the IAM role within the default
template, however be aware you should *only add the least permissions needed*. 
**Do not use a * in the Actions.**
2) Wait for the notebook to come up