# S3 CloudFormation
Creates a S3 resource.

## Description
This area can contain multiple flavors of S3 templates.  

## Work Ahead
* S3 bucket retention policies (Standard, IA, RRS, Glacier)
* Encryption options

## Contributors
Christopher De Witt <christopher.dewitt@lexisnexis.com>