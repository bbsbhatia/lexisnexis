These templates are used to create a DataCenterExtension VPC across up to 3 AZ's with 3 private subnets. It creates the VPN Gateway, SG's, Routes, NACL, IAM role.  This VPC will have no internet gateway or direct internet connection (that is obtained through connection back through our DataCenter)

To run these templates, you will need an S3 bucket in which you can place all the templates. This s3 bucket have a folder structure as follows: {S3Bucket}/{AwsAccountId}/{Region}

    Name all the files so that they match the prefix to your vpc name. Typically we name the vpc's with a number so in the future if we have to grow we can easily. example: newlexis-vpc1-*. Do not change the name structure of the files

    Open up the {vpcName}-gateways.template and set the appropriate default values for VPCName and AssetName.

    Upload the {vpcName}-gateways.template and {vpcName}-gateways-stack-policy.json file to the s3 bucket under the appropriate accountid/region. Note the s3 URL of the {vpcName}-gateways.template object since you will need this in the next step

    Create a new CloudFormation Stack, Click Specify an Amazon S3 template URL and specify the s3 url of the {vpcName}-gateways.template in step 3.

    set the StackName to match the name of the template minus the extension. ie. {vpcName}-gateways

    On the next page, click Advanced, go to Stack policy, and Upload the {vpcName}-gateways-stack-policy.json policy, click Next then Create. Note: This stack policy will prevent accidental deleting or update-replacement on the Internet Gateway and VPN

    Once the stack is finished, Click on the outputs tab of the stack and copy the VpnGatewayOne id. Note: there are 2 VpnGateway's that are created, the 2nd one isn't used unless we do upgrades on the DC circuits which require new circuits to be created, like when we went from 1 to 10GB

    Find the stack that is {vpcName}.template and open that up. This is the main driver stack for the entire VPC. This is the only stack you will have to create to build the rest of the VPC.

    Go through the Parameters and enter the appropriate Default values to make launching the template easier. Some key points are as follows: VpcName: Typically you want this to match the same prefix you used in the cloudformation templates OpsVpcCidr: If you are not peering to an operations account that's owned by an E2E team, set this value to be the same as the ProductVpcCidr. EnterpriseVpnGatewayId: This is the output from the gateways stack in step 7 NestedStackS3Location: s3 bucket location without the trailing / OpsPeeringConnectionId: currently not supported with these templates

    Once you have all the defaults set, upload all the templates into the s3 bucket location from before. Note the s3 url value of the {vpcName}.template file

    Create a new CloudFormation Stack, Click Specify an Amazon S3 template URL and specify the s3 url of the {vpcName}.template in step 10

    set the StackName to match the name of the template minus the extension. ie. {vpcName}

    On the next page, click Advanced, go to Stack policy, and Upload the {vpcName}-stack-policy.json policy, click Next then Create. Note: This stack policy will prevent accidental deleting or update-replacement on the Vpc Stack

    Check the box that says you understand this will create an IAM::Role, and click Create.

    Relax!

Updating changes

When you make updates you will need to copy the file into the s3 bucket. You only ever update the main stack, do not directly update the sub-stacks. If you are not making changes to the main stack {vpcName}.template, you can choose update stack and select use/existing, just continue through and update the stack. It will go through all the sub-stacks and look for changes to apply. If you made changes to the main, {vpcName}.template, then you need to use the new version of this stack in the Stack Update, rather than use existing. Just select the s3 option and point to the url of the {vpcName}.template in the s3 bucket.

More to come when I have time....

Known Issues/Enhancements:

Will add others when I remember 😃
