# AWS-CloudFormationSnippets
A repository for sharing cloudformation templates. These could be basic templates to help a team get started with infrastructure-as-code. 

## Guidelines
This repository will contain folders which represent the area the cloudformation template belongs to. For example, templates that would be used to stand up a new VPC would go in the subfolder VPC, along with a README.md providing documentation.

## Contributors
Mark Seitter <mark.seitter@lexisnexis.com>
Christopher De Witt <christopher.dewitt@lexisnexis.com>
