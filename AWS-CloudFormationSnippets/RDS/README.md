# RDS CloudFormation
Creates a RDS cluster.

## Description
This area can contain multiple flavors of RDS templates.  

## Work Ahead
The original template provided was for an Aurora DB. The other DB options in the template may require additional parameters. If you use this template for another RDS than Aurora, it would be most excellent if you can update this template with your additional parameters.

## Contributors
Christopher De Witt <christopher.dewitt@lexisnexis.com>