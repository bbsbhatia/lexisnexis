{
  "Description": "This template launches an Amazon RDS DB Cluster.",
  "Parameters": {
    "DNSHostedZoneName": {
      "Description": "Route 53 DNS hosted zone name",
      "Type": "String"
    },
    "DNSRecordSetName": {
      "Description": "Route 53 DNS record set name",
      "Type": "String"
    },
    "TagAssetID": {
      "Description": "LNG Asset ID",
      "Type": "String"
    },
    "TagAssetName": {
      "Description": "LNG Asset Name",
      "Type": "String"
    },
    "TagDBClusterName": {
      "Description": "LNG Asset DB Cluster Name",
      "Type": "String"
    },
    "TagDBInstanceName": {
      "Description": "LNG Asset DB Instance Name",
      "Type": "String"
    },
    "TagAssetGroup": {
      "Description": "LNG Asset Group Name (formerly stage)",
      "Type": "String",
      "Default": "ddc1",
      "AllowedValues": [
        "sand1",
        "ddc1",
        "cdc1",
        "pdc1"
      ]
    },
    "TagEnvironment": {
      "Description": "LNG Environment",
      "Type": "String",
      "Default": "DEV",
      "AllowedValues": [
        "SAND",
        "DEV",
        "CERT",
        "PROD"
      ]
    },
    "TagDBClusterPurpose": {
      "Description": "LNG Asset DB Cluster Purpose",
      "Type": "String",
      "Default": "Database Cluster"
    },
    "TagDBInstancePurpose": {
      "Description": "LNG Asset DB Instance Purpose",
      "Type": "String",
      "Default": "Database Instance"
    },
    "RDSInstanceType": {
      "Description": "The RDS Instance Type for the RDS instance. ",
      "Type": "String",
      "Default": "db.t2.micro",
      "AllowedValues": [
        "db.t2.micro",
        "db.t2.small",
        "db.t2.medium",
        "db.t2.large",
        "db.m4.large",
        "db.m4.xlarge",
        "db.m4.2xlarge",
        "db.m4.4xlarge",
        "db.m4.10xlarge",
        "db.r3.large",
        "db.r3.2xlarge",
        "db.r3.4xlarge",
        "db.r3.10xlarge"
      ]
    },
    "RDSEngine": {
      "Description": "The RDS Engine we will use.",
      "Type": "String",
      "Default": "aurora",
      "AllowedValues": [
        "mysql",
        "mariadb",
        "oracle-se1",
        "oracle-se2",
        "oracle-se",
        "oracle-ee",
        "sqlserver-ee",
        "sqlserver-se",
        "sqlserver-ex",
        "sqlserver-web",
        "postgres",
        "aurora"
      ]
    },
    "RDSInstanceIdentifier": {
      "Description": "The DB Identifier (e.g. rdsaurora)",
      "Type": "String"
    },
    "RDSUsername": {
      "Description": "The username for the RDS.",
      "Type": "String",
      "Default": "admin",
      "AllowedValues": [
        "admin"
      ]
    },
    "RDSPassword": {
      "Description": "[TODO: Requirements for input to meet password length/chars] The password for the RDS.",
      "Type": "String"
    }
  },
  "Mappings": {},
  "Resources": {
    "dbsubnetgroup": {
      "Type": "AWS::RDS::DBSubnetGroup",
      "Properties": {
        "DBSubnetGroupDescription": "DBSubnetGroups",
        "SubnetIds": [
          {
            "Fn::ImportValue": "PrivateSubnetA"
          },
          {
            "Fn::ImportValue": "PrivateSubnetB"
          }
        ]
      }
    },
    "dbcluster": {
      "Type": "AWS::RDS::DBCluster",
      "Properties": {
        "MasterUsername": {
          "Ref": "RDSUsername"
        },
        "MasterUserPassword": {
          "Ref": "RDSPassword"
        },
        "DBSubnetGroupName": {
          "Ref": "dbsubnetgroup"
        },
        "Engine": {
          "Ref": "RDSEngine"
        },
        "VpcSecurityGroupIds": [
          {
            "Fn::ImportValue": "SgDbBaseLayer"
          }
        ],
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "TagAssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "TagAssetName"
            }
          },
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                ":",
                [
                  {
                    "Ref": "TagAssetID"
                  },
                  {
                    "Ref": "TagDBClusterName"
                  }
                ]
              ]
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "TagAssetGroup"
            }
          },
          {
            "Key": "Environment",
            "Value": {
              "Ref": "TagEnvironment"
            }
          },
          {
            "Key": "Purpose",
            "Value": {
              "Ref": "TagDBClusterPurpose"
            }
          }
        ]
      },
      "DeletionPolicy": "Snapshot",
      "DependsOn": "dbsubnetgroup"
    },
    "dbinstance": {
      "Type": "AWS::RDS::DBInstance",
      "Properties": {
        "DBInstanceClass": {
          "Ref": "RDSInstanceType"
        },
        "DBInstanceIdentifier": {
          "Ref": "RDSInstanceIdentifier"
        },
        "Engine": {
          "Ref": "RDSEngine"
        },
        "DBSubnetGroupName": {
          "Ref": "dbsubnetgroup"
        },
        "DBClusterIdentifier": {
          "Ref": "dbcluster"
        },
        "PubliclyAccessible": "false",
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "TagAssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "TagAssetName"
            }
          },
          {
            "Key": "Name",
            "Value": {
              "Fn::Join": [
                ":",
                [
                  {
                    "Ref": "TagAssetID"
                  },
                  {
                    "Ref": "TagDBInstanceName"
                  }
                ]
              ]
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "TagAssetGroup"
            }
          },
          {
            "Key": "Environment",
            "Value": {
              "Ref": "TagEnvironment"
            }
          },
          {
            "Key": "Purpose",
            "Value": {
              "Ref": "TagDBInstancePurpose"
            }
          }
        ]
      },
      "DependsOn": "dbcluster"
    },
    "dbclusterdnsrecord": {
      "Type": "AWS::Route53::RecordSet",
      "Properties": {
        "HostedZoneName": {
          "Ref": "DNSHostedZoneName"
        },
        "Name": {
          "Ref": "DNSRecordSetName"
        },
        "Type": "CNAME",
        "TTL": "900",
        "ResourceRecords": [
          {
            "Fn::GetAtt": [
              "dbinstance",
              "Endpoint.Address"
            ]
          }
        ]
      }
    }
  }
}