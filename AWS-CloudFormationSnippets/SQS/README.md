# SQS CloudFormation
Creates a SQS resource.

## Description
This area can contain multiple flavors of SQS templates.  

## Work Ahead
It's unclear what other parameters or resources will make it in to the template.

## Contributors
Christopher De Witt <christopher.dewitt@lexisnexis.com>