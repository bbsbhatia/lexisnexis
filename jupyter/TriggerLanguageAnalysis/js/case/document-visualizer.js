var DocumentVisualizer = ( function (dictionary) {
    
    var nodes = [];
    
    var paraIds = dictionary.getParaIds();    
    paraIds.forEach(function(id){
        var color = dictionary.getPara(id).trigger ? '#F39C12' : '#ECF0F1';
        nodes.push({id: `para${id}`, label: `Para ${id}`, shape: 'circle', color: color, margin: 5});
    });
    
    var citeIds = dictionary.getCiteIds(), followedCiteIds = dictionary.getFollowedCiteIds(); 
    citeIds.forEach(function(id){
        var color = followedCiteIds.includes(id) ? '#196F3D' : '#ECF0F1';
        var descr = dictionary.getCite(id).descr;
        nodes.push({id: id, label: descr.substring(0,18), shape: 'box', color: color, margin: 5});
    });
    
      
    var edges = [], edgesCollected = {};
    citeIds.forEach(function(citeId){
        var trigger = dictionary.getCiteTrigger(citeId);
        if (!trigger) return;
        trigger.fragments.forEach(function(fragment){
           var paraId = `para${fragment['para_id']}`;
           edges.push({from: citeId, to: paraId, label: '', physics: true});
           edgesCollected[`${citeId}:${paraId}`] = true; 
        });
    });
    
    citeIds.forEach(function(citeId){
        var cite = dictionary.getCite(citeId);
        if (cite['anaphoric_id']) {
            edges.push({from: citeId, to: cite['anaphoric_id'], label: 'A', physics: true, arrows: 'to'});
        }
        var paraId = `para${cite['para_id']}`;
        if (edgesCollected[`${citeId}:${paraId}`]) return;
        edges.push({from: citeId, to: paraId, label: '', physics: true});
    });    

    var data = {
        nodes: nodes,
        edges: edges
    };

    var clickHandler = function(event) {
        console.log(event.nodes); 
    }

    var options = {};
    
    visualizer = {data:data, options:options};
    
    visualizer.render =  function(container) {
        if (this.network) this.network.destroy();
        var container = document.getElementById(container);
        this.network = new vis.Network(container, this.data, this.options);
        //this.network.on('click', clickHandler);
    }.bind(visualizer);

    visualizer.destroy =  function() {
        if (this.network) this.network.destroy();
    }.bind(visualizer);

    return visualizer;
    
}) (DocumentDictionary);