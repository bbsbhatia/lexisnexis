const initState = {
    paragraphIds: getParaIds(),
    followedCiteIds: getFollowedCiteIds(),
    selectedCiteId: null
}

function reducer(state = initState, action) {
    console.log(action, state);
    if (action.type == 'HILITE_LANGUAGE') {
        return {
            paragraphIds: state.paragraphIds,
            followedCiteIds: state.followedCiteIds,
            selectedCiteId: action.cite
        }
    }
}

const store = Redux.createStore(reducer);

const citeAction = { type: 'HILITE_LANGUAGE', cite: 'NV01' };

store.dispatch(citeAction);

const Provider = ReactRedux.Provider;
