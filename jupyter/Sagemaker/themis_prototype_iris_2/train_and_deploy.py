from __future__ import print_function

import argparse
import os
import json
import pandas as pd

from sklearn import tree
from sklearn.externals import joblib
from sklearn.metrics import precision_score, recall_score

from six import StringIO

from sagemaker_containers.beta.framework import worker

def _train_data(train_dir):
    # Take the set of files and read them all into a single pandas dataframe
    train_files = [ os.path.join(train_dir, file) for file in os.listdir(train_dir) ]
    
    if len(train_files) == 0:
        raise ValueError(('There are no files in {}.\n' +
                          'This usually indicates that the channel ({}) was incorrectly specified,\n' +
                          'the data specification in S3 was incorrectly specified or the role specified\n' +
                          'does not have permission to access the data.').format(train_dir, "train"))
    
    raw_data = [ pd.read_csv(file, header=None, engine="python") for file in train_files ]
    train_data = pd.concat(raw_data)
    
    return train_data


def _test_data(test_dir):
    # Take the set of files and read them all into a single pandas dataframe
    test_files = [ os.path.join(test_dir, file) for file in os.listdir(test_dir) ]
    
    if len(test_files) == 0:
        raise ValueError(('There are no files in {}.\n' +
                          'This usually indicates that the channel ({}) was incorrectly specified,\n' +
                          'the data specification in S3 was incorrectly specified or the role specified\n' +
                          'does not have permission to access the data.').format(test_dir, "test"))
    
    raw_data = [ pd.read_csv(file, header=None, engine="python") for file in test_files ]
    test_data = pd.concat(raw_data)
    return test_data


def input_fn(request_body, request_content_type):
    if request_content_type == 'application/json':
        json_data = json.load(StringIO(request_body))
        print('json_data:', json_data)
        print('json type:', type(json_data))
        return [json_data['params']]
    else:
        raise ValueError("{} not supported by script!".format(request_content_type))

        
def predict_fn(input_data, model):
    print('input_data:', input_data)
    prediction = model.predict(input_data)
    return prediction


def output_fn(prediction, accept):
    json_output = {"prediction": int(prediction[0])}
    print('json output:', json_output)
    return worker.Response(json.dumps(json_output), accept, mimetype=accept)

        
def model_fn(model_dir):
    """Deserialized and return fitted model
    
    Note that this should have the same name as the serialized model in the main method
    """
    clf = joblib.load(os.path.join(model_dir, "model.joblib"))
    return clf


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()

    # Hyperparameters are described here. In this simple example we are just including one hyperparameter.
    parser.add_argument('--max_leaf_nodes', type=int, default=-1)

    # Sagemaker specific arguments. Defaults are set in the environment variables.
    parser.add_argument('--output-data-dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])
    parser.add_argument('--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--train', type=str, default=os.environ['SM_CHANNEL_TRAIN'])
    parser.add_argument('--test', type=str, default=os.environ['SM_CHANNEL_TEST'])

    args = parser.parse_args()

    train_data = _train_data(args.train)

    # labels are in the first column
    train_y = train_data.ix[:,0]
    train_X = train_data.ix[:,1:]

    # Here we support a single hyperparameter, 'max_leaf_nodes'.
    max_leaf_nodes = args.max_leaf_nodes

    # Now use scikit-learn's decision tree classifier to train the model.
    clf = tree.DecisionTreeClassifier(max_leaf_nodes=max_leaf_nodes)
    clf = clf.fit(train_X, train_y)
    
    # Validate data
    test_data = _test_data(args.test)
    
    test_y = test_data.ix[:,0]
    test_X = test_data.ix[:,1:]
    
    pred_y = clf.predict(test_X)

    print("precision:", precision_score(test_y, pred_y, average='micro'))
    print("recall:", recall_score(test_y, pred_y, average='micro'))


    # Print the coefficients of the trained classifier, and save the coefficients
    joblib.dump(clf, os.path.join(args.model_dir, "model.joblib"))

