import json
import logging
import os
import sys

import requests

logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)


################################################################################
# Standard error handling
################################################################################
class FatalError(SystemExit):
    def __init__(self, reason):
        super(FatalError, self).__init__(-1)
        logger.error('Failing resource: {0}'.format(reason))


################################################################################
# Send Failed response
################################################################################
def send_failed_response(event: dict, reason: str, resource_id="None") -> None:
    status = 'FAILED'
    _send_response(event, status, reason, resource_id, {})


################################################################################
# Send Failed response
################################################################################
def send_success_response(event: dict, resource_id: str, data=None) -> None:
    status = 'SUCCESS'
    if not data:
        data = {}
    _send_response(event, status, "", resource_id, data)


################################################################################
# Send the Put response back to CloudFormation on success/failed
################################################################################
def _send_response(event: dict, response_status: str, reason: str, resource_id: str, data: dict) -> None:
    response_body = {'Status': response_status,
                     'Reason': reason,
                     'PhysicalResourceId': resource_id,
                     'StackId': event['StackId'],
                     'RequestId': event['RequestId'],
                     'LogicalResourceId': event['LogicalResourceId'],
                     'Data': data}
    logger.info('RESPONSE BODY: {0}'.format(response_body))
    logger.info('RESPONSE SIZE: %s bytes' % str(sys.getsizeof(json.dumps(response_body))))
    # TODO: if responsebody is greater than 4096 bytes, return failure
    req = ""
    try:
        req = requests.put(event['ResponseURL'], data=json.dumps(response_body))
        if req.status_code != 200:
            logger.error(req.text)
            raise FatalError('Recieved non 200 response while sending response to CFN.')
    except requests.exceptions.RequestException as e:
        logger.error(e)
        logger.error(req)
        raise FatalError("Error sending response body")


################################################################################
# return the AWS account id from a stack id
################################################################################
def get_account_id_from_stack_id(stack_id: str) -> str:
    return stack_id.split(':')[4]


################################################################################
# return the AWS region from the stack id
################################################################################
def get_region_from_stack_id(stack_id: str) -> str:
    return stack_id.split(':')[3]
