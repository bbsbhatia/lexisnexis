import logging
import os

from lng_aws_clients import session, apigateway, sts
from botocore.exceptions import ClientError
from time import sleep
import random

import custom_resource_helper

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

################################################################################
# Standard logging stuff for host log files to debug a run
################################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

asset_id = os.getenv('ASSET_ID')
assume_role_name = "ApiDocVersioningCrossAccountRole"


################################################################################
# main driver
################################################################################
def execute(event: dict) -> None:
    request_type = event['RequestType']

    # Always return success on a Delete event because we don't want to remove the Lambda Version record or add a new
    # one.  Deleting the Lambda function itself will in turn remove all the versions for us.
    if request_type == 'Delete':
        custom_resource_helper.send_success_response(event, event['LogicalResourceId'])
        return

    rest_api_id = event['ResourceProperties'].get('RestApiId')
    documentation_version = event['ResourceProperties'].get('DocumentationVersion')
    description = event['ResourceProperties'].get('Description', 'Build: {0}'.format(documentation_version))

    if not rest_api_id or not documentation_version:
        custom_resource_helper.send_failed_response(event,
                                                    'You must pass required parameters, RestApiId and DocumentationVersion')
        return

    stack_id = event['StackId']
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    region = custom_resource_helper.get_region_from_stack_id(stack_id)

    session.set_session()
    assume_resp = sts.get_client(local=True).assume_role(
        RoleArn='arn:aws:iam::{0}:role/AssetApplication_{1}/{2}'.format(account_id, asset_id, assume_role_name),
        RoleSessionName='ApiDocVersioning')

    session.set_assumed_session(region_name=region,
                                aws_access_key_id=assume_resp['Credentials']['AccessKeyId'],
                                aws_secret_access_key=assume_resp['Credentials']['SecretAccessKey'],
                                aws_session_token=assume_resp['Credentials']['SessionToken'])

    resp = {}
    # Note: If there are no changes from the previous version this API succeeds but no version is updated
    while True:
        try:
            resp = apigateway.get_client().create_documentation_version(restApiId=rest_api_id, documentationVersion=documentation_version,
                                                description=description)
        except ClientError as e:
            print(e.response["Error"]["Code"])
            if e.response["Error"]["Code"] in ['Throttling', 'TooManyRequestsException']:
                sleep_time = random.randint(15, 30)
                logger.warning(
                    "Received Throttling Exception on cloudformation.list_stacks sleeping {0} seconds before retry.".format(
                        sleep_time))

                sleep(sleep_time)
            elif e.response["Error"]["Code"] == 'ConflictException':
                # If we already have this created, just send the documentation_version back as the resource_id
                resp['version'] = documentation_version
                break
            else:
                break

    if not resp:
        custom_resource_helper.send_failed_response(event, 'Exception occurred when creating documentation version, check the logs')
    resource_id = resp['version']
    logger.info(resource_id)
    custom_resource_helper.send_success_response(event, resource_id)


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'c-sand'
    execute({
        'RequestType': 'Create',
        'ServiceToken': 'arn:aws:lambda:us-east-1:807841377931:function:LambdaVersionBuilder:STABLE',
        'ResponseURL': 'https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/mark-test-2/404a3e40-110b-11e8-bbc8-500c285ebefd%7CLambdaFunctionVersion%7Cea6cbdba-da55-470e-a652-0dc9d92533ec?AWSAccessKeyId=AKIAJNXHFR7P7YGKLDPQ&Expires=1518567312&Signature=KhcxX3U%2B4hxyAaA%2B8inl0m8QN%2BY%3D',
        'StackId': 'arn:aws:cloudformation:us-east-1:288044017584:stack/mark-test-2/404a3e40-110b-11e8-bbc8-500c285ebefd',
        'RequestId': 'ea6cbdba-da55-470e-a652-0dc9d92533ec',
        'LogicalResourceId': 'LambdaFunctionVersion',
        'ResourceType': 'Custom::LambdaVersion',
        'ResourceProperties': {
            'ServiceToken': 'arn:aws:lambda:us-east-1:807841377931:function:LambdaVersionBuilder:STABLE',
            'BuildNumber': '1',
            'LambdaName': 'mark-test-2-LambdaFunction-SINPWO9V0731'}
    })
