import logging
import os
import json
from time import sleep
import random
from botocore.exceptions import ClientError
from lng_aws_clients import session, cloudfront, sts

import custom_resource_helper

__author__ = "Kiran G, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"
################################################################################
# Standard logging stuff for host log files to debug a run
################################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)
asset_id = os.getenv('ASSET_ID')
assume_role_name = "CloudFrontOriginCrossAccountRole"


################################################################################
# main driver
################################################################################
def execute(event: dict) -> None:
    request_type = event['RequestType']

    stack_id = event['StackId']
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    region = custom_resource_helper.get_region_from_stack_id(stack_id)

    session.set_session()
    assume_resp = sts.get_client(local=True).assume_role(
        RoleArn='arn:aws:iam::{0}:role/AssetApplication_{1}/{2}'.format(account_id, asset_id, assume_role_name),
        RoleSessionName='CloudFrontOriginGroup')
    session.set_assumed_session(region_name=region,
                                aws_access_key_id=assume_resp['Credentials']['AccessKeyId'],
                                aws_secret_access_key=assume_resp['Credentials']['SecretAccessKey'],
                                aws_session_token=assume_resp['Credentials']['SessionToken'])

    if request_type == 'Create' or request_type == 'Update':
        create_update_origin_group(event)
        return

    if request_type == 'Delete':
        delete_origin_group(event)
        return


def create_update_origin_group(event) -> None:
    logger.info('Creating / Updating origin group  ')
    # unpack custom resource properties
    try:
        distribution_id = event['ResourceProperties']['DistributionId']
        primary_origin_id = event['ResourceProperties']['PrimaryOriginId']
        secondary_origin_id = event['ResourceProperties']['SecondaryOriginId']
        origin_group_id = event['ResourceProperties']['OriginGroupId']
        path_pattern = event['ResourceProperties']['PathPattern']
        failover_status_codes = event['ResourceProperties']['FailoverStatusCodes']

    except KeyError as key_error:
        logger.warning("KeyError unpacking ResourceProperties for {}".format(key_error))
        custom_resource_helper.send_failed_response(event,
                                                    "KeyError unpacking ResourceProperties for {}".format(key_error))
        return

    # get distribution
    distribution_response = interact_with_distribution(cloudfront.get_client().get_distribution_config,
                                                       Id=distribution_id)
    if distribution_response.get('Error'):
        custom_resource_helper.send_failed_response(event,
                                                    'Unable to get distribution : {}, Exception: {}'.format(
                                                        distribution_id,
                                                        distribution_response['Error']))
        return

    e_tag = distribution_response['ETag']
    logger.info("Distribution Config E Tag: {0}".format(e_tag))

    distribution_config = distribution_response["DistributionConfig"]
    logger.info("Distribution Config : {0}".format(json.dumps(distribution_config)))

    # validate origins
    for origin in (primary_origin_id, secondary_origin_id):
        if origin not in [remote_origin['Id'] for remote_origin in distribution_config['Origins']['Items']]:
            logger.info('Origin {} does not exist in Distribution {}'.format(origin, distribution_id))
            custom_resource_helper.send_failed_response(event,
                                                        'Origin {} does not exist in Distribution {}'.format(origin,
                                                                                                             distribution_id))
            return

    # validate and update behaviours
    behaviours = distribution_config["CacheBehaviors"]["Items"]
    path_pattern_exist = False
    for idx in range(len(behaviours)):
        if behaviours[idx]["PathPattern"] == path_pattern:
            behaviours[idx]["TargetOriginId"] = origin_group_id
            path_pattern_exist = True

    if not path_pattern_exist:
        logger.info("Path pattern {} does not exist in CacheBehaviour of Distribution {}".format(path_pattern,
                                                                                                 distribution_id))
        custom_resource_helper.send_failed_response(event,
                                                    "Path pattern {} does not exist in CacheBehaviour of "
                                                    "Distribution {}".format(path_pattern, distribution_id))
        return

    # validate and update origin group
    origin_groups = distribution_config["OriginGroups"]
    origin_groups_items = origin_groups.get("Items", [])
    for item in origin_groups_items:
        # no-op if origin group already exists
        if item["Id"] == origin_group_id:
            logger.info(
                'Origin Group {} already exists in Distribution {}'.format(origin_group_id, distribution_id))
            custom_resource_helper.send_success_response(event, origin_group_id)
            return

    new_origin_group_item = {
        "Id": origin_group_id,
        "FailoverCriteria": {
            "StatusCodes": {
                "Quantity": len(failover_status_codes),
                "Items": [int(i) for i in failover_status_codes]
            }
        },
        "Members": {
            "Quantity": 2,
            "Items": [
                {
                    "OriginId": primary_origin_id
                },
                {
                    "OriginId": secondary_origin_id
                }
            ]
        }
    }

    origin_groups_items.append(new_origin_group_item)
    origin_groups["Quantity"] = origin_groups["Quantity"] + 1
    origin_groups["Items"] = origin_groups_items

    # update distribution config
    distribution_config["OriginGroups"] = origin_groups
    distribution_config["CacheBehaviors"]["Items"] = behaviours
    logger.info("Updated Distribution Config : {0}".format(json.dumps(distribution_config)))

    # create/update distribution with origin group
    update_distribution_response = interact_with_distribution(cloudfront.get_client().update_distribution,
                                                              DistributionConfig=distribution_config,
                                                              Id=distribution_id,
                                                              IfMatch=e_tag
                                                              )

    if update_distribution_response.get('Error'):
        custom_resource_helper.send_failed_response(event,
                                                    'Unable to update distribution : {}, '
                                                    'Exception: {}'.format(distribution_id,
                                                                           update_distribution_response['Error']))
        return

    logger.info("Update Distribution Response : {0}".format(
        json.dumps(update_distribution_response["ResponseMetadata"])))
    custom_resource_helper.send_success_response(event, origin_group_id)
    return


def delete_origin_group(event) -> None:
    logger.info('Deleting origin group .. ')
    custom_resource_helper.send_success_response(event, event['LogicalResourceId'])
    return


def interact_with_distribution(cloudfront_func, **kwargs) -> dict:
    while True:
        try:
            distribution_response = cloudfront_func(**kwargs)
            return distribution_response
        except ClientError as e:
            logger.warning('ClientError: {}'.format(e.response['Error']['Code']))
            if e.response["Error"]["Code"] == 'Throttling':
                sleep_time = random.randint(15, 30)
                logger.warning(
                    "Received Throttling Exception on cloudfront.{0}, {1} seconds before retry.".format(
                        cloudfront_func.__name__,
                        sleep_time))
                sleep(sleep_time)
            else:
                logger.info('Unable to {0} with {1}, Error: {2}'.format(cloudfront_func.__name__, kwargs, e))
                return {'Error': e}
        except Exception as e:
            logger.info('Unable to {0} with {1}, Error: {2}'.format(cloudfront_func.__name__, kwargs, e))
            return {'Error': e}


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'c-sand'
    execute({
        "RequestType": "Create",
        "ServiceToken": "arn:aws:lambda:us-east-1:288044017584:function:add-cloudfront-origin",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/nanaax-test1-LambdaAsCustomResource/e1577300-aab4-11e8-8a5a-50d5ca6e6082%7CLambdaFunctionVersion%7C2a7a4d4d-4911-4670-bab1-e4ba0044d982?AWSAccessKeyId=AKIAJEMEJLKI636XPIMQ&Expires=1535462750&Signature=bieUMCXxMwXo6T4y5JzRm9EoQb0%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/nanaax-test1-LambdaAsCustomResource/e1577300-aab4-11e8-8a5a-50d5ca6e6082",
        "RequestId": "2a7a4d4d-4911-4670-bab1-e4ba0044d982",
        "LogicalResourceId": "LambdaFunctionVersion",
        "PhysicalResourceId": "123",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "DistributionId": "E3B7E2YD0C6GGA",
            "PrimaryOriginId": "US-East-1-Bucket-1",
            "SecondaryOriginId": "US-West-2-Bucket",
            "OriginGroupId": "Datalake-Object-Buckets",
            "PathPattern": "objects/store/*",
            "FailoverStatusCodes": ["403", "404", "500", "502", "503", "504"]
        }
    })
