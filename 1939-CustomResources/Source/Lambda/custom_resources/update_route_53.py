import base64
import datetime
import logging
import os
import re
import sys
import json

from lng_aws_clients import session, route53, sts, lambda_

import custom_resource_helper

__author__ = "Mark Seitter and Viral Desai"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "2.4"

################################################################################
# Standard logging stuff for host log files to debug a run
################################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

ALPHA = 'abcdefghijklmnopqrstuvwxyz'
ENCRYPT_KEY = 'markwashere2017!'
default_hosted_zone = os.getenv('DEFAULT_HOSTED_ZONE_NAME')


################################################################################
# Qualify the domain
# Ensure the domain name ends with a period (.) which makes the domain fully qualified
# Also ensure it's lowercase because Route53 entries are lowercase
################################################################################
def make_qualified(value: str) -> str:
    value = value.strip().lower()
    if value and not value[-1] == '.':
        value = '%s.' % value
    return value


################################################################################
# Encrypts the text with a key
# Uses Vignere cipher to do very simple encryption before writing out to route53 in plaintext
################################################################################
def encode(clear: str) -> str:
    enc = []
    for i in range(len(clear)):
        key_c = ENCRYPT_KEY[i % len(ENCRYPT_KEY)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        # Need to use latin_1 for backwards compatibility between python 2.7 and 3.6 with chr command
        enc.append(enc_c.encode('latin_1'))
    return base64.urlsafe_b64encode(b''.join(enc)).decode()


################################################################################
# Decrypts the text with a key
# Uses Vignere cipher to do very simple decryption
################################################################################
def decode(enc: str) -> str:
    dec = []
    # Need to use latin_1 for backwards compatibility between python 2.7 and 3.6 with chr command
    enc = base64.urlsafe_b64decode(enc).decode('latin_1')
    for i in range(len(enc)):
        key_c = ENCRYPT_KEY[i % len(ENCRYPT_KEY)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)


################################################################################
# Changes a EC2 DNS ip-10-20-30-40.ec2.internal into an ip address since we can't resolve on prem
################################################################################
def clean_record_ip(aliastarget: str) -> str:
    try:
        match_pattern = ".*?([\d]{1,3})[_.-]([\d]{1,3})[_.-]([\d]{1,3})[_.-]([\d]{1,3}).*"
        match_obj = re.match(match_pattern, aliastarget)
        if match_obj:
            aliastarget = '{0}.{1}.{2}.{3}'.format(match_obj.group(1), match_obj.group(2), match_obj.group(3), match_obj.group(4))

    except:
        pass

    return aliastarget


################################################################################
# returns if the record is in an ipv4 address format (doesn't validate the ip)
################################################################################
def is_ip_address(aliastarget: str) -> bool:
    match_pattern = "^([\d]{1,3})[.]([\d]{1,3})[.]([\d]{1,3})[.]([\d]{1,3})$"
    return bool(re.match(match_pattern, aliastarget))


################################################################################
# Return an CNAME record resourcerecordset
################################################################################
def cnameRecord(action, aliasrecord, aliastarget):
    # There is a condition in which this will return a value which will fail
    # If somebody changed the target the Alias points to, and a Delete request
    # is raised, this will return a record that can't be deleted. We want this happen
    # to give somebody time to be sure you want it to happen. If we need to have Delete
    # finish without issues copy the same logic in the txtRecord for action=='DELETE'
    type = 'CNAME'

    recordSet = {'Name': make_qualified(aliasrecord), 'Type': type, 'ResourceRecords': [{'Value': aliastarget}],
                 'TTL': 300}
    return {'Action': action, 'ResourceRecordSet': recordSet}


################################################################################
# Return an A record resourcerecordset
################################################################################
def aRecord(action, aliasrecord, hostedZone, aliastarget, cleanip=False):
    # There is a condition in which this will return a value which will fail
    # If somebody changed the target the Alias points to, and a Delete request
    # is raised, this will return a record that can't be deleted. We want this happen
    # to give somebody time to be sure you want it to happen. If we need to have Delete
    # finish without issues copy the same logic in the txtRecord for action=='DELETE'
    type = 'A'

    # This handles AWS DNS addresses like ip-10.178.15.20.ec2.internal that do not resolve
    # in our DNS, so we change them to the ip address for the A record
    if cleanip:
        logging.info('Attempting to cleaning aliastarget: %s' % aliastarget)
        aliastarget = clean_record_ip(aliastarget)
        logging.info('Cleaned aliastarget is now: %s' % aliastarget)

    # If we have an IP there is no AliasTarget, so we need to process special and not qualify the target
    if is_ip_address(aliastarget):
        logging.info('Have an IP address so creating an A record without AliasTarget')
        recordSet = {'Name': aliasrecord, 'Type': type, 'TTL': 300, 'ResourceRecords': [{"Value": aliastarget}]}
    else:
        aliasTarget = {'HostedZoneId': hostedZone, 'DNSName': make_qualified(aliastarget),
                       'EvaluateTargetHealth': False}
        recordSet = {'Name': aliasrecord, 'Type': type, 'AliasTarget': aliasTarget}

    return {'Action': action, 'ResourceRecordSet': recordSet}


################################################################################
# Return a TXT record resourcerecordset
################################################################################
def txtRecord(action: str, hostedzoneid: str, aliasrecord: str, stackId: str, accountId: str, region: str) -> dict:
    type = 'TXT'
    # if we are deleting, get the values directly from the txt record
    # delete values must match exactly otherwise the delete fails and if allowoverride
    # is set this can cause a legitimate delete to fail
    if action == 'DELETE':
        resourceRecordSet = get_record_set(hostedzoneid, aliasrecord, 'TXT')[0]
    else:
        resourceRecords = []

        keyPairValues = [{'Key': 'StackId', 'Value': stackId}, {'Key': 'AccountId', 'Value': accountId},
                         {'Key': 'Region', 'Value': region}, {'Key': 'AllowOverride', 'Value': 'false'}]

        # Note Value must have quotes around the actual string when passed in
        for value in keyPairValues:
            # Don't encrypt the allow-override feature
            if value['Key'] == 'AllowOverride':
                resourceRecords.append({'Value': '"%s=%s"' % (value['Key'], value['Value'])})
            else:
                resourceRecords.append({'Value': "\"" + encode("%s=%s" % (value['Key'], value['Value'])) + "\""})
        resourceRecordSet = {'Name': make_qualified(aliasrecord), 'Type': type, 'TTL': 300,
                             'ResourceRecords': resourceRecords}

    return {'Action': action, 'ResourceRecordSet': resourceRecordSet}


################################################################################
# return the record set
################################################################################
def get_record_set(zoneId, recordName, recordType):
    encryptedKeys = ['StackId', 'Region', 'AccountId', 'AllowOverride']

    # be sure to qualify the recordName
    qualified_name = make_qualified(recordName)

    resp = route53.get_client().list_resource_record_sets(HostedZoneId=zoneId, StartRecordName=qualified_name,
                                                          StartRecordType=recordType)
    results = []
    for r in resp['ResourceRecordSets']:
        # Is at the end of the list of matched records. No need to continue
        # since the records are sorted by name and type.
        if r['Name'] == qualified_name and r['Type'] == recordType:
            results.append(r)
        else:
            break

    # if recordType == 'TXT':
    #     for record in results:
    #         for value in record.get('ResourceRecords', ''):
    #             encryptedValue = value.get('Value', '')
    #             #Don't need to decrypt the AllowOverride
    #             key = str.replace(encryptedValue.split('=')[0], '"', '')
    #             if encryptedValue and key not in encryptedKeys:
    #             #if encryptedValue and not str.startswith(encryptedValue, '"%' %):
    #                 print encryptedValue
    #                 try:
    #                     decryptedValue = decode(encryptedValue)
    #                     value['Value'] = decryptedValue
    #                 except:
    #                     logging.error('Could not decrypt:')
    #                     logging.error(sys.exc_info())

    return results


################################################################################
# Decrypts the TXT record ignoring the AllowOverride since this is stored in plaintext
################################################################################
def decrypt_txt_record(enc):
    encryptedKeys = ['StackId', 'Region', 'AccountId', 'AllowOverride']

    for record in enc:
        for value in record.get('ResourceRecords', ''):
            encryptedValue = value.get('Value', '')
            # Don't need to decrypt the AllowOverride
            # Also handles legacy where the keys weren't encrypted
            key = str.replace(encryptedValue.split('=')[0], '"', '')
            if encryptedValue and key not in encryptedKeys:
                logging.info('Decrypting value: %s', encryptedValue)
                try:
                    decryptedValue = decode(encryptedValue)
                    value['Value'] = decryptedValue
                    logging.info('Decrypted value: %s', value['Value'])
                except:
                    logging.error('Could not decrypt:')
                    logging.error(sys.exc_info())

    return enc


################################################################################
# Validate routine
################################################################################
def validate_parameters(request_type, aliasrecord, aliastargetzoneid, aliastarget, aliascomment):
    if not aliasrecord:
        raise custom_resource_helper.FatalError(
            "No aliasrecord provided in the custom resource properties in the cloud Formation Template")

    # alias target will be available only for create/update
    if request_type == 'Create' or request_type == 'Update':
        if not aliastarget:
            raise custom_resource_helper.FatalError(
                "No aliastarget provided in the custom resource properties in the cloud Formation Template")

        if not aliastargetzoneid:
            raise custom_resource_helper.FatalError(
                "No aliastargetzoneid provided in the custom resource properties in the cloud Formation Template")


################################################################################
# remove the value from the route53
# input format is "Key=Value" and we need to strip the trailling "
################################################################################
def get_value_from_route53_txt(route53_value):
    value = route53_value.split('=')[1]
    if value and value[-1] == '"':
        value = value[:-1]
    return value


################################################################################
# Return hosted zone id from a hostedzone
################################################################################
def get_hosted_zone_id(hostedzone_name):
    # Need to do this loop because the query doesn't only return
    # the exact name (eg DNSName='152763.route53.lexis.com' returns all domains)
    hostedzoneid = ''
    # Get a zone
    hostedzone = route53.get_client().list_hosted_zones_by_name(DNSName=hostedzone_name)
    logging.info('Found hostedzone : %s' % hostedzone)
    for zone in hostedzone['HostedZones']:
        # find hosted zone id
        if zone['Name'] == make_qualified(hostedzone_name):
            hostedzoneid = zone['Id'].replace('/hostedzone/', '')
            logging.info('Found hostedzone id : %s' % hostedzoneid)
            break

    return hostedzoneid


################################################################################
# Update the Zone Serial Number if it's a new day
# RecordFormat   [authority-domain] [domain-of-zone-admin] [zone-serial-number]
#                 [refresh-time] [retry-time] [expire-time] [negative caching TTL]
################################################################################
def updateSoaZoneSerial(soa_record, hostedzoneid, curDate):
    zoneSerial = ''
    # Must be in this format (YYYYMMDD) so it's ever increasing
    dateNow = '%04d%02d%02d' % (curDate.year, curDate.month, curDate.day)
    # Santity check
    if soa_record and len(soa_record) == 1:
        recordValue = soa_record[0]['ResourceRecords'][0]['Value']
        recordValueSplit = recordValue.split(' ')
        if len(recordValueSplit) != 7:
            logging.error('Error: Too many record values expected 7, please correct')
            logging.error(recordValueSplit)
        else:
            zoneSerial = recordValueSplit[2]
            logging.info('Current ZoneSerial: %s', zoneSerial)
            logging.info('Current DateValue: %s', dateNow)
            if zoneSerial != dateNow:
                logging.info("Update needed to set ZoneSerial to: %s", dateNow)
                recordValueSplit[2] = str(dateNow)
                newRecordValue = ' '.join(recordValueSplit)
                soa_record[0]['ResourceRecords'][0]['Value'] = newRecordValue
                logging.info('Creating record set for SOA ZoneSerialNumber')
                logging.info('Action: UPSERT, Change: %s' % soa_record[0])
                resp = route53.get_client().change_resource_record_sets(HostedZoneId=hostedzoneid, ChangeBatch={
                    'Comment': 'Updating the SOA zone serial number',
                    'Changes': [{'Action': 'UPSERT', 'ResourceRecordSet': soa_record[0]}]})
                logging.info(resp)
                logging.info('Commited SOA Record')

    return ''


################################################################################
# Validate is the stack updating the resource belongs to the original created resource
################################################################################
def verify_route53_update_allowed(txt_object, requested_stack_id):
    stackIdSame = False
    allowOverride = False
    # we only have 1 object
    for val in txt_object[0]['ResourceRecords']:
        curVal = val['Value']
        if 'StackId=' in curVal:
            stackIdSame = (get_value_from_route53_txt(curVal) == requested_stack_id)
            # if you can override then we need to also return true to allow updates
        if 'AllowOverride=' in curVal:
            if get_value_from_route53_txt(curVal).lower() == 'true':
                allowOverride = True
    return (stackIdSame or allowOverride)


################################################################################
# modify entry into route53 recordset using common routine
################################################################################
def modify_route53_recordset_alias_record(event, request_type, aliasrecord, aliastargetzoneid, aliastarget,
                                          aliascomment, stack_id, hostedzoneid):
    # Action is derived from request_type, create/update are treated same
    if request_type == 'Create' or request_type == 'Update':
        action = 'UPSERT'
    else:
        action = 'DELETE'

    # get the region and account of the stack we are working on to
    # prevent issue in the future if we call lambda cross account/region
    stack_region = custom_resource_helper.get_region_from_stack_id(stack_id)
    stack_account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)

    logging.info('   stack region : %s', stack_region)
    logging.info('   stack accountid : %s', stack_account_id)

    # Get the a record if there is one
    # i.e. if delete request is issued for non-existent/already deleted alias record, then DELETE action
    # on change recordset fails at commit time and creates an exception.
    # This condition needs to be handled DELETE for non-existent a records need not create any changes
    a_record = get_record_set(hostedzoneid, aliasrecord, 'A')
    cname_record = get_record_set(hostedzoneid, aliasrecord, 'CNAME')

    logging.info('Looking up A record value in the zone to see if it exists already')
    logging.info('   a record : %s', a_record)
    logging.info('   action   : %s', action)

    if action == "UPSERT" or (action == "DELETE" and (a_record or cname_record)):
        # if the target is an alias then aliastargetzoneid is route53.lexis.com, otherwise it is elb hostedzoneid
        if aliastargetzoneid == 'ALIAS':
            # If we have cloudfront, set the target zone appropriately
            if getHostedZone(aliastarget) in ('cloudfront.net', 'cloudfront.net.'):
                aliastargetzoneid = 'Z2FDTNDATAQYW2'
            else:
                aliastargetzoneid = hostedzoneid
        # create change recordsets in the hosted zone
        logging.info('Creating new Change to Resource Record Set, hostedzoneid : %s', hostedzoneid)
        if aliastargetzoneid == 'CNAME':
            # CNAME TXT records must not match the CNAME record so we prefix with CNAME-
            aRecordSet = cnameRecord(action, aliasrecord, aliastarget)
            txtRecordSet = txtRecord(action, hostedzoneid, 'CNAME-' + aliasrecord, stack_id, stack_account_id,
                                     stack_region)
        else:
            aRecordSet = aRecord(action, aliasrecord, aliastargetzoneid, aliastarget, cleanip=True)
            txtRecordSet = txtRecord(action, hostedzoneid, aliasrecord, stack_id, stack_account_id, stack_region)

        logging.info('ChangeSet: %s%s' % (aRecordSet, txtRecordSet))
        resp = route53.get_client().change_resource_record_sets(HostedZoneId=hostedzoneid,
                                                                ChangeBatch={'Comment': aliascomment,
                                                                             'Changes': [aRecordSet,
                                                                                         txtRecordSet]})

        logging.info('Commiting change')
        logging.info(resp)

        logging.info('Request_type = %s, committed the changes', request_type)
    else:
        logging.info('Request_type = %s, but there was no Alias found, so bypassing action', request_type)

    # return alias record id back
    return aliasrecord


################################################################################
# return the hosted zone from a fully qualified route, otherwise return empty
################################################################################
def getHostedZone(routeName):
    splits = routeName.split('.')
    return '.'.join(splits[1:])


################################################################################
# Encrypt the accountid to hash the value for subdomains
# Until fully implemented, just return back the accountid passed in
################################################################################
def encryptAccountId(account_id):
    return account_id


################################################################################
# main driver
################################################################################
def execute(event):
    ################################################################################
    # Get the Request Type, Stack ID, and Logical Resource ID
    ################################################################################
    # Per support case # 234140611, the ELB hosted zone id will remain constant
    # for all ELB dns names, as of now different AWS account id can give different
    # hosted zone id, per the aws case, AWS is undergoing migration to consolidate
    # all ELB hosted zones under one zone per region, this list will be fixed even
    # after the migration is complete per the support case.

    elb_hosted_zones = {'us-west-1': 'Z368ELLRRE2KJ0',
                        'us-west-2': 'Z1H1FL5HABSF5',
                        'us-east-1': 'Z35SXDOTRQ7X7K',
                        'ap-southeast-1': 'Z1LMS91P8CMLE5',
                        'ap-southeast-2': 'Z1GM3OXH4ZPM65',
                        'ap-northeast-1': 'Z14GRHDCWA56QT',
                        'sa-east-1': 'Z2P70J7HTTTPLU',
                        'eu-west-1': 'Z32O12XQLNTSW2'
                        }

    logging.info(event)

    stack_id = event['StackId']
    region = custom_resource_helper.get_region_from_stack_id(stack_id)
    session.set_session()

    stack_id = event['StackId']

    request_type = event['RequestType']
    aliasrecord = event['ResourceProperties']['AliasRecord'].lower()
    aliastarget = event['ResourceProperties']['AliasTarget'].lower()
    aliascomment = event['ResourceProperties']['AliasComment']
    # Default to my current running account
    target_account = sts.get_client().get_caller_identity()['Account']
    # instead of getting from the resource properties, we will get this value
    # from the fixed dictionary object per the comment above in support case # 234140611
    aliastargetzoneid = elb_hosted_zones[region]

    ################################################################################
    # hosted zone is a constant for all practical purpose for all request here
    ################################################################################
    # hostedzone_name = getenv('HOSTED_ZONE_NAME', 'sand.r53-content.lexisnexis.com')
    hostedzone_name = getHostedZone(aliasrecord)
    if not hostedzone_name:
        # Attempt to get the default hosted zone
        hostedzone_name = default_hosted_zone
        if not hostedzone_name:
            custom_resource_helper.send_failed_response(event,
                                                        'You must pass in a fully qualified domain name or the default hosted zone needs to be set')
            logging.error('Found %s No fully qualified domain name found in %s' % (hostedzone_name, aliasrecord))
            return 'Done'

    logging.info('Starting a new change to hosted zone %s', hostedzone_name)

    ################################################################################
    # Still needs some work to be able to integarte accross accounts
    # For now commenting out until we have a requirement to update route53 entries
    # in subdomains to route53.lexis.com
    ################################################################################
    '''
    toplevel_hostedzone = 'route53.lexis.com'

    route53.lexisnexis.com - MD
    contentdev.route53.lexisnexis.com
    contentcert.route53.lexisnexis.com
    contentsandbox.route53.
    ccssandbox
    foobar.route53.lexisnexis.com
    lexisaidev.route53.lexisnexis.com


    splunkdev.route53.lexis.com

    hostedzone_name = getHostedZone(aliasrecord)
    target_account = ''
    #If the hostedzone_name exists, match it to which aws account it belongs to
    #If it's not a special case, assume the account the stack called from since we
    #can't decrypt the hash
    #If we have no hosted zone name, take the account id this came from, hash it
    #with our special key and append the top level domain to it
    if hostedzone_name:
        if hostedzone_name == 'route53.lexis.com':
            #We are hosted in this account so no need to change
            pass
        elif hostedzone_name.startswith('splunk'):
            target_account = 'placeholder'
        else:
            target_account = getAccountIdFromStackId(stack_id)
    else:
        target_account = getAccountIdFromStackId(stack_id)
        hashAccount = encryptAccountId(target_account)
        hostedzone_name= '%s.%s' % (hashAccount, toplevel_hostedzone)
    logging.info('Starting a new change to hosted zone %s', hostedzone_name)

    #If we have a target account and it's not ourself, 
    #assume the role to make the call into the route53 domain of target account
    if target_account and target_account != sts_client.get_caller_identity()['Account']:
        logging.info('Assuming Role in target account %s', target_account)
        role = 'arn:aws:iam::%s:role/customRoute53' % target_account
        resp = sts_client.assume_role(RoleArn=role, RoleSessionName='Route53-Custom-Resource')
        session = boto3.Session(region_name=region,aws_access_key_id=resp['Credentials']['AccessKeyId'],aws_secret_access_key=resp['Credentials']['SecretAccessKey'],aws_session_token=resp['Credentials']['SessionToken'])
        r53_client = session.client('route53')
'''

    ################################################################################
    # validate common parameters
    ################################################################################
    logging.info('Validating parameters:')
    logging.info('   request_type      : %s', request_type)
    logging.info('   aliasrecord       : %s', aliasrecord)
    logging.info('   aliastargetzoneid : %s', aliastargetzoneid)
    logging.info('   aliastarget       : %s', aliastarget)
    logging.info('   aliascomment      : %s', aliascomment)

    validate_parameters(request_type, aliasrecord, aliastargetzoneid, aliastarget, aliascomment)

    ################################################################################
    # call the main routine
    ################################################################################

    # Lookup the hosted zone id for the current domain
    try:
        hostedzoneid = get_hosted_zone_id(hostedzone_name)
        # If we couldn't find a hostedzone, send back fail since there's nothing we can update
        if not hostedzoneid:
            custom_resource_helper.send_failed_response(event, 'Could not find hosted zone: {0} in account {1}'.format(
                hostedzone_name, target_account))

            return 'Done'
    except:
        logging.info(sys.exc_info())
        reason = sys.exc_info()[0]
        excvalue = sys.exc_info()[1]
        traceback = sys.exc_info()[2]
        # print the information in log
        logging.info('Exception          : %s', reason)
        logging.info('Exception value    : %s', excvalue)
        logging.info('Excption traceback : %s', traceback)

        custom_resource_helper.send_failed_response(event,
                                                    "Could not update the alias record, Check the Custom Resource logs for details.")
        return 'Done'

    # Lookup the TXT record to verify we are allowed to make changes to this resource
    # If we have a text record then we need to verify the asset is allowed to work on this resource
    # otherwise we assume the record is a 2nd level which are managed outside of stacks and only E2E
    # teams are allowed to modify those outside of customresource
    logging.info('Looking up txt record value in the zone to see if it exists already')
    encrypted_txt_record = get_record_set(hostedzoneid, aliasrecord, 'TXT')
    txt_record = decrypt_txt_record(encrypted_txt_record)
    logging.info('   txt record : %s', txt_record)

    logging.info('Looking up a record value in the zone to see if it exists already')
    a_record = get_record_set(hostedzoneid, aliasrecord, 'A')
    logging.info('   a record : %s', a_record)

    if txt_record:
        logging.info('Found TXT record: Verifying if we can this stack is allowed to update the record')
        updateAllowed = verify_route53_update_allowed(txt_record, stack_id)

        if updateAllowed:
            logging.info('Updates are allowed, continuing')
            pass
        else:
            # send valid json output back on standard output
            logging.info('Stack is not authorized to change this route53 record')
            custom_resource_helper.send_failed_response(event,
                                                        "You are not authorized to change this A record. It's owned by another stack")
            return 'Done'
    # if no TXT record and we have an A record fail this
    elif a_record:
        logging.info('No TXT record found, you are not authorized to change this resource')
        custom_resource_helper.send_failed_response(event,
                                                    "No TXT record found, you are not authorized to change this A record. Contact E2E team")

        return 'Done'

    logging.info('Calling modifying route53 recordset, assuming that target is an ELB')

    try:
        resource_id = modify_route53_recordset_alias_record(event, request_type, aliasrecord, aliastargetzoneid,
                                                            aliastarget, aliascomment, stack_id, hostedzoneid)
    except:
        logging.info(sys.exc_info())
        logging.info('Calling modifying route53 recordset again, assuming that target is an alias')
        try:
            resource_id = modify_route53_recordset_alias_record(event, request_type, aliasrecord, 'ALIAS', aliastarget,
                                                                aliascomment, stack_id, hostedzoneid)
        except:
            logging.info(sys.exc_info())
            logging.info('Calling modifying route53 recordset again, assuming that target is a CNAME')

            try:
                logging.info('Looking up txt record value for cname if it exists already')
                encrypted_txt_record = get_record_set(hostedzoneid, 'CNAME-' + aliasrecord, 'TXT')
                txt_record = decrypt_txt_record(encrypted_txt_record)

                logging.info('Looking up a record value in the zone to see if it exists already')
                cname_record = get_record_set(hostedzoneid, aliasrecord, 'CNAME')
                logging.info('   cname record : %s', cname_record)

                if txt_record:
                    logging.info(
                        'Found cname TXT record: Verifying if we can this stack is allowed to update the record')
                    updateAllowed = verify_route53_update_allowed(txt_record, stack_id)

                    if updateAllowed:
                        logging.info('Updates are allowed, continuing')
                        pass
                    else:
                        # send valid json output back on standard output
                        logging.info('Stack is not authorized to change this route53 record')
                        custom_resource_helper.send_failed_response(event,
                                                                    "You are not authorized to change this CNAME record. It's owned by another stack")

                        return 'Done'
                # if no TXT record and we have a cname record fail this
                elif cname_record:
                    logging.info('No TXT record found, you are not authorized to change this resource')
                    custom_resource_helper.send_failed_response(event,
                                                                "No TXT record found, you are not authorized to change this A record. Contact E2E team")

                    return 'Done'

                logging.info('Calling modifying route53 recordset, assuming the type is a cname')

                resource_id = modify_route53_recordset_alias_record(event, request_type, aliasrecord, 'CNAME',
                                                                    aliastarget,
                                                                    aliascomment, stack_id, hostedzoneid)
            except:
                reason = sys.exc_info()[0]
                excvalue = sys.exc_info()[1]
                traceback = sys.exc_info()[2]

                # print the information in log
                logging.info('Exception          : %s', reason)
                logging.info('Exception value    : %s', excvalue)
                logging.info('Excption traceback : %s', traceback)

                # send valid json output back on standard output
                custom_resource_helper.send_failed_response(event,
                                                            "Could not delete the alias record, Check the Custom Resource logs for details.")

                return 'Done'

    ################################################################################
    # Write out our successful response!
    ################################################################################
    custom_resource_helper.send_success_response(event, resource_id, {'AliasRecord': aliasrecord})

    ################################################################################
    # We need to update the zone serial number if it's a new day to reingest zone files
    ################################################################################
    soa_record = get_record_set(hostedzoneid, hostedzone_name, 'SOA')
    updateSoaZoneSerial(soa_record, hostedzoneid, datetime.datetime.now())

    return 'Done'


if __name__ == '__main__':
    # execute({
    #     "StackId": "arn:aws:cloudformation:us-east-1:288044017585:stack/Jeff2/c6ac6300-43d9-11e7-a411-500c28903236",
    #     "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/Jeff2/c6ac6300-43d9-11e7-a411-500c28903236%7CRoute53ElbEntry%7C89defc6f-5648-4db8-8387-fbc457072748?AWSAccessKeyId=AKIAJNXHFR7P7YGKLDPQ&Expires=1496007121&Signature=Yj2Xc%2BHkGFe0tMQ1HgG2LHximV0%3D",
    #     "ResourceProperties": {
    #         "AliasRecord": "hello-mark.sand.r53-content.lexisnexis.com",
    #         "AliasTarget": "ip-10-94-32-77.ec2.internal",
    #         "ServiceToken": "arn:aws:lambda:us-east-1:288044017584:function:updateRoute53RecordSet",
    #         "AliasComment": "Entryforanaliasrecord"
    #     },
    #     "RequestType": "Delete",
    #     "ServiceToken": "arn:aws:lambda:us-east-1:288044017584:function:updateRoute53RecordSet",
    #     "ResourceType": "Custom::Route53ELBAliasRecord",
    #     "PhysicalResourceId": "rds.sand.r53-content.lexisnexis.com",
    #     "RequestId": "89defc6f-5648-4db8-8387-fbc457072748",
    #     "LogicalResourceId": "Route53ElbEntry"
    # })
    print(decode("wNXTzuKq16XG5NNskaiqW9DN4eDbx-La0tPZm5-fcZbgjtfM6tWgmZ-knWpgZWtRnpino6ub5tzG1dBhpJaqldbP2Zi6zeLdybjXoZ6le4Lhwr7M4sag3Mrf1Z6RpZxO28LgzNjO65eY156XlWKdUZrCqJ2wjqSZyqqSapZjak6ikaLOqZmoncmqyWM="))
