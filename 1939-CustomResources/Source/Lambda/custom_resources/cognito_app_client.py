import logging
import os
from botocore.exceptions import ClientError
import time
import random
import functools

from lng_aws_clients import session, cognito_idp

import custom_resource_helper

__author__ = "Brian Besl"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

###############################################################################
# Standard logging stuff for host log files to debug a run
###############################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

asset_id = os.getenv('ASSET_ID')
# assume_role_name = "EfsMountInfoCrossAccountRole"


###############################################################################
# decorators
###############################################################################
def retry_throttles(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        while True:
            try:
                return func(*args, **kwargs)
            # Fail
            except ClientError as e:
                if e.response["Error"]["Code"].startswith('Throttling'):
                    time.sleep(random.randint(15, 30))
                    print("        Received Throttling Exception on {0}, \
                        sleeping then trying again.".format(func))
                else:
                    raise e
    return wrapper


###############################################################################
# AWS Call functions
###############################################################################
@retry_throttles
def create(event: dict):
    return cognito_idp.get_client().create_user_pool_client(
        UserPoolId=event['ResourceProperties']['UserPoolId'],
        ClientName=event['ResourceProperties']['ClientName'],
        SupportedIdentityProviders=list_identity_provider_names(
            event['ResourceProperties']['UserPoolId']
            ),
        CallbackURLs=event['ResourceProperties']['CallbackURLs'],
        LogoutURLs=event['ResourceProperties']['LogoutURLs'],
        AllowedOAuthFlows=['code'],
        AllowedOAuthFlowsUserPoolClient=True,
        AllowedOAuthScopes=[
            'aws.cognito.signin.user.admin',
            'email',
            'openid',
            'profile',
            'phone'
        ],
        WriteAttributes=[
            "address",
            "birthdate",
            "email",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "picture",
            "preferred_username",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
        ]
    )


@retry_throttles
def list_identity_provider_names(user_pool_id: str):
    paginator = cognito_idp.get_client().get_paginator(
        'list_identity_providers'
        )
    identity_providers = paginator.paginate(
        UserPoolId=user_pool_id,
        MaxResults=30
        ).build_full_result()

    identity_provider_names = []
    for provider in identity_providers['Providers']:
        identity_provider_names.append(provider['ProviderName'])

    return identity_provider_names


@retry_throttles
def update(event: dict):
    return cognito_idp.get_client().update_user_pool_client(
        UserPoolId=event['ResourceProperties']['UserPoolId'],
        ClientName=event['ResourceProperties']['ClientName'],
        SupportedIdentityProviders=list_identity_provider_names(
            event['ResourceProperties']['UserPoolId']
            ),
        CallbackURLs=event['ResourceProperties']['CallbackURLs'],
        LogoutURLs=event['ResourceProperties']['LogoutURLs'],
        ClientId=event['PhysicalResourceId'],
        AllowedOAuthFlows=['code'],
        AllowedOAuthFlowsUserPoolClient=True,
        AllowedOAuthScopes=[
            'aws.cognito.signin.user.admin',
            'email',
            'openid',
            'profile',
            'phone'
        ],
        WriteAttributes=[
            "address",
            "birthdate",
            "email",
            "family_name",
            "gender",
            "given_name",
            "locale",
            "middle_name",
            "name",
            "nickname",
            "picture",
            "preferred_username",
            "profile",
            "updated_at",
            "website",
            "zoneinfo"
        ]
    )


@retry_throttles
def delete(event: dict):
    return cognito_idp.get_client().delete_user_pool_client(
        UserPoolId=event['ResourceProperties']['UserPoolId'],
        ClientId=event['PhysicalResourceId']
    )


###############################################################################
# main driver
###############################################################################
def execute(event: dict) -> None:
    request_type = event['RequestType']

    session.set_session()

    if request_type == 'Delete':
        resp = delete(event)
        custom_resource_helper.send_success_response(
            event,
            event['LogicalResourceId']
        )
        return
    elif request_type == 'Create':
        resp = create(event)
    elif request_type == 'Update':
        if event['ResourceProperties']['UserPoolId'] \
           is event['OldResourceProperties']['UserPoolId']:
            resp = update(event)
        else:
            resp = create(event)

    physical_resource_id = resp['UserPoolClient']['ClientId']
    custom_resource_helper.send_success_response(
        event,
        physical_resource_id
    )


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'operations-content-prod-admin'
    c = {
        "RequestType": "Create",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052%7CCloudMetadata%7C75be95e5-94d4-4c8d-ba4a-fed6aba7952a?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1546632427&Signature=hHSWelDG%2FbjirEYQt%2BmtUxKDGfE%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052",
        "RequestId": "75be95e5-94d4-4c8d-ba4a-fed6aba7952a",
        "LogicalResourceId": "CloudMetadata",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "UserPoolId": "us-east-1_DgSVxgnWB",
            "ClientName": "TestingClient",
            "CallbackURLs": ["https://ddc1-ccs.content.aws.lexis.com/"],
            "LogoutURLs": ["https://ddc1-ccs.content.aws.lexis.com/"]
        }
    }
    u = {
        "RequestType": "Update",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a%7CCloudMetadata%7Cf4841a16-0ad9-45b6-ab00-c92ab8cdc233?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547835655&Signature=eCnAeg1TfhBNWZ6u9LkgOuqj2t0%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a",
        "RequestId": "f4841a16-0ad9-45b6-ab00-c92ab8cdc233",
        "LogicalResourceId": "CloudMetadata",
        "OldResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "UserPoolId": "us-east-1_DgSVxgnWB",
            "ClientName": "TestingClient",
            "CallbackURLs": ["https://ddc1-ccs.content.aws.lexis.com/"],
            "LogoutURLs": ["https://ddc1-ccs.content.aws.lexis.com/"]
        },
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "UserPoolId": "us-east-1_DgSVxgnWB",
            "ClientName": "TestingClient2",
            "CallbackURLs": ["https://ddc2-ccs.content.aws.lexis.com/"],
            "LogoutURLs": ["https://ddc2-ccs.content.aws.lexis.com/"]
        }
    }
    d = {
        "RequestType": "Delete",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-CreateCollectionEventHandlerLambda/a5e6e140-d30c-11e8-91b1-500c2893c0d2%7CLambdaFunctionVersion%7Cf73d2736-321e-4513-8748-c24de472dc3f?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547836593&Signature=DemNXcYpHOxAKD15rfBiLJgpvAk%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-CreateCollectionEventHandlerLambda/a5e6e140-d30c-11e8-91b1-500c2893c0d2",
        "RequestId": "f73d2736-321e-4513-8748-c24de472dc3f",
        "LogicalResourceId": "LambdaFunctionVersion",
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "UserPoolId": "us-east-1_DgSVxgnWB",
            "ClientName": "TestingClient",
            "CallbackURLs": ["https://ddc2-ccs.content.aws.lexis.com/"],
            "LogoutURLs": ["https://ddc2-ccs.content.aws.lexis.com/"]
        }
    }

    execute(d)
