import logging
import os

from lng_aws_clients import session, rds, sts

from botocore.exceptions import ClientError
import custom_resource_helper

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

################################################################################
# Standard logging stuff for host log files to debug a run
################################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

asset_id = os.getenv('ASSET_ID')
assume_role_name = "RdsClusterRoleCrossAccountRole"


################################################################################
# main driver
################################################################################
#TODO: Rework this entire thing, this was a quick POC to get a group going, lots of assumptions and quirky code
def execute(event: dict) -> None:
    request_type = event['RequestType']
    session.set_session()


    db_cluster_id = event['ResourceProperties'].get('DBClusterIdentifier')
    role_arns = event['ResourceProperties'].get('RoleArns')

    if request_type != 'Delete':
        if not db_cluster_id or not role_arns:
            custom_resource_helper.send_failed_response(event, 'You must pass required parameters, DBClusterIdentifier and RoleArns')
            return

    stack_id = event['StackId']
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    region = custom_resource_helper.get_region_from_stack_id(stack_id)


    assume_resp = sts.get_client(local=True).assume_role(
        RoleArn='arn:aws:iam::{0}:role/AssetApplication_{1}/{2}'.format(account_id, asset_id, assume_role_name),
        RoleSessionName='RdsClusterToRoleMap')

    session.set_assumed_session(region_name=region,
                                aws_access_key_id=assume_resp['Credentials']['AccessKeyId'],
                                aws_secret_access_key=assume_resp['Credentials']['SecretAccessKey'],
                                aws_session_token=assume_resp['Credentials']['SessionToken'])

    # Always return success on a Delete event because we don't have anything to do
    if request_type == 'Delete':
        try:
            # If we have a fail becuase the role already exists, we don't want to delete on rollback
            if event['PhysicalResourceId'] != None:
                db_cluster_id = event['ResourceProperties'].get('DBClusterIdentifier')
                resp = rds.get_client().describe_db_clusters(DBClusterIdentifier=db_cluster_id)['DBClusters']
                for cluster in resp:
                    for role in cluster.get('AssociatedRoles', []):
                        rds.get_client().remove_role_from_db_cluster(DBClusterIdentifier=db_cluster_id, RoleArn=role['RoleArn'])
        except Exception as e:
            logging.error(e)
        finally:
            custom_resource_helper.send_success_response(event, event['LogicalResourceId'])
        return

    # Updates we have to find the intersection of the differences
    #if request_type == 'Update':
    resp = []
    try:
        resp = rds.get_client().describe_db_clusters(DBClusterIdentifier=db_cluster_id)['DBClusters']
    except ClientError as e:
        logging.warning(e.response)
        logging.warning(e.response["Error"]["Code"])
        if e.response["Error"]["Code"] not in ['InvalidParameterValue']:
            raise e

    if len(resp) != 1:
        custom_resource_helper.send_failed_response(event, 'DBCluster: {} does not exist'.format(db_cluster_id))
        return

    resource_id = resp[0]['DBClusterIdentifier']

    for role in role_arns:
        try:
            add_resp = rds.get_client().add_role_to_db_cluster(
                DBClusterIdentifier=resource_id,
                RoleArn=role
            )
            logging.debug(add_resp)
        except ClientError as e:
            logging.warning(e.response)
            logging.warning(e.response["Error"]["Code"])
            if e.response["Error"]["Code"] == 'DBClusterRoleAlreadyExists':
                custom_resource_helper.send_failed_response(event, 'Role {} already exists for the DBCluster'.format(role))
                return
            else:
                raise e


    logger.info(resource_id)
    custom_resource_helper.send_success_response(event, resource_id)


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'c-sand'
    execute({
        'RequestType': 'Create',
        'ServiceToken': 'arn:aws:sns:us-east-1:807841377931:CustomResourceGateway',
        'ResponseURL': 'https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/mark-test-2/404a3e40-110b-11e8-bbc8-500c285ebefd%7CLambdaFunctionVersion%7Cea6cbdba-da55-470e-a652-0dc9d92533ec?AWSAccessKeyId=AKIAJNXHFR7P7YGKLDPQ&Expires=1518567312&Signature=KhcxX3U%2B4hxyAaA%2B8inl0m8QN%2BY%3D',
        'StackId': 'arn:aws:cloudformation:us-east-1:288044017584:stack/mark-test-2/404a3e40-110b-11e8-bbc8-500c285ebefd',
        'RequestId': 'ea6cbdba-da55-470e-a652-0dc9d92533ec',
        'LogicalResourceId': 'LambdaFunctionVersion',
        'ResourceType': 'Custom::RoleToDbCluster',
        'ResourceProperties': {
            'ServiceToken': 'arn:aws:lambda:us-east-1:807841377931:function:LambdaVersionBuilder:STABLE',
            'DBClusterIdentifier': '1',
            'RoleArns': ['mark-test-2-LambdaFunction-SINPWO9V0731']}
    })
