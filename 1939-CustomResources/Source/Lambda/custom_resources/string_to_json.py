import logging
import os
from botocore.exceptions import ClientError
import time
import random
import functools
import hashlib
import json

from lng_aws_clients import session, es

import custom_resource_helper

__author__ = "Doug Heitkamp"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

###############################################################################
# Standard logging stuff for host log files to debug a run
###############################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

asset_id = os.getenv('ASSET_ID')


def transform(event: dict):
    return json.loads(event['ResourceProperties']['Input'])


###############################################################################
# main driver
###############################################################################
def execute(event: dict) -> None:
    # Callers will need to call { "Fn::GetAtt": ["Custom::StringToJson", "Output"] } to get the transformed result.
    data = {'Output': transform(event)}
    # The resource id is the sha1 hash so that changes to the input reflect a change to the output,
    # which will then force the resource that uses this data to (re)process.
    resource_id = hashlib.sha1(str.encode(event['ResourceProperties']['Input'])).hexdigest()
    logger.debug("string_to_json.py transformed:\n{0}\ninto:\n{1}\nwith a resource id: {2}".format(
        event['ResourceProperties']['Input'], data, resource_id))
    custom_resource_helper.send_success_response(event, resource_id, data=data)


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'product-content-sandbox-devopsadmin'
    c = {
        "RequestType": "Create",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052%7CCloudMetadata%7C75be95e5-94d4-4c8d-ba4a-fed6aba7952a?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1546632427&Signature=hHSWelDG%2FbjirEYQt%2BmtUxKDGfE%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052",
        "RequestId": "75be95e5-94d4-4c8d-ba4a-fed6aba7952a",
        "LogicalResourceId": "CloudMetadata",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "Input": "{\"foo\": \"bar\"}",
        }
    }
    u = {
        "RequestType": "Update",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a%7CCloudMetadata%7Cf4841a16-0ad9-45b6-ab00-c92ab8cdc233?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547835655&Signature=eCnAeg1TfhBNWZ6u9LkgOuqj2t0%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a",
        "RequestId": "f4841a16-0ad9-45b6-ab00-c92ab8cdc233",
        "LogicalResourceId": "CloudMetadata",
        "OldResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "Input": "{\"foo\": \"bar\"}",
        },
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "Input": "{\n  \"test\": \n    {\n      \"nested\": \"bar\"\n    }\n}",
        }
    }
    d = {
        "RequestType": "Delete",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-CreateCollectionEventHandlerLambda/a5e6e140-d30c-11e8-91b1-500c2893c0d2%7CLambdaFunctionVersion%7Cf73d2736-321e-4513-8748-c24de472dc3f?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547836593&Signature=DemNXcYpHOxAKD15rfBiLJgpvAk%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-CreateCollectionEventHandlerLambda/a5e6e140-d30c-11e8-91b1-500c2893c0d2",
        "RequestId": "f73d2736-321e-4513-8748-c24de472dc3f",
        "LogicalResourceId": "LambdaFunctionVersion",
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "Input": "{\"foo\": \"bar\"}",
        }
    }

    execute(u)
