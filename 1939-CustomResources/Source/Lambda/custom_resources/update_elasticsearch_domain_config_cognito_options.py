import logging
import os
from botocore.exceptions import ClientError
import time
import random
import functools

from lng_aws_clients import session, es, sts

import custom_resource_helper

__author__ = "Doug Heitkamp"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

###############################################################################
# Standard logging stuff for host log files to debug a run
###############################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

asset_id = os.getenv('ASSET_ID')
assume_role_name = "ElasticsearchDomainConfigAssumeRole"


###############################################################################
# decorators
###############################################################################
def retry_throttles(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        while True:
            try:
                return func(*args, **kwargs)
            # Fail
            except ClientError as e:
                if e.response["Error"]["Code"].startswith('Throttling'):
                    time.sleep(random.randint(15, 30))
                    print("        Received Throttling Exception on {0}, \
                        sleeping then trying again.".format(func))
                else:
                    raise e
    return wrapper


###############################################################################
# AWS Call functions
###############################################################################

@retry_throttles
def update(event: dict):
    es.get_client().update_elasticsearch_domain_config(
        DomainName=event['ResourceProperties']['DomainName'],
        CognitoOptions={
            'Enabled': event['ResourceProperties']['Enabled'] == 'True',
            'UserPoolId': event['ResourceProperties']['UserPoolId'],
            'IdentityPoolId': event['ResourceProperties']['IdentityPoolId'],
            'RoleArn': event['ResourceProperties']['RoleArn']
        }
    )


###############################################################################
# main driver
###############################################################################
def execute(event: dict) -> None:
    request_type = event['RequestType']

    if 'DomainName' not in event['ResourceProperties']:
        custom_resource_helper.send_failed_response(event, 'You must pass required parameters, DomainName')
        return

    domain_name = event['ResourceProperties']['DomainName']

    if 'Enabled' not in event['ResourceProperties']:
        custom_resource_helper.send_failed_response(event, 'You must pass required parameters, Enabled')
        return

    stack_id = event['StackId']
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    region = custom_resource_helper.get_region_from_stack_id(stack_id)

    session.set_session()
    assume_resp = sts.get_client(local=True).assume_role(
        RoleArn='arn:aws:iam::{0}:role/AssetApplication_{1}/{2}'.format(account_id, asset_id, assume_role_name),
        RoleSessionName='ElasticsearchDomainConfig')

    session.set_assumed_session(region_name=region,
                                aws_access_key_id=assume_resp['Credentials']['AccessKeyId'],
                                aws_secret_access_key=assume_resp['Credentials']['SecretAccessKey'],
                                aws_session_token=assume_resp['Credentials']['SessionToken'])

    if request_type == 'Create' or request_type == 'Update':
        update(event)

    custom_resource_helper.send_success_response(event, domain_name)


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'product-content-sandbox-devopsadmin'
    c = {
        "RequestType": "Create",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052%7CCloudMetadata%7C75be95e5-94d4-4c8d-ba4a-fed6aba7952a?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1546632427&Signature=hHSWelDG%2FbjirEYQt%2BmtUxKDGfE%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052",
        "RequestId": "75be95e5-94d4-4c8d-ba4a-fed6aba7952a",
        "LogicalResourceId": "CloudMetadata",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "DomainName": "discovery-dev-cognito",
            "Enabled": "True",
            "UserPoolId": "us-east-1_ZLHaj2WF1",
            "IdentityPoolId": "us-east-1:23d65759-5acb-4f05-9f0e-3a4d038f26de",
            "RoleArn": "arn:aws:iam::069379813652:role/AssetApplication_1947/discovery-es-cognito-ESCognitoDefaultRole-A6BFHPSWR6E"
        }
    }
    u = {
        "RequestType": "Update",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a%7CCloudMetadata%7Cf4841a16-0ad9-45b6-ab00-c92ab8cdc233?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547835655&Signature=eCnAeg1TfhBNWZ6u9LkgOuqj2t0%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a",
        "RequestId": "f4841a16-0ad9-45b6-ab00-c92ab8cdc233",
        "LogicalResourceId": "CloudMetadata",
        "OldResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "DomainName": "discovery-dev-cognito",
            "Enabled": "True",
            "UserPoolId": "us-east-1_ZLHaj2WF1",
            "IdentityPoolId": "us-east-1:23d65759-5acb-4f05-9f0e-3a4d038f26de",
            "RoleArn": "arn:aws:iam::069379813652:role/AssetApplication_1947/discovery-es-cognito-ESCognitoDefaultRole-A6BFHPSWR6E"
        },
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "DomainName": "discovery-dev-cognito",
            "Enabled": "True",
            "UserPoolId": "us-east-1_ZLHaj2WF1",
            "IdentityPoolId": "us-east-1:23d65759-5acb-4f05-9f0e-3a4d038f26de",
            "RoleArn": "arn:aws:iam::069379813652:role/AssetApplication_1947/discovery-es-cognito-ESCognitoDefaultRole-A6BFHPSWR6E"
        }
    }
    d = {
        "RequestType": "Delete",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-CreateCollectionEventHandlerLambda/a5e6e140-d30c-11e8-91b1-500c2893c0d2%7CLambdaFunctionVersion%7Cf73d2736-321e-4513-8748-c24de472dc3f?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547836593&Signature=DemNXcYpHOxAKD15rfBiLJgpvAk%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-CreateCollectionEventHandlerLambda/a5e6e140-d30c-11e8-91b1-500c2893c0d2",
        "RequestId": "f73d2736-321e-4513-8748-c24de472dc3f",
        "LogicalResourceId": "LambdaFunctionVersion",
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "Domain": "discovery-dev-cognito",
            "Enabled": "True",
            "UserPoolId": "us-east-1_ZLHaj2WF1",
            "IdentityPoolId": "us-east-1:23d65759-5acb-4f05-9f0e-3a4d038f26de",
            "RoleArn": "arn:aws:iam::069379813652:role/AssetApplication_1947/discovery-es-cognito-ESCognitoDefaultRole-A6BFHPSWR6E"
        }
    }
    f = {
        "RequestType": "Create",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052%7CCloudMetadata%7C75be95e5-94d4-4c8d-ba4a-fed6aba7952a?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1546632427&Signature=hHSWelDG%2FbjirEYQt%2BmtUxKDGfE%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052",
        "RequestId": "75be95e5-94d4-4c8d-ba4a-fed6aba7952a",
        "LogicalResourceId": "CloudMetadata",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "DomainName": "discovery-dev-cognito",
            "UserPoolId": "us-east-1_ZLHaj2WF1",
            "IdentityPoolId": "us-east-1:23d65759-5acb-4f05-9f0e-3a4d038f26de",
            "RoleArn": "arn:aws:iam::069379813652:role/AssetApplication_1947/discovery-es-cognito-ESCognitoDefaultRole-A6BFHPSWR6E"
        }
    }

    execute(c)
