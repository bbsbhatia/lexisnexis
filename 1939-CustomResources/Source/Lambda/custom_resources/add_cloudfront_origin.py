import logging
import os

from lng_aws_clients import session, cloudfront, sts

import custom_resource_helper

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"
################################################################################
# Standard logging stuff for host log files to debug a run
################################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)
asset_id = os.getenv('ASSET_ID')
assume_role_name = "CloudFrontOriginCrossAccountRole"


################################################################################
# main driver
################################################################################
def execute(event: dict) -> None:
    request_type = event['RequestType']

    stack_id = event['StackId']
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    region = custom_resource_helper.get_region_from_stack_id(stack_id)

    # unpack custom resource properties
    distribution_id = event['ResourceProperties'].get('DistributionId')
    domain_name = event['ResourceProperties'].get('DomainName')
    pattern = event['ResourceProperties'].get('PathPattern')
    lambda_function_association_arn = event['ResourceProperties'].get('LambdaFunctionAssociationARN')

    session.set_session()
    assume_resp = sts.get_client(local=True).assume_role(
        RoleArn='arn:aws:iam::{0}:role/AssetApplication_{1}/{2}'.format(account_id, asset_id, assume_role_name),
        RoleSessionName='CloudFrontOrigin')
    session.set_assumed_session(region_name=region,
                                aws_access_key_id=assume_resp['Credentials']['AccessKeyId'],
                                aws_secret_access_key=assume_resp['Credentials']['SecretAccessKey'],
                                aws_session_token=assume_resp['Credentials']['SessionToken'])

    cloudfront_client = cloudfront.get_client()

    if request_type == 'Delete':
        logger.info('start delete ..')
        try:
            if domain_name and distribution_id and pattern:
                # Logic for delete origin goes here
                response = cloudfront_client.get_distribution_config(Id=distribution_id)
                origin_id = get_origin_id(domain_name)
                remove_origin(response, origin_id, pattern)
                resp = cloudfront_client.update_distribution(DistributionConfig=response['DistributionConfig'],
                                                             Id=distribution_id,
                                                             IfMatch=response['ETag'])
                logger.info('client.update_distribution response :{}'.format(resp))
                logger.info('distribution config after deleting Origin : {}'.format(origin_id))
                logger.info(response)
        except Exception as e:
            logger.info('Delete operation complete with the following info : {}'.format(e))
        finally:
            custom_resource_helper.send_success_response(event, event['LogicalResourceId'])
        return

    if not distribution_id or not domain_name or not pattern or not lambda_function_association_arn:
        custom_resource_helper.send_failed_response(event,
                                                    'You must pass required parameters, '
                                                    'DistributionId,DomainName,PathPattern '
                                                    'and LambdaFunctionAssociationARN')
        return

    response = cloudfront_client.get_distribution_config(Id=distribution_id)

    if request_type == 'Create':
        logger.info('start creating .. ')
        add_new_origin(response, domain_name, pattern, lambda_function_association_arn)
        resp = cloudfront_client.update_distribution(DistributionConfig=response['DistributionConfig'],
                                                     Id=distribution_id,
                                                     IfMatch=response['ETag'])
        logger.info('client.update_distribution response :{}'.format(resp))
        origin_id = get_origin_id(domain_name)
        resource_id = 'distribution_id:{}-Origin_id:{}'.format(distribution_id, origin_id)
        logger.info(resource_id)
        custom_resource_helper.send_success_response(event, resource_id)
        return

    if request_type == 'Update':
        logger.info('start updating .. ')
        # Check if there is value change
        if event['OldResourceProperties'].get('DistributionId') != distribution_id \
                or event['OldResourceProperties'].get('DomainName') != domain_name \
                or event['OldResourceProperties'].get('PathPattern') != pattern:
            # Do update only if there is value change or change in service token arn
            old_origin_id = get_origin_id(event['OldResourceProperties'].get('DomainName'))
            old_pattern = get_origin_id(event['OldResourceProperties'].get('PathPattern'))
            remove_origin(response, old_origin_id, old_pattern)
            add_new_origin(response, domain_name, pattern, lambda_function_association_arn)
            resp = cloudfront_client.update_distribution(DistributionConfig=response['DistributionConfig'],
                                                         Id=distribution_id,
                                                         IfMatch=response['ETag'])
            logger.info('client.update_distribution response :{}'.format(resp))
        origin_id = get_origin_id(domain_name)
        resource_id = 'distribution_id:{}-Origin_id:{}'.format(distribution_id, origin_id)
        custom_resource_helper.send_success_response(event, resource_id)
    return


def add_new_origin(distribution_config: dict, domain_name: str, pattern: str,
                   lambda_function_association_arn: str) -> None:
    logger.info('adding origin .. ')
    logger.info('domain name : {} , pattern :{}'.format(domain_name, pattern))
    logger.info(distribution_config)
    # Check if item is already exist then raise an error
    origin_id = get_origin_id(domain_name)
    for item in distribution_config['DistributionConfig']['Origins']['Items']:
        if item['Id'] == origin_id:
            raise Exception('domain name: {} already in use'.format(domain_name))
    origin_id = get_origin_id(domain_name)
    origin_item = {
        "Id": origin_id,
        "DomainName": domain_name,
        "OriginPath": "",
        "CustomHeaders": {
            "Quantity": 0
        },
        "CustomOriginConfig": {
            "HTTPPort": 80,
            "HTTPSPort": 443,
            "OriginProtocolPolicy": "https-only",
            "OriginSslProtocols": {
                "Quantity": 3,
                "Items": [
                    "TLSv1",
                    "TLSv1.1",
                    "TLSv1.2"
                ]
            },
            "OriginReadTimeout": 30,
            "OriginKeepaliveTimeout": 5
        }
    }
    distribution_config['DistributionConfig']['Origins']['Items'].append(origin_item)
    distribution_config['DistributionConfig']['Origins']['Quantity'] += 1
    logger.info('distribution config after adding new Origin')
    logger.info(distribution_config)
    add_pattern_path(distribution_config, pattern, origin_id, lambda_function_association_arn)


def add_pattern_path(distribution_config: dict, pattern: str, target_origin_id: str,
                     lambda_function_association_arn: str) -> None:
    logger.info('adding pattern ..')
    logger.info('pattern : {}'.format(pattern))
    logger.info('origin id : {}'.format(target_origin_id))
    logger.info('lambda function association ARN : {}'.format(lambda_function_association_arn))
    logger.info(distribution_config)
    new_cache_item = {
        "PathPattern": "{}/*".format(pattern),
        "TargetOriginId": target_origin_id,
        "ForwardedValues": {
            "QueryString": True,
            "Cookies": {
                "Forward": "none"
            },
            "Headers": {
                "Quantity": 0
            },
            "QueryStringCacheKeys": {
                "Quantity": 0
            }
        },
        "TrustedSigners": {
            "Enabled": False,
            "Quantity": 0
        },
        "ViewerProtocolPolicy": "redirect-to-https",
        "MinTTL": 0,
        "AllowedMethods": {
            "Quantity": 7,
            "Items": [
                "HEAD",
                "GET",
                "OPTIONS",
                "PUT",
                "POST",
                "PATCH",
                "DELETE"
            ],
            "CachedMethods": {
                "Quantity": 2,
                "Items": [
                    "HEAD",
                    "GET"
                ]
            }
        },
        "SmoothStreaming": False,
        "DefaultTTL": 0,
        "MaxTTL": 0,
        "Compress": False,
        "LambdaFunctionAssociations": {
            "Quantity": 1,
            "Items": [
                {
                    "EventType": "origin-request",
                    "LambdaFunctionARN": lambda_function_association_arn
                }
            ]
        },
        "FieldLevelEncryptionId": ""
    }
    if distribution_config['DistributionConfig']['CacheBehaviors']['Quantity'] == 0:
        distribution_config['DistributionConfig']['CacheBehaviors']['Items'] = [new_cache_item]
    else:
        distribution_config['DistributionConfig']['CacheBehaviors']['Items'].append(new_cache_item)
    distribution_config['DistributionConfig']['CacheBehaviors']['Quantity'] += 1
    logger.info('distribution config after adding pattern')
    logger.info(distribution_config)


def remove_origin(distribution_config: dict, origin_id: str, pattern) -> None:
    logger.info('remove origin distribution config input with origin id :{}'.format(origin_id))
    logger.info(distribution_config)
    # remove from caching section
    for c_idx, c_item in enumerate(distribution_config['DistributionConfig']['CacheBehaviors']['Items']):
        if c_item['TargetOriginId'] == origin_id and c_item['PathPattern'] == "{}/*".format(pattern):
            distribution_config['DistributionConfig']['CacheBehaviors']['Items'].pop(c_idx)
            distribution_config['DistributionConfig']['CacheBehaviors']['Quantity'] -= 1
            # remove from Origins
            for idx, item in enumerate(distribution_config['DistributionConfig']['Origins']['Items']):
                if item['Id'] == origin_id:
                    distribution_config['DistributionConfig']['Origins']['Items'].pop(idx)
                    distribution_config['DistributionConfig']['Origins']['Quantity'] -= 1
                    break
            break
    logger.info('remove origin distribution config output')
    logger.info(distribution_config)


def get_origin_id(domain_name: str):
    return domain_name.split('.', 1)[0]


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'c-sand'
    execute({
        "RequestType": "Create",
        "ServiceToken": "arn:aws:lambda:us-east-1:288044017584:function:add-cloudfront-origin",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/nanaax-test1-LambdaAsCustomResource/e1577300-aab4-11e8-8a5a-50d5ca6e6082%7CLambdaFunctionVersion%7C2a7a4d4d-4911-4670-bab1-e4ba0044d982?AWSAccessKeyId=AKIAJEMEJLKI636XPIMQ&Expires=1535462750&Signature=bieUMCXxMwXo6T4y5JzRm9EoQb0%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/nanaax-test1-LambdaAsCustomResource/e1577300-aab4-11e8-8a5a-50d5ca6e6082",
        "RequestId": "2a7a4d4d-4911-4670-bab1-e4ba0044d982",
        "LogicalResourceId": "LambdaFunctionVersion",
        "PhysicalResourceId": "123",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:lambda:us-east-1:288044017584:function:add-cloudfront-origin",
            "PathPattern": "pattern",
            "DomainName": "NoNumber.com",
            "DistributionId": "E336D1F7D9X6N9",
            "LambdaFunctionAssociationARN": "arn:aws:lambda:us-east-1:288044017584:function:nanaamxTestLambdaAtEdge:1"
        }
    })
