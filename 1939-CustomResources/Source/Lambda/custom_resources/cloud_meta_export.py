import functools
import logging
import os
import random
import time
from datetime import datetime

from botocore.exceptions import ClientError
from lng_aws_clients import session, dynamodb

import custom_resource_helper

__author__ = "Brian Besl"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

###############################################################################
# Standard logging stuff for host log files to debug a run
###############################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

asset_id = os.getenv('ASSET_ID')
CLOUD_META_DYNAMO_TABLE = os.getenv('CLOUD_META_DYNAMO_TABLE')


###############################################################################
# decorators
###############################################################################
def retry_throttles(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        while True:
            try:
                return func(*args, **kwargs)
            # Fail
            except ClientError as e:
                if e.response["Error"]["Code"].startswith('Throttling'):
                    time.sleep(random.randint(15, 30))
                    print("        Received Throttling Exception on {0}, \
                        sleeping then trying again.".format(func))
                else:
                    raise e

    return wrapper


###############################################################################
# main driver
###############################################################################
def execute(event: dict) -> None:
    request_type = event['RequestType']
    stack_id = event['StackId']
    properties = event['ResourceProperties']
    timestamp = get_current_timestamp()

    session.set_session()

    if request_type == 'Delete':
        # validate but dont send failures because we may fail a create which results in a delete rollback
        if is_valid_request(event, False):
            remove_stack_variables(properties, stack_id)
        custom_resource_helper.send_success_response(
            event=event,
            resource_id=event['PhysicalResourceId']
        )
        return
    elif request_type == 'Create':
        # If we don't have a valid event just return since we already send the failure back
        if not is_valid_request(event, True):
            return
        insert_stack_variables(properties, stack_id, timestamp)
        custom_resource_helper.send_success_response(
            event=event,
            resource_id=event['RequestId']
        )
    elif request_type == 'Update':
        if not is_valid_request(event, True):
            return

        resource_id = event['PhysicalResourceId']
        # If we have a new resource then change the resource id which will force a delete from CFT to come through and
        # cleanup what we had. We also just do the insert because any previous resource is no longer valid so doing a
        # process_update which does a diff would be incorrect and drop data since the namespace changed but maybe not the
        # varaibles values and keys
        if is_new_resource(properties, event['OldResourceProperties']):
            resource_id = event['RequestId']
            insert_stack_variables(properties, stack_id, timestamp)
        else:
            process_update(properties, event['OldResourceProperties'],stack_id,  timestamp)
        custom_resource_helper.send_success_response(
            event=event,
            resource_id=resource_id
        )


###############################################################################
# AWS Call functions
###############################################################################

def process_update(new_properties:dict, old_properties:dict, stack_id: str, timestamp:str):
    base_record = new_properties.copy()
    insert_records = base_record.pop('Exports')
    delete_records = {}
    new_exports = new_properties['Exports']
    old_exports = old_properties['Exports']
    for key, value in old_exports.items():
        #Check if we have same key in both
        if key in new_exports:
            # If the values match, remove the record from the insert so prevent writes
            if value == new_exports[key]:
                insert_records.pop(key)
        else:
            # key doesn't exist in new update so remove it
            delete_records[key] = value

    if insert_records:
        base_record['Exports'] = insert_records
        logger.info("Update to add the following records: {0}".format(base_record))
        insert_stack_variables(base_record, stack_id, timestamp)

    if delete_records:
        base_record['Exports'] = delete_records
        logger.info("Update to delete the following records: {0}".format(base_record))
        remove_stack_variables(base_record, stack_id)

def is_new_resource(new_properties: dict, old_properties: dict) -> bool:
    # If the AssetID, AssetGroup, or AssetAreaName changed,
    # treat this as a new entry
    if (old_properties['AssetID'] != new_properties['AssetID']) \
            or (old_properties['AssetGroup'] != new_properties['AssetGroup']) \
            or (old_properties['AssetAreaName'] != new_properties['AssetAreaName']):
        return True
    else:
        return False


def is_valid_request(event: dict, send_failure: bool) -> bool:
    properties = event['ResourceProperties']
    for prop in ['AssetID', 'AssetAreaName', 'AssetGroup', 'Exports']:
        if prop not in properties:
            if send_failure:
                custom_resource_helper.send_failed_response(event,
                                                            'You must pass required parameters, AssetID, AssetAreaName, AssetGroup, Exports')
            return False

    exports = properties['Exports']
    if type(exports) != dict:
        if send_failure:
            custom_resource_helper.send_failed_response(event,
                                                        'Exports data type must be a dictionary')
        return False
    for key, val in properties['Exports'].items():
        if type(val) != str:
            if send_failure:
                custom_resource_helper.send_failed_response(event,
                                                            'Invalid value for key: {0} Data types of Exports values must be int/str'.format(
                                                                key))
            return False

    return True


def get_current_timestamp():
    date_obj = datetime.now()
    iso_str = date_obj.isoformat()
    # If micoseconds are 0, isoformat won't return them, but we want consistent format so we add them
    if '.' not in iso_str:
        iso_str = "{0}.000".format(iso_str)

    return "{0}{1}".format(iso_str[:23], "Z")


def insert_stack_variables(resource_properties: dict, stack_id: str, timestamp: str) -> None:
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    asset_group = resource_properties['AssetGroup']
    asset_id = resource_properties['AssetID']
    asset_area_name = resource_properties['AssetAreaName']
    for key, val in resource_properties['Exports'].items():
        resource_dict = {'asset-group-key': '|'.join([account_id, asset_group]),
                         'asset-id': asset_id,
                         'asset-area': asset_area_name.split('/')[0],
                         'key': key,
                         'value': val}

        resource_dict['asset-meta'] = '|'.join([asset_id, asset_area_name, stack_id.split('/')[1], key])
        resource_dict['full_composite_key'] = '|'.join([resource_dict['asset-group-key'], resource_dict['asset-meta']])

        _update_cloudmeta_record(resource_dict, stack_id, timestamp, account_id)


def _update_cloudmeta_record(record: dict, stack_id: str, timestamp: str, account_id: str) -> None:
    try:
        dynamodb.get_client(local=True).update_item(TableName=CLOUD_META_DYNAMO_TABLE,
                                                    Key={'AssetGroup': {"S": record['asset-group-key']},
                                                         'AssetMetadata': {"S": record['asset-meta']}},
                                                    UpdateExpression="SET AssetID=:id, AssetAreaName=:area_name,"
                                                                     "StackId=:stack, VariableKey=:key, VariableValue=:val, DiscoverTimestamp=:timestamp,"
                                                                     "AccountId=:acc_id, Version=:ver",
                                                    ExpressionAttributeValues={
                                                        ":id": {"S": record['asset-id']},
                                                        ":area_name": {"S": record['asset-area']},
                                                        ":stack": {"S": stack_id},
                                                        ":key": {"S": record['key']},
                                                        ":val": {"S": str(record['value'])},
                                                        ":timestamp": {
                                                            "S": timestamp},
                                                        ":acc_id": {
                                                            "S": account_id},
                                                        ":ver": {"N": "2"}})
    except Exception as e:
        logger.error(e)


def remove_stack_variables(resource_properties: dict, stack_id: str) -> None:
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    asset_group = resource_properties['AssetGroup']
    asset_id = resource_properties['AssetID']
    asset_area_name = resource_properties['AssetAreaName']
    for key, val in resource_properties['Exports'].items():
        resource_dict = {'asset-group-key': '|'.join([account_id, asset_group])}
        resource_dict['asset-meta'] = '|'.join([asset_id, asset_area_name, stack_id.split('/')[1], key])

        _delete_cloudmeta_record(resource_dict, stack_id)


@retry_throttles
def _delete_cloudmeta_record(record: dict, stack_id: str) -> None:
    try:
        dynamodb.get_client(local=True).delete_item(TableName=CLOUD_META_DYNAMO_TABLE,
                                                    Key={'AssetGroup': {"S": record['asset-group-key']},
                                                         'AssetMetadata': {"S": record['asset-meta']}},
                                                    ConditionExpression='StackId=:stack_id and Version=:ver',
                                                    ExpressionAttributeValues={":stack_id": {"S": stack_id},
                                                                               ":ver": {"N": "2"}})
    except ClientError as e:
        error_code = e.response['Error']['Code']
        if error_code != "ConditionalCheckFailedException":
            raise e


if __name__ == '__main__':
    logging.lastResort.setLevel(logging.DEBUG)
    os.environ['AWS_PROFILE'] = 'operations-content-prod-admin'
    os.environ['CLOUD_META_DYNAMO_TABLE'] = ''
    # Sample Event Dict for a Create Request
    c = {
        "RequestType": "Create",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052%7CCloudMetadata%7C75be95e5-94d4-4c8d-ba4a-fed6aba7952a?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1546632427&Signature=hHSWelDG%2FbjirEYQt%2BmtUxKDGfE%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-man-DataLake-ObjectApiDocumentation/794ad610-104b-11e9-b43b-0a1726208052",
        "RequestId": "75be95e5-94d4-4c8d-ba4a-fed6aba7952a",
        "LogicalResourceId": "CloudMetadata",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "AssetID": "62",
            "AssetAreaName": "Core/Rewriter",
            "AssetGroup": "feature-mas",
            "Exports": {
                "param1": "value1",
                "param2": "value2"
            }
        }
    }
    # Sample Event Dict for Update request
    # Includes
    #   - removal of one export
    #   - addition of a new export
    #   - updated value for existing export
    u1 = {
        "RequestType": "Update",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a%7CCloudMetadata%7Cf4841a16-0ad9-45b6-ab00-c92ab8cdc233?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547835655&Signature=eCnAeg1TfhBNWZ6u9LkgOuqj2t0%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a",
        "RequestId": "f4841a16-0ad9-45b6-ab00-c92ab8cdc233",
        "LogicalResourceId": "CloudMetadata",
        "OldResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "AssetID": "62",
            "AssetAreaName": "Core/Rewriter",
            "AssetGroup": "feature-mas",
            "Exports": {
                "param1": "value1",
                "param2": "value2",
                "param3": "value3"
            }
        },
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "AssetID": "62",
            "AssetAreaName": "Core/Rewriter",
            "AssetGroup": "feature-mas",
            "Exports": {
                "param1": "valueA",
                "param2": "value2",
                "param3": "valueC"
            }
        }
    }
    # Includes
    #   - Change of AssetAreaName
    #   - removal of one export
    #   - addition of a new export
    #   - updated value for existing export
    u2 = {
        "RequestType": "Update",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a%7CCloudMetadata%7Cf4841a16-0ad9-45b6-ab00-c92ab8cdc233?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547835655&Signature=eCnAeg1TfhBNWZ6u9LkgOuqj2t0%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-RemoveCollectionObjects/502f1010-d30c-11e8-9b6b-50a686e4bb4a",
        "RequestId": "f4841a16-0ad9-45b6-ab00-c92ab8cdc233",
        "LogicalResourceId": "CloudMetadata",
        "OldResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "AssetID": "62",
            "AssetAreaName": "Core/Rewriter",
            "AssetGroup": "feature-mas",
            "Exports": {
                "param1": "value1",
                "param2": "value2"
            }
        },
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "AssetID": "62",
            "AssetAreaName": "core/tracking",
            "AssetGroup": "feature-mas",
            "Exports": {
                "param1": "valueA",
                "param3": "valueC"
            }
        }
    }
    # Sample Event Dict for Delete request
    d = {
        "RequestType": "Delete",
        "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
        "ResponseURL": "https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/feature-mas-DataLake-CreateCollectionEventHandlerLambda/a5e6e140-d30c-11e8-91b1-500c2893c0d2%7CLambdaFunctionVersion%7Cf73d2736-321e-4513-8748-c24de472dc3f?AWSAccessKeyId=AKIAI5B7XFKUGEZ7W2RQ&Expires=1547836593&Signature=DemNXcYpHOxAKD15rfBiLJgpvAk%3D",
        "StackId": "arn:aws:cloudformation:us-east-1:288044017584:stack/feature-mas-DataLake-CreateCollectionEventHandlerLambda/a5e6e140-d30c-11e8-91b1-500c2893c0d2",
        "RequestId": "f73d2736-321e-4513-8748-c24de472dc3f",
        "LogicalResourceId": "LambdaFunctionVersion",
        "PhysicalResourceId": "24cc72bgmi8m6evl74hne6iavk",
        "ResourceType": "Custom::DoesNotMatter",
        "ResourceProperties": {
            "ServiceToken": "arn:aws:sns:us-east-1:288044017584:CustomResourceGateway",
            "AssetID": "62",
            "AssetAreaName": "core/tracking",
            "AssetGroup": "feature-mas",
            "Exports": {
                "param1": "valueA",
                "param3": "valueC"
            }
        }
    }

    execute(u1)
