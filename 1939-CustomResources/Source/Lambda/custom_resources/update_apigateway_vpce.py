import logging
import os
from botocore.exceptions import ClientError

from lng_aws_clients import session, apigateway, sts

import custom_resource_helper

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

################################################################################
# Standard logging stuff for host log files to debug a run
################################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

asset_id = os.getenv('ASSET_ID')
assume_role_name = "ApiGatewayVpceAssumeRole"

def build_patch_operation(vpc_endpoints:list, operation:str)->list:
    op_list = []
    for endpoint in vpc_endpoints:
        op_list.append({
            'op': operation,
            'path': '/endpointConfiguration/vpcEndpointIds',
            'value': endpoint
        })
    return op_list
################################################################################
# main driver
################################################################################
def execute(event: dict) -> None:
    request_type = event['RequestType']
    resource_properties = event['ResourceProperties']

    stack_id = event['StackId']
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    region = custom_resource_helper.get_region_from_stack_id(stack_id)
    session.set_session()
    assume_resp = sts.get_client(local=True).assume_role(
        RoleArn='arn:aws:iam::{0}:role/AssetApplication_{1}/{2}'.format(account_id, asset_id, assume_role_name),
        RoleSessionName='ApiGwVpce')

    session.set_assumed_session(region_name=region,
                                aws_access_key_id=assume_resp['Credentials']['AccessKeyId'],
                                aws_secret_access_key=assume_resp['Credentials']['SecretAccessKey'],
                                aws_session_token=assume_resp['Credentials']['SessionToken'])

    # Always return success on a Delete event because we don't have anything to do
    if request_type == 'Delete':
        try:
            apigateway.get_client().update_rest_api(
                restApiId=resource_properties.get('RestApiId'),
                patchOperations=build_patch_operation(resource_properties.get('VpcEndpointIds'), 'remove')
            )
        except ClientError as e:
            error_code = e.response['Error']['Code']
            if error_code != 'NotFoundException':
                logger.error("Unknown Error: {0}".format(e))
        custom_resource_helper.send_success_response(event, event['LogicalResourceId'])
        return

    rest_api = resource_properties.get('RestApiId')
    vpc_endpoints = resource_properties.get('VpcEndpointIds')

    if not rest_api or not vpc_endpoints:
        custom_resource_helper.send_failed_response(event, 'You must pass required parameters: RestApiId, VpcEndpointIds')
        return

    if type(vpc_endpoints) != list:
        custom_resource_helper.send_failed_response(event, 'VpcEndpointIds must be a List not a {0}'.format(type(vpc_endpoints)))
        return

    # NOTE: We are not supporting updates currently because we only have 1 VPCE in the AWS accounts
    # and adding vpce to api is on CFTs roadmap, no need to waste dev time for sortlived feature
    try:
        apigateway.get_client().update_rest_api(
            restApiId=resource_properties.get('RestApiId'),
            patchOperations=build_patch_operation(resource_properties.get('VpcEndpointIds'), 'add')
        )
    except ClientError as e:
        error_code = e.response['Error']['Code']
        if error_code == 'NotFoundException':
            custom_resource_helper.send_failed_response(event, "RestApi: {0} does not exist".format(rest_api))
        else:
            logger.error("Unknown Error: {0}".format(e))
            custom_resource_helper.send_failed_response(event, "Unknown error: {0}".format(e))
    resource_id = event.get('PhysicalResourceId', event['RequestId'])
    logger.info(resource_id)
    custom_resource_helper.send_success_response(event, resource_id)


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'c-sand'
    execute({
        'RequestType': 'Create',
        'ServiceToken': 'arn:aws:sns:us-east-1:807841377931:CustomResourceGateway',
        'ResponseURL': 'https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/mark-test-2/404a3e40-110b-11e8-bbc8-500c285ebefd%7CLambdaFunctionVersion%7Cea6cbdba-da55-470e-a652-0dc9d92533ec?AWSAccessKeyId=AKIAJNXHFR7P7YGKLDPQ&Expires=1518567312&Signature=KhcxX3U%2B4hxyAaA%2B8inl0m8QN%2BY%3D',
        'StackId': 'arn:aws:cloudformation:us-east-1:288044017584:stack/mark-test-2/404a3e40-110b-11e8-bbc8-500c285ebefd',
        'RequestId': 'ea6cbdba-da55-470e-a652-0dc9d92533ec',
        'LogicalResourceId': 'LambdaFunctionVersion',
        'ResourceType': 'Custom::LambdaVersion',
        'ResourceProperties': {
            'ServiceToken': 'arn:aws:lambda:us-east-1:807841377931:function:LambdaVersionBuilder:STABLE',
            'BuildNumber': '1',
            'LambdaName': 'mark-test-2-LambdaFunction-SINPWO9V0731'}
    })
