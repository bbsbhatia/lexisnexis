import json
import logging
import os

from lng_aws_clients import session, sns, sts

import custom_resource_helper

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

################################################################################
# Standard logging stuff for host log files to debug a run
################################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)

asset_id = os.getenv('ASSET_ID')
assume_role_name = 'SnsSubscriptionFilterCrossAccountRole'


################################################################################
# main driver
################################################################################
def execute(event: dict) -> None:
    request_type = event['RequestType']

    # Always return success on a Delete event because we can't delete a filter.
    # Deleting the sns subscription itself will in turn remove the filter for us.
    if request_type == 'Delete':
        custom_resource_helper.send_success_response(event, event['LogicalResourceId'])
        return

    topic_arn = event['ResourceProperties'].get('TopicArn')
    subscription_endpoint = event['ResourceProperties'].get('SubscriptionEndpoint')
    subscription_protocol = event['ResourceProperties'].get('SubscriptionProtocol')
    subscription_filter_policy = event['ResourceProperties'].get('FilterPolicy')

    if not topic_arn or not subscription_endpoint or not subscription_filter_policy or not subscription_protocol:
        custom_resource_helper.send_failed_response(event,
                                                    'You must pass required parameters, TopicArn, SubscriptionEndpoint, '
                                                    'SubscriptionProtocol and FilterPolicy')
        return

    stack_id = event['StackId']
    account_id = custom_resource_helper.get_account_id_from_stack_id(stack_id)
    region = custom_resource_helper.get_region_from_stack_id(stack_id)

    session.set_session()
    assume_resp = sts.get_client(local=True).assume_role(
        RoleArn='arn:aws:iam::{0}:role/AssetApplication_{1}/{2}'.format(account_id, asset_id, assume_role_name),
        RoleSessionName='SnsSubscriptionFilter')

    session.set_assumed_session(region_name=region,
                                aws_access_key_id=assume_resp['Credentials']['AccessKeyId'],
                                aws_secret_access_key=assume_resp['Credentials']['SecretAccessKey'],
                                aws_session_token=assume_resp['Credentials']['SessionToken'])

    paginator = sns.get_client().get_paginator('list_subscriptions_by_topic')

    subscription_arn = None

    for subscription in paginator.paginate(TopicArn=topic_arn).build_full_result()['Subscriptions']:
        if subscription['Owner'] == account_id and subscription['Protocol'] == subscription_protocol and \
                subscription['Endpoint'] == subscription_endpoint:
            subscription_arn = subscription['SubscriptionArn']
            break

    if not subscription_arn:
        custom_resource_helper.send_failed_response(event,
                                                    'Failed to find a subscription based on the passed parameters')
        return

    resp = sns.get_client().set_subscription_attributes(SubscriptionArn=subscription_arn,
                                                        AttributeName='FilterPolicy',
                                                        AttributeValue=json.dumps(subscription_filter_policy))
    resource_id = resp['ResponseMetadata']['RequestId']
    logger.info(resource_id)
    custom_resource_helper.send_success_response(event, resource_id)


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'c-sand'
    execute({'RequestType': 'Create',
             'ServiceToken': 'arn:aws:lambda:us-east-1:807841377931:function:SnsSubscriptionFilter:STABLE',
             'ResponseURL': 'https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/test-subscription2/450b5080-310f-11e8-b8e3-503aca26168d%7CTopicSubscriptionFilter%7C1e5d8afd-c1a6-49cb-8d3c-0af85ae06f88?AWSAccessKeyId=AKIAJNXHFR7P7YGKLDPQ&Expires=1522087947&Signature=7PkSEUd7r2jIFZr%2FDNM6mWAv%2Fjc%3D',
             'StackId': 'arn:aws:cloudformation:us-east-1:288044017584:stack/test-subscription2/450b5080-310f-11e8-b8e3-503aca26168d',
             'RequestId': '1e5d8afd-c1a6-49cb-8d3c-0af85ae06f88',
             'LogicalResourceId': 'TopicSubscriptionFilter',
             'PhysicalResourceId': 'cb5de0f3-ce38-5abb-bb3a-b3c0d32852e3',
             'ResourceType': 'Custom::SnsSubscriptionFilter',
             'ResourceProperties': {
                 'ServiceToken': 'arn:aws:lambda:us-east-1:807841377931:function:SnsSubscriptionFilter:STABLE',
                 'TopicArn': 'arn:aws:sns:us-east-1:288044017584:mark-test',
                 'FilterPolicy': {'event-name': ['Subscription::Delete']},
                 'SubscriptionEndpoint': 'arn:aws:lambda:us-east-1:288044017584:function:test-subscription2-LambdaFunction-SBVQEXMWQNM9',
                 'SubscriptionProtocol': 'lambda'
             }
             })
