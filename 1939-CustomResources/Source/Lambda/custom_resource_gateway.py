import json
import logging
import os

import custom_resource_helper
from custom_resources import lambda_version_builder, \
    glue_endpoint_information, update_route_53, add_cloudfront_origin, cloudfront_origin_group_datalake, \
    apigateway_doc_version_builder, efs_mount_information, cognito_app_client, add_role_db_cluster, xray_group, \
    update_elasticsearch_domain_config_cognito_options, cognito_idp_user_pool_domain, string_to_json, \
    cognito_idp_get_user_pool_client_id, cloud_meta_export, update_apigateway_vpce
from lng_aws_clients import session, lambda_


__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

################################################################################
# Standard logging stuff for host log files to debug a run
################################################################################
logger = logging.getLogger(__name__)
log_level = os.getenv('LOG_LEVEL')
if log_level:
    logger.setLevel(log_level)
else:
    logger.setLevel(logging.DEBUG)


################################################################################
# main driver
################################################################################
def lambda_handler(event, context):
    message = None
    # Everything in the try to handle any possible failure so the stack won't hang
    try:
        logger.info(json.dumps(event))
        message = json.loads(event['Records'][0]['Sns']['Message'])
        logger.info("Message: {0}".format(json.dumps(message)))
        resource_type = message['ResourceType']

        if resource_type == 'Custom::GlueEndpointInformation':
            glue_endpoint_information.execute(message)
        elif resource_type == 'Custom::StringToJson':
            string_to_json.execute(message)
        elif resource_type == 'Custom::UpdateElasticsearchDomainConfigCognitoOptions':
            update_elasticsearch_domain_config_cognito_options.execute(message)
        elif resource_type == 'Custom::CognitoIdpUserPoolDomain':
            cognito_idp_user_pool_domain.execute(message)
        elif resource_type == 'Custom::CognitoIdpGetUserPoolClientId':
            cognito_idp_get_user_pool_client_id.execute(message)
        elif resource_type == 'Custom::LambdaVersion':
            lambda_version_builder.execute(message)
        elif resource_type == 'Custom::CognitoAppClient':
            cognito_app_client.execute(message)
        elif resource_type == 'Custom::Route53ELBAliasRecord':
            update_route_53.execute(message)
        elif resource_type == 'Custom::ApiGatewayDocumentationVersion':
            apigateway_doc_version_builder.execute(message)
        elif resource_type == 'Custom::CloudFrontAddOrigin':
            add_cloudfront_origin.execute(message)
        elif resource_type == 'Custom::CloudFrontOriginGroupDataLake':
            cloudfront_origin_group_datalake.execute(message)
        elif resource_type == 'Custom::EfsMountInformation':
            efs_mount_information.execute(message)
        elif resource_type == 'Custom::AddRoleRdsCluster':
            add_role_db_cluster.execute(message)
        elif resource_type == 'Custom::XrayGroup':
            xray_group.execute(message)
        elif resource_type == 'Custom::ApiGatewayVpce':
            update_apigateway_vpce.execute(message)
        elif resource_type == 'Custom::CloudMetadataExport':
            cloud_meta_export.execute(message)
        elif resource_type == 'Custom::CloudMetadata':
            # For now call out to CloudMeta and let them handle the request
            session.set_session()

            stage = "$LATEST"
            try:
                stage = context.invoked_function_arn.split(":")[7]
            except IndexError:
                pass

            lambda_.get_client().invoke(
                FunctionName='arn:aws:lambda:us-east-1:807841377931:function:cloud_metadata',
                InvocationType='Event',
                LogType='None',
                Payload=json.dumps(message),
                Qualifier=stage
            )
        # Catch all if nothing matches
        else:
            error_msg = 'Unknown resource type: {0}'.format(resource_type)
            logger.error(error_msg)
            custom_resource_helper.send_failed_response(message, error_msg)

    except Exception as e:
        logger.exception(e)
        custom_resource_helper.send_failed_response(message, 'Unknown Error occurred')


if __name__ == '__main__':
    os.environ['AWS_PROFILE'] = 'operations-content-prod-admin'
    lambda_handler({
        "Records": [
            {
                "EventSource": "aws:sns",
                "EventVersion": "1.0",
                "EventSubscriptionArn": "arn:aws:sns:us-east-1:083190835473:CustomResourceGateway:c6a67cbe-4161-4264-a9bd-2873b1c5acba",
                "Sns": {
                    "Type": "Notification",
                    "MessageId": "74f30dcd-bcd7-5fd4-9732-5e214956788d",
                    "TopicArn": "arn:aws:sns:us-east-1:083190835473:CustomResourceGateway",
                    "Subject": "AWS CloudFormation custom resource request",
                    "Message": "{\"RequestType\":\"Create\",\"ServiceToken\":\"arn:aws:sns:us-east-1:083190835473:CustomResourceGateway\",\"ResponseURL\":\"https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A083190835473%3Astack/MikesTestAutoScalingStackUsingCRGateway/8386a550-9fd5-11e8-900e-50fae9826c35%7CRoute53Entry%7Ccbca6220-787c-489b-a2f5-aa39137ba607?AWSAccessKeyId=AKIAJEMEJLKI636XPIMQ&Expires=1534267233&Signature=VtUnHD6TOFLgIphWYTGGEl1UNJQ%3D\",\"StackId\":\"arn:aws:cloudformation:us-east-1:083190835473:stack/MikesTestAutoScalingStackUsingCRGateway/8386a550-9fd5-11e8-900e-50fae9826c35\",\"RequestId\":\"cbca6220-787c-489b-a2f5-aa39137ba607\",\"LogicalResourceId\":\"Route53Entry\",\"ResourceType\":\"Custom::Route53ELBAliasRecord\",\"ResourceProperties\":{\"ServiceToken\":\"arn:aws:sns:us-east-1:083190835473:CustomResourceGateway\",\"AliasTarget\":\"internal-MikesTest-LoadBala-N9DJOUQZWOBV-180643860.us-east-1.elb.amazonaws.com\",\"AliasComment\":\"Route53AliasEntryfordev-Mike-webELB.\",\"AliasRecord\":\"cerberus-test.content.aws.lexis.com.\"}}",
                    "Timestamp": "2018-08-14T15:20:33.330Z",
                    "SignatureVersion": "1",
                    "Signature": "bXgA9xItY42Ao/X5UI5QCIcukQlJeLtn+LUPJRWFDI1R0/geze0l/lSvYPqqVtgtlRgmoAfkhJnsZw/IZVfnkoqMxomBMDuXJm2coH8Ojg2BkSvD85c/JUsD9c2jwRuj7h7VWV7FeQrYC1elKSM2z3kBu2WjgcBQNebRa4HBcBbOm9xmtd2vVIusCXYRC+NlKiZ6jepQ8QHf/US045yUyuBfKoCgc8RKPeM5Kjer0g1crU7wI47dVZ50NWk6B+nYfJH3JYBnPQcH1/CZivqSatK8z3NyghhSnbNa3G9aEhVGwfxMbPcsDoBjyZNCcpDCmqHDfQKxWaAQToCyuSQhAg==",
                    "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-eaea6120e66ea12e88dcd8bcbddca752.pem",
                    "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:083190835473:CustomResourceGateway:c6a67cbe-4161-4264-a9bd-2873b1c5acba",
                    "MessageAttributes": {}
                }
            }
        ]
    }, '')
