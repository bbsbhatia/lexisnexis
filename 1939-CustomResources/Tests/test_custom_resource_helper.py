import unittest
from unittest.mock import patch

from custom_resource_helper import get_account_id_from_stack_id, get_region_from_stack_id, send_failed_response, FatalError

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestLambdaVersionBuilder(unittest.TestCase):

    def test_get_account_id_from_stack_id(self):
        self.assertEqual(
            get_account_id_from_stack_id('arn:aws:lambda:us-east-1:807841377931:function:LambdaVersionBuilder:STABLE'),
            '807841377931')
        self.assertEqual(get_account_id_from_stack_id('arn:aws:iam::807841377931:role/mark-test'), '807841377931')
        self.assertEqual(get_account_id_from_stack_id('arn:aws:s3::807841377931:bucket:testmark-test'), '807841377931')

    def test_get_region_from_stack_id(self):
        self.assertEqual(
            get_region_from_stack_id('arn:aws:lambda:us-east-1:807841377931:function:LambdaVersionBuilder:STABLE'),
            'us-east-1')
        self.assertEqual(get_region_from_stack_id('arn:aws:iam::807841377931:role/mark-test'), '')
        self.assertEqual(get_region_from_stack_id('arn:aws:lambda:us-west-2:807841377931:role/mark-test'), 'us-west-2')

    @patch('requests.put')
    def test_insert_dynamo_record_null(self, requests_put_mock):
        class MockRequestObject(object):
            status_code = 200
            text = 'Handled Mock Error'

        requests_put_mock.return_value = MockRequestObject
        event_json = {'RequestType': 'Create',
                      'ServiceToken': 'arn:aws:lambda:us-east-1:807841377931:function:SnsSubscriptionFilter:STABLE',
                      'ResponseURL': 'https://cloudformation-custom-resource-response-useast1.s3.amazonaws.com/arn%3Aaws%3Acloudformation%3Aus-east-1%3A288044017584%3Astack/test-subscription2/450b5080-310f-11e8-b8e3-503aca26168d%7CTopicSubscriptionFilter%7C1e5d8afd-c1a6-49cb-8d3c-0af85ae06f88?AWSAccessKeyId=AKIAJNXHFR7P7YGKLDPQ&Expires=1522087947&Signature=7PkSEUd7r2jIFZr%2FDNM6mWAv%2Fjc%3D',
                      'StackId': 'arn:aws:cloudformation:us-east-1:288044017584:stack/test-subscription2/450b5080-310f-11e8-b8e3-503aca26168d',
                      'RequestId': '1e5d8afd-c1a6-49cb-8d3c-0af85ae06f88',
                      'LogicalResourceId': 'TopicSubscriptionFilter',
                      'PhysicalResourceId': 'cb5de0f3-ce38-5abb-bb3a-b3c0d32852e3',
                      'ResourceType': 'Custom::SnsSubscriptionFilter',
                      'ResourceProperties': {
                          'ServiceToken': 'arn:aws:lambda:us-east-1:807841377931:function:SnsSubscriptionFilter:STABLE',
                          'TopicArn': 'arn:aws:sns:us-east-1:288044017584:mark-test',
                          'FilterPolicy': {'event-name': ['Subscription::Delete']},
                          'SubscriptionEndpoint': 'arn:aws:lambda:us-east-1:288044017584:function:test-subscription2-LambdaFunction-SBVQEXMWQNM9',
                          'SubscriptionProtocol': 'lambda'
                      }
                      }
        message = 'You must pass required parameters, TopicArn, SubscriptionEndpoint, SubscriptionProtocol and FilterPolicy'
        self.assertIsNone(send_failed_response(event_json, message))

        MockRequestObject.status_code = 400
        self.assertRaises(FatalError, send_failed_response, event_json, message)


if __name__ == '__main__':
    unittest.main()
