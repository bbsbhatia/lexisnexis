import json
import os


# Every path MUST have an options for us given that we enable CORS
# this allows us to safely use .pop per path and any failure is a misconfiguration
def remove_options_methods(swagger_input: dict) -> None:
    for path, val in swagger_input['paths'].items():
        val.pop('options')


def remove_not_implemented_methods(swagger_input: dict, stage: str) -> None:
    methods_to_remove = []
    all_paths = swagger_input['paths']
    for path, val in all_paths.items():
        for method, spec in val.items():
            tags = spec.get('tags', [])
            for tag in tags:
                if tag in ['NotImplemented', 'Deprecated'] and stage != 'LATEST':
                    methods_to_remove.append({'path': path, 'method': method})

    for item in methods_to_remove:
        all_paths[item['path']].pop(item['method'])


def remove_empty_paths(swagger_input: dict) -> None:
    paths_to_remove = []
    all_paths = swagger_input['paths']
    for path, val in all_paths.items():
        if not val:
            paths_to_remove.append(path)

    for path in paths_to_remove:
        all_paths.pop(path)


def update_documentation():
    # DataLake URL
    base_url = os.getenv('BASE_URL', 'datalake.content.aws.lexis.com')
    base_url = base_url.replace('https://', '')
    # The base Swagger-UI shell html page that is deployed through Core/Infrastructure so we can point
    # to the DataLake swagger/json file on page load
    starter_swagger_file = os.getenv('STARTER_HTML', 'starter-swagger.html')
    # Asset Name
    asset_name = os.getenv('ASSET_NAME', 'DataLake')
    # Asset Area Name: Administration, Collection, Object, or Subscription
    asset_area_name = os.getenv('ASSET_AREA_NAME')
    # Swagger file pulled from API Gateway in json format
    swagger_file = os.getenv('SWAGGER_DOC')
    api_name = asset_area_name.lower()
    # If the Asset Area Name is not Administration we add an 's' to the end of it for the base path: collections,
    # objects, subscriptions
    if asset_area_name != 'Administration':
        api_name = '{}s'.format(asset_area_name.lower())
    # The file name of the Swagger-UI shell we'll be updating to point to the Swagger/json file we pulled
    # from API Gateway
    html_file = os.getenv('HTML_FILE')
    input_swagger = ''

    with open(swagger_file, 'r') as read_file:
        for line in read_file:
            input_swagger = input_swagger + line
        swagger_dict = json.loads(input_swagger)

        stage = swagger_dict['servers'][0]['variables']['basePath']['default'].split('/')[-1]
        remove_options_methods(swagger_dict)
        remove_not_implemented_methods(swagger_dict, stage)
        remove_empty_paths(swagger_dict)

        # We replace the API Gateway url with our pretty url
        # We include the asset area name, to point to the correct CloudFront distribution
        # We hard code the stage, so that our users can't hit one stage using a different stage's api
        # Before: 'https://fakeapiid.execute-api.us-east-1.amazonaws.com/{basePath}', After: 'https://datalake-feature-jose.content.aws.lexis.com/collections/v1'
        swagger_dict['servers'][0]['url'] = '{}{}/{}/{}'.format('https://', base_url, api_name, stage)
        # Remove server variables, so that stage input box does not appear in Swagger UI
        swagger_dict['servers'][0].pop('variables')

        output_file = json.dumps(swagger_dict, indent=2)
        # The name of our swagger/json file
        output_file_name = '{}-{}-SwaggerDocumentation.json'.format(asset_name, asset_area_name)
    with open(output_file_name, 'w') as write_file:
        write_file.write(output_file)

    # for now hardcode this to only v1, we need to figure out a better way to support multiple versions
    if stage == 'v1':
        input_html = ''
        with open(starter_swagger_file, 'r') as starter_file:
            for line in starter_file:
                input_html = input_html + line

            # Replace the static string 'json_replacement' so that our newly created file points to our
            # swagger/json file on page load
            input_html = input_html.replace('json_replacement', '{}/{}'.format(stage, output_file_name))

        with open(html_file, 'w') as output_html:
            output_html.write(input_html)

    # Print the name of our newly created Swagger-UI html shell which points to our swagger/json file so
    # Jenkins can copy it to S3
    print(output_file_name)


if __name__ == '__main__':
    local_test = False
    if local_test == True:
        os.environ['BASE_URL'] = 'datalake-feature-mas.content.aws.lexis.com'
        os.environ['ASSET_AREA_NAME'] = 'feature-mas'
        os.environ['SWAGGER_DOC'] = 'C:\\Users\\SeitteMA\\PycharmProjects\\62-DataLake\\swagger-test.json'
    update_documentation()
