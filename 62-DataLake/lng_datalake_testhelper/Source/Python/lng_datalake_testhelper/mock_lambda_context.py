class MockLambdaContext:
    """Mock implementation of AWS Lambda context object.  This class uses class attributes instead of
    instance attributes, so you can simply pass the class itself without having to instantiate an
    instance"""
    aws_request_id = '563r1jug9i'
    invoked_function_arn = 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'
    log_group_name = '/aws/lambda/TestLambda'
    log_stream_name = '2017/10/25/[$LATEST]adfa3b80b0b14b4099a3449fc32b44de'

    @staticmethod
    def get_remaining_time_in_millis():
        """This method is static so that it can be used standalone like
        MockLambdaContext.get_remaining_time_in_millis()"""
        return 300000
