import json
import os
import random
import string


class IOUtils:
    def __init__(self, file_path, sub_data_folder=None):
        keep_walking = True
        test_file_dir = os.path.dirname(file_path)

        module_root_dir = test_file_dir
        second_last_dir_name = None
        while keep_walking:
            cur_dir_name = os.path.basename(module_root_dir)
            # Walk one more up and then stop because Tests should always be one level below the module root
            if cur_dir_name == 'Tests':
                keep_walking = False
            # Failsafe, if we hit the root, stop going and set the root dir back to the test file dir old logic
            elif cur_dir_name == second_last_dir_name:
                keep_walking = False
                module_root_dir = test_file_dir
                continue
            module_root_dir = os.path.dirname(module_root_dir)
            # Go 2 folders back which guarantees we get to the root eventually and don't spin
            second_last_dir_name = os.path.basename(os.path.dirname(module_root_dir))

        self.lambda_path = os.path.join(module_root_dir, 'Source', 'Lambda')
        self.data_path = os.path.join(test_file_dir, 'Data')
        # if we have data sub-foldered in the Data dir, set the path to there.
        if sub_data_folder:
            self.data_path = os.path.join(self.data_path, sub_data_folder)
        self.schema_path = os.path.join(self.lambda_path, 'Schemas')

    def load_data_json(self, file_name):
        full_file_path = os.path.join(self.data_path, file_name)
        return self.__load_json(full_file_path)

    def load_schema_json(self, file_name):
        full_file_path = os.path.join(self.schema_path, file_name)
        return self.__load_json(full_file_path)

    @staticmethod
    def __load_json(path):
        with open(path, "r") as f:
            json_file = json.load(f)
            f.close()
        return json_file

    def load_txt(self, file_name):
        full_file_path = os.path.join(self.data_path, file_name)
        with open(full_file_path, "r") as text_file:
            text_data = text_file.read().strip()
            text_file.close()
        return text_data

    @staticmethod
    def generate_message_of_len(n):
        s = ""
        while True:
            byte_len = len(s.encode())
            if byte_len >= n:
                break
            rnd_char = random.choice(string.ascii_letters)
            s += rnd_char
        return s
