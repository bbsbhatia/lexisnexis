import os
import unittest

from lng_datalake_testhelper.io_utils import IOUtils

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestPass(unittest.TestCase):

    # + io_utils._init__ - can create class and data path exists
    def test_io_utils(self):
        io_util = IOUtils(__file__)
        self.assertTrue(io_util)
        self.assertTrue(os.path.exists(io_util.data_path))

    # + io_utils._init__ - can create class with sub-folder
    def test_io_utils_1(self):
        io_util = IOUtils(__file__, 'IOUtils')
        self.assertTrue(io_util)
        self.assertTrue(os.path.exists(io_util.data_path))

    # + io_utils._init__ - doesn't overrun into loop
    def test_io_utils_1_1(self):
        root_path = os.path.join("Mock", "UnitTest", "Path")
        io_util = IOUtils(os.path.join(root_path, "testile.py"))
        expected = os.path.join(root_path, 'Data')
        self.assertTrue(io_util)
        self.assertEqual(io_util.data_path, expected)

    # + io_utils.load_data_json - can load json data file
    def test_io_utils_2(self):
        io_util = IOUtils(__file__, 'IOUtils')
        valid_file_name = "valid_data_file.json"
        actual_response = io_util.load_data_json(valid_file_name)
        expected_response = {'Test': 'IOUtils Data File'}
        self.assertDictEqual(actual_response, expected_response)

    # + io_utils.load_schema_json - can load json schema file
    def test_io_utils_3(self):
        io_util = IOUtils(__file__, 'IOUtils')
        # override schema path to point to Test/Data path
        io_util.schema_path = io_util.data_path
        valid_file_name = "valid_schema_file.json"
        actual_response = io_util.load_schema_json(valid_file_name)
        expected_response = {'$schema': 'http://json-schema.org/draft-04/schema#', 'title': 'SNS Message',
                             'type': 'object',
                             'properties': {'object-id': {'type': 'string'}, 'collection-id': {'type': 'integer'},
                                            'event-name': {'type': 'string'}, 'timestamp': {'type': 'string'},
                                            'event-id': {'type': 'string'}},
                             'required': ['collection-id', 'event-name']}
        self.assertDictEqual(actual_response, expected_response)

    # + io_utils.load_txt - can load text data file
    def test_io_utils_4(self):
        io_util = IOUtils(__file__, 'IOUtils')
        valid_file_name = "valid_data_file.json"
        actual_response = io_util.load_txt(valid_file_name)
        expected_response = '{\n  "Test": "IOUtils Data File"\n}'
        self.assertEqual(actual_response, expected_response)

    # + io_utils.load_txt - can load text data file
    def test_io_utils_5(self):
        io_util = IOUtils(__file__)
        expected_len = 15
        actual_response = io_util.generate_message_of_len(expected_len)
        actual_len = len(actual_response)
        self.assertEqual(actual_len, expected_len)


if __name__ == '__main__':
    unittest.main()
