import unittest

from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

__author__ = "Brian Besl"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestPass(unittest.TestCase):

    # + mock_lambda_context._init__ - can create class and attributes exist
    def test_mock_lambda_context(self):
        mock_lambda_context = MockLambdaContext()
        self.assertTrue(mock_lambda_context)
        self.assertEqual(mock_lambda_context.aws_request_id, '563r1jug9i')
        self.assertEqual(mock_lambda_context.invoked_function_arn,
                         'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')
        self.assertEqual(mock_lambda_context.log_group_name, '/aws/lambda/TestLambda')
        self.assertEqual(mock_lambda_context.log_stream_name, '2017/10/25/[$LATEST]adfa3b80b0b14b4099a3449fc32b44de')

    # + mock_lambda_context.get_remaining_time_in_millis
    def test_mock_lambda_context_1(self):
        mock_lambda_context = MockLambdaContext()
        self.assertEqual(mock_lambda_context.get_remaining_time_in_millis(), 300000)


if __name__ == '__main__':
    unittest.main()
