import datetime
import logging
import orjson
import os
from inspect import stack, getmodulename

from aws_xray_sdk.core import patch
from lng_aws_clients import stepfunctions
from lng_datalake_commons import publish_sns_topic, session_decorator, sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, collection_status, event_handler_status
from lng_datalake_dal.collection_table import CollectionTable

from service_commons import collection_event_handler

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set environment variables
target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
remove_collection_state_machine_arn = os.getenv("REMOVE_COLLECTION_STATE_MACHINE_ARN")
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)
# schemas for the notifications to publish
notification_schemas = ['Schemas/Publish/v0/remove-collection.json', 'Schemas/Publish/v1/remove-collection.json']
# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
@error_handler.handle
@tracker.track_event_handler(event_names.COLLECTION_TERMINATE)
def lambda_handler(event, context):
    logger.info("Event: {0}".format(event))
    logger.info("Context: {0}".format(context))
    return remove_collection_event_handler(event, context.invoked_function_arn)


def remove_collection_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.COLLECTION_TERMINATE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.COLLECTION_TERMINATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    additional_message_attributes = message.get('additional-attributes', {})

    if 'sns-publish-only' in additional_message_attributes:
        notifications = additional_message_attributes['sns-publish-only']["notifications"]
        notification_attributes = additional_message_attributes['sns-publish-only']["notification_attributes"]
        collection_event_handler.publish_notifications(notifications, notification_attributes, target_arn)
        return event_handler_status.SUCCESS

    take_down_flag = message.get("takedown", False)
    collection_id = message['collection-id']
    if take_down_flag:
        state = collection_status.TERMINATING
    else:
        state = collection_status.TERMINATED

    # Getting catalog ids before updating the dynamo db to keep it consistent across all event handlers.
    # If we get catalog ids after updating dynamo db and we get throttled then in some cases we will miss publishing
    # to those catalogs.
    catalog_ids = publish_sns_topic.get_catalog_ids_by_collection_id(collection_id)

    collection_data = update_collection_state(collection_id, state, message['event-id'])

    if take_down_flag:
        start_object_deletion_for_collection(collection_id)

    collection_event_handler.send_collection_notifications(collection_data, notification_schemas,
                                                           message, target_arn, catalog_ids)
    logger.debug("Done")
    return event_handler_status.SUCCESS


def start_object_deletion_for_collection(collection_id):
    execution_input = orjson.dumps({'collection-id': collection_id}).decode()
    execution_name = "{}-{:%Y%m%d.%H%M%S}".format(collection_id, datetime.datetime.now())
    try:
        resp = stepfunctions.get_client().start_execution(
            stateMachineArn=remove_collection_state_machine_arn,
            name=execution_name,
            input=execution_input)
        logger.debug("State Machine Response: {0}".format(resp))
        logger.info("State Machine {0} started for CollectionID {1}".format(execution_name, collection_id))
    except error_handler.retryable_exceptions as e:
        """
        ExecutionAlreadyExists - The execution has the same name as another execution (but a different input).
        ExecutionLimitExceeded - The maximum number of running executions has been reached.
                                 Running executions must end or be stopped before a new execution can be started. 
        InvalidArn - The provided Amazon Resource Name (ARN) is invalid.
        InvalidExecutionInput - The provided JSON input data is invalid.
        InvalidName - The provided name is invalid.
        StateMachineDeleting - The specified state machine is being deleted.
        StateMachineDoesNotExist - The specified state machine does not exist.
        """
        raise e
    except Exception as e:
        raise TerminalErrorException(
            'Unknown Exception: {} remove_collection_state_machine_arn = {}, CollectionID = {}'.format(
                e, remove_collection_state_machine_arn, collection_id))


def update_collection_state(collection_id: str, state: str, event_id: str) ->  dict:
    try:
        collection_data = CollectionTable().get_item({"collection-id": collection_id})
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e2:
        raise TerminalErrorException(
            "A general exception occurred when attempting to get row from CollectionTable | Exception = {0}".format(e2))

    if not collection_data:
        raise TerminalErrorException("Invalid Collection ID {0}".format(collection_id))

    collection_data['event-ids'] = CollectionTable().build_event_ids_list(collection_data, event_id)
    collection_data['collection-state'] = state

    try:
        CollectionTable().update_item(collection_data)
        logger.debug("Updated collection state to {0} for collection id {1}".format(state,
                                                                                    collection_data['collection-id']))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e2:
        raise TerminalErrorException(
            "A general exception occurred when attempting to update row in CollectionTable | Exception = {0}".format(
                e2))

    return collection_data


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE COLLECTION EVENT HANDLER LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['REMOVE_COLLECTION_STATE_MACHINE_ARN'] = 'arn:aws:states:us-east-1:288044017584:stateMachine:' \
                                                        'J on_Deletion_State_Machine'
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ['SUBSCRIPTION_NOTIFICATION_TOPIC_ARN'] = 'arn:aws:' \
                                                        'sns:us-east-1:288044017584:' \
                                                        'SubscriptionNotificationLambda-' \
                                                        'SubscriptionNotificationTopic-1OB8OXXGDBQ5R'
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/CollectionEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CollectionTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-TrackingTable'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CatalogTable'
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CatalogCollectionMappingTable'

    target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
    remove_collection_state_machine_arn = os.getenv("REMOVE_COLLECTION_STATE_MACHINE_ARN")
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {
        "event-name": event_names.COLLECTION_TERMINATE,
        "event-id": "YTvFtYCjMiXQkcmP",
        "request-time": "2017-11-13T10:16:08.829Z",
        "event-version": 1,
        "stage": "LATEST",
        "item-name": "Jonathan Collection",
        "collection-id": "411",
        "owner-id": 442,
        "old-object-versions-to-keep": 0,
        "asset-id": 62
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    mock_context = mock_lambda_context.MockLambdaContext()
    lambda_handler(sample_event, mock_context)
    logger.debug("[LOCAL RUN END - REMOVE COLLECTION EVENT HANDLER LAMBDA]")
