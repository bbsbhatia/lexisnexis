import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, UnhandledException, NoSuchCollection
from lng_datalake_commons import session_decorator
from lng_datalake_constants import collection_status
from lng_datalake_dal.catalog_collection_mapping_table import CatalogCollectionMappingTable
from lng_datalake_dal.collection_table import CollectionTable

from service_commons import collection_common

__author__ = "Shekhar Ralhan, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

my_location = os.path.dirname(__file__)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/GetCollectionCommand-RequestSchema.json',
                         'Schemas/GetCollectionCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return get_collection_command(event['request'], event['context']['stage'])


def get_collection_command(request: dict, stage: str) -> dict:
    collection_id = request['collection-id']

    try:
        collection_response = CollectionTable().get_item({'collection-id': collection_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Collection Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not collection_response:
        raise NoSuchCollection("Invalid Collection ID {0}".format(collection_id),
                               "Collection ID does not exist")

    catalog_ids = get_catalog_ids(collection_id)

    return {"response-dict": generate_response_json(collection_response, catalog_ids, stage)}


def get_catalog_ids(collection_id: str) -> list:
    try:
        items = CatalogCollectionMappingTable().query_items(
            KeyConditionExpression='CollectionID=:collection_id',
            ExpressionAttributeValues={":collection_id": {"S": collection_id}})
    except ClientError as error:
        raise InternalError("Unable to query rows from Catalog Collection Mapping Table", error)

    except Exception as error:
        raise InternalError("Unhandled Exception Occurred", error)

    return [item['catalog-id'] for item in items]


def generate_response_json(collection_data: dict, catalog_ids: list, stage: str) -> dict:
    required_props_defaults = \
        {
            "collection-url": "/collections/{0}/{1}".format(stage, collection_data['collection-id']),
            "collection-state": collection_data["collection-state"]
        }

    required_props = command_wrapper.get_required_response_schema_keys('collection-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('collection-properties')

    response_dict = {}
    prop = None

    try:
        for prop in required_props:
            if prop in required_props_defaults:
                response_dict[prop] = required_props_defaults[prop]
            else:
                response_dict[prop] = collection_data[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in collection_data:
            response_dict[prop] = collection_data[prop]
        elif "object-expiration" in collection_data and prop == "object-expiration-hours":
            response_dict["object-expiration-hours"] = collection_common.transform_expiration_hours(
                collection_data['object-expiration'])
        elif prop == "catalog-ids" and catalog_ids:
            response_dict[prop] = catalog_ids
        elif "collection-hash" in collection_data and prop == "collection-key-url" and collection_data[
            "collection-state"] not in [collection_status.TERMINATED,
                                        collection_status.TERMINATING,
                                        collection_status.TERMINATING_FAILED]:
            response_dict["collection-key-url"] = collection_common.build_collection_key_url(
                collection_data["collection-hash"])

    return response_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    from datetime import datetime

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - GET COLLECTION COMMAND LAMBDA]")

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = '{0}-DataLake-CatalogCollectionMappingTable'.format(
        ASSET_GROUP)

    collection_common.DATA_LAKE_URL = "https://datalake-{0}.content.aws.lexis.com".format(ASSET_GROUP)

    lambda_context = \
        {
            "context":
                {
                    "client-request-id": "12345-request-id-wxyz",
                    "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                    "client-id": "abcd-client-id-7890",
                    "http-method": "GET",
                    "stage": "LATEST",
                    "api-key-id": "testApiKeyId"
                },
            "request":
                {
                    "collection-id": "AARON_1"
                }
        }


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    lambda_response = lambda_handler(lambda_context, mock_context)
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN STOP - GET COLLECTION COMMAND LAMBDA]")
