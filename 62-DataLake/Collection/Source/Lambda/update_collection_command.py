import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import ForbiddenStateTransitionError, UnhandledException, \
    InvalidRequestPropertyValue, NoSuchCollection, SemanticError, InternalError
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names, collection_status
from lng_datalake_dal.collection_table import CollectionTable

from service_commons import collection_common

__author__ = "Maen Nanaa, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)

non_update_states = [collection_status.TERMINATED, collection_status.TERMINATING,
                     collection_status.TERMINATING_FAILED]


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/UpdateCollectionCommand-RequestSchema.json',
                         'Schemas/UpdateCollectionCommand-ResponseSchema.json', event_names.COLLECTION_UPDATE)
def lambda_handler(event, context):
    return update_collection_command(event['request'],
                                     event['context']['client-request-id'],
                                     event['context']['stage'],
                                     event['context']['api-key-id'])


def update_collection_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required properties
    collection_id = request['collection-id']

    current_collection = get_collection_by_id(collection_id)

    # owner authorization
    owner_id = current_collection['owner-id']
    owner_authorization.authorize(owner_id, api_key_id)

    if current_collection['collection-state'] in non_update_states:
        raise SemanticError('Invalid Collection ID {0}'.format(current_collection['collection-id']),
                            'Collection cannot be updated in {0} state'.format(
                                current_collection['collection-state']))

    # If we have patch operations then replace them to simplify the change (suspend and resume dont have this currently)
    if request.get('patch-operations'):
        request = replace_op_commands(request)

    if not request.get('resume', False) and current_collection.get('collection-state') == collection_status.SUSPENDED:
        raise SemanticError("Cannot update collection in {0} state".format(collection_status.SUSPENDED),
                            "Resume then make data changes")

    asset_id = request.get('asset-id', current_collection.get('asset-id'))
    is_asset_changed = asset_id != current_collection.get('asset-id')

    state_change = collection_state_change(request, current_collection)

    if is_asset_changed and state_change['event-name'] != event_names.COLLECTION_UPDATE:
        raise SemanticError("Cannot resume or suspend collection while making data changes",
                            "Resume then make changes or make changes and suspend")

    # find if there is any changes in update request to process and if the request is to change data..
    if not is_asset_changed and state_change['event-name'] == event_names.COLLECTION_UPDATE:
        raise SemanticError("No data changes")

    current_collection.update(request)

    event_dict = generate_event_store_item(current_collection, request_id, state_change['event-name'], stage)

    return {'response-dict': generate_response_json(current_collection, state_change['collection-state'], stage),
            'event-dict': event_dict}


def get_collection_by_id(collection_id: str) -> dict:
    try:
        collection_response = CollectionTable().get_item({'collection-id': collection_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Collection Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not collection_response:
        raise NoSuchCollection("Invalid Collection ID {0}".format(collection_id),
                               "Collection ID does not exist")
    return collection_response


def collection_state_change(request: dict, current_collection: dict) -> dict:
    state_dict = {}
    suspend_collection = request.get('suspend')
    is_suspendable_collection = collection_status.CREATED == current_collection['collection-state']
    if suspend_collection and not is_suspendable_collection:
        raise ForbiddenStateTransitionError(
            "Cannot suspend collection in {0} state".format(current_collection['collection-state']))

    resume_collection = request.get('resume')
    is_resumable_collection = collection_status.SUSPENDED == current_collection['collection-state']

    if resume_collection and not is_resumable_collection:
        raise ForbiddenStateTransitionError(
            "Cannot resume collection in {0} state".format(current_collection['collection-state']))

    if is_suspendable_collection and suspend_collection:
        state_dict['event-name'] = event_names.COLLECTION_SUSPEND
        state_dict['collection-state'] = collection_status.SUSPENDED
    elif is_resumable_collection and resume_collection:
        state_dict['event-name'] = event_names.COLLECTION_RESUME
        state_dict['collection-state'] = collection_status.CREATED
    else:
        state_dict['event-name'] = event_names.COLLECTION_UPDATE
        state_dict['collection-state'] = current_collection['collection-state']
    return state_dict


# TODO: Add ability for smart patch merging (ie add) today we only support full replace
def replace_op_commands(request: dict) -> dict:
    for item in request['patch-operations']:
        if item['op'] != "replace":
            raise InvalidRequestPropertyValue("Invalid operation verb supplied", "Only replace is currently supported")
        # Handle suspend and resume the same way in the code for now
        if item['path'] == "/suspended":
            if item['value']:
                request['suspend'] = True
            else:
                request['resume'] = True
        else:
            request[item['path'].split('/')[1]] = item['value']

    del request['patch-operations']
    return request


def generate_event_store_item(collection_status: dict, request_id: str, event_name: str, stage: str) -> dict:
    event_dict = {
        'event-id': request_id,
        'request-time': command_wrapper.get_request_time(),
        'event-name': event_name,
        'event-version': event_version,
        'stage': stage,
        'collection-id': collection_status['collection-id'],
        'asset-id': collection_status['asset-id']
    }

    return event_dict


def generate_response_json(collection: dict, collection_state: str, stage: str) -> dict:
    required_props_defaults = \
        {
            "collection-url": "/collections/{0}/{1}".format(stage, collection['collection-id']),
            "collection-state": collection_state
        }
    required_props = command_wrapper.get_required_response_schema_keys('collection-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('collection-properties')

    response_dict = {}
    prop = None

    try:
        for prop in required_props:
            if prop in required_props_defaults:
                response_dict[prop] = required_props_defaults[prop]
            else:
                response_dict[prop] = collection[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in collection:
            response_dict[prop] = collection[prop]
        elif "object-expiration" in collection and prop == "object-expiration-hours":
            response_dict["object-expiration-hours"] = collection_common.transform_expiration_hours(
                collection['object-expiration'])

    return response_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    from datetime import datetime

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE COLLECTION COMMAND LAMBDA]")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ['ASSET_DYNAMODB_TABLE'] = 'feature-DataLake-AssetTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-OwnerTable'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-DataLake-CollectionTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-DataLake-TrackingTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "PATCH",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": "Ab-1",
                # "owner-id": 1,
                "suspend": True
                # "resume": True
                # "asset-id": 62
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - UPDATE COLLECTION COMMAND LAMBDA]")
