import logging
import os
import re

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import owner_authorization, command_wrapper
from lng_datalake_commands.exceptions import InternalError, UnhandledException, InvalidRequestPropertyValue, \
    NotAuthorizedError, CollectionAlreadyExists, DataLakeException
from lng_datalake_commons import session_decorator
from lng_datalake_constants import collection_status, event_names
from lng_datalake_dal.collection_table import CollectionTable

__author__ = "Shekhar Ralhan"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/CreateCollectionCommand-RequestSchema.json',
                         'Schemas/CreateCollectionCommand-ResponseSchema.json', event_names.COLLECTION_CREATE)
def lambda_handler(event, context):
    return create_collection_command(event['request'],
                                     event['context']['client-request-id'],
                                     event['context']['stage'],
                                     event['context']['api-key-id'])


def create_collection_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required properties
    collection_id = request['collection-id']

    # optional properties
    classification_type = request.get('classification-type', 'Content')

    # Get OwnerID from authorization key
    owner_response = owner_authorization.get_owner_by_api_key_id(api_key_id)
    owner_id = owner_response['owner-id']

    collection_prefix = owner_response.get('collection-prefix')

    # validate collection-id
    validate_collection_id(collection_id, collection_prefix)

    response_dict = generate_response_json(request, collection_id, owner_id, stage, classification_type)

    event_dict = generate_event_store_item(response_dict, request_id, stage)

    return {'response-dict': response_dict, 'event-dict': event_dict}


def get_default_objects_expiration(classification_type: str) -> int:
    if classification_type == "Test":
        # 7 days
        return 168
    # further types and their expirations can be added here.


def validate_collection_id(collection_id: str, owner_registered_prefix: str = None) -> None:
    if len(collection_id) > 40:
        raise InvalidRequestPropertyValue("Invalid Collection ID {0}".format(collection_id),
                                          "Collection ID length should not exceed 40 characters")

    if collection_id.isdigit():
        raise InvalidRequestPropertyValue("Invalid Collection ID {0}".format(collection_id),
                                          "Collection ID cannot contain only digits")

    result = collection_id.split("_")
    if len(result) > 1:
        collection_prefix = result[0]
        if owner_registered_prefix != collection_prefix:
            raise NotAuthorizedError("Invalid Collection ID {0}".format(collection_id),
                                     "Collection prefix is not registered by current owner")
        base_collection_id = "_".join(result[1:])
    else:
        base_collection_id = collection_id

    if not re.match("^[A-Za-z0-9-]*$", base_collection_id):
        raise InvalidRequestPropertyValue("Invalid Collection ID {0}".format(base_collection_id),
                                          "Collection ID can only contain letters, digits and hyphens")

    try:
        if CollectionTable().get_item({'collection-id': collection_id}):
            raise CollectionAlreadyExists("Invalid Collection ID {0}".format(collection_id),
                                          "Collection ID already exists")
    except DataLakeException as e:
        raise e
    except ClientError as e:
        raise InternalError("Unable to get item from Collection Table", e)
    except Exception as ex:
        raise UnhandledException(ex)


def generate_response_json(request: dict, collection_id: str, owner_id: int, stage: str,
                           classification_type: str) -> dict:
    response_dict = {}
    prop = None

    required_props_defaults = \
        {
            "collection-id": collection_id,
            "owner-id": owner_id,
            "collection-url": "/collections/{0}/{1}".format(stage, collection_id),
            "collection-state": collection_status.CREATED,
            "classification-type": classification_type,
            "old-object-versions-to-keep": request.get('old-object-versions-to-keep', 0),
            "collection-timestamp": command_wrapper.get_request_time()
        }

    required_props = command_wrapper.get_required_response_schema_keys('collection-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('collection-properties')

    try:
        for prop in required_props:
            if prop in required_props_defaults:
                response_dict[prop] = required_props_defaults[prop]
            else:
                response_dict[prop] = request[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in request:
            response_dict[prop] = request[prop]

    # if the objects-expiration is not provided, default it except for Content type,
    # since content type never expires by default
    if not request.get('object-expiration-hours') and classification_type != "Content":
        response_dict['object-expiration-hours'] = get_default_objects_expiration(classification_type)

    return response_dict


def generate_event_store_item(collection_response: dict, request_id: str, stage: str) -> dict:
    event_dict = {
        'collection-id': collection_response['collection-id'],
        'owner-id': collection_response['owner-id'],
        'asset-id': collection_response['asset-id'],
        'classification-type': collection_response['classification-type'],
        'old-object-versions-to-keep': collection_response['old-object-versions-to-keep'],
        'event-id': request_id,
        'request-time': command_wrapper.get_request_time(),
        'event-name': event_names.COLLECTION_CREATE,
        'event-version': event_version,
        'stage': stage
    }

    if 'object-expiration-hours' in collection_response:
        event_dict['object-expiration'] = "{}h".format(collection_response['object-expiration-hours'])

    if 'description' in collection_response:
        event_dict['description'] = collection_response['description']

    return event_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    from datetime import datetime

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE COLLECTION COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-OwnerTable'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-DataLake-CollectionTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-DataLake-TrackingTable'

    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "POST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": "CreateCollectionTest",
                "asset-id": 62,
                "description": "Create Collection Test",
                "object-expiration-hours": 20
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE COLLECTION COMMAND LAMBDA]")
