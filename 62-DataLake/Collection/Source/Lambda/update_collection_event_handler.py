import logging
import os
from inspect import stack, getmodulename

from aws_xray_sdk.core import patch
from lng_datalake_commons import publish_sns_topic
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, collection_status, event_handler_status
from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError

from service_commons import collection_event_handler

__author__ = "Jonathan A. Mitchall, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set environment variables
target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)
# schemas for the notifications to publish
notification_schemas = ['Schemas/Publish/v0/update-collection.json', 'Schemas/Publish/v1/update-collection.json']
# Set module name for error handling
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
@error_handler.handle
@tracker.track_event_handler(event_names.COLLECTION_UPDATE)
def lambda_handler(event, context):
    logger.info("Event: {}".format(event))
    logger.info("Context: {}".format(vars(context)))
    return update_collection_event_handler(event, context.invoked_function_arn)


def update_collection_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.COLLECTION_UPDATE,
                                           event_names.COLLECTION_SUSPEND, event_names.COLLECTION_RESUME):
        raise TerminalErrorException(
            "Failed to extract event: {0} or event-name doesn't "
            "match {1}, {2} or {3}".format(event, event_names.COLLECTION_UPDATE, event_names.COLLECTION_SUSPEND,
                                           event_names.COLLECTION_RESUME))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    additional_message_attributes = message.get('additional-attributes', {})

    if 'sns-publish-only' in additional_message_attributes:
        notifications = additional_message_attributes['sns-publish-only']["notifications"]
        notification_attributes = additional_message_attributes['sns-publish-only']["notification_attributes"]
        collection_event_handler.publish_notifications(notifications, notification_attributes, target_arn)
        return event_handler_status.SUCCESS

    collection_id = message['collection-id']
    existing_collection = get_collection_by_id(collection_id)

    # Getting catalog ids before updating the dynamo db to keep it consistent across all event handlers.
    # If we get catalog ids after updating dynamo db and we get throttled then in some cases we will miss publishing
    # to those catalogs.
    catalog_ids = publish_sns_topic.get_catalog_ids_by_collection_id(collection_id)

    updated_item = validate_collection_state(message, existing_collection)
    if updated_item:
        updated_item = generate_collection_item(message, updated_item)
        update_collection_table(updated_item)
        collection_event_handler.send_collection_notifications(updated_item, notification_schemas,
                                                               message, target_arn, catalog_ids)

    return event_handler_status.SUCCESS


def get_collection_by_id(collection_id: str) -> dict:
    try:
        collection_response = CollectionTable().get_item({'collection-id': collection_id})
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as ex:
        raise TerminalErrorException(
            "A general exception occurred when attempting to update row in CollectionTable | Exception = {0}".format(
                ex))

    if not collection_response:
        raise TerminalErrorException("Invalid Collection ID {0} "
                                     "Collection ID does not exist".format(collection_id))
    return collection_response


def validate_collection_state(message: dict, existing_collection: dict) -> dict:
    """
    return an empty dictionary instead of raising a terminal error for a no-op
    :param message: changes to make to existing collection
    :param existing_collection:
    :return: empty dictionary if no-op
    """
    if (message['event-name'] == event_names.COLLECTION_SUSPEND and
        existing_collection['collection-state'] == collection_status.SUSPENDED) or \
            (message['event-name'] == event_names.COLLECTION_RESUME and
             existing_collection['collection-state'] == collection_status.CREATED):
        return {}

    if message['event-name'] == event_names.COLLECTION_SUSPEND and \
            existing_collection['collection-state'] != collection_status.CREATED:
        raise TerminalErrorException("Collection {0} state cannot be changed from {1} to {2}"
                                     .format(message['collection-id'], existing_collection['collection-state'],
                                             collection_status.SUSPENDED))

    if message['event-name'] == event_names.COLLECTION_RESUME and \
            existing_collection['collection-state'] != collection_status.SUSPENDED:
        raise TerminalErrorException("Collection {0} state cannot be changed from {1} to {2}"
                                     .format(message['collection-id'], existing_collection['collection-state'],
                                             collection_status.CREATED))

    return existing_collection


def generate_collection_item(message: dict, existing_collection: dict) -> dict:
    collection_item = {}
    prop = None
    try:

        for prop in CollectionTable().get_required_schema_keys():
            if prop == 'collection-state':
                # set state to existing collection state, overwrite if event-name is suspend/resume
                if message['event-name'] == event_names.COLLECTION_SUSPEND:
                    collection_item[prop] = collection_status.SUSPENDED
                elif message['event-name'] == event_names.COLLECTION_RESUME:
                    collection_item[prop] = collection_status.CREATED
                else:
                    collection_item[prop] = existing_collection[prop]
            elif prop == "event-ids":
                collection_item[prop] = CollectionTable().build_event_ids_list(existing_collection, message['event-id'])
            else:
                collection_item[prop] = message.get(prop, existing_collection[prop])
    except Exception as ex:
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in CollectionTable().get_optional_schema_keys():
        if prop in message:
            collection_item[prop] = message[prop]
        elif prop in existing_collection:
            collection_item[prop] = existing_collection[prop]

    return collection_item


def update_collection_table(item):
    """
    Inserts collection event as an item into Collection table
    :return: True or False
    :param item: Collection Event item to be inserted
    """
    try:
        CollectionTable().update_item(item)
        logger.info("Insertion to Collection table succeeded.")
    except SchemaValidationError as e:
        raise TerminalErrorException(
            "Failed to insert item: {} into Collection table due to SchemaValidationError from DAL. {}".format(item, e))
    except ConditionError as e:
        raise TerminalErrorException("Failed to insert item {} into Collection table due to ConditionError from DAL. {}"
                                     .format(item, e))
    except SchemaError as e:
        raise TerminalErrorException(
            "Failed to insert item {} into Collection table due to SchemaError from DAL. {}".format(item, e))
    except SchemaLoadError as e:
        raise TerminalErrorException(
            "Failed to insert item {} into Collection table due to SchemaLoadError from DAL. {}".format(item, e))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to insert item {} into Collection table due to unknown reasons. {}".format(item, e))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE COLLECTION EVENT HANDLER LAMBDA]")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/CollectionEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ[
        'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN'] = 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CollectionTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-TrackingTable'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CatalogTable'
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CatalogCollectionMappingTable'

    target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {
        "description": "Description of collection",
        "object-expiration": "20h",
        "request-time": "2019-09-13T19:41:25.971Z",
        "event-name": "Collection::Update",
        "classification-type": "Content",
        "owner-id": 205,
        "collection-id": "411",
        "event-id": "fCkAAlXtxOMxEsTJ",
        "item-name": "Updated_2 Classification Collection 31",
        "stage": "LATEST",
        "asset-id": 62,
        "event-version": 1,
        "old-object-versions-to-keep": 0,
        "seen-count": 0
    }

    sample_event = {
        "Records": [{
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:us-east-1:288044017584:UpdateCollectionEventHandlerTopic:49f4a938"
                                    "-0394-419e-a739-2ffdb3a75190",
            "Sns": {
                "Type": "Notification",
                "MessageId": "85b707ad-c852-52e4-b10a-55488ee08219",
                "TopicArn": "arn:aws:sns:us-east-1:288044017584:UpdateCollectionEventHandlerTopic",
                "Subject": "",
                "Message": json.dumps(msg),
                "Timestamp": "2018-01-26T15:08:34.053Z",
                "SignatureVersion": "1",
                "Signature": "f0ykfJhySH"
                             "+JyMQ58gnCNNp29diBMUjv5rNzqCwG886yvU7dbWaX3zIZlzVepXgoQtfYRluAinx5U5qeHh7CzCZzLYcOTpbIv1"
                             "blto/qdNVnCm68FXVtF/vKMR+/6m+Dk/vIQAQsRutrTCs5Y5/uZ1PmlTzLoa0Z1/+vfFs3zmblvG+Va/D4gEtm2v"
                             "zzF0a/tO0kSpBHacbMEQ7k7PLZDHt8VAoZEqaSHuXRvHCuuRqHzMhzqxZkmFXVFm1F/imbmKdC7QXjLeytJNBhzH"
                             "Czr645LhTcfjbD7pwOBhqQzMIuWpV6CxL++Uk4NxQVGeHkrpcnAn1VoN/Ap329e5PlXw==",
                "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService"
                                  "-433026a4050d206028891664da859041.pem",
                "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws"
                                  ":sns:us-east-1:288044017584:UpdateCollectionEventHandlerTopic:49f4a938-0394-419e"
                                  "-a739-2ffdb3a75190",
                "MessageAttributes": {}
            }
        }
        ]
    }
    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - UPDATE COLLECTION EVENT HANDLER LAMBDA]")
