import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, NoSuchCollection, UnhandledException, \
    ForbiddenStateTransitionError
from lng_datalake_commons import session_decorator
from lng_datalake_constants import collection_status
from lng_datalake_constants import event_names
from lng_datalake_dal.collection_table import CollectionTable

from service_commons import collection_common

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/RemoveCollectionCommand-RequestSchema.json',
                         'Schemas/RemoveCollectionCommand-ResponseSchema.json', event_names.COLLECTION_TERMINATE)
def lambda_handler(event, context):
    return remove_collection_command(event["request"],
                                     event['context']['client-request-id'],
                                     event['context']['stage'],
                                     event['context']['api-key-id'])


def remove_collection_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required properties
    collection_id = request['collection-id']

    # optional properties
    takedown = request.get('takedown', False)

    collection_response = get_collection_by_id(collection_id)

    # owner authorization
    owner_authorization.authorize(collection_response['owner-id'], api_key_id)

    collection_state = collection_response['collection-state']
    if collection_state != collection_status.SUSPENDED:
        raise ForbiddenStateTransitionError(
            "Collection must be in a {0} state to be removed".format(collection_status.SUSPENDED),
            "Suspend collection before removing it")

    event_dict = \
        {
            'collection-id': collection_id,
            'takedown': takedown,
            'event-id': request_id,
            'request-time': command_wrapper.get_request_time(),
            'event-name': event_names.COLLECTION_TERMINATE,
            'event-version': event_version,
            'stage': stage
        }

    return {"response-dict": generate_response_json(collection_response, stage), "event-dict": event_dict}


def get_collection_by_id(collection_id: str) -> dict:
    try:
        collection_response = CollectionTable().get_item({"collection-id": collection_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Collection Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not collection_response:
        raise NoSuchCollection("Invalid Collection ID {0}".format(collection_id),
                               "Collection ID does not exist")
    return collection_response


def generate_response_json(collection: dict, stage: str) -> dict:
    required_props_defaults = \
        {
            "collection-url": "/collections/{0}/{1}".format(stage, collection['collection-id']),
            "collection-state": collection_status.TERMINATING
        }

    required_props = command_wrapper.get_required_response_schema_keys('collection-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('collection-properties')

    response_dict = {}
    prop = None

    try:
        for prop in required_props:
            if prop in required_props_defaults:
                response_dict[prop] = required_props_defaults[prop]
            else:
                response_dict[prop] = collection[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in collection:
            response_dict[prop] = collection[prop]
        elif "object-expiration" in collection and prop == "object-expiration-hours":
            response_dict["object-expiration-hours"] = collection_common.transform_expiration_hours(
                collection['object-expiration'])

    return response_dict


if __name__ == '__main__':
    import json
    from lng_datalake_dal.tracking_table import TrackingTable
    from lng_datalake_testhelper import mock_lambda_context
    from datetime import datetime
    import random
    import string

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE COLLECTION COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-DataLake-CollectionTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-OwnerTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-DataLake-TrackingTable'

    TrackingTable().table_name = os.getenv("TRACKING_DYNAMODB_TABLE")


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "DELETE",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": "Ab-1",
                "takedown": True
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    lambda_response = lambda_handler(lambda_event, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - REMOVE COLLECTION COMMAND LAMBDA]")
