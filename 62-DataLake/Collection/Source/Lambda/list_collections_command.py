import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, paginator_token, command_validator
from lng_datalake_commands.exceptions import InternalError, UnhandledException, SemanticError
from lng_datalake_commons import session_decorator
from lng_datalake_constants import collection_status
from lng_datalake_dal.collection_table import CollectionTable

from service_commons import collection_common

__author__ = "Aaron Belvo, Kiran G"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

ct_owner_index = "collection-by-owner-index"
ct_state_and_owner_index = "collection-state-ownerid-index"
DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 1000))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/ListCollectionsCommand-RequestSchema.json',
                         'Schemas/ListCollectionsCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return list_collections_command(event['request'], event['context']['stage'])


def list_collections_command(request: dict, stage: str) -> dict:
    # optional properties
    max_items = request.get('max-items', DEFAULT_MAX_ITEMS)
    next_token = request.get('next-token')
    owner_id = request.get('owner-id')
    show_terminated_only = request.get('show-terminated-only', False)
    show_terminated = request.get('show-terminated', True)

    pagination_token = None
    # If we have a next-token validate the max-items matches the original request
    # Dyanmo doesn't handle pagination if the max-items changes
    if next_token:
        tokens = paginator_token.unpack_validate_pagination_token(next_token, max_items)
        pagination_token = tokens['pagination-token']

    # sanity check for max-items (request schema should prevent)
    command_validator.validate_max_results(max_items, DEFAULT_MAX_ITEMS)

    # make sure terminated parameters make sense
    # TODO: Should be removed at some point since we should only accept state as a QP and not these
    if not show_terminated and show_terminated_only:
        raise SemanticError("The show-terminated parameter cannot be false if the "
                            "show-terminated-only parameter is true")

    # get optional index and query/scan parameters
    index, kwargs = generate_filter_parameters(owner_id, show_terminated_only, show_terminated)

    collection_list = get_collection_list(index, max_items, pagination_token, kwargs)

    return {"response-dict": generate_response_json(collection_list, CollectionTable().get_pagination_token(), stage,
                                                    max_items)}


def generate_filter_parameters(owner_id: int, show_terminated_only: bool, show_terminated: bool) -> tuple:
    index = None
    kwargs = {}

    # get terminated collections by owner id
    if owner_id and show_terminated_only:
        index = ct_state_and_owner_index
        kwargs = {"KeyConditionExpression": 'CollectionState=:collection_state and OwnerID=:owner_id',
                  "ExpressionAttributeValues": {":collection_state": {"S": collection_status.TERMINATED},
                                                ":owner_id": {"N": str(owner_id)}}}

    # get all collections by owner id
    elif owner_id:
        index = ct_owner_index
        kwargs = {"KeyConditionExpression": 'OwnerID=:owner_id',
                  "ExpressionAttributeValues": {":owner_id": {"N": str(owner_id)}}}

    # get all terminated collections
    elif show_terminated_only:
        index = ct_state_and_owner_index
        kwargs = {"KeyConditionExpression": 'CollectionState=:collection_state',
                  "ExpressionAttributeValues": {":collection_state": {"S": collection_status.TERMINATED}}}

    # add filter if not returning terminated collections
    if not show_terminated and not show_terminated_only:
        kwargs["FilterExpression"] = 'CollectionState<>:collection_state2'
        if "ExpressionAttributeValues" not in kwargs:
            kwargs["ExpressionAttributeValues"] = {":collection_state2": {"S": collection_status.TERMINATED}}
        else:
            kwargs["ExpressionAttributeValues"].update({":collection_state2": {"S": collection_status.TERMINATED}})

    return index, kwargs


def get_collection_list(index: str, max_results: int, pagination_token: str, kwargs) -> list:
    # run the query or scan
    if index:
        try:
            collection_list = CollectionTable().query_items(index=index,
                                                            max_items=max_results,
                                                            pagination_token=pagination_token,
                                                            **kwargs)
        except ClientError as e1:
            raise InternalError("Unable to query items from Collection Table index {0}".format(index), e1)
        except Exception as e2:
            raise UnhandledException(e2)
    else:
        try:
            collection_list = CollectionTable().get_all_items(max_items=max_results,
                                                              pagination_token=pagination_token,
                                                              **kwargs)
        except ClientError as e1:
            raise InternalError("Unable to get items from Collection Table", e1)
        except Exception as e2:
            raise UnhandledException(e2)

    return collection_list


def generate_response_json(collection_list: list, pagination_token: str, stage: str, max_items: int) -> dict:
    collection_response = []
    for collection_data in collection_list:
        collection_response.append(generate_response_item_json(collection_data, stage))

    response = {"collections": collection_response}

    if pagination_token:
        response['next-token'] = paginator_token.generate_pagination_token(max_items, pagination_token)

    response['item-count'] = len(collection_list)

    return response


def generate_response_item_json(collection_data: dict, stage: str) -> dict:
    required_props_defaults = \
        {
            "collection-url": "/collections/{0}/{1}".format(stage, collection_data['collection-id']),
            "collection-state": collection_data["collection-state"]
        }

    required_props = command_wrapper.get_required_response_schema_keys('collections-n-pagination', 'properties',
                                                                       'collections', 'items')
    optional_props = command_wrapper.get_optional_response_schema_keys('collections-n-pagination', 'properties',
                                                                       'collections', 'items')
    response_item = {}

    for prop in required_props:
        if prop in required_props_defaults:
            response_item[prop] = required_props_defaults[prop]
        elif prop in collection_data:
            response_item[prop] = collection_data[prop]

    for prop in optional_props:
        if prop in collection_data:
            response_item[prop] = collection_data[prop]
        elif prop == "object-expiration-hours" and "object-expiration" in collection_data:
            response_item["object-expiration-hours"] = collection_common.transform_expiration_hours(
                collection_data['object-expiration'])
        elif prop == "collection-key-url" and "collection-hash" in collection_data and collection_data[
            "collection-state"] not in [collection_status.TERMINATED,
                                        collection_status.TERMINATING,
                                        collection_status.TERMINATING_FAILED]:
            response_item["collection-key-url"] = collection_common.build_collection_key_url(
                collection_data["collection-hash"])

    return response_item


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    from datetime import datetime

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{0}-DataLake-OwnerTable'.format(ASSET_GROUP)

    collection_common.DATA_LAKE_URL = "https://datalake-{0}.content.aws.lexis.com".format(ASSET_GROUP)

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - LIST COLLECTIONS COMMAND LAMBDA]")

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "owner-id": 8
                # "next-token": "1|eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ29sbGVjdGlvbklEIjogeyJTIjogIjEwMCJ9LCAiT3duZXJJRCI6IHsiTiI6ICIyMDUifSwgIkNvbGxlY3Rpb25OYW1lIjogeyJTIjogInJlZ3Jlc3Npb25fdGVzdF8yMDE4LTExLTEzIDA5OjUzOjQ5LjY5ODkzOCJ9fX0=",
                # "show-terminated-only": True
                # "show-terminated": False,
            }

        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    lambda_response = lambda_handler(lambda_event, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - LIST COLLECTIONS COMMAND LAMBDA]")
