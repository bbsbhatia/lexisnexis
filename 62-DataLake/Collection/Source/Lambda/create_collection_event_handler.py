import hashlib
import logging
import os
from inspect import stack, getmodulename

from aws_xray_sdk.core import patch
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, collection_status, event_handler_status
from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module namep
error_handler.lambda_name = getmodulename(stack()[0][1])

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@error_handler.handle
@tracker.track_event_handler(event_names.COLLECTION_CREATE)
def lambda_handler(event, context):
    logger.info("Event: {}".format(event))
    logger.info("Context: {}".format(vars(context)))
    return create_collection_event_handler(event, context.invoked_function_arn)


def create_collection_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.COLLECTION_CREATE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.COLLECTION_CREATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    collection_data = collection_item_transform(message)
    if collection_data:
        insert_to_collection_table(collection_data)
    else:
        raise TerminalErrorException("Failed to insert item into Collection table. Failed to transform invalid "
                                     "item. Item = {0}".format(message))

    logger.debug("Done")
    return event_handler_status.SUCCESS


def insert_to_collection_table(item: dict):
    """
    Inserts collection event as an item into Collection table.

    :param item: Collection Event item to be inserted
    """
    try:
        CollectionTable().put_item(item)
        logger.info("Insertion to Collection table succeeded.")
    except (SchemaValidationError, ConditionError, SchemaError, SchemaLoadError) as e:
        raise TerminalErrorException("Failed to insert item into Collection table. Item = {0} | Exception = {1}"
                                     .format(item, e))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Failed to insert item into Collection table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(item, e))


def collection_item_transform(item: dict) -> dict:
    # Use {} vs. dict() for efficiency
    new_dict = {}
    try:
        default_properties = {
            "collection-timestamp": item["request-time"],
            "collection-state": collection_status.CREATED
        }
        for prop in CollectionTable().get_required_schema_keys():
            if prop in default_properties:
                new_dict[prop] = default_properties[prop]
            elif prop == "event-ids":
                new_dict[prop] = CollectionTable().build_event_ids_list({}, item['event-id'])
            else:
                new_dict[prop] = item[prop]
    except KeyError as e:
        raise TerminalErrorException("Key = {} does not exist.".format(e))

    # TODO: move the following to default_properties once we change collection-hash to be a required property in schema
    new_dict["collection-hash"] = hashlib.sha1(item["collection-id"].encode()).hexdigest()

    for key in CollectionTable().get_optional_schema_keys():
        if key in item:
            new_dict[key] = item[key]

    return new_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE COLLECTION EVENT HANDLER LAMBDA]")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/CollectionEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CollectionTable'

    error_handler.terminal_errors_bucket = "ccs-sandbox-lambda-terminal-errors"

    msg = {
        "description": "Description of collection",
        "request-time": "2018-10-30T17:37:11.932Z",
        "event-name": "Collection::Create",
        "classification-type": "Content",
        "owner-id": 205,
        "collection-id": "Prashant-32",
        "event-id": "nTTcqbDeImYQqRkV",
        "item-name": "Classification Collection 31",
        "stage": "LATEST",
        "old-object-versions-to-keep": 0,
        "asset-id": 62,
        "event-version": 1,
        "seen-count": 0
    }
    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - CREATE COLLECTION EVENT HANDLER LAMBDA]")
