import os

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


DATA_LAKE_URL = os.getenv("DATA_LAKE_URL")


def transform_expiration_hours(expiration_hours: str) -> int:
    """
    Transform the expiration hours string to an integer value. For example: "128h" -> 128
    :param expiration_hours: string with the number of hours and a trailing 'h'
    :return: number of hours
    """
    return int(expiration_hours.replace('h', ''))


def build_collection_key_url(collection_hash: str) -> str:
    return '{0}/objects/store/?prefix={1}/'.format(DATA_LAKE_URL, collection_hash)
