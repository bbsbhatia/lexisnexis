import logging
import os

from lng_datalake_commons import publish_sns_topic
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException

from service_commons import collection_common

__author__ = "John Konderla, Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


def send_collection_notifications(collection_item: dict, schemas: list, event: dict, target_arn: str,
                                  catalog_ids: list = None) -> None:
    notification_attributes = {'event-name': event['event-name'], 'collection-id': [collection_item['collection-id']]}
    if catalog_ids:
        notification_attributes['catalog-id'] = catalog_ids
    notifications = []
    concat_notification = {}
    schema_versions = []
    for schema_name in schemas:
        schema_version = schema_name.split('/')[2]
        notification = generate_collection_notification(schema_name, schema_version, collection_item, event,
                                                        catalog_ids)
        notifications.append(
            {"schema-rel-path": schema_name, "schema-version": schema_version, "message": notification})
        if len(schemas) > 1:
            schema_versions.append(schema_version)
            if schema_version == 'v0':
                concat_notification.update(notification)
            else:
                concat_notification[schema_version] = notification

    if concat_notification:
        notifications.append({"schema-version": ''.join(sorted(schema_versions)), "message": concat_notification})

    publish_notifications(notifications, notification_attributes, target_arn)


def generate_collection_notification(schema_name: str, schema_version: str, collection_item: dict, event: dict,
                                     catalog_ids: list) -> dict:
    notification_schema = publish_sns_topic.load_json_schema(schema_name)
    required_props, optional_props = get_schema_props(notification_schema, schema_version)
    collection_msg = {}

    try:
        for prop in required_props:
            if prop == "event-name":
                collection_msg[prop] = event['event-name']
            elif prop == "item-state":
                collection_msg[prop] = collection_item["collection-state"]
            else:
                collection_msg[prop] = collection_item[prop]
    except KeyError as e:
        raise TerminalErrorException(
            "Failed to generate collection notification. Missing required property: {}".format(e))

    for prop in optional_props:
        if prop == "object-expiration-hours" and "object-expiration" in collection_item:
            collection_msg[prop] = collection_common.transform_expiration_hours(
                collection_item['object-expiration'])
        elif prop == "catalog-ids" and catalog_ids:
            collection_msg[prop] = catalog_ids
        elif prop in collection_item:
            collection_msg[prop] = collection_item[prop]

    if schema_version == 'v0':
        return collection_msg

    context_msg = get_context(notification_schema, event)

    return {"SchemaVersion": notification_schema['properties']['SchemaVersion']["enum"][0],
            "Schema": notification_schema['properties']['Schema']["enum"][0],
            "Type": "Publish",
            "context": context_msg,
            "collection": collection_msg
            }


def get_context(notification_schema: dict, event: dict) -> dict:
    context_msg = {}

    required_context_props, optional_context_props = get_schema_context_props(notification_schema)
    try:
        default_properties = {
            "request-id": event['event-id']
        }
        for prop in required_context_props:
            if prop in default_properties:
                context_msg[prop] = default_properties[prop]
            else:
                context_msg[prop] = event[prop]

    except KeyError as e:
        raise TerminalErrorException(
            "Failed to generate collection notification. Missing required property: {}".format(e))

    for prop in optional_context_props:
        context_msg[prop] = event[prop]

    return context_msg


def get_schema_props(schema: dict, schema_version: str) -> tuple:
    if schema_version == 'v0':
        required_props = schema['required']
        all_props = schema['properties'].keys()
    else:
        required_props = schema['properties']['collection']['required']
        all_props = schema['properties']['collection']['properties'].keys()
    optional_props = [p for p in all_props if p not in required_props]
    return required_props, optional_props


def get_schema_context_props(schema: dict) -> tuple:
    required_props = schema['properties']['context']['required']
    all_props = schema['properties']['context']['properties'].keys()
    optional_props = [p for p in all_props if p not in required_props]
    return required_props, optional_props


def publish_notifications(notifications: list, notification_attributes: dict, target_arn: str) -> None:
    for index, notification in enumerate(notifications):
        try:
            publish_sns_topic.publish_to_topic(notification, notification_attributes, target_arn)
        except error_handler.retryable_exceptions as e:
            logger.warning("Exception when sending message {} to "
                           "SNS topic {}||{}".format(notification, target_arn, e))
            retry_info = {"notifications": notifications[index:], "notification_attributes": notification_attributes}
            error_handler.set_message_additional_attribute('sns-publish-only', retry_info)
            raise e
