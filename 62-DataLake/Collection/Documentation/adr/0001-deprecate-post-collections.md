
# Deprecate Post Collections


Date: 2019-07-29

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Our POST collections api is used very little compared to POST collections/{collectionId}.
 
If we deprecate this we will simplify our api, improve our swagger, and reduce the amount of code we have to maintain.

### Usage on prod
2 total executions in the last 2 weeks
3 total executions in the last 4 weeks
39 total executions in the last 4 months

1 successful execution in the last 4 months (excluding executions by the DataLake team)

### Usage on cert
66 total executions in last 4 weeks
77 total executions in last 4 weeks
268 total executions in last 4 months

98 successful executions in 4 months (excluding asset-id 62)
74/98 were from a single owner
16 unique owners



## Decision
We have decided to deprecate POST Collections.

## Consequences
* POST collections gets removed from our swagger client v1
* Some time in the future, POST collections gets removed from the api
* Users will have to specify a collection-id when creating a collection
* Team wormhole will have to notify Users who have used this in the last 4 months that it has been deprecated

## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/TeamRoom.mvc/Show/26489)