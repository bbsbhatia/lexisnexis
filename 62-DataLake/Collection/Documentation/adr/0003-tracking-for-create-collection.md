
# Track Creation of Collections


Date: 2019-10-30

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Technical Story: [S-84979](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:954533)

Some Users of the DataLake require immediate interaction with Collections they create.  These Users do not wish to wait for the DataLake's native, eventual consistency architecture to interact with their recently created Collections.

Currently, we are returning an error to Users when they attempt to Create a Collection and then immediately attempt to insert Objects into that Collection or attempt to add that Collection into a Catalog.

## Decision
For any APIs that currently validate the existence of a Users's Collection, we will need to take a peek into the Tracking Table (if the Collection is not found in the Collection Table) to determine if a Create Collection (Collection::Create) event for that Collection is in-flight.  If the creation of the Collection is in-flight, we will also interrogate the EventStore Table to determine if the Caller of the API is the Owner of the Collection.

##### APIs Affected
* Create Collection
* Update Collection
* Update Catalog *
* Create Object
* Update Object
* Create Object Multipart Upload
* Update Object Multipart Upload
* Create Folder Upload
* Update Folder Upload
* Finish Folder Upload
* Create Ingestion

\* Note: Update Catalog would not require an Event Store Table lookup since there is no Owner validation for Collections   

##### Alternate Options Discussed
* User's could have polled DataLake to determine when the Collection is created, but that defeats DataLakes eventing architecture.
* DataLake's Create Collection Command could write directly to the Collection Table, but that would again introduce inconsistencies in DataLake's eventual consistency architecture.
* A subscription filter including owner-id for notifications for Collection Create events could be created by Users.  The Create Collection Event Handler would need to be modified to send notifications.

## Consequences
* Create Collection events will need to be tracked in the Tracking Table
* Current Collection validation will need to be modified if the Collection is not found in the Collection Table
* Lookups to two additional DynamoDBs will need to be performed.

## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:954533)