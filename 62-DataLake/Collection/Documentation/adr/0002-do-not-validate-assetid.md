
# Deprecate Post Collections


Date: 2019-07-29

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Currently we maintain Glitz asset id in a dynamodb table which we must manually keep in sync. We validate asset-id against
this dynamo table to ensure it's a valid Glitz AssetID.  We are proposing to drop validating the AssetID on the request
and instead accept any number they provide.
Pro: Don't need to keep Glitz and our dynamo in sync
Pro: Reduce Dynamo calls when creating collections and reduce cost
Con: AssetIDs could be invalid and mapped to collections

## Decision
We have decided to not validate AssetID when Create/Update a collection

## Consequences
* It's possible to have invalid Glitz AssetID's tied to a collection

## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:897351)