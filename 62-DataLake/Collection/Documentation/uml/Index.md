# Index
[README](README.md)

* Collections
    * [Create Collection Command](CreateCollectionCommand.puml)
    * [Create Collection Event Handler](CreateCollectionEventHandler.puml)
    * [Get Collection Command](GetCollectionCommand.puml)
    * [List Collections Command](ListCollectionsCommand.puml)
    * [Remove Collection Command](RemoveCollectionCommand.puml)
    * [Remove Collection Event Handler](RemoveCollectionEventHandler.puml)
    * [Update Collection Command](UpdateCollectionCommand.puml)
    * [Update Collection Event Handler](UpdateCollectionEventHandler.puml)
