import logging
import os
import unittest
from datetime import datetime

import time
from lng_datalake_client.Admin.remove_owner import remove_owner
from lng_datalake_client.Collection.get_collection import get_collection
from lng_datalake_client.Utils import setup_utils
from time import sleep

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


class TestCollectionOrchestration(unittest.TestCase):
    get_owner_loop_times = int(os.getenv('GET_OWNER_LOOP_TIMES', 5))
    get_collection_loop_times = int(os.getenv('GET_COLLECTION_LOOP_TIMES', 5))
    wait_time = int(os.getenv('WAIT_TIME', 5))

    default_asset_id = int(os.getenv('ASSET_ID', 62))
    owner_id = None
    owner_id2 = None
    api_key2 = None

    @classmethod
    def setUpClass(cls):  # NOSONAR
        logger.info("### Setting up tests for {} ###".format(cls.__name__))

        # This owner will only be used for negative tests for API key authorization
        owner_body_input = \
            {
                "owner-name": "regression-collection.1.{}".format(datetime.now().isoformat()),
                "email-distribution": ["regression-collection@lexisnexis.com"]
            }
        logger.info('Creating owner: {}'.format(owner_body_input))
        owner_response = setup_utils.setup_create_owner(owner_body_input, cls.get_owner_loop_times, cls.wait_time)
        cls.owner_id2 = owner_response['owner']['owner-id']
        cls.api_key2 = owner_response['owner']['api-key']

        logger.info("Successfully created owner-id {}.".format(cls.owner_id2))

        # This is the primary owner that will be used for tests
        # - must be created last so API key is associated with Helper functions
        owner_body_input = \
            {
                "owner-name": "regression-collection.2.{}".format(datetime.now().isoformat()),
                "email-distribution": ["regression-collection@lexisnexis.com"],
                "collection-prefixes": ['{0:X}'.format(round(time.time()))]
            }
        logger.info('Creating owner: {}'.format(owner_body_input))
        owner_response = setup_utils.setup_create_owner(owner_body_input, cls.get_owner_loop_times, cls.wait_time)
        cls.owner_id = owner_response['owner']['owner-id']
        cls.owner_collection_prefix = owner_response['owner']['collection-prefixes'][0]
        logger.info("Successfully created owner-id {}.".format(cls.owner_id))

        # Set Owner in environment variables
        os.environ['OWNER-ID'] = str(owner_response['owner']['owner-id'])
        os.environ['X-API-KEY'] = owner_response['owner']['api-key']

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        logger.info("### Cleaning up tests for {} ###".format(cls.__name__))

        logger.info("Deleting owner-id {}...".format(cls.owner_id))
        owner_request = {
            "owner-id": cls.owner_id
        }
        # TODO: Can't remove an owner has collections (even if they are all terminated)
        owner_response = remove_owner(owner_request)
        if owner_response.status_code != 202 and 'message' in owner_response.json():
            logger.error("Unable to delete owner-id {}. Message=[{}].".format(cls.owner_id,
                                                                              owner_response.json()['message']))

        logger.info("Deleting owner-id {}...".format(cls.owner_id2))
        owner_request = {
            "headers": {
                "Content-Type": "application/json",
                "x-api-key": cls.api_key2
            },
            "owner-id": cls.owner_id2
        }
        owner_response = remove_owner(owner_request)
        if owner_response.status_code != 202 and 'message' in owner_response.json():
            logger.error("Unable to delete owner-id {}. Message=[{}].".format(cls.owner_id2,
                                                                              owner_response.json()['message']))

    def test_orchestration(self):
        collection, collection_with_optionals = self.create_collections()
        self.get_collections(collection, collection_with_optionals)
        collection, collection_with_optionals = self.update_collections(collection, collection_with_optionals)
        self.list_collections(collection, collection_with_optionals)
        self.remove_collections(collection, collection_with_optionals)

    def create_collections(self):
        logger.info("### Running create collection tests ###")
        # must import other test cases inside function so the unittest framework doesn't find them and try to run them
        from Tests.Regression.regression_create_collection import TestCreateCollection

        test_create_collection = TestCreateCollection()
        input_dict = \
            {
                "collection-id": "{}_regression-no-opts-{}".format(self.owner_collection_prefix,
                                                           str(datetime.timestamp(datetime.now()))[:-7]),
                'asset-id': self.default_asset_id
            }
        # + Valid created collection to use in future tests.
        logger.info('Creating a collection with no optional attributes: {}'.format(input_dict))
        create_response = test_create_collection.test_create_collection_correct_input(input_dict)

        logger.info('Response from creating the collection: {}'.format(create_response))

        input_dict_with_optionals = \
            {
                "collection-id": "{}_regression-with-opts-{}".format(self.owner_collection_prefix,
                                                           str(datetime.timestamp(datetime.now()))[:-7]),
                "asset-id": self.default_asset_id,
                "old-object-versions-to-keep": 5,
                "description": "Collection with options and ' quote check",
                "classification-type": "Test",
                "object-expiration-hours": 24
            }

        logger.info('Creating a collection with optional attributes: {}'.format(input_dict_with_optionals))
        create_response_with_optionals = \
            test_create_collection.test_create_collection_correct_input(input_dict_with_optionals)

        logger.info('Created my collection with the optional attributes: {}'.format(create_response_with_optionals))

        self.validate_created_collection(create_response)
        logger.info("Successfully created collection with collection-id: {}".format(create_response["collection-id"]))

        self.validate_created_collection(create_response_with_optionals)
        logger.info("Successfully created collection with collection-id: {}".format(
            create_response_with_optionals["collection-id"]))

        # - Invalid create collection tests
        logger.info('Moving to invalid test cases for create collection...')
        test_create_collection.test_create_collection_invalid_collection_id_length()
        logger.info("Successfully tested create collection: Invalid collection id length")
        test_create_collection.test_create_collection_invalid_classification_type(input_dict)
        logger.info("Successfully tested create collection: Invalid classification type")

        create_invalid_asset_dict = \
            {
                'asset-id': -1
            }

        create_invalid_collection_id_dict = \
            {
                'asset-id': 62,
                'prefix': self.owner_collection_prefix
            }
        test_create_collection.test_create_collection_invalid_asset_id(create_invalid_asset_dict)
        logger.info("Successfully tested create collection: invalid asset id")
        create_invalid_asset_dict.pop('asset-id')
        test_create_collection.test_create_collection_missing_asset_id(create_invalid_asset_dict)
        logger.info("Successfully tested create collection: missing asset id")
        test_create_collection.test_create_collection__invalid_x_api_key()
        logger.info("Successfully tested create collection: invalid x-api-key")
        test_create_collection.test_create_collection_unsupported_media()
        logger.info("Successfully tested create collection: invalid content type")
        test_create_collection.test_create_collection_error_invalid_collection_id_bad_chars(input_dict)
        logger.info("Successfully tested create collection: invalid collection id string")
        test_create_collection.test_create_collection_error_multiple_underscores_in_collection_id(
            create_invalid_collection_id_dict)
        logger.info("Successfully tested create collection: invalid collection id string with multiple underscores")
        test_create_collection.test_create_collection_error_invalid_collection_id_numbers(input_dict)
        logger.info("Successfully tested create collection: invalid collection id only numbers")
        test_create_collection.test_create_collection_error_invalid_old_object_versions_to_keep(input_dict)
        logger.info("Successfully tested create collection: invalid old object versions to keep")
        test_create_collection.test_create_collection_invalid_collection_id_duplicate(input_dict_with_optionals)
        logger.info("Successfully tested create collection: duplicate collection id")

        create_invalid_collection_prefix_dict = \
            {
                "collection-id": "{0:X}_regression-{0}".format(round(time.time()),
                                                               str(datetime.timestamp(datetime.now()))[:-7]),
                "asset-id": self.default_asset_id

            }

        test_create_collection.test_create_collection_error_invalid_owner_prefix(
            create_invalid_collection_prefix_dict)
        logger.info("Successfully tested create collection: prefix is not registered for this owner")

        return create_response, create_response_with_optionals

    def validate_created_collection(self, create_response: dict) -> None:
        for i in range(0, self.get_collection_loop_times):
            sleep(self.wait_time)
            response = get_collection(create_response)
            if response.status_code == 200:
                break
            if i == self.get_collection_loop_times - 1:
                raise Exception("Collection {} not created; status={}".format(create_response, response.status_code))

    @staticmethod
    def get_collections(create_response, create_response_with_optionals):
        logger.info("### Running get collection tests ###")
        from Tests.Regression.regression_get_collection import TestGetCollection

        # + Valid get collection
        test_get_collection = TestGetCollection()
        test_get_collection.test_get_collection(create_response)
        test_get_collection.test_get_collection(create_response_with_optionals)
        logger.info('Successfully got the created collections {} and {}, '
                    'moving on to invalid test cases for get collection'.format(create_response['collection-id'],
                                                                                create_response_with_optionals[
                                                                                    'collection-id']))

        # - Invalid get collection tests
        test_get_collection.test_get_collection_invalid_collection_id()
        logger.info("Successfully tested get collection: invalid collection id")
        test_get_collection.test_get_collection_invalid_content_type()
        logger.info("Successfully tested get collection: invalid content type")
        test_get_collection.test_get_collection_invalid_x_api_key()
        logger.info("Successfully tested get collection: invalid x-api-key")

    def list_collections(self, *args):
        logger.info("### Running list collections tests ###")
        from Tests.Regression.regression_list_collections import TestListCollections
        test_list_collections = TestListCollections()

        collections = []
        for collection in args:
            collections.append(collection)

        # + Valid list collections
        test_list_collections.test_list_collections_all_results()
        logger.info("Successfully tested list collections: All results")
        test_list_collections.test_list_collection_by_owner_id_get_all(collections, self.owner_id)
        logger.info("Successfully tested list collections: All results by owner id")
        test_list_collections.test_list_collections_by_owner_id_with_pagination(collections, self.owner_id, 1)
        logger.info("Successfully tested list collections: Results by owner id and pagination")

        # - Invalid list collections tests
        test_list_collections.test_list_collections_invalid_owner_id(-100)
        logger.info("Successfully tested list collections: Invalid owner id")
        test_list_collections.test_list_collections_max_items_is_greater_than_1000()
        logger.info("Successfully tested list collections: Invalid max-items")
        test_list_collections.test_list_invalid_collections_pagination_diff_max_items()
        logger.info("Successfully tested list collections: Invalid diff max-items")
        test_list_collections.test_list_collections_invalid_x_api_key()
        logger.info("Successfully tested list collections: Invalid x-api-key")
        test_list_collections.test_list_collections_invalid_content_type()
        logger.info("Successfully tested list collections: Invalid content-key")

    def update_collections(self, create_response, create_response_with_optionals):
        logger.info("### Running update collection tests ###")
        from Tests.Regression.regression_update_collection import TestUpdateCollection
        from Tests.Regression.regression_get_collection import TestGetCollection

        # Change the collection name to allow update.
        create_response['asset-id'] = 1
        logger.info('Updating collection {} name to allow an update: {}'.format(create_response['collection-id'],
                                                                                create_response))

        # + Valid update collection test to use in the future.
        test_update_collection = TestUpdateCollection()
        create_response = test_update_collection.test_update_collection_asset_id(create_response)

        self.validate_updated_collection(create_response, 'asset-id', create_response['asset-id'])
        logger.info('Successfully updated the asset-id of collection: {}'.format(create_response["collection-id"]))

        create_response, create_response_with_optionals = \
            self.suspend_collections(create_response, create_response_with_optionals)

        logger.info('Running invalid suspend on suspended collection')

        test_update_collection.test_update_collection_suspend_in_suspended(create_response)

        logger.info('Resuming collections...')

        create_response = test_update_collection.test_update_collection_resume(create_response)

        create_response_with_optionals = \
            test_update_collection.test_update_collection_resume(create_response_with_optionals)

        self.validate_updated_collection(create_response, 'collection-state', "Created")
        logger.info('Successfully resumed the collection: {}'.format(create_response["collection-id"]))

        self.validate_updated_collection(create_response_with_optionals, 'collection-state', "Created")
        logger.info('Successfully resumed the collection with optionals: {}'.format(
            create_response_with_optionals["collection-id"]))

        # - Invalid update collection tests
        logger.info('Moving on to invalid test cases for update collection...')
        test_update_collection.test_update_collection_resume_non_suspended(create_response)
        logger.info("Successfully tested update collection: invalid resume of non suspended collection")
        update_invalid_collection_dict = \
            {
                'collection-id': -1,
                'owner-id': self.owner_id,
                'asset-id': self.default_asset_id
            }
        test_update_collection.test_update_collection_invalid_collection_id(update_invalid_collection_dict)
        logger.info("Successfully tested update collection: invalid collection id")

        update_invalid_asset_dict = \
            {
                'collection-id': create_response['collection-id'],
                'asset-id': -1
            }
        test_update_collection.test_update_collection_invalid_asset_id(update_invalid_asset_dict)
        logger.info("Successfully tested update collection: invalid asset id")
        test_update_collection.test_update_collection_invalid_content_type()
        logger.info("Successfully tested update collection: invalid content type")
        test_update_collection.test_update_collection_invalid_x_api_key()
        logger.info("Successfully tested update collection: invalid x-api-key")
        test_update_collection.test_update_collection_incorrect_x_api_key(create_response, self.api_key2)
        logger.info("Successfully tested update collection: incorrect x-api-key")
        test_update_collection.test_update_collection_without_data_change(create_response)
        logger.info("Successfully tested update collection: no data change")

        # + Valid get collection test to ensure that the collection was updated.
        test_get_collection = TestGetCollection()
        create_response = test_get_collection.test_get_collection(create_response)
        logger.info('Successfully got the response after update of collection: {}'.format(create_response))

        # + Set all newly created collections to suspended state for deletion
        logger.info('Setting collections {} and {} to suspended so they can be deleted.'
                    .format(create_response['collection-id'], create_response_with_optionals['collection-id']))

        create_response, create_response_with_optionals = \
            self.suspend_collections(create_response, create_response_with_optionals)

        logger.info("Got back suspended collections {} and {}".format(create_response['collection-id'],
                                                                      create_response_with_optionals['collection-id']))

        return create_response, create_response_with_optionals

    def validate_updated_collection(self, update_response: dict, prop: str, value: str) -> None:
        for i in range(0, self.get_collection_loop_times):
            sleep(self.wait_time)
            response = get_collection(update_response)
            if response.status_code == 200 and response.json()['collection'][prop] == value:
                break
            if i == self.get_collection_loop_times - 1:
                raise Exception("Collection {} not updated; status={}".format(update_response, response.status_code))

    def suspend_collections(self, create_response, create_response_with_optionals):
        # must import other test cases inside function so the unittest framework doesn't find them and try to run them
        from Tests.Regression.regression_update_collection import TestUpdateCollection
        test_update_collection = TestUpdateCollection()

        create_response = test_update_collection.test_update_collection_begin_suspend(create_response)

        create_response_with_optionals['collection-state'] = "Suspended"
        create_response_with_optionals = \
            test_update_collection.test_update_collection_begin_suspend(create_response_with_optionals)

        self.validate_updated_collection(create_response, 'collection-state', "Suspended")
        logger.info('Successfully suspended the collection: {}'.format(create_response['collection-id']))

        self.validate_updated_collection(create_response_with_optionals, 'collection-state', "Suspended")
        logger.info('Successfully suspended the collection with optionals: {}'.format(
            create_response_with_optionals['collection-id']))

        return create_response, create_response_with_optionals

    def remove_collections(self, create_response, create_response_with_optionals):
        logger.info("### Running remove collection tests ###")
        # must import other test cases inside function so the unittest framework doesn't find them and try to run them
        from Tests.Regression.regression_remove_collection import TestRemoveCollection
        from Tests.Regression.regression_update_collection import TestUpdateCollection

        test_update_collection = TestUpdateCollection()
        test_remove_collection = TestRemoveCollection()

        # removing collection create_response
        logger.info('Removing collection {}: {}'.format(create_response['collection-id'],
                                                        create_response))
        test_remove_collection.test_remove_collection(create_response)

        logger.info('Running remove invalid collection')
        test_remove_collection.test_remove_collection_invalid_collection_id()

        # removing collection create_response_with_optionals
        logger.info('Removing collection {}: {}'.format(create_response_with_optionals['collection-id'],
                                                        create_response_with_optionals))
        test_remove_collection.test_remove_collection(create_response_with_optionals)

        for i in range(0, self.get_collection_loop_times):
            sleep(self.wait_time)
            response = get_collection(create_response)
            if response.status_code == 200 \
                    and response.json()['collection']['collection-state'] == "Terminated":
                break
            if i == self.get_collection_loop_times - 1:
                raise Exception(
                    "Collection without optionals was not terminated; status={}".format(response.status_code))
        test_update_collection.test_update_collection_terminated_status(create_response)
        test_remove_collection.test_remove_collection_invalid_collection_state(create_response)

        for i in range(0, self.get_collection_loop_times):
            sleep(self.wait_time)
            response = get_collection(create_response)
            if response.status_code == 200 \
                    and response.json()['collection']['collection-state'] == "Terminated":
                break
            if i == self.get_collection_loop_times - 1:
                raise Exception("Collection with optionals was not terminated; status={}".format(response.status_code))

        create_response = get_collection(create_response).json()['collection']
        create_response_with_optionals = get_collection(create_response_with_optionals).json()['collection']

        logger.info("Successfully terminated collections: {} and {}".format(create_response['collection-id'],
                                                                            create_response_with_optionals[
                                                                                'collection-id']))

        # - Invalid update collection tests
        logger.info('Moving on to invalid test cases for remove collection...')
        test_remove_collection.test_remove_collection_invalid_content_type()
        logger.info("Successfully tested remove collection: invalid content type")
        test_remove_collection.test_remove_collection_invalid_x_api_key()
        logger.info("Successfully tested remove collection: invalid x-api-key")
        test_remove_collection.test_remove_collection_incorrect_x_api_key(create_response, self.api_key2)
        logger.info("Successfully tested remove collection: incorrect x-api-key")

        return create_response, create_response_with_optionals


if __name__ == '__main__':
    unittest.main()
