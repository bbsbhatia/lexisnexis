import os
import unittest
from importlib import reload
from unittest.mock import patch, call

import lng_datalake_commons.publish_sns_topic as publish_sns_topic_module
from botocore.exceptions import ClientError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import update_collection_event_handler

__author__ = "John Konderla, Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"
io_util = IOUtils(__file__, 'UpdateCollectionEventHandler')
topic_arn = 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'


class TestUpdateCollectionEventHandler(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {
        'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': topic_arn,
        'LAMBDA_TASK_ROOT': os.path.dirname(io_util.schema_path)})
    def setUpClass(cls):  # NOSONAR
        # reload the update_collection_event_handler so that when it is instantiated my arn is picked up
        # from the os.environ
        reload(update_collection_event_handler)
        reload(publish_sns_topic_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()
        reload(update_collection_event_handler)

    # +lambda_handler - update asset id
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler(self, mock_sns, aws_session_mock, mocked_get_client, mock_helper):
        mocked_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('lng.dynamodb.tracking_table.respond.json'),
                                             io_util.load_data_json('lng.dynamodb.mapping_table.respond.json')]
        mocked_get_client.return_value.put_item.return_value = None
        original_collection = io_util.load_data_json('lng.dynamodb.get_collection_response.json')
        mocked_get_client.return_value.get_item.return_value = original_collection
        mock_sns.return_value.publish.return_value = io_util.load_data_json('successful_publish_response.json')
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        request_input = io_util.load_data_json("sns_event.json")
        self.assertEqual(update_collection_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

        # validate that one message for each schema version was sent and one for the concatenation
        # in the following order: v0, v1, v0v1
        calls = []
        for file in ["notification_v0.json", "notification_v1.json", "notification_v0v1.json"]:
            notification = io_util.load_data_json(file)
            calls.append(call(TargetArn=topic_arn, Message=notification["message"], MessageStructure="json",
                              MessageAttributes=notification["message_attributes"]))
        mock_sns.return_value.publish.assert_has_calls(calls, any_order=False)

        # validate the asset-id was the only property updated in the collection
        updated_collection = original_collection["Item"]
        updated_collection["AssetID"]["N"] = '62'
        mocked_get_client.return_value.put_item.assert_called_with(Item=updated_collection, TableName=None)

    # +lambda_handler - suspend collection
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_suspend_collection(self, mock_sns, aws_session_mock, mocked_get_client, mock_helper,
                                               mock_timestamp):
        mocked_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('lng.dynamodb.tracking_table.respond.json'),
                                             io_util.load_data_json('lng.dynamodb.mapping_table.respond.json')]
        mocked_get_client.return_value.put_item.return_value = None
        original_collection = io_util.load_data_json('lng.dynamodb.get_collection_response.json')
        mocked_get_client.return_value.get_item.return_value = original_collection
        mock_sns.return_value.publish.return_value = io_util.load_data_json('successful_publish_response.json')
        mock_helper.return_value = ['us-east-1']
        mock_timestamp.return_value = "2019-04-10T12:00:00.000Z"
        aws_session_mock.return_value = None
        request_input = io_util.load_data_json("suspend_valid_event.json")
        self.assertEqual(update_collection_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

        # validate the state was the only property updated in the collection
        updated_collection = original_collection["Item"]
        updated_collection["CollectionState"]["S"] = 'Suspended'
        updated_collection["EventIds"] = {'L': [{'S': 'xyz'}]}
        updated_collection["dl:LastUpdated"] = {'S': '2019-04-10T12:00:00.000Z'}
        mocked_get_client.return_value.put_item.assert_called_with(Item=updated_collection, TableName=None)
        mock_sns.return_value.publish.assert_called()

    # +lambda_handler - resume collection
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_resume_collection(self, mock_sns, aws_session_mock, mocked_get_client, mock_helper,
                                              mock_timestamp):
        mocked_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('lng.dynamodb.tracking_table.respond.json'),
                                             io_util.load_data_json('lng.dynamodb.mapping_table.respond.json')]
        mocked_get_client.return_value.put_item.return_value = None
        original_collection = io_util.load_data_json('lng.dynamodb.get_collection_suspended_response.json')
        mocked_get_client.return_value.get_item.return_value = original_collection
        mock_sns.return_value.publish.return_value = io_util.load_data_json('successful_publish_response.json')
        mock_helper.return_value = ['us-east-1']
        mock_timestamp.return_value = "2019-04-10T12:00:00.000Z"
        aws_session_mock.return_value = None
        request_input = io_util.load_data_json("resume_valid_event.json")
        self.assertEqual(update_collection_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

        # validate the state was the only property updated in the collection
        updated_collection = original_collection["Item"]
        updated_collection["CollectionState"]["S"] = 'Created'
        updated_collection["EventIds"] = {'L': [{'S': 'xyz'}]}
        updated_collection["dl:LastUpdated"] = {'S': '2019-04-10T12:00:00.000Z'}
        mocked_get_client.return_value.put_item.assert_called_with(Item=updated_collection, TableName=None)
        mock_sns.return_value.publish.assert_called()

    # -lambda_handler - suspend collection in terminated state
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_suspend_collection_terminated(self, mock_sns, aws_session_mock, mocked_get_client,
                                                          mock_helper):
        mocked_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('lng.dynamodb.mapping_table.respond.json')
        mocked_get_client.return_value.put_item.return_value = None
        original_collection = io_util.load_data_json('lng.dynamodb.get_collection_terminated_response.json')
        mocked_get_client.return_value.get_item.return_value = original_collection
        mock_sns.return_value.publish.return_value = io_util.load_data_json('successful_publish_response.json')
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        request_input = io_util.load_data_json("suspend_valid_event.json")
        with self.assertRaisesRegex(TerminalErrorException,
                                    "state cannot be changed from Terminated to Suspended"):
            update_collection_event_handler.update_collection_event_handler(request_input,
                                                                            MockLambdaContext().invoked_function_arn)
        mock_sns.return_value.publish.assert_not_called()
        mocked_get_client.return_value.put_item.assert_not_called()

    # -lambda_handler - update asset id - failed publishing the second message -
    # must retry the event to publish 2nd (v1) and 3rd (v0v1) notifications
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler(self, mock_sns, aws_session_mock, mocked_get_client, mock_helper, mock_timestamp):
        mocked_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('lng.dynamodb.mapping_table.respond.json')
        mocked_get_client.return_value.put_item.return_value = None
        original_collection = io_util.load_data_json('lng.dynamodb.get_collection_response.json')
        mocked_get_client.return_value.get_item.return_value = original_collection
        mock_sns.return_value.publish.side_effect = \
            [io_util.load_data_json('successful_publish_response.json'),
             ClientError(
                 {'ResponseMetadata': {}, 'Error': {'Code': 'ThrottlingException', 'Message': 'This is a mock'}},
                 "FAKE")]
        mock_helper.return_value = ['us-east-1']
        mock_timestamp.return_value = "2019-04-10T12:00:00.000Z"
        aws_session_mock.return_value = None
        request_input = io_util.load_data_json("sns_event.json")
        with self.assertRaisesRegex(ClientError, "ThrottlingException"):
            update_collection_event_handler.update_collection_event_handler(request_input,
                                                                            MockLambdaContext().invoked_function_arn)

        # validate a message for schema versions v0 and v1 was sent, but not for v0v1
        # because a ClientError was raised for v1
        calls = []
        for file in ["notification_v0.json", "notification_v1.json"]:
            notification = io_util.load_data_json(file)
            calls.append(call(TargetArn=topic_arn, Message=notification["message"], MessageStructure="json",
                              MessageAttributes=notification["message_attributes"]))
        mock_sns.return_value.publish.assert_has_calls(calls, any_order=False)

        # validate the asset-id was the only property updated in the collection
        updated_collection = original_collection["Item"]
        updated_collection["AssetID"]["N"] = '62'
        updated_collection['EventIds'] = {'L': [{'S': 'xyz'}]}
        updated_collection["dl:LastUpdated"] = {'S': '2019-04-10T12:00:00.000Z'}
        mocked_get_client.return_value.put_item.assert_called_with(Item=updated_collection, TableName=None)


if __name__ == '__main__':
    unittest.main()
