import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import list_collections_command as list_collections_command_module
from list_collections_command import lambda_handler

__author__ = "Akeem Bolarinwa"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListCollections')


# need to override globals in list_collection_command.py  module
class TestListCollectionsLambda(unittest.TestCase):
    @classmethod
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_OWNER_ID_INDEX': "collection-by-owner-index",
                             'COLLECTION_DYNAMODB_STATE_AND_OWNER_ID_INDEX': "collection-state-ownerid-index",
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    def setUpClass(cls):  # NOSONAR
        reload(list_collections_command_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + list_collection_command without owner id or show-terminated-only - valid
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collection_get_all_items(self,
                                           session_mock,
                                           mock_dynamodb_get_client):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.\
            build_full_result.return_value = io_util.load_data_json('dynamodb.response.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request_input = io_util.load_data_json('apigateway.request.accepted_get_all_items.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + list_collection_command with owner id - valid
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collection_show_owner_id(self,
                                           session_mock,
                                           mock_dynamodb_get_client):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'lng.dynamodb.owner_table.get_item.valid_response.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.\
            return_value = io_util.load_data_json('dynamodb.response.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request_input = io_util.load_data_json('apigateway.request.accepted_owner_id.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + list_collection_command with owner id and show-terminated - valid
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collection_command_owner_id_terminated(self,
                                                         session_mock,
                                                         mock_dynamodb_get_client):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'lng.dynamodb.owner_table.get_item.valid_response.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.\
            return_value = io_util.load_data_json('dynamodb.response.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request_input = io_util.load_data_json('apigateway.request.accepted_show_owner_id_terminated_only.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + list_collection_command with show terminated only - valid
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collection_show_terminated_only(self,
                                                  session_mock,
                                                  mock_dynamodb_get_client):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.\
            return_value = io_util.load_data_json('dynamodb.response.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request_input = io_util.load_data_json('apigateway.request.accepted_show_terminated_only.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                                 response_output)


if __name__ == '__main__':
    unittest.main()
