import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from update_collection_command import lambda_handler

__author__ = "Maen Nanaa, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.1"

io_util = IOUtils(__file__, 'UpdateCollection')


class TestUpdateCollectionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + Test for Lambda Handler
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_update_collection_command(self,
                                       session_mock,
                                       mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('lng.dynamodb.owner_table.get_item.valid_response.json')
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('lng.dynamodb.collection_table.get_name_change.valid_response.json')
            return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.put.valid_response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.request_asset_change.json')
        expected_response = io_util.load_data_json('apigateway.response_asset_change_valid.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)

    # + Test for Lambda Handler to resume a collection with has-change-sets == true
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_update_collection_command_resume(self, session_mock, mock_dynamodb_get_client):
        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('lng.dynamodb.owner_table.get_item.valid_response.json')
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('lng.dynamodb.collection_table.get_resume.valid_response.json')
            return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.put.valid_response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.request_resume.json')
        expected_response = io_util.load_data_json('apigateway.response_resume_valid.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)

    # + Test for Lambda Handler to resume a collection with has-change-sets == true
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_update_collection_command_suspend(self, session_mock, mock_dynamodb_get_client):
        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('lng.dynamodb.owner_table.get_item.valid_response.json')
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('lng.dynamodb.collection_table.get_suspend.valid_response.json')
            return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.put.valid_response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.request_suspend.json')
        expected_response = io_util.load_data_json('apigateway.response_suspend_valid.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)


if __name__ == '__main__':
    unittest.main()
