import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_collection_command import lambda_handler

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateCollection')


class TestCreateCollection(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # Test for Lambda Handler
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_collection_command_handler_success(self,
                                                       session_mock,
                                                       mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamo.owner_data_valid.json')
            elif TableName == 'fake_collection_table':
                return io_util.load_data_json('dynmo.collection_data.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.owner_data_valid.json'), {}]
        mock_dynamodb_get_client.return_value.put_item.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)

    # Test for Lambda Handler when collection id provided and owner has prefix set
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_collection_command_handler_success_1(self,
                                                         session_mock,
                                                         mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamo.owner_data_valid1.json')
            elif TableName == 'fake_collection_table':
                return io_util.load_data_json('dynmo.collection_data.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.owner_data_valid1.json'), {}]

        mock_dynamodb_get_client.return_value.put_item.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.input_valid1.json')
        expected_response = io_util.load_data_json('apigateway.expected_response1.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)

    # Test for Lambda Handler - classification-type: Test
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_collection_command_handler_success_2(self,
                                                         session_mock,
                                                         mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamo.owner_data_valid.json')
            elif TableName == 'fake_collection_table':
                return io_util.load_data_json('dynmo.collection_data.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.owner_data_valid.json'), {}]
        mock_dynamodb_get_client.return_value.put_item.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.input_valid_2.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_2.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)

    # Test for Lambda Handler - classification-type: Test and object-expiration: 20
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_collection_command_handler_success_3(self,
                                                         session_mock,
                                                         mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamo.owner_data_valid.json')
            elif TableName == 'fake_collection_table':
                return io_util.load_data_json('dynmo.collection_data.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.owner_data_valid.json'), {}]
        mock_dynamodb_get_client.return_value.put_item.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.input_valid_3.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_3.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(input_data, MockLambdaContext), expected_response)


if __name__ == '__main__':
    unittest.main()
