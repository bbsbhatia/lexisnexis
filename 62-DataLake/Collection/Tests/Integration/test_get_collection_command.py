import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_collection_command import lambda_handler

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetCollection')


class TestGetCollectionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +lambda_handler - valid
    @patch.dict(os.environ, {'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'CATALOG_COLLECTION_MAPPING_TABLE': 'fake_mapping_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_command_decorator_success(self, session_mock, mocked_get_client):
        session_mock.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mocked_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'lng.dynamodb.valid_response.json')
        mocked_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = io_util.load_data_json(
            'lng.dynamodb.mapping_table.valid_response.json')

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_valid.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()), expected_response)


if __name__ == '__main__':
    unittest.main()
