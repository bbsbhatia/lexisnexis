import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import remove_collection_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveCollectionCommand')


class TestRemoveCollectionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # Successful test using lambda_handler with command_wrapper decorator
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success(self, session_mock, mock_dynamodb_get_client):
        reload(remove_collection_command)

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('lng.dynamodb.owner_table.get_item.valid_response.json')
            elif TableName == 'fake_collection_table':
                return io_util.load_data_json('lng.dynamodb.collection_table.get_item.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(remove_collection_command.lambda_handler(request_input,
                                                                          mock_lambda_context.MockLambdaContext()),
                                 response_output)


if __name__ == '__main__':
    unittest.main()
