import os
import unittest
from importlib import reload
from unittest.mock import patch, call

import lng_datalake_commons.publish_sns_topic as publish_sns_topic_module
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import remove_collection_event_handler

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveCollectionEventHandler')
topic_arn = 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'


class TestRemoveCollectionEventHandler(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': topic_arn,
                             'REMOVE_COLLECTION_STATE_MACHINE_ARN': 'arn:aws:states:us-east-1:288044017584:'
                                                                    'stateMachine:Jon_Deletion_State_Machine',
                             'LAMBDA_TASK_ROOT': os.path.dirname(io_util.schema_path)})
    def setUpClass(cls):  # NOSONAR
        reload(remove_collection_event_handler)
        reload(publish_sns_topic_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # Integration test for the lambda_handler
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.stepfunctions.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler(self, mocked_dynamodb_client, mocked_state_machine_start, aws_session_mock, mock_sns_client,
                            mock_helper, mock_timestamp):
        mocked_state_machine_start.return_value.start_execution.return_value = io_util.load_data_json(
            "state_machine_start_execution_response.json")
        request_input = io_util.load_data_json("valid_event.json")
        original_collection = io_util.load_data_json("get_collection_result.json")
        mocked_dynamodb_client.return_value.get_item.return_value = original_collection
        mocked_dynamodb_client.return_value.put_item.return_value = io_util.load_data_json("put_valid_response.json")
        aws_session_mock.return_value = None
        mock_sns_client.return_value.publish.return_value = io_util.load_data_json("successful_publish_response.json")
        mock_helper.return_value = ['us-east-1']
        mock_timestamp.return_value = "2019-04-10T12:00:00.000Z"
        mocked_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('lng.dynamodb.tracking_table.respond.json'),
                                             io_util.load_data_json('lng.dynamodb.mapping_table.respond.json')]

        self.assertEqual(remove_collection_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

        # validate that one message for each schema version was sent and one for the concatenation
        # in the following order: v0, v1, v0v1
        calls = []
        for file in ["notification_v0.json", "notification_v1.json", "notification_v0v1.json"]:
            notification = io_util.load_data_json(file)
            calls.append(call(TargetArn=topic_arn, Message=notification["message"], MessageStructure="json",
                              MessageAttributes=notification["message_attributes"]))
        mock_sns_client.return_value.publish.assert_has_calls(calls, any_order=False)

        # validate the asset-id was the only property updated in the collection
        updated_collection = original_collection["Item"]
        updated_collection["CollectionState"]["S"] = 'Terminating'
        updated_collection["EventIds"] = {'L': [{'S': 'PVOuprQJLZGIKiBa'}]}
        updated_collection["dl:LastUpdated"] = {'S': '2019-04-10T12:00:00.000Z'}
        mocked_dynamodb_client.return_value.put_item.assert_called_with(Item=updated_collection, TableName=None)


if __name__ == '__main__':
    unittest.main()
