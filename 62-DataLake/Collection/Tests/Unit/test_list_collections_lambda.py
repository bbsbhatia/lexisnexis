import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, SemanticError, InvalidRequestPropertyValue, \
    InvalidRequestPropertyName
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import list_collections_command as list_collections_command_module
from list_collections_command import list_collections_command, lambda_handler

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListCollectionsCommand')

# need to override globals in list_collection_command.py module
list_collections_command_module.ct_owner_index = "collection-by-owner-index"
list_collections_command_module.ct_state_and_owner_index = "collection-state-ownerid-index"


class TestListCollections(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # Schema validation error using lambda_handler with command_wrapper decorator
    # - invalid value (non-Boolean) for show-terminated in request
    @patch('lng_aws_clients.session.set_session')
    def test_command_decorator_fail1(self, session_mock):
        session_mock.return_value = None

        request_input = io_util.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, mock_lambda_context.MockLambdaContext())

    # Schema validation error using lambda_handler with command_wrapper decorator
    # - request has max-items property that is too large
    @patch('lng_aws_clients.session.set_session')
    def test_command_decorator_fail2(self, session_mock):
        session_mock.return_value = None

        request_input = io_util.load_data_json('apigateway.request.failure_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyValue):
                lambda_handler(request_input, mock_lambda_context.MockLambdaContext())

    # Successful test of Lambda with max-items
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_pagination_token')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_all_items')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_success(self, session_mock, ct_get_all_items_mock,
                                      ct_pagination_token_mock):
        session_mock.return_value = None

        ct_get_all_items_mock.return_value = \
            [
                {
                    "collection-id": "251",
                    "collection-state": "Created",
                    "collection-timestamp": "2018-01-23T15:41:43.573Z",
                    "owner-id": 100,
                    "asset-id": 62,
                    "old-object-versions-to-keep": 0,
                    "classification-type": "Content"
                },
                {
                    "collection-id": "3389",
                    "collection-state": "Created",
                    "collection-timestamp": "2018-01-24T12:26:16.725Z",
                    "owner-id": 100,
                    "asset-id": 62,
                    "old-object-versions-to-keep": 0,
                    "classification-type": "Test",
                    "object-expiration-hours": 20
                }
            ]

        ct_pagination_token_mock.return_value = "abcd"

        request_input = \
            {
                "max-items": 2
            }

        expected_response_output = \
            {
                "collections":
                    [
                        {
                            "collection-id": "251",
                            "collection-url": "/collections/LATEST/251",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-23T15:41:43.573Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Content"
                        },
                        {
                            "collection-id": "3389",
                            "collection-url": "/collections/LATEST/3389",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-24T12:26:16.725Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Test",
                            "object-expiration-hours": 20
                        }
                    ],
                'item-count': 2,
                "next-token": "eyJtYXgtaXRlbXMiOjIsInBhZ2luYXRpb24tdG9rZW4iOiJhYmNkIn0="
            }

        self.assertEqual(list_collections_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Successful test of Lambda with max-items
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_pagination_token')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_all_items')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_success1(self, session_mock, ct_get_all_items_mock,
                                       ct_pagination_token_mock):
        session_mock.return_value = None

        ct_get_all_items_mock.return_value = \
            [
                {
                    "collection-id": "251",
                    "collection-state": "Created",
                    "collection-timestamp": "2018-01-23T15:41:43.573Z",
                    "owner-id": 100,
                    "asset-id": 62,
                    "old-object-versions-to-keep": 0,
                    "classification-type": "Content"
                },
                {
                    "collection-id": "3389",
                    "collection-state": "Created",
                    "collection-timestamp": "2018-01-24T12:26:16.725Z",
                    "owner-id": 100,
                    "asset-id": 62,
                    "old-object-versions-to-keep": 0,
                    "classification-type": "Test",
                    "object-expiration-hours": 20
                }
            ]

        ct_pagination_token_mock.return_value = "abcde"

        request_input = \
            {
                "max-items": 2,
                "next-token": "eyJtYXgtaXRlbXMiOiAyLCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkIn0="
            }

        expected_response_output = \
            {
                "collections":
                    [
                        {
                            "collection-id": "251",
                            "collection-url": "/collections/LATEST/251",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-23T15:41:43.573Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Content"
                        },
                        {
                            "collection-id": "3389",
                            "collection-url": "/collections/LATEST/3389",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-24T12:26:16.725Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Test",
                            "object-expiration-hours": 20
                        }
                    ],
                'item-count': 2,
                "next-token": "eyJtYXgtaXRlbXMiOjIsInBhZ2luYXRpb24tdG9rZW4iOiJhYmNkZSJ9"
            }

        self.assertEqual(list_collections_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Successful test of Lambda for query by owner-id
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_pagination_token')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_success2(self, session_mock, ot_get_item_mock, ct_query_items_mock,
                                       ct_pagination_token_mock):
        session_mock.return_value = None
        ct_pagination_token_mock.return_value = None

        ot_get_item_mock.return_value = io_util.load_data_json("owner_data.json")

        ct_query_items_mock.return_value = io_util.load_data_json("collection_data.json")

        request_input = \
            {
                "owner-id": 100
            }

        expected_response_output = \
            {
                "collections":
                    [
                        {
                            "collection-id": "251",
                            "collection-url": "/collections/LATEST/251",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-23T15:41:43.573Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Content"
                        },
                        {
                            "collection-id": "3389",
                            "collection-url": "/collections/LATEST/3389",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-24T12:26:16.725Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Test",
                            "object-expiration-hours": 20
                        }
                    ],
                'item-count': 2
            }

        self.assertEqual(list_collections_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Successful test of Lambda for query by owner-id and show-terminated-only
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_pagination_token')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_success3(self, session_mock, ot_get_item_mock, ct_query_items_mock,
                                       ct_pagination_token_mock):
        session_mock.return_value = None
        ct_pagination_token_mock.return_value = None

        ot_get_item_mock.return_value = io_util.load_data_json("owner_data.json")

        ct_query_items_mock.return_value = io_util.load_data_json("collection_data-terminated.json")

        request_input = \
            {
                "owner-id": 100,
                "show-terminated-only": True
            }

        expected_response_output = \
            {
                "collections":
                    [
                        {
                            "collection-id": "251",
                            "collection-url": "/collections/LATEST/251",
                            "collection-state": "Terminated",
                            "collection-timestamp": "2018-01-23T15:41:43.573Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Content"
                        },
                        {
                            "collection-id": "3389",
                            "collection-url": "/collections/LATEST/3389",
                            "collection-state": "Terminated",
                            "collection-timestamp": "2018-01-24T12:26:16.725Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Test",
                            "object-expiration-hours": 20
                        }
                    ],
                'item-count': 2
            }

        self.assertEqual(list_collections_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Successful test of Lambda for using scan filter for not show-terminated
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_pagination_token')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_all_items')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_success4(self, session_mock, ct_get_all_items_mock, ct_pagination_token_mock):
        session_mock.return_value = None
        ct_pagination_token_mock.return_value = None

        ct_get_all_items_mock.return_value = io_util.load_data_json("collection_data.json")

        request_input = \
            {
                "show-terminated": False
            }

        expected_response_output = \
            {
                "collections":
                    [
                        {
                            "collection-id": "251",
                            "collection-url": "/collections/LATEST/251",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-23T15:41:43.573Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Content"
                        },
                        {
                            "collection-id": "3389",
                            "collection-url": "/collections/LATEST/3389",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-24T12:26:16.725Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Test",
                            "object-expiration-hours": 20
                        }
                    ],
                'item-count': 2
            }

        self.assertEqual(list_collections_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Successful test of Lambda for query by owner-id and filter for not show-terminated
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_pagination_token')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_success5(self, session_mock, ot_get_item_mock, ct_query_items_mock,
                                       ct_get_pagination_token_mock):
        session_mock.return_value = None
        ct_get_pagination_token_mock.return_value = None

        ot_get_item_mock.return_value = io_util.load_data_json("owner_data.json")

        ct_query_items_mock.return_value = io_util.load_data_json("collection_data.json")

        request_input = \
            {
                "owner-id": 100,
                "show-terminated": False
            }

        expected_response_output = \
            {
                "collections":
                    [
                        {
                            "collection-id": "251",
                            "collection-url": "/collections/LATEST/251",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-23T15:41:43.573Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Content"
                        },
                        {
                            "collection-id": "3389",
                            "collection-url": "/collections/LATEST/3389",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-24T12:26:16.725Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Test",
                            "object-expiration-hours": 20
                        }
                    ],
                'item-count': 2
            }

        self.assertEqual(list_collections_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Successful test of Lambda for query by owner-id with optional description on 1 collection
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_pagination_token')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_success6(self, session_mock, ot_get_item_mock, ct_query_items_mock,
                                       ct_pagination_token_mock):
        session_mock.return_value = None
        ct_pagination_token_mock.return_value = None

        ot_get_item_mock.return_value = io_util.load_data_json("owner_data.json")

        ct_query_items_mock.return_value = io_util.load_data_json("collection_data-opt.json")

        request_input = \
            {
                "owner-id": 100
            }

        expected_response_output = \
            {
                "collections":
                    [
                        {
                            "collection-id": "251",
                            "collection-url": "/collections/LATEST/251",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-23T15:41:43.573Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "description": "Description",
                            "classification-type": "Content"
                        },
                        {
                            "collection-id": "3389",
                            "collection-url": "/collections/LATEST/3389",
                            "collection-state": "Created",
                            "collection-timestamp": "2018-01-24T12:26:16.725Z",
                            "owner-id": 100,
                            "asset-id": 62,
                            "old-object-versions-to-keep": 0,
                            "classification-type": "Test",
                            "object-expiration-hours": 20
                        }
                    ],
                'item-count': 2
            }
        self.assertEqual(list_collections_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Fail test of Lambda for terminated parameters that don't make sense
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_fail1(self, session_mock):
        session_mock.return_value = None

        request_input = \
            {
                "show-terminated": False,
                "show-terminated-only": True
            }

        with self.assertRaisesRegex(SemanticError, "The show-terminated parameter cannot be false if the "
                                                   "show-terminated-only parameter is true.*"):
            list_collections_command(request_input, "LATEST")


    # Fail test of Lambda for exceptions when scanning CollectionTable
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_all_items')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_fail98(self, session_mock, ct_get_all_items_mock):
        session_mock.return_value = None
        ct_get_all_items_mock.side_effect = ClientError({}, "dummy")

        request_input = {}

        with self.assertRaisesRegex(InternalError, "Unable to get items from Collection Table"):
            list_collections_command(request_input, "LATEST")

        ct_get_all_items_mock.side_effect = Exception

        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            list_collections_command(request_input, "LATEST")

    # Fail test of Lambda for exceptions when querying CollectionTable
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_fail99(self, session_mock, ct_query_items_mock):
        session_mock.return_value = None
        ct_query_items_mock.side_effect = ClientError({}, "dummy")

        request_input = \
            {
                "show-terminated-only": True
            }

        with self.assertRaisesRegex(InternalError, "Unable to query items from Collection Table"):
            list_collections_command(request_input, "LATEST")

        ct_query_items_mock.side_effect = Exception()

        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            list_collections_command(request_input, "LATEST")

    # -Failure test of Lambda for invalid max items
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_command_fail_invalid_max_results(self, session_mock):
        session_mock.return_value = None

        request_input = \
            {
                "collection-id": '274',
                "max-items": 1001

            }

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid max-items value:"):
            list_collections_command(request_input, "LATEST")

    # -Failure test of Lambda for invalid max results
    @patch('lng_aws_clients.session.set_session')
    def test_list_collections_command_fail_invalid_max_results_2(self, session_mock):
        session_mock.return_value = None
        request_input = \
            {
                "collection-id": '274',
                "max-items": 1,
                "next-token": "eyJtYXgtaXRlbXMiOiAzLCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkIn0=",
            }
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Continuation request must pass the same max-items as initial request"):
            list_collections_command(request_input, "LATEST")


if __name__ == '__main__':
    unittest.main()
