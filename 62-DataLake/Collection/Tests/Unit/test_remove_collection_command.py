import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NoSuchCollection, ForbiddenStateTransitionError, \
    NotAuthorizedError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from remove_collection_command import remove_collection_command, lambda_handler, generate_response_json

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveCollectionCommand')


class TestRemoveCollectionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_command_decorator_success(self, session_mock, ct_get_item_mock, mock_owner_authorization,
                                       est_put_item_mock, tt_put_item_mock):
        session_mock.return_value = None
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": "Suspended",
                "collection-timestamp": "2017-12-15T14:10:04.453Z",
                "owner-id": 101,
                "asset-id": 62,
                "old-object-versions-to-keep": 0,
                "description": "My first collection",
                "classification-type": "Content"
            }
        mock_owner_authorization.return_value = {}
        est_put_item_mock.return_value = None
        tt_put_item_mock.return_value = None

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                             response_output)

    # -Failure Lambda for Collection in wrong state
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_collection_failure1(self, session_mock, ct_get_item_mock, mock_owner_authorization,
                                        est_put_item_mock):
        session_mock.return_value = None
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": "Created",
                "collection-timestamp": "2017-12-15T14:10:04.453Z",
                "owner-id": 101,
                "asset-id": 62,
                "old-object-versions-to-keep": 0,
                "description": "My first collection",
                "classification-type": "Content"
            }
        mock_owner_authorization.return_value = {}

        est_put_item_mock.return_value = None

        request_input = \
            {
                "collection-id": "274"
            }

        with self.assertRaises(ForbiddenStateTransitionError):
            remove_collection_command(request_input, "abcde", "LATEST", "testApiKeyId")

    # -Failure Lambda for Collection does not exist
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_collection_failure2(self, session_mock, ct_get_item_mock, mock_owner_authorization,
                                        est_put_item_mock):
        session_mock.return_value = None
        ct_get_item_mock.return_value = {}
        mock_owner_authorization.return_value = {}
        est_put_item_mock.return_value = None

        request_input = \
            {
                "collection-id": "274"
            }

        with self.assertRaises(NoSuchCollection):
            remove_collection_command(request_input, "abcde", "LATEST", "testApiKeyId")

    # -Failure Lambda for ClientError when getting collection
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_collection_failure3(self, session_mock, ct_get_item_mock, mock_owner_authorization,
                                        est_put_item_mock):
        session_mock.return_value = None
        ct_get_item_mock.side_effect = ClientError({}, "dummy")
        mock_owner_authorization.return_value = {}
        est_put_item_mock.return_value = None

        request_input = \
            {
                "collection-id": "274"
            }

        with self.assertRaisesRegex(InternalError, "Unable to get item from Collection Table"):
            remove_collection_command(request_input, "abcde", "LATEST", "testApiKeyId")

    # -Failed test of Lambda - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_collection_command_failed_owner_auth(self, session_mock, ct_get_item_mock,
                                                         mock_owner_authorization):
        session_mock.return_value = None
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": "Created",
                "collection-timestamp": "2017-12-15T14:10:04.453Z",
                "owner-id": 101,
                "asset-id": 62,
                "old-object-versions-to-keep": 0,
                "description": "My first collection",
                "classification-type": "Content"
            }
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")

        request_input = \
            {
                "collection-id": "274"
            }

        with self.assertRaisesRegex(NotAuthorizedError,
                                    "NotAuthorizedError||API Key provided is not valid for Owner ID.*"):
            remove_collection_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # - generate_response_json - Missing required
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_missing_required(self, required_mock, opt_mock):
        required_mock.return_value = ['collection-timestamp']
        opt_mock.return_value = []
        with self.assertRaisesRegex(InternalError, "Missing required property: collection-timestamp"):
            generate_response_json({"collection-id": "123", 'collection-state': 'Created'}, "LATEST")

    # + generate_response_json - has object-expiration
    def test_generate_response_json_opt_properties(self):
        coll_resp = {
            "collection-id": "122",
            "collection-state": "Created",
            "collection-timestamp": "2018-01-01",
            "owner-id": 101,
            "asset-id": 102,
            "old-object-versions-to-keep": 0,
            "classification-type": "Test",
            "object-expiration": "20h"
        }
        expected_resp = {'collection-state': 'Terminating',
                         'collection-timestamp': '2018-01-01',
                         'collection-id': '122',
                         'collection-url': '/collections/LATEST/122',
                         'owner-id': 101,
                         'asset-id': 102,
                         'classification-type': 'Test',
                         'old-object-versions-to-keep': 0,
                         'object-expiration-hours': 20}

        self.assertDictEqual(generate_response_json(coll_resp, "LATEST"), expected_resp)


if __name__ == '__main__':
    unittest.main()
