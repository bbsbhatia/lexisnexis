import os
import unittest
from importlib import reload
from unittest.mock import patch

import lng_datalake_commons.publish_sns_topic as publish_sns_topic_module
from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commands.exceptions import InternalError, NoSuchCollection
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, collection_status, event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

import update_collection_event_handler as update_module
from update_collection_event_handler import update_collection_event_handler, validate_collection_state, \
    update_collection_table, generate_collection_item, get_collection_by_id

io_utils = IOUtils(__file__, 'UpdateCollectionEventHandler')


class UpdateCollectionEventHandlerTest(unittest.TestCase):
    session_patch = None
    tracker_oldest_item_patch = None
    tracker_is_oldest_patch = None
    tracker_delete_patch = None

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'TestArn',
                             'LAMBDA_TASK_ROOT': os.path.dirname(io_utils.schema_path)})
    def setUpClass(cls):  # NOSONAR
        reload(update_module)
        reload(publish_sns_topic_module)
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.tracker_oldest_item_patch = patch(
            "lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id").start()
        cls.tracker_is_oldest_patch = patch("lng_datalake_commons.tracking.tracker._is_oldest_event").start()
        cls.tracker_delete_patch = patch("lng_datalake_commons.tracking.tracker._delete_tracking_item").start()

        cls.tracker_oldest_item_patch.return_value = {'event-id': 1}
        cls.tracker_is_oldest_patch.return_value = True

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()
        cls.session_patch.stop()
        cls.tracker_oldest_item_patch.stop()
        cls.tracker_is_oldest_patch.stop()
        cls.tracker_delete_patch.stop()

    # + update_collection_event_handler: success
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch('service_commons.collection_event_handler.send_collection_notifications')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch("lng_datalake_dal.collection_table.CollectionTable.update_item")
    def test_update_collection_event_handler(self, mock_update_collection, mock_get_collection, mock_publish,
                                             mock_get_catalog):
        original_collection = io_utils.load_data_json("valid_collection_item.json")
        mock_get_collection.return_value = original_collection
        mock_update_collection.return_value = None
        mock_get_catalog.return_value = ["catalog-01"]
        mock_publish.return_value = None
        valid_event = io_utils.load_data_json("valid_event.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         update_collection_event_handler(valid_event,
                                                         'arn:aws:lambda:us-east-1:1234567:function:TestLambda:LATEST'))
        original_collection["asset-id"] = 62
        mock_publish.assert_called_with(original_collection, update_module.notification_schemas, "Collection::Update",
                                        'TestArn', ["catalog-01"])

    # + update_collection_event_handler: no-op suspend success
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch('service_commons.collection_event_handler.send_collection_notifications')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_collection_event_handler(self, mock_get_collection, mock_publish,
                                             mock_get_catalog):
        original_collection = io_utils.load_data_json("valid_collection_item_suspended.json")
        mock_get_collection.return_value = original_collection
        mock_get_catalog.return_value = ["catalog-01"]
        mock_publish.return_value = None
        valid_event = io_utils.load_data_json("valid_event_suspend.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         update_collection_event_handler(valid_event,
                                                         'arn:aws:lambda:us-east-1:1234567:function:TestLambda:LATEST'))

    # + update_collection_event_handler: no-op resume success
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch('service_commons.collection_event_handler.send_collection_notifications')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_collection_event_handler(self, mock_get_collection, mock_publish,
                                             mock_get_catalog):
        original_collection = io_utils.load_data_json("valid_collection_item.json")
        mock_get_collection.return_value = original_collection
        mock_get_catalog.return_value = ["catalog-01"]
        mock_publish.return_value = None
        valid_event = io_utils.load_data_json("valid_event_resume.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         update_collection_event_handler(valid_event,
                                                         'arn:aws:lambda:us-east-1:1234567:function:TestLambda:LATEST'))

    # + update_collection_event_handler: sns-publish-only
    @patch('service_commons.collection_event_handler.publish_notifications')
    def test_update_collection_event_handler_sns_publish_only(self, mock_publish):
        mock_publish.return_value = None
        valid_event = io_utils.load_data_json("valid_event_sns_publish_only.json")
        self.assertEqual(
            event_handler_status.SUCCESS,
            update_collection_event_handler(valid_event, 'arn:aws:lambda:us-east-1:1234567:function:TestLambda:LATEST'))
        sns_publish_data = io_utils.load_data_json("valid_notification_messages.json")
        mock_publish.assert_called_with(sns_publish_data["notifications"], sns_publish_data["notification_attributes"],
                                        "TestArn")

    # - update_collection_event_handler - message incorrect event
    def test_update_collection_event_handler_fail_1(self):
        event = io_utils.load_data_json("invalid_event_type.json")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to extract event'):
            update_collection_event_handler(event, 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    def test_update_collection_event_handler_bad_message(self):
        valid_event = io_utils.load_data_json("invalid_event.json")

        with self.assertRaises(TerminalErrorException):
            update_collection_event_handler(valid_event,
                                            'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # + No-Op for Suspend on Suspended
    def test_validate_collection_state_no_op_suspend(self):
        suspended_collection = io_utils.load_data_json("valid_collection_item_suspended.json")
        valid_deserialized_sns_message = io_utils.load_data_json("valid_suspend_message.json")
        output_item = {}
        return_item = validate_collection_state(valid_deserialized_sns_message, suspended_collection)
        self.assertDictEqual(return_item, output_item)

    # + No-Op for Resume on Created
    def test_validate_collection_state_no_op_resume(self):
        created_collection = io_utils.load_data_json("valid_collection_item.json")
        valid_deserialized_sns_message = io_utils.load_data_json("valid_resume_message.json")
        output_item = {}
        return_item = validate_collection_state(valid_deserialized_sns_message, created_collection)
        self.assertDictEqual(return_item, output_item)

    # - generate_collection_item - Invalid state in db
    def test_validate_collection_state_invalid_state2(self):
        terminated_collection = io_utils.load_data_json("valid_collection_item_terminated.json")
        valid_deserialized_sns_message = io_utils.load_data_json("valid_deserialized_sns_message.json")
        valid_deserialized_sns_message['event-name'] = event_names.COLLECTION_RESUME
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Collection 551 state cannot be changed from {0} to {1}"
                                            .format(collection_status.TERMINATED,
                                                    collection_status.CREATED)):
            validate_collection_state(valid_deserialized_sns_message, terminated_collection)

    def test_generate_collection_item_required_keys(self):
        existing_collection = io_utils.load_data_json("valid_collection_mock_return.json")

        valid_deserialized_sns_message = io_utils.load_data_json("valid_deserialized_sns_message.json")
        output_item = io_utils.load_data_json("returned_collection_mock_with_optional.json")
        return_item = generate_collection_item(valid_deserialized_sns_message, existing_collection)
        self.assertEqual(return_item, output_item)

        existing_collection.pop("owner-id")
        self.assertRaises(TerminalErrorException, generate_collection_item, valid_deserialized_sns_message,
                          existing_collection)

    # +generate_collection_item - move collection to suspend state
    def test_generate_collection_item_keys_suspend(self):
        existing_collection = io_utils.load_data_json("valid_collection_mock_return.json")

        valid_deserialized_sns_message = io_utils.load_data_json("valid_suspend_message.json")
        output_item = io_utils.load_data_json("returned_collection_suspended.json")

        return_item = generate_collection_item(valid_deserialized_sns_message, existing_collection)
        self.assertEqual(return_item, output_item)

    # +generate_collection_item - move collection to created state
    def test_generate_collection_item_resume(self):
        existing_collection = io_utils.load_data_json("valid_suspended_collection_mock_return.json")

        valid_deserialized_sns_message = io_utils.load_data_json("valid_resume_message.json")
        output_item = io_utils.load_data_json("returned_collection_mock_with_optional.json")

        return_item = generate_collection_item(valid_deserialized_sns_message, existing_collection)
        self.assertEqual(return_item, output_item)

    @patch("lng_datalake_dal.collection_table.CollectionTable.update_item")
    def test_update_to_collection_table(self, mocked_update_item):
        """
        Test successful and failed insertions to DynamoDB collection table
        :param mocked_update_item: Mocked DAL put_item call
        """
        # Verify successful insertion to Collection table
        mocked_update_item.return_value = None
        valid_serialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        response = update_collection_table(valid_serialized_item)
        self.assertIsNone(response)

        # Verify failed insertions to Collection table
        mocked_update_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, update_collection_table,
                          valid_serialized_item)

        # Verify failed insertions to Collection table
        mocked_update_item.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {'Code': 'Unit Test', 'Message': 'collection_id takes'
                                                                                                ' an int'}},
                                                     "DAL")
        self.assertRaises(ClientError, update_collection_table,
                          valid_serialized_item)

        invalid_serialized_item = io_utils.load_data_json("invalid_deserialized_sns_message.json")

        mocked_update_item.side_effect = SchemaValidationError
        self.assertRaises(TerminalErrorException,
                          update_collection_table, invalid_serialized_item)

        mocked_update_item.side_effect = ConditionError
        self.assertRaises(TerminalErrorException,
                          update_collection_table, invalid_serialized_item)

        mocked_update_item.side_effect = SchemaError
        self.assertRaises(TerminalErrorException,
                          update_collection_table, invalid_serialized_item)

        mocked_update_item.side_effect = SchemaLoadError
        self.assertRaises(TerminalErrorException,
                          update_collection_table, invalid_serialized_item)

        mocked_update_item.side_effect = Exception
        self.assertRaises(Exception,
                          update_collection_table, invalid_serialized_item)

    # +get_collection_by_id - Valid request
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_by_id_pass(self, mock_collection_get):
        response = {'Test': 'Data'}
        mock_collection_get.return_value = response
        self.assertEqual(get_collection_by_id('12'), response)

    # -get_collection_by_id - TerminalErrorException: Exception
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_by_id_fail_2(self, mock_collection_get):
        mock_collection_get.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException,
                                    "A general exception occurred when attempting to update row in CollectionTable"):
            get_collection_by_id('12')

    # -get_collection_by_id - TerminalErrorException: empty collection dict response
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_by_id_fail_3(self, mock_collection_get):
        mock_collection_get.return_value = {}
        with self.assertRaisesRegex(TerminalErrorException, "Invalid Collection ID"):
            get_collection_by_id('12')

    # -get_collection_by_id - EndpointConnectionError
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_by_id_fail_4(self, mock_get_collection):
        mock_get_collection.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            get_collection_by_id("fake_collection_id")


if __name__ == '__main__':
    unittest.main()
