import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, \
    InvalidRequestPropertyName, NotAuthorizedError, CollectionAlreadyExists
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_collection_command import validate_collection_id, create_collection_command, \
    lambda_handler, generate_response_json

__author__ = "Shekhar Ralhan"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateCollectionCommand')


class TestCreateCollection(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()
        TableCache.clear()

    # +Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_collection_command_success1(self, mock_collection_get_item, mock_owner_resp,
                                                mock_collection_query_items,
                                                mock_event_store_put_item, mock_tracking_put_item):
        mock_collection_get_item.return_value = {}
        mock_owner_resp.return_value = io_util.load_data_json('dynamo.owner_data_valid.json')

        mock_collection_query_items.return_value = []
        mock_event_store_put_item.return_value = None
        mock_tracking_put_item.return_value = None

        request_input = io_util.load_data_json('apigateway.request1.json')
        expected_response_output = io_util.load_data_json('apigateway.response1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             expected_response_output)

    # +Successful test using lambda_handler with command_wrapper decorator with collection-id specified
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_collection_command_success2(self, mock_collection_get_item, mock_owner_resp,
                                                mock_collection_query_items,
                                                mock_event_store_put_item, mock_tracking_put_item):
        mock_collection_get_item.return_value = {}
        mock_owner_resp.return_value = io_util.load_data_json('dynamo.owner_data_valid_collection_prefix.json')

        mock_collection_query_items.return_value = []
        mock_event_store_put_item.return_value = None
        mock_tracking_put_item.return_value = None

        request_input = io_util.load_data_json('apigateway.request2.json')
        expected_response_output = io_util.load_data_json('apigateway.response2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 expected_response_output)

    # +Successful test using lambda_handler with command_wrapper decorator with classification-type specified as Test
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_collection_command_success3(self, mock_collection_get_item, mock_owner_resp,
                                                mock_collection_query_items,
                                                mock_event_store_put_item, mock_tracking_put_item):
        mock_collection_get_item.return_value = {}
        mock_owner_resp.return_value = io_util.load_data_json('dynamo.owner_data_valid.json')

        mock_collection_query_items.return_value = []
        mock_event_store_put_item.return_value = None
        mock_tracking_put_item.return_value = None

        request_input = io_util.load_data_json('apigateway.request3.json')
        expected_response_output = io_util.load_data_json('apigateway.response3.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             expected_response_output)

    # +Successful test using lambda_handler with command_wrapper decorator with classification-type specified as Content
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_collection_command_success4(self, mock_collection_get_item, mock_owner_resp,
                                                mock_collection_query_items,
                                                mock_event_store_put_item, mock_tracking_put_item):
        mock_collection_get_item.return_value = {}
        mock_owner_resp.return_value = io_util.load_data_json('dynamo.owner_data_valid.json')

        mock_collection_query_items.return_value = []
        mock_event_store_put_item.return_value = None
        mock_tracking_put_item.return_value = None

        request_input = io_util.load_data_json('apigateway.request4.json')
        expected_response_output = io_util.load_data_json('apigateway.response4.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             expected_response_output)

    # +Successful test using lambda_handler with command_wrapper decorator with classification-type specified as Test and object-expiration
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_collection_command_success5(self, mock_collection_get_item, mock_owner_resp,
                                                mock_collection_query_items,
                                                mock_event_store_put_item, mock_tracking_put_item):
        mock_collection_get_item.return_value = {}
        mock_owner_resp.return_value = io_util.load_data_json('dynamo.owner_data_valid.json')

        mock_collection_query_items.return_value = []
        mock_event_store_put_item.return_value = None
        mock_tracking_put_item.return_value = None

        request_input = io_util.load_data_json('apigateway.request5.json')
        expected_response_output = io_util.load_data_json('apigateway.response5.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 expected_response_output)

    # -Schema validation error using lambda_handler with command_wrapper decorator
    # - request missing asset-id required property
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_command_decorator_fail(self, mock_tracking_put_item):
        request_input = io_util.load_data_json('apigateway.input_invalid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mock_tracking_put_item.return_value = None
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_collection_id_success_with_prefix(self, mock_collection_get_item):
        mock_collection_get_item.return_value = {}
        collection_id = "FPD_collection-122"
        self.assertIsNone(validate_collection_id(collection_id, "FPD"))
        mock_collection_get_item.assert_called_with({'collection-id': collection_id})

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_collection_id_success_with_prefix_all_digits(self, mock_collection_get_item):
        mock_collection_get_item.return_value = {}
        collection_id = "FPD_999"
        self.assertIsNone(validate_collection_id(collection_id, "FPD"))
        mock_collection_get_item.assert_called_with({'collection-id': collection_id})

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_collection_id_success_without_prefix(self, mock_collection_get_item):
        mock_collection_get_item.return_value = {}
        collection_id = "collection-122"
        self.assertIsNone(validate_collection_id(collection_id, "FPD"))
        mock_collection_get_item.assert_called_with({'collection-id': collection_id})

    def test_validate_collection_id_invalid_chars(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid CollectionID Ab1-#||"
                                                                 "CollectionID can only contain letters, digits and hyphens"):
            validate_collection_id("Ab1-#")

    def test_validate_collection_id_multiple_underscores(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid CollectionID Ab1_Coll||"
                                                                 "CollectionID can only contain letters, digits and hyphens"):
            validate_collection_id("FPD_Ab1_Coll", "FPD")

    def test_validate_collection_id_only_digits(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid CollectionID 999||"
                                                                 "CollectionID can not contain only digits"):
            validate_collection_id("999", "FPD")

    def test_validate_collection_id_invalid_length(self):
        collection_id = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTUu"
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid CollectionID {}||"
                                                                 "CollectionID length should not exceed 40 characters"
                .format(collection_id)):
            validate_collection_id(collection_id)

    def test_validate_collection_id_invalid_prefix_1(self):
        with self.assertRaisesRegex(NotAuthorizedError,
                                    "Invalid Collection ID OTHERSPREFIX_Ab-1||"
                                    "Collection prefix is not registered by current owner"):
            validate_collection_id("OTHERSPREFIX_Ab-1", "MYPREFIX")

    def test_validate_collection_id_invalid_prefix_2(self):
        with self.assertRaisesRegex(NotAuthorizedError,
                                    "Invalid Collection ID OTHERSPREFIX_Ab-1||"
                                    "Collection prefix is not registered by current owner"):
            validate_collection_id("OTHERSPREFIX_Ab-1")

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_collection_id_invalid_collection_id(self, mock_collection_get_item):
        mock_collection_get_item.return_value = {"collection-id": "Ab-1"}
        with self.assertRaisesRegex(CollectionAlreadyExists, "Invalid CollectionID Ab-1||"
                                                             "CollectionID already exists"):
            validate_collection_id("Ab-1")

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_collection_id_error_1(self, mock_collection_get_item):
        mock_collection_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                            'Error': {
                                                                'Code': 'OTHER',
                                                                'Message': 'This is a mock'}},
                                                           "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to get item from Collection Table"):
            validate_collection_id("FooBar")

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_collection_id_error_2(self, mock_collection_get_item):
        mock_collection_get_item.side_effect = Exception({}, "FAKE")
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            validate_collection_id("FooBar")

    # - generate_response_json - Missing required
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_missing_required(self, required_mock, opt_mock):
        required_mock.return_value = ['foobar']
        opt_mock.return_value = []
        with self.assertRaisesRegex(InternalError, "Missing required property: foobar"):
            generate_response_json({}, "123", 12, "LATEST", "TEST")


if __name__ == '__main__':
    unittest.main()
