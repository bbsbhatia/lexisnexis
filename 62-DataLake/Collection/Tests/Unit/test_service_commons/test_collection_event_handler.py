import os
import unittest
from importlib import reload
from unittest.mock import patch

import lng_datalake_commons.publish_sns_topic as publish_sns_topic_module
from botocore.exceptions import ClientError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_testhelper.io_utils import IOUtils

from service_commons.collection_event_handler import publish_notifications, get_schema_props, \
    send_collection_notifications, generate_collection_notification, get_schema_context_props

__author__ = "John Konderla, Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'CollectionEventHandler')


class TestServiceCommonsCollectionEventHandler(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'LAMBDA_TASK_ROOT': os.path.dirname(io_utils.schema_path)})
    def setUpClass(cls):  # NOSONAR
        reload(publish_sns_topic_module)

    # + send_collection_notifications - success
    @patch("service_commons.collection_event_handler.publish_notifications")
    def test_send_collection_notifications_success(self, mock_publish_notifications):
        mock_publish_notifications.return_value = None
        collection_item = io_utils.load_data_json('valid_collection_item.json')
        schemas = ["Schemas/Publish/v0/update-collection.json",
                   "Schemas/Publish/v1/update-collection.json"]
        event = io_utils.load_data_json('valid_event.json')
        catalog_ids = ['catalog-test1', 'catalog-test2']
        target_arn = "subscription arn"
        self.assertIsNone(
            send_collection_notifications(collection_item, schemas, event, target_arn, catalog_ids))
        notifications = io_utils.load_data_json('valid_notification_messages.json')
        notification_attributes = {'event-name': 'Collection::Update',
                                   'collection-id': ['274'],
                                   'catalog-id': ['catalog-test1', 'catalog-test2']}
        mock_publish_notifications.assert_called_with(notifications, notification_attributes, target_arn)

    # - generate_object_notification - Missing required properties to generate the notification message
    def test_generate_object_notification_missing_obj_props(self):
        collection_item = io_utils.load_data_json('invalid_collection_item.json')
        schema_file = "Schemas/Publish/v1/update-collection.json"
        schema_version = "v1"
        event = io_utils.load_data_json('valid_event.json')
        catalog_ids = ['catalog-test1', 'catalog-test2']
        with self.assertRaisesRegex(TerminalErrorException, "Failed to generate collection notification. "
                                                            "Missing required property: 'collection-id'"):
            generate_collection_notification(schema_file, schema_version, collection_item, event, catalog_ids)

    # + get_schema_props - success - schema version = v1
    def test_get_schema_props_success_v1(self):
        schema = io_utils.load_schema_json("Publish/v1/update-collection.json")
        expected_required_collection_props = ["collection-id", "collection-state", "collection-timestamp", "event-name",
                                   "owner-id", "asset-id", "classification-type", "old-object-versions-to-keep"]
        expected_optional_collection_props = ["catalog-ids", "description", "object-expiration-hours"]
        req_collection_props, opt_collection_props = get_schema_props(schema, schema_version='v1')
        self.assertEqual(expected_required_collection_props, req_collection_props)
        self.assertEqual(expected_optional_collection_props, sorted(opt_collection_props))

    # + get_schema_context_props - success - schema version = v1
    def test_get_schema_context_props_success_v1(self):
        schema = io_utils.load_schema_json("Publish/v1/update-collection.json")
        expected_required_context_props = ["request-time", "request-id"]
        expected_optional_context_props = []
        req_context_props, opt_context_props = get_schema_context_props(schema)
        self.assertEqual(expected_required_context_props, req_context_props)
        self.assertEqual(expected_optional_context_props, sorted(opt_context_props))

    @patch('service_commons.collection_event_handler.get_schema_context_props')
    def test_generate_collection_notification_with_optional_schema_key(self, mock_get_schema_context_props):
        # We mock an optional schema prop, because context currently does not have any
        mock_get_schema_context_props.return_value = (
            ["request-id", "request-time"], ["pretend-optional-key"])

        collection_item = io_utils.load_data_json('valid_collection_item.json')
        schema_file = "Schemas/Publish/v1/update-collection.json"
        event = io_utils.load_data_json('valid_event_with_pretend_optional_key.json')
        schema_version = "v1"
        self.assertEqual(io_utils.load_data_json("valid_notification_message_with_pretend_optional_key.json"),
                         generate_collection_notification(schema_file, schema_version, collection_item, event, []))
    # + get_schema_props - success - schema version = v0
    def test_get_schema_props_success_v0(self):
        schema = io_utils.load_schema_json("Publish/v0/update-collection.json")
        expected_required_props = ["collection-id", "item-state", "collection-timestamp", "event-name",
                                   "owner-id", "asset-id", "classification-type", "old-object-versions-to-keep"]
        expected_optional_props = ["catalog-ids", "description", "object-expiration"]
        req_props, opt_props = get_schema_props(schema, schema_version='v0')
        self.assertEqual(expected_required_props, req_props)
        self.assertEqual(expected_optional_props, sorted(opt_props))

    # - publish_notifications: client error
    @patch('lng_datalake_commons.error_handling.error_handler.set_message_additional_attribute')
    @patch('lng_datalake_commons.publish_sns_topic.publish_to_topic')
    def test_publish_notifications(self, mock_sns_publish, mock_error_handler):
        mock_sns_publish.side_effect = [None, ClientError({'ResponseMetadata': {},
                                                           'Error': {
                                                               'Code': 'OTHER',
                                                               'Message': 'This is a mock'}},
                                                          "FAKE")]
        mock_error_handler.return_value = None
        target_arn = "subscription arn"
        notifications = io_utils.load_data_json('valid_notification_messages.json')
        notification_attributes = {'event-name': 'Collection::Update',
                                   'collection-id': '274',
                                   'catalog-id': ['catalog-test1', 'catalog-test2']}
        with self.assertRaisesRegex(ClientError, "This is a mock"):
            publish_notifications(notifications, notification_attributes, target_arn)
        mock_sns_publish.assert_any_call(notifications[0], notification_attributes, target_arn)
        mock_sns_publish.assert_any_call(notifications[1], notification_attributes, target_arn)
        mock_error_handler.assert_called_with('sns-publish-only', {"notifications": notifications[1:],
                                                                   "notification_attributes": notification_attributes})

    def test_generate_collection_notification_missing_context_props(self):
        collection_details = io_utils.load_data_json('valid_collection_item.json')
        schema_file = "Schemas/Publish/v1/update-collection.json"
        event = io_utils.load_data_json('invalid_message_missing_key.json')
        schema_version = "v1"
        with self.assertRaisesRegex(TerminalErrorException, "Failed to generate collection notification. "
                                                            "Missing required property: 'request-time'"):
            generate_collection_notification(schema_file, schema_version, collection_details, event, [])


if __name__ == '__main__':
    unittest.main()
