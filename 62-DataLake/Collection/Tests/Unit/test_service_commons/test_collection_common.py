import unittest

from service_commons.collection_common import transform_expiration_hours

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestServiceCommonsCollectionCommon(unittest.TestCase):

    # + transform_expiration_hours - success
    def test_transform_expiration_hours_success(self):
        expiration_hours = "128h"
        expected_hours = 128
        self.assertEqual(expected_hours, transform_expiration_hours(expiration_hours))


if __name__ == '__main__':
    unittest.main()
