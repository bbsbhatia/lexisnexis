import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, SemanticError, InvalidRequestPropertyValue, \
    NotAuthorizedError, NoSuchCollection, ForbiddenStateTransitionError
from lng_datalake_commons import validate
from lng_datalake_constants import event_names, collection_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from update_collection_command import lambda_handler, update_collection_command, get_collection_by_id, \
    collection_state_change, replace_op_commands, generate_response_json

__author__ = "Maen Nanaa, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateCollectionCommand')


class TestUpdateCollectionCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +apig Schema test - valid
    def test_valid_suspend_json(self):
        # Data has all required attributes
        input_data = io_util.load_data_json('apigateway.suspend_valid.json')
        schema = io_util.load_schema_json("UpdateCollectionCommand-RequestSchema.json")
        self.assertTrue(validate.is_valid_input_json(input_data, schema))

    # +apig Schema test - valid
    def test_valid_resume_json(self):
        # Data has all required attributes
        input_data = io_util.load_data_json('apigateway.resume_valid.json')
        schema = io_util.load_schema_json("UpdateCollectionCommand-RequestSchema.json")
        self.assertTrue(validate.is_valid_input_json(input_data, schema))

    # +apig Schema test - valid
    def test_valid_update_json(self):
        # Data has all required attributes
        input_data = io_util.load_data_json('apigateway.update_valid.json')
        schema = io_util.load_schema_json("UpdateCollectionCommand-RequestSchema.json")
        self.assertTrue(validate.is_valid_input_json(input_data, schema))

    # -apig Schema test - invalid
    def test_invalid_input_json(self):
        # Data has missing required attribute - missing collection-id
        input_data = io_util.load_data_json('apigateway.resume_invalid.json')
        schema = io_util.load_schema_json("UpdateCollectionCommand-RequestSchema.json")
        self.assertFalse(validate.is_valid_input_json(input_data, schema))

    # +lambda_handler resume collection - valid
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_decorator_success_resume(self, ct_get_item_mock,
                                              mock_owner_authorization,
                                              mock_collection_query_items,
                                              mock_event_store_table_put_item, mock_tracking_table_put_item):
        mock_owner_authorization.return_value = {}
        mock_event_store_table_put_item.return_value = None
        mock_tracking_table_put_item.return_value = None
        ct_get_item_mock.return_value = \
            {
                "collection-id": "684",
                "collection-state": "Suspended",
                "collection-timestamp": "2017-12-15T14:10:04.453Z",
                "owner-id": 111,
                "asset-id": 62,
                "old-object-versions-to-keep": 0,
                'description': 'My first collection test',
                "classification-type": "Content"
            }
        mock_collection_query_items.return_value = []
        request_input = io_util.load_data_json('apigateway.request_resume.json')
        expected_response = io_util.load_data_json('apigateway.response_resume_valid.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_wrapper._is_initialized = False

        self.assertEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                         expected_response)

    # +lambda_handler suspend collection - valid
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_decorator_success_suspend(self, ct_get_item_mock,
                                               mock_owner_authorization,
                                               mock_collection_query_items,
                                               mock_event_store_table_put_item, mock_tracking_table_put_item):
        mock_owner_authorization.return_value = {}
        mock_event_store_table_put_item.return_value = None
        mock_tracking_table_put_item.return_value = None
        ct_get_item_mock.return_value = \
            {
                "collection-id": "684",
                "collection-state": "Created",
                "collection-timestamp": "2017-12-15T14:10:04.453Z",
                'description': 'My first collection test',
                "owner-id": 121,
                "asset-id": 65,
                "old-object-versions-to-keep": 0,
                "classification-type": "Content"
            }
        mock_collection_query_items.return_value = []
        request_input = io_util.load_data_json('apigateway.request_suspend.json')
        expected_response = io_util.load_data_json('apigateway.response_suspend_valid.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_wrapper._is_initialized = False

        self.assertEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                         expected_response)

    # -Failed test of Lambda - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_collection_command_failed_owner_auth(self, ct_get_item_mock,
                                                         mock_owner_authorization):
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": "Created",
                "collection-timestamp": "2017-12-15T14:10:04.453Z",
                "owner-id": 101,
                "asset-id": 62,
                "old-object-versions-to-keep": 0,
                "description": "My first collection",
                "classification-type": "Content"
            }
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")

        request_input = \
            {
                "collection-id": "274"
            }

        with self.assertRaisesRegex(NotAuthorizedError,
                                    "NotAuthorizedError||API Key provided is not valid for Owner ID.*"):
            update_collection_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # -update_collection_command - InvalidRequestPropertyValue (collection-id)
    @patch('update_collection_command.get_collection_by_id')
    def test_update_collection_command_test_invalid_collection_id(self,
                                                                  mock_get_collection_by_id):
        request = {
            "collection-id": "10000020",
            "asset-id": 62
        }

        mock_get_collection_by_id.side_effect = InvalidRequestPropertyValue("Invalid Collection ID",
                                                                            "Collection ID {} does not exist".format(
                                                                                request["collection-id"]))
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "InvalidRequestPropertyValue||"
                                    "Invalid Collection ID||Collection ID {} does not exist".format(
                                        request["collection-id"])):
            update_collection_command(request, "123", "LATEST", "testApiKeyId")

    # -update_collection_command - SemanticError: request updates has no changes
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('update_collection_command.get_collection_by_id')
    def test_update_collection_command_test_with_no_changes(self,
                                                            mock_get_collection_by_id,
                                                            mock_owner_authorization
                                                            ):
        mock_owner_authorization.return_value = {}

        request1 = {
            "collection-id": "10000020"
        }
        request2 = {
            "collection-id": "10000020",
            "asset-id": 62
        }
        request3 = {
            "collection-id": "10000020",
            "asset-id": 62
        }

        current_collection = {
            "collection-id": "10000020",
            "collection-state": "Created",
            "collection-timestamp": "2017-12-15T14:10:04.453Z",
            "description": "My first collection test",
            "owner-id": 101,
            "asset-id": 62,
            "old-object-versions-to-keep": 0,
            "classification-type": "Content"
        }
        mock_get_collection_by_id.return_value = current_collection

        with self.assertRaisesRegex(SemanticError, "No data changes"):
            update_collection_command(request1, "123", "LATEST", "testApiKeyId")

        with self.assertRaisesRegex(SemanticError, "No data changes"):
            update_collection_command(request2, "123", "LATEST", "testApiKeyId")

        with self.assertRaisesRegex(SemanticError, "No data changes"):
            update_collection_command(request3, "123", "LATEST", "testApiKeyId")

    # -update_collection_command - SemanticError: update suspended collection
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_update_suspended_collection(self, ct_get_item_mock, mock_owner_authorization):
        mock_owner_authorization.return_value = {}
        ct_get_item_mock.return_value = \
            {
                "collection-id": "122",
                "collection-state": "Suspended",
                "collection-timestamp": "2017-12-15T14:10:04.453203",
                'description': 'My first collection test',
                "owner-id": 122,
                "asset-id": 65,
                "old-object-versions-to-keep": 0,
                "classification-type": "Content"
            }
        request_input = {
            "collection-id": "122",
            "asset-id": 62,
        }

        with self.assertRaisesRegex(SemanticError, "Cannot update collection in Collection::"
                                                   "Suspended state||Resume then make data changes"):
            update_collection_command(request_input, "123", "LATEST", "testApiKeyId")

    # -update_collection_command - SemanticError: update terminated collection
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_update_terminated_collection(self, ct_get_item_mock, mock_owner_authorization):
        mock_owner_authorization.return_value = {}
        ct_get_item_mock.return_value = \
            {
                "collection-id": "122",
                "collection-state": "Terminated",
                "collection-timestamp": "2017-12-15T14:10:04.453203",
                'description': 'My first collection test',
                "owner-id": 122,
                "asset-id": 65,
                "old-object-versions-to-keep": 0,
                "classification-type": "Content"
            }
        request_input = {
            "collection-id": "122",
            "asset-id": 62
        }

        with self.assertRaisesRegex(SemanticError, "Invalid Collection ID||Collection 122 cannot be "
                                                   "updated in Collection||Terminated state"):
            update_collection_command(request_input, "123", "LATEST", "testApiKeyId")

    # -update_collection_command - SemanticError: update and suspended collection
    @patch('lng_datalake_dal.collection_table.CollectionTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_update_and_suspend_collection(self, ct_get_item_mock,
                                                   mock_owner_authorization,
                                                   mock_collection_query_items
                                                   ):
        mock_owner_authorization.return_value = {}
        ct_get_item_mock.return_value = \
            {
                "collection-id": "122",
                "collection-state": "Created",
                "collection-timestamp": "2017-12-15T14:10:04.453203",
                'description': 'My first collection test',
                "owner-id": 122,
                "asset-id": 65,
                "old-object-versions-to-keep": 0,
                "classification-type": "Content"
            }
        mock_collection_query_items.return_value = []

        request_input = {
            "collection-id": "122",
            "patch-operations": [{
                "op": "replace",
                "path": "/suspended",
                "value": True
            },
                {
                    "op": "replace",
                    "path": "/asset-id",
                    "value": 1
                }]
        }

        with self.assertRaisesRegex(SemanticError, "SemanticError||Cannot resume or suspend collection"
                                                   " while making data changes||Resume then make changes"):
            update_collection_command(request_input, "123", "LATEST", "testApiKeyId")

    # +get_collection_by_id - Valid request
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_by_id_pass(self, mock_collection_get):
        response = {'Test': 'Data'}
        mock_collection_get.return_value = response
        self.assertEqual(get_collection_by_id('12'), response)

    # -get_collection_by_id - InternalError: ClientError
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_by_id_fail_1(self, mock_collection_get):
        mock_collection_get.side_effect = ClientError({'ResponseMetadata': {},
                                                       'Error': {
                                                           'Code': 'ProvisionedThroughputExceededException',
                                                           'Message': 'This is a mock'}},
                                                      "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to get item from Collection Table"):
            get_collection_by_id('12')

    # -get_collection_by_id - InternalError: Exception
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_by_id_fail_2(self, mock_collection_get):
        mock_collection_get.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_collection_by_id('12')

    # -get_collection_by_id - InvalidRequestPropertyValue: empty collection dict response
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_by_id_fail_3(self, mock_collection_get):
        mock_collection_get.return_value = {}
        with self.assertRaisesRegex(NoSuchCollection, "Invalid Collection ID"):
            get_collection_by_id('12')

    # +collection_state_change from Created to Suspend - Pass
    def test_collection_state_change_suspend_created(self):
        request = {
            'suspend': True,
            'resume': False
        }
        current_collection = {
            'collection-state': collection_status.CREATED
        }

        response_dict = {
            'event-name': event_names.COLLECTION_SUSPEND,
            'collection-state': collection_status.SUSPENDED
        }

        self.assertDictEqual(response_dict, collection_state_change(request, current_collection))

    # +collection_state_change from Suspended to Created - Pass
    def test_collection_state_change_resume_created(self):
        request = {
            'suspend': False,
            'resume': True
        }
        current_collection = {
            'collection-state': collection_status.SUSPENDED
        }

        response_dict = {
            'event-name': event_names.COLLECTION_RESUME,
            'collection-state': collection_status.CREATED
        }

        self.assertDictEqual(response_dict, collection_state_change(request, current_collection))

    # -collection_state_change Suspend and Resume collection
    def test_collection_state_change_suspend_and_resume(self):
        request = {
            'suspend': True,
            'resume': True
        }
        current_collection = {
            'collection-state': collection_status.SUSPENDED
        }

        self.assertRaises(ForbiddenStateTransitionError, collection_state_change, request, current_collection)

    # -collection_state_change Suspend and is not suspendable
    def test_collection_state_change_suspend_is_not_suspendable(self):
        request = {
            'suspend': True,
            'resume': False
        }
        current_collection = {
            'collection-state': collection_status.SUSPENDED
        }

        self.assertRaises(ForbiddenStateTransitionError, collection_state_change, request, current_collection)

    # -collection_state_change resume and is not resumable
    def test_collection_state_change_resume_is_not_resumable(self):
        request = {
            'suspend': False,
            'resume': True
        }
        current_collection = {
            'collection-state': collection_status.TERMINATED
        }

        self.assertRaises(ForbiddenStateTransitionError, collection_state_change, request, current_collection)

    # + replace_op_commands - Valid request
    def test_replace_op_commands_valid(self):
        input_request = io_util.load_data_json('patch_request_data_valid.json')
        expected_resp = {
            "collection-id": "122",
            "asset-id": 1
        }
        self.assertDictEqual(replace_op_commands(input_request), expected_resp)

    # + replace_op_commands - Valid request suspend
    def test_replace_op_commands_valid_suspend(self):
        input_request = io_util.load_data_json('patch_request_data_suspend_valid.json')
        expected_resp = {
            "collection-id": "122",
            "suspend": True
        }
        self.assertDictEqual(replace_op_commands(input_request), expected_resp)

    # + replace_op_commands - Valid request resume
    def test_replace_op_commands_valid_resume(self):
        input_request = io_util.load_data_json('patch_request_data_resume_valid.json')
        expected_resp = {
            "collection-id": "122",
            "resume": True
        }
        self.assertDictEqual(replace_op_commands(input_request), expected_resp)

    # - replace_op_commands - Invalid request
    def test_replace_op_commands_fail(self):
        input_request = io_util.load_data_json('patch_request_data_invalid.json')
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid operation verb supplied"):
            replace_op_commands(input_request)

    # - generate_response_json - Missing required
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_missing_required(self, required_mock, opt_mock):
        required_mock.return_value = ['collection-timestamp']
        opt_mock.return_value = []
        with self.assertRaisesRegex(InternalError, "Missing required property: collection-timestamp"):
            generate_response_json({'collection-id': '123'}, "Created", "LATEST")


if __name__ == '__main__':
    unittest.main()
