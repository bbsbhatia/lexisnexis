import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import NoSuchCollection, InternalError
from lng_datalake_commons import validate
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

from get_collection_command import get_collection_command, get_catalog_ids, generate_response_json

__author__ = "Shekhar Ralhan, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.1"

io_util = IOUtils(__file__, 'GetCollectionCommand')


class TestGetCollectionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +apig Schema test - valid
    def test_valid_input_json(self):
        # Data has all required attributes
        input_data = io_util.load_data_json('apigateway.input_valid.json')
        schema = io_util.load_schema_json("GetCollectionCommand-RequestSchema.json")
        self.assertTrue(validate.is_valid_input_json(input_data, schema))

    # -apig schema test - invalid
    def test_invalid_input_json(self):
        # Data has missing required attribute - missing collection-id
        input_data = io_util.load_data_json('apigateway.input_invalid.json')
        schema = io_util.load_schema_json("GetCollectionCommand-RequestSchema.json")
        self.assertFalse(validate.is_valid_input_json(input_data, schema))

    # +get_collection_command - valid
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    @patch('lng_aws_clients.session.set_session')
    def test_valid_get_collection_command(self, session_mock, mocked_get_item,
                                          mock_catalog_collection_mapping_table_query_items):
        session_mock.return_value = None
        mock_catalog_collection_mapping_table_query_items.return_value = []
        mocked_get_item.return_value = io_util.load_data_json('dynamodb.mock_get_response.json')
        request_input = io_util.load_data_json('valid_input.json')
        expected_response = io_util.load_data_json('valid_response.json')
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetCollectionCommand-ResponseSchema.json')):
            self.assertEqual(get_collection_command(request_input, "LATEST")['response-dict'], expected_response)

    # +get_collection_command - valid
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    @patch('lng_aws_clients.session.set_session')
    def test_valid_get_collection_command_with_opt(self, session_mock, mocked_get_item,
                                                   mock_catalog_collection_mapping_table_query_items):
        session_mock.return_value = None
        mock_catalog_collection_mapping_table_query_items.return_value = [{"collection-id": "122",
                                                                           "catalog-id": "catalog-01"},
                                                                          {"collection-id": "122",
                                                                           "catalog-id": "catalog-02"}]
        mocked_get_item.return_value = io_util.load_data_json('dynamodb.mock_get_response_with_opt.json')
        request_input = io_util.load_data_json('valid_input.json')
        expected_response = io_util.load_data_json('valid_response_with_opt.json')
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetCollectionCommand-ResponseSchema.json')):
            self.assertEqual(get_collection_command(request_input, "LATEST")['response-dict'], expected_response)

    # -get_collection_command - ClientError
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_invalid_get_collection_command1(self, session_mock, mocked_get_item):
        session_mock.return_value = None
        mocked_get_item.side_effect = ClientError({}, 'dummy')
        request_input = io_util.load_data_json('valid_input.json')
        with self.assertRaisesRegex(InternalError, 'Unable to get item from Collection Table'):
            get_collection_command(request_input, "LATEST")

    # -get_collection_command - Exception
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_invalid_get_collection_exception(self, session_mock, mocked_get_item):
        session_mock.return_value = None
        mocked_get_item.side_effect = Exception
        request_input = io_util.load_data_json('valid_input.json')
        with self.assertRaisesRegex(InternalError, 'Unhandled exception occurred'):
            get_collection_command(request_input, "LATEST")

    # -get_collection_command - InvalidRequestPropertyValue
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_invalid_get_collection2(self, session_mock, mock_collection_get):
        session_mock.return_value = None
        # Empty dict to keep from a valid id being returned
        mock_collection_get.return_value = {}
        request_input = io_util.load_data_json('valid_input.json')
        with self.assertRaisesRegex(NoSuchCollection, 'does not exist'):
            get_collection_command(request_input, "LATEST")

    # -get_catalog_ids - ClientError
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_ids_fail_1(self, mock_seesion, mock_get_catalog_ids):
        mock_seesion.return_value = None
        mock_get_catalog_ids.side_effect = ClientError({}, 'dummy')
        with self.assertRaisesRegex(InternalError, 'Unable to query rows from Catalog Collection Mapping Table'):
            get_catalog_ids("collection-01")

    # -get_catalog_ids - Exception
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_ids_fail_2(self, mock_seesion, mock_get_catalog_ids):
        mock_seesion.return_value = None
        mock_get_catalog_ids.side_effect = Exception
        with self.assertRaisesRegex(InternalError, 'Unhandled Exception Occurred'):
            get_catalog_ids("collection-01")

    # - generate_response_json - Missing required
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_missing_required(self, required_mock, opt_mock):
        required_mock.return_value = ['collection-timestamp']
        opt_mock.return_value = []
        with self.assertRaisesRegex(InternalError, "Missing required property: collection-timestamp"):
            generate_response_json({"collection-id": "123", 'collection-state': 'Created'}, [], "LATEST")


if __name__ == '__main__':
    unittest.main()
