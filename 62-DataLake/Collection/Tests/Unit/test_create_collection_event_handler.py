import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons import sns_extractor
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import collection_status, event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

import create_collection_event_handler as create_module
from create_collection_event_handler import insert_to_collection_table, collection_item_transform, \
    create_collection_event_handler

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CreateCollectionEventHandler")


class CollectionEventHandlerTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'TestArn'})
    def setUpClass(cls):  # NOSONAR
        reload(create_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_all_create_collection_event_handler(self, mocked_put_item, mocked_set_session, mock_validate,
                                                 ):
        event = io_utils.load_data_json("valid_event.json")
        mocked_put_item.return_value = None
        mocked_set_session.return_value = None
        mock_validate.return_value = True

        self.assertEqual(
            create_collection_event_handler(event, 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'),
            event_handler_status.SUCCESS)

        event = io_utils.load_data_json("invalid_event.json")
        self.assertRaises(TerminalErrorException, create_collection_event_handler, event,
                          'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_all_create_collection_event_handler_validate_false(self, mocked_put_item, mocked_set_session,
                                                                mock_validate):
        mocked_put_item.return_value = None
        mocked_set_session.return_value = None
        mock_validate.return_value = False
        event = io_utils.load_data_json("invalid_event.json")
        self.assertRaises(TerminalErrorException, create_collection_event_handler, event,
                          'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_create_collection_event_handler(self, mocked_put_item):
        event = io_utils.load_data_json("valid_event.json")
        expected_message = io_utils.load_data_json("valid_deserialized_sns_message.json")
        message = sns_extractor.extract_sns_message(event)

        self.assertEqual(expected_message, message)

        mocked_put_item.return_value = None
        self.assertIsNone(insert_to_collection_table(message))

    # - create_collection_event_handler - message incorrect event
    @patch("lng_aws_clients.session.set_session")
    def test_create_collection_event_handler_fail_1(self, mocked_set_session):
        mocked_set_session.return_value = None
        event = io_utils.load_data_json("invalid_event_type.json")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to extract event'):
            create_collection_event_handler(event,
                                            'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # - create_collection_event_handler - message incorrect stage
    @patch("lng_aws_clients.session.set_session")
    def test_create_collection_event_handler_fail_2(self, mocked_set_session):
        mocked_set_session.return_value = None
        event = io_utils.load_data_json("invalid_event_stage.json")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to process event'):
            create_collection_event_handler(event,
                                            'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:V1')

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_success(self, mocked_put_item, mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.return_value = None
        self.assertIsNone(insert_to_collection_table(valid_deserialized_item))

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_transform_fail(self, mocked_put_item, mocked_set_session):
        invalid_deserialized_item = io_utils.load_data_json("invalid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = SchemaValidationError
        self.assertRaises(TerminalErrorException, insert_to_collection_table, invalid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("create_collection_event_handler.collection_item_transform")
    def test_insertion_to_collection_table_transform_exception(self, mocked_transform, mocked_set_session):
        invalid_deserialized_item = io_utils.load_data_json("invalid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_transform.return_value = {}
        self.assertRaises(TerminalErrorException, insert_to_collection_table, invalid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_schema_validation_error(self, mocked_put_item, mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = SchemaValidationError
        self.assertRaises(TerminalErrorException, insert_to_collection_table, valid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_condition_error(self, mocked_put_item, mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = ConditionError
        self.assertRaises(TerminalErrorException, insert_to_collection_table, valid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_schema_error(self, mocked_put_item, mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = SchemaError
        self.assertRaises(TerminalErrorException, insert_to_collection_table, valid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_schema_load_error(self, mocked_put_item, mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = SchemaLoadError
        self.assertRaises(TerminalErrorException, insert_to_collection_table, valid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_exception(self, mocked_put_item, mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = Exception
        self.assertRaises(TerminalErrorException, insert_to_collection_table, valid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_provisioned_throughput_exceeded_exception(self, mocked_put_item,
                                                                                     mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = ClientError({"ResponseMetadata": {},
                                                   "Error": {"Code": "ProvisionedThroughputExceededException",
                                                             "Message": "message"}},
                                                  "client-error-mock")
        self.assertRaises(ClientError, insert_to_collection_table, valid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_endpoint_connection_error_exception(self, mocked_put_item,
                                                                               mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, insert_to_collection_table, valid_deserialized_item)

    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.collection_table.CollectionTable.put_item")
    def test_insertion_to_collection_table_client_error(self, mocked_put_item, mocked_set_session):
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_set_session.return_value = None
        mocked_put_item.side_effect = ClientError({"ResponseMetadata": {},
                                                   "Error": {"Code": "code",
                                                             "Message": "message"}},
                                                  "client-error-mock")
        self.assertRaises(ClientError, insert_to_collection_table, valid_deserialized_item)

    def test_collection_item_transform(self):
        item = {
            "request-time": "A",
            "item-name": "B",
            "collection-state": "C",
            "collection-id": "1",
            "owner-id": 2,
            "asset-id": 3,
            "old-object-versions-to-keep": 0,
            "description": "I",
            "event-name": "L",
            "event-id": "M",
            "classification-type": "Content"
        }
        expected = {
            "collection-timestamp": "A",
            "collection-state": collection_status.CREATED,
            "collection-id": "1",
            "collection-hash": "356a192b7913b04c54574d18c28d46e6395428ab",
            "owner-id": 2,
            "asset-id": 3,
            "old-object-versions-to-keep": 0,
            "description": "I",
            'event-ids': ['M'],
            "classification-type": "Content"
        }
        result = collection_item_transform(item)
        self.assertEqual(expected, result)

    def test_collection_item_transform_required_only(self):
        item = {
            "request-time": "A",
            "item-name": "B",
            "collection-state": "C",
            "collection-id": "1",
            "owner-id": 2,
            "asset-id": 3,
            "event-id": "M",
            "old-object-versions-to-keep": 10,
            "classification-type": "Content",
            "Extra": "Extra"
        }
        expected = {
            "collection-timestamp": "A",
            "collection-state": collection_status.CREATED,
            "collection-id": "1",
            "collection-hash": "356a192b7913b04c54574d18c28d46e6395428ab",
            "owner-id": 2,
            "asset-id": 3,
            'event-ids': ['M'],
            "old-object-versions-to-keep": 10,
            "classification-type": "Content"
        }
        result = collection_item_transform(item)
        self.assertEqual(expected, result)


if __name__ == "__main__":
    unittest.main()
