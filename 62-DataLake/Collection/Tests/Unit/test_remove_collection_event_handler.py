import os
import unittest
from datetime import datetime
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

import lng_datalake_commons.publish_sns_topic as publish_sns_topic_module
from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import collection_status, event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

import remove_collection_event_handler as remove_module
from remove_collection_event_handler import update_collection_state, remove_collection_event_handler, \
    start_object_deletion_for_collection

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "RemoveCollectionEventHandler")


class RemoveCollectionEventHandlerTest(TestCase):
    session_patch = None
    tracker_oldest_item_patch = None
    tracker_is_oldest_patch = None
    tracker_delete_patch = None

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'TestArn',
                             'LAMBDA_TASK_ROOT': os.path.dirname(io_utils.schema_path)})
    def setUpClass(cls):  # NOSONAR
        reload(remove_module)
        reload(publish_sns_topic_module)
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.tracker_oldest_item_patch = patch(
            "lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id").start()
        cls.tracker_is_oldest_patch = patch("lng_datalake_commons.tracking.tracker._is_oldest_event").start()
        cls.tracker_delete_patch = patch("lng_datalake_commons.tracking.tracker._delete_tracking_item").start()

        cls.tracker_oldest_item_patch.return_value = {'event-id': 1}
        cls.tracker_is_oldest_patch.return_value = True

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        cls.tracker_oldest_item_patch.stop()
        cls.tracker_is_oldest_patch.stop()
        cls.tracker_delete_patch.stop()
        TableCache.clear()

    # + remove_collection_event_handler - Valid takedown event
    @patch('service_commons.collection_event_handler.send_collection_notifications')
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("remove_collection_event_handler.start_object_deletion_for_collection")
    @patch("remove_collection_event_handler.update_collection_state")
    def test_remove_collection_event_handler(self, mocked_update_collection_state, mocked_state_machine_start,
                                             mock_get_catalog_ids_by_collection_id, mock_publish):
        mocked_state_machine_start.return_value = None
        updated_collection = io_utils.load_data_json("valid_collection_insert.json")
        mocked_update_collection_state.return_value = updated_collection
        mock_get_catalog_ids_by_collection_id.return_value = ['catalog-01']
        mock_publish.return_value = None
        event = io_utils.load_data_json("valid_event.json")
        message = io_utils.load_data_json("valid_event_message_takedown_true.json")
        self.assertEqual(
            event_handler_status.SUCCESS,
            remove_collection_event_handler(event, 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'))
        mock_publish.assert_called_with(updated_collection, remove_module.notification_schemas, message,
                                        "TestArn", ['catalog-01'])

    # + remove_collection_event_handler - Valid non-takedown event
    @patch('service_commons.collection_event_handler.send_collection_notifications')
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("remove_collection_event_handler.start_object_deletion_for_collection")
    @patch("remove_collection_event_handler.update_collection_state")
    def test_remove_collection_event_handler_2(self, mocked_update_collection_state, mocked_state_machine_start,
                                             mock_get_catalog_ids_by_collection_id, mock_publish):
        mocked_state_machine_start.return_value = None
        updated_collection = io_utils.load_data_json("update_collection_result.json")
        mocked_update_collection_state.return_value = updated_collection
        mock_get_catalog_ids_by_collection_id.return_value = ['catalog-01']
        mock_publish.return_value = None
        event = io_utils.load_data_json("valid_event_non_destructive.json")
        message = io_utils.load_data_json("valid_event_message.json")
        self.assertEqual(
            event_handler_status.SUCCESS,
            remove_collection_event_handler(event, 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'))
        mock_publish.assert_called_with(updated_collection, remove_module.notification_schemas, message,
                                        "TestArn", ['catalog-01'])

    # + remove_collection_event_handler - sns-publish-only
    @patch('service_commons.collection_event_handler.publish_notifications')
    def test_remove_collection_event_handler_sns_publish_only(self, mock_publish):
        mock_publish.return_value = None
        event = io_utils.load_data_json("valid_event_sns_publish_only.json")
        self.assertEqual(
            event_handler_status.SUCCESS,
            remove_collection_event_handler(event, 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'))
        sns_publish_data = io_utils.load_data_json("valid_notification_messages.json")
        mock_publish.assert_called_with(sns_publish_data["notifications"], sns_publish_data["notification_attributes"],
                                        "TestArn")

    # - remove_collection_event_handler - message incorrect event
    def test_remove_collection_event_handler_fail_1(self):
        event = io_utils.load_data_json("invalid_event_type.json")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to extract event'):
            remove_collection_event_handler(event, 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # + start_object_deletion_for_collection - Valid return
    @patch("lng_aws_clients.stepfunctions.get_client")
    def test_start_object_deletion_for_collection(self, mock_sfn):
        output = {
            'executionArn': 'arn:blah',
            'startDate': datetime(2015, 1, 1)
        }
        mock_sfn.return_value.start_execution.return_value = output
        valid_deserialized_item = io_utils.load_data_json("valid_deserialized_sns_message.json")
        self.assertIsNone(start_object_deletion_for_collection(valid_deserialized_item))

    # - start_object_deletion_for_collection - General Exception
    @patch("lng_aws_clients.stepfunctions.get_client")
    def test_start_object_deletion_for_collection_sfn_exception(self, mock_sfn):
        mock_sfn.return_value.start_execution.side_effect = Exception
        self.assertRaises(TerminalErrorException, start_object_deletion_for_collection, '551')

    # - start_object_deletion_for_collection - EndpointConnectionError
    @patch("lng_aws_clients.stepfunctions.get_client")
    def test_start_object_deletion_for_collection_endpoint_connection_error_exception(self, mock_sfn):
        output = {
            'executionArn': 'arn:blah',
            'startDate': datetime(2015, 1, 1)
        }
        mock_sfn.return_value.start_execution.return_value = output
        mock_sfn.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, start_object_deletion_for_collection, '551')

    # - start_object_deletion_for_collection - Client Error
    @patch("lng_aws_clients.stepfunctions.get_client")
    def test_start_object_deletion_for_collection_client_error(self, mock_sfn):
        output = {
            'executionArn': 'arn:blah',
            'startDate': datetime(2015, 1, 1)
        }
        mock_sfn.return_value.start_execution.return_value = output
        mock_sfn.side_effect = ClientError({"ResponseMetadata": {},
                                            "Error": {"Code": "StateMachineDoesNotExist",
                                                      "Message": "message"}},
                                           "client-error-mock")
        self.assertRaises(ClientError, start_object_deletion_for_collection, '551')

    # + update_collection_state - Valid request
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    @patch("lng_datalake_dal.collection_table.CollectionTable.update_item")
    def test_update_collection_table_success(self, mocked_update_item, mocked_get_item):
        mocked_get_item.return_value = io_utils.load_data_json("get_collection_result.json")
        update_result = io_utils.load_data_json("valid_collection_insert.json")
        mocked_update_item.return_value = update_result
        self.assertEqual(update_collection_state('100089', collection_status.TERMINATING, 'test_event'), update_result)

    # - update_collection_state - No collection-id
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    def test_get_collection_table_collection_missing(self, mocked_get_item):
        mocked_get_item.return_value = {}
        self.assertRaises(TerminalErrorException, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

    # - update_collection_state - Client and Exception
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    def test_get_collection_table_exceptions(self, mocked_get_item):
        mocked_get_item.side_effect = Exception
        self.assertRaises(Exception, update_collection_state, '551', collection_status.TERMINATING)

        mocked_get_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

        mocked_get_item.side_effect = ClientError({"ResponseMetadata": {},
                                                   "Error": {"Code": "ProvisionedThroughputExceededException",
                                                             "Message": "message"}},
                                                  "client-error-mock")
        self.assertRaises(ClientError, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

    # - update_collection_state - Multiple Exception paths
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    @patch("lng_datalake_dal.collection_table.CollectionTable.update_item")
    def test_update_collection_table_exceptions(self, mocked_update_item, mocked_get_item):
        mocked_get_item.return_value = io_utils.load_data_json("get_collection_result.json")
        mocked_update_item.side_effect = SchemaValidationError
        self.assertRaises(TerminalErrorException, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

        mocked_update_item.side_effect = ConditionError
        self.assertRaises(TerminalErrorException, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

        mocked_update_item.side_effect = SchemaError
        self.assertRaises(TerminalErrorException, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

        mocked_update_item.side_effect = SchemaLoadError
        self.assertRaises(TerminalErrorException, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

        mocked_update_item.side_effect = Exception
        self.assertRaises(TerminalErrorException, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

        mocked_update_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, update_collection_state, '551', collection_status.TERMINATING, 'test_event')

        mocked_update_item.side_effect = ClientError({"ResponseMetadata": {},
                                                      "Error": {"Code": "ProvisionedThroughputExceededException",
                                                                "Message": "message"}},
                                                     "client-error-mock")
        self.assertRaises(ClientError, update_collection_state, '551', collection_status.TERMINATING, 'test_event')


if __name__ == '__main__':
    unittest.main()
