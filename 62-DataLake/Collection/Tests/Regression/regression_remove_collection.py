import os
import unittest

from lng_datalake_client.Collection.remove_collection import remove_collection

__author__ = "Akeem Bolarinwa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestRemoveCollection(unittest.TestCase):
    default_collection_id = '1'
    default_collection_name = 'Collection1'
    default_owner_id = 1
    default_asset_id = 1
    default_classification_type = "Content"

    # + Test the 202 status code and collection_name for remove_collection
    def test_remove_collection(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        collection_id = input_dict.get("collection-id", self.default_collection_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "collection-id": collection_id
            }

        expected_response = \
            {
                "collection-state": "Terminating",
                "collection-id": collection_id,
                "collection-url": "/collections/{0}/{1}".format(stage, collection_id),
                "asset-id": input_dict.get("asset-id", self.default_asset_id),
                "old-object-versions-to-keep": input_dict.get('old-object-versions-to-keep', 0),
                "classification-type": input_dict.get("classification-type", self.default_classification_type)
            }
        # Add any additional attributes to validate against the collection
        for key, value in input_dict.items():
            if key not in expected_response:
                expected_response[key] = value

        response = remove_collection(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("collection", response_dict)
        expected_response['collection-timestamp'] = response_dict['collection'].get('collection-timestamp')

        # test the response body
        self.assertDictEqual(expected_response, response_dict['collection'])
        return response_dict['collection']

    # - remove_collection with invalid collection id --400
    def test_remove_collection_invalid_collection_id(self):

        # must use a collection-id we know doesn't exist (still has to contain valid chars or we get another error)
        invalid_collection_id = "regression-abcdefghijklmnopqrstuvwxyz"
        request_dict = {
            "collection-id": invalid_collection_id
        }

        response = remove_collection(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(invalid_collection_id))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - remove_collection with invalid x-api-key -- 403
    def test_remove_collection_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "collection-id": self.default_collection_id
        }

        response = remove_collection(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - wrong x-api-key, auth error
    def test_remove_collection_incorrect_x_api_key(self, input_dict=None, api_key=None):
        if not api_key:
            api_key = "some-key"
        owner_id = input_dict.get("owner-id", self.default_owner_id)
        request_dict = {
            'headers': {
                'Content-type': 'application/json',
                'x-api-key': api_key
            },
            "collection-id": input_dict.get("collection-id", self.default_collection_id)
        }
        response = remove_collection(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")

    # - remove_collection with invalid content-type -- 415
    def test_remove_collection_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "collection-id": self.default_collection_id
        }

        response = remove_collection(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - remove_collection with invalid collection state --422
    def test_remove_collection_invalid_collection_state(self, input_dict=None):
        if input_dict:
            request_dict = input_dict
            request_dict["body"] = {}
        else:
            request_dict = {
                "collection-id": self.default_collection_id
            }

        response = remove_collection(request_dict)

        # test the status code 409
        self.assertEqual(409, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "ForbiddenStateTransitionError")
        self.assertEqual(json_error_resp['message'], "Collection must be in a Suspended state to be removed")
        self.assertEqual(json_error_resp['corrective-action'], "Suspend collection before removing it")


if __name__ == '__main__':
    unittest.main()
