import os
import unittest

from lng_datalake_client.Collection.list_collections import list_collections
from lng_datalake_testhelper.io_utils import IOUtils

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__)


class TestListCollections(unittest.TestCase):
    default_owner_id = int(os.getenv("OWNER-ID", 210))
    default_asset_id = int(os.getenv("ASSET-ID", 62))
    default_page_size = int(os.getenv("PAGE-SIZE", 1))
    default_invalid_max_results = 1001

    # + Test the 200 status code and response for all results
    def test_list_collections_all_results(self):
        response = list_collections({})

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_dict["context"]["stage"])

        self.assertIn("collections", response_dict)

        # test the response body
        self.assertGreater(len(response_dict["collections"]), 0)
        self.assertTrue(isinstance(response_dict["item-count"], int))

    # + Test the 200 status code and collection properties for list collections
    def test_list_collection_by_owner_id_get_all(self,
                                                 list_collection_data: list = None,
                                                 owner_id=default_owner_id):
        request_dict = {
            "owner-id": owner_id
        }
        response = list_collections(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"],
                         response_dict["context"].get("stage"))

        # test the response body
        collection_resp = response_dict['collections']
        # sort in ascending timestamp since that's how the list_collection_data is added
        collection_resp.sort(key=lambda x: x['collection-timestamp'])
        self.assertEqual(list_collection_data, collection_resp)
        self.assertTrue(isinstance(response_dict["item-count"], int))

    # + Test list collections for given owner id and pagination
    def test_list_collections_by_owner_id_with_pagination(self,
                                                          list_collection_data: list = None,
                                                          owner_id=default_owner_id,
                                                          page_size=default_page_size
                                                          ):

        # assert test data size is greater than page size
        # so we can paginate
        self.assertGreater(len(list_collection_data), page_size)

        request_dict = {
            "owner-id": owner_id,
            "max-items": page_size
        }
        response = list_collections(request_dict)
        response_dict = response.json()

        # test the 200 response code
        self.assertEqual(200, response.status_code)

        # test the response context
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"],
                         response_dict["context"].get("stage"))

        self.assertIn("collections", response_dict)

        results = response_dict["collections"]

        # assert that pagination-token is present in the response
        self.assertIn("next-token", response_dict)

        # assert that number of items received match the page size
        items_count = len(results)
        self.assertEqual(page_size, items_count)
        self.assertTrue(isinstance(response_dict["item-count"], int))

        request_dict["next-token"] = response_dict['next-token']
        response_2 = list_collections(request_dict)

        # test the status code 200
        self.assertEqual(200, response_2.status_code)

        # test the response body
        response_2 = response_2.json()
        self.assertIn("context", response_2)
        self.assertTrue(isinstance(response_2["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_2["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_2["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_2["context"]["stage"])
        self.assertIn("collections", response_2)
        self.assertEqual(1, len(response_2["collections"]))
        self.assertTrue(isinstance(response_2["item-count"], int))

        # test if the two responses are different after pagination
        self.assertNotEqual(response_dict["collections"], response_2["collections"])

    # TODO: Reimplement this functionality
    # + test list collections with show-terminated-only as true
    # def test_list_collections_show_terminated_only(self):
    #     request_dict = {
    #         "show-terminated-only": "true"
    #     }
    #     response = list_collections(request_dict)
    #     response_dict = response.json()
    #
    #     # test the 200 response code
    #     self.assertEqual(200, response.status_code)
    #
    #     self.assertIn("context", response_dict)
    #     self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
    #     self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
    #     self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
    #     self.assertEqual(get_environ_variables.get_environment_variables()["STAGE"],
    #                      response_dict["context"].get("stage"))
    #
    #     self.assertIn("collections", response_dict)
    #     self.assertTrue(isinstance(response_dict["item-count"], int))
    #
    #     # test the body to have only terminated collection
    #     results = response_dict["collections"]
    #     if len(results) > 0:
    #         collection_state_list = [collection["collection-state"] for collection in results]
    #         collection_state_set = set(collection_state_list)
    #         self.assertEqual(len(collection_state_set), 1)
    #         self.assertTrue(collection_state_set.pop() == "Terminated")
    #     else:
    #         print("No terminated collections")
    #         self.assertTrue(True)

    # + test list collections with show-terminated as false
    # def test_list_collections_show_terminated_false(self):
    #     request_dict = {
    #         "show-terminated": "false"
    #     }
    #     response = list_collections(request_dict)
    #     response_dict = response.json()
    #
    #     # test the 200 response code
    #     self.assertEqual(200, response.status_code)
    #
    #     self.assertIn("context", response_dict)
    #     self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
    #     self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
    #     self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
    #     self.assertEqual(get_environ_variables.get_environment_variables()["STAGE"],
    #                      response_dict["context"].get("stage"))
    #
    #     self.assertIn("collections", response_dict)
    #     self.assertTrue(isinstance(response_dict["item-count"], int))
    #
    #     # test the body does not have a terminated collection
    #     results = response_dict["collections"]
    #     collection_state_list = [collection["collection-state"] for collection in results]
    #     self.assertFalse("Terminated" in collection_state_list)

    # - test list collections with invalid owner id
    def test_list_collections_invalid_owner_id(self, owner_id=None):
        if not owner_id:
            owner_id = -100

        request_dict = {
            "owner-id": owner_id
        }
        response = list_collections(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)
        
        response_json = response.json()
        # test the response message
        self.assertIn("context", response_json)
        self.assertTrue(isinstance(response_json["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_json["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_json["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_json["context"]["stage"])
        self.assertIn("collections", response_json)
        self.assertEqual(0, len(response_json["collections"]))
        self.assertEqual(response_json["item-count"], 0)

    # - test list collections with invalid max-results value
    def test_list_collections_max_items_is_greater_than_1000(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        invalid_max_results = input_dict.get("max-results", self.default_invalid_max_results)
        request_dict = {
            "max-items": invalid_max_results
        }

        response = list_collections(request_dict)

        # test the status code 400
        self.assertEqual(400, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid max-items value: 1001".format(invalid_max_results))
        self.assertEqual(json_error_resp['corrective-action'], "Value must be between 1 and 1000")

    # - list collections with invalid max-results value
    def test_list_invalid_collections_pagination_diff_max_items(self):
        input_dict = {
            "max-items": 1,
            "next-token": "eyJtYXgtaXRlbXMiOiA0LCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkPSJ9"
        }
        response = list_collections(input_dict)
        self.assertEqual(400, response.status_code)

    # - test list collections with invalid x-api-key
    def test_list_collections_invalid_x_api_key(self):
        input_dict = {
            "headers": {
                "Content-type": "application/json"
            }
        }

        response = list_collections(input_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - test list collections with invalid content type
    def test_list_collections_invalid_content_type(self):
        key = os.environ["X-API-KEY"]
        input_dict = {
            "headers": {
                "Content-type": "application/atom+xml",
                "x-api-key": key
            }
        }

        response = list_collections(input_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
