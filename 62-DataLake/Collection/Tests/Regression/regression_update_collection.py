import os
import unittest
from datetime import datetime

from lng_datalake_client.Collection.update_collection import update_collection, suspend_collection, resume_collection

__author__ = "Prashant Srivastava, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestUpdateCollection(unittest.TestCase):
    default_collection_id = '1'
    default_collection_name = 'regression_test_{}'.format(datetime.now())
    default_collection_state = "Created"
    default_owner_id = 1
    default_asset_id = 1
    default_old_object_versions_to_keep = 0
    default_classification_type = "Content"

    # + Test the 202 status code and asset_id for update_collection
    def test_update_collection_asset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        collection_id = input_dict.get("collection-id", self.default_collection_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "body": {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/asset-id",
                        "value": 1
                    }]
                },
                "collection-id": collection_id
            }

        expected_response = \
            {
                "collection-state": input_dict.get('collection-state', self.default_collection_state),
                "collection-id": collection_id,
                "collection-url": "/collections/{0}/{1}".format(stage, collection_id),
                "asset-id": input_dict.get('asset-id', 1),
                "old-object-versions-to-keep": input_dict.get('old-object-versions-to-keep',
                                                              self.default_old_object_versions_to_keep),
                "classification-type": input_dict.get("classification-type", self.default_classification_type)
            }
        # Add any additional attributes to validate against the collection
        for key, value in input_dict.items():
            if key not in expected_response:
                expected_response[key] = value

        response = update_collection(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("collection", response_dict)
        expected_response['collection-timestamp'] = response_dict['collection'].get('collection-timestamp')

        # test the response body
        self.assertDictEqual(expected_response, response_dict['collection'])
        return response_dict['collection']

    # + Test the 202 status code and set collection to suspended state
    def test_update_collection_begin_suspend(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        collection_id = input_dict.get("collection-id", self.default_collection_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "collection-id": collection_id
            }

        expected_response = \
            {
                "collection-state": "Suspended",
                "collection-id": input_dict.get('collection-id', self.default_collection_id),
                "collection-url": "/collections/{0}/{1}".format(stage, collection_id),
                "asset-id": input_dict.get('asset-id', self.default_asset_id),
                "old-object-versions-to-keep": input_dict.get('old-object-versions-to-keep',
                                                              self.default_old_object_versions_to_keep),
                "classification-type": input_dict.get("classification-type", self.default_classification_type)
            }
        # Add any additional attributes to validate against the collection
        for key, value in input_dict.items():
            if key not in expected_response:
                expected_response[key] = value

        response = suspend_collection(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("collection", response_dict)
        expected_response['collection-timestamp'] = response_dict['collection'].get('collection-timestamp')

        # test the response body
        self.assertDictEqual(expected_response, response_dict['collection'])
        return response_dict['collection']

    # + Test the 202 status code and move from suspended to created/change set finished
    def test_update_collection_resume(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        collection_id = input_dict.get("collection-id", self.default_collection_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "collection-id": collection_id,
            }

        expected_response = \
            {
                "collection-state": "Created",
                "collection-id": collection_id,
                "collection-url": "/collections/{0}/{1}".format(stage, collection_id),
                "asset-id": input_dict.get('asset-id', self.default_asset_id),
                "old-object-versions-to-keep": input_dict.get('old-object-versions-to-keep',
                                                              self.default_old_object_versions_to_keep),
                "classification-type": input_dict.get("classification-type", self.default_classification_type)
            }
        # Add any additional attributes to validate against the collection
        for key, value in input_dict.items():
            if key not in expected_response:
                expected_response[key] = value

        response = resume_collection(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("collection", response_dict)
        expected_response['collection-timestamp'] = response_dict['collection'].get('collection-timestamp')

        # test the response body
        self.assertDictEqual(expected_response, response_dict['collection'])
        return response_dict['collection']

    # - update_collection with invalid collection id --404
    def test_update_collection_invalid_collection_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        # must use a collection-id we know doesn't exist (still has to contain valid chars or we get another error)
        invalid_collection_id = "regression-abcdefghijklmnopqrstuvwxyz"
        request_dict = {
            "body": {
                "patch-operations": [{
                    "op": "replace",
                    "path": "/asset-id",
                    "value": input_dict.get("asset-id", self.default_asset_id)
                }]
            },
            "collection-id": invalid_collection_id
        }

        response = update_collection(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {0}".format(invalid_collection_id))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - update_collection with invalid asset-id --400
    def test_update_collection_invalid_asset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "body": {
                "patch-operations": [{
                    "op": "replace",
                    "path": "/asset-id",
                    "value": 'invalid'
                }]
            },
            "collection-id": input_dict.get("collection-id", self.default_collection_id)
        }

        response = update_collection(request_dict)

        # test the status code 400
        self.assertEqual(400, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyName")
        self.assertEqual(json_error_resp['message'], "Request did not match JSON schema")

    # - test_update_collection not suspended collection resume -- 409
    def test_update_collection_resume_non_suspended(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {

            'collection-id': input_dict.get("collection-id", self.default_collection_id),
            "resume": True
        }

        response = resume_collection(request_dict)

        # test the status code 409
        self.assertEqual(409, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "ForbiddenStateTransitionError")
        self.assertEqual(json_error_resp['message'],
                         "Cannot resume collection in {} state".format(input_dict.get('collection-state', 'Created')))

    # - test_update_collection suspend collection in suspended state -- 422
    def test_update_collection_suspend_in_suspended(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'collection-id': input_dict.get("collection-id", self.default_collection_id),
            "suspend": True
        }

        response = suspend_collection(request_dict)

        # test the status code 422
        self.assertEqual(422, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "Cannot update collection in Suspended state")
        self.assertEqual(json_error_resp['corrective-action'], "Resume then make data changes")

    # - test_update_collection with terminated collection -- 422
    def test_update_collection_terminated_status(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "body": {
                "patch-operations": [{
                    "op": "replace",
                    "path": "/asset-id",
                    "value": input_dict.get("asset-id", self.default_asset_id)
                }]
            },
            "collection-id": input_dict.get("collection-id", self.default_collection_id)
        }

        response = update_collection(request_dict)

        # test the status code 422
        self.assertEqual(422, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {0}".format(
            input_dict.get("collection-id", self.default_collection_id)))
        self.assertEqual(json_error_resp['corrective-action'], "Collection cannot be updated in Terminated state")

    # - update_collection with invalid x-api-key -- 403
    def test_update_collection_invalid_x_api_key(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            'body': {
                "patch-operations": [{
                    "op": "replace",
                    "path": "/asset-id",
                    "value": 1
                }]
            },
            "collection-id": input_dict.get("collection-id", self.default_collection_id)
        }

        response = update_collection(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - wrong x-api-key, auth error
    def test_update_collection_incorrect_x_api_key(self, input_dict=None, api_key=None):
        if not api_key:
            api_key = "some-key"
        owner_id = input_dict.get("owner-id", self.default_owner_id)
        request_dict = {
            'headers': {
                'Content-type': 'application/json',
                'x-api-key': api_key
            },
            'body': {
                "patch-operations": [{
                    "op": "replace",
                    "path": "/asset-id",
                    "value": 1
                }]
            },
            "collection-id": input_dict.get("collection-id", self.default_collection_id)
        }
        response = update_collection(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")

    # - update_collection with invalid content-type -- 415
    def test_update_collection_invalid_content_type(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            'body': {
                "patch-operations": [{
                    "op": "replace",
                    "path": "/asset-id",
                    "value": 1
                }]
            },
            "collection-id": input_dict.get("collection-id", self.default_collection_id)
        }

        response = update_collection(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - update_collection with no data change -- 422
    def test_update_collection_without_data_change(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "patch-operations": [{
                    "op": "replace",
                    "path": "/asset-id",
                    "value": input_dict.get("asset-id", self.default_asset_id)
                }]
            },
            "collection-id": input_dict.get("collection-id", self.default_collection_id)
        }

        response = update_collection(request_dict)

        # test the status code 422
        self.assertEqual(422, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "No data changes")


if __name__ == '__main__':
    unittest.main()
