import os
import unittest

from lng_datalake_client.Collection.get_collection import get_collection

__author__ = "Prashant Srivastava, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.1"


class TestGetCollection(unittest.TestCase):

    # + Test the 200 status code and collection properties for GET_COLLECTION
    def test_get_collection(self, collection_data=None):
        if not collection_data:
            collection_data = {}
        collection_id = collection_data.get('collection-id', '1')
        stage = os.environ['STAGE']

        request_dict = \
            {
                "collection-id": collection_id
            }

        expected_response = \
            {
                "collection-state": collection_data.get("collection-state", "Created"),
                "asset-id": collection_data.get('asset-id', 1),
                "owner-id": collection_data.get('owner-id', 1),
                'collection-id': collection_id,
                'collection-url': "/collections/{0}/{1}".format(stage, collection_id),
                "old-object-versions-to-keep": collection_data.get('old-object-versions-to-keep', 0),
                "classification-type": collection_data.get('classification-type', "Content"),
            }
        # Add any additional attributes to validate against the collection
        for key, value in collection_data.items():
            if key not in expected_response:
                expected_response[key] = value

        response = get_collection(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("collection", response_dict)
        expected_response['collection-timestamp'] = response_dict['collection'].get('collection-timestamp')

        # test the response body
        self.assertDictEqual(expected_response, response_dict["collection"])
        return response.json()["collection"]

    # - get_collection with invalid collection id
    def test_get_collection_invalid_collection_id(self, collection_data=None):
        if not collection_data:
            collection_data = {}

        # must use a collection-id we know doesn't exist (still has to contain valid chars or we get another error)
        invalid_collection_id = collection_data.get('collection-id', 'regression-abcdefghijklmnopqrstuvwxyz')
        request_dict = {
            "collection-id": invalid_collection_id
        }

        response = get_collection(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(invalid_collection_id))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - get_collection with invalid x-api-key
    def test_get_collection_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "collection-id": '1'
        }

        response = get_collection(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - get_collection with invalid content-type
    def test_get_collection_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "collection-id": "1"
        }

        response = get_collection(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
