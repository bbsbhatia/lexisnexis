import os
import unittest
from datetime import datetime

from lng_datalake_client.Collection.create_collection import create_collection

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestCreateCollection(unittest.TestCase):
    default_collection_name = 'regression_test_{}'.format(datetime.now())
    default_owner_id = 1
    default_asset_id = 1
    default_duplicate_collection_id = "Ab-1"
    default_prefix = "A123"

    # + creation of collection - valid
    def test_create_collection_correct_input(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        stage = os.environ['STAGE']

        request_dict = \
            {
                'body': {
                    "asset-id": input_dict.get('asset-id', self.default_asset_id)
                }
            }

        # Add any additional attributes to create the collection
        for key, value in input_dict.items():
            if key == "collection-id":
                request_dict[key] = value
            else:
                request_dict['body'][key] = value

        # Pop off the state from the request dictionary, if it was supplied in the input dictionary
        request_dict['body'].pop('collection-state', False)

        expected_response = \
            {
                "collection-state": "Created",
                "owner-id": int(os.environ['OWNER-ID']),
                "asset-id": input_dict.get('asset-id', self.default_asset_id),
                "old-object-versions-to-keep": input_dict.get('old-object-versions-to-keep', 0),
                "classification-type": input_dict.get('classification-type', "Content")
            }
        # Add any additional attributes to validate against the collection
        for key, value in input_dict.items():
            if key not in expected_response:
                expected_response[key] = value

        response = create_collection(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("collection", response_dict)
        if 'collection-id' not in input_dict.keys():
            expected_response['collection-id'] = response_dict['collection'].get('collection-id')
        expected_response['collection-url'] = '/collections/{0}/{1}'.format(stage,
                                                                            expected_response.get('collection-id'))
        # Pop the timestamp since we cant determine the exact value
        self.assertTrue(isinstance(response_dict['collection'].pop('collection-timestamp'), str))

        self.assertDictEqual(expected_response, response_dict['collection'])
        return response_dict['collection']

    # - invalid collection-id length
    def test_create_collection_invalid_collection_id_length(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "collection-id": "regression-invalid-collection-id-test-abcdef",
            'body': {

                "asset-id": input_dict.get('asset-id', self.default_asset_id)
            }
        }
        response = create_collection(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Collection ID regression-invalid-collection-id-test-abcdef")
        self.assertEqual(json_error_resp['corrective-action'],
                         "Collection ID length should not exceed 40 characters")

    # - invalid classification-type
    def test_create_collection_invalid_classification_type(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "asset-id": input_dict.get('asset-id', self.default_asset_id),
                "classification-type": "Dummy"
            },
            'collection-id': "regression-collection-invalid-classification"
        }
        response = create_collection(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "BAD_REQUEST_BODY")
        self.assertEqual(json_error_resp['message'], "Invalid request body")
        self.assertEqual(json_error_resp['corrective-action'],
                         "[instance value (\"Dummy\") not found in enum (possible values: [\"Content\",\"Test\"])]")

    # - non-existent asset-id - invalid
    def test_create_collection_invalid_asset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "asset-id": "invalid"
            },
            'collection-id': "regression-collection-invalid-asset-id"
        }
        response = create_collection(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "BAD_REQUEST_BODY")
        self.assertEqual(json_error_resp['message'], "Invalid request body")
        self.assertEqual(json_error_resp['corrective-action'], "[instance type (string) does not match any allowed primitive type (allowed: [\"integer\"])]")

    # - malformed body, missing asset-id - invalid
    def test_create_collection_missing_asset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {
            'body': {
                "old-object-versions-to-keep": 1
            },
            'collection-id': "regression-collection-missing-asset-id"
        }

        response = create_collection(request_dict)

        # test the status code 400
        self.assertEqual(400, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "BAD_REQUEST_BODY")
        self.assertEqual(json_error_resp['message'], "Invalid request body")
        self.assertEqual(json_error_resp['corrective-action'],
                         "[object has missing required properties ([\"asset-id\"])]")

    # - disallowed characters in collection-id - invalid
    def test_create_collection_error_invalid_collection_id_bad_chars(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "asset-id": input_dict.get('asset-id', self.default_asset_id)
            },
            "collection-id": '09:4y'
        }

        response = create_collection(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_dict['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Collection ID can only contain letters, digits and hyphens")

    # - disallowed characters in collection-id - passing in collection-id with multiple underscores
    def test_create_collection_error_multiple_underscores_in_collection_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "asset-id": input_dict.get('asset-id', self.default_asset_id)
            },
            "collection-id": "{0}_{1}".format(input_dict.get("prefix", self.default_prefix), "collection_01")
        }

        response = create_collection(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format("collection_01"))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Collection ID can only contain letters, digits and hyphens")

    # - Only numeric collection-id - invalid
    def test_create_collection_error_invalid_collection_id_numbers(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "asset-id": input_dict.get('asset-id', self.default_asset_id)
            },
            "collection-id": "123"
        }

        response = create_collection(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_dict['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID cannot contain only digits")

    # - wrong owner prefix in collection-id - invalid
    def test_create_collection_error_invalid_owner_prefix(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "asset-id": input_dict.get('asset-id', self.default_asset_id)
            },
            'collection-id': "regression-collection-invalid-owner-prefix"
        }
        # Remove the collection-id if it exists
        if 'collection-id' in input_dict:
            request_dict['collection-id'] = input_dict.pop('collection-id')

        response = create_collection(request_dict)

        self.assertEqual(403, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_dict['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Collection prefix is not registered by current owner")

    # - Only numeric collection-id - invalid
    def test_create_collection_error_invalid_old_object_versions_to_keep(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "asset-id": input_dict.get('asset-id', self.default_asset_id),
                "old-object-versions-to-keep": -2
            },
            'collection-id': "regression-collection-invalid-object-versions"
        }

        response = create_collection(request_dict)

        self.assertEqual(400, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "BAD_REQUEST_BODY")
        self.assertEqual(json_error_resp['message'], "Invalid request body")

    # - no x-api-key, auth error - invalid
    def test_create_collection__invalid_x_api_key(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            'body': {
                "asset-id": 1
            },
            'collection-id': "regression-collection-invalid-x-api-key"
        }

        response = create_collection(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - incorrect Content-type - invalid
    def test_create_collection_unsupported_media(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            'headers': {
                'Content-type': 'application/xml',
                'x-api-key': key
            },
            'body': {
                "asset-id": 1
            },
            'collection-id': "regression-collection-unsupported-media"
        }

        response = create_collection(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - duplicated collection-id - invalid
    def test_create_collection_invalid_collection_id_duplicate(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            'body': {
                "asset-id": input_dict.get('asset-id', self.default_asset_id)
            },
            "collection-id": input_dict.get('collection-id', self.default_duplicate_collection_id)
        }

        response = create_collection(request_dict)

        # test the status code 409
        self.assertEqual(409, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "CollectionAlreadyExists")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_dict['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID already exists")


if __name__ == '__main__':
    unittest.main()
