import inspect
import os
import sys
import unittest

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Can't use __file__ because when running with coverage via command line, ___file__ is not the full path
my_location = os.path.dirname(os.path.abspath(inspect.stack()[0][1]))


def test_runner_suite():
    tests_sources_root = os.path.join(my_location, 'Tests')

    # region Needed to run when using coverage.py so the imports are properly resolved
    python_sources_root = os.path.join(my_location, 'Source', 'Python')
    lambda_sources_root = os.path.join(my_location, 'Source', 'Lambda')
    sys.path.append(lambda_sources_root)
    sys.path.append(python_sources_root)
    # endregion

    tests = unittest.TestLoader().discover(tests_sources_root)
    return unittest.runner.TextTestRunner().run(tests)


if __name__ == '__main__':
    print(test_runner_suite())
