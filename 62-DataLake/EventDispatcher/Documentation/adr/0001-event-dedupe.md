
# Dedupe multiple lambda invocation events


Date: 2019-07-30

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Multiple lambda invocations off the DynamoDB stream happen under heavy load.  These invocations are ms apart and cause
duplicate events to be sent through our system.  Because the events are so close in time, typically they will both be 
picked up by the event handler since the tracking record isn't yet removed.  One ultimately will fail as long as the 
event handler is properly coded and does conditional inserts leading to many terminal errors.
We looked at potentially modifying the tracker to "lock" the tracking record while processing to prevent dupes as well,
however it was deemed the complexity and time to implement was longer and this approach would be attempted first to gather
data.


## Decision
As an attempt to remedy the solution we are going to do a conditional insert into a dedupe table based on the stream and
the unique message-id (AKA the eventID).  If no conditional error is thrown we send the message otherwise we log a message
and continue on.  We will monitor this solution to see if it's worth while and providing us less duplication errors in 
terminal. The TTL on the records will be 1 day

The dedupe table will not have PITR or be a Global Table because the data is temporary and not important to the system
## Consequences

We have another dynamodb table to manage.  This only applies to events off the dynamo stream and NOT retry events through SQS