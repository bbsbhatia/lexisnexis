import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError, ConnectTimeoutError
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from event_dispatcher_lambda import lambda_handler, dispatch_events, is_valid_ddb_stream_event_type, \
    is_valid_image_type, create_message_attributes, publish_to_topic, \
    create_sns_message, process_records

__author__ = "Arunprasath Shankar, Mark Seitter"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__)


class EventDispatcherLambdaTest(unittest.TestCase):
    def setUp(self):  # NOSONAR
        self.session = patch("lng_aws_clients.session.set_session").start()
        self.success_sns_publish_response = io_util.load_data_json("success_sns_publish_response.json")

    def tearDown(self):  # NOSONAR
        self.session.stop()

    # + lambda_handler - Valid
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    @patch('lng_datalake_dal.event_dedupe_table.EventDedupeTable.put_item')
    def test_lambda_handler_valid(self, mock_put_item, mock_valid_topic, mock_sns_client):
        mock_put_item.return_value = None
        mock_valid_topic.return_value = True
        mock_sns_client.return_value.publish.return_value = self.success_sns_publish_response
        event = io_util.load_data_json('valid_dynamo_stream_event.json')
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)

    # + lambda_handler - Valid SQS message
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    @patch('lng_datalake_dal.event_dedupe_table.EventDedupeTable.put_item')
    def test_lambda_handler_valid_1(self, mock_put_item, mock_valid_topic, mock_sns_client):
        mock_put_item.return_value = None
        mock_valid_topic.return_value = True
        mock_sns_client.return_value.publish.return_value = self.success_sns_publish_response
        event = io_util.load_data_json('valid_sqs_event.json')
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)

    # - lambda_handler - Invalid topic arn with exit code 1
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    def test_lambda_handler_fail_1(self, mock_valid_topic):
        mock_valid_topic.return_value = False
        event = io_util.load_data_json('valid_dynamo_stream_event.json')

        with self.assertRaisesRegex(SystemExit, '1'):
            lambda_handler(event, MockLambdaContext())

    # - lambda_handler - Too many seen
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    def test_lambda_handler_invalid_1(self, mock_valid_topic, mock_sns_client, mock_terminal_error):
        mock_valid_topic.return_value = True
        mock_sns_client.return_value.publish.return_value = self.success_sns_publish_response
        event = io_util.load_data_json('invalid_sqs_event_too_many_seen.json')
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        self.assertEqual(1, mock_terminal_error.call_count)

    # + dispatch_events - Valid Request
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    @patch('lng_datalake_dal.event_dedupe_table.EventDedupeTable.put_item')
    def test_dispatch_dynamo_stream_events_valid(self, mock_put_item, mock_valid_topic, mock_sns_client):
        mock_put_item.return_value = None
        mock_valid_topic.return_value = True
        mock_sns_client.return_value.publish.return_value = self.success_sns_publish_response
        event = io_util.load_data_json('valid_dynamo_stream_event.json')

        self.assertEqual(dispatch_events(event), event_handler_status.SUCCESS)

    # -dispatch_events - Valid Request with invalid event delete
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    @patch('lng_aws_clients.sns.get_client')
    def test_dispatch_dynamo_stream_events_fail_1(self, mock_get_client, mock_valid_topic, mock_terminal_error):
        mock_get_client.return_value = None
        mock_valid_topic.return_value = True
        event = io_util.load_data_json('invalid_delete_dynamo_stream_event.json')
        self.assertEqual(dispatch_events(event), event_handler_status.SUCCESS)
        self.assertEqual(2, mock_terminal_error.call_count)

    # -dispatch_events - Valid Request with invalid event oldimage
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    @patch('lng_aws_clients.sns.get_client')
    def test_dispatch_dynamo_stream_events_fail_2(self, mock_get_client, mock_valid_topic, mock_terminal_error):
        mock_get_client.return_value = None
        mock_valid_topic.return_value = True
        event = io_util.load_data_json('invalid_oldimage_dynamo_stream_event.json')
        self.assertEqual(dispatch_events(event), event_handler_status.SUCCESS)
        self.assertEqual(2, mock_terminal_error.call_count)

    # -dispatch_events - Valid Request with exception thrown
    @patch('lng_datalake_dal.event_dedupe_table.EventDedupeTable.put_item')
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    @patch('lng_aws_clients.sns.get_client')
    def test_dispatch_dynamo_stream_events_fail_3(self, mock_sns_client, mock_valid_topic, mock_put_item):
        mock_valid_topic.return_value = True
        mock_sns_client.return_value.publish.return_value = None
        mock_put_item.side_effect = Exception("Foobar")
        event = io_util.load_data_json('valid_dynamo_stream_event.json')
        self.assertEqual(dispatch_events(event), event_handler_status.SUCCESS)
        self.assertEqual(7, mock_sns_client.return_value.publish.call_count)

    # + process_records - Condition Error dedupe
    @patch('event_dispatcher_lambda.use_dedupe')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_datalake_dal.event_dedupe_table.EventDedupeTable.put_item')
    def test_process_records_condition_error(self, mock_put_item, mock_sns_client, mock_dedupe_logic):
        mock_dedupe_logic.return_value = True
        event = io_util.load_data_json('valid_event_record.json')
        mock_put_item.side_effect = ConditionError()
        self.assertIsNone(process_records(event, 1564545977))
        mock_sns_client.return_value.publish.assert_not_called()

    # + process_records - Don't call dedupe
    @patch('event_dispatcher_lambda.use_dedupe')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_datalake_dal.event_dedupe_table.EventDedupeTable.put_item')
    def test_process_records_no_dedupe(self, mock_put_item, mock_sns_client, mock_dedupe_logic):
        mock_dedupe_logic.return_value = False
        event = io_util.load_data_json('valid_event_record.json')
        self.assertIsNone(process_records(event, 1564545977))
        mock_put_item.assert_not_called()
        mock_sns_client.return_value.publish.assert_called_once()

    # - process_records - Unhandled error
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_datalake_dal.event_dedupe_table.EventDedupeTable.put_item')
    def test_process_records_unhandled_error(self, mock_put_item, mock_sns_client):
        event = io_util.load_data_json('valid_event_record.json')
        mock_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                 'Error': {
                                                     'Code': 'OTHER',
                                                     'Message': 'This is a mock'}},
                                                "FAKE")
        self.assertIsNone(process_records(event, 1564545977))
        self.assertEqual(1, mock_sns_client.return_value.publish.call_count)

    # - process_records - Unhandled error
    @patch('event_dispatcher_lambda.use_dedupe')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_datalake_dal.event_dedupe_table.EventDedupeTable.put_item')
    def test_process_records_unhandled_error(self, mock_put_item, mock_sns_client, mock_dedupe):
        event = io_util.load_data_json('valid_event_record.json')
        mock_dedupe.return_value = True
        mock_put_item.side_effect = Exception()
        self.assertIsNone(process_records(event, 1564545977))
        self.assertEqual(1, mock_sns_client.return_value.publish.call_count)

    # + is_valid_ddb_stream_event_type - Valid input
    def test_validate_ddb_stream_event_type_valid(self):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        self.assertTrue(is_valid_ddb_stream_event_type(input_event))

    # - is_valid_ddb_stream_event_type - Missing dynamostream eventName
    def test_validate_ddb_stream_event_type_fail_1(self):
        input_event = io_util.load_data_json('invalid_dynamo_event_no_eventname.json')
        self.assertFalse(is_valid_ddb_stream_event_type(input_event))

    # - is_valid_ddb_stream_event_type - Invalid eventtype
    def test_validate_ddb_stream_event_type_fail_2(self):
        input_event = io_util.load_data_json('invalid_dynamo_event_modify.json')
        self.assertFalse(is_valid_ddb_stream_event_type(input_event))

    # + is_valid_image_type - Valid input
    def test_validate_image_type_vaid(self):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        self.assertTrue(is_valid_image_type(input_event))

    # - is_valid_image_type - Missing dynamodb
    def test_validate_image_type_fail_1(self):
        input_event = io_util.load_data_json('invalid_dynamo_event_no_dynamodb.json')
        self.assertFalse(is_valid_image_type(input_event))

    # - is_valid_image_type - No NewImage
    def test_validate_image_type_fail_2(self):
        input_event = io_util.load_data_json('invalid_dynamo_event_no_newimage.json')
        self.assertFalse(is_valid_image_type(input_event))

    # + publish_to_topic - Valid input
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_to_topic_valid(self, mock_sns_client):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        mock_sns_client.return_value.publish.return_value = self.success_sns_publish_response
        self.assertDictEqual(publish_to_topic(input_event), self.success_sns_publish_response)

    # - publish_to_topic - sns not publishable
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sns.is_publishable_sns_message')
    def test_publish_to_topic_fail_0(self, mock_sns_is_pub, mock_terminal_error):
        input_event = io_util.load_data_json('valid_dynamo_event.json')

        mock_sns_is_pub.return_value = False
        self.assertDictEqual(publish_to_topic(input_event), {})
        self.assertEqual(1, mock_terminal_error.call_count)

    # - publish_to_topic -EndpointConnectionError
    @patch('lng_datalake_commons.error_handling.error_handler.throttle_error')
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_to_topic_fail_endpoint_connection_error_exception(self, mock_sns_client, mock_throttle_error):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        mock_sns_client.return_value.publish.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        self.assertDictEqual(publish_to_topic(input_event), {})
        mock_throttle_error.assert_called_once_with(input_event, service='EndpointConnectionError', is_event=False)

    # - publish_to_topic -ConnectionTimeoutError
    @patch('lng_datalake_commons.error_handling.error_handler.throttle_error')
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_to_topic_fail_connection_timeout_error_exception(self, mock_sns_client, mock_throttle_error):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        mock_sns_client.return_value.publish.side_effect = ConnectTimeoutError(
            endpoint_url='https://fake.content.aws.lexis.com')
        self.assertDictEqual(publish_to_topic(input_event), {})
        mock_throttle_error.assert_called_once_with(input_event, service='ConnectionError', is_event=False)

    # - publish_to_topic - Client Error Throttle with stream record
    @patch('lng_datalake_commons.error_handling.error_handler.throttle_error')
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_to_topic_fail_1(self, mock_sns_client, mock_throttle_error):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        mock_sns_client.return_value.publish.side_effect = ClientError({'ResponseMetadata': {},
                                                                        'Error': {
                                                                            'Code': 'Throttling',
                                                                            'Message': 'This is a mock'}},
                                                                       "FAKE")
        self.assertDictEqual(publish_to_topic(input_event), {})
        self.assertEqual(1, mock_throttle_error.call_count)

    # - publish_to_topic - Client Error InternalError with stream record
    @patch('lng_datalake_commons.error_handling.error_handler.throttle_error')
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_to_topic_fail_client_internal_error(self, mock_sns_client, mock_throttle_error):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        mock_sns_client.return_value.publish.side_effect = ClientError({'ResponseMetadata': {},
                                                                        'Error': {
                                                                            'Code': 'InternalError',
                                                                            'Message': 'An error occurred (InternalError) '
                                                                                       'when calling the Publish operation '
                                                                                       '(reached max retries: 4): '
                                                                                       'Request could not be completed'}},
                                                                       "FAKE")
        self.assertDictEqual(publish_to_topic(input_event), {})
        self.assertEqual(1, mock_throttle_error.call_count)

    # - publish_to_topic - Client Error Throttle without stream record
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_to_topic_fail_2(self, mock_sns_client, mock_terminal_error):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        mock_sns_client.return_value.publish.side_effect = ClientError({'ResponseMetadata': {},
                                                                        'Error': {
                                                                            'Code': 'Throttling',
                                                                            'Message': 'This is a mock'}},
                                                                       "FAKE")
        self.assertDictEqual(publish_to_topic(input_event), {})
        self.assertEqual(1, mock_terminal_error.call_count)

    # - publish_to_topic - Client Error non-throttle
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_to_topic_fail_3(self, mock_sns_client, mock_terminal_error):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        mock_sns_client.return_value.publish.side_effect = ClientError({'ResponseMetadata': {},
                                                                        'Error': {
                                                                            'Code': 'AccessDenied',
                                                                            'Message': 'This is a mock'}},
                                                                       "FAKE")
        self.assertDictEqual(publish_to_topic(input_event), {})
        self.assertEqual(1, mock_terminal_error.call_count)

    # - publish_to_topic - Exception
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_to_topic_fail_4(self, mock_sns_client, mock_terminal_error):
        input_event = io_util.load_data_json('valid_dynamo_event.json')
        mock_sns_client.return_value.publish.side_effect = Exception
        self.assertDictEqual(publish_to_topic(input_event), {})
        self.assertEqual(1, mock_terminal_error.call_count)

    # + create_sns_message - Valid input
    def test_create_sns_message_valid(self):
        dynamo_message = io_util.load_data_json('valid_dynamo_event_deserialized.json')
        # Need this to not deserialize so we use load_txt instead of load_json
        expected_message = io_util.load_txt('valid_sns_message.json')
        self.assertEqual(create_sns_message(dynamo_message), expected_message)

    # + create_message_attributes - event name input
    def test_create_message_attributes_valid_1(self):
        valid_response = {'event-name': {"DataType": "String", "StringValue": 'Collection::Create'},
                          'event-version': {"DataType": "Number", "StringValue": "1"},
                          'stage': {"DataType": "String", "StringValue": 'STABLE'}}
        self.assertDictEqual(create_message_attributes({'event-name': 'Collection::Create', 'stage': 'STABLE'}),
                             valid_response)

    # + create_message_attributes - missing input
    def test_create_message_attributes_valid_2(self):
        valid_response = {'event-name': {"DataType": "String", "StringValue": 'UNKNOWN'},
                          'event-version': {"DataType": "Number", "StringValue": "1"},
                          'stage': {"DataType": "String", "StringValue": 'UNKNOWN'}}
        self.assertDictEqual(create_message_attributes({'missing': 'event'}), valid_response)

if __name__ == '__main__':
    print(unittest.main())
