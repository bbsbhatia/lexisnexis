@Library('DevOpsShared@v1') _
pipeline {
    agent any
    parameters {
        string(name: 'releaseUnitBuild', description: '[Required] The build number of the release unit to deploy')
        string(name: 'assetGroup', description: '[Optional] The name of the asset group to deploy into')
        string(name: 'targetAccount', description: '[Required] The AWS account number to deploy into', defaultValue: '288044017584')
    }
    environment {
        def ASSET_ID = "62"
        def ASSET_NAME = "DataLake"
        def ASSET_AREA_NAME = "EventDispatcher"
        def ASSET_GROUP = "${params.assetGroup ?: commons.getBranchName()}"
        def RELEASE_UNIT = [commons.getBranchName(), params.releaseUnitBuild].join('/')
    }
    stages {
        stage('Approvals') {
            when {
                equals expected: '195052678233', actual: params.targetAccount
            }
            steps {
                timeout(time: 60) {
                    input(message: "Please provide manual approval to deploy to production.", submitter: "AWS-Role LNG 195052678233 WormholeOperator")
                }
            }
        }
        stage('Stage Release Unit') {
            steps {
                script {
                    if (params.targetAccount != '288044017584' && BRANCH_NAME != 'master') {
                        error "We can't deploy a non-master branch to cert or prod"
                    }
                    if (params.targetAccount == '873434867576' && ASSET_GROUP != 'staging') {
                        error "Only staging assetGroup is allowed to be deployed to cert"
                    }
                    if (params.targetAccount == '195052678233' && ASSET_GROUP != 'release') {
                        error "Only release assetGroup is allowed to be deployed to prod"
                    }
                }

                stageReleaseUnit()
            }
        }
        stage('Deploy CloudFormation') {
            parallel {
                stage('EventDispatcher') {
                    steps {
                        cloudformation(
                                command: 'createChangeSet',
                                account: params.targetAccount,
                                templateName: 'EventDispatcher',
                                stackParameters: ['LambdaBucket'  : STAGING_BUCKET,
                                                  'LambdaS3Object': [ASSET_ID, ASSET_NAME, ASSET_AREA_NAME, RELEASE_UNIT, "${ASSET_AREA_NAME.replaceAll("/", "-")}.zip"].join('/'),
                                                  'AssetGroup'    : ASSET_GROUP,
                                                  'BuildNumber'   : RELEASE_UNIT]
                        )
                        cloudformation(
                                command: 'executeChangeSet',
                                account: params.targetAccount,
                                templateName: 'EventDispatcher'
                        )
                    }
                }
            }
        }
    }
    post {
        success {
            script {
                if (commons.getBranchName() == 'master' && params.assetGroup == 'staging' && params.targetAccount == '873434867576') {
                    build job: JOB_NAME,
                            wait: false,
                            parameters: [string(name: 'releaseUnitBuild', value: params.releaseUnitBuild),
                                         string(name: 'targetAccount', value: '195052678233'),
                                         string(name: 'assetGroup', value: 'release')
                            ]
                }
            }
        }
    }
}