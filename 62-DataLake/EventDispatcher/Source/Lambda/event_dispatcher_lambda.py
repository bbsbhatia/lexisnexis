import logging
import os
from concurrent.futures import ThreadPoolExecutor, as_completed
from datetime import datetime, timedelta
from inspect import stack, getmodulename

import orjson
from botocore.exceptions import ClientError, EndpointConnectionError
from lng_aws_clients import sns
from lng_datalake_commons import sns_extractor, sqs_extractor, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_constants import event_handler_status
from lng_datalake_dal import dynamo_mapper
from lng_datalake_dal.event_dedupe_table import EventDedupeTable
from lng_datalake_dal.exceptions import ConditionError

__author__ = "Mark Seitter, Shekhar Ralhan"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

EVENT_DISPATCHER_TOPIC_ARN = os.getenv("EVENT_DISPATCHER_TOPIC_ARN")
MAX_RETRY_COUNT = os.getenv("MAX_RETRY_COUNT", 500)
USE_DEDUPE_LOGIC = True if os.getenv("USE_DEDUPE_LOGIC") == "True" else False

RETRY_EXCEPTIONS = ["ProvisionedThroughputExceededException", "InvalidClientTokenId", "ThrottlingException",
                    "InternalServerError", "500", "Throttling", "InternalError", "ServiceUnavailable"]


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))
    return dispatch_events(event)


def dispatch_events(event: dict) -> str:
    # First thing we do is validate the topic arn is valid, otherwise we fail the lambda function so the dynamo stream
    # sequence number won't be updated and will just auto-retry.  This allows quick failure for invalid configuration
    # while we fix the config.
    if not sns.is_valid_topic_arn(EVENT_DISPATCHER_TOPIC_ARN):
        logger.error("SNS topic Arn is not valid, correct before events can be published: TopicArn={0}".format(
            EVENT_DISPATCHER_TOPIC_ARN))
        os.sys.exit(1)

    # initialize the sns client before threading
    sns.get_client()
    # initialize the dedupe table before threading
    if use_dedupe():
        EventDedupeTable()

    pending_epoch = int((datetime.now() + timedelta(days=1)).timestamp())

    records = event["Records"]
    num_records = len(records)

    logger.info("Number of records per transaction: {0}".format(num_records))
    futures = []
    with ThreadPoolExecutor(max_workers=10) as executor:
        for record in records:
            futures.append(executor.submit(process_records, event_record=record, pending_epoch=pending_epoch))

    for future in as_completed(futures):
        if future.exception():
            logger.error("Uncaught exception in threads: {0}".format(future.exception()))
            error_handler.terminal_error(event, is_event=False)

    return event_handler_status.SUCCESS


def process_records(event_record: dict, pending_epoch: int) -> None:
    if is_valid_dynamo_stream_record(event_record):
        # Attempt to acquire a lock to prevent multiple processing of the same message
        message_id = event_record['eventID']
        if use_dedupe():
            try:
                EventDedupeTable().put_item(
                    {'message-id': message_id, 'stream-arn': event_record['eventSourceARN'],
                     'pending-expiration-epoch': pending_epoch})
            except ConditionError:
                logger.debug("Ignoring Duplicate Event: {0}".format(message_id))
                return
            except Exception as e:
                # we dont want to prevent publishing for exceptions since duplicates downstream can be handled
                logger.error("Uncaught exception writing to EventDedupeTable: {0}".format(e))
                logger.info("Continue to publish")

        # Already validated these keys exist in the is_valid_dynamo_stream_record call
        transformed_record = dynamo_mapper.transform_to_client_dict(event_record["dynamodb"]["NewImage"])
        transformed_record['seen-count'] = 0
        publish_to_topic(transformed_record)

    elif is_valid_sqs_record(event_record):
        # We extract the sqs message first, which is actually the original sns message sent, so we need to
        # extract the sns message to it's original form before posting back onto sns
        sqs_message = sqs_extractor.extract_sqs_message(event_record)
        transformed_record = sns_extractor.extract_sns_message(sqs_message)
        seen_count = transformed_record.get('seen-count', 0) + 1

        # If we have seen this too many times throw it to terminal error and don't resend
        if seen_count > MAX_RETRY_COUNT:
            logger.error(
                "Record Retried too many times. "
                "Failing the record and sending to terminal error bucket. Record: {0}".format(event_record))
            error_handler.terminal_error(event_record, is_event=False)
        else:
            transformed_record['seen-count'] = seen_count

            publish_to_topic(transformed_record)
    else:
        logger.error("Validation of event type failed. "
                     "Unsupported event name found. {0}".format(event_record))
        error_handler.terminal_error(event_record, is_event=False)


# Hotfix to fix production and get tests working
def use_dedupe() -> bool:
    return USE_DEDUPE_LOGIC


def is_valid_dynamo_stream_record(record: dict) -> bool:
    return is_valid_ddb_stream_event_type(record) and is_valid_image_type(record)


# Validate we have an INSERT event type record all others are invalid for EventStore
def is_valid_ddb_stream_event_type(record: dict) -> bool:
    # The "eventName" attribute from within the record dictionary is generated by AWS
    event_name = record.get('eventName', 'MISSING')

    # We only expect INSERT events to come from EventStore anything else is an error
    if event_name == 'INSERT':
        return True

    return False


def is_valid_image_type(record: dict) -> bool:
    dynamodb_item = record.get('dynamodb')

    if not dynamodb_item:
        logger.error("Validation of image type failed. Key = 'dynamodb' does not exist. "
                     "Record = {0}".format(record))
        return False

    # We need to only process the NewImage event records, if none exist, this record isn't valid
    if dynamodb_item.get('NewImage'):
        return True
    else:
        logger.error("Validation of image type failed. Invalid DynamoDB stream image type found. "
                     "Record = {0}".format(record))

    return False


# Validate we have a sqs event type
def is_valid_sqs_record(record: dict) -> bool:
    # The "eventSource" attribute from within the record dictionary is generated by AWS
    event_name = record.get('eventSource', 'MISSING')

    # We only expect aws:sqs events
    if event_name == 'aws:sqs':
        return True

    return False


def publish_to_topic(message: dict) -> dict:
    try:

        sns_message = create_sns_message(message)

        if sns.is_publishable_sns_message(sns_message):
            response = sns.get_client().publish(
                TargetArn=EVENT_DISPATCHER_TOPIC_ARN,
                Message=sns_message,
                MessageStructure="json",
                MessageAttributes=create_message_attributes(message)
            )
            logger.info(
                "Event dispatch succeeded. Message published to SNS topic."
                " Attributes= {0},Message={1} Response = {2}".format(
                    create_message_attributes(message), sns_message, response))
            return response
        else:
            logger.error("Message is not a valid sns message. Sns_Message = {}"
                         .format(sns_message))
            error_handler.terminal_error(message, is_event=False)

    except ClientError as e:
        # Throttle messages can be retried, we need to throw the original message back into a dynamo stream record
        # and put it on the queue
        if e.response["Error"]["Code"] in RETRY_EXCEPTIONS:
            logger.warning("Message failed to publish to SNS topic. Throttling Exception")
            if message:
                error_handler.throttle_error(message, service="Sns", is_event=False)
                return {}

        logger.error("Message failed to publish to SNS topic. Message = {} | Topic = {} | Exception = {}"
                     .format(message, EVENT_DISPATCHER_TOPIC_ARN, e))
        error_handler.terminal_error(message, is_event=False)
    except error_handler.retryable_exceptions as e:
        logger.warning("Message failed to publish to SNS topic. Retryable exception encountered")
        if message:
            service = 'EndpointConnectionError' if isinstance(e, EndpointConnectionError) else 'ConnectionError'
            error_handler.throttle_error(message, service=service, is_event=False)
            return {}
    except Exception as e:
        logger.error("Message failed to publish to SNS topic due to unknown reasons. "
                     "Message = {} | Topic = {} | Exception = {}"
                     .format(message, EVENT_DISPATCHER_TOPIC_ARN, e))
        error_handler.terminal_error(message, is_event=False)

    return {}


# Need to flatten the message into a string with a key of default, then flatten the entire message for sns
def create_sns_message(message: dict) -> str:
    try:
        return orjson.dumps({"default": orjson.dumps(message).decode()}).decode()
    except Exception as e:
        logger.error(
            'Unknown exception occurred when creating the sns message. Message:{0}, Error:{1}'.format(message, e))
        raise e


# Creates the SNS message attributes that are needed for us to filter the message for subscribers
def create_message_attributes(message: dict) -> dict:
    return {'event-name': {"DataType": "String", "StringValue": message.get('event-name', 'UNKNOWN')},
            'event-version': {"DataType": "Number", "StringValue": str(message.get('event-version', 1))},
            'stage': {"DataType": "String", "StringValue": message.get('stage', 'UNKNOWN')}}


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    from lng_datalake_constants import event_names

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("** Local Test Run **")

    os.environ["AWS_PROFILE"] = "c-sand"

    error_handler.terminal_errors_bucket = "ccs-sandbox-lambda-terminal-errors"
    error_handler.retry_queue_url = "https://sqs.us-east-1.amazonaws.com/288044017584/mark-test"
    EVENT_DISPATCHER_TOPIC_ARN = 'arn:aws:sns:us-east-1:288044017584:mark-test'

    event_json = {
        "Records": [
            {"eventName": "INSERT",
             "dynamodb": {"NewImage": {
                 "EventName": {"S": event_names.SUBSCRIPTION_CREATE},
                 "Stage": {"S": "LATEST"}}
             }},
            {"eventName": "INSERT",
             "dynamodb": {"NewImage": {
                 "EventName": {"S": event_names.COLLECTION_CREATE}}
             }},
            {"eventName": "INSERT",
             "dynamodb": {"NewImage": {
                 "EventName": {"S": event_names.COLLECTION_UPDATE}}
             }},
            {"eventName": "INSERT",
             "dynamodb": {"NewImage": {
                 "EventName": {"S": event_names.OBJECT_CREATE}}
             }},
            {"eventName": "INSERT",
             "dynamodb": {"NewImage": {
                 "EventName": {"S": event_names.OBJECT_UPDATE}}
             }},
            {"eventName": "INSERT",
             "dynamodb": {"NewImage": {
                 "EventName": {"S": event_names.OBJECT_REMOVE}}
             }},
            {"eventName": "INSERT",
             "dynamodb": {"OldImage": {
                 "EventName": {"S": event_names.COLLECTION_UPDATE}}
             }},
            {"eventName": "INSERT",
             "dynamodb": {"OldImage": {
                 "EventName": {"S": event_names.SUBSCRIPTION_CREATE}}
             }},
            {"eventName": "INSERT",
             "dynamodb": {"NewImage": {
                 "EventName": {"S": event_names.OWNER_CREATE}}
             }},
            {"eventName": "INSERT"},
            {"eventName": "MODIFY"},
            {"eventName": "DELETE"}
        ]
    }
    lambda_handler(event_json, MockLambdaContext)
