import unittest

from lng_datalake_commons.email_helper import is_valid_email_format

__author__ = "Jonathan A Mitchall"
__credits__ = "Daniel Trunley, Simon Grimshaw"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestEmailHelper(unittest.TestCase):

    # +Check
    def test_is_valid_arn_format_success(self):
        self.assertTrue(is_valid_email_format("Test@Email.com"))
        self.assertTrue(is_valid_email_format("Test.more.info@Email.com"))
        self.assertTrue(is_valid_email_format("Test_more.info@Email.com"))
        self.assertTrue(is_valid_email_format("Test_more_info@Email.com"))
        self.assertTrue(is_valid_email_format("Test_more.info@Email.net"))
        self.assertTrue(is_valid_email_format("Test_more.info@Email.long.weird_domain.org"))

    # -Check
    def test_is_valid_arn_format_failed(self):
        self.assertFalse(is_valid_email_format("Test.Email.com"))
        self.assertFalse(is_valid_email_format("Test.more info@Email.com"))
        self.assertFalse(is_valid_email_format("Test_more_info Email_com"))
        self.assertFalse(is_valid_email_format("Test_more_info_Email.uk"))
        self.assertFalse(is_valid_email_format("Test_more.info@Email long.weird_domain.org"))
        self.assertFalse(is_valid_email_format("arn:aws:sns:us-east-2:288044017584:SubscriptionEventHandlerTopic"))


if __name__ == '__main__':
    unittest.main()
