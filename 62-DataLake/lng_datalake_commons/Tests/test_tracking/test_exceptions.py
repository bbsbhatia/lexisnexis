import unittest

from lng_datalake_commons.tracking.exceptions import *

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestExceptions(unittest.TestCase):
    # +DataLakeException - too few passed
    def test_data_lake_exception_1(self):
        with self.assertRaisesRegex(DataLakeException,
                                    "'500||Args dont match: (\'test\\n failure\',)||Contact DL team with request-id'"):
            raise DataLakeException("test\n failure")

    # +InvalidRequestPropertyName - can create object
    def test_invalid_request_property_name(self):
        with self.assertRaisesRegex(InvalidRequestPropertyName, "400||InvalidRequestPropertyName||test failure||"):
            raise InvalidRequestPropertyName("test\n failure")

    # +NotAllowedError - can create object
    def test_not_allowed_error(self):
        with self.assertRaisesRegex(NotAllowedError, "405||test failure||"):
            raise NotAllowedError("test\n failure")

    # +InternalError - can create object
    def test_internal_error(self):
        with self.assertRaisesRegex(InternalError, "500||test failure||"):
            raise InternalError("test\n failure")

    # +UnhandledException - can create object
    def test_unhandled_error(self):
        with self.assertRaisesRegex(UnhandledException, "500||Unhandled exception occurred||foo"):
            raise UnhandledException(Exception('foo'))

    # -InternalError - stacktrace
    def test_internal_error_stacktrace(self):
        with self.assertRaisesRegex(InternalError, "500||test failure||can only join an iterable"):
            try:
                "".join(object)
            except Exception as e:
                raise InternalError("test failure", e)

    # -InternalError - exception
    def test_internal_error_exception(self):
        with self.assertRaisesRegex(InternalError, "500||InternalError||test failure||foobar"):
            raise InternalError("test failure", Exception("foobar"))


if __name__ == '__main__':
    print(unittest.main())
