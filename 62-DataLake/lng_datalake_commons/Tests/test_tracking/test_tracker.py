import unittest
from unittest import TestCase
from unittest.mock import patch, MagicMock

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import lng_datalake_commons.tracking.tracker as tracker
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, NoChangeEventException
from lng_datalake_commons.tracking.exceptions import InternalError, NotAllowedError, InvalidRequestPropertyName

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "Tracker")


class TrackerTest(TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.valid_command_lambda_event = io_utils.load_data_json("valid_command_lambda_event.json")
        cls.valid_command_lambda_create_collection_event = io_utils.load_data_json(
            "valid_command_lambda_create_collection_event.json")
        cls.valid_object_command_lambda_event = io_utils.load_data_json("valid_object_command_lambda_event.json")
        cls.valid_command_lambda_update_pending_event = \
            io_utils.load_data_json('valid_command_lambda_update_pending_event.json')
        cls.invalid_command_lambda_event = io_utils.load_data_json("invalid_command_lambda_event.json")
        cls.valid_event_handler_event = io_utils.load_data_json("valid_event_handler_event.json")
        cls.invalid_event_handler_event = io_utils.load_data_json("invalid_event_handler_event.json")
        cls.invalid_event_handler_event_missing_key = io_utils.load_data_json(
            "invalid_event_handler_event_missing_key.json")
        cls.valid_item = io_utils.load_data_json("valid_item.json")
        cls.valid_object_item = io_utils.load_data_json("valid_object_item.json")
        cls.invalid_item = io_utils.load_data_json("invalid_item.json")
        cls.other_item = io_utils.load_data_json("other_item.json")
        cls.valid_key = io_utils.load_data_json("valid_key.json")
        cls.valid_object_key = io_utils.load_data_json("valid_object_key.json")
        cls.valid_other_key = io_utils.load_data_json("valid_other_key.json")
        cls.valid_collection_blocker = io_utils.load_data_json("valid_collection_blocker.json")
        cls.valid_event_handler_object_event = io_utils.load_data_json("valid_object_event.json")
        cls.valid_create_ingestion_command_event = io_utils.load_data_json('valid_ingestion_command_event.json')
        cls.valid_command_collection_blocker_event = \
            io_utils.load_data_json('valid_command_collection_blocker_event.json')
        cls.valid_command_collection_blocker_item = \
            io_utils.load_data_json('valid_command_collection_blocker_item.json')
        cls.valid_command_collection_blocker_keys = \
            io_utils.load_data_json('valid_command_collection_blocker_keys.json')
        cls.invalid_object_command_event = io_utils.load_data_json('invalid_object_command_lambda_event.json')
        cls.invalid_object_command_event_2 = io_utils.load_data_json(
            'invalid_object_command_lambda_event_no_request_att.json')
        cls.object_store_item = io_utils.load_data_json('object_store_item.json')
        cls.valid_command_lambda_object_update = io_utils.load_data_json('valid_command_lambda_object_update.json')

    @patch("lng_datalake_commons.tracking.tracker._put_event_to_tracking_table")
    def test_track_command(self, put_event_to_tracking_table_mock):
        put_event_to_tracking_table_mock.return_value = None
        wrapper = tracker.track_command("Collection::Update")
        func_mock = MagicMock()
        func_wrapper = wrapper(func_mock)
        response = func_wrapper(event=self.valid_command_lambda_event, context=MockLambdaContext())
        put_event_to_tracking_table_mock.assert_called_once()
        self.assertIsNotNone(response)

    @patch("lng_datalake_commons.tracking.tracker._put_event_to_tracking_table")
    def test_track_command_with_create_collection(self, put_event_to_tracking_table_mock):
        put_event_to_tracking_table_mock.return_value = None
        wrapper = tracker.track_command("Collection::Create")
        expected_tracking_dict = \
            {
                'event-id': 'MLGbGPVVXwphUHOk',
                'tracking-id': '10000020|collection',
                'request-time': '2018-04-09T17:09:29.120Z',
                'api-key-id': 'MockApiKeyID',
                'event-name': 'Collection::Create'
            }
        func_mock = MagicMock()
        func_wrapper = wrapper(func_mock)
        response = func_wrapper(event=self.valid_command_lambda_create_collection_event, context=MockLambdaContext())
        put_event_to_tracking_table_mock.assert_called_once()
        put_event_to_tracking_table_mock.assert_called_with(expected_tracking_dict)
        self.assertIsNotNone(response)

    @patch("lng_datalake_commons.tracking.tracker._validate_collection")
    @patch('lng_datalake_commons.tracking.tracker.__get_expiration_epoch')
    @patch("lng_datalake_commons.tracking.tracker._put_event_to_tracking_table")
    def test_track_command_update_pending(self, put_event_to_tracking_table_mock, get_expiration_epoch_mock,
                                          validate_collection_mock):
        put_event_to_tracking_table_mock.return_value = None
        validate_collection_mock.return_value = None
        wrapper = tracker.track_command("Object::Update")
        expected_dict = {
            'event-id': 'MLGbGPVVXwphUHOk',
            'tracking-id': '10000020|123|object',
            'request-time': '2018-04-09T17:09:29.120Z',
            'event-name': 'Object::Update',
            'pending-expiration-epoch': 1539732854
        }

        get_expiration_epoch_mock.return_value = 1539732854

        def dict_mock(event, context):
            return {
                'response': {
                    "object": {
                        'object-state': 'Pending'
                    }
                }
            }

        func_mock = dict_mock
        func_wrapper = wrapper(func_mock)
        response = func_wrapper(event=self.valid_command_lambda_update_pending_event, context=MockLambdaContext())
        put_event_to_tracking_table_mock.assert_called_once()
        put_event_to_tracking_table_mock.assert_called_with(expected_dict)
        validate_collection_mock.assert_called_once()
        self.assertIsNotNone(response)

    @patch("lng_datalake_commons.tracking.tracker._validate_collection")
    @patch('lng_datalake_commons.tracking.tracker.__get_expiration_epoch')
    @patch("lng_datalake_commons.tracking.tracker._put_event_to_tracking_table")
    def test_track_command_create_pending(self, put_event_to_tracking_table_mock, get_expiration_epoch_mock,
                                          validate_collection_mock):
        put_event_to_tracking_table_mock.return_value = None
        validate_collection_mock.return_value = None
        wrapper = tracker.track_command("Object::Create")
        expected_dict = {
            'event-id': 'MLGbGPVVXwphUHOk',
            'tracking-id': '10000020|123|object',
            'request-time': '2018-04-09T17:09:29.120Z',
            'event-name': 'Object::Create',
            'pending-expiration-epoch': 1539732854
        }

        get_expiration_epoch_mock.return_value = 1539732854

        def dict_mock(event, context):
            return {
                'response': {
                    "object": {
                        'object-state': 'Pending'
                    }
                }
            }

        func_mock = dict_mock
        func_wrapper = wrapper(func_mock)
        response = func_wrapper(event=self.valid_command_lambda_update_pending_event, context=MockLambdaContext())
        put_event_to_tracking_table_mock.assert_called_once()
        put_event_to_tracking_table_mock.assert_called_with(expected_dict)
        validate_collection_mock.assert_called_once()
        self.assertIsNotNone(response)

    @patch("lng_datalake_commons.tracking.tracker._validate_collection")
    @patch("lng_datalake_commons.tracking.tracker._put_event_to_tracking_table")
    def test_track_command_update_object_without_pending(self, put_event_to_tracking_table_mock,
                                                         validate_collection_mock):
        put_event_to_tracking_table_mock.return_value = None
        validate_collection_mock.return_value = None
        wrapper = tracker.track_command("Object::Update")

        def dict_mock(event, context):
            return {
                'response': {
                    'object-state': 'Created'
                }
            }

        func_mock = dict_mock
        func_wrapper = wrapper(func_mock)
        response = func_wrapper(event=self.valid_object_command_lambda_event, context=MockLambdaContext())
        self.assertEqual(1, put_event_to_tracking_table_mock.call_count)
        put_event_to_tracking_table_mock.assert_called_with(self.valid_command_lambda_object_update)
        validate_collection_mock.assert_called_once()
        self.assertIsNotNone(response)

    @patch('lng_datalake_commons.tracking.tracker.get_collection_blocker_expiration')
    @patch("lng_datalake_commons.tracking.tracker._put_collection_blocker")
    def test_track_command_create_ingestion(self, put_collection_blocker, get_expiration_epoch_mock):
        put_collection_blocker.return_value = None
        wrapper = tracker.track_command("Object::IngestionCreate")

        get_expiration_epoch_mock.return_value = 1539732854

        def dict_mock(event, context):
            return {
                'response': {
                }
            }

        func_mock = dict_mock
        func_wrapper = wrapper(func_mock)
        response = func_wrapper(event=self.valid_create_ingestion_command_event, context=MockLambdaContext())
        put_collection_blocker.assert_called_with(self.valid_create_ingestion_command_event, "Object::IngestionCreate")
        self.assertIsNotNone(response)

    # + generate_tracking_id: success
    def test_generate_tracking_id(self):
        self.assertEqual(tracker.generate_tracking_id("collection", "123"), "123|collection")
        self.assertEqual(tracker.generate_tracking_id("catalog", "456"), "456|catalog")
        self.assertEqual(tracker.generate_tracking_id("subscription", 777), "777|subscription")
        self.assertEqual(tracker.generate_tracking_id("owner", 888), "888|owner")
        self.assertEqual(tracker.generate_tracking_id("object", "abc", "123"), "abc|123|object")

    @patch('lng_datalake_commons.tracking.tracker.get_collection_blocker_expiration')
    @patch("lng_datalake_commons.tracking.tracker._put_collection_blocker")
    def test_track_command_batch_remove(self, put_collection_blocker, get_expiration_epoch_mock):
        put_collection_blocker.return_value = None
        wrapper = tracker.track_command("Object::BatchRemove")

        get_expiration_epoch_mock.return_value = 1539732854

        def dict_mock(event, context):
            return {
                'response': {
                }
            }

        func_mock = dict_mock
        func_wrapper = wrapper(func_mock)
        response = func_wrapper(event=self.valid_create_ingestion_command_event, context=MockLambdaContext())
        put_collection_blocker.assert_called_with(self.valid_create_ingestion_command_event, "Object::BatchRemove")
        self.assertIsNotNone(response)

    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.put_item')
    def test_put_collection_blocker(self, mock_put_item):
        event = self.valid_command_collection_blocker_event
        mock_put_item.return_value = None
        self.assertIsNone(
            tracker._put_collection_blocker(event, "Object::CreateIngestion"))

    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.put_item')
    def test_put_collection_blocker_exception(self, mock_put_item):
        mock_put_item.side_effect = Exception("Mock Exception")
        event = self.valid_command_collection_blocker_event
        with self.assertRaisesRegex(InternalError, "Failed to put event into collection blocker table."):
            tracker._put_collection_blocker(event, "Object::CreateIngestion")

    # + put collection blocker condition error request IDs match success
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.put_item')
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_item')
    @patch('lng_datalake_commons.tracking.tracker._get_collection_blocker_request_id')
    def test_put_collection_blocker_condition_error_equal(self, mock_get_request_id, mock_get_item, mock_put_item):
        mock_put_item.side_effect = ConditionError("Mock Exception")
        mock_get_item.return_value = self.valid_command_collection_blocker_item
        mock_get_request_id.return_value = "MLGbGPVVXwphUHOk"
        event = self.valid_command_collection_blocker_event
        self.assertIsNone(tracker._put_collection_blocker(event, "Object::Create"))
        mock_put_item.assert_called_once_with(self.valid_command_collection_blocker_item)
        mock_get_request_id.assert_called_once_with(self.valid_command_collection_blocker_item)

    # - put collection blocker condition error request IDs do NOT match
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.put_item')
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_item')
    @patch('lng_datalake_commons.tracking.tracker._get_collection_blocker_request_id')
    def test_put_collection_blocker_internal_exception(self, mock_get_request_id, mock_get_item, mock_put_item):
        mock_put_item.side_effect = ConditionError("Condition Error")
        mock_get_item.return_value = self.valid_command_collection_blocker_item
        mock_get_request_id.return_value = "SomeOtherRequestID"
        event = self.valid_command_collection_blocker_event
        with self.assertRaisesRegex(InternalError, "Failed to put event into collection blocker.*Condition Error"):
            tracker._put_collection_blocker(event, "Object::Create")
        mock_put_item.assert_called_once_with(self.valid_command_collection_blocker_item)
        mock_get_request_id.assert_called_once_with(self.valid_command_collection_blocker_item)

    # - put collection blocker condition error request IDs not found
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.put_item')
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_item')
    @patch('lng_datalake_commons.tracking.tracker._get_collection_blocker_request_id')
    def test_put_collection_blocker_condition_exception2(self, mock_get_request_id, mock_get_item, mock_put_item):
        mock_put_item.side_effect = ConditionError("Condition Error")
        mock_get_item.return_value = self.valid_command_collection_blocker_item
        mock_get_request_id.return_value = ""
        event = self.valid_command_collection_blocker_event
        with self.assertRaisesRegex(InternalError, "Failed to put event into collection blocker.*Condition Error"):
            tracker._put_collection_blocker(event, "Object::Create")
        mock_put_item.assert_called_once_with(self.valid_command_collection_blocker_item)
        mock_get_request_id.assert_called_once_with(self.valid_command_collection_blocker_item)

    # + get collection blocker request ID
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_item')
    def test_get_collection_blocker_request_id(self, mock_get_item):
        mock_get_item.return_value = self.valid_command_collection_blocker_item
        collection_blocker_keys = self.valid_command_collection_blocker_keys
        get_item_key = {
            'collection-id': collection_blocker_keys['collection-id'],
            'request-time': collection_blocker_keys['request-time']
        }
        self.assertEqual(tracker._get_collection_blocker_request_id(collection_blocker_keys), "MLGbGPVVXwphUHOk")
        mock_get_item.assert_called_once_with(get_item_key, ConsistentRead=True)

    # + get collection blocker request ID not found
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_item')
    def test_get_collection_blocker_request_id_empty(self, mock_get_item):
        mock_get_item.return_value = {}
        collection_blocker_keys = self.valid_command_collection_blocker_keys
        get_item_key = {
            'collection-id': collection_blocker_keys['collection-id'],
            'request-time': collection_blocker_keys['request-time']
        }
        self.assertEqual(tracker._get_collection_blocker_request_id(collection_blocker_keys), "")
        mock_get_item.assert_called_once_with(get_item_key, ConsistentRead=True)

    # + get collection blocker request ID exception
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_item')
    def test_get_collection_blocker_request_id_exception(self, mock_get_item):
        mock_get_item.side_effect = Exception("Mock Exception")
        collection_blocker_keys = self.valid_command_collection_blocker_keys
        get_item_key = {
            'collection-id': collection_blocker_keys['collection-id'],
            'request-time': collection_blocker_keys['request-time']
        }
        self.assertEqual(tracker._get_collection_blocker_request_id(collection_blocker_keys), "")
        mock_get_item.assert_called_once_with(get_item_key, ConsistentRead=True)

    @patch("lng_datalake_commons.tracking.tracker._validate_collection")
    def test_track_command_fail(self, validate_collection_mock):
        validate_collection_mock.side_effect = NotAllowedError("MOCK Exception")
        wrapper = tracker.track_command("Object::Update")
        func_mock = MagicMock()
        func_wrapper = wrapper(func_mock)
        with self.assertRaisesRegex(NotAllowedError, "MOCK Exception"):
            func_wrapper(event=self.valid_command_lambda_event, context=MockLambdaContext())

    @patch("lng_datalake_commons.tracking.tracker._put_event_to_tracking_table")
    def test_track_command_fai1_1(self, put_event_to_tracking_table_mock):
        put_event_to_tracking_table_mock.side_effect = KeyError()
        wrapper = tracker.track_command("Collection::Update")
        func_mock = MagicMock()
        func_wrapper = wrapper(func_mock)
        with self.assertRaisesRegex(InternalError, "Unknown exception in track_command"):
            func_wrapper(event=self.valid_command_lambda_event, context=MockLambdaContext())

    def test_extract_namespace_state_from_event_success(self):
        self.assertEqual(tracker._extract_namespace_state_from_event("Collection::Create"), ("collection", "create"))
        self.assertEqual(tracker._extract_namespace_state_from_event("Object::Create"), ("object", "create"))
        self.assertEqual(tracker._extract_namespace_state_from_event("Subscription::Create"),
                         ("subscription", "create"))

    def test_extract_keys_for_collection_success(self):
        actual = tracker._extract_keys(event=self.valid_command_lambda_create_collection_event, namespace="collection")
        self.assertDictEqual(actual, self.valid_key)

    # + _extract_object_keys_for_command
    def test_extract_object_keys_success(self):
        actual = tracker._extract_object_keys_for_command(event=self.valid_object_command_lambda_event)
        self.assertEqual(actual, self.valid_object_key)

    # - _extract_keys - invalid request property error: no collection id
    def test_extract_keys_object_key_error_collection_id(self):
        with self.assertRaisesRegex(InvalidRequestPropertyName, "Request must include 'collection-id'"):
            tracker._extract_object_keys_for_command(event=self.invalid_object_command_event)

    # - _extract_keys - invalid request property error: no request attribute
    def test_extract_keys_object_key_error(self):
        with self.assertRaisesRegex(InternalError, "KeyError = 'request'"):
            tracker._extract_object_keys_for_command(event=self.invalid_object_command_event_2)

    def test_extract_keys_object_event_success(self):
        event_test = {
            "context": {
                "client-request-id": "ClientRequestID",
                "client-request-time": "152329376912",
                "resource-path": "/{objectId}"
            },
            "request": {
                'collection-id': '123',
                "content-type": "plain/text"
            }}
        expected_response = {"event-id": "ClientRequestID",
                             "tracking-id": "ClientRequestID|123|object",
                             "request-time": "2018-04-09T17:09:29.120Z"}
        response = tracker._extract_keys(event=event_test, namespace="object")
        self.assertEqual(response, expected_response)

    def test_extract_keys_failure(self):
        with self.assertRaises(InternalError):
            tracker._extract_keys(event=self.invalid_command_lambda_event, namespace="collection")

    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    def test_put_event_to_tracking_table_success(self, put_item_mock):
        put_item_mock.return_value = None
        self.assertIsNone(tracker._put_event_to_tracking_table(self.valid_key))

    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    def test_put_event_to_tracking_table_failure(self, put_item_mock):
        side_effects = (SchemaValidationError, ConditionError, SchemaError, SchemaLoadError, ClientError, Exception)
        for side_effect in side_effects:
            put_item_mock.side_effect = side_effect
            with self.assertRaises(InternalError):
                tracker._put_event_to_tracking_table(self.valid_key)

    # + un_track_command_by_event: Object::IngestionCreate
    @patch("lng_datalake_commons.tracking.tracker._delete_collection_blocker")
    def test_un_track_command_by_event_ingestion(self, mock_delete_cb):
        mock_delete_cb.return_value = None
        self.assertIsNone(tracker.un_track_command_by_event({}, 'Object::IngestionCreate', 'STAGE'))

    # - un_track_command_by_event: Object::IngestionCreate exception
    @patch("lng_datalake_commons.tracking.tracker._delete_collection_blocker")
    def test_un_track_command_by_event_ingestion_exception(self, mock_delete_cb):
        mock_delete_cb.side_effect = TerminalErrorException('exception')
        self.assertIsNone(tracker.un_track_command_by_event({}, 'Object::IngestionCreate', 'STAGE'))

    # + un_track_command_by_event: Object::BatchRemove
    @patch("lng_datalake_commons.tracking.tracker._delete_collection_blocker")
    def test_un_track_command_by_event_batch_remove(self, mock_delete_cb):
        mock_delete_cb.return_value = None
        self.assertIsNone(tracker.un_track_command_by_event({}, 'Object::BatchRemove', 'STAGE'))

    # - un_track_command_by_event: Object::BatchRemove exception
    @patch("lng_datalake_commons.tracking.tracker._delete_collection_blocker")
    def test_un_track_command_by_event_batch_remove_exception(self, mock_delete_cb):
        mock_delete_cb.side_effect = TerminalErrorException('exception')
        self.assertIsNone(tracker.un_track_command_by_event({}, 'Object::BatchRemove', 'STAGE'))

    # + un_track_command_by_event: Object::FolderBulkCreate
    @patch("lng_datalake_commons.tracking.tracker._delete_tracking_item")
    def test_un_track_command_by_event_object(self, mock_delete_tracking):
        mock_delete_tracking.return_value = None
        request_dict = io_utils.load_data_json('valid_command_lambda_event.json')
        self.assertIsNone(tracker.un_track_command_by_event(request_dict, 'Object::FolderBulkCreate', 'STAGE'))

    # - un_track_command_by_event: Object::FolderBulkCreate exception
    @patch("lng_datalake_commons.tracking.tracker._delete_tracking_item")
    def test_un_track_command_by_event_object_exception(self, mock_delete_tracking):
        mock_delete_tracking.side_effect = TerminalErrorException('exception')
        request_dict = io_utils.load_data_json('valid_command_lambda_event.json')
        self.assertIsNone(tracker.un_track_command_by_event(request_dict, 'Object::FolderBulkCreate', 'STAGE'))

    # + oldest tracking record for Collection
    @patch("lng_datalake_commons.tracking.tracker._is_oldest_event")
    @patch("lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id")
    @patch("lng_datalake_commons.tracking.tracker._delete_tracking_item")
    def test_track_event_handler_non_throttle(self, delete_by_event_id_mock, oldest_item,
                                              is_oldest_event_mock):
        is_oldest_event_mock.return_value = True
        oldest_item.return_value = self.valid_item
        delete_by_event_id_mock.return_value = None

        @tracker.track_event_handler("Collection::Update")
        def command(event, context):
            return "data"

        # Call command() to invoke tracker.track_event_handler
        response = command(self.valid_event_handler_event, MockLambdaContext())
        self.assertEqual(response, "data")

    # + oldest tracking record for Object
    @patch("lng_datalake_commons.tracking.tracker._is_oldest_event")
    @patch("lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id")
    @patch("lng_datalake_commons.tracking.tracker._delete_tracking_item")
    def test_track_event_handler_object_event_non_throttle(self, delete_by_event_id_mock,
                                                           oldest_item, is_oldest_event_mock):
        is_oldest_event_mock.return_value = True
        oldest_item.return_value = self.valid_object_item
        delete_by_event_id_mock.return_value = None

        @tracker.track_event_handler("Object::Update")
        def command(event, context):
            return "data"

        # Call command() to invoke tracker.track_event_handler
        response = command(self.valid_event_handler_object_event, MockLambdaContext())
        self.assertEqual(response, "data")

    # - track_event_handler: NoChangeEventException
    @patch("lng_datalake_commons.tracking.tracker._is_oldest_event")
    @patch("lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id")
    @patch("lng_datalake_commons.tracking.tracker._delete_tracking_item")
    def test_track_event_handler_no_change_event(self, delete_tracking_item_mock, oldest_item,
                                                 is_oldest_event_mock):
        is_oldest_event_mock.return_value = True
        oldest_item.return_value = self.valid_item
        delete_tracking_item_mock.return_value = None

        @tracker.track_event_handler("Collection::Update")
        def command(event, context):
            raise NoChangeEventException("Some duplicate")

        # Call command() to invoke tracker.track_event_handler
        with self.assertRaisesRegex(NoChangeEventException, 'Some duplicate'):
            command(self.valid_event_handler_event, MockLambdaContext())
        delete_tracking_item_mock.assert_called_once()

    @patch("lng_datalake_commons.tracking.tracker._is_oldest_event")
    @patch('lng_datalake_dal.tracking_table.TrackingTable.query_items')
    @patch("lng_datalake_commons.tracking.tracker._delete_tracking_item")
    def test_track_event_handler_collection_no_record_found(self, delete_by_event_id_mock,
                                                            mock_query_tracking_table, is_oldest_event_mock):
        is_oldest_event_mock.return_value = True
        mock_query_tracking_table.return_value = []
        delete_by_event_id_mock.return_value = None

        @tracker.track_event_handler("Collection::Update")
        def command(event, context):
            return "data"

        # Call command() to invoke tracker.track_event_handler
        with self.assertRaisesRegex(TerminalErrorException, "ignored because no tracking record found"):
            command(self.valid_event_handler_object_event, MockLambdaContext())

    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    @patch("lng_datalake_commons.tracking.tracker._is_oldest_event")
    @patch("lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id")
    @patch("lng_datalake_commons.tracking.tracker._delete_tracking_item")
    def test_track_event_handler_throttle(self, delete_by_event_id_mock, oldest_item, is_oldest_event_mock,
                                          throttle_error_mock):
        is_oldest_event_mock.return_value = False
        oldest_item.return_value = self.valid_item
        delete_by_event_id_mock.return_value = None
        throttle_error_mock.return_value = None
        wrapper = tracker.track_event_handler("Collection::Update")
        func_mock = MagicMock()
        func_wrapper = wrapper(func_mock)
        response = func_wrapper(event=self.valid_event_handler_event, context=MockLambdaContext())
        self.assertIsNone(response)

    def test_extract_keys_from_event_success(self):
        expected = {'event-id': 'PVOuprQJLZGIKiBa', 'event-name': 'Collection::Update', 'namespace-id': '100022',
                    'request-time': '2018-01-26T10:08:24.940004', 'stage': 'TEST'}
        self.assertDictEqual(tracker._extract_keys_from_event(self.valid_event_handler_event, namespace="collection"),
                             expected)

    def test_extract_keys_from_object_event_success(self):
        expected = {'event-id': 'PVOuprQJLZGIKiBa', 'event-name': 'Object::CreatePending',
                    'namespace-id': 'object-test',
                    'request-time': '2018-01-26T10:08:24.940004', 'stage': 'LATEST', 'collection-id': 'collection-test'}
        self.assertDictEqual(
            tracker._extract_keys_from_event(self.valid_event_handler_object_event, namespace="object"),
            expected)

    def test_extract_keys_from_event_failure(self):
        with self.assertRaises(TerminalErrorException):
            tracker._extract_keys_from_event(self.invalid_event_handler_event_missing_key, namespace="collection")

    # + test extract_keys_for_collection_blocker: with changeset-id
    def test_extract_keys_for_collection_blocker_success(self):
        expected_response = {'changeset-id': 'test-changeset',
                             'collection-id': 'test-collection',
                             'pending-expiration-epoch': 1523898569,
                             'request-id': 'aueZrsiOAYsfwhqG',
                             'request-time': '2018-04-09T17:09:29.120Z'}
        self.assertDictEqual(tracker._extract_keys_for_collection_blocker(self.valid_create_ingestion_command_event),
                             expected_response)

    def test_extract_keys_for_collection_blocker_failure(self):
        with self.assertRaisesRegex(InternalError, 'Failed to extract keys from incoming event.'):
            tracker._extract_keys_for_collection_blocker({})

    def test_is_oldest_event_success(self):
        self.assertTrue(tracker._is_oldest_event(self.other_item['event-id'], self.valid_other_key['event_id']))

    def test_is_oldest_event_failure(self):
        self.assertFalse(tracker._is_oldest_event(self.valid_item, self.valid_other_key['event_id']))

    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_oldest_item_by_tracking_id_success(self, query_items_mock):
        query_items_mock.return_value = [self.valid_item]
        self.assertEqual(tracker._get_oldest_item_by_tracking_id(tracking_id="100022_collection"), self.valid_item)

    # - _get_oldest_item_by_tracking_id - No item in tracking table
    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_oldest_item_by_tracking_id_exception_no_item(self, query_items_mock):
        query_items_mock.return_value = []
        self.assertEqual(tracker._get_oldest_item_by_tracking_id(tracking_id="100022_collection"), {})

    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_oldest_item_by_tracking_id_failure_endpoint_connection_error_exception(self, query_items_mock):
        query_items_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            tracker._get_oldest_item_by_tracking_id(tracking_id="100022_collection")

    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_oldest_item_by_tracking_id_failure_client_error(self, query_items_mock):
        query_items_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                    "Error": {"Code": "mock error code",
                                                              "Message": "mock error message"}},
                                                   "client-error-mock")
        with self.assertRaises(ClientError):
            tracker._get_oldest_item_by_tracking_id(tracking_id="100022_collection")

    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_oldest_item_by_tracking_id_failure_terminal_error(self, query_items_mock):
        query_items_mock.side_effect = Exception
        with self.assertRaises(TerminalErrorException):
            tracker._get_oldest_item_by_tracking_id(tracking_id="100022_collection")

    # + get_collection_blocker_expiration: success
    def test_get_collection_blocker_expiration(self):
        self.assertEqual(tracker.get_collection_blocker_expiration("2018-04-09T17:09:29.120Z"), 1523898569)

    # + _delete_tracking_item_success - Success
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_success(self, mock_track_delete):
        input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940004"}
        self.assertIsNone(tracker._delete_tracking_item(input, 'testing', stage='TEST'))

    # - _delete_tracking_item_ - Fail EndpointConnectionError
    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_endpoint_connection(self, mock_track_delete, mock_throttle_error):
        mock_throttle_error.return_value = None
        mock_track_delete.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940Z"}
        tracker._delete_tracking_item(input, 'testing', 'TEST')
        mock_throttle_error.assert_called_with(
            {'event-id': 'testing', 'stage': 'TEST', 'event-name': 'Tracking::Remove', 'tracking-id': 'foobar',
             'request-time': '2018-12-26T10:08:24.940Z'},
            service='Tracking')

    # - _delete_tracking_item_success - Failure delete 1
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_1(self, mock_track_delete):
        mock_track_delete.side_effect = ClientError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to delete item from tracking table"):
            input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940004"}
            tracker._delete_tracking_item(input, 'testing', 'TEST')

    # - _delete_tracking_item_success - Failure empty
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_empty(self, mock_track_delete):
        mock_track_delete.side_effect = ClientError
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to delete an item that does not exist in the tracking table"):
            input = {}
            tracker._delete_tracking_item(input, 'testing', 'TEST')

    # - _delete_tracking_item_success - Failure delete 1
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_2(self, mock_track_delete):
        mock_track_delete.side_effect = ClientError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to delete item from tracking table"):
            input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940004"}
            tracker._delete_tracking_item(input, 'testing', 'TEST')

    # - _delete_tracking_item_success - Failure delete general exception
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_4(self, mock_track_delete):
        mock_track_delete.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Failed to delete item from tracking table"):
            input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940004"}
            tracker._delete_tracking_item(input, 'testing', 'TEST')

    # - _delete_tracking_item_success - Fail delete 3
    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_5(self, mock_track_delete, mock_throttle_error):
        mock_throttle_error.return_value = None
        mock_track_delete.side_effect = ClientError({"Error": {"Code": "ThrottlingException", "Message": ""}}, "delete")
        input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940004"}
        self.assertIsNone(tracker._delete_tracking_item(input, 'testing', 'TEST'))

    # - _delete_tracking_item_success - Fail delete InternalError Throttle
    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_5_1(self, mock_track_delete, mock_throttle_error):
        mock_throttle_error.return_value = None
        mock_track_delete.side_effect = ClientError({"Error": {"Code": "InternalServerError", "Message": ""}}, "delete")
        input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940004"}
        self.assertIsNone(tracker._delete_tracking_item(input, 'testing', 'TEST'))

    # - _delete_tracking_item_success - Fail delete ProvisionedThroughputExceededException Throttle
    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_5_2(self, mock_track_delete, mock_throttle_error):
        mock_throttle_error.return_value = None
        mock_track_delete.side_effect = ClientError(
            {"Error": {"Code": "ProvisionedThroughputExceededException", "Message": ""}}, "delete")
        input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940004"}
        self.assertIsNone(tracker._delete_tracking_item(input, 'testing', 'TEST'))

    # - _delete_tracking_item_success - Fail delete 4
    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_fail_6(self, mock_track_delete, mock_throttle_error):
        mock_throttle_error.return_value = None
        mock_track_delete.side_effect = ClientError({"Error": {"Code": "UnknownException", "Message": ""}}, "delete")
        with self.assertRaisesRegex(TerminalErrorException, "Failed to delete item from tracking table"):
            input = {"tracking-id": "foobar", "request-time": "2018-12-26T10:08:24.940004"}
            tracker._delete_tracking_item(input, 'testing', 'TEST')

    # + _delete_collection_blocker - success
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_success(self, cbt_mock_delete):
        cbt_mock_delete.return_value = None
        event = self.valid_command_collection_blocker_event
        self.assertIsNone(tracker._delete_collection_blocker(event, 'LATEST'))
        cbt_mock_delete.assert_called_with({'collection-id': '10000020', 'request-time': '2018-04-09T17:09:29.120Z'})

    # - _delete_collection_blocker - key error
    def test_delete_collection_blocker_key_error(self):
        event = io_utils.load_data_json('invalid_command_lambda_event.json')
        with self.assertRaisesRegex(TerminalErrorException, "Failed to extract keys from incoming event"):
            tracker._delete_collection_blocker(event, 'LATEST')

    # - _delete_collection_blocker - retry exception on EndpointConnectionError
    @patch('lng_datalake_commons.error_handling.error_handler.throttle_error')
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_endpoint_connection_error_exception(self, cbt_mock_delete, throttle_mock):
        cbt_mock_delete.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        throttle_mock.return_value = None
        event = self.valid_command_collection_blocker_event
        tracker._delete_collection_blocker(event, 'LATEST')
        throttle_mock.assert_called_with(
            {'collection-id': '10000020', 'event-id': 'MLGbGPVVXwphUHOk', 'stage': 'LATEST',
             'request-time': '2018-04-09T17:09:29.120Z', 'event-name': 'Tracking::Remove'},
            service='Tracking')

    # - _delete_collection_blocker - retry exception
    @patch('lng_datalake_commons.error_handling.error_handler.throttle_error')
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_client_error(self, cbt_mock_delete, throttle_mock):
        cbt_mock_delete.side_effect = ClientError({"ResponseMetadata": {},
                                                   "Error": {"Code": "ThrottlingException",
                                                             "Message": "mock error message"}},
                                                  "client-error-mock")
        throttle_mock.return_value = None
        event = self.valid_command_collection_blocker_event
        tracker._delete_collection_blocker(event, 'LATEST')
        throttle_mock.assert_called_with(
            {'collection-id': '10000020', 'event-id': 'MLGbGPVVXwphUHOk', 'stage': 'LATEST',
             'request-time': '2018-04-09T17:09:29.120Z', 'event-name': 'Tracking::Remove'}, service='Tracking')

    # - _delete_collection_blocker - non-retry exception
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_internal_error(self, cbt_mock_delete):
        client_error = ClientError({"ResponseMetadata": {},
                                    "Error": {"Code": "Mock Response",
                                              "Message": "mock error message"}},
                                   "client-error-mock")
        cbt_mock_delete.side_effect = client_error
        event = self.valid_command_collection_blocker_event
        with self.assertRaisesRegex(TerminalErrorException,
                                    "when calling the client-error-mock operation: mock error message"):
            tracker._delete_collection_blocker(event, 'LATEST')

    # - _delete_collection_blocker - general exception
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_general_exception(self, cbt_mock_delete):
        cbt_mock_delete.side_effect = Exception('general exception')
        event = self.valid_command_collection_blocker_event
        with self.assertRaisesRegex(TerminalErrorException,
                                    'general exception'):
            tracker._delete_collection_blocker(event, 'LATEST')

    # + _validate_collection - success
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_validate_collection_success(self, query_collection_blocker_table_mock):
        query_collection_blocker_table_mock.return_value = []
        self.assertIsNone(tracker._validate_collection(self.valid_command_collection_blocker_event))

    # - _validate_collection - TerminalError - fail to obtain the collection blocker
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_validate_collection_terminal_error(self, query_collection_blocker_table_mock):
        query_collection_blocker_table_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                                       "Error": {"Code": "mock error code",
                                                                                 "Message": "mock error message"}},
                                                                      "client-error-mock")
        with self.assertRaisesRegex(InternalError, "Failed obtaining the collection blocker"):
            tracker._validate_collection(self.valid_command_collection_blocker_event)

    # - _validate_collection - NotAllowedError - existing collection blocker
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_validate_collection_not_allowed_error(self, query_collection_blocker_table_mock):
        query_collection_blocker_table_mock.return_value = [self.valid_collection_blocker]
        collection_id = '10000020'
        ingestion_id = '80297675-3c4d-11e9-8767-8bced846a1d7'
        with self.assertRaisesRegex(NotAllowedError,
                                    "Collection ID {} temporarily locked by Ingestion ID {}".format(collection_id,
                                                                                                    ingestion_id)):
            tracker._validate_collection(self.valid_command_collection_blocker_event)

    # - _validate_collection - invalid request property error: no collection id
    def test_extract_keys_for_validate_collection_key_error(self):
        with self.assertRaisesRegex(InvalidRequestPropertyName, "Request must include 'collection-id'"):
            tracker._validate_collection(event=self.invalid_object_command_event)

    # + _get_expiration_epoch_for_upload_events: folder
    @patch('lng_datalake_commons.tracking.tracker.__get_expiration_epoch')
    def test_get_expiration_epoch_for_upload_events_folder(self, get_expiration_epoch):
        get_expiration_epoch.return_value = 123
        self.assertEqual(tracker._get_expiration_epoch_for_upload_events('/{objectId}/folder'), 123)
        get_expiration_epoch.assert_called_with(1.25)

    # + _get_expiration_epoch_for_upload_events: large
    @patch('lng_datalake_commons.tracking.tracker.__get_expiration_epoch')
    def test_get_expiration_epoch_for_upload_events_large(self, get_expiration_epoch):
        get_expiration_epoch.return_value = 123
        self.assertEqual(tracker._get_expiration_epoch_for_upload_events('/{objectId}/large'), 123)
        get_expiration_epoch.assert_called_with(3)

    # + _get_expiration_epoch_for_upload_events: multipart
    @patch('lng_datalake_commons.tracking.tracker.__get_expiration_epoch')
    def test_get_expiration_epoch_for_upload_events_multipart(self, get_expiration_epoch):
        get_expiration_epoch.return_value = 123
        self.assertEqual(tracker._get_expiration_epoch_for_upload_events('/{objectId}/multipart'), 123)
        get_expiration_epoch.assert_called_with(3)

    # +


if __name__ == '__main__':
    unittest.main()
