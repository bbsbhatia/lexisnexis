import unittest

from lng_datalake_commons import sqs_extractor
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_testhelper.io_utils import IOUtils

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'SqsExtractor')

class TestPass(unittest.TestCase):


    # +sqs_extractor.extract_sqs_message - returns message with no filtering
    def test_validate_1(self):
        input_json = io_util.load_data_json("valid_sqs_event_msg.json")
        sqs_out = sqs_extractor.extract_sqs_message(input_json)
        expected = io_util.load_data_json("valid_sqs_msg_extracted.json")
        self.assertDictEqual(sqs_out, expected)


    # -sqs_extractor.extract_sqs_message - No sqs record -> TerminalErrorException
    def test_validate_fail_1(self):
        with self.assertRaisesRegex(TerminalErrorException, "Empty SQS record supplied for message extraction"):
            sqs_extractor.extract_sqs_message({})

    # -sqs_extractor.extract_sqs_message - No Records
    def test_validate_fail_2(self):
        input_json = io_util.load_data_json("invalid_sqs_event_msg_body_missing.json")
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Extraction of SQS message failed. Key = .*? does not exist."):
            sqs_extractor.extract_sqs_message(input_json)

    # -sqs_extractor.extract_sqs_message - message empty -> TerminalErrorException
    def test_validate_fail_3(self):
        input_json = io_util.load_data_json("invalid_sqs_event_msg_message_empty.json")
        with self.assertRaisesRegex(TerminalErrorException, "No message exists in SQS Notification"):
            sqs_extractor.extract_sqs_message(input_json)

    # -sqs_extractor.extract_sqs_message - JSONDecodeError -> TerminalErrorException
    def test_validate_fail_4(self):
        input_json = io_util.load_data_json("invalid_sqs_event_msg_deserialize_error.json")
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Cannot load message: .*? due to .*? "):
            sqs_extractor.extract_sqs_message(input_json)


if __name__ == '__main__':
    unittest.main()
