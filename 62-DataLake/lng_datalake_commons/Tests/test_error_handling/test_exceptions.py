import unittest

from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException, \
    IgnoreEventException, NoChangeEventException

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestPass(unittest.TestCase):

    # +exceptions - can raise Exceptions
    def test_io_utils(self):
        msg = "Fail"
        with self.assertRaisesRegex(TerminalErrorException, msg):
            raise TerminalErrorException(msg)

        with self.assertRaisesRegex(RetryEventException, msg):
            raise RetryEventException(msg)

        with self.assertRaisesRegex(IgnoreEventException, msg):
            raise IgnoreEventException(msg)

        with self.assertRaisesRegex(NoChangeEventException, msg):
            raise NoChangeEventException(msg)


if __name__ == '__main__':
    unittest.main()
