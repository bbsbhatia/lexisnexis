import unittest
from datetime import datetime
from unittest import TestCase
from unittest import mock
from unittest.mock import patch

import orjson
from botocore.exceptions import ClientError, ReadTimeoutError, EndpointConnectionError, IncompleteReadError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import lng_datalake_commons.error_handling.error_handler as error_handler
from lng_datalake_commons.error_handling.error_handler import handle
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException, \
    IgnoreEventException, NoChangeEventException

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'ErrorHandler')


class ErrorHandlerTest(TestCase):

    # +handle - Happy path
    def test_handle_event_wrapper(self):
        payload = io_utils.load_data_json("valid_event.json")
        success = 'Complete'

        @handle
        def test_handle(payload, context):
            return success

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            self.assertEqual(test_handle(payload, MockLambdaContext), success)
            log_mock.assert_not_called()

    # -handle - TerminalErrorException
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_handle_event_wrapper_fail_1(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise TerminalErrorException("Terminal Error Exception Test")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            test_handle(payload, MockLambdaContext)
            log_mock.exception.assert_called_once()

    # -handle - IgnoreEventException
    def test_handle_event_wrapper_fail_ignore_exception(self):
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise IgnoreEventException("Ignore Event Exception Test")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            self.assertIsNone(test_handle(payload, MockLambdaContext))
            log_mock.debug.assert_called_once()

    # -handle - ClientError
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_handle_event_wrapper_fail_2(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise ClientError({"ResponseMetadata": {},
                               "Error": {"Code": "mock error code",
                                         "Message": "mock error message"}},
                              "client-error-mock")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            test_handle(payload, MockLambdaContext)
            log_mock.exception.assert_called_once()

    # -handle - RetryEventException
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_handle_event_wrapper_fail_4(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise RetryEventException("RetryEventException Test")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            test_handle(payload, MockLambdaContext)
            log_mock.exception.assert_called_once()

    # -handle - NoChangeEventException
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_handle_event_wrapper_fail_10(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise NoChangeEventException("NoChangeEventException Test")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            test_handle(payload, MockLambdaContext)
            log_mock.exception.assert_not_called()

    # -handle - EndpointConnectionError
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_handle_event_wrapper_fail_11(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise EndpointConnectionError(endpoint_url="https://fake.content.aws.lexis.com")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            test_handle(payload, MockLambdaContext)
            log_mock.exception.assert_called_once()

    # -handle - ReadTimeoutError
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_handle_event_wrapper_fail_5(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise ReadTimeoutError(endpoint_url="test")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            test_handle(payload, MockLambdaContext)
            log_mock.exception.assert_called_once()

    # -handle - IncompleteReadError
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_handle_event_wrapper_fail_incomplete_read(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise IncompleteReadError(endpoint_url="test")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            test_handle(payload, MockLambdaContext)
            log_mock.exception.assert_called_once()

    # -handle - Exception
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_handle_event_wrapper_fail_3(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        @handle
        def test_handle(payload, context):
            raise Exception("Exception Test")

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            test_handle(payload, MockLambdaContext)
            log_mock.exception.assert_called_once()

    # +get_event_id:
    def test_get_event_id_valid_case(self):
        valid_event = io_utils.load_data_json("valid_event.json")
        expected = "WNeqfaiHsdDuuFkS"
        actual = error_handler.get_event_id(valid_event)
        self.assertEqual(actual, expected)

    # -get_event_id: Returns empty string
    def test_get_event_id_invalid_case(self):
        expected = ""
        invalid_event = io_utils.load_data_json("invalid_event.json")
        actual = error_handler.get_event_id(invalid_event)
        self.assertEqual(actual, expected)

    # -get_event_id - Test each exception type
    @patch('json.loads')
    def test_get_event_id_fail_4(self, json_loads_mock):
        payload = io_utils.load_data_json("valid_event.json")
        for side_effect in [KeyError, Exception, ValueError, TypeError]:
            json_loads_mock.side_effect = side_effect
            with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
                error_handler.get_event_id(payload)
                log_mock.critical.assert_called_once()

    # +compute_uid_of_context: happy path
    def test_get_uid_of_context(self):
        actual = error_handler.compute_uid_of_context(MockLambdaContext)
        self.assertRegex(actual, '.*?_563r1jug9i')

    # -compute_uid_of_context: Exception
    @patch('time.time')
    def test_get_uid_of_context_fail_5(self, time_mock):
        time_mock.side_effect = Exception
        expected = ""
        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            actual = error_handler.compute_uid_of_context(MockLambdaContext)
            log_mock.critical.assert_called_once()
            self.assertEqual(actual, expected)

    # +compute_uid_of_payload: happy path
    def test_compute_uid_of_payload(self):
        payload = io_utils.load_data_json("valid_event.json")
        actual = error_handler.compute_uid_of_payload(payload)
        self.assertRegex(actual, '.*?_9a8be49a547997d70dfe8306c3082475')

    # -compute_uid_of_payload: Exception
    @patch('orjson.dumps')
    def test_compute_uid_of_payload_fail_6(self, jdumps_mock):
        payload = io_utils.load_data_json("valid_event.json")
        expected = ""
        for side_effect in [TypeError, Exception]:
            jdumps_mock.side_effect = side_effect
            with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
                actual = error_handler.compute_uid_of_payload(payload)
                log_mock.critical.assert_called_once()
                self.assertEqual(actual, expected)

    # +terminal_error: happy path - no additional attributes
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.s3.is_valid_bucket_name")
    @patch("lng_aws_clients.s3.put_s3_object")
    @patch("lng_datalake_commons.error_handling.error_handler.datetime")
    def test_terminal_error_fail_7(self, mocked_datetime, mocked_put_s3_object, mocked_is_valid_bucket_name, _):
        valid_event = io_utils.load_data_json("valid_event.json")
        mocked_is_valid_bucket_name.return_value = True
        mocked_put_s3_object.return_value = None
        mocked_datetime.utcnow.return_value = datetime(2019, 4, 10, 12)

        expected = None
        error_handler.lambda_name = "CollectionEventHandler"
        error_message = "Test error message"
        error_handler.additional_attributes = {}
        actual = error_handler.terminal_error(valid_event, MockLambdaContext, error_message=error_message)
        self.assertEqual(actual, expected)
        body = {
            "original-payload": valid_event,
            "error-message": error_message,
            "log-group-name": MockLambdaContext.log_group_name,
            "log-stream-name": MockLambdaContext.log_stream_name,
            "aws-request-id": MockLambdaContext.aws_request_id
        }
        mocked_put_s3_object.assert_called_once_with(orjson.dumps(body).decode(), None,
                                                     "CollectionEventHandler/2019/04/10/12/WNeqfaiHsdDuuFkS",
                                                     content_type="application/json")

    # +terminal_error: happy path - with additional attributes
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.s3.is_valid_bucket_name")
    @patch("lng_aws_clients.s3.put_s3_object")
    @patch("lng_datalake_commons.error_handling.error_handler.datetime")
    def test_terminal_error_fail_8(self, mocked_datetime, mocked_put_s3_object, mocked_is_valid_bucket_name, _):
        valid_event = io_utils.load_data_json("valid_event.json")
        mocked_is_valid_bucket_name.return_value = True
        mocked_put_s3_object.return_value = None
        mocked_datetime.utcnow.return_value = datetime(2019, 4, 10, 12)

        expected = None
        error_handler.lambda_name = "CollectionEventHandler"
        error_message = "Test error message"
        error_handler.additional_attributes = {"test_attribute": "mock additional attribute"}
        actual = error_handler.terminal_error(valid_event, MockLambdaContext, error_message=error_message)
        self.assertEqual(actual, expected)
        body = {
            "original-payload": valid_event,
            "error-message": error_message,
            "additional-attributes": error_handler.additional_attributes,
            "log-group-name": MockLambdaContext.log_group_name,
            "log-stream-name": MockLambdaContext.log_stream_name,
            "aws-request-id": MockLambdaContext.aws_request_id
        }
        mocked_put_s3_object.assert_called_once_with(orjson.dumps(body).decode(), None,
                                                     "CollectionEventHandler/2019/04/10/12/WNeqfaiHsdDuuFkS",
                                                     content_type="application/json")

    # +throttle_error: happy path DynamoDB throttle error
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_dynamodb_throttle_error(self, sqs_client_mock, is_valid_queue_url_mock, is_valid_message_mock, _):
        valid_event = io_utils.load_data_json("valid_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        expected = None
        sqs_client_mock.return_value.send_message.return_value = expected
        self.assertIsNone(error_handler.throttle_error(valid_event, "DynamoDB", MockLambdaContext, ))

    # +throttle_error: happy path Sns throttle error
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_sns_throttle_error(self, sqs_client_mock, is_valid_queue_url_mock, is_valid_message_mock, _):
        valid_event = io_utils.load_data_json("valid_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        sqs_client_mock.return_value.send_message.return_value = None
        self.assertIsNone(error_handler.throttle_error(valid_event, "Sns", MockLambdaContext))

    # +throttle_error: updating the payload with additional attributes
    @patch("lng_datalake_commons.error_handling.error_handler._add_attributes_to_payload")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_sns_throttle_error_additional_attributes(self, sqs_client_mock, is_valid_queue_url_mock,
                                                      is_valid_message_mock, add_attribute_mock):
        error_handler.additional_attributes = {"test_attribute": "mock additional attribute"}
        valid_event = io_utils.load_data_json("valid_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        add_attribute_mock.return_value = io_utils.load_data_json("valid_event_updated.json")
        sqs_client_mock.return_value.send_message.return_value = None
        self.assertIsNone(error_handler.throttle_error(valid_event, "Sns", MockLambdaContext, ))

    # +throttle_error: happy path S3 throttle error
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_s3_throttle_error(self, sqs_client_mock, is_valid_queue_url_mock, is_valid_message_mock, _):
        valid_event = io_utils.load_data_json("valid_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        sqs_client_mock.return_value.send_message.return_value = None
        self.assertIsNone(error_handler.throttle_error(valid_event, "S3", MockLambdaContext))

    # +throttle_error: happy path S3 throttle error
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_glue_throttle_error(self, sqs_client_mock, is_valid_queue_url_mock, is_valid_message_mock, _):
        valid_event = io_utils.load_data_json("valid_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        sqs_client_mock.return_value.send_message.return_value = None
        self.assertIsNone(error_handler.throttle_error(valid_event, "Glue", MockLambdaContext))

    # +throttle_error: happy path Retry throttle error
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_retry_throttle_error(self, sqs_client_mock, is_valid_queue_url_mock, is_valid_message_mock, _):
        valid_event = io_utils.load_data_json("valid_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        sqs_client_mock.return_value.send_message.return_value = None
        self.assertIsNone(error_handler.throttle_error(valid_event, "Retry", MockLambdaContext))

    # +throttle_error: happy path for errors that set DelaySeconds to 900
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_service_throttle_error_900(self, sqs_client_mock, is_valid_queue_url_mock, is_valid_message_mock,
                                        _):
        valid_event = io_utils.load_data_json("valid_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        sqs_client_mock.return_value.send_message.return_value = None

        for service in ('ServiceUnavailable', 'EndpointConnectionError'):
            self.assertIsNone(error_handler.throttle_error(valid_event, service, MockLambdaContext))
            send_message_kwargs = sqs_client_mock.return_value.send_message.call_args[1]
            self.assertEqual(send_message_kwargs['DelaySeconds'], 900)

    # +throttle_error: happy path for errors that does not specify DelaySeconds
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_read_timeout_throttle_error(self, sqs_client_mock, is_valid_queue_url_mock, is_valid_message_mock, _):
        valid_event = io_utils.load_data_json("valid_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        sqs_client_mock.return_value.send_message.return_value = None

        for service in ("DynamoDB", "Sns", "S3", "Glue", "Retry"):
            self.assertIsNone(error_handler.throttle_error(valid_event, service, MockLambdaContext))
            send_message_kwargs = sqs_client_mock.return_value.send_message.call_args[1]
            self.assertNotIn('DelaySeconds', send_message_kwargs)

    # +throttle_error: happy path Tracking error that uses DelaySeconds=300
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_aws_clients.sqs.is_valid_message")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.get_client")
    def test_retry_tracking_error(self, sqs_client_mock, is_valid_queue_url_mock, is_valid_message_mock, _):
        valid_event = io_utils.load_data_json("valid_tracking_event.json")
        is_valid_message_mock.return_value = True
        is_valid_queue_url_mock.return_value = True
        sqs_client_mock.return_value.send_message.return_value = None
        self.assertIsNone(error_handler.throttle_error(valid_event, "Tracking", MockLambdaContext))
        send_message_kwargs = sqs_client_mock.return_value.send_message.call_args[1]
        self.assertEqual(send_message_kwargs['DelaySeconds'], 300)

    # -terminal_error: ClientError, Exception
    @patch("lng_datalake_commons.error_handling.error_handler._handle_terminal_error")
    def test_terminal_error_exceptions_fail_8(self, handle_terminal_error_mock):
        payload = io_utils.load_data_json("valid_event.json")
        for side_effect in [ClientError({"ResponseMetadata": {},
                                         "Error": {"Code": "mock error code",
                                                   "Message": "mock error message"}},
                                        "client-error-mock"), Exception]:
            handle_terminal_error_mock.side_effect = side_effect
            with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
                self.assertIsNone(error_handler.terminal_error(payload))
                self.assertTrue(log_mock.error.call_count, 2)

    # + terminal_error: happy path - object event for ingestion
    @patch("lng_datalake_commons.error_handling.error_handler._update_ingestion_error_count")
    @patch("lng_datalake_commons.error_handling.error_handler._handle_terminal_error")
    def test_terminal_error_ingestion_update(self, handle_terminal_error_mock, update_ingestion_mock):
        valid_event = io_utils.load_data_json("valid_event_object.json")
        handle_terminal_error_mock.return_value = None
        update_ingestion_mock.return_value = None
        self.assertIsNone(error_handler.terminal_error(valid_event, MockLambdaContext))

    # - terminal_error: Exception for invalid message - object event for ingestion
    @patch("lng_datalake_commons.error_handling.error_handler._handle_terminal_error")
    def test_terminal_error_ingestion_update_invalid_message(self, handle_terminal_error_mock):
        valid_event = io_utils.load_data_json("invalid_event_object.json")
        handle_terminal_error_mock.return_value = None
        self.assertIsNone(error_handler.terminal_error(valid_event, MockLambdaContext))

    # - terminal_error: Exception for invalid message - object event for create ingestion
    @patch("lng_datalake_commons.error_handling.error_handler._handle_terminal_error")
    @patch("lng_datalake_commons.error_handling.error_handler._update_ingestion_error_count")
    def test_terminal_error_create_ingestion_update_invalid_message(self, mock_error_count, handle_terminal_error_mock):
        valid_event = io_utils.load_data_json("valid_event_create_ingestion.json")
        handle_terminal_error_mock.return_value = None
        self.assertIsNone(error_handler.terminal_error(valid_event, MockLambdaContext))
        mock_error_count.assert_not_called()

    # + update_ingestion_error_count: valid ingestion id
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_ingestion_error_count_success(self, sqs_mock):
        sqs_mock.return_value.send_message.return_value = None
        ingestion_id = 'ingestion-01'
        self.assertIsNone(error_handler._update_ingestion_error_count(ingestion_id))

    # - update_ingestion_error_count: invalid client error
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_ingestion_error_count_client_error(self, sqs_mock):
        sqs_mock.side_effect = ClientError({'ResponseMetadata': {},
                                            'Error': {
                                                'Code': 'OTHER',
                                                'Message': 'This is a mock'}},
                                           "FAKE")
        ingestion_id = 'ingestion-01'
        self.assertIsNone(error_handler._update_ingestion_error_count(ingestion_id))

    # - update_ingestion_error_count: invalid general exception
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_ingestion_error_count_exception(self, sqs_mock):
        sqs_mock.side_effect = Exception
        ingestion_id = 'ingestion-01'
        self.assertIsNone(error_handler._update_ingestion_error_count(ingestion_id))

    # -_set_uid_fail_retry_strategy: uid not present in payload
    @patch("lng_datalake_commons.error_handling.error_handler.get_event_id")
    @patch("lng_datalake_commons.error_handling.error_handler.compute_uid_of_context")
    def test_set_uid_fail_retry_strategy_fail_9(self, compute_uid_of_context_mock, get_event_id_mock):
        get_event_id_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")
        compute_uid_of_context_mock.return_value = "uid1"
        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            self.assertEqual(error_handler._set_uid_fail_retry_strategy(MockLambdaContext, True, payload, "uid3"),
                             "uid1")
            log_mock.error.assert_called_once()

    # +_set_uid_for_non_event: happy paths
    @patch("lng_datalake_commons.error_handling.error_handler.compute_uid_of_payload")
    @patch("lng_datalake_commons.error_handling.error_handler.compute_uid_of_context")
    def test_set_uid_for_non_event(self, compute_uid_of_context_mock, compute_uid_of_payload_mock):
        compute_uid_of_context_mock.return_value = "uid1"
        compute_uid_of_payload_mock.return_value = "uid2"
        payload = io_utils.load_data_json("valid_event.json")
        self.assertEqual(error_handler._set_uid_for_non_event(MockLambdaContext, False, payload, "uid3"), "uid1")
        self.assertEqual(error_handler._set_uid_for_non_event(None, False, payload, "uid3"), "uid2")

    # +handle_client_error: transaction conditional exception
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    def test_handle_client_error_transaction_conditional(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        class TransactionCanceledException:
            response = {
                "Error": {"Code": "TransactionCanceledException",
                          "Message": 'Transaction cancelled, please refer cancellation reasons for specific reasons [None, ConditionalCheckFailed]'}}

        self.assertIsNone(
            error_handler.handle_client_error(payload, MockLambdaContext, TransactionCanceledException))
        terminal_error_mock.assert_called_with(payload, MockLambdaContext,
                                               error_message=str(TransactionCanceledException))

    # +handle_client_error: terminal_error
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    def test_handle_client_error_terminal(self, terminal_error_mock):
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        class NonThrottleException:
            response = {
                "Error": {"Code": "Exception",
                          "Message": "Non Throttling Exception"}}
            args = ("Non Throttling error message",)

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, NonThrottleException))
        terminal_error_mock.assert_called_with(payload, MockLambdaContext, error_message=str(NonThrottleException))

    # +handle_client_error: throttle_error
    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    def test_handle_client_error_throttle(self, throttle_error_mock):
        throttle_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")

        class ProvisionedThroughputExceededException:
            response = {
                "Error": {"Code": "ProvisionedThroughputExceededException",
                          "Message": "Provisioned Throughput Exceeded Exception"}}

        self.assertIsNone(
            error_handler.handle_client_error(payload, MockLambdaContext, ProvisionedThroughputExceededException))
        throttle_error_mock.assert_called_with(payload, "DynamoDB", MockLambdaContext)

        class TransactionInProgressException:
            response = {
                "Error": {"Code": "TransactionInProgressException",
                          "Message": "Transaction In Progress Exception"}}

        self.assertIsNone(
            error_handler.handle_client_error(payload, MockLambdaContext, TransactionInProgressException))
        throttle_error_mock.assert_called_with(payload, "DynamoDB", MockLambdaContext)

        class TransactionCanceledException:
            response = {
                "Error": {"Code": "TransactionCanceledException",
                          "Message": 'Transaction cancelled, please refer cancellation reasons for specific reasons [None, TransactionConflict]'}}

        self.assertIsNone(
            error_handler.handle_client_error(payload, MockLambdaContext, TransactionCanceledException))
        throttle_error_mock.assert_called_with(payload, "DynamoDB", MockLambdaContext)

        class InternalServerErrorException:
            response = {
                "Error": {"Code": "InternalServerError",
                          "Message": "InternalServerError"}}

        self.assertIsNone(
            error_handler.handle_client_error(payload, MockLambdaContext, InternalServerErrorException))
        throttle_error_mock.assert_called_with(payload, "DynamoDB", MockLambdaContext)

        class ThrottlingException:
            response = {
                "Error": {"Code": "ThrottlingException",
                          "Message": "Throttling Exception"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, ThrottlingException))
        throttle_error_mock.assert_called_with(payload, "Sns", MockLambdaContext)

        class Throttling:
            response = {
                "Error": {"Code": "Throttling",
                          "Message": "An error occurred (Throttling) when calling the Publish operation (reached max retries: 4): Rate exceeded"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, Throttling))
        throttle_error_mock.assert_called_with(payload, "Sns", MockLambdaContext)

        class ValidationException:
            response = {
                "Error": {"Code": "ValidationException",
                          "Message": "Cannot read from backfilling global secondary index XXXX. Please wait and retry again"}}

        error_handler.handle_client_error(payload, MockLambdaContext, ValidationException)
        throttle_error_mock.assert_called_with(payload, "DynamoDB", MockLambdaContext)
        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, ValidationException))

        class SlowDownException:
            response = {
                "Error": {"Code": "SlowDown",
                          "Message": "Please reduce your request rate."}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, SlowDownException))
        throttle_error_mock.assert_called_with(payload, "S3", MockLambdaContext)

        class ConcurrentException:
            response = {
                "Error": {"Code": "ConcurrentRunsExceededException",
                          "Message": "Concurrent runs exceeded for release-GlueBulkProcessor"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, ConcurrentException))
        throttle_error_mock.assert_called_with(payload, "Glue", MockLambdaContext)

        class OperationTimeoutException:
            response = {
                "Error": {"Code": "OperationTimeoutException",
                          "Message": "Operation timeout for release-GlueBulkProcessor"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, OperationTimeoutException))
        throttle_error_mock.assert_called_with(payload, "Glue", MockLambdaContext)

        class InternalServiceException:
            response = {
                "Error": {"Code": "InternalServiceException",
                          "Message": "Internal error for release-GlueBulkProcessor"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, InternalServiceException))
        throttle_error_mock.assert_called_with(payload, "Glue", MockLambdaContext)

        class ResourceNumberLimitExceededException:
            response = {
                "Error": {"Code": "ResourceNumberLimitExceededException",
                          "Message": "Resource limits not met for release-GlueBulkProcessor"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, ResourceNumberLimitExceededException))
        throttle_error_mock.assert_called_with(payload, "Glue", MockLambdaContext)

        class AWSGlueException:
            response = {
                "Error": {"Code": "AWSGlueException",
                          "Message": "Glue exception occurred for release-GlueBulkProcessor"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, AWSGlueException))
        throttle_error_mock.assert_called_with(payload, "Glue", MockLambdaContext)

        class ServiceUnavailableException:
            response = {
                "Error": {"Code": "ServiceUnavailable",
                          "Message": "ServiceUnavailable Exception"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, ServiceUnavailableException))
        throttle_error_mock.assert_called_with(payload, "ServiceUnavailable", MockLambdaContext)

        class ServiceUnavailable503Exception:
            response = {
                "Error": {"Code": "503",
                          "Message": "ServiceUnavailable Exception"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, ServiceUnavailableException))
        throttle_error_mock.assert_called_with(payload, "ServiceUnavailable", MockLambdaContext)

        class InvalidClientTokenIdException:
            response = {
                "Error": {"Code": "InvalidClientTokenId",
                          "Message": "InvalidClientTokenId Exception"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, InvalidClientTokenIdException))
        throttle_error_mock.assert_called_with(payload, "InvalidClientTokenId", MockLambdaContext)

        class InternalErrorException:
            response = {
                "Error": {"Code": "InternalError",
                          "Message": "InternalError Exception"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, InternalErrorException))
        throttle_error_mock.assert_called_with(payload, "S3", MockLambdaContext)

        class S3InternalErrorException:
            response = {
                "Error": {"Code": "500",
                          "Message": "Internal Server Error"}}

        self.assertIsNone(error_handler.handle_client_error(payload, MockLambdaContext, S3InternalErrorException))
        throttle_error_mock.assert_called_with(payload, "S3", MockLambdaContext)

    # -throttle_error: Client Error => terminal_error
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    @patch("lng_datalake_commons.error_handling.error_handler._validation_fail")
    @patch("lng_aws_clients.sqs.is_valid_queue_url")
    @patch("lng_aws_clients.sqs.is_valid_message")
    def test_throttle_error_exceptions_fail_10(self, is_valid_message_mock, is_valid_queue_url_mock,
                                               validation_fail_mock,
                                               terminal_error_mock):
        is_valid_queue_url_mock.return_value = False
        is_valid_message_mock.return_value = False
        terminal_error_mock.return_value = None
        payload = io_utils.load_data_json("valid_event.json")
        for side_effect in [ClientError({"ResponseMetadata": {},
                                         "Error": {"Code": "mock error code",
                                                   "Message": "mock error message"}},
                                        "client-error-mock"), Exception]:
            validation_fail_mock.side_effect = side_effect
            with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
                error_handler.throttle_error(payload, service="DynamoDB")
                log_mock.error.assert_called_once()

    # +_validation_fail: for sqs and message
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    def test_validation_fail(self, terminal_error_mock):
        payload = io_utils.load_data_json("valid_event.json")
        terminal_error_mock.return_value = None
        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            self.assertIsNone(error_handler._validation_fail(payload, MockLambdaContext(), False, True, False))
            log_mock.error.assert_called_once()

        with mock.patch('lng_datalake_commons.error_handling.error_handler.logger') as log_mock:
            self.assertIsNone(error_handler._validation_fail(payload, MockLambdaContext(), False, False, False))
            self.assertTrue(log_mock.error.call_count, 2)

    # +update the payload with additional attributes
    def test_add_attributes_to_payload(self):
        valid_event = io_utils.load_data_json("valid_event.json")
        error_handler.additional_attributes = {"test_attribute": "mock additional attribute"}
        expected_updated_event = io_utils.load_data_json("valid_event_updated.json")
        self.assertDictEqual(error_handler._add_attributes_to_payload(valid_event), expected_updated_event)

    # +update the payload for tracking error with additional attributes
    def test_add_attributes_to_payload_tracking_error(self):
        valid_event = io_utils.load_data_json("valid_tracking_event.json")
        error_handler.additional_attributes = {"test_attribute": "mock additional attribute"}
        expected_updated_event = io_utils.load_data_json("valid_tracking_event_updated.json")
        self.assertDictEqual(error_handler._add_attributes_to_payload(valid_event), expected_updated_event)

    # +set additional attributes
    def test_set_message_additional_attribute(self):
        error_handler.set_message_additional_attribute("test_attribute", "mock attribute value")
        self.assertEqual(error_handler.additional_attributes, {"test_attribute": "mock attribute value"})

    # +update the payload event name for upsert
    def test_reclassify_payload_event_name(self):
        valid_event = io_utils.load_data_json("valid_event.json")
        error_handler.reclassified_event = {"event-name": "mock_event"}
        expected_updated_event = io_utils.load_data_json("valid_event_updated_upsert.json")
        self.assertDictEqual(error_handler._reclassify_payload_event_name(valid_event), expected_updated_event)

    # +update the payload of tracking error for upsert
    def test_reclassify_payload_event_name_tracking_error(self):
        valid_event = io_utils.load_data_json("valid_tracking_event.json")
        error_handler.reclassified_event = {"event-name": "Object::Create"}
        expected_updated_event = {
            "event-id": "WgAWzjNjGuGTRjFa",
            "event-name": "Object::Create"
        }
        self.assertDictEqual(error_handler._reclassify_payload_event_name(valid_event), expected_updated_event)

    # +set reclassify event
    def test_set_reclassify_event(self):
        error_handler.reclassify_event_name("Object::Create")
        self.assertEqual(error_handler.reclassified_event, {"event-name": "Object::Create"})


if __name__ == "__main__":
    unittest.main()
