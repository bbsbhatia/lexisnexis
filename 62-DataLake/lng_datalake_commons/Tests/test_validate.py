import unittest

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_commons.validate import is_valid_input_json, is_valid_event_message, is_valid_event_version, \
    is_valid_event_stage

io_util = IOUtils(__file__, 'Validate')


class TestPass(unittest.TestCase):

    # +is_valid_input_json - can validate json to a schema
    def test_is_valid_input_json_pass(self):
        input_json = io_util.load_data_json("valid_command_request.json")
        input_schema = io_util.load_data_json("valid_command_request_schema.json")
        self.assertTrue(is_valid_input_json(input_json, input_schema))

    # -is_valid_input_json - returns false
    def test_is_valid_input_json_fail(self):
        input_json = io_util.load_data_json("invalid_command_request.json")
        input_schema = io_util.load_data_json("valid_command_request_schema.json")
        self.assertFalse(is_valid_input_json(input_json, input_schema))

    # +is_valid_event_message - can validate json to a schema
    def test_is_valid_event_message_pass(self):
        self.assertTrue(is_valid_event_message({'event-name': 'mock-test'}, 'mock-test'))

    # +is_valid_multi - the event name is one of the values supplied
    def test_is_valid_multi_event_pass(self):
        self.assertTrue(is_valid_event_message({'event-name': 'mock-test'}, 'mock-test', 'mock-test-2'))

    # -is_valid_event_message - empty message
    def test_is_valid_event_message_fail(self):
        self.assertFalse(is_valid_event_message({}, 'test'))

    # -is_valid_event_message - invalid event
    def test_is_valid_event_message_fail_1(self):
        self.assertFalse(is_valid_event_message({'event-name': 'mock-test'}, 'test'))

    # -is_valid_multi - the event name is not in the values supplied
    def test_is_valid_multi_event_fail(self):
        self.assertFalse(is_valid_event_message({'event-name': 'mock-test'}, 'mock-test-1', 'mock-test-2'))

    # +is valid event_version - all values supplied
    def test_is_valid_event_version_pass(self):
        self.assertTrue(is_valid_event_version({'event-version': 1}, 1))

    # -is valid event_version - event_version mis-match
    def test_is_valid_event_version_fail(self):
        self.assertFalse(is_valid_event_version({'event-version': 2}, 1))

    # -is valid event_version - no event_version supplied
    def test_is_valid_event_version_fail_1(self):
        self.assertFalse(is_valid_event_version({'event-version': {}}, 1))

    # -is_valid_event_version - empty message
    def test_is_valid_event_version_fail_2(self):
        self.assertFalse(is_valid_event_version({}, 1))

    # +is_valid_event_stage - all values supplied
    def test_is_valid_event_stage_pass(self):
        self.assertTrue(is_valid_event_stage({'stage': 'LATEST'},
                                             'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'))

    # -is_valid_event_stage - lambda arn does not contain stage
    def test_is_valid_event_version_fail_1(self):
        self.assertFalse(is_valid_event_stage({'stage': 'LATEST'},
                                              'arn:aws:lambda:us-east-1:123456789012:function:TestLambda'))

    # -is_valid_event_stage - empty message
    def test_is_valid_event_stage_fail_2(self):
        self.assertFalse(is_valid_event_stage({}, 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'))


if __name__ == '__main__':
    unittest.main()
