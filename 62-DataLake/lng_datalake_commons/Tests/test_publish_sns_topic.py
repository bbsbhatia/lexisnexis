import os
import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError, ConnectionClosedError
from lng_datalake_testhelper.io_utils import IOUtils

import lng_datalake_commons.publish_sns_topic as publish_sns_topic_module
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.publish_sns_topic import publish_to_topic, message_to_publishable_json, \
    get_catalog_ids_by_collection_id, load_json_schema, generate_schema_validator, validate_message_against_schema, \
    build_filter

io_util = IOUtils(__file__, 'PublishSnsTopic')

__author__ = "John Morelock, Jose Molinet"
__credits__ = "Jonathan A Mitchall"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestPass(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        publish_sns_topic_module.WORKING_DIRECTORY = os.path.join(os.path.dirname(__file__), "Data", "PublishSnsTopic")
        cls.valid_message = io_util.load_data_json("valid_message_v1.json")
        cls.valid_changeset_message = io_util.load_data_json('valid_changeset_message_v1.json')

    # + publish_sns_topic.message_to_publishable_json - returns publishable message
    @patch('lng_aws_clients.sns.is_publishable_sns_message')
    def test_message_to_publishable_json(self, mock_is_publishable):
        mock_is_publishable.return_value = True
        valid_message = io_util.load_data_json("valid_sns_msg_deserialized.json")
        expected_response = io_util.load_txt("valid_sns_publishable_msg_v1.txt")
        actual_response = message_to_publishable_json(valid_message)
        self.assertEqual(expected_response, actual_response)

    # - publish_sns_topic.message_to_publishable_json - json dump Exception -> TerminalErrorException
    @patch('orjson.dumps')
    def test_message_to_publishable_json_fail(self, mock_json_dumps):
        mock_json_dumps.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Failed to json dump message: .*? due to .*"):
            message_to_publishable_json({'Foo': 'Bar'})

    # - publish_sns_topic.message_to_publishable_json - sns.is_publishable returns false -> TerminalErrorException
    @patch('lng_aws_clients.sns.is_publishable_sns_message')
    def test_message_to_publishable_json_fail_1(self, mock_is_publishable):
        mock_is_publishable.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "Can not publish using message: .*"):
            message_to_publishable_json({'Foo': 'Bar'})

    # + publish_to_topic - schema-version: 'v0'
    @patch("lng_aws_clients.sns.get_client")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_success_v0(self, mocked_is_valid_topic_arn, mocked_get_client):
        mocked_is_valid_topic_arn.return_value = True
        mocked_get_client.return_value.publish.return_value = None
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        valid_message_v0 = io_util.load_data_json("valid_message_v0.json")
        self.assertIsNone(publish_to_topic(valid_message_v0, message_attributes, target_arn))

        publishable_msg = io_util.load_txt('valid_sns_publishable_msg_v0.txt')
        attributes = {'notification-type': {'DataType': 'String', 'StringValue': 'event'},
                      "schema-version": {"DataType": "String", "StringValue": valid_message_v0['schema-version']},
                      'event-name': {'DataType': 'String', 'StringValue': 'Object::Remove'},
                      'collection-id': {'DataType': 'String.Array', 'StringValue': '["collection-test"]'},
                      'catalog-id': {'DataType': 'String.Array',
                                     'StringValue': '["catalog-01","catalog-02"]'}}
        mocked_get_client.return_value.publish.assert_called_with(Message=publishable_msg, MessageStructure='json',
                                                                  MessageAttributes=attributes, TargetArn=target_arn)

    # + publish_to_topic - schema-version >= 'v1'
    @patch("lng_aws_clients.sns.get_client")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_success(self, mocked_is_valid_topic_arn, mocked_get_client):
        mocked_is_valid_topic_arn.return_value = True
        mocked_get_client.return_value.publish.return_value = None
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        self.assertIsNone(publish_to_topic(self.valid_message, message_attributes, target_arn))

        publishable_msg = io_util.load_txt('valid_sns_publishable_msg_v1.txt')
        attributes = {'notification-type': {'DataType': 'String', 'StringValue': 'event'},
                      "schema-version": {"DataType": "String", "StringValue": self.valid_message['schema-version']},
                      'event-name': {'DataType': 'String', 'StringValue': 'Object::Remove'},
                      'collection-id': {'DataType': 'String.Array', 'StringValue': '["collection-test"]'},
                      'catalog-id': {'DataType': 'String.Array',
                                     'StringValue': '["catalog-01","catalog-02"]'}}
        mocked_get_client.return_value.publish.assert_called_with(Message=publishable_msg, MessageStructure='json',
                                                                  MessageAttributes=attributes, TargetArn=target_arn)

    # + publish_to_topic - only changeset-id
    @patch("lng_aws_clients.sns.get_client")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_success_changeset_id_only(self, mocked_is_valid_topic_arn, mocked_get_client):
        mocked_is_valid_topic_arn.return_value = True
        mocked_get_client.return_value.publish.return_value = None
        message_attributes = {
            "event-name": "Changeset::Open",
            "changeset-id": "test-changeset"
        }
        target_arn = "random.arn.topic"
        self.assertIsNone(publish_to_topic(self.valid_changeset_message, message_attributes, target_arn))

        publishable_msg = io_util.load_txt('valid_sns_changeset_publishable_msg_v1.txt')
        attributes = {'notification-type': {'DataType': 'String', 'StringValue': 'event'},
                      "schema-version": {"DataType": "String", "StringValue": self.valid_message['schema-version']},
                      'event-name': {'DataType': 'String', 'StringValue': 'Changeset::Open'},
                      "changeset-id": {'DataType': 'String', 'StringValue': 'test-changeset'}
                      }
        mocked_get_client.return_value.publish.assert_called_with(Message=publishable_msg, MessageStructure='json',
                                                                  MessageAttributes=attributes, TargetArn=target_arn)

    # + publish_to_topic - publish a notification with two concatenated messages, corresponding to two schemas ('v0v1')
    @patch('lng_datalake_commons.publish_sns_topic.schema_validators', {})
    @patch("lng_aws_clients.sns.get_client")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_two_schemas_message(self, mocked_is_valid_topic_arn, mocked_get_client):
        mocked_is_valid_topic_arn.return_value = True
        mocked_get_client.return_value.publish.return_value = None
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        message = io_util.load_data_json("valid_message_v0v1.json")
        self.assertIsNone(publish_to_topic(message, message_attributes, target_arn))

        publishable_msg = io_util.load_txt('valid_sns_publishable_msg_v0v1.txt')
        attributes = {'notification-type': {'DataType': 'String', 'StringValue': 'event'},
                      "schema-version": {"DataType": "String", "StringValue": message['schema-version']},
                      'event-name': {'DataType': 'String', 'StringValue': 'Object::Remove'},
                      'collection-id': {'DataType': 'String.Array', 'StringValue': '["collection-test"]'},
                      'catalog-id': {'DataType': 'String.Array',
                                     'StringValue': '["catalog-01","catalog-02"]'}}
        mocked_get_client.return_value.publish.assert_called_with(Message=publishable_msg, MessageStructure='json',
                                                                  MessageAttributes=attributes, TargetArn=target_arn)

    # - publish_to_topic - invalid arn -> TerminalErrorException
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_invalid_arn(self, mocked_is_valid_topic_arn):
        mocked_is_valid_topic_arn.return_value = False
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        with self.assertRaisesRegex(TerminalErrorException, r"Can not publish message: .* to invalid Topic Arn"):
            publish_to_topic(self.valid_message, message_attributes, target_arn)

    # - publish_to_topic - sns.get_client Exception -> TerminalErrorException
    @patch("lng_datalake_commons.publish_sns_topic.message_to_publishable_json")
    @patch("lng_aws_clients.sns.get_client")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_general_exception(self, mocked_is_valid_topic_arn, mocked_get_client,
                                                mock_publishable_json):
        mocked_is_valid_topic_arn.return_value = True
        mocked_get_client.return_value.publish.side_effect = Exception
        mock_publishable_json.return_value = None
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        with self.assertRaisesRegex(TerminalErrorException, r"Failed to publish message: .*? to subscription topic: .*?"
                                                            r" due to unknown reasons. .*?"):
            publish_to_topic(self.valid_message, message_attributes, target_arn)

    # - publish_to_topic - sns.get_client EndpointConnectionError
    @patch("lng_datalake_commons.publish_sns_topic.message_to_publishable_json")
    @patch("lng_aws_clients.sns.get_client")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_endpoint_connection_error_exception(self, mocked_is_valid_topic_arn, mocked_get_client,
                                                                  mock_publishable_json):
        mocked_is_valid_topic_arn.return_value = True
        mocked_get_client.return_value.publish.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        mock_publishable_json.return_value = None
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        with self.assertRaises(EndpointConnectionError):
            publish_to_topic(self.valid_message, message_attributes, target_arn)

    # - publish_to_topic - sns.get_client ConnectionClosedError
    @patch("lng_datalake_commons.publish_sns_topic.message_to_publishable_json")
    @patch("lng_aws_clients.sns.get_client")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_endpoint_connection_closed_exception(self, mocked_is_valid_topic_arn, mocked_get_client,
                                                                   mock_publishable_json):
        mocked_is_valid_topic_arn.return_value = True
        mocked_get_client.return_value.publish.side_effect = ConnectionClosedError(
            endpoint_url='https://fake.content.aws.lexis.com')
        mock_publishable_json.return_value = None
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        with self.assertRaises(ConnectionClosedError):
            publish_to_topic(self.valid_message, message_attributes, target_arn)

    # - publish_to_topic - sns.get_client ClientError
    @patch("lng_datalake_commons.publish_sns_topic.message_to_publishable_json")
    @patch("lng_aws_clients.sns.get_client")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_client_error(self, mocked_is_valid_topic_arn, mocked_get_client, mock_publishable_json):
        mocked_is_valid_topic_arn.return_value = True
        mocked_get_client.return_value.publish.side_effect = ClientError(
            {"ResponseMetadata": {}, "Error": {"Code": "NotFound", "Message": "message"}}, "mocked-error")
        mock_publishable_json.return_value = None
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        with self.assertRaises(ClientError):
            publish_to_topic(self.valid_message, message_attributes, target_arn)

    # - publish_to_topic - Failed to json dump message --> TerminalErrorException
    @patch("lng_datalake_commons.publish_sns_topic.message_to_publishable_json")
    @patch("lng_aws_clients.sns.is_valid_topic_arn")
    def test_publish_to_topic_failed_json_dump_message(self, mocked_is_valid_topic_arn, mocked_publishable_json):
        mocked_is_valid_topic_arn.return_value = True
        mocked_publishable_json.side_effect = TerminalErrorException("Failed to json dump message")
        message_attributes = {
            "event-name": "Object::Remove",
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        target_arn = "random.arn.topic"
        with self.assertRaises(TerminalErrorException):
            publish_to_topic(self.valid_message, message_attributes, target_arn)

    # + get_catalogs_for_collection returns two items
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    def test_get_catalogs_for_collection(self, mock_mapping_table):
        input_data = io_util.load_data_json('dynamo.catalog_data_valid.json')
        mock_mapping_table.return_value = input_data['Items']
        expected_response = ["catalog-a", "catalog-b"]
        self.assertEqual(get_catalog_ids_by_collection_id("collection-01"), expected_response)

    # + get_catalogs_for_collection no catalogs returned
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    def test_get_catalogs_for_collection_empty(self, mock_mapping_table):
        mock_mapping_table.return_value = []
        self.assertFalse(get_catalog_ids_by_collection_id("-1"))

    # - get_catalogs_for_collection invalid
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    def test_invalid_catalog_id_fail(self, mock_mapping_table):
        mock_mapping_table.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {
                                                          'Code': 'Unit Test',
                                                          'Message': 'This is a mock'}},
                                                     "FAKE")
        self.assertRaises(ClientError, get_catalog_ids_by_collection_id, "-1")
        mock_mapping_table.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, get_catalog_ids_by_collection_id, "-1")

        mock_mapping_table.side_effect = Exception("FAKE")
        self.assertRaises(TerminalErrorException, get_catalog_ids_by_collection_id, "-1")

    # + load_json_schema - success
    def test_load_json_schema_success(self):
        json_schema = load_json_schema('Schemas/remove-object_NotificationSchema_v1.0.json')
        expected_required_props = ["SchemaVersion", "Schema", "NotificationType", "object"]
        self.assertEqual(expected_required_props, json_schema['required'])

    # - load_json_schema - JSONDecodeError -> TerminalErrorException
    def test_load_json_schema_terminal_exception(self):
        schema_file = 'Schemas/invalid_json_schema.json'
        with self.assertRaisesRegex(TerminalErrorException, "Error loading JSON schema {}".format(schema_file)):
            load_json_schema(schema_file)

    # - load_json_schema - schema file doesn't exist
    def test_load_json_schema_file_missing(self):
        schema_file = 'Schemas/this_file_does_not_exist.json'
        with self.assertRaisesRegex(TerminalErrorException, r"Error opening schema file .* No such file or directory"):
            load_json_schema(schema_file)

    # - generate_schema_validator - raises TerminalErrorException from schema definition error
    def test_generate_schema_validator_terminal_error(self):
        schema = 'Schemas/invalid_schema.json'
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to compile the schema {}. Error: enum must be an array".format(schema)):
            generate_schema_validator(schema)

    # + validate_message_against_schema - success
    def test_validate_message_against_schema_success(self):
        message_json = io_util.load_data_json("valid_message_v1.json")['message']
        self.assertIsNone(
            validate_message_against_schema(message_json, 'Schemas/remove-object_NotificationSchema_v1.0.json'))

    # - validate_message_against_schema - JsonSchemaException --> TerminalErrorException
    def test_validate_message_against_schema_terminal_error(self):
        message_json = io_util.load_data_json("invalid_message.json")
        with self.assertRaisesRegex(TerminalErrorException, r"Notification did not match JSON schema data must "
                                                            r"contain \['SchemaVersion', 'Schema', 'NotificationType', "
                                                            r"'object'\] properties"):
            validate_message_against_schema(message_json, 'Schemas/remove-object_NotificationSchema_v1.0.json')

    # - build_filter - message_attributes missing required attribute
    def test_build_filter_missing_att(self):
        invalid_message_attributes = {
            "collection-id": ["collection-test"],
            "catalog-id": ['catalog-01', 'catalog-02']
        }
        schema_version = 'v1'
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to build notification filters. Error: 'event-name'"):
            build_filter(invalid_message_attributes, schema_version)

    # - build_filter - message_attributes missing all of the one of optional attribute
    def test_build_filter_missing_optional(self):
        invalid_message_attributes = {
            "event-name": "random-event"
        }
        schema_version = 'v1'
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Filter attributes require at least one of the following"):
            build_filter(invalid_message_attributes, schema_version)


if __name__ == '__main__':
    unittest.main()
