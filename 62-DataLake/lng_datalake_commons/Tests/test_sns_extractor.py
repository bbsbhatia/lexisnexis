import unittest

from lng_datalake_commons import sns_extractor
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_testhelper.io_utils import IOUtils

__author__ = "John Morelock"
__credits__ = "Jonathan A Mitchall, Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'SnsExtractor')

class TestPass(unittest.TestCase):


    # +sns_extractor.extract_sns_message - returns message with no filtering
    def test_validate_1(self):
        input_json = io_util.load_data_json("valid_sns_event_msg.json")
        sns_out = sns_extractor.extract_sns_message(input_json)
        expected = io_util.load_data_json("valid_sns_msg_extracted.json")
        self.assertDictEqual(sns_out, expected)

    # +sns_extractor.extract_sns_message - returns message with validation
    def test_validate_2(self):
        input_json = io_util.load_data_json("valid_sns_event_msg.json")
        input_schema = io_util.load_data_json("valid_sns_event_msg_schema.json")
        sns_out = sns_extractor.extract_sns_message(input_json, valid_schema=input_schema)
        expected = io_util.load_data_json("valid_sns_msg_extracted.json")
        self.assertDictEqual(sns_out, expected)

    # -sns_extractor.extract_sns_message - validation error -> TerminalErrorException
    def test_validate_fail_0(self):
        input_json = io_util.load_data_json("invalid_sns_event_msg_decode_error.json")
        input_schema = io_util.load_data_json("valid_sns_event_msg_schema.json")
        with self.assertRaisesRegex(TerminalErrorException, ".*Could not validate data .*? with schema .*"):
            sns_extractor.extract_sns_message(input_json, valid_schema=input_schema)


    # -sns_extractor.extract_sns_message - more than 1 record
    def test_validate_fail_0_1(self):
        input_json = io_util.load_data_json("invalid_sns_event_multiple_records.json")
        with self.assertRaisesRegex(TerminalErrorException, "More than 1 records discovered. SNS records should only ever be sent with 1"):
            sns_extractor.extract_sns_message(input_json)



    # -sns_extractor.extract_sns_message - No SNS record -> TerminalErrorException
    def test_validate_fail_1(self):
        with self.assertRaisesRegex(TerminalErrorException, "Empty SNS record supplied for message extraction"):
            sns_extractor.extract_sns_message({})

    # -sns_extractor.extract_sns_message - No Records
    def test_validate_fail_2(self):
        input_json = io_util.load_data_json("invalid_sns_event_msg_record_missing.json")
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Extraction of SNS message failed. Key = .*? does not exist."):
            sns_extractor.extract_sns_message(input_json)

    # -sns_extractor.extract_sns_message - message empty -> TerminalErrorException
    def test_validate_fail_3(self):
        input_json = io_util.load_data_json("invalid_sns_event_msg_message_empty.json")
        with self.assertRaisesRegex(TerminalErrorException, "No message exists in SNS Notification"):
            sns_extractor.extract_sns_message(input_json)

    # -sns_extractor.extract_sns_message - JSONDecodeError -> TerminalErrorException
    def test_validate_fail_4(self):
        input_json = io_util.load_data_json("invalid_sns_event_msg_deserialize_error.json")
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Cannot load message: .*? due to .*? "):
            sns_extractor.extract_sns_message(input_json)


if __name__ == '__main__':
    unittest.main()
