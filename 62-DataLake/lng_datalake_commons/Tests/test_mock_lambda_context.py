import unittest

from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

io_util = IOUtils(__file__)

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestPass(unittest.TestCase):

    # + MockLambdaContext.get_remaining_time_in_millis - can create class and call method successfully
    def test_mock_lambda_context(self):
        mlc = MockLambdaContext()
        self.assertTrue(mlc.get_remaining_time_in_millis())


if __name__ == '__main__':
    unittest.main()
