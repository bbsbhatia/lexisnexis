import unittest
from datetime import datetime
from unittest.mock import patch

from lng_datalake_commons.time_helper import get_current_timestamp, format_time

__author__ = "Mark Seitter, Shekhar Ralhan"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestTimeHelper(unittest.TestCase):

    # +format_time - Valid time formatting
    def test_format_time_success(self):
        self.assertEqual(format_time(1519166937665), "2018-02-20T22:48:57.665Z")
        self.assertEqual(format_time(15191669376), "2018-02-20T22:48:57.600Z")
        self.assertEqual(format_time("1519166937665"), "2018-02-20T22:48:57.665Z")
        self.assertEqual(format_time("15191669376"), "2018-02-20T22:48:57.600Z")
        self.assertEqual(format_time(datetime(2012, 12, 12, 14, 00, 00, 10)), "2012-12-12T14:00:00.000Z")
        self.assertEqual(format_time("1552492894000"), "2019-03-13T16:01:34.000Z")

    # -format_time - Invalid time formatting
    def test_format_time_fail(self):
        self.assertRaises(ValueError, format_time, "ab")
        self.assertRaises(ValueError, format_time, "12.23")
        self.assertRaises(ValueError, format_time, "151916693766512365")

    def test_get_current_timestamp_success(self):
        with patch('lng_datalake_commons.time_helper.datetime') as mock_date:
            mock_date.utcnow.return_value = datetime(2012, 12, 12, 14, 00, 00, 10)
            self.assertEqual(get_current_timestamp(), "2012-12-12T14:00:00.000Z")


if __name__ == '__main__':
    unittest.main()
