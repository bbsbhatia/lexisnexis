import unittest
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from lng_datalake_commons import session_decorator

__author__ = "John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'SessionDecorator')


class SessionDecoratorTest(TestCase):

    # +test_session_decorator
    @patch('lng_aws_clients.session.set_session')
    def test_session_decorator(self, session_mock):
        payload = io_utils.load_data_json("valid_event.json")
        success = 'Complete'
        default_ttl = session_decorator._session_ttl

        @session_decorator.lng_aws_session()
        def test_session(local_payload, local_context):
            # Assert wrapper did not change parameters
            self.assertDictEqual(local_payload, payload)
            self.assertEqual(local_context, MockLambdaContext)
            return success

        self.assertEqual(test_session(payload, MockLambdaContext), success)
        session_mock.assert_called_once()
        # Verifies TTL was NOT changed
        assert session_decorator._session_ttl == default_ttl
        # Reset for remaining tests that can run in any order!
        session_decorator._session_start_epoch = 0

    # +test_session_decorator_parameter_ttl - TTL parameter sets _session_ttl
    @patch('lng_aws_clients.session.set_session')
    def test_session_decorator_parameter_ttl(self, session_mock):
        payload = io_utils.load_data_json("valid_event.json")
        success = 'Complete'
        ttl_param = 1234

        @session_decorator.lng_aws_session(session_ttl=ttl_param)
        def test_session(local_payload, local_context):
            self.assertDictEqual(local_payload, payload)
            self.assertEqual(local_context, MockLambdaContext)
            return success

        self.assertEqual(test_session(payload, MockLambdaContext), success)
        session_mock.assert_called_once()
        # Verifies TTL was changed correctly
        assert session_decorator._session_ttl == ttl_param
        # Reset for remaining tests that can run in any order!
        session_decorator._session_start_epoch = 0

    # +test_session_decorator_with_params - params are passed thru to session.set_session
    @patch('lng_aws_clients.session.set_session')
    def test_session_decorator_with_params(self, session_mock):
        payload = io_utils.load_data_json("valid_event.json")
        success = 'Complete'
        default_ttl = session_decorator._session_ttl

        @session_decorator.lng_aws_session(param1='us-day-1', param2='c-sand')
        def test_session(local_payload, local_context):
            self.assertDictEqual(local_payload, payload)
            self.assertEqual(local_context, MockLambdaContext)
            return success

        self.assertEqual(test_session(payload, MockLambdaContext), success)
        # Verify parameters were passed thru to session.set_session
        session_mock.assert_called_once_with(param1='us-day-1', param2='c-sand')
        # Verifies TTL was NOT changed
        assert session_decorator._session_ttl == default_ttl
        # Reset for remaining tests that can run in any order!
        session_decorator._session_start_epoch = 0

    # +test_session_decorator_session_persistence
    @patch('lng_aws_clients.session.set_session')
    def test_session_decorator_session_persistence(self, session_mock):
        payload = io_utils.load_data_json("valid_event.json")
        success = 'Complete'
        default_ttl = session_decorator._session_ttl

        @session_decorator.lng_aws_session()
        def test_session(local_payload, local_context):
            self.assertDictEqual(local_payload, payload)
            self.assertEqual(local_context, MockLambdaContext)
            return success

        # First call initializes the session
        self.assertEqual(test_session(payload, MockLambdaContext), success)
        session_mock.assert_called_once()

        # Second call in less than 44 minutes should not initialize the session
        self.assertEqual(test_session(payload, MockLambdaContext), success)
        session_mock.assert_called_once()
        # Verifies TTL was NOT changed
        assert session_decorator._session_ttl == default_ttl
        # Reset for remaining tests that can run in any order!
        session_decorator._session_start_epoch = 0


if __name__ == "__main__":
    unittest.main()
