
# Add TTL to Object Uploads in Initial Tracking Write


Date: 2019-08-23

## Status: accepted

* Deciders: Wormhole Team

## Context
Technical Story: [S-76585](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:848315) <!-- optional -->

Currently we update the existing tracking record for a create/update multipart/large object in the Tracking module after the command lambda has run. This happens outside the Command Wrapper and therefore does not handle throttle errors correctly and requires 2 writes to the tracking table.

This should happen on the initial write of the tracking record. We can see the appropriate paths to write the ttl for in the event.context.resource-path attribute.

### Driver
* If a client calls into the DataLake for multipart, large, or folder objects we write an initial tracking record 
that is then updated with a pending-expiration-epoch after the completion of the command logic.
Currently if this second tracking write is throttled we fail without removing the tracking record or writing to the
retry queue for the tracking delete lambda. 

### Options
* [option 1]: Move tracking update into the command logic to handle errors better, still requiring 2 tracking table writes
* [option 2]: Add TTL logic in the initial write so that pending-expiration-epoch is always present for upload objects 
(preferred by Mark Seitter and John Konderla)


## Decision


Chosen option: option 2, because limiting the number of writes is always desirable when possible.


## Consequences

* Good, because it will limit the number of writes to the tracking table
* Good, because it simplifies and centralizes the logic for large, multipart, and folder objects' tracking
* Bad, because more object specific logic will be included in the tracking module


## Links

* [S-76585](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:848315)