
# Validate notification messages against schema


Date: 2019-08-05
<!-- [YYYY-MM-DD when the decision was last updated] optional -->

## Status:  Accepted


* Deciders:  Wormhole Team

## Context
Technical Story: [S-71080](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:780243)
* DataLake clients need to get consistent notification messages for the events they are subscribed. 
We can provide that consistency adding schemas for the published messages and keeping 
different schema versions when introducing changes.
* DataLake clients must be able to migrate to a new schema version (v0 -> v1) without losing notifications.

## Decision

* Create schemas for the notifications sent after each event (Object::Create, Collection::Update, etc.).
* Validate the messages against the schemas before publishing.
* When a new schema version is introduced, a message for each schema version (old and new) will be published 
as well as a concatenated message containing the properties from the two versions. Once all the clients migrate to 
the new schema version, only the messages with that version will be published.

## Consequences

* A 200% increase in the SNS cost, because two additional messages are published until all clients migrate to the 
new schema version. 
* For example: during the migration from v0 to v1, we keep publishing the current messages (v0), and additionally we
start publishing the notifications for schema v1 and the concatenated message (v0v1).

Example of message payload for schema version 'v0' (currently published message)
```json
{
     "object-id": "object-test",
     "collection-id": "collection-test",
     "object-state": "Removed",
     ...
}
```

Example of message payload for schema version 'v1'
```json
{
  "v1": {
        "SchemaVersion": "v1.0"
        "Schema": "https://datalake.content.aws.lexis.com/schemas/remove_object_schema_v1.0.json",
        "NotificationType": "Event",
        "object": {
             "object-id": "object-test",
             "collection-id": "collection-test",
             "object-state": "Removed",
             ...
  }
}
```

Example of message payload for the concatenated message 'v0v1'
```json
{
  "object-id": "object-test",
  "collection-id": "collection-test",
  "object-state": "Removed",
  ...
  "v1": {
        "SchemaVersion": "v1.0"
        "Schema": "https://datalake.content.aws.lexis.com/schemas/remove_object_schema_v1.0.json",
        "NotificationType": "Event",
        "object": {
             "object-id": "object-test",
             "collection-id": "collection-test",
             "object-state": "Removed",
             ...
  }
}
```

Example of concatenated message for future versions ('v1v2', 'v2v3', etc.)
```json
{
  "v1": {
        "SchemaVersion": "v1.0"
        "Schema": "https://datalake.content.aws.lexis.com/schemas/remove_object_schema_v1.0.json",
        "NotificationType": "Event",
        "object": {
             "object-id": "object-test",
             "collection-id": "collection-test",
             "object-state": "Removed",
             ...
  },
  "v2": {
        "SchemaVersion": "v2.0"
        "Schema": "https://datalake.content.aws.lexis.com/schemas/remove_object_schema_v2.0.json",
        "NotificationType": "Event",
        "object": {
             "object-id": "object-test",
             "collection-id": "collection-test",
             "object-state": "Removed",
             ...
    }
}
```

## Links
* [Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:780243)