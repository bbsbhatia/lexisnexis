import orjson
import logging
import os

import fastjsonschema

from lng_datalake_commons.error_handling.exceptions import TerminalErrorException

__author__ = "Jonathan A Mitchall, Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


def extract_sns_message(sns_record: dict, valid_schema=None) -> dict:
    """
    Extracts SNS message from event
    :param valid_schema:  Some extractions require Validation of Message
    :param sns_record: Lambda event that consists of an Sns record
    """
    if not sns_record:
        raise TerminalErrorException('Empty SNS record supplied for message extraction')

    if len(sns_record.get('Records', [])) > 1:
        raise TerminalErrorException("More than 1 records discovered. SNS records should only ever be sent with 1. "
                                     "Message={0}".format(sns_record))

    # extract Message portion of SNS
    try:
        sns_string_message = sns_record['Records'][0]['Sns']['Message']
    except KeyError as key_error:
        raise TerminalErrorException("Extraction of SNS message failed. Key = {} does not exist."
                                     .format(key_error))

    # Check For Empty Message
    if not sns_string_message:
        raise TerminalErrorException('No message exists in SNS Notification')

    # load Sns Json String to Dictionary
    try:
        loaded_message = orjson.loads(sns_string_message)
    except (ValueError, TypeError) as json_error:
        raise TerminalErrorException("Cannot load message: {} due to {} ".format(sns_string_message, json_error))

    # validate Message
    if valid_schema:
        try:  # Validate JSON to Schema
            fastjsonschema.validate(valid_schema, loaded_message)
        except Exception as ex:
            raise TerminalErrorException('{0} Could not validate data {1} with schema {2} '
                                         .format(ex, loaded_message, valid_schema))

    return loaded_message
