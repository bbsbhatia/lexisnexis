import logging
import os
from datetime import datetime
from functools import wraps
from hashlib import md5

import orjson
import time
from botocore.exceptions import ClientError, ReadTimeoutError, EndpointConnectionError, ConnectTimeoutError, \
    ConnectionClosedError, IncompleteReadError
from lng_aws_clients import s3, sqs
from lng_datalake_constants import ingestion_counter, event_names
from urllib3.exceptions import ProtocolError

from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException, \
    IgnoreEventException, NoChangeEventException
from lng_datalake_commons.time_helper import get_current_timestamp

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Set environment variables
retry_queue_url = os.getenv("RETRY_QUEUE_URL")
terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")
INGESTION_COUNTER_QUEUE_URL = os.getenv('INGESTION_COUNTER_QUEUE_URL')

# Set main event handler
lambda_name = None

# additional_attributes is used by other modules to specify extra information for the payload's message
additional_attributes = {}

# used to re-class an event from Update to Create on event handler side
reclassified_event = {}

# retryable exceptions
retryable_exceptions = (
    ClientError,
    ReadTimeoutError,
    EndpointConnectionError,
    ConnectionClosedError,
    ConnectTimeoutError,
    ProtocolError,
    IncompleteReadError
)


def handle(func):
    """
    A decorator that wraps the passed in lambda_handler function and
    takes action based on type of raised custom exceptions.

    :param func: lambda_handler function
    """

    @wraps(func)
    def func_wrapper(payload, context):
        global additional_attributes
        additional_attributes = {}
        global reclassified_event
        reclassified_event = {}
        try:
            return func(payload, context)
        except (IgnoreEventException, NoChangeEventException) as e:
            logger.debug('Exception requiring no intervention thrown: {0}'.format(e))
        except TerminalErrorException as e:
            logger.exception(e)
            terminal_error(payload, context, error_message=str(e))
        except RetryEventException as e:
            logger.exception(e)
            throttle_error(payload, 'Retry', context)
        except ClientError as e:
            logger.exception(e)
            handle_client_error(payload, context, e)
        except EndpointConnectionError as e:
            logger.exception(e)
            throttle_error(payload, 'EndpointConnectionError', context)
        except (ReadTimeoutError, ConnectionClosedError, ConnectTimeoutError, IncompleteReadError) as e:
            logger.exception(e)
            throttle_error(payload, 'ConnectionError', context)
        except Exception as e:
            logger.exception(e)
            terminal_error(payload, context, error_message=str(e))

    return func_wrapper


def get_event_id(payload: dict) -> str:
    """
    Extracts event ID from the incoming event dictionary.

    :param payload: Incoming payload (event from an event handler)
    :return: event-id
    """
    try:
        event_id = orjson.loads(payload["Records"][0]["Sns"]["Message"])["event-id"]
        return event_id
    except KeyError as e:
        logger.critical("Extraction of 'event-id' from event failed. Event: {0} | Exception = {1}".format(payload, e))
    except (ValueError, TypeError) as e:
        logger.critical("Extraction of 'event-id' from event failed. Event: {0} | Exception = {1}".format(payload, e))
    except Exception as e:
        logger.critical("Extraction of 'event-id' from event failed. Event: {0} | Exception = {1}".format(payload, e))
    return ""


def compute_uid_of_context(context) -> str:
    try:
        millis = str(round(time.time() * 1000))
        aws_request_id = context.aws_request_id
        return "_".join([millis, aws_request_id])
    except Exception as e:
        logger.critical("Cannot compute UID from context due to unknown reasons. Context = {0} | Exception: {1}"
                        .format(vars(context), e))
    return ""


def compute_uid_of_payload(payload: dict) -> str:
    try:
        millis = str(round(time.time() * 1000))
        md5_hash = md5(orjson.dumps(payload)).hexdigest()
        return "_".join([millis, md5_hash])
    except TypeError as e:
        logger.critical("Cannot compute UID from payload. Payload = {0} | Exception: {1}".format(payload, e))
    except Exception as e:
        logger.critical("Cannot compute UID from payload due to unknown reasons. Payload = {0} | Exception: {1}"
                        .format(payload, e))
    return ""


def terminal_error(payload: dict, context=None, is_event=True, uid=None, error_message=None):
    """
    This method pipes and persists the failed payload due to terminal errors to S3 Terminal Errors bucket.

    :param payload: Incoming payload (can be any message or an event)
    :param context: Lambda Context
    :param is_event: Flag to distinguish an event from any of the event handlers to other forms of payload
    :param uid: Unique ID used for naming the failed payload object to S3 terminal errors location
    :param error_message: Optional error message to add to the payload

    P.S.: UID and context are mutually exclusive. Don't set both context and uid together.
          If both set, uid is used and context is neglected.

    """
    logger.error("Terminal error occurred. Moving the failed payload to terminal errors bucket. "
                 "Bucket = {0} | Payload = {1}".format(terminal_errors_bucket, payload))

    # update the ingestion item before fail an object event
    if is_event:
        try:
            message = orjson.loads(payload["Records"][0]["Sns"]["Message"])
            # if an Object event raises a terminal error during ingestion, then update ObjectErrorCount
            # if there is an ingestion id and it's not an ingestion event
            if 'ingestion-id' in message and message['event-name'].lower().split("::")[0] == 'object' \
                    and message['event-name'] not in [event_names.OBJECT_CREATE_INGESTION,
                                                      event_names.OBJECT_CANCEL_INGESTION]:
                _update_ingestion_error_count(message['ingestion-id'])
        except Exception as e:
            logger.error("Extraction of event-name from event failed. Event: {0} | Exception = {1}".format(payload, e))

    try:
        _handle_terminal_error(context, is_event, payload, uid, error_message)
    except ClientError as e:
        logger.error("Terminal error handling failed. Payload Lost. Bucket = {0} | Payload = {1} | Message = {2} "
                     "| Exception = {3}".format(terminal_errors_bucket, payload, error_message, e))
    except Exception as e:
        logger.error("Terminal error handling failed due to known reasons. Payload Lost. Bucket = {0} | Payload = {1} "
                     "| Message = {2} | Exception = {3}".format(terminal_errors_bucket, payload, error_message, e))


def _update_ingestion_error_count(ingestion_id: str) -> None:
    """
    Send a message to INGESTION_COUNTER_QUEUE to update ObjectErrorCount
    :param ingestion_id: Ingestion ID
    """
    message = {ingestion_id: {ingestion_counter.ERROR: 1}}
    try:
        sqs.get_client().send_message(QueueUrl=INGESTION_COUNTER_QUEUE_URL, MessageBody=orjson.dumps(message).decode())
    except ClientError as e:
        logger.warning("Unable to update ObjectErrorCount in Ingestion Table||{}".format(e))
    except Exception as ex:
        logger.error("Failed to update ObjectErrorCount in Ingestion Table||{}".format(ex))


def _handle_terminal_error(context, is_event, payload, uid, error_message):
    if s3.is_valid_bucket_name(terminal_errors_bucket):
        if not uid:
            uid = _set_uid_for_event(context, is_event, payload, uid)
            uid = _set_uid_fail_retry_strategy(context, is_event, payload, uid)
            uid = _set_uid_for_non_event(context, is_event, payload, uid)
        if context:
            _persist_to_s3(payload, uid, error_message, context.log_group_name, context.log_stream_name,
                           context.aws_request_id)
        else:
            _persist_to_s3(payload, uid, error_message)
    else:
        logger.critical("Terminal error handling failed. Event Lost. Invalid bucket name. Bucket = {0} | "
                        "Event = {1}".format(terminal_errors_bucket, payload))


def _set_uid_for_event(context, is_event, payload, uid):
    if is_event and not context:
        uid = get_event_id(payload)
    return uid


def _set_uid_fail_retry_strategy(context, is_event, payload, uid):
    if is_event and context:
        uid = get_event_id(payload)
        if not uid:
            logger.error("Cannot extract event-id, hence computing unique ID of context request ID.")
            uid = compute_uid_of_context(context)
    return uid


def _set_uid_for_non_event(context, is_event, payload, uid):
    if not is_event and context:
        uid = compute_uid_of_context(context)
    if not is_event and not context:
        uid = compute_uid_of_payload(payload)
    return uid


def _persist_to_s3(payload, uid, error_message=None, log_group_name=None, log_stream_name=None, aws_request_id=None):
    # key format: <lambda_name>/YYYY/MM/DD/HH/<uid>
    dt = datetime.utcnow().timetuple()
    if not uid:
        if aws_request_id:
            uid = '{0}-{1}'.format(aws_request_id, get_current_timestamp())
        else:
            uid = get_current_timestamp()
    key = "{0}/{1}/{2:02d}/{3:02d}/{4:02d}/{5}".format(lambda_name, dt.tm_year, dt.tm_mon, dt.tm_mday, dt.tm_hour, uid)
    body = {
        "original-payload": payload
    }
    if error_message:
        body['error-message'] = error_message
    if additional_attributes:
        body['additional-attributes'] = additional_attributes
    if log_group_name:
        body['log-group-name'] = log_group_name
    if log_stream_name:
        body['log-stream-name'] = log_stream_name
    if aws_request_id:
        body['aws-request-id'] = aws_request_id

    s3.put_s3_object(orjson.dumps(body).decode(), terminal_errors_bucket, key, content_type="application/json")


def handle_client_error(payload, context, e):
    client_error_code = e.response["Error"]["Code"]
    if client_error_code in (
            "ProvisionedThroughputExceededException", "InternalServerError", "TransactionInProgressException") \
            or (client_error_code == "ValidationException"
                and 'Cannot read from backfilling global secondary index' in e.response['Error']['Message']) \
            or (client_error_code == "TransactionCanceledException"
                and "ConditionalCheckFailed" not in e.response['Error']['Message']):
        throttle_error(payload, "DynamoDB", context)
    elif client_error_code in ["ThrottlingException", "Throttling"]:
        throttle_error(payload, "Sns", context)
    elif client_error_code in ("SlowDown", "InternalError") or (
            client_error_code == "500" and "Internal Server Error" in e.response['Error']['Message']):
        throttle_error(payload, "S3", context)
    elif client_error_code in ["ConcurrentRunsExceededException",
                               "OperationTimeoutException",
                               "InternalServiceException",
                               "ResourceNumberLimitExceededException",
                               "AWSGlueException"]:
        throttle_error(payload, "Glue", context)
    elif client_error_code in ('ServiceUnavailable', '503'):
        throttle_error(payload, "ServiceUnavailable", context)
    elif client_error_code == 'InvalidClientTokenId':
        throttle_error(payload, "InvalidClientTokenId", context)
    else:
        logger.error(client_error_code)
        terminal_error(payload, context, error_message=str(e))


def throttle_error(payload: dict, service: str, is_event=True, context=None) -> None:
    """
    This method facilitates exponential backoff and moves payloads due to DynamoDB or Sns throttle errors
    to a retry queue.

    :param payload: payload: Incoming payload (can be any message or an event)
    :param service: AWS service (DynamoDB or Sns)
    :param is_event: Flag to distinguish an event from any of the event handlers to other forms of payload
    :param context: Lambda Context
    """
    try:
        is_valid_sqs_url = sqs.is_valid_queue_url(retry_queue_url)
        # add the additional attributes to the message in the payload
        if additional_attributes:
            payload = _add_attributes_to_payload(payload)
        # re-class event from Update to Create
        if reclassified_event:
            payload = _reclassify_payload_event_name(payload)
        # Used to handle when the original payload is not an SNS message like retry tracking delete
        # (event dispatcher expects these)
        if 'Records' not in payload:
            payload = {"Records": [{"Sns": {"Message": orjson.dumps(payload).decode()}}]}
        sqs_message = orjson.dumps(payload).decode()
        is_valid_message = sqs.is_valid_message(sqs_message)
        if is_valid_sqs_url and is_valid_message:
            _send_message(sqs_message, service)
        else:
            _validation_fail(payload, context, is_valid_message, is_valid_sqs_url, is_event)
    except ClientError as e:
        error_message = "Failed to write the item to the retry queue||{}".format(e)
        logger.error("{0}||Moving the payload to terminal error S3 bucket||"
                     "Payload: {1}".format(error_message, payload, e))
        terminal_error(payload, context, is_event, error_message=error_message)
    except Exception as e:
        error_message = "Failed to write the item to the retry queue due to unknown reasons||{}".format(e)
        logger.error("{0}||Moving the payload to terminal error S3 bucket||"
                     "Payload: {1}".format(error_message, payload, e))
        terminal_error(payload, context, is_event, error_message=error_message)


def _add_attributes_to_payload(payload: dict) -> dict:
    if 'Records' not in payload:
        payload["additional-attributes"] = additional_attributes
    else:
        message = orjson.loads(payload["Records"][0]["Sns"]["Message"])
        message["additional-attributes"] = additional_attributes
        payload["Records"][0]["Sns"]["Message"] = orjson.dumps(message).decode()
    return payload


def set_message_additional_attribute(attribute_name: str, attribute_value) -> None:
    """
    Add or overwrite the attribute in the message additional attributes
    :param attribute_name: name of the attribute to set
    :param attribute_value: value to assign, could be a list, dictionary, etc.
    """
    global additional_attributes
    additional_attributes[attribute_name] = attribute_value


def _reclassify_payload_event_name(payload: dict) -> dict:
    if 'Records' not in payload:
        payload["event-name"] = reclassified_event["event-name"]
    else:
        message = orjson.loads(payload["Records"][0]["Sns"]["Message"])
        message["event-name"] = reclassified_event["event-name"]
        payload["Records"][0]["Sns"]["Message"] = orjson.dumps(message).decode()
    return payload


def reclassify_event_name(event_name: str) -> None:
    """
    reclassify  event name for upsert
    :param event_name: event name to be updated
    """
    global reclassified_event
    reclassified_event["event-name"] = event_name


def _validation_fail(payload, context, is_valid_message, is_valid_sqs_url, is_event):
    if not is_valid_sqs_url:
        error_message = "Failed to write the item to the retry queue||Invalid SQS Queue URL: {}".format(retry_queue_url)
        logger.error("{0}||Moving the payload to terminal error S3 bucket||"
                     "Payload: {1}".format(error_message, payload))
        terminal_error(payload, context, is_event, error_message=error_message)
    if not is_valid_message:
        error_message = "Failed to write the item to the retry queue. Invalid SQS message."
        logger.error("{0}||Moving the payload to terminal error S3 bucket||"
                     "Payload: {1}".format(error_message, payload))
        terminal_error(payload, context, is_event, error_message=error_message)


def _send_message(payload, service):
    # services are "DynamoDB", "Sns", "S3", "Glue", "Retry", 'Tracking' ,
    # 'ServiceUnavailable', 'EndpointConnectionError', 'ConnectionError'
    kwargs = {'QueueUrl': retry_queue_url, 'MessageBody': payload, 'MessageAttributes': {
        "ThrottleType": {
            "StringValue": service,
            "DataType": "String"
        }
    }}

    # Change default delay from CFT for these service types...
    if service == 'Tracking':
        kwargs['DelaySeconds'] = 300
    elif service in ('ServiceUnavailable', 'EndpointConnectionError'):
        kwargs['DelaySeconds'] = 900

    logger.warning("{} exception raised. Moving the failed payload to the retry queue.".format(service))
    sqs.get_client().send_message(**kwargs)


if __name__ == '__main__':
    from lng_aws_clients import session
    from lng_datalake_testhelper import mock_lambda_context

    os.environ['AWS_PROFILE'] = 'c-sand'
    terminal_errors_bucket = "feature-jek-datalake-lambda-terminal-errors-288044017584"
    lambda_name = "test_lambda.py"

    session.set_session()

    payload = {
        "Records": [
            {
                "EventSource": "aws:sns",
                "EventVersion": "1.0",
                "EventSubscriptionArn": "arn:aws:sns:us-east-1:195052678233:release-DataLake-EventDispatcher-EventDispatcherTopic-C974JPFGTBR6:369d8317-9a80-41fd-a7c2-e8e32128d06a",
                "Sns": {
                    "Type": "Notification",
                    "MessageId": "b6dc053a-b613-5b46-aa17-501bf8ac6727",
                    "TopicArn": "arn:aws:sns:us-east-1:195052678233:release-DataLake-EventDispatcher-EventDispatcherTopic-C974JPFGTBR6",
                }
            }
        ]
    }
    terminal_error(payload,
                   context=mock_lambda_context.MockLambdaContext(),
                   uid="abbcdefa-579f-11e9-9d9e-17b837787c63",
                   is_event=False,
                   error_message="Sample error message")
