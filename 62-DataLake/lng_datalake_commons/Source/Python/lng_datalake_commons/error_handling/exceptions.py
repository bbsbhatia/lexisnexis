class TerminalErrorException(Exception):
    def __init__(self, description):
        message = "TerminalErrorException||{}".format(description).replace('\n', '')
        super().__init__(message)


class RetryEventException(Exception):
    def __init__(self, description):
        message = "RetryEventException||{}".format(description).replace('\n', '')
        super().__init__(message)


class IgnoreEventException(Exception):
    def __init__(self, description):
        message = "IgnoreEventException||{}".format(description).replace('\n', '')
        super().__init__(message)


class NoChangeEventException(Exception):
    def __init__(self, description):
        message = "NoChangeEventException||{}".format(description).replace('\n', '')
        super().__init__(message)
