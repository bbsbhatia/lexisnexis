import logging
import os

import fastjsonschema
import orjson
from botocore.exceptions import ClientError, EndpointConnectionError
from lng_aws_clients import sns
from lng_datalake_dal.catalog_collection_mapping_table import CatalogCollectionMappingTable

from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException

__author__ = "Jonathan A. Mitchall, Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))
WORKING_DIRECTORY = os.getenv('LAMBDA_TASK_ROOT')

# compiled schema functions
schema_validators = {}


def publish_to_topic(message: dict, message_attributes: dict, target_arn: str) -> None:
    """
    Create filters, validate against the schema, transform to string and publish the message to the Subscription topic
    :param message: dict with the schema-rel-path, schema-version and the message
    :param message_attributes: attributes to generate the filter.
                               Required attributes: "event-name"
                               At least one of the optional attributes: "catalog-id", "collection-id", "changeset-id"
    :param target_arn: arn for target sns
    """
    if not sns.is_valid_topic_arn(target_arn):
        raise TerminalErrorException("Can not publish message: {} to invalid Topic Arn {}".format(message, target_arn))

    if 'schema-rel-path' in message and message['schema-rel-path'] not in schema_validators:
        schema_validators[message['schema-rel-path']] = generate_schema_validator(message['schema-rel-path'])

    filter_dict = build_filter(message_attributes, message['schema-version'])
    try:
        if 'schema-rel-path' in message:
            validate_message_against_schema(message['message'], message['schema-rel-path'])

        # publishable message must have the format: {<version_number>: <message>} except for 'v0' and concatenated msg
        if message["schema-version"] == 'v0' or message["schema-version"].count('v') == 2:
            publishable_message = message_to_publishable_json(message['message'])
        else:
            publishable_message = message_to_publishable_json({message["schema-version"]: message['message']})

        sns.get_client().publish(TargetArn=target_arn, Message=publishable_message, MessageStructure="json",
                                 MessageAttributes=filter_dict)
        logger.debug(
            "Message {0} with attributes {1} published to {2}".format(publishable_message, filter_dict, target_arn))
    except TerminalErrorException as e:
        raise e
    except error_handler.retryable_exceptions as e:
        logger.error(e)
        raise e
    except Exception as e:
        logger.error(e)
        raise TerminalErrorException("Failed to publish message: {} to subscription topic: {} due to "
                                     "unknown reasons. {}".format(message, target_arn, e))


def generate_schema_validator(schema_rel_path: str):
    schema_json = load_json_schema(schema_rel_path)
    try:
        return fastjsonschema.compile(schema_json)
    except fastjsonschema.JsonSchemaDefinitionException as e:
        logger.error("Failed validation due to invalid schema {}. Error: {}".format(schema_rel_path, e.message))
        raise TerminalErrorException("Failed to compile the schema {}. Error: {}".format(schema_rel_path, e.message))


def load_json_schema(rel_path: str) -> dict:
    try:
        norm_rel_path = os.path.normpath(rel_path)
        with open(os.path.join(WORKING_DIRECTORY, norm_rel_path)) as file:
            return orjson.loads(file.read())
    except orjson.JSONDecodeError as decode_error:
        logger.error("Error loading JSON schema {}::{}".format(rel_path, decode_error.msg))
        raise TerminalErrorException("Error loading JSON schema {}::{}".format(rel_path, decode_error.msg))
    except Exception as e:
        logger.error("Error opening schema file {} from {}. Error {}".format(rel_path, WORKING_DIRECTORY, e))
        raise TerminalErrorException(
            "Error opening schema file {} from {}. Error {}".format(rel_path, WORKING_DIRECTORY, e))


def build_filter(message_attributes: dict, schema_version: str) -> dict:
    optional_filter_attributes = ('changeset-id', 'collection-id', 'catalog-id')
    try:
        filter_dict = {
            "notification-type": {"DataType": "String", "StringValue": "event"},
            "schema-version": {"DataType": "String", "StringValue": schema_version},
            "event-name": {"DataType": "String", "StringValue": message_attributes['event-name']}
        }
        if 'catalog-id' in message_attributes:
            filter_dict['catalog-id'] = {"DataType": "String.Array",
                                         "StringValue": orjson.dumps(message_attributes['catalog-id']).decode()}
        if 'collection-id' in message_attributes:
            filter_dict["collection-id"] = {"DataType": "String.Array",
                                            "StringValue": orjson.dumps(message_attributes['collection-id']).decode()}
        if 'changeset-id' in message_attributes:
            filter_dict["changeset-id"] = {"DataType": "String",
                                           "StringValue": message_attributes['changeset-id']}
    except Exception as e:
        raise TerminalErrorException("Failed to build notification filters. Error: {}".format(e))

    if not any(key in filter_dict for key in optional_filter_attributes):
        raise TerminalErrorException(
            "Filter attributes require at least one of the following: {0}||Filter: {1}".format(
                optional_filter_attributes,
                filter_dict))

    return filter_dict


def validate_message_against_schema(message, schema):
    try:
        schema_validators[schema](message)
    except fastjsonschema.JsonSchemaException as validation_error:
        logger.error('Notification {} failed schema validation {}'.format(message, validation_error.message))
        raise TerminalErrorException("Notification did not match JSON schema {}".format(validation_error.message))


def message_to_publishable_json(message: dict) -> str:
    """
    Transform message to JSON formatted string
    :param message: notification message
    :return: Response String
    """
    try:
        json_message = orjson.dumps(message).decode()
    except Exception as e:
        logger.error(e)
        raise TerminalErrorException("Failed to json dump message: {} due to {}".format(message, e))

    publishable_message = orjson.dumps({"default": json_message}).decode()

    if not sns.is_publishable_sns_message(publishable_message):
        raise TerminalErrorException("Can not publish using message: {}".format(message))

    return publishable_message


def get_catalog_ids_by_collection_id(collection_id: str):
    """
    Gets all the catalogs to which the input Collection belongs to
    :param collection_id: Collection ID
    :return: list of catalog ids
    """
    catalogs = []
    try:
        response_items = CatalogCollectionMappingTable().query_items(
            KeyConditionExpression='CollectionID=:collection_id',
            ExpressionAttributeValues={":collection_id": {"S": collection_id}})
    except (ClientError, EndpointConnectionError) as e:
        logger.error(e)
        raise e
    except Exception as e:
        logger.error(e)
        raise TerminalErrorException("Failed to get catalogs for Collection ID: {} due to {}".format(collection_id, e))

    for item in response_items:
        catalogs.append(item["catalog-id"])
    return catalogs
