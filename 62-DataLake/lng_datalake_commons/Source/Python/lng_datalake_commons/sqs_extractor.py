import orjson
import logging
import os

from lng_datalake_commons.error_handling.exceptions import TerminalErrorException

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


def extract_sqs_message(sqs_record: dict) -> dict:
    """
    Extracts SQS message from event
    :param sqs_record: Lambda event that consists of an Sqs record
    """
    if not sqs_record:
        raise TerminalErrorException('Empty SQS record supplied for message extraction')

    # extract Message portion of SNS
    try:
        sqs_string_message = sqs_record['body']
    except KeyError as key_error:
        raise TerminalErrorException("Extraction of SQS message failed. Key = {} does not exist."
                                     .format(key_error))

    # Check For Empty Message
    if not sqs_string_message:
        raise TerminalErrorException('No message exists in SQS Notification')

    # load Sns Json String to Dictionary
    try:
        loaded_message = orjson.loads(sqs_string_message)
    except (ValueError, TypeError) as json_error:
        raise TerminalErrorException("Cannot load message: {} due to {} ".format(sqs_string_message, json_error))

    return loaded_message
