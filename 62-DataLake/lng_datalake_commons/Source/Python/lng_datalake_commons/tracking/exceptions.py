__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

message_pattern = "{0}||{1}||{2}"


class DataLakeException(Exception):
    def __init__(self, *args):
        # Fail safe to guarantee we always have 3 parts since the vm tempaltes expect this
        if len(args) != 3:
            message = message_pattern.format("500", "Args dont match: {0}".format(args),
                                             "Contact DL team with request-id")
        else:
            message = message_pattern.format(*args).replace('\n', '')
        super().__init__(message)


class InvalidRequestPropertyName(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("400", description, corrective_action)


class NotAllowedError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("405", description, corrective_action)


class InternalError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("500", description, corrective_action)


class UnhandledException(InternalError):
    def __init__(self, corrective_action: object = ""):
        super().__init__("Unhandled exception occurred", corrective_action)
