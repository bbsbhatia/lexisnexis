import logging
import os
from datetime import datetime, timedelta

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_constants import event_names
from lng_datalake_dal.collection_blocker_table import CollectionBlockerTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.tracking_table import TrackingTable

from lng_datalake_commons import sns_extractor
from lng_datalake_commons import time_helper
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, NoChangeEventException
from lng_datalake_commons.tracking.exceptions import InternalError, NotAllowedError, InvalidRequestPropertyName, \
    DataLakeException

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

RETRY_EXCEPTIONS = ["ProvisionedThroughputExceededException",
                    "ThrottlingException", "InternalServerError"]


def track_command(event_name: str):
    """
    Tracking decorator to command wrapper. Ensures the incoming event is put into the tracking table first.
    :param event_name: Event Name
    :return: Command Lambda client response
    """

    def wrapper(func):
        def func_wrapper(event, context):
            try:
                if event_name:
                    namespace, state = _extract_namespace_state_from_event(event_name)
                    if event_name in [event_names.OBJECT_CREATE_INGESTION, event_names.OBJECT_BATCH_REMOVE]:
                        _put_collection_blocker(event, event_name)
                    elif namespace == 'object':
                        _validate_collection(event)
                        keys_dict = _extract_keys(event, namespace)
                        keys_dict['event-name'] = event_name
                        _put_event_to_tracking_table(keys_dict)
                    elif event_name == event_names.COLLECTION_CREATE or state != "create":
                        keys_dict = _extract_keys(event, namespace)
                        keys_dict['event-name'] = event_name
                        _put_event_to_tracking_table(keys_dict)
            except DataLakeException as e:
                raise e
            except Exception as e:
                logger.error("Unhandled exception occurred in the track_command function. Event {0} | "
                             "EventName {1} | Error: {2}".format(event, event_name, e))
                raise InternalError("Unknown exception in track_command: {0}".format(e))

            return func(event, context)

        return func_wrapper

    return wrapper


def track_event_handler(event_name: str):
    """
    A decorator that wraps business logic of event handler lambda.
    Ensures the incoming event is the oldest in tracking table to processed.
    Moves it to retry queue if not for later processing.
    :param event_name: Event Name
    """

    def wrapper(func):
        def func_wrapper(event, context):
            namespace = _extract_namespace_state_from_event(event_name)[0]
            event_keys = _extract_keys_from_event(event, namespace)
            collection_id = event_keys['collection-id'] if namespace == 'object' else None
            tracking_id = generate_tracking_id(namespace, event_keys['namespace-id'], collection_id)
            oldest_tracking_item = _get_oldest_item_by_tracking_id(tracking_id)
            if not oldest_tracking_item:
                raise TerminalErrorException(
                    "Event: {0} ignored because no tracking record found".format(event))

            # validate that the event is the oldest in the tracking table
            if not _is_oldest_event(oldest_tracking_item.get('event-id'), event_keys['event-id']):
                logging.warning("Event not current in Tracking Table, putting onto retry queue for later. LatestEvent: "
                                "{0} MyEvent: {1}".format(oldest_tracking_item.get('event-id'), event_keys['event-id']))
                error_handler.throttle_error(event, service="Tracking")
                return

            try:
                resp = func(event, context)
            except NoChangeEventException as no_change_exception:
                logger.warning('No change event has been discovered.')
                _delete_tracking_item(oldest_tracking_item, event_keys['event-id'], event_keys['stage'])
                raise no_change_exception
            _delete_tracking_item(oldest_tracking_item, event_keys['event-id'], event_keys['stage'])
            return resp

        return func_wrapper

    return wrapper


def generate_tracking_id(namespace: str, item_id: object, collection_id: str = None) -> str:
    """
    Extracts and constructs keys needed for DynamoDB query actions.
    :param namespace: Namespace
    :param item_id: Item ID (int or str)
    :param collection_id: Collection ID (required for object namespace)
    :return: NewTracking|ID
    """
    if namespace == "object":
        # Append the collection-id on the end of the item-id to ensure uniqueness in the tracking table
        # for ids that require composite keys
        return "{0}|{1}|{2}".format(item_id, collection_id, namespace)
    else:
        return "{0}|{1}".format(item_id, namespace)


def _extract_namespace_state_from_event(event_name: str) -> tuple:
    """
    Extracts namespace and state from event name.

    :param event_name: Event Name
    :return: Extracted namespace and state
    """
    return tuple(event_name.lower().split("::"))


def _extract_keys(event: dict, namespace: str) -> dict:
    """
    Extracts and constructs keys needed for DynamoDB query actions.
    :param event: Incoming Event
    :param namespace: Namespace
    :return: Keys dict
    """
    if namespace == 'object':
        return _extract_object_keys_for_command(event)
    try:
        item_id = event["request"]["{0}-id".format(namespace)]
        keys_dict = \
            {
                "event-id": event["context"]["client-request-id"],
                "tracking-id": generate_tracking_id(namespace, item_id),
                "request-time": time_helper.format_time(event["context"]["client-request-time"])
            }
        # Api Key ID will will be written to the Tracking Table for Collection events
        if namespace == 'collection':
            keys_dict['api-key-id'] = event["context"]["api-key-id"]

        return keys_dict
    except KeyError as e:
        raise InternalError("Failed to extract keys from incoming event. Event = {0} | KeyError = {1}".format(event, e))


def _extract_object_keys_for_command(event: dict) -> dict:
    """
    Extracts and constructs keys needed for DynamoDB query actions.
    :param event: Incoming Event
    :return: Keys dict
    """
    namespace = 'object'
    try:
        request_id = event["context"]["client-request-id"]
        # in case of a create object event with no object-id specified, the request-id is used as the object-id
        if "object-id" not in event["request"]:
            item_id = request_id
        else:
            item_id = event["request"]["{0}-id".format(namespace)]
        try:
            collection_id = event['request']['collection-id']
        except KeyError as e:
            logger.error(
                "Failure to extract collection-id from request. Event = {0} | KeyError = {1}".format(event, e))
            raise InvalidRequestPropertyName("Request did not match JSON schema",
                                             "Request must include {}".format(e))
        tracking_id = generate_tracking_id(namespace, item_id, collection_id)
        request_time = time_helper.format_time(event["context"]["client-request-time"])
        tracking_record = {"event-id": request_id, "tracking-id": tracking_id, "request-time": request_time}
        pending_expiration_epoch = _get_expiration_epoch_for_upload_events(event['context']['resource-path'])
        if pending_expiration_epoch:
            logger.debug('Adding pending-expiration-epoch for upload object: {0}'
                         ' to record tracking-id: {1}, request-time: {2}'.format(pending_expiration_epoch,
                                                                                 tracking_record['tracking-id'],
                                                                                 tracking_record['request-time']))
            tracking_record['pending-expiration-epoch'] = pending_expiration_epoch
        changeset_id = _extract_changeset_id_if_exists(event['request'])
        if changeset_id:
            tracking_record['changeset-id'] = changeset_id
        return tracking_record
    except KeyError as e:
        raise InternalError("Failed to extract keys from incoming event. Event = {0} | KeyError = {1}".format(event, e))


def _extract_keys_for_collection_blocker(event: dict) -> dict:
    """
    Extracts and constructs keys needed for DynamoDB actions.
    :param event: Incoming Event
    :return: Keys dict
    """
    try:
        request_id = event["context"]["client-request-id"]
        collection_id = event["request"]["collection-id"]
        request_time = time_helper.format_time(event["context"]["client-request-time"])
        pending_expiration_epoch = get_collection_blocker_expiration(request_time)
        collection_blocker_item = {"request-id": request_id, "collection-id": collection_id,
                                   "request-time": request_time,
                                   'pending-expiration-epoch': pending_expiration_epoch}

        changeset_id = _extract_changeset_id_if_exists(event['request'])
        if changeset_id:
            collection_blocker_item['changeset-id'] = changeset_id
        return collection_blocker_item
    except KeyError as e:
        raise InternalError("Failed to extract keys from incoming event. Event = {0} | KeyError = {1}".format(event, e))


def _validate_collection(event: dict) -> None:
    """
    Validate no collection blocker exist for the collection id
    :param event: full event containing the collection id
    """
    try:
        collection_id = event['request']['collection-id']
    except KeyError as e:
        logger.error("Failure to extract collection-id from request. Event = {0} | KeyError = {1}".format(event, e))
        raise InvalidRequestPropertyName("Request did not match JSON schema",
                                         "Request must include {}".format(e))
    try:
        items = CollectionBlockerTable().query_items(max_items=1,
                                                     KeyConditionExpression="CollectionID = :collection_id",
                                                     ExpressionAttributeValues={":collection_id": {"S": collection_id}},
                                                     ConsistentRead=True,
                                                     ScanIndexForward=True)
    except Exception as e:
        raise InternalError(
            "Failed obtaining the collection blocker for collection-id: {} || Error = {}".format(collection_id, e))
    if items:
        raise NotAllowedError(
            "Collection ID {} temporarily locked by Ingestion ID {}".format(collection_id, items[0]['request-id']))


def get_collection_blocker_expiration(request_time: str, hour_offset: int = 168) -> int:
    """
    :param request_time: formatted date
    :param hour_offset: number of hours in the future to expire a row from dynamoDb
    :return: unix epoch
    """
    collection_blocker_expiration_epoch = int(
        (datetime.strptime(request_time + '+0000', "%Y-%m-%dT%H:%M:%S.%fZ%z")
         + timedelta(hours=hour_offset)).timestamp())
    return collection_blocker_expiration_epoch


def _put_event_to_tracking_table(primary_key: dict) -> None:
    """
    Puts incoming event to the command lambdas into tracking table.

    :param: primary_key: request_id, tracking_id, request_time
    :return None
    """
    try:
        TrackingTable().put_item(primary_key)
        logger.info("Successfully added event to the tracking table!")
    except Exception as e:
        raise InternalError("Failed to put event into tracking table. Primary Key = {0} | Exception = {1}"
                            .format(primary_key, e))


def _put_collection_blocker(event: dict, event_name: str) -> None:
    """
    Puts incoming event to the command lambdas into collection blocker table.

    :param event: Incoming Event
    :return None
    """
    collection_blocker_keys = _extract_keys_for_collection_blocker(event)
    collection_blocker_keys['event-name'] = event_name
    internal_error_message = "Failed to put event into collection blocker table. Primary Key = {0} | Exception = {1}"
    try:
        CollectionBlockerTable().put_item(collection_blocker_keys)
        logger.info("Successfully added event to the collection blocker table!")
    except ConditionError as ce:
        # Ignore ConditionError if RequestIDs are equal.
        request_id = _get_collection_blocker_request_id(collection_blocker_keys)
        if request_id == collection_blocker_keys['request-id']:
            logger.info("ConditionError ignored - Successfully added event to the collection blocker table!")
        else:
            raise InternalError(internal_error_message.format(collection_blocker_keys, ce))
    except Exception as e:
        raise InternalError(internal_error_message.format(collection_blocker_keys, e))


def _get_collection_blocker_request_id(collection_blocker_keys: dict) -> str:
    """
    Get the request id for the specified collection blocker from the CollectionBlockerTable.
    :param collection_blocker_keys: Collection blocker keys
    :return: Collection blocker request id if any, empty string if exception or item not found
    """
    request_id = ''
    try:
        item = CollectionBlockerTable().get_item(
            {'collection-id': collection_blocker_keys['collection-id'],
             'request-time': collection_blocker_keys['request-time']},
            ConsistentRead=True)
        if item:
            request_id = item['request-id']
    except Exception as e:
        logger.info("Failed to get collection blocker request-id. Primary Key = {0} | Exception = {1}"
                    .format(collection_blocker_keys, e))

    return request_id


def un_track_command_by_event(event: dict, event_name: str, stage: str) -> None:
    """
    Un tracks event of tracking or collection blocker table in case of exceptions inside command wrapper.
    :param event: Event ID
    :param event_name: Event Name
    :param stage: The stage that was invoked
    :return: None
    """
    if event_name in [event_names.OBJECT_CREATE_INGESTION, event_names.OBJECT_BATCH_REMOVE]:
        try:
            _delete_collection_blocker(event, stage)
        except TerminalErrorException as e:
            logger.critical(
                "Deleting from collection blocker table failed. Human Intervention is needed! | Event = {0} | "
                "Exception = {1}".format(event, e))
    else:
        namespace = _extract_namespace_state_from_event(event_name)[0]
        keys = _extract_keys(event, namespace)
        tracking_item_key = {'tracking-id': keys['tracking-id'], 'request-time': keys['request-time']}
        try:
            _delete_tracking_item(tracking_item=tracking_item_key,
                                  event_id=keys['event-id'], stage=stage)
            logger.info("Successfully removed tracking tracking item: {0}".format(tracking_item_key))
        except TerminalErrorException as e:
            logger.critical(
                "Deleting from tracking table failed. Human Intervention is needed! | Tracking item = {0} | "
                "Exception = {1}".format(tracking_item_key, e))


def _extract_keys_from_event(event: dict, namespace: str) -> dict:
    """
    Extracts IDs from event.
    :param event: Incoming event to event handler
    :param namespace: Namespace
    :return: Keys
    """
    try:
        sns_message = sns_extractor.extract_sns_message(event)
        event_keys = {
            'event-id': sns_message["event-id"],
            'event-name': sns_message["event-name"],
            'namespace-id': str(sns_message["{}-id".format(namespace)]),
            'request-time': sns_message["request-time"],
            'stage': sns_message["stage"]
        }
        if namespace == 'object':
            event_keys['collection-id'] = sns_message["collection-id"]
        return event_keys
    except KeyError as e:
        raise TerminalErrorException("Failed to extract IDs. Event = {0} | Namespace = {1} | Exception = {2}"
                                     .format(event, namespace, e))


def _is_oldest_event(oldest_event_id: str, event_id: str) -> bool:
    """
    Find if event is the oldest in line to be processed.
    :param: oldest_event_id: Oldest Event ID from tracking table
    :param: event_id: Event ID
    :return: True if oldest, False otherwise. Also returns False if item is empty
    """

    return oldest_event_id == event_id


def _get_oldest_item_by_tracking_id(tracking_id: str) -> dict:
    """
    Get oldest item from tracking table by tracking-id.
    This method is specific to the event handlers. The received event from event parameter is retried
    in case of a DDB throttle or is piped to the terminal errors bucket in case of a non throttle error.
    :param tracking_id: Tracking ID
    :return: Item dict
    """
    try:
        items = TrackingTable().query_items(KeyConditionExpression="TrackingID = :tracking_id",
                                            ExpressionAttributeValues={":tracking_id": {"S": tracking_id}},
                                            PaginationConfig={'MaxItems': 1},
                                            ConsistentRead=True,
                                            ScanIndexForward=True)
    except (ClientError, EndpointConnectionError) as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to get item by tracking-id. | Tracking ID = {0} | Exception = {1}".format(tracking_id, e))

    if not items:
        return {}
    return items[0]


def _delete_tracking_item(tracking_item: dict, event_id: str, stage: str) -> None:
    """
    Deletes event of the tracking table.
    :param: tracking_item: Tracking Table Item
    :param: event_id: Id of the event
    :param: stage: Stage of the request
    :return None
    """
    if not tracking_item:
        raise TerminalErrorException(
            "Failed to delete an item that does not exist in the tracking table. Event ID = {0}".format(event_id))

    try:
        TrackingTable().delete_item(
            {"tracking-id": tracking_item["tracking-id"], "request-time": tracking_item["request-time"]})
        logger.debug("Tracking Item Removed: {0}".format(tracking_item))
    except ClientError as e:
        logger.error(e)
        if e.response["Error"]["Code"] in RETRY_EXCEPTIONS:
            error_handler.throttle_error(
                {"event-id": event_id, "stage": stage, "event-name": event_names.TRACKING_REMOVE, "tracking-id": tracking_item["tracking-id"], "request-time": tracking_item["request-time"]},
                service="Tracking")
        else:
            raise TerminalErrorException(
                "Failed to delete item from tracking table. Event ID = {0} | Exception = {1}".format(event_id, e))
    except error_handler.retryable_exceptions as e:
        logger.error(e)
        error_handler.throttle_error(
            {"event-id": event_id, "stage": stage, "event-name": event_names.TRACKING_REMOVE, "tracking-id": tracking_item["tracking-id"], "request-time": tracking_item["request-time"]},
            service="Tracking")
    except Exception as e:
        raise TerminalErrorException(
            "Failed to delete item from tracking table. Event ID = {0} | Exception = {1}".format(event_id, e))


def _delete_collection_blocker(event: dict, stage: str) -> None:
    """
    Deletes event of the collection blocker table.
    :param: tracking_item: Tracking Table Item
    :param: event_id: Id of the event
    :param: stage: Stage of the request
    :return None
    """
    try:
        keys = _extract_keys_for_collection_blocker(event)
    except InternalError as ie:
        raise TerminalErrorException(
            "Failed to delete item from collection blocker table. Exception: {0} | Event {1}".format(ie, event))
    _delete_collection_blocker_by_key(keys['collection-id'], keys['request-time'], keys["request-id"], stage)


def _delete_collection_blocker_by_key(collection_id: str, request_time: str, event_id: str, stage: str) -> None:
    """
    Remove an item from the CollectionBlocker Table by primary key
    :param collection_id: Collection ID
    :param request_time: Request Time
    :param event_id: Event ID
    :param stage: Stage
    """
    primary_key = {"collection-id": collection_id, "request-time": request_time}
    try:
        CollectionBlockerTable().delete_item(primary_key)
        logger.info("Successfully removed collection blocker item: {}".format(primary_key))
    except ClientError as e:
        logger.error(e)
        if e.response["Error"]["Code"] in RETRY_EXCEPTIONS:
            error_handler.throttle_error(
                {"collection-id": collection_id, "event-id": event_id, "stage": stage,
                 "request-time": request_time, "event-name": event_names.TRACKING_REMOVE},
                service="Tracking")
        else:
            raise TerminalErrorException("Failed to delete item from collection blocker table. Event ID = {0} |"
                                         " Exception = {1}".format(event_id, e))
    except error_handler.retryable_exceptions as e:
        logger.error(e)
        error_handler.throttle_error(
            {"collection-id": collection_id, "event-id": event_id, "stage": stage,
             "request-time": request_time, "event-name": event_names.TRACKING_REMOVE},
            service="Tracking")
    except Exception as e:
        raise TerminalErrorException(
            "Failed to delete item from collection blocker table. Event ID: {0} | Exception: {1}".format(event_id, e))


def _get_expiration_epoch_for_upload_events(resource_path: str) -> int:
    resource = resource_path.split('/')[-1]
    if resource in ('multipart', 'large'):
        return __get_expiration_epoch(3)
    if resource == 'folder':
        return __get_expiration_epoch(1.25)
    return 0


def __get_expiration_epoch(expiration_offset: float) -> int:
    return int((datetime.now() + timedelta(hours=expiration_offset)).timestamp())


def _extract_changeset_id_if_exists(request: dict) -> str:
    return request['changeset-id'] if 'changeset-id' in request else ''


if __name__ == '__main__':
    from lng_aws_clients import session

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL TEST RUN - BEGIN]")
    feature = 'feature-jtm'
    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["TRACKING_DYNAMODB_TABLE"] = "{0}-DataLake-TrackingTable".format(feature)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(feature)
    TrackingTable().table_name = os.getenv("TRACKING_DYNAMODB_TABLE")
    os.environ[
        "RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/" \
                             "{0}-DataLake-RetryQueues-RetryQueue-1XUZYW1ISN027".format(feature)
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")
    session.set_session()

    logger.debug("[LOCAL TEST RUN - Tracking Table]")
    tracking_item1 = {"event-id": "abc001", "tracking-id": "1000001_collection", "request-time": "2018-12-01"}
    tracking_item2 = {"event-id": "abc002", "tracking-id": "1000001_collection", "request-time": "2018-12-02"}
    tracking_item3 = {"event-id": "abc003", "tracking-id": "1000001_collection", "request-time": "2018-12-03"}
    tracking_item4 = {"event-id": "abc004", "tracking-id": "1000001_collection", "request-time": "2018-12-04"}
    tracking_item5 = {"event-id": "abc005", "tracking-id": "1000001_collection", "request-time": "2018-12-05"}
    _put_event_to_tracking_table(tracking_item1)
    _put_event_to_tracking_table(tracking_item2)
    _put_event_to_tracking_table(tracking_item3)
    _put_event_to_tracking_table(tracking_item4)
    _put_event_to_tracking_table(tracking_item5)

    item_ = _get_oldest_item_by_tracking_id(tracking_id="1000001_collection")
    logger.debug(item_)
    item_['event-name'] = event_names.TRACKING_REMOVE
    error_handler.throttle_error(item_, service="Tracking")
    _delete_tracking_item(tracking_item1, event_id="abc001", stage='TEST')
    _delete_tracking_item(tracking_item2, event_id="abc002", stage='TEST')
    _delete_tracking_item(tracking_item3, event_id="abc003", stage='TEST')
    _delete_tracking_item(tracking_item4, event_id="abc004", stage='TEST')
    _delete_tracking_item(tracking_item5, event_id="abc005", stage='TEST')

    logger.debug("[LOCAL TEST RUN - Collection Blocker Table]")
    event_item1 = {
        "context": {
            "client-request-id": "mock-5202-4931-a28d-48d577da1d15",
            "client-request-time": datetime.now(),
            "client-type": "aws-apigateway",
            "stage": "TEST",
            "client-id": "mock-client-id",
            "http-method": "POST",
            "resource-id": "mock-resource-id",
            "resource-path": "/{objectId}",
            "api-key-id": "mock-api-key-id"
        },
        "request": {
            "collection-id": "mock-collection-1575275545",
            "object-id": "mock-object-id-505353",
            "content-type": "application/json",
            "content-md5": "mock-md5",
            "is-large-object": False
        }
    }
    _put_collection_blocker(event_item1, event_names.OBJECT_CREATE)
    _put_collection_blocker(event_item1, event_names.OBJECT_CREATE)

    _delete_collection_blocker(event_item1, "TEST")

    logger.debug("[LOCAL TEST RUN - Untrack Command]")
    _put_collection_blocker(event_item1, event_names.OBJECT_CREATE_INGESTION)
    try:
        raise InternalError("My Exception")
    except DataLakeException as dle:
        un_track_command_by_event(event_item1, event_names.OBJECT_CREATE_INGESTION, 'v1')
        raise dle
