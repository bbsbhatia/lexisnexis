import logging
import os

from lng_aws_clients import session
from time import time

__author__ = "John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Default to 44 minutes since tokens renew every hour and lambdas can run for 15 minutes
_session_ttl = os.getenv('SESSION_TTL', 44 * 60)
# Setting to zero will guarantee session initialization
_session_start_epoch = 0


def lng_aws_session(**kwargs_session):
    global _session_ttl
    if 'session_ttl' in kwargs_session:
        _session_ttl = kwargs_session['session_ttl']
        del kwargs_session['session_ttl']
        logger.debug("Setting AWS Session TTL to {}".format(_session_ttl))

    def wrapper(func):
        def func_wrapper(*args, **kwargs):
            global _session_start_epoch
            curr_epoch = time()
            session_age = curr_epoch - _session_start_epoch
            if session_age >= _session_ttl:
                logger.info("Initializing AWS Session")
                session.set_session(**kwargs_session)
                _session_start_epoch = curr_epoch
            return func(*args, **kwargs)

        return func_wrapper

    return wrapper
