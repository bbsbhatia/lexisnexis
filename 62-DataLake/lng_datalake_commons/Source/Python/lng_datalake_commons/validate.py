from fastjsonschema import validate


def is_valid_input_json(input_json, input_schema):
    ret_val = True

    try:
        validate(input_schema, input_json)
    except Exception:
        ret_val = False

    return ret_val


def is_valid_event_message(message: dict, *event_names) -> bool:
    """
    Checks if the message contains the appropriate event-name to continue to process
    :param message: message to check if the event-name matches the passed event-name
    :param event_names: all the event_name constants to match against the message
    :return: True if the message isn't empty and the event-name matches
    """
    if not message or message.get('event-name') not in event_names:
        return False
    return True


def is_valid_event_version(message: dict, event_version) -> bool:
    """
    Checks if the message contains the appropriate event-version to continue to process
    :param message: message to check if the event-version matches the passed event-version
    :param event_version: the event_version constant to match against the message
    :return: True if the message isn't empty and the event-version matches
    """
    if not message or message['event-version'] != event_version:
        return False
    return True


def is_valid_event_stage(message: dict, lambda_arn) -> bool:
    """
    Checks if the message contains the appropriate stage to continue to process
    :param message: message to check if the stage matches the stage in the lambda_arn
    :param lambda_arn: the lambda arn to parse for stage
    :return: True if the message isn't empty, the lambda_arn contains that stage and the stage matches
    """
    try:
        stage = lambda_arn.split(":")[7]
    except IndexError:
        return False
    if not message or message['stage'] != stage:
        return False
    return True
