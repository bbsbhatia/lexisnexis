from datetime import datetime

__author__ = "Mark Seitter, Shekhar Ralhan"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


def get_current_timestamp():
    return format_time(datetime.utcnow())


def format_time(value) -> str:
    try:
        if isinstance(value, (int, str)):
            # Be sure we have micoseconds in epoch, otherwise pad with 0
            pad_value = int("{:0<13d}".format(int(value)))
            return _format_datetime_object(datetime.utcfromtimestamp(pad_value / 1000))
        else:
            return _format_datetime_object(value)
    except (ValueError, TypeError, OSError) as e:
        raise ValueError('Invalid timestamp {0}: {1}'.format(value, e))


def _format_datetime_object(date_obj: datetime) -> str:
    iso_str = date_obj.isoformat()
    # If micoseconds are 0, isoformat won't return them, but we want consistent format so we add them
    if '.' not in iso_str:
        iso_str = "{0}.000".format(iso_str)

    return "{0}{1}".format(iso_str[:23], "Z")
