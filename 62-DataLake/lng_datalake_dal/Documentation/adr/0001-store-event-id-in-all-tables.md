# Store event ids in all tables

Date: 2019-09-09

## Status:  Accepted

* Deciders: Wormhole Team

## Context
* Currently apart from tracking and event-store table we do not store event ids in any other tables.
* During maintenance and debugging if we have to find out an event-id that created/updated/removed an item we have 
to dig through logs which can take some time. If we stored the event id with the item in the table then it would help 
with maintenance activities. 
* Also it would be helpful if we stored certain number of previous event ids apart from the current event id 
to see previous events that affected the item.

## Decision
* We will store up to three event ids (latest and two previous) as list in all tables except ObjectStoreVersionTable and CatalogCollectionMappingTable. Attribute will be named as event-ids.
* For object-store-version table since every event creates a new item,  we will store only that event id as string. Attribute will be named as event-id. 
* CatalogCollectionMappingTable will also store event-id as string. Attribute will be named as event-id.
* All table schemas will be updated to make event-ids/event-id as required attribute.
* Event-ids stored will be used for internal purposes only and will not be exposed to clients.
* Following tables will not be updated to store event-ids :
  * TrackingTable, EventStoreTable, EventStoreBackendTables and CollectionBlockerTable - already store event-ids.
  * RepublishCollectionTable - RepublishID is the event-id for this table.
  * ObjectRelationshipTable, RelationshipCommitmentTable, RelationshipOwnerTable - design for these tables may change.
  * AssetTable, CounterTable

## Consequences
* Additional attribute will be stored in all updated tables.
* Additional code will be added to write event ids to tables. 

Note that in case of duplicate requests where the 2nd event is no-op, we wont be storing the no-op event id.

## Links
* [Version One Story S-69621](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:760636)