import os
import unittest
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.tracking_table import TrackingTable

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'TrackingTable')


@patch.dict('os.environ',
            values={'TRACKING_DYNAMODB_TABLE': 'DataLakeDynamo-TrackingTable-1ABCTDW533FLF'},
            clear=True)
class TestTrackingTable(unittest.TestCase):
    # +TrackingTable.__init__ - can create tracking table object and load schema
    @patch.dict(os.environ, {'TRACKING_DYNAMODB_TABLE': 'DataLakeDynamo-TrackingTable-1ABCTDW533FLF'})
    def test_tracking_table_init(self):
        tt = TrackingTable()
        self.assertIsNotNone(tt)
        self.assertEqual(tt.table_name, 'DataLakeDynamo-TrackingTable-1ABCTDW533FLF')

    # -TrackingTable.update_counters - raises NotImplementedError
    def test_tracking_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            TrackingTable().update_counter(primary_key)

    # -TrackingTable.update_counters - raises NotImplementedError
    def test_tracking_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            TrackingTable().update_counters(primary_key, {"Counter": 1})


if __name__ == '__main__':
    unittest.main()
