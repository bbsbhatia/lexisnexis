import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.catalog_table import CatalogTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table

__author__ = "John Morelock, Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CatalogTable')


@patch.dict('os.environ',
            values={'CATALOG_DYNAMODB_TABLE': 'feature-DataLake-CatalogTable'},
            clear=True)
class TestCatalogTable(unittest.TestCase):
    # +CatalogTable.__init__ - can create catalog table object and load schema
    def test_catalog_table_init(self):
        ct = CatalogTable()
        self.assertIsNotNone(ct)
        self.assertEqual(ct.table_name, 'feature-DataLake-CatalogTable')

    # +CatalogTable.put_item
    def test_catalog_table_put_item(self):
        test_key_dict = {'catalog-id': 'catalog-01'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(CatalogID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create CatalogTable super class that we want to test
            ost = CatalogTable()
            ost.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -CatalogTable.put_item - raises ConditionError
    def test_catalog_table_put_item_fail1(self):
        test_key_dict = {'catalog-id': 'catalog-01'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create CatalogTable super class that we want to test
            with self.assertRaises(ConditionError):
                CatalogTable().put_item(put_dict)

    # +CatalogTable.update_item
    def test_catalog_table_update_item(self):
        test_key_dict = {'catalog-id': 12345}
        test_attrs_dict = \
            {
                'description': 'New description'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create CatalogTable super class that we want to test
            ost = CatalogTable()
            ost.update_item(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict, None)

    # -CatalogTable.update_counter - raises NotImplementedError
    def test_catalog_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CatalogTable().update_counter(primary_key)

    # -CatalogTable.update_counters - raises NotImplementedError
    def test_catalog_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CatalogTable().update_counters(primary_key, {"Counter": 1})

    # -CatalogTable.batch_write_items - raises NotImplementedError
    def test_catalog_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            CatalogTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
