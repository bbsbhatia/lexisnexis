import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_dal.event_dedupe_table import EventDedupeTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


@patch.dict('os.environ',
            values={'EVENT_DEDUPE_DYNAMODB_TABLE': 'DataLakeDynamo-EventDedupeTable-xyz'},
            clear=True)
class TestEventDedupeTable(unittest.TestCase):
    # +EventDedupeTable.__init__ - can create catalog table object and load schema
    def test_event_store_table_init(self):
        est = EventDedupeTable()
        self.assertIsNotNone(est)
        self.assertEqual(est.table_name, 'DataLakeDynamo-EventDedupeTable-xyz')

    # -EventDedupeTable.get_all_items - raises NotImplementedError
    def test_event_store_table_get_all_items(self):
        with self.assertRaises(NotImplementedError):
            EventDedupeTable().get_all_items()

    # -EventDedupeTable.update_counter - raises NotImplementedError
    def test_event_store_table_get_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            EventDedupeTable().update_counter(primary_key)

    # -EventDedupeTable.update_counters - raises NotImplementedError
    def test_event_store_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            EventDedupeTable().update_counters(primary_key, {"Counter": 1})

    # +EventDedupeTable.put_item
    def test_event_store_table_put_item(self):
        test_key_dict = {'message-id': 'xyz', 'stream': '2017-11-13T10:16:08.829755'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(MessageID) and attribute_not_exists(StreamArn)'

        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'put_item') as mock_method:
            est = EventDedupeTable()
            est.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition, ReturnValues="NONE")

    # -EventDedupeTable.put_item - raises ConditionError
    def test_event_store_table_put_item_fail1(self):
        test_key_dict = {'message-id': 'xyz', 'stream': '2017-11-13T10:16:08.829755'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as _:
            with self.assertRaises(ConditionError):
                EventDedupeTable().put_item(put_dict)


if __name__ == '__main__':
    unittest.main()
