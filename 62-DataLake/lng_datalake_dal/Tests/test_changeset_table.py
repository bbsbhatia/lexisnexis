import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.changeset_table import ChangesetTable
from lng_datalake_dal.table import Table

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ChangesetTable')


@patch.dict('os.environ',
            values={'CHANGESET_DYNAMODB_TABLE': 'DataLakeDynamo-ChangesetTable'},
            clear=True)
class TestChangesetTable(unittest.TestCase):
    # +ChangesetTable.__init__ - can create ChangesetTable object and load schema
    def test_changeset_table_init(self):
        ct = ChangesetTable()
        self.assertIsNotNone(ct)
        self.assertEqual(ct.table_name, 'DataLakeDynamo-ChangesetTable')

    # +ChangesetTable.put_item
    def test_changeset_table_put_item(self):
        test_key_dict = {"changeset-id": "123"}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(ChangesetID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ChangesetTable super class that we want to test
            ct = ChangesetTable()
            ct.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -ChangesetTable.put_item - raises ConditionError
    def test_changeset_table_put_item_fail1(self):
        test_key_dict = {"changeset-id": "123"}

        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create ChangesetTable super class that we want to test
            with self.assertRaisesRegex(ConditionError, 'already exists in Changeset table'):
                ChangesetTable().put_item(put_dict)

    # +ChangesetTable.update_item - success
    def test_changeset_table_update_item_success(self):
        test_key_dict = {'changeset-id': '12345'}
        test_attrs_dict = \
            {
                "changeset-state": "Opened",
                "changeset-timestamp": "2019-06-03T16:09:33.861Z"
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        condition = 'attribute_exists(ChangesetID)'
        with mock.patch.object(Table, 'put_item') as mock_method:
            self.assertIsNone(ChangesetTable().update_item(update_dict, condition=condition))

    # - test_changeset_table_update_item_item_fail_exception
    def test_changeset_table_update_item_fail_exception(self):
        test_key_dict = {'changeset-id': '12345'}
        test_attrs_dict = \
            {
                'changeset-state': 'Closed'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaisesRegex(Exception,
                                    r'Update must contain a condition with attribute_exists\(ChangesetID\)'):
            ChangesetTable().update_item(update_dict)

    # -ChangesetTable.update_counter - raises NotImplementedError
    def test_changeset_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ChangesetTable().update_counter(primary_key)

    # -ChangesetTable.update_counters - raises NotImplementedError
    def test_changeset_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ChangesetTable().update_counters(primary_key, {"Counter": 1})

    # -ChangesetTable.batch_write_items - raises NotImplementedError
    def test_changeset_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            ChangesetTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
