import os
import unittest

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError, SchemaError, SchemaValidationError, SchemaLoadError

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'EventStoreTable')


class TestCatalogTable(unittest.TestCase):
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'DataLakeDynamo-EventStoreTable-19J1JQ600ZQG9'

    # +ConditionError - can create object
    def test_condition_error(self):
        self.assertIsNotNone(ConditionError())

    # +SchemaError - can create object
    def test_schema_error(self):
        self.assertIsNotNone(SchemaError())

    # +SchemaValidationError - can create object
    def test_schema_validation_error(self):
        self.assertIsNotNone(SchemaValidationError())

    # +SchemaLoadError - can create object
    def test_schema_load_error(self):
        self.assertIsNotNone(SchemaLoadError())


if __name__ == '__main__':
    unittest.main()
