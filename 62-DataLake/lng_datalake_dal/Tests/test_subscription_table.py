import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.subscription_table import SubscriptionTable
from lng_datalake_dal.table import Table

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'SubscriptionTable')


@patch.dict('os.environ',
            values={'SUBSCRIPTION_DYNAMODB_TABLE': 'DataLakeDynamo-SubscriptionTable-1S3JN68FQK7IF'},
            clear=True)
class TestSubscriptionTable(unittest.TestCase):
    # +SubscriptionTable.__init__ - can create SubscriptionTable object and load schema
    def test_subscription_table_init(self):
        st = SubscriptionTable()
        self.assertIsNotNone(st)
        self.assertEqual(st.table_name, 'DataLakeDynamo-SubscriptionTable-1S3JN68FQK7IF')

    # +SubscriptionTable.put_item - raises NotImplementedError
    def test_subscription_table_put_item(self):
        test_key_dict = {"subscription-id": "Testing123-HashOfSubIdColIdEventName"}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(SubscriptionID)'
        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create CatalogTable super class that we want to test
            ost = SubscriptionTable()
            ost.put_item(put_dict, )
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -SubscriptionTable.put_item - raises ConditionError
    def test_subscription_table_put_item_fail1(self):
        test_key_dict = {
            "subscription-id": "Testing123-HashOfSubIdColIdEventName",
            "file-id": "1234"
        }
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create CatalogTable super class that we want to test
            with self.assertRaises(ConditionError):
                SubscriptionTable().put_item(put_dict)

    # +SubscriptionTable.update_counter - raises NotImplementedError
    def test_subscription_table_update_counter(self):
        with self.assertRaises(NotImplementedError):
            SubscriptionTable().update_counter('Test')

    # -SubscriptionTable.update_counters - raises NotImplementedError
    def test_subscription_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            SubscriptionTable().update_counters(primary_key, {"Counter": 1})

    # +SubscriptionTable.update_item
    def test_subscription_table_update_item(self):
        test_key_dict = {'subscription-id': 123}
        test_attrs_dict = {"subscription-id": 123,
                           "subscription-state": "valid state",
                           "subscription-name": "xyz",
                           "endpoint": "xyz@test.com",
                           "protocol": "email",
                           "filter": {},
                           "subscription-pending-confirmation": True,
                           "owner-id": 100}
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create SubscriptionTable super class that we want to test
            st = SubscriptionTable()
            st.update_item(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict, None)

    # -SubscriptionTable.batch_write_items - raises NotImplementedError
    def test_subscription_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            SubscriptionTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
