import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.owner_table import OwnerTable
from lng_datalake_dal.table import Table

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'OwnerTable')


@patch.dict('os.environ',
            values={'OWNER_DYNAMODB_TABLE': 'DataLakeDynamo-OwnerTable-1ADVZ3OZKTY6M'},
            clear=True)
class TestOwnerTable(unittest.TestCase):
    # +OwnerTable.__init__ - can create OwnerTable object and load schema
    def test_owner_table_init(self):
        cst = OwnerTable()
        self.assertIsNotNone(cst)
        self.assertEqual(cst.table_name, 'DataLakeDynamo-OwnerTable-1ADVZ3OZKTY6M')

    # -OwnerTable.update_counter - raises NotImplementedError
    def test_owner_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            OwnerTable().update_counter(primary_key)

    # -OwnerTable.update_counters - raises NotImplementedError
    def test_owner_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            OwnerTable().update_counters(primary_key, {"Counter": 1})

    # -OwnerTable.batch_write_items - raises NotImplementedError
    def test_owner_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            OwnerTable().batch_write_items()

    # +OwnerTable.put_item
    def test_collection_table_put_item(self):
        test_key_dict = {'owner-id': '12'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(OwnerID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            ost = OwnerTable()
            ost.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -OwnerTable.put_item - raises ConditionError
    def test_collection_table_put_item_fail1(self):
        test_key_dict = {'owner-id': '12'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            with self.assertRaises(ConditionError):
                OwnerTable().put_item(put_dict)

    # +OwnerTable.update_item
    def test_owner_table_update_item(self):
        test_key_dict = {'foo': 'bar'}
        test_attrs_dict = \
            {
                'doesnotmatter': 'no validation here'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create RelationshipOwnerTable super class that we want to test
            ost = OwnerTable()
            ost.update_item(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict, condition='attribute_exists(OwnerID)')

    # -OwnerTable.update_item - raises ConditionError
    def test_collection_table_update_item_fail(self):
        test_key_dict = {'owner-id': '12'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            with self.assertRaises(ConditionError):
                OwnerTable().update_item(put_dict)


if __name__ == '__main__':
    unittest.main()
