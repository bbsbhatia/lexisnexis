import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.object_store_table import ObjectStoreTable
from lng_datalake_dal.table import Table
from lng_datalake_dal.transaction import Transaction

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ObjectStoreTable')


@patch.dict('os.environ',
            values={'OBJECT_STORE_DYNAMODB_TABLE': 'DataLakeDynamo-ObjectStoreTable-1IU3DI07UKZQR'},
            clear=True)
class TestObjectStoreTable(unittest.TestCase):
    # +ObjectStoreTable.__init__ - can create ObjectStoreTable object and load schema
    def test_object_store_table_init(self):
        ost = ObjectStoreTable()
        self.assertIsNotNone(ost)
        self.assertEqual(ost.table_name, 'DataLakeDynamo-ObjectStoreTable-1IU3DI07UKZQR')

    # +ObjectStoreTable.put_item
    def test_object_store_table_put_item(self):
        test_key_dict = {'object-id': 'ABCDefgh', 'collection-id': 2}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = '(attribute_not_exists(ObjectID) and attribute_not_exists(CollectionID)) or ObjectState =:state'
        put_expression = {":state": "Removed"}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = ObjectStoreTable()
            ost.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition, expression_values=put_expression)

    # -ObjectStoreTable.put_item - raises ConditionError
    def test_object_store_table_put_item_fail1(self):
        test_key_dict = {'object-id': 'ABCDefgh', 'collection-id': 2}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create ObjectStoreTable super class that we want to test
            with self.assertRaises(ConditionError):
                ObjectStoreTable().put_item(put_dict)

    # +ObjectStoreTable.update_item
    def test_object_store_table_update_item(self):
        test_key_dict = {'object-id': 'ABCDefgh', 'collection-id': 2}
        test_attrs_dict = \
            {
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created",
                'version-number': 2,
                'version-timestamp': '2018-02-20T10:53:56.605653'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = ObjectStoreTable()
            ost.update_item(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict,
                                           condition='VersionTimestamp < :v AND VersionNumber = :n',
                                           expression_values={':v': test_attrs_dict['version-timestamp'],
                                                              ':n': test_attrs_dict['version-number'] - 1})

    # -ObjectStoreTable.update_item - raises ConditionError
    def test_object_store_table_update_item_fail_1(self):
        test_key_dict = {'object-id': 'ABCDefgh', 'collection-id': 2}
        test_attrs_dict = \
            {
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created",
                'version-number': 2,
                'version-timestamp': '2018-02-20T10:53:56.605653'
            }
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.update_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create ObjectStoreTable super class that we want to test
            with self.assertRaises(ConditionError):
                ObjectStoreTable().update_item(put_dict)

    # -ObjectStoreTable.update_item - raises KeyError: missing version-number
    def test_object_store_table_update_item_fail_2(self):
        test_key_dict = {'object-id': 'ABCDefgh', 'collection-id': 2}
        test_attrs_dict = \
            {
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created",
                'version-timestamp': '2018-02-20T10:53:56.605653'
            }
        put_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaisesRegex(KeyError, "Missing required attribute: 'version-number'"):
            ObjectStoreTable().update_item(put_dict)

    # +ObjectStoreTable.put_transaction_item
    def test_table_put_transaction_item(self):
        transaction = Transaction()

        put_condition = '(attribute_not_exists(ObjectID) and attribute_not_exists(CollectionID)) or ObjectState =:state'
        put_expression = {":state": {"S": "Removed"}}
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_attrs_dict = {
            'bucket-name': 'aaron',
            'object-key': 'key1',
            'object-state': 'Created',
            'content-type': 'text/plain',
            'raw-content-length': 10,
            'version-number': 1,
            'version-timestamp': '10101',
            'collection-hash': 'da4b9237bacccdf19c0760cab7aec4a8359010b0'
        }
        test_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_transaction_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = ObjectStoreTable()
            ost.put_transaction_item(test_dict, transaction)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(test_dict, transaction, condition=put_condition,
                                           expression_values=put_expression)

    # +ObjectStoreTable.update_transaction_item
    def test_object_store_table_update_transaction_item(self):
        test_key_dict = {'object-id': 'ABCDefgh', 'collection-id': '2'}
        test_attrs_dict = \
            {
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created",
                'version-number': 2,
                'version-timestamp': '123123123'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        trans = Transaction()
        with mock.patch.object(Table, 'put_transaction_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = ObjectStoreTable()
            ost.update_transaction_item(update_dict, trans)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict, trans,
                                           condition='VersionTimestamp < :v AND VersionNumber = :n',
                                           expression_values={':v': {"S": test_attrs_dict['version-timestamp']},
                                                              ':n': {"N": str(test_attrs_dict['version-number'] - 1)}})

    # -ObjectStoreTable.update_transaction_item: KeyError
    def test_object_store_table_update_transaction_item_key_error(self):
        test_key_dict = {'object-id': 'ABCDefgh', 'collection-id': '2'}
        test_attrs_dict = \
            {
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created",
                'version-timestamp': '123123123'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        trans = Transaction()
        ost = ObjectStoreTable()
        with self.assertRaisesRegex(KeyError, "Missing required attribute: 'version-number'"):
            ost.update_transaction_item(update_dict, trans)

    # +ObjectStoreTable.update_counter - raises NotImplementedError
    def test_object_store_table_update_counter(self):
        with self.assertRaises(NotImplementedError):
            ObjectStoreTable().update_counter('Test')

    # -ObjectStoreTable.update_counters - raises NotImplementedError
    def test_object_store_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ObjectStoreTable().update_counters(primary_key, {"Counter": 1})

    # +ObjectStoreTable.batch_write_items - raises NotImplementedError
    def test_object_store_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            ObjectStoreTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
