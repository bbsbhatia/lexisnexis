import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.object_relationship_table import ObjectRelationshipTable
from lng_datalake_dal.table import Table

__author__ = "Doug Heitkamp, Shekhar Ralhan"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ObjectRelationshipTable')


@patch.dict('os.environ',
            values={'OBJECT_RELATIONSHIP_DYNAMODB_TABLE': 'DataLakeDynamo-ObjectRelationshipTable-1ADVZ3OZKTY6M'},
            clear=True)
class TestObjectRelationshipTable(unittest.TestCase):
    # +ObjectRelationshipTable.__init__ - can create OwnerTable object and load schema
    def test_object_relationship_table_init(self):
        table = ObjectRelationshipTable()
        self.assertIsNotNone(table)
        self.assertEqual(table.table_name, 'DataLakeDynamo-ObjectRelationshipTable-1ADVZ3OZKTY6M')

    # -ObjectRelationshipTable.update_counter - raises NotImplementedError
    def test_object_relationship_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ObjectRelationshipTable().update_counter(primary_key)

    # -ObjectRelationshipTable.update_counters - raises NotImplementedError
    def test_object_relationship_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ObjectRelationshipTable().update_counters(primary_key, {"Counter": 1})

    # -ObjectRelationshipTable.batch_write_items - raises NotImplementedError
    def test_object_relationship_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            ObjectRelationshipTable().batch_write_items()

    # -ObjectRelationshipTable.batch_write_items - raises NotImplementedError
    def test_object_relationship_table_get_all_items(self):
        with self.assertRaises(NotImplementedError):
            ObjectRelationshipTable().get_all_items()

    # +ObjectRelationshipTable.put_item
    def test_object_relationship_table_put_item(self):
        test_key_dict = {'target-id': 'mock-target-id', 'relationship-key': 'mock-relationship-key'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(TargetID) and attribute_not_exists(RelationshipKey)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectRelationshipTable super class that we want to test
            table = ObjectRelationshipTable()
            table.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -ObjectRelationshipTable.put_item - raises ConditionError
    def test_object_relationship_table_put_item_fail(self):
        test_key_dict = {'target-id': 'mock-target-id', 'relationship-key': 'mock-relationship-key'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError):
            with self.assertRaises(ConditionError):
                ObjectRelationshipTable().put_item(put_dict)

    # +ObjectRelationshipTable.update_item
    def test_object_relationship_table_update_item(self):
        test_key_dict = {'target-id': 'mock-target-id', 'relationship-key': 'mock-relationship-key'}
        test_attrs_dict = \
            {
                'doesnotmatter': 'no validation here'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        update_condition = 'attribute_exists(TargetID) and attribute_exists(RelationshipKey)'
        with mock.patch.object(Table, 'put_item'):
            self.assertIsNone(ObjectRelationshipTable().update_item(update_dict, condition=update_condition))

    # -ObjectRelationshipTable.update_item - Exception
    def test_object_relationship_table_update_item_fail(self):
        test_key_dict = {'target-id': 'mock-target-id', 'relationship-key': 'mock-relationship-key'}
        test_attrs_dict = \
            {
                'doesnotmatter': 'no validation here'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaisesRegex(Exception, 'Update must contain a condition with attribute_exists'):
            ObjectRelationshipTable().update_item(update_dict)

    # +ObjectRelationshipTable.generate_relationship_key - produces a value
    def test_generate_relationship_key(self):
        self.assertIsNotNone(ObjectRelationshipTable.generate_relationship_key('foo', 1))

    # +ObjectRelationshipTable.unpack_relationship_key - produces tuple
    def test_unpack_relationship_key(self):
        self.assertTupleEqual(ObjectRelationshipTable.unpack_relationship_key("judges|v00003"), ("judges", 3))

    # -ObjectRelationshipTable.unpack_relationship_key - produces ValueError
    def test_unpack_relationship_key_value_error(self):
        with self.assertRaisesRegex(ValueError, 'Could not unpack version number'):
            ObjectRelationshipTable.unpack_relationship_key("judges|v00x03")

    # +ObjectRelationshipTable.generate_object_id - produces a value
    def test_generate_object_id(self):
        self.assertIsNotNone(ObjectRelationshipTable.generate_object_composite_id('foo', 'bar', 1))


if __name__ == '__main__':
    unittest.main()
