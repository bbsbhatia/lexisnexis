import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.collection_blocker_table import CollectionBlockerTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CollectionBlockerTable')

collection_blocker_table_name = 'feature-jek-DataLake-CollectionBlockerTable'


@patch.dict('os.environ',
            values={'COLLECTION_BLOCKER_DYNAMODB_TABLE': collection_blocker_table_name},
            clear=True)
class TestCollectionBlockerTable(unittest.TestCase):
    # +CollectionBlockerTable.__init__ - can create collection blocker table object and load schema
    def test_collection_blocker_table_init(self):
        cbt = CollectionBlockerTable()
        self.assertIsNotNone(cbt)
        self.assertEqual(cbt.table_name, collection_blocker_table_name)

    # +CollectionBlockerTable.put_item
    def test_collection_blocker_table_put_item(self):
        test_key_dict = {'collection-id': 'collection-01'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(CollectionID) and attribute_not_exists(RequestTime)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create CollectionBlockerTable super class that we want to test
            cbt = CollectionBlockerTable()
            cbt.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -CollectionBlockerTable.put_item - raises ConditionError
    def test_collection_blocker_table_put_item_fail1(self):
        test_key_dict = {'collection-id': 'collection-01'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError):
            # Create CollectionBlockerTable super class that we want to test
            with self.assertRaises(ConditionError):
                CollectionBlockerTable().put_item(put_dict)

    ####################################################################################################################
    # +CollectionBlockerTable.update_item - success
    def test_collection_blocker_table_update_item_success(self):
        test_key_dict = {'collection-id': '12345', 'request-time': '1999-10-12'}
        test_attrs_dict = \
            {
                'event-name': 'Object::Remove',
                'request-id': '123456',
                'pending-expiration-epoch': 1234567890
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        condition = 'attribute_exists(CollectionID)'
        with mock.patch.object(Table, 'put_item') as mock_method:
            self.assertIsNone(CollectionBlockerTable().update_item(update_dict, condition=condition))

    # -test_collection_blocker_table_update_item_fail_exception
    def test_collection_blocker_table_update_item_fail_exception(self):
        test_key_dict = {'collection-id': 12345}
        test_attrs_dict = \
            {
                'description': 'New description'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaisesRegex(Exception,
                                    r'Update must contain a condition with attribute_exists\(CollectionID\)'):
            CollectionBlockerTable().update_item(update_dict)

    # -CollectionBlockerTable.update_counter - raises NotImplementedError
    def test_collection_blocker_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CollectionBlockerTable().update_counter(primary_key)

    # -CollectionBlockerTable.update_counters - raises NotImplementedError
    def test_collection_blocker_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CollectionBlockerTable().update_counters(primary_key, {"Counter": 1})

    # -CollectionBlockerTable.batch_write_items - raises NotImplementedError
    def test_collection_blocker_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            CollectionBlockerTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
