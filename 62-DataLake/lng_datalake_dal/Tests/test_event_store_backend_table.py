import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.event_store_backend_table import EventStoreBackendTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'EventStoreTable')


@patch.dict('os.environ',
            values={'EVENT_STORE_BACKEND_DYNAMODB_TABLE': 'DataLakeDynamo-EventStoreBackendTable-19J1JQ600ZQG9'},
            clear=True)
class TestEventStoreBackendTable(unittest.TestCase):
    # +EventStoreBackendTable.__init__ - can create catalog table object and load schema
    def test_event_store_table_init(self):
        est = EventStoreBackendTable()
        self.assertIsNotNone(est)
        self.assertEqual(est.table_name, 'DataLakeDynamo-EventStoreBackendTable-19J1JQ600ZQG9')

    # -EventStoreBackendTable.get_all_items - raises NotImplementedError
    def test_event_store_backend_table_get_all_items(self):
        with self.assertRaises(NotImplementedError):
            EventStoreBackendTable().get_all_items()

    # -EventStoreBackendTable.query_items - raises NotImplementedError
    def test_event_store_backend_table_query_items(self):
        with self.assertRaises(NotImplementedError):
            EventStoreBackendTable().query_items()

    # -EventStoreBackendTable.update_counter - raises NotImplementedError
    def test_event_store_backend_table_get_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            EventStoreBackendTable().update_counter(primary_key)

    # -EventStoreBackendTable.update_counters - raises NotImplementedError
    def test_event_store_backend_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            EventStoreBackendTable().update_counters(primary_key, {"Counter": 1})

    # +EventStoreBackendTable.put_item
    def test_event_store_backend_table_put_item(self):
        test_key_dict = {'event-id': 'xyz', 'request-time': '2017-11-13T10:16:08.829755'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(EventID) and attribute_not_exists(RequestTime)'

        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'put_item') as mock_method:
            est = EventStoreBackendTable()
            est.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -EventStoreBackendTable.put_item - raises ConditionError
    def test_event_store_backend_table_put_item_fail1(self):
        test_key_dict = {'event-id': 'xyz', 'request-time': '2017-11-13T10:16:08.829755'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as _:
            with self.assertRaises(ConditionError):
                EventStoreBackendTable().put_item(put_dict)


if __name__ == '__main__':
    print(unittest.main())
