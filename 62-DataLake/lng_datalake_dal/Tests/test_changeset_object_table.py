import unittest
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.changeset_object_table import ChangesetObjectTable

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ChangesetObjectTable')


@patch.dict('os.environ',
            values={'CHANGESET_OBJECT_DYNAMODB_TABLE': 'DataLakeDynamo-ChangesetObjectTable-YX'},
            clear=True)
class TestChangesetObjectTable(unittest.TestCase):
    # +ChangesetObjectTable.__init__ - can create ChangesetObjectTable object and load schema
    def test_changeset_object_table_init(self):
        cot = ChangesetObjectTable()
        self.assertIsNotNone(cot)
        self.assertEqual(cot.table_name, 'DataLakeDynamo-ChangesetObjectTable-YX')

    # +ChangesetObjectTable.generate_composite_key - keys are equal
    def test_changeset_object_table_generate_composite_key(self):
        self.assertEqual(ChangesetObjectTable.generate_composite_key('ObjId', 'CollectionId', 'VersionNo'),
                         'ObjId|CollectionId|VersionNo')

    # -ChangesetObjectTable.get_all_items - raises NotImplementedError
    def test_changeset_object_table_get_all_items(self):
        with self.assertRaises(NotImplementedError):
            ChangesetObjectTable().get_all_items()

    # +ChangesetObjectTable.update_counter - raises NotImplementedError
    def test_changeset_object_table_update_counter(self):
        with self.assertRaises(NotImplementedError):
            ChangesetObjectTable().update_counter('Test')

    # -ChangesetObjectTable.update_counters - raises NotImplementedError
    def test_object_store_version_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ChangesetObjectTable().update_counters(primary_key, {"Counter": 1})

    # +ChangesetObjectTable.batch_write_items - raises NotImplementedError
    def test_changeset_object_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            ChangesetObjectTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
