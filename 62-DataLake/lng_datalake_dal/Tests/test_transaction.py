import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.transaction import Transaction

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'Transaction')


class TestTable(unittest.TestCase):

    # +test_commit_transaction: positive test for transaction
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_commit_transaction(self, mock_get_client):
        trans = Transaction()

        for i in range(10):
            trans.add_item_to_transaction_list('item')
        mock_get_client.return_value.transact_write_items.return_value = None
        trans.commit_transaction()

    # -test_add_transaction_terminal_error: too many items
    def test_add_transaction_terminal_error(self):
        trans = Transaction()

        for i in range(10):
            trans.add_item_to_transaction_list('item')

        with self.assertRaisesRegex(Exception, 'Too many items attempted to be added to a transaction::'):
            trans.add_item_to_transaction_list('exception item')

    # -test_commit_transaction_raise_errors: failure to write to dynamo_db
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_commit_transaction_raise_errors(self, mock_get_client):

        mock_get_client.side_effect = [ClientError({"Error": {'Code': "Fake", 'Message': 'Mock Error'}}, 'MOCK'),
                                       Exception("Unknown Error")]
        trans = Transaction()
        with self.assertRaisesRegex(ClientError, "Error"):
            trans.commit_transaction()
        with self.assertRaisesRegex(Exception, "Unknown Error"):
            trans.commit_transaction()


