import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.changeset_collection_table import ChangesetCollectionTable
from lng_datalake_dal.table import Table

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ChangesetCollectionTable')


@patch.dict('os.environ',
            values={'CHANGESET_COLLECTION_DYNAMODB_TABLE': 'DataLakeDynamo-ChangesetCollectionTable'},
            clear=True)
class TestChangesetCollectionTable(unittest.TestCase):
    # +ChangesetCollectionTable.__init__ - can create ChangesetCollectionTable object and load schema
    def test_changeset_collection_table_init(self):
        cct = ChangesetCollectionTable()
        self.assertIsNotNone(cct)
        self.assertEqual(cct.table_name, 'DataLakeDynamo-ChangesetCollectionTable')

    # +ChangesetCollectionTable.put_item
    def test_changeset_collection_table_put_item(self):
        test_key_dict = {"changeset-id": "123",
                         "collection-id": "456"}
        test_attrs_dict = {"objects-processed": 10}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(ChangesetID) and attribute_not_exists(CollectionID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ChangesetCollectionTable super class that we want to test
            cct = ChangesetCollectionTable()
            cct.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -ChangesetCollectionTable.put_item - raises ConditionError
    def test_changeset_collection_table_put_item_fail1(self):
        test_key_dict = {"changeset-id": "123",
                         "collection-id": "456"}
        test_attrs_dict = {"objects-processed": 10}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create ChangesetCollectionTable super class that we want to test
            with self.assertRaisesRegex(ConditionError,
                                        'ChangesetID {} already exists for CollectionID {} in Changeset table'.format(
                                            put_dict["changeset-id"], put_dict["collection-id"])):
                ChangesetCollectionTable().put_item(put_dict)

    # +ChangesetCollectionTable.update_item - success
    def test_changeset_collection_table_update_item_success(self):
        test_key_dict = {"changeset-id": "123",
                         "collection-id": "456"}
        test_attrs_dict = {"objects-processed": 10}
        update_dict = {**test_key_dict, **test_attrs_dict}
        condition = 'attribute_exists(ChangesetID) and attribute_exists(CollectionID)'
        with mock.patch.object(Table, 'put_item') as mock_method:
            self.assertIsNone(ChangesetCollectionTable().update_item(update_dict, condition=condition))

    # - test_changeset_collection_table_update_item_item_fail_exception
    def test_changeset_collection_table_update_item_fail_exception(self):
        test_key_dict = {
            "collection-id": "456"}
        test_attrs_dict = {"objects-processed": 10}
        update_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaisesRegex(Exception,
                                    r'Update must contain a condition with attribute_exists\(ChangesetID\)'):
            ChangesetCollectionTable().update_item(update_dict)

    # +Table.update_counters - calls base class with correct parameters and value
    def test_changeset_collection_table_update_counters(self):
        changestset_id = 'test_chanegeset_id'
        collection_id = 'test_collection_id'
        pending_expiration_epoch = '123'

        dict_key = {'ChangesetID': {'S': changestset_id}, 'CollectionID': {'S': collection_id}}
        counters = {"ObjectsProcessed": 20}
        additional_attr = {"PendingExpirationEpoch": {"N": '123'}}
        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'update_counters') as mock_method:
            # Create ChangesetCollectionTable super class that we want to test
            cct = ChangesetCollectionTable()
            cct.update_counters(changestset_id, collection_id, pending_expiration_epoch, counters, condition=None)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(dict_key, counters, True, None, additional_attr)

    # -ChangesetCollectionTable.update_counter - raises NotImplementedError
    def test_changeset_collection_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ChangesetCollectionTable().update_counter(primary_key)

    # -ChangesetCollectionTable.batch_write_items - raises NotImplementedError
    def test_changeset_collection_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            ChangesetCollectionTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
