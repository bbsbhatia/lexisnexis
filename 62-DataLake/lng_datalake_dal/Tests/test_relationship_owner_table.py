import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.relationship_owner_table import RelationshipOwnerTable
from lng_datalake_dal.table import Table
from lng_datalake_dal.exceptions import ConditionError

__author__ = "Doug Heitkamp"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RelationshipOwnerTable')


@patch.dict('os.environ',
            values={'RELATIONSHIP_OWNER_DYNAMODB_TABLE': 'DataLakeDynamo-RelationshipOwnerTable-1ADVZ3OZKTY6M'},
            clear=True)
class TestRelationshipOwnerTable(unittest.TestCase):
    # +RelationshipOwnerTable.__init__ - can create OwnerTable object and load schema
    def test_relationship_owner_table_init(self):
        table = RelationshipOwnerTable()
        self.assertIsNotNone(table)
        self.assertEqual(table.table_name, 'DataLakeDynamo-RelationshipOwnerTable-1ADVZ3OZKTY6M')

    # -RelationshipOwnerTable.update_counter - raises NotImplementedError
    def test_relationship_owner_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            RelationshipOwnerTable().update_counter(primary_key)

    # -RelationshipOwnerTable.update_counters - raises NotImplementedError
    def test_relationship_owner_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            RelationshipOwnerTable().update_counters(primary_key, {"Counter": 1})

    # -RelationshipOwnerTable.batch_write_items - raises NotImplementedError
    def test_relationship_owner_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            RelationshipOwnerTable().batch_write_items()

    # +RelationshipOwnerTable.put_item
    def test_relationship_owner_table_put_item(self):
        test_key_dict = {'foo': 'bar'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(RelationshipID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create RelationshipOwnerTable super class that we want to test
            table = RelationshipOwnerTable()
            table.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -RelationshipOwnerTable.put_item - raises ConditionError
    def test_relationship_owner_table_put_item_fail(self):
        test_key_dict = {'relationship-id': 'mock'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create RelationshipOwnerTable super class that we want to test
            with self.assertRaises(ConditionError):
                RelationshipOwnerTable().put_item(put_dict)

    # +RelationshipOwnerTable.update_item
    def test_relationship_owner_table_update_item(self):
        test_key_dict = {'foo': 'bar'}
        test_attrs_dict = \
            {
                'doesnotmatter': 'no validation here'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create RelationshipOwnerTable super class that we want to test
            ost = RelationshipOwnerTable()
            ost.update_item(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict, None, None)


if __name__ == '__main__':
    unittest.main()
