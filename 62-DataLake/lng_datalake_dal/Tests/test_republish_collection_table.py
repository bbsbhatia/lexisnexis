import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.republish_collection_table import RepublishCollectionTable
from lng_datalake_dal.table import Table

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RepublishCollectionTable')


@patch.dict('os.environ',
            values={'REPUBLISH_COLLECTION_DYNAMODB_TABLE': 'DataLakeDynamo-RepublishCollectionTable-YNH2CMRGBSCS'},
            clear=True)
class TestRepublishCollectionTable(unittest.TestCase):
    # +RepublishCollectionTable.__init__ - can create RepublishCollectionTable object and load schema
    def test_republish_collection_table_init(self):
        ct = RepublishCollectionTable()
        self.assertIsNotNone(ct)
        self.assertEqual(ct.table_name, 'DataLakeDynamo-RepublishCollectionTable-YNH2CMRGBSCS')

    # +RepublishCollectionTable.update_item - success
    def test_republish_collection_table_update_item_success(self):
        test_key_dict = {
            "republish-id": "123",
            "collection-id": "456"
        }
        test_attrs_dict = \
            {
                "subscription-id": 99,
                "republish-state": "Processing",
                "end-filter-timestamp": "2019-07-03T16:09:33.861Z",
                "start-filter-timestamp": "2019-08-03T16:09:33.861Z",
                "last-processed-page": "xyzl5a4st1"

            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        condition = 'attribute_exists(RepublishID)'
        with mock.patch.object(Table, 'put_item') as mock_method:
            self.assertIsNone(RepublishCollectionTable().update_item(update_dict, condition=condition))

    # - test_republish_collection_table_update_item_item_fail_exception
    def test_republish_collection_update_item_fail_exception(self):
        test_key_dict = {'collection-id': '12345'}
        test_attrs_dict = \
            {
                'description': 'New description'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaisesRegex(Exception,
                                    r'Update must contain a condition with attribute_exists\(RepublishID\)'):
            RepublishCollectionTable().update_item(update_dict)

    # -RepublishCollectionTable.update_counter - raises NotImplementedError
    def test_republish_collection_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            RepublishCollectionTable().update_counter(primary_key)

    # -RepublishCollectionTable.update_counters - raises NotImplementedError
    def test_republish_collection_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            RepublishCollectionTable().update_counters(primary_key, {"Counter": 1})


if __name__ == '__main__':
    unittest.main()
