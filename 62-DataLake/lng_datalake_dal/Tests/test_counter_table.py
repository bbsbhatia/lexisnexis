import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.counter_table import CounterTable
from lng_datalake_dal.table import Table

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CounterTable')


@patch.dict('os.environ',
            values={'COUNTER_DYNAMODB_TABLE': 'DataLakeDynamo-CounterTable-RUUH7FG935KA'},
            clear=True)
class TestCounterTable(unittest.TestCase):
    # +CounterTable.__init__ - can create CounterTable object and load schema
    def test_counter_table_init(self):
        cst = CounterTable()
        self.assertIsNotNone(cst)
        self.assertEqual(cst.table_name, 'DataLakeDynamo-CounterTable-RUUH7FG935KA')

    # -CounterTable.delete_item - raises NotImplementedError
    def test_counter_table_delete_item(self):
        with self.assertRaises(NotImplementedError):
            CounterTable().delete_item('test')

    # -CounterTable.put_item - raises NotImplementedError
    def test_counter_table_put_item(self):
        with self.assertRaises(NotImplementedError):
            CounterTable().put_item('test')

    # -CounterTable.get_item - raises NotImplementedError
    def test_counter_table_get_item(self):
        with self.assertRaises(NotImplementedError):
            CounterTable().get_item('test')

    # -CounterTable.get_all_items - raises NotImplementedError
    def test_counter_table_get_all_items(self):
        with self.assertRaises(NotImplementedError):
            CounterTable().get_all_items()

    # -CounterTable.query_items - raises NotImplementedError
    def test_counter_table_query_items(self):
        with self.assertRaises(NotImplementedError):
            CounterTable().query_items()

    # -CounterTable.update_item - raises NotImplementedError
    def test_counter_table_update_item(self):
        with self.assertRaises(NotImplementedError):
            CounterTable().update_item('test')

    # -CounterTable.update_counters - raises NotImplementedError
    def test_counter_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CounterTable().update_counters(primary_key, {"Counter": 1})

    # +CounterTable.update_counter - calls base class with correct parameters and value
    def test_counter_table_update_counter(self):
        namespace_key = 'test_namespace_key'
        dict_key = {'Namespace': {'S': namespace_key}}
        counter_attribute_name = 'AtomicCounter'
        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'update_counter') as mock_method:
            # Create CatalogTable super class that we want to test
            ct = CounterTable()
            ct.update_counter(namespace_key)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(dict_key, counter_attribute_name)

    # -CounterTable.batch_write_items - raises NotImplementedError
    def test_counter_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            CounterTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
