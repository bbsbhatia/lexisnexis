import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.relationship_commitment_table import RelationshipCommitmentTable
from lng_datalake_dal.table import Table
from lng_datalake_dal.exceptions import ConditionError

__author__ = "Doug Heitkamp"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RelationshipCommitmentTable')


@patch.dict('os.environ',
            values={
                'RELATIONSHIP_COMMITMENT_DYNAMODB_TABLE': 'DataLakeDynamo-RelationshipCommitmentTable-1ADVZ3OZKTY6M'},
            clear=True)
class TestRelationshipCommitmentTable(unittest.TestCase):
    # +RelationshipCommitmentTable.__init__ - can create OwnerTable object and load schema
    def test_relationship_commitment_table_init(self):
        table = RelationshipCommitmentTable()
        self.assertIsNotNone(table)
        self.assertEqual(table.table_name, 'DataLakeDynamo-RelationshipCommitmentTable-1ADVZ3OZKTY6M')

    # -RelationshipCommitmentTable.update_counter - raises NotImplementedError
    def test_relationship_commitment_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            RelationshipCommitmentTable().update_counter(primary_key)

    # -RelationshipCommitmentTable.update_counters - raises NotImplementedError
    def test_relationship_commitment_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            RelationshipCommitmentTable().update_counters(primary_key, {"Counter": 1})

    # -RelationshipCommitmentTable.get_all_items - raises NotImplementedError
    def test_relationship_commitment_table_get_all_items(self):
        with self.assertRaises(NotImplementedError):
            RelationshipCommitmentTable().get_all_items()

    # +RelationshipCommitmentTable.put_item
    def test_relationship_commitment_table_put_item(self):
        test_key_dict = {'foo': 'bar'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(RelationshipID) and attribute_not_exists(TargetCompositeKey)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create RelationshipCommitmentTable super class that we want to test
            table = RelationshipCommitmentTable()
            table.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -RelationshipCommitmentTable.put_item - raises ConditionError
    def test_relationship_commitment_table_put_item_fail(self):
        test_key_dict = {'relationship-id': 'mock', 'target-composite-key': 'mock|key'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create RelationshipCommitmentTable super class that we want to test
            with self.assertRaises(ConditionError):
                RelationshipCommitmentTable().put_item(put_dict)

    # -RelationshipCommitmentTable.put_item - raises ConditionError
    def test_relationship_commitment_table_put_item_fail_message(self):
        test_key_dict = {'relationship-id': 'mock', 'target-composite-key': 'mock|key'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create RelationshipCommitmentTable super class that we want to test
            with self.assertRaisesRegex(ConditionError,
                                        "RelationshipID mock with TargetCompositeKey mock|key "
                                        "already exists in Relationship Commitment table"):
                RelationshipCommitmentTable().put_item(put_dict)

    # +RelationshipCommitmentTable.update_item
    def test_relationship_commitment_table_update_item(self):
        test_key_dict = {'foo': 'bar'}
        test_attrs_dict = \
            {
                'doesnotmatter': 'no validation here'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create RelationshipCommitmentTable super class that we want to test
            ost = RelationshipCommitmentTable()
            ost.update_item(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict, None, None)

    # +RelationshipCommitmentTable.generate_target_composite_key - produces a value
    def test_generate_target_composite_key(self):
        self.assertIsNotNone(RelationshipCommitmentTable.generate_target_composite_key('foo', 'bar'))


if __name__ == '__main__':
    unittest.main()
