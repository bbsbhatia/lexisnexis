import unittest
from decimal import Decimal
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal import dynamo_mapper

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'dynamo_mapper')

dynamo_dict_write = {'String': {'S': 'strValue'},
                     'List': {'L': [{'S': 'ListVal0'}, {'S': 'ListVal1'}]},
                     'Dict': {'M': {'dictKey': {'S': 'dictVal'}}},
                     'Set': {'SS': ['1']},
                     'Dec': {'N': '3.14'},
                     'Int': {'N': '7'},
                     'dl:LastUpdated': {'S': '2019-11-05T19:21:00.123Z'}}

dynamo_dict = {'String': {'S': 'strValue'},
               'List': {'L': [{'S': 'ListVal0'}, {'S': 'ListVal1'}]},
               'Dict': {'M': {'dictKey': {'S': 'dictVal'}}},
               'Set': {'SS': ['1']},
               'Dec': {'N': '3.14'},
               'Int': {'N': '7'}}


class TestDynamoMapper(unittest.TestCase):

    # +test_transform_to_dynamo_dict_default() - can transform from client to dynamo default read_only
    def test_transform_to_dynamo_dict_default(self):
        client_dict = {'string': 'strValue',
                       'list': ['ListVal0', 'ListVal1'],
                       'dict': {'dictKey': 'dictVal'},
                       'set': set('1'),
                       'dec': Decimal('3.14'),
                       'int': 7}
        out_dynamo_dict = dynamo_mapper.transform_to_dynamo_dict(client_dict)
        self.assertDictEqual(out_dynamo_dict, dynamo_dict)

    # +test_transform_to_dynamo_dict_read_only() - can transform from client to dynamo read_only=True
    def test_transform_to_dynamo_dict_read_only(self, read_only=True):
        client_dict = {'string': 'strValue',
                       'list': ['ListVal0', 'ListVal1'],
                       'dict': {'dictKey': 'dictVal'},
                       'set': set('1'),
                       'dec': Decimal('3.14'),
                       'int': 7}
        out_dynamo_dict = dynamo_mapper.transform_to_dynamo_dict(client_dict)
        self.assertDictEqual(out_dynamo_dict, dynamo_dict)

    # +test_transform_to_dynamo_dict_write() - can transform from client to dynamo read_only=False
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    def test_transform_to_dynamo_dict_write(self, mock_timestamp):
        mock_timestamp.return_value = '2019-11-05T19:21:00.123Z'
        client_dict = {'string': 'strValue',
                       'list': ['ListVal0', 'ListVal1'],
                       'dict': {'dictKey': 'dictVal'},
                       'set': set('1'),
                       'dec': Decimal('3.14'),
                       'int': 7}
        out_dynamo_dict = dynamo_mapper.transform_to_dynamo_dict(client_dict, read_only=False)
        self.assertDictEqual(out_dynamo_dict, dynamo_dict_write)

    # +transform_to_client_dict() - can transform from dynamo to client
    def test_transform_to_client_dict(self):
        client_dict = {'string': 'strValue',
                       'list': ['ListVal0', 'ListVal1'],
                       'dict': {'dictKey': 'dictVal'},
                       'set': ['1'],
                       'dec': 3.14,
                       'int': 7}
        out_client_dict = dynamo_mapper.transform_to_client_dict(dynamo_dict)
        self.assertDictEqual(out_client_dict, client_dict)

    # +transform_to_client_dict() - doesn't return the replication data
    def test_transform_to_client_dict_no_rep(self):
        input_dict = {'aws:rep:deleting': {'BOOL': False}, 'aws:rep:updateregion': {'S': 'us-east-1'},
                      'AssetName': {'S': 'ODPP'}, 'aws:rep:updatetime': {'N': '1535405368.741001'},
                      'AssetID': {'N': '251'}}
        out_client_dict = dynamo_mapper.transform_to_client_dict(input_dict)
        self.assertDictEqual(out_client_dict, {'asset-id': 251, 'asset-name': 'ODPP'})

    # +transform_to_client_dict() - doesn't return any attributes prefixed by "dl:"
    def test_transform_to_client_dict_no_dl(self):
        input_dict = {'dl:MockAttribute': {'S': "AttrValue"},
                      'AssetName': {'S': 'ODPP'},
                      'dl:LastUpdated': {'S': '2019-11-05T19:21:00.123Z'},
                      'dl:FooBar': {'N': '867'},
                      'AssetID': {'N': '251'}}
        out_client_dict = dynamo_mapper.transform_to_client_dict(input_dict)
        self.assertDictEqual(out_client_dict, {'asset-id': 251, 'asset-name': 'ODPP'})


if __name__ == '__main__':
    unittest.main()
