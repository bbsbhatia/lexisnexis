import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.republish_table import RepublishTable
from lng_datalake_dal.table import Table

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RepublishTable')


@patch.dict('os.environ',
            values={'REPUBLISH_DYNAMODB_TABLE': 'DataLakeDynamo-RepublishTable-YNH2CMRGBSCS'},
            clear=True)
class TestRepublishTable(unittest.TestCase):
    # +RepublishTable.__init__ - can create RepublishTable object and load schema
    def test_republish_table_init(self):
        ct = RepublishTable()
        self.assertIsNotNone(ct)
        self.assertEqual(ct.table_name, 'DataLakeDynamo-RepublishTable-YNH2CMRGBSCS')

    # +RepublishTable.put_item
    def test_republish_table_put_item(self):
        test_key_dict = {"republish-id": "123",
                         "request-time": "2019-06-03T16:09:33.861Z"}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(RepublishID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = RepublishTable()
            ost.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -RepublishTable.put_item - raises ConditionError
    def test_republish_table_put_item_fail1(self):
        test_key_dict = {"republish-id": "123",
                         "request-time": "2019-06-03T16:09:33.861Z"}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create RepublishTable super class that we want to test
            with self.assertRaisesRegex(ConditionError, 'already exists in Republish table'):
                RepublishTable().put_item(put_dict)

    # +RepublishTable.update_item - success
    def test_republish_table_update_item_success(self):
        test_key_dict = {'republish-id': '12345'}
        test_attrs_dict = \
            {
                "request-time": "2019-06-03T16:09:33.861Z",
                "subscription-id": 99,
                "republish-state": "Processing",
                "republish-timestamp": "2019-06-03T16:09:33.861Z",
                "start-timestamp": "2019-07-03T16:09:33.861Z",
                "end-timestamp": "2019-08-03T16:09:33.861Z"

            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        condition = 'attribute_exists(RepublishID)'
        with mock.patch.object(Table, 'put_item') as mock_method:
            self.assertIsNone(RepublishTable().update_item(update_dict, condition=condition))

    # - test_republish_table_update_item_item_fail_exception
    def test_republish_table_update_item_fail_exception(self):
        test_key_dict = {'republish-id': '12345'}
        test_attrs_dict = \
            {
                'description': 'New description'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaisesRegex(Exception,
                                    r'Update must contain a condition with attribute_exists\(RepublishID\)'):
            RepublishTable().update_item(update_dict)

    # -RepublishnTable.update_counter - raises NotImplementedError
    def test_republish_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            RepublishTable().update_counter(primary_key)

    # -RepublishTable.update_counters - raises NotImplementedError
    def test_republish_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            RepublishTable().update_counters(primary_key, {"Counter": 1})

    # -RepublishTable.batch_write_items - raises NotImplementedError
    def test_republish_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            RepublishTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
