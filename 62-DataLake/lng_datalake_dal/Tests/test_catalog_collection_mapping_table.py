import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.catalog_collection_mapping_table import CatalogCollectionMappingTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table

__author__ = "Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CatalogCollectionMappingTable')


@patch.dict('os.environ',
            values={'CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE': 'feature-DataLake-CatalogCollectionMappingTable'},
            clear=True)
class TestCatalogCollectionMappingTable(unittest.TestCase):
    # +CatalogCollectionMappingTable.__init__ - can create catalogCollectionMapping table object and load schema
    def test_catalog_table_init(self):
        mt = CatalogCollectionMappingTable()
        self.assertIsNotNone(mt)
        self.assertEqual(mt.table_name, 'feature-DataLake-CatalogCollectionMappingTable')

    # +CatalogCollectionMappingTable.put_item
    def test_mapping_table_put_item(self):
        test_key_dict = {'catalog-id': 'catalog-01',
                         'collection-id': 'collection-01'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(CatalogID) AND attribute_not_exists(CollectionID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create CatalogCollectionMappingTable super class that we want to test
            mt = CatalogCollectionMappingTable()
            mt.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -CatalogCollectionMappingTable.put_item - raises ConditionError
    def test_mapping_table_put_item_fail1(self):
        test_key_dict = {'catalog-id': 'catalog-01',
                         'collection-id': 'collection-01'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create CatalogCollectionMappingTable super class that we want to test
            with self.assertRaises(ConditionError):
                CatalogCollectionMappingTable().put_item(put_dict)

    # -CatalogCollectionMappingTable.get_all_items - raises NotImplementedError
    def test_mapping_table_get_all_items(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CatalogCollectionMappingTable().get_all_items(primary_key)


    # -CatalogCollectionMappingTable.update_counters - raises NotImplementedError
    def test_mapping_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CatalogCollectionMappingTable().update_counters(primary_key, {"Counter": 1})

    # -CatalogCollectionMappingTable.update_counter - raises NotImplementedError
    def test_mapping_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CatalogCollectionMappingTable().update_counter(primary_key)


if __name__ == '__main__':
    unittest.main()
