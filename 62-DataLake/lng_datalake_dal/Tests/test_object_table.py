import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.object_table import ObjectTable
from lng_datalake_dal.table import Table

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ObjectTable')


@patch.dict('os.environ',
            values={'OBJECT_TABLE': 'feature-DataLake-ObjectTable1'},
            clear=True)
class TestObjectTable(unittest.TestCase):
    # +ObjectTable.__init__ - can create ObjectTable object and load schema
    def test_object_table_init(self):
        ost = ObjectTable()
        self.assertIsNotNone(ost)
        self.assertEqual(ost.table_name, 'feature-DataLake-ObjectTable1')

    # +ObjectTable.generate_composite_key
    def test_object_table_generate_composite_key(self):
        self.assertEqual(ObjectTable.generate_composite_key('ObjId', 'CollectionId'), 'ObjId|CollectionId')

    # +ObjectTable.put_item
    def test_object_table_put_item(self):
        test_key_dict = {'composite-key': 'abc|d', 'version-number': 2}
        test_attrs_dict = {'object-id': 'abc', 'collection-id': 'd'}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = '(attribute_not_exists(CompositeKey) and attribute_not_exists(VersionNumber))'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = ObjectTable()
            ost.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -ObjectTable.put_item - raises ConditionError
    def test_object_table_put_item_fail1(self):
        test_key_dict = {'composite-key': 'abc|d', 'version-number': 2}
        test_attrs_dict = {'object-id': 'abc', 'collection-id': 'd'}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create ObjectStoreTable super class that we want to test
            with self.assertRaises(ConditionError):
                ObjectTable().put_item(put_dict)

    # +ObjectTable.delete_latest_version
    def test_object_table_delete_latest_version(self):
        test_key_dict = {'composite-key': 'abc|d', 'version-number': 2}
        test_attrs_dict = \
            {
                'object-id': 'abc',
                'collection-id': 'd',
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created",
                'version-timestamp': '2018-02-20T10:53:56.605653'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = ObjectTable()
            ost.delete_latest_version_attribute(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict,
                                           condition='VersionTimestamp = :v',
                                           expression_values={':v': update_dict['version-timestamp']})

    # +ObjectTable.delete_latest_version - with latest-version in input dict
    def test_object_table_delete_latest_version_2(self):
        test_key_dict = {'composite-key': 'abc|d', 'version-number': 2}
        test_attrs_dict = \
            {
                'object-id': 'abc',
                'collection-id': 'd',
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created",
                'latest-version': 'Y',
                'version-timestamp': '2018-02-20T10:53:56.605653'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = ObjectTable()
            ost.delete_latest_version_attribute(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict,
                                           condition='VersionTimestamp = :v',
                                           expression_values={':v': update_dict['version-timestamp']})

    # -ObjectTable.delete_latest_version - raises ConditionError
    def test_object_table_delete_latest_version_fail_1(self):
        test_key_dict = {'composite-key': 'abc|d', 'version-number': 2}
        test_attrs_dict = \
            {
                'object-id': 'abc',
                'collection-id': 'd',
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created",
                'version-timestamp': '2018-02-20T10:53:56.605653'
            }
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.update_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create ObjectStoreTable super class that we want to test
            with self.assertRaises(ConditionError):
                ObjectTable().delete_latest_version_attribute(put_dict)

    # -ObjectTable.delete_latest_version - raises KeyError: missing version-timestamp
    def test_object_table_delete_latest_version_fail_2(self):
        test_key_dict = {'composite-key': 'abc|d', 'version-number': 2}
        test_attrs_dict = \
            {
                'object-id': 'abc',
                'collection-id': 'd',
                'bucket-name': 'aaron',
                'object-key': 'key1',
                'object-state': "Created"
            }
        put_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaisesRegex(KeyError, "Missing required attribute: 'version-timestamp'"):
            ObjectTable().delete_latest_version_attribute(put_dict)

    # -ObjectTable.update_item - raises NotImplementedError
    def test_object_table_update_item(self):
        with self.assertRaises(NotImplementedError):
            ObjectTable().update_item()

    # +ObjectTable.update_counter - raises NotImplementedError
    def test_object_table_update_counter(self):
        with self.assertRaises(NotImplementedError):
            ObjectTable().update_counter('Test')

    # -ObjectTable.update_counters - raises NotImplementedError
    def test_object_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ObjectTable().update_counters(primary_key, {"Counter": 1})

    # +ObjectTable.batch_write_items - raises NotImplementedError
    def test_object_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            ObjectTable().batch_write_items()

    # +ObjectTable.put_transaction_item - raises NotImplementedError
    def test_object_table_put_transaction_item(self):
        with self.assertRaises(NotImplementedError):
            ObjectTable().put_transaction_item()

    # +ObjectTable.update_transaction_item - raises NotImplementedError
    def test_object_table_update_transaction_item(self):
        with self.assertRaises(NotImplementedError):
            ObjectTable().update_transaction_item()

    # +ObjectTable.delete_transaction_item - raises NotImplementedError
    def test_object_table_delete_transaction_item(self):
        with self.assertRaises(NotImplementedError):
            ObjectTable().delete_transaction_item()


if __name__ == '__main__':
    unittest.main()
