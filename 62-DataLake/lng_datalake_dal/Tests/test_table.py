import os
import unittest
from datetime import datetime
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_testhelper.io_utils import IOUtils

import lng_datalake_dal.exceptions
from lng_datalake_dal import asset_table, owner_table
from lng_datalake_dal.table import Table, TableCache
from lng_datalake_dal.transaction import Transaction

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'Table')


class TestTable(unittest.TestCase):

    # + TableCache.__call__ - Verify we cache the appropriate data
    def test_table_cache_call(self):
        first_asset_instance = asset_table.AssetTable()
        first_owner_instance = owner_table.OwnerTable()
        second_asset_instance = asset_table.AssetTable()
        second_owner_instance = owner_table.OwnerTable()
        self.assertEqual(first_asset_instance, second_asset_instance)
        self.assertEqual(first_owner_instance, second_owner_instance)
        self.assertNotEqual(first_asset_instance, first_owner_instance)

    # + TableCache.clear - Verify we clear cache the appropriate data
    def test_table_cache_clear(self):
        first_asset_instance = asset_table.AssetTable()
        first_owner_instance = owner_table.OwnerTable()

        TableCache.clear()

        second_asset_instance = asset_table.AssetTable()
        second_owner_instance = owner_table.OwnerTable()
        self.assertNotEqual(first_asset_instance, second_asset_instance)
        self.assertNotEqual(first_owner_instance, second_owner_instance)
        self.assertNotEqual(first_asset_instance, first_owner_instance)

    # +table.__init__ - verify can create object, load a schema, query required and optional keys
    def test_table_init(self):
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        required_keys = ['asset-id', "classification-type", 'collection-id', 'collection-state',
                         'collection-timestamp', 'event-ids',
                         'old-object-versions-to-keep', 'owner-id']
        optional_keys = ['catalog-ids', 'collection-hash', 'compression-algorithm', 'description', 'high-availability',
                         'object-expiration']
        self.assertIsNotNone(ct)
        self.assertEqual(sorted(ct.get_required_schema_keys()), required_keys)
        self.assertListEqual(sorted(ct.get_optional_schema_keys()), optional_keys)

    # Put Item ********************************************

    # +Table.put_item
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_table_put_item(self, datetime_mock, db_get_client_mock):
        db_get_client_mock.return_value.put_item.return_value = None
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123",
                         "collection-timestamp": "2018-01-26T15:08:34.053Z"}
        test_attrs_dict = io_util.load_data_json("valid_test_attr.json")
        test_dict = {**test_key_dict, **test_attrs_dict}
        ct.put_item(test_dict)
        dynamo_put_item_item = io_util.load_data_json("valid_dynamo_put_item.json")
        db_get_client_mock.return_value.put_item.assert_called_once_with(Item=dynamo_put_item_item,
                                                                         TableName='CollectionTable')

    # +Table.put_item - with condition
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_table_put_item_2(self, datetime_mock, db_get_client_mock):
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        db_get_client_mock.return_value.put_item.return_value = None
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123",
                         "collection-timestamp": "2018-01-26T15:08:34.053Z"}
        test_attrs_dict = io_util.load_data_json("valid_test_attr.json")
        test_dict = {**test_key_dict, **test_attrs_dict}
        test_cond = 'attribute_not_exists(TestAttribute)'
        ct.put_item(test_dict, condition=test_cond)
        dynamo_put_item_item = io_util.load_data_json("valid_dynamo_put_item.json")
        db_get_client_mock.return_value.put_item.assert_called_once_with(
            ConditionExpression=test_cond, Item=dynamo_put_item_item,
            TableName='CollectionTable')

    # +Table.put_item - with expression values for a condition
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_table_put_item_condition_expression(self, datetime_mock, db_get_client_mock):
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        db_get_client_mock.return_value.put_item.return_value = None
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123",
                         "collection-timestamp": "2018-01-26T15:08:34.053Z"}
        test_attrs_dict = io_util.load_data_json("valid_test_attr.json")
        test_dict = {**test_key_dict, **test_attrs_dict}
        test_cond = "CollectionTimestamp = :timestamp"
        test_exp_value = {":timestamp": test_key_dict["collection-timestamp"]}
        ct.put_item(test_dict, condition=test_cond, expression_values=test_exp_value)
        dynamo_put_item_item = io_util.load_data_json("valid_dynamo_put_item.json")
        db_get_client_mock.return_value.put_item.assert_called_once_with(
            ConditionExpression=test_cond, Item=dynamo_put_item_item,
            TableName='CollectionTable', ExpressionAttributeValues={":timestamp": {"S": "2018-01-26T15:08:34.053Z"}})

    # +Table.put_item - with kwargs
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_table_put_item_3(self, datetime_mock, db_get_client_mock):
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        db_get_client_mock.return_value.put_item.return_value = None
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123",
                         "collection-timestamp": "2018-01-26T15:08:34.053Z"}
        test_attrs_dict = io_util.load_data_json("valid_test_attr.json")
        test_dict = {**test_key_dict, **test_attrs_dict}
        test_cond = 'attribute_not_exists(TestAttribute)'
        ct.put_item(test_dict, condition=test_cond, ReturnValues="None")
        dynamo_put_item_item = io_util.load_data_json("valid_dynamo_put_item.json")
        db_get_client_mock.return_value.put_item.assert_called_once_with(
            ConditionExpression=test_cond, Item=dynamo_put_item_item,
            TableName='CollectionTable', ReturnValues="None")

    # +Table.put_transaction_item
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_table_put_transaction_item(self, datetime_mock, mock_get_client):
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        transaction = Transaction()
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_attrs_dict = {
            'bucket-name': 'aaron',
            'object-key': 'key1',
            'object-state': 'Created',
            'content-type': 'text/plain',
            'raw-content-length': 10,
            'version-number': 1,
            'version-timestamp': '10101',
            'collection-hash': 'da4b9237bacccdf19c0760cab7aec4a8359010b0',
            'event-ids': ["test-event-id"]
        }
        test_dict = {**test_key_dict, **test_attrs_dict}
        ost.put_transaction_item(test_dict, transaction)
        self.assertNotIn('dl:last-updated', test_dict)
        expected_transaction_item = io_util.load_data_json("valid_put_transaction_item.json")
        transaction.commit_transaction()
        mock_get_client.return_value.transact_write_items.assert_called_with(TransactItems=[expected_transaction_item])

    # +Table.put_transaction_item with condition
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_table_put_transaction_item_with_condition(self, datetime_mock, mock_get_client):
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        transaction = Transaction()
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_attrs_dict = {
            'bucket-name': 'aaron',
            'object-key': 'key1',
            'object-state': 'Created',
            'content-type': 'text/plain',
            'raw-content-length': 10,
            'version-number': 1,
            'version-timestamp': '10101',
            'collection-hash': 'da4b9237bacccdf19c0760cab7aec4a8359010b0',
            'event-ids': ['test-event-id']
        }
        test_cond = 'attribute_exists(TestAttribute)'
        test_dict = {**test_key_dict, **test_attrs_dict}
        ost.put_transaction_item(test_dict, transaction, test_cond)
        expected_transaction_item = io_util.load_data_json("valid_put_transaction_item_with_condition.json")
        transaction.commit_transaction()
        mock_get_client.return_value.transact_write_items.assert_called_with(TransactItems=[expected_transaction_item])

    # +Table.put_transaction_item with condition and expression value
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_table_put_transaction_item_with_condition_expression_value(self, datetime_mock, mock_get_client):
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        transaction = Transaction()
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_attrs_dict = {
            'bucket-name': 'aaron',
            'object-key': 'key1',
            'object-state': 'Created',
            'content-type': 'text/plain',
            'raw-content-length': 10,
            'version-number': 1,
            'version-timestamp': '10101',
            'collection-hash': 'da4b9237bacccdf19c0760cab7aec4a8359010b0',
            'event-ids': ["test-event-id"]
        }
        test_cond = 'TestAttribute=:testAttribute'
        test_expression_value = {':testAttribute': {'S': "TestValue"}}
        test_dict = {**test_key_dict, **test_attrs_dict}
        ost.put_transaction_item(test_dict, transaction, test_cond, test_expression_value)
        expected_transaction_item = io_util.load_data_json("valid_put_transaction_item_with_condition_value.json")
        transaction.commit_transaction()
        mock_get_client.return_value.transact_write_items.assert_called_with(TransactItems=[expected_transaction_item])

    # -Table.put_item - missing attribute
    def test_table_put_item_fail(self):
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        test_attrs_dict = io_util.load_data_json("valid_test_attr.json")
        test_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaises(lng_datalake_dal.exceptions.SchemaValidationError):
            ct.put_item(test_dict)

    # -Table.put_item - client error
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_put_item_fail_2(self, db_get_client_mock):
        db_get_client_mock.return_value.put_item.side_effect = \
            ClientError({'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
                         'Error': {
                             'Code': 'ProvisionedThroughputExceededException',
                             'Message': 'This is a mock'}},
                        "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123",
                         "collection-timestamp": "2018-01-26T15:08:34.053Z"}
        test_attrs_dict = io_util.load_data_json("valid_test_attr.json")
        test_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaises(ClientError):
            ct.put_item(test_dict)

    # -Table.put_item - client ConditionalCheckFailedException error
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_put_item_fail_3(self, db_get_client_mock):
        db_get_client_mock.return_value.put_item.side_effect = \
            ClientError({'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
                         'Error': {
                             'Code': 'ConditionalCheckFailedException',
                             'Message': 'This is a mock'}},
                        "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123",
                         "collection-timestamp": "2018-01-26T15:08:34.053Z"}
        test_attrs_dict = io_util.load_data_json("valid_test_attr.json")
        test_dict = {**test_key_dict, **test_attrs_dict}
        with self.assertRaises(lng_datalake_dal.exceptions.ConditionError):
            ct.put_item(test_dict)

            # +Table.put_transaction_item_exception

    def test_table_put_transaction_item_exception(self):
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        transaction = Transaction()
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_attrs_dict = {
            'bucket-name': 'aaron',
            'object-key': 'key1',
            'object-state': 'Created',
            'content-type': 'text/plain',
            'raw-content-length': 10,
            'version-number': 1,
            'version-timestamp': '10101',
            'collection-hash': 'da4b9237bacccdf19c0760cab7aec4a8359010b0',
            "event-ids": ["test_event_id", "test_event_id_2"]
        }
        test_dict = {**test_key_dict, **test_attrs_dict}
        for i in range(10):
            transaction.add_item_to_transaction_list(i)

        with self.assertRaisesRegex(Exception, 'Too many items attempted to be added to a transaction::'):
            ost.put_transaction_item(test_dict, transaction)

    # +Table.put_transaction_item_with_condition_exception
    def test_table_put_transaction_item_with_condition_exception(self):
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        transaction = Transaction()
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_attrs_dict = {
            'bucket-name': 'aaron',
            'object-key': 'key1',
            'object-state': 'Created',
            'content-type': 'text/plain',
            'raw-content-length': 10,
            'version-number': 1,
            'version-timestamp': '10101',
            'collection-hash': 'da4b9237bacccdf19c0760cab7aec4a8359010b0',
            "event-ids": ["test_event_id", "test_event_id_2"]
        }
        test_cond = 'attribute_exists(TestAttribute)'
        test_dict = {**test_key_dict, **test_attrs_dict}
        for i in range(10):
            transaction.add_item_to_transaction_list(i)
        with self.assertRaisesRegex(Exception, 'Too many items attempted to be added to a transaction::'):
            ost.put_transaction_item(test_dict, transaction, test_cond)

    # +Table.put_transaction_item_with_condition_expression_value_exception
    def test_table_put_transaction_item_with_condition_value_exception(self):
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        transaction = Transaction()
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_attrs_dict = {
            'bucket-name': 'aaron',
            'object-key': 'key1',
            'object-state': 'Created',
            'content-type': 'text/plain',
            'raw-content-length': 10,
            'version-number': 1,
            'version-timestamp': '10101',
            'collection-hash': 'da4b9237bacccdf19c0760cab7aec4a8359010b0',
            "event-ids": ["test_event_id", "test_event_id_2"]
        }
        test_cond = 'TestAttribute=:testAttribute'
        test_expression_value = {':testAttribute': {'S': "TestValue"}}
        test_dict = {**test_key_dict, **test_attrs_dict}
        for i in range(10):
            transaction.add_item_to_transaction_list(i)
        with self.assertRaisesRegex(Exception, 'Too many items attempted to be added to a transaction::'):
            ost.put_transaction_item(test_dict, transaction, test_cond, test_expression_value)

    # Delete Item ********************************************

    # +Table.delete_item
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_delete_item(self, db_get_client_mock):
        db_get_client_mock.return_value.delete_item.return_value = None

        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        ct.delete_item(test_key_dict)
        db_get_client_mock.return_value.delete_item.assert_called_once_with(Key={'CollectionID': {'S': '274123'}},
                                                                            TableName='CollectionTable')

    # +Table.delete_item - with condition
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_delete_item_1(self, db_get_client_mock):
        db_get_client_mock.return_value.delete_item.return_value = None
        test_cond = 'attribute_exists(TestAttribute)'
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        ct.delete_item(test_key_dict, condition=test_cond)
        db_get_client_mock.return_value.delete_item.assert_called_once_with(ConditionExpression=test_cond,
                                                                            Key={'CollectionID': {'S': '274123'}},
                                                                            TableName='CollectionTable')

    # -Table.delete_item - client error
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_delete_item_fail_2(self, db_get_client_mock):
        db_get_client_mock.return_value.delete_item.side_effect = \
            ClientError({'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
                         'Error': {
                             'Code': 'ProvisionedThroughputExceededException',
                             'Message': 'This is a mock'}},
                        "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        with self.assertRaises(ClientError):
            ct.delete_item(test_key_dict)

    # -Table.delete_item - ConditionError
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_delete_item_fail_3(self, db_get_client_mock):
        db_get_client_mock.return_value.delete_item.side_effect = \
            ClientError({'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
                         'Error': {
                             'Code': 'ConditionalCheckFailedException',
                             'Message': 'This is a mock'}},
                        "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        with self.assertRaises(lng_datalake_dal.exceptions.ConditionError):
            ct.delete_item(test_key_dict)

    @patch('lng_aws_clients.dynamodb.get_client')
    # +Table.delete_transaction_item
    def test_table_delete_transaction_item(self, mock_get_client):
        transaction = Transaction()
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        ost.delete_transaction_item(test_key_dict, transaction)
        expected_transaction_item = io_util.load_data_json("valid_delete_transaction_item.json")
        transaction.commit_transaction()
        mock_get_client.return_value.transact_write_items.assert_called_with(TransactItems=[expected_transaction_item])

    # +Table.delete_transaction_item_with_condition
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_delete_transaction_item_with_condition(self, mock_get_client):
        transaction = Transaction()
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_cond = 'attribute_exists(TestAttribute)'
        ost.delete_transaction_item(test_key_dict, transaction, condition=test_cond)
        expected_transaction_item = io_util.load_data_json("valid_delete_transaction_item_with_condition.json")
        transaction.commit_transaction()
        mock_get_client.return_value.transact_write_items.assert_called_with(TransactItems=[expected_transaction_item])

    # +Table.delete_transaction_item_with_condition and expression value
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_delete_transaction_item_with_condition_expression_value(self, mock_get_client):
        transaction = Transaction()
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_cond = 'TestAttribute=:testAttribute'
        test_expression_value = {':testAttribute': {'S': "TestValue"}}
        ost.delete_transaction_item(test_key_dict, transaction, condition=test_cond,
                                    expression_values=test_expression_value)
        expected_transaction_item = io_util.load_data_json("valid_delete_transaction_item_with_condition_value.json")
        transaction.commit_transaction()
        mock_get_client.return_value.transact_write_items.assert_called_with(TransactItems=[expected_transaction_item])

    # +Table.delete_transaction_item_exception
    def test_table_delete_transaction_item_exception(self):
        transaction = Transaction()
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        for i in range(10):
            transaction.add_item_to_transaction_list(i)

        with self.assertRaisesRegex(Exception, 'Too many items attempted to be added to a transaction::'):
            ost.delete_transaction_item(test_key_dict, transaction)

    # +Table.delete_transaction_item_with_condition_exception
    def test_table_delete_transaction_item_with_condition_exception(self):
        transaction = Transaction()
        ost = Table('ObjectStoreTable', 'ObjectStoreInterfaceSchema.json')
        test_key_dict = {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
        test_cond = 'attribute_exists(TestAttribute)'
        for i in range(10):
            transaction.add_item_to_transaction_list(i)

        with self.assertRaisesRegex(Exception, 'Too many items attempted to be added to a transaction::'):
            ost.delete_transaction_item(test_key_dict, transaction, condition=test_cond)

    # Get Item ********************************************

    # +Table.get_item
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_get_item(self, db_get_client_mock):
        db_get_item_response = io_util.load_data_json("valid_get_item_dynamo_response.json")
        db_get_client_mock.return_value.get_item.return_value = db_get_item_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        item = ct.get_item(test_key_dict)
        get_item_response = io_util.load_data_json("valid_get_item_response.json")
        self.assertDictEqual(item, get_item_response)
        db_get_client_mock.return_value.get_item.assert_called_once_with(Key={'CollectionID': {'S': '274123'}},
                                                                         TableName='CollectionTable')

    # -Table.get_item - Key error causes empty dictionary to be returned
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_get_item_fail_1(self, db_get_client_mock):
        # "Item" is missing causing get_item to return empty dictionary
        db_get_client_mock.return_value.get_item.return_value = {"Foo": "Bar"}
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        # Empty dictionary evaluates to false
        self.assertFalse(ct.get_item(test_key_dict))

    # -Table.get_item - client error
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_get_item_fail_2(self, db_get_client_mock):
        db_get_client_mock.return_value.get_item.side_effect = \
            ClientError({'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
                         'Error': {
                             'Code': 'ProvisionedThroughputExceededException',
                             'Message': 'This is a mock'}},
                        "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        with self.assertRaises(ClientError):
            ct.get_item(test_key_dict)

    # Batch Get Item ********************************************

    # +Table.batch_get_item
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_batch_get_item(self, db_batch_get_client_mock):
        db_batch_get_client_mock.return_value.batch_get_item.side_effect = [
            io_util.load_data_json("valid_batch_get_item_dynamo_response_1.json"),
            io_util.load_data_json("valid_batch_get_item_dynamo_response_2.json")
        ]
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key = "collection-id"
        test_values = ['123', '456', '101', '102']
        items = ct.batch_get_items(test_key, test_values)
        expected_items = io_util.load_data_json("valid_batch_get_item_response.json")
        self.assertListEqual(items, expected_items)

    # +Table.batch_get_item - exponential backoff and retry due to ProvisionedThroughputExceededException
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_batch_get_item_throughput_exception(self, db_batch_get_client_mock):
        db_batch_get_client_mock.return_value.batch_get_item.side_effect = [
            ClientError({'Error': {
                'Code': 'ProvisionedThroughputExceededException',
                'Message': 'This is a mock'}},
                "FAKE"),
            io_util.load_data_json("valid_batch_get_item_dynamo_response_2.json")
        ]
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key = "collection-id"
        test_values = ['101', '102']
        items = ct.batch_get_items(test_key, test_values)
        expected_items = io_util.load_data_json("valid_batch_get_item_response.json")[2:]
        self.assertListEqual(items, expected_items)

    # -Table.batch_get_item - ClientError
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_batch_get_item_client_error(self, db_batch_get_client_mock):
        db_batch_get_client_mock.return_value.batch_get_item.side_effect = ClientError(
            {'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
             'Error': {
                 'Code': 'ValidationException',
                 'Message': 'This is a mock'}},
            "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key = "collection-id"
        test_values = ['101', '102']
        with self.assertRaisesRegex(ClientError, "This is a mock"):
            ct.batch_get_items(test_key, test_values)

    # -Table.batch_get_item - General Exception
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_batch_get_item_general_exception(self, db_batch_get_client_mock):
        db_batch_get_client_mock.return_value.batch_get_item.side_effect = Exception
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key = "collection-id"
        test_values = ['101', '102']
        with self.assertRaises(Exception):
            ct.batch_get_items(test_key, test_values)

    # -Table.batch_get_item - ClientError - max_items_per_batch > 100
    def test_table_batch_get_item_client_error_2(self):
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key = "collection-id"
        test_values = ['101', '102']
        with self.assertRaises(ClientError):
            ct.batch_get_items(test_key, test_values, max_items_per_batch=110)

    # -Table.batch_get_item - max_retries_exception
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_batch_get_item_max_retries_exception(self, db_batch_get_client_mock):
        db_batch_get_client_mock.return_value.batch_get_item.side_effect = [
            io_util.load_data_json("valid_batch_get_item_dynamo_response_1.json"),
            io_util.load_data_json("valid_batch_get_item_dynamo_response_2.json")
        ]
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key = "collection-id"
        test_values = ['123', '456', '101', '102']
        with self.assertRaises(Exception):
            ct.batch_get_items(test_key, test_values, max_retries=0)

    # Batch Write Item ********************************************

    # + test create next batch of transformed items to write.
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_get_next_batch(self, datetime_mock):
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        max_items_per_batch = 4
        items_to_put = [{"catalog-id": "101", "collection-id": "1", "event-id": "test-id"},
                        {"catalog-id": "101", "collection-id": "2", "event-id": "test-id"}]
        items_to_delete = [{"catalog-id": "101", "collection-id": "3"}, {"catalog-id": "101", "collection-id": "4"}]
        unprocessed_items = [{"DeleteRequest": {"Key": {"CatalogID": {'S': "101"}, "CollectionID": {'S': "5"}}}}]
        ct = Table('CatalogCollectionMappingTable', 'CatalogCollectionMappingInterfaceSchema.json')

        dynamodb_dict_items = ct._get_next_batch(max_items_per_batch, items_to_put, items_to_delete,
                                                 unprocessed_items)
        self.assertEqual(dynamodb_dict_items,
                         [{"DeleteRequest": {"Key": {"CatalogID": {'S': "101"}, "CollectionID": {'S': "5"}}}},
                          {"PutRequest": {"Item": {"CatalogID": {'S': "101"}, "CollectionID": {'S': "2"},
                                                   "EventID": {'S': "test-id"},
                                                   'dl:LastUpdated': {'S': '2019-04-10T12:00:00.000Z'}}}},
                          {"PutRequest": {"Item": {"CatalogID": {'S': "101"}, "CollectionID": {'S': "1"},
                                                   "EventID": {'S': "test-id"},
                                                   'dl:LastUpdated': {'S': '2019-04-10T12:00:00.000Z'}}}},
                          {"DeleteRequest": {"Key": {"CatalogID": {'S': "101"}, "CollectionID": {'S': "4"}}}}])
        self.assertEqual(items_to_put, [])
        self.assertEqual(items_to_delete, [{"catalog-id": "101", "collection-id": "3"}])
        for item in items_to_put:
            self.assertNotIn('dl:last-updated', item)

    # + Test get_unprocessed_item
    def test_get_unprocessed_items(self):
        ct = Table('CatalogCollectionMappingTable', 'CatalogCollectionMappingInterfaceSchema.json')
        items_to_put = [{"catalog-id": "101", "collection-id": "1"}]
        items_to_delete = [{"catalog-id": "101", "collection-id": "4"}]
        dynamodb_dict_items = [{"PutRequest": {"Item": {"CatalogID": {'S': "101"}, "CollectionID": {'S': "2"}}}},
                               {"DeleteRequest": {"Key": {"CatalogID": {'S': "101"}, "CollectionID": {'S': "3"}}}}]
        expected_output = {"PutRequest": [{"catalog-id": "101", "collection-id": "1"},
                                          {"catalog-id": "101", "collection-id": "2"}],
                           "DeleteRequest": [{"catalog-id": "101", "collection-id": "4"},
                                             {"catalog-id": "101", "collection-id": "3"}]}
        self.assertEqual(ct._get_unprocessed_items(dynamodb_dict_items, items_to_put, items_to_delete), expected_output)

    # +Table.batch_write_items -- exponential backoff and retry due to unprocessed items returned for the first batch
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_batch_write_items(self, db_batch_write_client_mock):
        db_batch_write_client_mock.return_value.batch_write_item.side_effect = [
            io_util.load_data_json("valid_batch_write_item_dynamo_response.json"),
            {"UnprocessedItems": {}},
            {"UnprocessedItems": {}}
        ]
        ct = Table('CatalogCollectionMappingTable', 'CatalogCollectionMappingInterfaceSchema.json')
        dict_items_put = [{"catalog-id": "101", "collection-id": "1", "event-id": "test-id"},
                          {"catalog-id": "101", "collection-id": "2", "event-id": "test-id"}]
        dict_items_delete = [{"catalog-id": "101", "collection-id": "4", "event-id": "test-id"},
                             {"catalog-id": "101", "collection-id": "3", "event-id": "test-id"}]
        batch_size = 3
        self.assertDictEqual(ct.batch_write_items(dict_items_put, dict_items_delete, batch_size), {})

    # +Table.batch_write_items -- exponential backoff and retry due to ProvisionedThroughputExceededException
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_batch_write_items_retry_after_throughput(self, db_batch_write_client_mock):
        db_batch_write_client_mock.return_value.batch_write_item.side_effect = [
            ClientError({'Error': {'Code': 'ProvisionedThroughputExceededException',
                                   'Message': 'This is a mock'}
                         },
                        "FAKE"),
            {"UnprocessedItems": {}}
        ]
        ct = Table('CatalogCollectionMappingTable', 'CatalogCollectionMappingInterfaceSchema.json')
        dict_items_put = [{"catalog-id": "101", "collection-id": "1", "event-id": "test-id"},
                          {"catalog-id": "101", "collection-id": "2", "event-id": "test-id"}]
        self.assertDictEqual(ct.batch_write_items(dict_items_put), {})

    # -Table.batch_write_items - ClientError - max_items_per_batch > 25
    def test_batch_write_items_max_items_per_batch(self):
        ct = Table('CatalogCollectionMappingTable', 'CatalogCollectionMappingInterfaceSchema.json')
        dict_items_put = [{"catalog-id": "101", "collection-id": "1"}, {"catalog-id": "101", "collection-id": "2"}]
        with self.assertRaises(ClientError):
            ct.batch_write_items(dict_items_put, max_items_per_batch=26)

    # -Table.batch_write_items -- return unprocessed items after reach the retries limit
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_batch_write_items_max_retries(self, db_batch_write_client_mock):
        db_batch_write_client_mock.return_value.batch_write_item.side_effect = [
            io_util.load_data_json("valid_batch_write_item_dynamo_response.json"),
            io_util.load_data_json("valid_batch_write_item_dynamo_response.json")
        ]
        ct = Table('CatalogCollectionMappingTable', 'CatalogCollectionMappingInterfaceSchema.json')
        dict_items_put = [{"catalog-id": "101", "collection-id": "1", "event-id": "test-id"}]
        dict_items_delete = [{"catalog-id": "101", "collection-id": "3"}]
        expected_response = {"PutRequest": [{"catalog-id": "101", "collection-id": "1"}],
                             "DeleteRequest": [{"catalog-id": "101", "collection-id": "3"}]}
        self.assertDictEqual(ct.batch_write_items(dict_items_put, dict_items_delete, max_retries=1),
                             expected_response)

    # -Table.batch_write_items -- ClientError
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_batch_write_items_client_error(self, db_batch_write_client_mock):
        db_batch_write_client_mock.return_value.batch_write_item.side_effect = ClientError(
            {'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
             'Error': {
                 'Code': 'ValidationException',
                 'Message': 'This is a mock'}},
            "FAKE")
        ct = Table('CatalogCollectionMappingTable', 'CatalogCollectionMappingInterfaceSchema.json')
        dict_items_put = [{"catalog-id": "101", "collection-id": "1", "event-id": "test-id"},
                          {"catalog-id": "101", "collection-id": "2", "event-id": "test-id"}]
        with self.assertRaisesRegex(ClientError, r'An error occurred \(ValidationException\) when calling'
                                                 r' the FAKE operation: This is a mock'):
            ct.batch_write_items(dict_items_put)

    # -Table.batch_write_items -- General Exception
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_batch_write_items_general_exception(self, db_batch_write_client_mock):
        db_batch_write_client_mock.return_value.batch_write_item.side_effect = Exception('Mock Exception')
        ct = Table('CatalogCollectionMappingTable', 'CatalogCollectionMappingInterfaceSchema.json')
        dict_items_put = [{"catalog-id": "101", "collection-id": "1", "event-id": "mock-event-id"},
                          {"catalog-id": "101", "collection-id": "2", "event-id": "mock-event-id"}]
        with self.assertRaisesRegex(Exception, r"Mock Exception"):
            ct.batch_write_items(dict_items_put)

    # Item  Exists ********************************************

    # +Table.item_exists - true
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_item_exists(self, db_get_client_mock):
        db_get_item_response = io_util.load_data_json("valid_get_item_dynamo_response.json")
        db_get_client_mock.return_value.get_item.return_value = db_get_item_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        self.assertTrue(ct.item_exists(test_key_dict))
        db_get_client_mock.return_value.get_item.assert_called_once_with(Key={'CollectionID': {'S': '274123'}},
                                                                         TableName='CollectionTable')

    # +Table.item_exists - false
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_item_exists_1(self, db_get_client_mock):
        db_get_client_mock.return_value.get_item.return_value = {}
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123"}
        self.assertFalse(ct.item_exists(test_key_dict))
        db_get_client_mock.return_value.get_item.assert_called_once_with(Key={'CollectionID': {'S': '274123'}},
                                                                         TableName='CollectionTable')

    # -Table.get_item - client error
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_item_exists_fail_2(self, db_get_client_mock):
        db_get_client_mock.return_value.get_item.side_effect = \
            ClientError({'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
                         'Error': {
                             'Code': 'ProvisionedThroughputExceededException',
                             'Message': 'This is a mock'}},
                        "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": '274123'}
        with self.assertRaises(ClientError):
            ct.item_exists(test_key_dict)

    # Get All Items ********************************************

    # +Table.get_all_items
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_get_all_items(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_paginate_response = io_util.load_data_json("valid_get_all_items_dynamo_response.json")
        db_build_full_result_mock.return_value = db_paginate_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        item = ct.get_all_items()
        get_item_response = io_util.load_data_json("valid_get_all_items_response.json")
        self.assertListEqual(item, get_item_response)
        for i in range(len(item)):
            self.assertDictEqual(item[i], get_item_response[i])
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # +Table.get_all_items - max items passed in
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
           'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_get_all_items_valid_2(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_paginate_response = io_util.load_data_json("valid_get_all_items_dynamo_response.json")
        db_build_full_result_mock.return_value = db_paginate_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        max_items = 2
        item = ct.get_all_items(max_items=max_items)
        get_item_response = io_util.load_data_json("valid_get_all_items_response.json")
        self.assertListEqual(item, get_item_response)
        for i in range(len(item)):
            self.assertDictEqual(item[i], get_item_response[i])
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.return_value.paginate.assert_called_once_with(TableName='CollectionTable',
                                                                            PaginationConfig={'MaxItems': max_items,
                                                                                              'PageSize': max_items,
                                                                                              'StartingToken': None})

    # +Table.get_all_items - empty result
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_get_all_items_2(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_build_full_result_mock.return_value = {}
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        item = ct.get_all_items()
        self.assertListEqual(item, [])
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # -Table.get_all_items - ClientError
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_get_all_items_fail(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_build_full_result_mock.side_effect = \
            ClientError({'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
                         'Error': {
                             'Code': 'ProvisionedThroughputExceededException',
                             'Message': 'This is a mock'}},
                        "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        with self.assertRaises(ClientError):
            ct.get_all_items()
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # Query Items ********************************************

    # +Table.query_items - multiple items
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_query_items(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_paginate_response = io_util.load_data_json("valid_query_items_dynamo_response.json")
        db_build_full_result_mock.return_value = db_paginate_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        items = ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                               ExpressionAttributeValues={":collection_id": {"S": "274123"}})
        query_items_response = io_util.load_data_json("valid_query_items_response.json")
        for i in range(len(items)):
            self.assertDictEqual(items[i], query_items_response[i])
        db_get_paginator_mock.return_value.paginate.assert_called_once_with(
            ExpressionAttributeValues={':collection_id': {'S': '274123'}},
            KeyConditionExpression='CollectionID = :collection_id',
            TableName='CollectionTable')
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # +Table.query_items - zero items
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_query_items_1(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_build_full_result_mock.return_value = {}
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        items = ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                               ExpressionAttributeValues={":collection_id": {"S": "274123"}})
        self.assertFalse(items)
        db_get_paginator_mock.return_value.paginate.assert_called_once_with(
            ExpressionAttributeValues={':collection_id': {'S': '274123'}},
            KeyConditionExpression='CollectionID = :collection_id',
            TableName='CollectionTable')
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # +Table.query_items - /w index
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_query_items_2(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_paginate_response = io_util.load_data_json("valid_query_items_dynamo_response.json")
        db_build_full_result_mock.return_value = db_paginate_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        items = ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                               ExpressionAttributeValues={":collection_id": {"S": "274123"}},
                               index="foobar")
        query_items_response = io_util.load_data_json("valid_query_items_response.json")
        for i in range(len(items)):
            self.assertDictEqual(items[i], query_items_response[i])
        db_get_paginator_mock.return_value.paginate.assert_called_once_with(
            ExpressionAttributeValues={':collection_id': {'S': '274123'}},
            KeyConditionExpression='CollectionID = :collection_id',
            IndexName='foobar',
            TableName='CollectionTable')
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # +Table.query_items - /w max_items
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_query_items_3(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_paginate_response = io_util.load_data_json("valid_query_items_dynamo_response.json")
        db_build_full_result_mock.return_value = db_paginate_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        items = ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                               ExpressionAttributeValues={":collection_id": {"S": "274123"}},
                               max_items=2)
        query_items_response = io_util.load_data_json("valid_query_items_response.json")
        for i in range(len(items)):
            self.assertDictEqual(items[i], query_items_response[i])
        db_get_paginator_mock.return_value.paginate.assert_called_once_with(
            ExpressionAttributeValues={':collection_id': {'S': '274123'}},
            PaginationConfig={'MaxItems': 2, 'PageSize': 2, 'StartingToken': None},
            KeyConditionExpression='CollectionID = :collection_id',
            TableName='CollectionTable')
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # +Table.query_items - /w max_items and index
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_query_items_4(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_paginate_response = io_util.load_data_json("valid_query_items_dynamo_response.json")
        db_build_full_result_mock.return_value = db_paginate_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        items = ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                               ExpressionAttributeValues={":collection_id": {"S": "274123"}},
                               index="foobar",
                               max_items=2)
        query_items_response = io_util.load_data_json("valid_query_items_response.json")
        for i in range(len(items)):
            self.assertDictEqual(items[i], query_items_response[i])
        db_get_paginator_mock.return_value.paginate.assert_called_once_with(
            ExpressionAttributeValues={':collection_id': {'S': '274123'}},
            KeyConditionExpression='CollectionID = :collection_id',
            IndexName='foobar',
            PaginationConfig={'MaxItems': 2, 'PageSize': 2, 'StartingToken': None},
            TableName='CollectionTable')
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # 1Table.query_items - ClientError
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_query_items_fail(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_build_full_result_mock.side_effect = ClientError(
            {'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
             'Error': {
                 'Code': 'ProvisionedThroughputExceededException',
                 'Message': 'This is a mock'}},
            "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        with self.assertRaises(ClientError):
            ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                           ExpressionAttributeValues={":collection_id": {"S": "274123"}})
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # Count Items ********************************************
    # +Table.count_items
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_count_query_items(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_paginate_response = io_util.load_data_json("valid_count_items_dynamo_response.json")
        db_build_full_result_mock.return_value = db_paginate_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        count = ct.count_query_items(KeyConditionExpression='CollectionID = :collection_id',
                                     ExpressionAttributeValues={":collection_id": {"S": "274123"}})
        self.assertEqual(count, 1)
        db_get_paginator_mock.return_value.paginate.assert_called_once_with(
            ExpressionAttributeValues={':collection_id': {'S': '274123'}},
            KeyConditionExpression='CollectionID = :collection_id',
            Select='COUNT',
            TableName='CollectionTable')
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # +Table.count_items - with index
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_count_query_items_index(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_paginate_response = io_util.load_data_json("valid_count_items_dynamo_response.json")
        db_build_full_result_mock.return_value = db_paginate_response
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        count = ct.count_query_items(index='mock_index',
                                     KeyConditionExpression='CollectionID = :collection_id',
                                     ExpressionAttributeValues={":collection_id": {"S": "274123"}}
                                     )
        self.assertEqual(count, 1)
        db_get_paginator_mock.return_value.paginate.assert_called_once_with(
            IndexName='mock_index',
            ExpressionAttributeValues={':collection_id': {'S': '274123'}},
            KeyConditionExpression='CollectionID = :collection_id',
            Select='COUNT',
            TableName='CollectionTable')
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # -Table.count_query_items - ClientError
    @patch(
        'lng_aws_clients.dynamodb.get_client.return_value.get_paginator.return_value.'
        'paginate.return_value.build_full_result')
    @patch('lng_aws_clients.dynamodb.get_client.return_value.get_paginator', autospec=True)
    @patch('lng_aws_clients.dynamodb.get_client', autospec=True)
    def test_table_count_query_items_fail(self, db_get_client_mock, db_get_paginator_mock, db_build_full_result_mock):
        db_build_full_result_mock.side_effect = ClientError(
            {'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
             'Error': {
                 'Code': 'ProvisionedThroughputExceededException',
                 'Message': 'This is a mock'}},
            "FAKE")
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        with self.assertRaises(ClientError):
            ct.count_query_items(KeyConditionExpression='CollectionID = :collection_id',
                                 ExpressionAttributeValues={":collection_id": {"S": "274123"}})
        db_get_client_mock.assert_called_once()
        db_get_paginator_mock.assert_called_once()

    # Update Item ********************************************

    # +Table.update_item
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_commons.time_helper.datetime')
    def test_table_update_item(self, datetime_mock, db_get_client_mock):
        datetime_mock.utcnow.return_value = datetime(2019, 4, 10, 12)
        db_get_client_mock.return_value.put_item.return_value = None
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        test_key_dict = {"collection-id": "274123",
                         "collection-timestamp": "2018-01-26T15:08:34.053Z"}
        test_attrs_dict = io_util.load_data_json("valid_test_attr.json")
        test_dict = {**test_key_dict, **test_attrs_dict}
        ct.update_item(test_dict)
        dynamo_put_item_item = io_util.load_data_json("valid_dynamo_put_item.json")
        db_get_client_mock.return_value.put_item.assert_called_once_with(Item=dynamo_put_item_item,
                                                                         TableName='CollectionTable')
        self.assertNotIn('dl:last-updated', test_dict)

    # Update Counter ********************************************

    # +Table.update_counter
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counter(self, db_get_client_mock):
        update_item_response = io_util.load_data_json("valid_update_counter_response.json")
        db_get_client_mock.return_value.update_item.return_value = update_item_response
        ct = Table('CounterTable')
        counter_val = ct.update_counter({'Namespace': {"S": "test"}}, "AtomicCounter")
        self.assertEqual(counter_val, 44)
        call_args = db_get_client_mock.return_value.update_item.call_args[1]
        # Assert counter was incremented by default value 1
        self.assertDictEqual(call_args['ExpressionAttributeValues'], {':count': {'N': '1'}})
        print("counter_val={0}".format(counter_val))

    # +Table.update_counter increment = 1000
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counter_increment(self, db_get_client_mock):
        update_item_response = io_util.load_data_json("valid_update_counter_response.json")
        db_get_client_mock.return_value.update_item.return_value = update_item_response
        ct = Table('CounterTable')
        counter_val = ct.update_counter({'Namespace': {"S": "test"}}, "AtomicCounter", 1000)
        self.assertEqual(counter_val, 44)
        call_args = db_get_client_mock.return_value.update_item.call_args[1]
        # Assert counter was incremented by 1000
        self.assertDictEqual(call_args['ExpressionAttributeValues'], {':count': {'N': '1000'}})
        self.assertEqual(call_args['ReturnValues'], 'UPDATED_NEW')
        print("counter_val={0}".format(counter_val))

    # +Table.update_counter counter_only=False
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counter_increment_2(self, db_get_client_mock):
        update_item_response = io_util.load_data_json("valid_update_counter_response.json")
        db_get_client_mock.return_value.update_item.return_value = update_item_response
        ct = Table('CounterTable')
        response = ct.update_counter({'Namespace': {"S": "test"}}, "AtomicCounter", counter_only=False)
        self.assertEqual(response, {'atomic-counter': 44})
        call_args = db_get_client_mock.return_value.update_item.call_args[1]
        self.assertDictEqual(call_args['ExpressionAttributeValues'], {':count': {'N': '1'}})
        self.assertEqual(call_args['ReturnValues'], 'ALL_NEW')
        print("response={0}".format(response))

    # +Table.update_counters
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counters(self, db_get_client_mock):
        update_item_response = io_util.load_data_json("valid_update_counters_response.json")
        db_get_client_mock.return_value.update_item.return_value = update_item_response
        ct = Table('IngestionTable')
        counter_val = ct.update_counters({'Namespace': {"S": "test"}}, {"AtomicCounter": 1, "SecondAtomicCounter": 5},
                                         counter_only=True)
        self.assertDictEqual(counter_val, {"atomic-counter": 44, "second-atomic-counter": 5})
        call_args = db_get_client_mock.return_value.update_item.call_args[1]
        # Assert counter was incremented by default value 1
        self.assertDictEqual(call_args['ExpressionAttributeValues'], {':item0': {'N': '1'}, ':item1': {'N': '5'}})
        self.assertEqual(call_args['ReturnValues'], 'UPDATED_NEW')
        print("counter_val={0}".format(counter_val))

    # +Table.update_counters
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counters_2(self, db_get_client_mock):
        update_item_response = io_util.load_data_json("valid_update_counters_response.json")
        db_get_client_mock.return_value.update_item.return_value = update_item_response
        ct = Table('IngestionTable')
        counter_val = ct.update_counters({'Namespace': {"S": "test"}}, {"AtomicCounter": 1, "SecondAtomicCounter": 5},
                                         counter_only=False)
        self.assertDictEqual(counter_val, {"atomic-counter": 44, "second-atomic-counter": 5})
        call_args = db_get_client_mock.return_value.update_item.call_args[1]
        # Assert counter was incremented by default value 1
        self.assertDictEqual(call_args['ExpressionAttributeValues'], {':item0': {'N': '1'}, ':item1': {'N': '5'}})
        self.assertEqual(call_args['ReturnValues'], 'ALL_NEW')
        print("counter_val={0}".format(counter_val))

    # +Table.update_counters with condition
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counters_3(self, db_get_client_mock):
        update_item_response = io_util.load_data_json("valid_update_counters_response.json")
        db_get_client_mock.return_value.update_item.return_value = update_item_response
        ct = Table('IngestionTable')
        counter_val = ct.update_counters({'Namespace': {"S": "test"}}, {"AtomicCounter": 1, "SecondAtomicCounter": 5},
                                         counter_only=False, condition='attribute_exists(Namespace)')
        self.assertDictEqual(counter_val, {"atomic-counter": 44, "second-atomic-counter": 5})
        call_args = db_get_client_mock.return_value.update_item.call_args[1]
        # Assert counter was incremented by default value 1
        self.assertDictEqual(call_args['ExpressionAttributeValues'], {':item0': {'N': '1'}, ':item1': {'N': '5'}})
        self.assertEqual(call_args['ReturnValues'], 'ALL_NEW')
        self.assertEqual(call_args['ConditionExpression'], 'attribute_exists(Namespace)')
        print("counter_val={0}".format(counter_val))

    # +Table.update_counters with additional attributes
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counters_4(self, db_get_client_mock):
        update_item_response = io_util.load_data_json("valid_update_counters_response.json")
        db_get_client_mock.return_value.update_item.return_value = update_item_response
        ct = Table('IngestionTable')
        counter_val = ct.update_counters({'Namespace': {"S": "test"}}, {"AtomicCounter": 1, "SecondAtomicCounter": 5},
                                         counter_only=False, condition=False,
                                         additional_attributes={"attr0": {"N": "123"},
                                                                "attr1": {"S": "abc"}})
        self.assertDictEqual(counter_val, {"atomic-counter": 44, "second-atomic-counter": 5})
        call_args = db_get_client_mock.return_value.update_item.call_args[1]
        # Assert counter was incremented by default value 1
        self.assertDictEqual(call_args['ExpressionAttributeValues'], {':item0': {'N': '1'}, ':item1': {'N': '5'},
                                                                      ':val0': {"N": "123"}, ':val1': {"S": "abc"}})
        self.assertEqual(call_args['ReturnValues'], 'ALL_NEW')
        print("counter_val={0}".format(counter_val))

    # +Table.update_counters - Exception
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counters_exception(self, db_get_client_mock):
        db_get_client_mock.return_value.update_item.side_effect = ClientError(
            {'ResponseMetadata': {'RequestId': '1234'},
             'Error': {
                 'Code': 'ProvisionedThroughputExceededException',
                 'Message': 'This is a mock'}},
            "FAKE")
        ct = Table('IngestionTable')
        with self.assertRaisesRegex(ClientError, "An error occurred"):
            ct.update_counters({'Namespace': {"S": "test"}}, {"AtomicCounter": 1, "SecondAtomicCounter": 5},
                               counter_only=False)

    # -Table.update_counter
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_table_update_counter_fail(self, db_get_client_mock):
        db_get_client_mock.return_value.update_item.side_effect = \
            ClientError({'ResponseMetadata': {'RequestId': 'A4V2HMNJDMFNJBE2IKQJ3B7QKFVV4KQNSO5AEMVJF66Q9ASUAAJG'},
                         'Error': {
                             'Code': 'ProvisionedThroughputExceededException',
                             'Message': 'This is a mock'}},
                        "FAKE")
        ct = Table('CounterTable')
        with self.assertRaises(ClientError):
            ct.update_counter({'Namespace': {"S": "test"}}, "AtomicCounter")

    # +Get Pagination Token ********************************************

    def test_table_get_pagination_token(self):
        ct = Table('TestTable')
        token = ct.get_pagination_token()
        self.assertIsNone(token)

    # - _load_json_schema - Invalid Schema
    def test_load_json_schema_fail_1(self):
        with self.assertRaises(lng_datalake_dal.exceptions.SchemaLoadError):
            # Need to pass in the file path, not the file since we load within
            json_file_path = os.path.join(io_util.data_path, "invalid_json_schema.json")
            Table._load_json_schema(json_file_path)

    # - _generate_schema_validator - SchemaError
    def test_generate_schema_validator_schema_error(self):
        json_file_path = os.path.join(io_util.data_path, "invalid_schema.json")
        schema = Table._load_json_schema(json_file_path)
        with self.assertRaisesRegex(lng_datalake_dal.exceptions.SchemaError, "Failed to compile the schema"):
            Table('CollectionTable', 'CollectionInterfaceSchema.json')._generate_schema_validator(schema)

    # - _validate_against_interface_schema - SchemaValidationError
    def test_validate_against_interface_schema_fail_1(self):
        with self.assertRaisesRegex(lng_datalake_dal.exceptions.SchemaValidationError,
                                    "Items did not match interface schema"):
            ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
            ct._validate_against_interface_schema({'test'})

    # + _convert_to_list - Valid
    def test_convert_to_list_valid(self):
        self.assertEqual(Table._convert_to_list(None), [])
        self.assertEqual(Table._convert_to_list([]), [])
        self.assertEqual(Table._convert_to_list([{"test": "data"}]), [{"test": "data"}])

    # + build_event_ids_list - no event id stored
    def test_build_event_ids_list_1(self):
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        collection_item = {'collection-id': 'test_collection'}
        expected_output = ['test_event_1']
        self.assertEqual(ct.build_event_ids_list(collection_item, 'test_event_1'), expected_output)
        self.assertEqual(collection_item, {'collection-id': 'test_collection'})

    # + build_event_ids_list - less than 3 event-ids stored
    def test_build_event_ids_list_2(self):
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        collection_item = {'collection-id': 'test_collection',
                           'event-ids': ['test_event_0']}
        expected_output = ['test_event_0', 'test_event_1']
        self.assertEqual(ct.build_event_ids_list(collection_item, 'test_event_1'), expected_output)
        self.assertEqual(collection_item, {'collection-id': 'test_collection',
                                           'event-ids': ['test_event_0']})

    # + build_event_ids_list - 3 event-ids stored
    def test_build_event_ids_list_3(self):
        ct = Table('CollectionTable', 'CollectionInterfaceSchema.json')
        collection_item = {'collection-id': 'test_collection',
                           'event-ids': ['test_event_0', 'test_event_1', 'test_event_2']}
        expected_output = ['test_event_1', 'test_event_2', 'test_event_3']
        self.assertEqual(ct.build_event_ids_list(collection_item, 'test_event_3'), expected_output)
        self.assertEqual(collection_item, {'collection-id': 'test_collection',
                                           'event-ids': ['test_event_0', 'test_event_1', 'test_event_2']})


if __name__ == '__main__':
    unittest.main()
