import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CollectionTable')


@patch.dict('os.environ',
            values={'COLLECTION_DYNAMODB_TABLE': 'DataLakeDynamo-CollectionTable-YNH2CMRGBSCS'},
            clear=True)
class TestCollectionTable(unittest.TestCase):
    # +CollectionTable.__init__ - can create CollectionTable object and load schema
    def test_collection_table_init(self):
        ct = CollectionTable()
        self.assertIsNotNone(ct)
        self.assertEqual(ct.table_name, 'DataLakeDynamo-CollectionTable-YNH2CMRGBSCS')

    # +CollectionTable.put_item
    def test_collection_table_put_item(self):
        test_key_dict = {'collection-id': 'ABCDefgh'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(CollectionID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = CollectionTable()
            ost.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -CollectionTable.put_item - raises ConditionError
    def test_collection_table_put_item_fail1(self):
        test_key_dict = {'collection-id': 'ABCDefgh'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create CollectionTable super class that we want to test
            with self.assertRaises(ConditionError):
                CollectionTable().put_item(put_dict)

    # +CollectionTable.update_item
    def test_collection_table_update_item(self):
        test_key_dict = {'collection-id': 'ABCDefgh'}
        test_attrs_dict = \
            {
                'description': 'New description'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create ObjectStoreTable super class that we want to test
            ost = CollectionTable()
            ost.update_item(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict, None, None)

    # -CollectionTable.update_counter - raises NotImplementedError
    def test_collection_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CollectionTable().update_counter(primary_key)

    # -CollectionTable.update_counters - raises NotImplementedError
    def test_collection_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            CollectionTable().update_counters(primary_key, {"Counter": 1})

    # -CollectionTable.batch_write_items - raises NotImplementedError
    def test_collection_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            CollectionTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
