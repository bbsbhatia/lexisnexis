import unittest
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.object_store_version_table import ObjectStoreVersionTable

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ObjectStoreVersionTable')


@patch.dict('os.environ',
            values={'OBJECT_STORE_VERSION_DYNAMODB_TABLE': 'DataLakeDynamo-ObjectStoreVersionTable-WB4Q353A38QA'},
            clear=True)
class TestObjectStoreVersionTable(unittest.TestCase):
    # +ObjectStoreVersionTable.__init__ - can create ObjectStoreVersionTable object and load schema
    def test_object_store_table_init(self):
        ost = ObjectStoreVersionTable()
        self.assertIsNotNone(ost)
        self.assertEqual(ost.table_name, 'DataLakeDynamo-ObjectStoreVersionTable-WB4Q353A38QA')

    # +ObjectStoreVersionTable.generate_composite_key - keys are equal
    def test_object_store_table_generate_composite_key(self):
        self.assertEqual(ObjectStoreVersionTable.generate_composite_key('ObjId', 'CollectionId'), 'ObjId|CollectionId')

    # -ObjectStoreVersionTable.get_all_items - raises NotImplementedError
    def test_object_store_table_get_all_items(self):
        with self.assertRaises(NotImplementedError):
            ObjectStoreVersionTable().get_all_items()

    # +ObjectStoreVersionTable.update_counter - raises NotImplementedError
    def test_object_store_table_update_counter(self):
        with self.assertRaises(NotImplementedError):
            ObjectStoreVersionTable().update_counter('Test')

    # -ObjectStoreVersionTable.update_counters - raises NotImplementedError
    def test_object_store_version_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            ObjectStoreVersionTable().update_counters(primary_key, {"Counter": 1})

    # +ObjectStoreVersionTable.batch_write_items - raises NotImplementedError
    def test_object_store_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            ObjectStoreVersionTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
