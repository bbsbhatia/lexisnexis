import unittest
from unittest import mock
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.ingestion_table import IngestionTable
from lng_datalake_dal.table import Table

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'IngestionTable')

ingestion_table_name = 'feature-jek-DataLake-IngestionTable'


@patch.dict('os.environ',
            values={'INGESTION_DYNAMODB_TABLE': ingestion_table_name},
            clear=True)
class TestIngestionTable(unittest.TestCase):
    # +IngestionTable.__init__ - can create ingestion table object and load schema
    def test_ingestion_table_init(self):
        it = IngestionTable()
        self.assertIsNotNone(it)
        self.assertEqual(it.table_name, ingestion_table_name)

    # +IngestionTable.put_item
    def test_ingestion_table_put_item(self):
        test_key_dict = {'ingestion-id': 'ingestion-01'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        put_condition = 'attribute_not_exists(IngestionID)'
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create IngestionTable super class that we want to test
            it = IngestionTable()
            it.put_item(put_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(put_dict, condition=put_condition)

    # -IngestionTable.put_item - raises ConditionError
    def test_ingestion_table_put_item_fail1(self):
        test_key_dict = {'ingestion-id': 'ingestion-01'}
        test_attrs_dict = {}
        put_dict = {**test_key_dict, **test_attrs_dict}
        # Mock the base class method Table.put_item()
        with mock.patch.object(Table, 'put_item', side_effect=ConditionError) as mock_method:
            # Create IngestionTable super class that we want to test
            with self.assertRaises(ConditionError):
                IngestionTable().put_item(put_dict)

    # +IngestionTable.update_item
    def test_ingestion_table_update_item(self):
        test_key_dict = {'ingestion-id': 12345}
        test_attrs_dict = \
            {
                'error-description': 'Error description'
            }
        update_dict = {**test_key_dict, **test_attrs_dict}
        with mock.patch.object(Table, 'put_item') as mock_method:
            # Create IngestionTable super class that we want to test
            it = IngestionTable()
            it.update_item(update_dict)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(update_dict, None)

    # +CounterTable.update_counters - calls base class with correct parameters and value
    def test_counter_table_update_counter(self):
        ingestion_id = 'test_ingestion_id'
        dict_key = {'IngestionID': {'S': ingestion_id}}
        counters = {'ObjectTrackedCount': 10, "ObjectProcessedCount": 2}
        # Mock the base class method Table.update_counter()
        with mock.patch.object(Table, 'update_counters') as mock_method:
            # Create IngestionTable super class that we want to test
            it = IngestionTable()
            it.update_counters(ingestion_id, counters)
            # verify mocked base class method called with correct parameters and values
            mock_method.assert_called_with(dict_key, counters, True)

    # -IngestionTable.update_counter - raises NotImplementedError
    def test_ingestion_table_update_counter(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            IngestionTable().update_counter(primary_key)

    # -IngestionTable.batch_write_items - raises NotImplementedError
    def test_ingestion_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            IngestionTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
