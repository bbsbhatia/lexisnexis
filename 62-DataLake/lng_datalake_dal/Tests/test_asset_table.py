import unittest
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_dal.asset_table import AssetTable

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'AssetTable')


@patch.dict('os.environ',
            values={'ASSET_DYNAMODB_TABLE': 'DataLakeDynamo-AssetTable-12S1V8YP519H8'},
            clear=True)
class TestAssetTable(unittest.TestCase):
    # +AssetTable.__init__ - can create asset table object and load schema
    def test_asset_table_init(self):
        at = AssetTable()
        self.assertIsNotNone(at)
        self.assertEqual(at.table_name, 'DataLakeDynamo-AssetTable-12S1V8YP519H8')

    # -AssetTable.query_items - raises NotImplementedError
    def test_asset_table_query_items(self):
        with self.assertRaises(NotImplementedError):
            AssetTable().query_items()

    # -AssetTable.update_counter - raises NotImplementedError
    def test_asset_table_update_counter(self):
        primary_key = {"asset-id": 66666}
        with self.assertRaises(NotImplementedError):
            AssetTable().update_counter(primary_key)

    # -AssetTable.update_counters - raises NotImplementedError
    def test_asset_table_update_counters(self):
        primary_key = 'test'
        with self.assertRaises(NotImplementedError):
            AssetTable().update_counters(primary_key, {"Counter": 1})

    # -AssetTable.batch_write_items - raises NotImplementedError
    def test_asset_table_batch_write_items(self):
        with self.assertRaises(NotImplementedError):
            AssetTable().batch_write_items()


if __name__ == '__main__':
    unittest.main()
