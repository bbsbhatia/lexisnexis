import re
from os import getenv

from setuptools import setup, find_packages


def build_version():
    major = '1'
    minor = '2'
    rev = getenv('BUILD_NUMBER', '0')  # This is provided as a Jenkins Global Variable in the build system
    branch = getenv('GIT_BRANCH', 'local')  # This is provided as a Jenkins Global Variable when using Pipeline SCM

    base_version = '.'.join([major, minor, rev])

    if branch.endswith('master'):
        return base_version
    else:
        sanitized_branch = re.sub(r'[^a-zA-Z0-9]+', '.', branch)
        return '+'.join([base_version, sanitized_branch])


setup(
    name='lng_datalake_dal',
    packages=find_packages(),
    package_data={'lng_datalake_dal': ['Schemas/*.json']},
    include_package_data=True,
    version=build_version(),
    description='LNG Data Access Layer',
    author='John Morelock',
    author_email='John.Morelock@lexisnexis.com',
    url='https://tfs-glo-lexisadvance.visualstudio.com/DefaultCollection/Content/_git/62-DataLakeUtils',
    keywords=['lng_datalake_dal'],  # arbitrary keywords
    install_requires=['boto3', 'botocore', 'lng_aws_clients', 'lng_datalake_testhelper', 'lng_datalake_commons',
                      'fastjsonschema', 'lng_orjson']
)
