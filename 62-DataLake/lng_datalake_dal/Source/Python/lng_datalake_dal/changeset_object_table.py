import os

from lng_datalake_dal.table import Table, TableCache

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


# We did not implement put_item with condition on this table on purpose because we will always going to do put.
class ChangesetObjectTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("CHANGESET_OBJECT_DYNAMODB_TABLE")
        interface_schema_name = 'ChangesetObjectInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    @staticmethod
    def generate_composite_key(object_id, collection_id, version_number):
        return "{0}|{1}|{2}".format(object_id, collection_id, version_number)

    def get_all_items(self, max_items=None, pagination_token=None, **kwargs):
        # calls to this method will throw exception since we don't want it called.
        raise NotImplementedError

    def update_counter(self, counter_attribute_name):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session
    import json
    import logging

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'
    os.environ['CHANGESET_OBJECT_DYNAMODB_TABLE'] = 'feature-maen-DataLake-ChangesetObjectTable'

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    test_key_dict = \
        {
            "composite-key": "49cf62814f629ba0a679147d8784b516cde30a17|274|1",
            "changeset-id": "1"

        }
    test_attrs_dict = \
        {
            "collection-hash": "10",
            "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            "collection-id": "274",
            "object-key": "49cf62814f629ba0a679147d8784b516cde30a17.gz",
            "version-timestamp": "2018-02-07T09:40:59.592Z",
            "raw-content-length": 10,
            "bucket-name": "feature-maen-datalake-object-store-288044017584",
            "content-type": "pdf",
            "event-id": "123",
            "version-number": 1

        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    osvt = ChangesetObjectTable()
    osvt.put_item(test_dict)
    logger.debug(json.dumps(osvt.query_items(
        KeyConditionExpression='CompositeKey=:composite_key and ChangesetID=:changeset_id',
        ExpressionAttributeValues={":composite_key": {"S": "49cf62814f629ba0a679147d8784b516cde30a17|274|1"},
                                   ":changeset_id": {"S": str(1)}}), indent=4))
    logger.debug(json.dumps(osvt.get_item(test_key_dict), indent=4))
    osvt.delete_item(test_key_dict)
