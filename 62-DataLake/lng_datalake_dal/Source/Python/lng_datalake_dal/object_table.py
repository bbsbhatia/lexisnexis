import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class ObjectTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("OBJECT_TABLE")
        interface_schema_name = 'ObjectInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    @staticmethod
    def generate_composite_key(object_id, collection_id):
        return "{0}|{1}".format(object_id, collection_id)

    def put_item(self, dict_items):
        dict_items["latest-version"] = 'Y'
        try:
            super().put_item(dict_items,
                             condition='(attribute_not_exists(CompositeKey) and attribute_not_exists(VersionNumber))')
        except ConditionError as e:
            e.args = e.args + ("ObjectID {0} with VersionNumber {1} already exists for CollectionID {2} in {3}"
                               .format(dict_items["object-id"],
                                       dict_items["version-number"],
                                       dict_items["collection-id"],
                                       os.getenv("OBJECT_TABLE")),)
            raise

    def delete_latest_version_attribute(self, dict_items):
        if dict_items.get("latest-version"):
            dict_items.pop("latest-version")
        try:
            super().put_item(dict_items,
                             condition='VersionTimestamp = :v',
                             expression_values={
                                 ':v': dict_items['version-timestamp']
                             })

        except KeyError as e:
            e.args = e.args + ("version-timestamp is required attribute for {0} item comparison. Missing required "
                               "attribute: {1}".format(os.getenv("OBJECT_TABLE"), e),)
            raise

        except ConditionError as e:
            e.args = e.args + ("Cannot change VersionTimestamp for ObjectID {0} CollectionID {1} VersionNumber {2} "
                               "in {3}".format(dict_items["object-id"],
                                               dict_items["collection-id"],
                                               dict_items["version-number"],
                                               os.getenv("OBJECT_TABLE")),)
            raise

    def update_item(self):
        raise NotImplementedError

    def update_counter(self, counter_attribute_name):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError

    def put_transaction_item(self):
        raise NotImplementedError

    def update_transaction_item(self):
        raise NotImplementedError

    def delete_transaction_item(self):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session
    import json
    import logging

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OBJECT_TABLE'] = 'feature-kgg-DataLake-ObjectTable1'

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    test_key_dict = \
        {
            'composite-key': 'jun18|test_coll',
            'version-number': 2
        }
    test_attrs_dict = \
        {
            "object-id": "jun18",
            "collection-id": "test_coll",
            "collection-hash": "test_hash",
            "latest-version": "Y",
            "object-state": "Created",
            "object-key": "test_key",
            "bucket-name": "test_bucket",
            "replicated-buckets": ["bucket2"],
            "content-type": "text/plain",
            "raw-content-length": 10,
            "version-timestamp": "2019-06-03T16:09:33.861Z"

        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    ot = ObjectTable()
    # ot.put_item(test_dict)
    ot.delete_latest_version_attribute(test_dict)

    logger.debug(json.dumps(ot.get_item(test_key_dict), indent=4))
    logger.debug(json.dumps(ot.query_items(KeyConditionExpression='CompositeKey = :composite_key',
                                           ExpressionAttributeValues={":composite_key": {"S": "jun18|test_coll"}}),
                            indent=4))

    logger.debug(json.dumps(ot.query_items(index="object-collection-hash-and-latest-version-index-1",
                                           KeyConditionExpression='CollectionHash=:collection_hash',
                                           ExpressionAttributeValues={":collection_hash": {"S": "test_hash"}}),
                            indent=4))

    ot.delete_item(test_key_dict)
