import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Maen Nanaa, Aaron Belvo"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"


class CollectionTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("COLLECTION_DYNAMODB_TABLE")
        interface_schema_name = 'CollectionInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(CollectionID)')
        except ConditionError as e:
            e.args = e.args + ("CollectionID {0} already exists in Collection table"
                               .format(dict_items["collection-id"]),)
            raise

    def update_item(self, dict_items, condition=None, expression_values=None):
        super().put_item(dict_items, condition, expression_values)

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-DataLake-CollectionTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().isoformat()

    test_key_dict = \
        {
            "collection-id": "DAL Test 1"
        }
    test_attrs_dict = \
        {
            "asset-id": 62,
            "classification-type": "Test",
            "description": "My first collection test",
            "owner-id": 19,
            "collection-state": "Collection::Created",
            "collection-timestamp": curr_timestamp,
            "old-object-versions-to-keep": 3,
            "catalog-ids": ["catalog-01", "catalog-02"]
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    ct = CollectionTable()
    ct.put_item(test_dict)
    print(json.dumps(ct.get_item(test_key_dict), indent=4))
    print(json.dumps(ct.get_all_items(), indent=4))
    print(json.dumps(ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                                    ExpressionAttributeValues={":collection_id": {"S": "274123"}}), indent=4))
    print(json.dumps(ct.query_items(index="collection-by-owner-and-name-index",
                                    KeyConditionExpression='OwnerID = :owner_id',
                                    ExpressionAttributeValues={":owner_id": {"N": "101"}}), indent=4))
    ct.delete_item(test_key_dict)
