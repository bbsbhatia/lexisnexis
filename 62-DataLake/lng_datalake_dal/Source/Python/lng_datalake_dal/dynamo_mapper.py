import decimal

from boto3.dynamodb.types import TypeDeserializer
from boto3.dynamodb.types import TypeSerializer
from lng_datalake_commons import time_helper

_ACRONYMS = ['id', 'url']
_GLOBAL_TABLE_PREFIX = 'aws:rep:'
_DATALAKE_PREFIX = 'dl:'
_HIDDEN_PREFIXES = (_GLOBAL_TABLE_PREFIX, _DATALAKE_PREFIX)
_DL_LAST_UPDATED_ATTR = '{0}{1}'.format(_DATALAKE_PREFIX, 'LastUpdated')


def transform_to_dynamo_dict(input_dict: dict, read_only: bool = True) -> dict:
    dynamo_serializer = TypeSerializer()
    dynamo_dict = {}

    for key in input_dict:
        dynamo_dict[__to_upper_camelcase(key)] = dynamo_serializer.serialize(input_dict[key])

    # Add "dl:LastUpdated" attribute to all writable dynamo DB dicts
    if not read_only:
        dynamo_dict[_DL_LAST_UPDATED_ATTR] = dynamo_serializer.serialize(time_helper.get_current_timestamp())

    return dynamo_dict


def transform_to_client_dict(input_dict: dict) -> dict:
    dynamo_deserializer = TypeDeserializer()
    client_dict = {}
    for key in input_dict:
        # Don't return hidden table attributes, these should never be touched or modified
        if not key.startswith(_HIDDEN_PREFIXES):
            client_dict[__to_hyphenated_lowercase(key)] = __replace_decimals(
                dynamo_deserializer.deserialize(input_dict[key]))

    return client_dict


def __to_upper_camelcase(name: str) -> str:
    cc_name = str().join([part.upper() if part.lower() in _ACRONYMS else part[0].upper() + part[1:].lower()
                          for part in name.split('-')])
    return cc_name


def __to_hyphenated_lowercase(name: str) -> str:
    for acronym in _ACRONYMS:
        name = name.replace(acronym.upper(), '-' + acronym)
    return str().join(['-' + char.lower() if char.isupper() else char for char in name])[1:]

    # https://github.com/boto/boto3/issues/369


def __replace_decimals(obj: object) -> object:
    if isinstance(obj, set):
        return __replace_decimals(list(obj))
    elif isinstance(obj, list):
        for i in range(len(obj)):
            obj[i] = __replace_decimals(obj[i])
        return obj
    elif isinstance(obj, dict):
        for k, v in obj.items():
            obj[k] = __replace_decimals(v)
        return obj
    elif isinstance(obj, decimal.Decimal):
        if obj % 1 == 0:
            return int(obj)
        else:
            return float(obj)
    else:
        return obj


if __name__ == '__main__':
    in_client_dict = {
        'foo-bar': 'test1',
        'game-on': 'test2',
        'my-id': 'test3',
        'your-url': 'test4'}
    print("in_client_dict={}".format(in_client_dict))
    db_dict = transform_to_dynamo_dict(in_client_dict)
    print("dynamo_dict={}".format(db_dict))
    db_dict = transform_to_dynamo_dict(in_client_dict, read_only=False)
    print("dynamo_dict={}".format(db_dict))
    out_client_dict = transform_to_client_dict(db_dict)
    print("out_client_dict={}".format(out_client_dict))
