import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Doug Heitkamp, Shekhar Ralhan"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class ObjectRelationshipTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("OBJECT_RELATIONSHIP_DYNAMODB_TABLE")
        interface_schema_name = 'ObjectRelationshipInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    @staticmethod
    # Example relationship key for a non-aggregate: judges|v00003
    def generate_relationship_key(relationship_id: str, relationship_version: int) -> str:
        return "{0}|v{1:0>5}".format(relationship_id, relationship_version)

    @staticmethod
    # Unpack relationship key (e.g. judges|v00003) for a non-aggregate into ("judges", 3)
    # NOTE: The relationship key has a v0000X version component since the relationship key is
    # the Object Relationship table's sort (range) key.  The version component was structured this way
    # for the proper sort of a number component when the attribute is a string.
    def unpack_relationship_key(relationship_key: str):
        key_parts = relationship_key.split('|')
        try:
            relationship_version = int(key_parts[-1].split('v')[-1])
        except ValueError as ve:
            ve.args = ve.args + (
                "Could not unpack version number from relationship key for {}".format(relationship_key),)
            raise

        return key_parts[0], relationship_version

    @staticmethod
    def generate_object_composite_id(object_id: str, collection_id: str, version_number: int) -> str:
        return "{0}|{1}|{2}".format(object_id, collection_id, version_number)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(TargetID) and '
                                                   'attribute_not_exists(RelationshipKey)')
        except ConditionError as ce:
            ce.args = ce.args + (
                "TargetID {0} with RelationshipKey {1} already exists in Object Relationship table".format(
                    dict_items["target-id"], dict_items["relationship-key"]),)
            raise

    def update_item(self, dict_items, condition=None, expression_values=None):
        if not condition or 'attribute_exists(TargetID)' not in condition:
            raise Exception("Update must contain a condition with attribute_exists(TargetID)")
        super().put_item(dict_items, condition, expression_values)

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def get_all_items(self, max_items=None, pagination_token=None, **kwargs):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OBJECT_RELATIONSHIP_DYNAMODB_TABLE'] = 'feature-sxr-DataLake-ObjectRelationshipTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")

    item = \
        {
            "target-object": {
                "object-id": "testObject",
                "collection-id": "allNews",
                "version-number": 2
            },
            "related-object": {
                "object-id": "relatedObject",
                "collection-id": "caseLaw",
                "version-number": 1
            },
            "relationship-id": "companies"
        }

    target_object = item["target-object"]
    related_object = item["related-object"]

    target_id = ObjectRelationshipTable.generate_object_composite_id(target_object["object-id"],
                                                                     target_object["collection-id"],
                                                                     target_object["version-number"])

    rel_key = ObjectRelationshipTable.generate_relationship_key(item["relationship-id"], 1)

    related_id = ObjectRelationshipTable.generate_object_composite_id(related_object["object-id"],
                                                                      related_object["collection-id"],
                                                                      related_object["version-number"])

    test_key_dict = \
        {
            "target-id": target_id,
            "relationship-key": rel_key
        }

    test_attrs_dict = \
        {
            "related-id": related_id,
            "object-relationship-state": "Created",
            "object-relationship-timestamp": curr_timestamp
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    ort = ObjectRelationshipTable()
    ort.put_item(test_dict)
    print(json.dumps(ort.get_item(test_key_dict), indent=4))
    try:
        print("All items:")
        print(json.dumps(ort.get_all_items(), indent=4))
    except Exception as e:
        print('get_all_items() not implemented.')
    print("Composite Key Query Result:")
    print(json.dumps(ort.query_items(
        KeyConditionExpression='TargetID = :target_id and RelationshipKey = :relationship_key',
        ExpressionAttributeValues={":target_id": {"S": target_id},
                                   ":relationship_key": {"S": rel_key}}), indent=4))
    print("Related Object Query Result:")
    print(json.dumps(ort.query_items(
        index="object-relationship-by-related-id-index",
        KeyConditionExpression='RelatedID = :rid',
        ExpressionAttributeValues={":rid": {"S": related_id}}), indent=4))

    ort.delete_item(test_key_dict)
