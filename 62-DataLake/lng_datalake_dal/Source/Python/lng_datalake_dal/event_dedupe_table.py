import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

class EventDedupeTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("EVENT_DEDUPE_DYNAMODB_TABLE")
        super().__init__(table_name)

    def put_item(self, dict_items):
        super().put_item(dict_items,
                         condition='attribute_not_exists(MessageID) and attribute_not_exists(StreamArn)',
                         ReturnValues="NONE")

    def get_all_items(self):
        raise NotImplementedError

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-EventStoreTable'
    session.set_session()

    test_key_dict = \
        {
            "event-id": "test-event-id-123",
            "request-time": "2017-11-10T09:40:59.592Z"
        }
    test_attrs_dict = \
        {
            "collection-id": "507",
            "asset-id": 62,
            "owner-id": 100,
            "event-name": "Collection::Create",
            "item-name": "Test Collection",
            "event-version": 1,
            "stage": "LATEST",
            "old-object-versions-to-keep": 3
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    EventStoreTable().put_item(test_dict)
    print(EventStoreTable().get_item(test_key_dict))
    EventStoreTable().delete_item(test_key_dict)
