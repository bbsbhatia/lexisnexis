import os

from lng_datalake_dal.table import Table, TableCache

__author__ = "Maen Nanaa, Aaron Belvo"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"


class CounterTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("COUNTER_DYNAMODB_TABLE")
        super().__init__(table_name)

    def delete_item(self, dict_key, condition=None):
        raise NotImplementedError

    def put_item(self, dict_items, condition=None):
        raise NotImplementedError

    def get_item(self, dict_key):
        raise NotImplementedError

    def get_all_items(self):
        raise NotImplementedError

    def query_items(self, index=None):
        raise NotImplementedError

    def update_item(self, dict_items, condition=None):
        raise NotImplementedError

    def update_counter(self, namespace_key):
        return super().update_counter({'Namespace': {"S": namespace_key}}, 'AtomicCounter')

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COUNTER_DYNAMODB_TABLE'] = 'feature-DataLake-CounterTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    ct = CounterTable()

    print(ct.update_counter('test'))
