import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

class EventStoreBackendTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("EVENT_STORE_BACKEND_DYNAMODB_TABLE")
        interface_schema_name = 'EventStoreInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items,
                             condition='attribute_not_exists(EventID) and attribute_not_exists(RequestTime)')
        except ConditionError as e:
            e.args = e.args + ("EventID {0} already exists for RequestTime {1} in EventStoreBackend table"
                               .format(dict_items["event-id"], dict_items["request-time"]),)
            raise

    def get_all_items(self):
        raise NotImplementedError

    def query_items(self, index=None):
        raise NotImplementedError

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['EVENT_STORE_BACKEND_DYNAMODB_TABLE'] = 'feature-mas-DataLake-EventStoreBackendTable'
    session.set_session()

    test_key_dict = \
        {
            "event-id": "test-event-id-123",
            "request-time": "2017-11-10T09:40:59.592Z"
        }
    test_attrs_dict = \
        {
            "collection-id": "507",
            "asset-id": 62,
            "owner-id": 100,
            "event-name": "Collection::Create",
            "item-name": "Test Collection",
            "event-version": 1,
            "stage": "LATEST",
            "old-object-versions-to-keep": 3
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    EventStoreBackendTable().put_item(test_dict)
    print(EventStoreBackendTable().get_item(test_key_dict))
    EventStoreBackendTable().delete_item(test_key_dict)
