import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "John Morelock"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"


class CollectionBlockerTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("COLLECTION_BLOCKER_DYNAMODB_TABLE")
        interface_schema_name = 'CollectionBlockerInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items, condition=None):
        try:
            super().put_item(dict_items,
                             condition='attribute_not_exists(CollectionID) and attribute_not_exists(RequestTime)')
        except ConditionError as e:
            e.args = e.args + ("CollectionID {0} already exists in Collection Blocker table"
                               .format(dict_items["collection-id"]),)
            raise

    def update_item(self, dict_items, condition=None, expression_values=None):
        if not condition or 'attribute_exists(CollectionID)' not in condition:
            raise Exception("Update must contain a condition with attribute_exists(CollectionID)")
        super().put_item(dict_items, condition, expression_values)

    def update_counter(self, counter_attribute_name):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session
    import json
    import logging

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = 'feature-jek-DataLake-CollectionBlockerTable'

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    collection_id = 'ingestion_collection_id_test'
    test_key_dict = \
        {
            'collection-id': collection_id,
            'request-time': '2019-02-26T09:19:59.592Z'
        }
    test_attrs_dict = \
        {
            'request-id': '0110f4da-314a-11e9-91bc-09464e75f09c',
            'event-name': 'Object::Update',
            'pending-expiration-epoch': 1561190831
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    cbt = CollectionBlockerTable()
    cbt.put_item(test_dict)
    logger.debug(json.dumps(cbt.get_item(test_key_dict), indent=4))
    logger.debug(json.dumps(cbt.query_items(KeyConditionExpression='CollectionID = :collection_id',
                                            ExpressionAttributeValues={
                                                ":collection_id": {"S": collection_id}}),
                            indent=4))


    test_dict['event-status'] = 'Locked'
    cbt.update_item(test_dict, condition='attribute_exists(CollectionID) and attribute_not_exists(EventStatus)')

    logger.debug(json.dumps(cbt.query_items(KeyConditionExpression='CollectionID = :collection_id',
                                            ExpressionAttributeValues={
                                                ":collection_id": {"S": collection_id}}),
                            indent=4))

    # Try to lock again - should get ConditionError
    try:
        logger.debug(json.dumps(cbt.update_item(test_dict, condition='attribute_exists(CollectionID) and attribute_not_exists(EventStatus)'),
                                indent=4))
    except ConditionError as ex:
        # This is expected
        pass
    except Exception as ex:
        logger.exception(ex)

    # Remove Lock for Retry - should not get condition error
    try:
        test_dict['event-status'] = 'Retry-10-NA'
        logger.debug(json.dumps(cbt.update_item(test_dict,
                                                condition='attribute_exists(CollectionID) and EventStatus = :es',
                                                expression_values={':es': 'Locked'}),
                     indent=4))
    except Exception as ex:
        logger.exception(ex)

    logger.debug(json.dumps(cbt.query_items(KeyConditionExpression='CollectionID = :collection_id',
                                            ExpressionAttributeValues={
                                                ":collection_id": {"S": collection_id}}),
                            indent=4))

    cbt.delete_item(test_key_dict)
