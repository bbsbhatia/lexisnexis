import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class ChangesetCollectionTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("CHANGESET_COLLECTION_DYNAMODB_TABLE")
        interface_schema_name = 'ChangesetCollectionInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items,
                             condition='attribute_not_exists(ChangesetID) '
                                       'and attribute_not_exists(CollectionID)')
        except ConditionError as e:
            e.args = e.args + ("ChangesetID {0} already exists for CollectionID {1} in Changeset table"
                               .format(dict_items["changeset-id"], dict_items["collection-id"]),)
            raise

    def update_item(self, dict_items, condition=None, expression_values=None):
        if not condition or 'attribute_exists(ChangesetID)' not in condition:
            raise Exception("Update must contain a condition with attribute_exists(ChangesetID)")
        super().put_item(dict_items, condition, expression_values)

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, changeset_id: str, collection_id: str, pending_expiration_epoch: str,
                        counter_attributes: dict,
                        counter_only: bool = True, condition: bool = None):
        return super().update_counters({"ChangesetID": {"S": changeset_id}, "CollectionID": {"S": collection_id}},
                                       counter_attributes, counter_only, condition,
                                       {"PendingExpirationEpoch": {"N": pending_expiration_epoch}})

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    ASSET_GROUP = 'feature-kgg'
    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CHANGESET_COLLECTION_DYNAMODB_TABLE'] = '{}-DataLake-ChangesetCollectionTable'.format(ASSET_GROUP)

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().isoformat()

    test_key_dict = \
        {
            "changeset-id": "123",
            "collection-id": "456"
        }
    test_attrs_dict = \
        {
            "objects-processed": 0,
            "pending-expiration-epoch": 120

        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    cct = ChangesetCollectionTable()
    cct.put_item(test_dict)

    print(json.dumps(cct.get_item(test_key_dict), indent=4))
    test_dict['pending-expiration-epoch'] = 122
    cct.update_item(test_dict, condition='attribute_exists(ChangesetID)')

    print(json.dumps(cct.get_item(test_key_dict), indent=4))

    print(json.dumps(cct.get_all_items(), indent=4))
    cct.update_counters(changeset_id=test_key_dict['changeset-id'], collection_id=test_key_dict['collection-id'],
                        pending_expiration_epoch='123',
                        counter_attributes={'ObjectsProcessed': 20}, condition=None)
    print(json.dumps(cct.query_items(KeyConditionExpression='ChangesetID = :changeset_id',
                                     ExpressionAttributeValues={":changeset_id": {"S": "123"}}), indent=4))

    # cct.delete_item(test_key_dict)
