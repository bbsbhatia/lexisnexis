import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class ChangesetTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("CHANGESET_DYNAMODB_TABLE")
        interface_schema_name = 'ChangesetInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(ChangesetID)')
        except ConditionError as e:
            e.args = e.args + ("ChangesetID {0} already exists in Changeset table"
                               .format(dict_items["changeset-id"]),)
            raise

    def update_item(self, dict_items, condition=None, expression_values=None):
        if not condition or 'attribute_exists(ChangesetID)' not in condition:
            raise Exception("Update must contain a condition with attribute_exists(ChangesetID)")
        super().put_item(dict_items, condition, expression_values)

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'
    os.environ['CHANGESET_DYNAMODB_TABLE'] = 'feature-maen-DataLake-ChangesetTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().isoformat()

    test_key_dict = \
        {
            "changeset-id": "123"
        }
    test_attrs_dict = \
        {
            "changeset-state": "Opened",
            "changeset-timestamp": "2019-06-03T16:09:33.861Z",
            "owner-id": 1,
            "pending-expiration-epoch": 1573909785,
            "description": "test description",
            "event-ids": ["event-id"]
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    ct = ChangesetTable()
    ct.put_item(test_dict)

    print(json.dumps(ct.get_item(test_key_dict), indent=4))
    test_dict['changeset-state'] = 'Closed'
    ct.update_item(test_dict, condition='attribute_exists(ChangesetID)')

    print(json.dumps(ct.get_item(test_key_dict), indent=4))

    print(json.dumps(ct.get_all_items(), indent=4))
    print(json.dumps(ct.query_items(KeyConditionExpression='ChangesetID = :changeset_id',
                                    ExpressionAttributeValues={":changeset_id": {"S": "123"}}), indent=4))

    ct.delete_item(test_key_dict)
