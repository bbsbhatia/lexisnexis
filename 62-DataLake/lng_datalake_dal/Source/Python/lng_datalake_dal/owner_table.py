import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache


class OwnerTable(Table, metaclass=TableCache):

    def __init__(self):
        table_name = os.getenv("OWNER_DYNAMODB_TABLE")
        interface_schema_name = 'OwnerInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(OwnerID)')
        except ConditionError as e:
            e.args = e.args + ("OwnerID {0} already exists in Owner table".format(dict_items["owner-id"]),)
            raise

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def update_item(self, dict_items, condition=None, expression_values=None):
        try:
            super().put_item(dict_items, condition='attribute_exists(OwnerID)')
        except ConditionError as e:
            e.args = e.args + ("OwnerID {0} does not exist in Owner table"
                               .format(dict_items["owner-id"]),)
            raise

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-OwnerTable'
    session.set_session()

    test_key_dict = \
        {
            "owner-id": 919,
            "api-key": "TestAPIKeyTestAPIKeyTestAPIKeyTestAPIKey"
        }
    test_attrs_dict = \
        {
            "owner-name": "Wormhole Team",
            "email-distribution-list": ["email1@lexisnexis.com", "email2@lexisnexis.com"]

        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    OwnerTable().put_item(test_dict)
    del test_key_dict['api-key']
    print(OwnerTable().get_item(test_key_dict))
    OwnerTable().delete_item(test_key_dict)
