import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class RepublishCollectionTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("REPUBLISH_COLLECTION_DYNAMODB_TABLE")
        interface_schema_name = 'RepublishCollectionInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def update_item(self, dict_items, condition=None, expression_values=None):
        if not condition or 'attribute_exists(RepublishID)' not in condition:
            raise Exception("Update must contain a condition with attribute_exists(RepublishID)")
        super().put_item(dict_items, condition, expression_values)

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'
    os.environ['REPUBLISH_COLLECTION_DYNAMODB_TABLE'] = 'feature-jek-DataLake-RepublishCollectionTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().isoformat()

    test_key_dict = \
        {
            "republish-id": "123",
            "collection-id": "456"
        }
    test_attrs_dict = \
        {
            "subscription-id": 99,
            "republish-state": "Processing",
            "end-filter-timestamp": "2019-07-03T16:09:33.861Z",
            "start-filter-timestamp": "2019-08-03T16:09:33.861Z",
            "last-processed-page": "xyzl5a4st1"

        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    print(json.dumps(test_dict))

    ct = RepublishCollectionTable()
    ct.put_item(test_dict)
    print(json.dumps(ct.get_item(test_key_dict), indent=4))
    print(json.dumps(ct.get_all_items(), indent=4))
    print(json.dumps(ct.query_items(KeyConditionExpression='RepublishID = :republish_id',
                                    ExpressionAttributeValues={":republish_id": {"S": "123"}}), indent=4))

    ct.delete_item(test_key_dict)
