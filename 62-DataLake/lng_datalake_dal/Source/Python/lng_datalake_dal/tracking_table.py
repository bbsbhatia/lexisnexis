import logging
import os

from lng_datalake_dal.table import Table, TableCache

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))


class TrackingTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("TRACKING_DYNAMODB_TABLE")
        interface_schema_name = "TrackingInterfaceSchema.json"
        super().__init__(table_name, interface_schema_name)

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session
    from time import sleep
    from datetime import datetime

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["TRACKING_DYNAMODB_TABLE"] = "feature-DataLake-TrackingTable"

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL TEST RUN]")

    session.set_session()

    table = TrackingTable()
    dt = datetime.now().isoformat()
    mock_item = {
        "tracking-id": "123_collection",
        "request-time": dt,
        "event-id": "abc"
    }
    logger.debug("[Testing PUT item]")
    table.put_item(mock_item)
    sleep(2)
    logger.debug("[Testing QUERY event-id]")
    items = table.query_items(IndexName="event-id-index",
                              KeyConditionExpression="EventID = :event_id",
                              ExpressionAttributeValues={":event_id": {"S": "abc"}})
    logger.debug(items)
    sleep(2)
    logger.debug("[Testing DELETE item]")
    mock_key = {"tracking-id": "123_collection", "request-time": dt}
    table.delete_item(mock_key)
    sleep(2)
    logger.debug("[DONE]")
