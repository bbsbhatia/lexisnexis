import logging
import os

from botocore.exceptions import ClientError
from lng_aws_clients import dynamodb

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))


class Transaction:
    """
    DynamoDB will reject the entire TransactWriteItems request if any of the following is true:
    *  A table in the TransactWriteItems request does not exist.
    *  A table in the TransactWriteItems request is on a different account or region.
    *  Operations contain item schema violations.
    *  More than one write operation (UpdateItem, PutItem, DeleteItem) operates on the same item.
    *  More than one check operation operates on the same item.
    *  The number of operations sent in the TransactWriteItems request is 0 or greater than 10.
    *  A TransactWriteItems request exceeds the maximum 4 MB request size.
    *  Any operation in the TransactWriteItems request would cause an item to become larger than 400KB.
    """

    def __init__(self):
        self.__transaction_items = []

    def add_item_to_transaction_list(self, item):
        if len(self.__transaction_items) > 9:
            raise Exception("Too many items attempted to be added to a transaction::"
                            "Max is 10 current length is {}".format(len(self.__transaction_items)))
        self.__transaction_items.append(item)

    def commit_transaction(self):
        try:
            dynamodb.get_client().transact_write_items(TransactItems=self.__transaction_items)
            self.__transaction_items = []
        except ClientError as ce:
            raise ce
        except Exception as e:
            logger.error('Failed transaction: {}'.format(e))
            raise e


if __name__ == '__main__':
    from lng_aws_clients import session
    import logging
    from lng_datalake_dal.collection_table import CollectionTable
    import json

    os.environ['AWS_PROFILE'] = 'dev-doa'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-DataLake-CollectionTable'

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)
    session.set_session()

    test_key_dict = \
        {
            'collection-id': '293'
        }
    test_attrs_dict = \
        {
            "asset-id": 62,
            "classification-type": "Test",
            "description": "My first collection test",
            "owner-id": 19,
            "collection-state": "Collection::Suspended",
            "collection-timestamp": "12345",
            "old-object-versions-to-keep": 3,
            "catalog-ids": ["catalog-01", "catalog-02"]
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    first_transaction = Transaction()
    CollectionTable().put_transaction_item(test_dict, first_transaction, condition="CollectionState=:state",
                                           expression_values={":state": {"S": "Collection::Suspended"}})
    first_transaction.commit_transaction()
    logger.debug('Successfully inserted my item into the CollectionTable')
    logger.debug(json.dumps(CollectionTable().get_item(test_key_dict), indent=4))
    second_transaction = Transaction()
    CollectionTable().delete_transaction_item(test_key_dict, second_transaction)
    second_transaction.commit_transaction()
    logger.debug('Successfully deleted my item from the CollectionTable')

