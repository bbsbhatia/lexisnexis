import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Doug Heitkamp"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class RelationshipOwnerTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("RELATIONSHIP_OWNER_DYNAMODB_TABLE")
        interface_schema_name = 'RelationshipOwnerInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(RelationshipID)')
        except ConditionError as e:
            e.args = e.args + ("RelationshipID {0} already exists in Relationship Owner table"
                               .format(dict_items["relationship-id"]),)
            raise

    def update_item(self, dict_items, condition=None, expression_values=None):
        super().put_item(dict_items, condition, expression_values)

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'
    os.environ['RELATIONSHIP_OWNER_DYNAMODB_TABLE'] = 'feature-djh-DataLake-RelationshipOwnerTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().isoformat()

    test_key_dict = \
        {
            "relationship-id": "DALsRel"
        }
    test_attrs_dict = \
        {
            "description": "This is a DAL test relationship",
            "owner-id": 101,
            "relationship-type": "Metadata",
            "relationship-state": "Created"
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    rot = RelationshipOwnerTable()
    rot.put_item(test_dict)
    print(json.dumps(rot.get_item(test_key_dict), indent=4))
    print("All items:")
    print(json.dumps(rot.get_all_items(), indent=4))
    print("Relationship Query Result:")
    print(json.dumps(rot.query_items(KeyConditionExpression='RelationshipID = :relationship_id',
                                     ExpressionAttributeValues={":relationship_id": {"S": "DALsRel"}}), indent=4))
    print("Owner Query Result:")
    print(json.dumps(rot.query_items(index="relationship-owner-by-owner-id-index",
                                     KeyConditionExpression='OwnerID = :owner_id',
                                     ExpressionAttributeValues={":owner_id": {"N": "101"}}), indent=4))
    rot.delete_item(test_key_dict)
