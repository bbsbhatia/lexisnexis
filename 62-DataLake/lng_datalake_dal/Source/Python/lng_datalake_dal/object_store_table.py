import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache, Transaction

__author__ = "Maen Nanaa, Aaron Belvo"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"


class ObjectStoreTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("OBJECT_STORE_DYNAMODB_TABLE")
        interface_schema_name = 'ObjectStoreInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items,
                             condition='(attribute_not_exists(ObjectID) and attribute_not_'
                                       'exists(CollectionID)) or ObjectState =:state',
                             expression_values={':state': 'Removed'})
        except ConditionError as e:
            e.args = e.args + ("ObjectID {0} already exists for CollectionID {1} in ObjectStore table"
                               .format(dict_items["object-id"], dict_items["collection-id"]),)
            raise

    def update_item(self, dict_items):

        try:
            super().put_item(dict_items,
                             condition='VersionTimestamp < :v AND VersionNumber = :n',
                             expression_values={
                                 ':v': dict_items['version-timestamp'],
                                 ':n': dict_items['version-number'] - 1
                             })

        except KeyError as e:
            e.args = e.args + ("version-number and version-timestamp are required attributes for Object Store Table"
                               " item comparison. Missing required attribute: {}".format(e),)
            raise

        except ConditionError as e:
            e.args = e.args + ("ObjectID {0} CollectionID {1} in ObjectStore table has an identical or more recent "
                               "version."
                               .format(dict_items["object-id"], dict_items["collection-id"]),)
            raise

    def put_transaction_item(self, dict_items: dict, transaction: Transaction):
        return super().put_transaction_item(dict_items, transaction,
                                            condition='(attribute_not_exists(ObjectID) and attribute_not_'
                                                      'exists(CollectionID)) or ObjectState =:state',
                                            expression_values={':state': {'S': 'Removed'}})

    def update_transaction_item(self, dict_items: dict, transaction: Transaction):
        try:
            return super().put_transaction_item(dict_items, transaction,
                                                condition='VersionTimestamp < :v AND '
                                                          'VersionNumber = :n',
                                                expression_values={
                                                    ':v': {'S': dict_items['version-timestamp']},
                                                    ':n': {'N': str(dict_items['version-number'] - 1)}
                                                }
                                                )
        except KeyError as e:
            e.args = e.args + ("version-number and version-timestamp are required attributes for Object Store Table"
                               " item comparison. Missing required attribute: {}".format(e),)
            raise

    def update_counter(self, counter_attribute_name):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session
    import json
    import logging

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-ObjectStoreTable'

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    test_key_dict = \
        {
            'object-id': 'ABCDefgh',
            'collection-id': '2'
        }
    test_attrs_dict = \
        {
            'bucket-name': 'aaron',
            'object-key': 'key1',
            'object-state': 'Object::Created',
            'content-type': 'text/plain',
            'raw-content-length': 10
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    ost = ObjectStoreTable()
    ost.put_item(test_dict)
    logger.debug(json.dumps(ost.get_item(test_key_dict), indent=4))
    logger.debug(json.dumps(ost.query_items(KeyConditionExpression='ObjectID = :object_id',
                                            ExpressionAttributeValues={":object_id": {"S": "ABCDefgh"}}), indent=4))
    ost.delete_item(test_key_dict)
