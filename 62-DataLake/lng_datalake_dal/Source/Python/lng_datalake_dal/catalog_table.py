import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"


class CatalogTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("CATALOG_DYNAMODB_TABLE")
        interface_schema_name = 'CatalogInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(CatalogID)')
        except ConditionError as e:
            e.args = e.args + ("CatalogID {0} already exists in Catalog table"
                               .format(dict_items["catalog-id"]),)
            raise

    def update_item(self, dict_items, condition=None):
        super().put_item(dict_items, condition)

    def update_counter(self, primary_key, counter_attribute_name=None):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-DataLake-CatalogTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().isoformat()

    test_key_dict = \
        {
            "catalog-id": "catalog-02"
        }
    test_attrs_dict = \
        {
            "description": "My first catalog test",
            "owner-id": 19,
            "catalog-timestamp": curr_timestamp
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    ct = CatalogTable()
    ct.put_item(test_dict)
    print(json.dumps(ct.get_item(test_key_dict), indent=4))
    print(json.dumps(ct.get_all_items(), indent=4))
    print(json.dumps(ct.query_items(KeyConditionExpression='CatalogID = :catalog_id',
                                    ExpressionAttributeValues={":catalog_id": {"S": "catalog-02"}}), indent=4))
    ct.delete_item(test_key_dict)
