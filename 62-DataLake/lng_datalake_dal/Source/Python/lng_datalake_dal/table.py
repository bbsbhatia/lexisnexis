import logging
import os
import time

import fastjsonschema
import orjson
from botocore.exceptions import ClientError
from lng_aws_clients import dynamodb

import lng_datalake_dal.dynamo_mapper as mapper
import lng_datalake_dal.exceptions
from lng_datalake_dal.transaction import Transaction

__author__ = "Maen Nanaa, Aaron Belvo, Jose Molinet"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

my_location = os.path.dirname(__file__)


class TableCache(type):
    _table = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._table:
            cls._table[cls] = super().__call__(*args, **kwargs)
        return cls._table[cls]

    @classmethod
    def clear(cls):
        cls._table.clear()


class Table:
    def __init__(self, table_name, interface_schema_name=None):
        self.table_schema = None
        self.table_name = table_name
        self.pagination_token = None

        self.required_schema_keys = []
        self.optional_schema_keys = []
        if interface_schema_name is not None:
            self.__schema_path = os.path.normpath(os.path.join(my_location, 'Schemas', interface_schema_name))
            self.table_schema = self._load_json_schema(self.__schema_path)
            self.schema_validator = self._generate_schema_validator(self.table_schema)
            if "required" in self.table_schema:
                self.required_schema_keys = self.table_schema["required"]
            # if Required does not exist Optional will contain every property
            all_schema_keys = list(self.table_schema["properties"].keys())
            self.optional_schema_keys = list(set(all_schema_keys) - set(self.required_schema_keys))

    def get_required_schema_keys(self):
        return self.required_schema_keys

    def get_optional_schema_keys(self):
        return self.optional_schema_keys

    def put_item(self, dict_items, condition=None, expression_values=None, **kwargs):
        """
        Add new item or replace an old item if the item already exists in table.
        :param dict_items:(dict)The item primary key.
        :param condition:Conditional Expression.A condition that must be satisfied in order for a conditional
        Put to succeed.
        :param expression_values: values that can be substituted in the conditional expression
        :return: None
        """
        if self.table_schema is not None:
            # validate the input
            self._validate_against_interface_schema(dict_items)
        dynamodb_item = mapper.transform_to_dynamo_dict(dict_items, read_only=False)

        # do the put
        try:
            if condition is None:
                dynamodb.get_client().put_item(TableName=self.table_name, Item=dynamodb_item, **kwargs)
            elif expression_values is None:
                dynamodb.get_client().put_item(TableName=self.table_name, Item=dynamodb_item,
                                               ConditionExpression=condition, **kwargs)
            else:
                dynamodb.get_client().put_item(TableName=self.table_name, Item=dynamodb_item,
                                               ConditionExpression=condition,
                                               ExpressionAttributeValues=mapper.transform_to_dynamo_dict(
                                                   expression_values), **kwargs)

        # except dynamodb.get_client().exceptions.ConditionalCheckFailedException:
        except ClientError as e:
            error_code = e.response['Error']['Code']
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            if error_code == "ConditionalCheckFailedException":
                raise lng_datalake_dal.exceptions.ConditionError(
                    "Conditional check failed when attempting to put item into {0}::{1}".format(self.table_name, e))

            else:
                logger.error("Unable to put item into {0}::{1}".format(self.table_name, e))
                raise

    def put_transaction_item(self, dict_items, transaction: Transaction, condition=None, expression_values=None):
        """
        Add new item or replace an old item if the item already exists in table.
        :param dict_items:(dict)The item primary key.
        :param transaction: the transaction to add the item to
        :param condition:Conditional Expression. A condition that must be satisfied in order for a conditional
        Put to succeed.
        :param expression_values:Expression Attribute Values for the attributes specified in the condition expression.
        :return:dict that can be added to a transact_write_items list.
        """
        if self.table_schema is not None:
            # validate the input
            self._validate_against_interface_schema(dict_items)
        dynamodb_item = mapper.transform_to_dynamo_dict(dict_items, read_only=False)

        if condition is None:
            transaction_item = {
                'Put': {
                    'TableName': self.table_name,
                    'Item': dynamodb_item
                }
            }
        elif expression_values is None:
            transaction_item = {
                'Put': {
                    'TableName': self.table_name,
                    'Item': dynamodb_item,
                    'ConditionExpression': condition
                }
            }
        else:
            transaction_item = {
                'Put': {
                    'TableName': self.table_name,
                    'Item': dynamodb_item,
                    'ConditionExpression': condition,
                    'ExpressionAttributeValues': expression_values
                }
            }

        try:
            transaction.add_item_to_transaction_list(transaction_item)
        except Exception as e:
            raise e

    def delete_item(self, dict_key, condition=None):
        """
        Delete item by primary key.
        :param dict_key: (dict) the primary key of the item to be deleted.
        :param condition:Conditional Expression.A condition that must be satisfied in order for a conditional
        Delete to succeed.
        :return:None
        """
        try:
            if condition is None:
                dynamodb.get_client().delete_item(TableName=self.table_name,
                                                  Key=mapper.transform_to_dynamo_dict(dict_key))
            else:
                dynamodb.get_client().delete_item(TableName=self.table_name,
                                                  Key=mapper.transform_to_dynamo_dict(dict_key),
                                                  ConditionExpression=condition)

        except ClientError as e:
            error_code = e.response['Error']['Code']
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            if error_code == "ConditionalCheckFailedException":
                raise lng_datalake_dal.exceptions.ConditionError(
                    "Conditional check failed when attempting to delete item from {0}::{1}".format(self.table_name, e))
            else:
                logger.error("Unable to delete item from {0}::{1}".format(self.table_name, e))
                raise

    def delete_transaction_item(self, dict_key, transaction: Transaction, condition=None, expression_values=None):
        """
        Delete item by primary key.
        :param dict_key: (dict) the primary key of the item to be deleted.
        :param transaction: the transaction to add the item to
        :param condition:Conditional Expression. A condition that must be satisfied in order for a conditional
        Delete to succeed.
        :param expression_values:Expression Attribute Values for the attributes specified in the condition expression.
        :return:dict that can be added to a transact_write_items list.
        """

        dynamodb_item = mapper.transform_to_dynamo_dict(dict_key)
        if condition is None:
            transaction_item = {
                'Delete': {
                    'TableName': self.table_name,
                    'Key': dynamodb_item
                }
            }
        elif expression_values is None:
            transaction_item = {
                'Delete': {
                    'TableName': self.table_name,
                    'Key': dynamodb_item,
                    'ConditionExpression': condition
                }
            }
        else:
            transaction_item = {
                'Delete': {
                    'TableName': self.table_name,
                    'Key': dynamodb_item,
                    'ConditionExpression': condition,
                    'ExpressionAttributeValues': expression_values
                }
            }
        try:
            transaction.add_item_to_transaction_list(transaction_item)
        except Exception as e:
            raise e

    def get_item(self, dict_key, **kwargs):
        """
        Get item by primary key.
        :param dict_key: (dict)Primary key of the item to be retrieved.
        :return:dict represents the item found. Empty dict if no item found.
        """
        try:
            response = dynamodb.get_client().get_item(TableName=self.table_name,
                                                      Key=mapper.transform_to_dynamo_dict(dict_key),
                                                      **kwargs)
            try:
                item = response['Item']
            except KeyError:
                return {}

            client_response = mapper.transform_to_client_dict(item)
            return client_response
        except ClientError as e:
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            logger.error("Unable to get item from {0}::{1}".format(self.table_name, e))
            raise

    def batch_get_items(self, key, values, max_items_per_batch=100, max_retries=2):
        """
        Get a batch of items by primary key. DynamoDB BatchGetItem can retrieve up to 100 items per call
        :param key: primary key
        :param values: primary key's value of the items to get ['value1', 'value2', ...]
        :param max_items_per_batch: max number of items to get with one call to BatchGetItem.
        Depend on the item size and table read capacity.
        :param max_retries: maximum number of retries for a single batch_get_item call.
        :return: list of the valid collection items found
        """
        if max_items_per_batch > 100:
            error_response = {'Error': {'Code': 'Validation Error',
                                        'Message': 'Member must have length less than or equal to 100'}}
            raise ClientError(error_response, "batch_get_item")

        items = []
        retries = 0
        transformed_keys = [mapper.transform_to_dynamo_dict({key: val}) for val in values[:max_items_per_batch]]
        values = values[max_items_per_batch:]
        while transformed_keys:
            try:
                response = dynamodb.get_client().batch_get_item(RequestItems={
                    self.table_name: {
                        'Keys': transformed_keys
                    }
                })
                for item in response["Responses"][self.table_name]:
                    items.append(mapper.transform_to_client_dict(item))

                # In case of Unprocessed Keys or failure to get a response due to
                # either ProvisionedThroughputExceededException or ThrottlingException,
                # we need to retry the batch_get_item call with exponential sleep time.
                # Else, we can just continue to get the next set of values and the continue statement
                # to kick off the next loop without retries or sleep.

                # continue for rest of the items
                if not response["UnprocessedKeys"]:
                    retries = 0
                    transformed_keys = [mapper.transform_to_dynamo_dict({key: val}) for val in
                                        values[:max_items_per_batch]]
                    values = values[max_items_per_batch:]
                    continue

                # If the response has UnprocessedKeys, we assign these keys to the transformed keys
                # since they are already in dynamodb format. Then we fill the remaining space in max_items_per_batch
                # by getting more keys from values, transforming them, adding to the transformed keys and then
                # removing these keys from values.
                transformed_keys = response["UnprocessedKeys"][self.table_name]['Keys']
                batch_size_diff = max_items_per_batch - len(transformed_keys)
                transformed_keys.extend(
                    [mapper.transform_to_dynamo_dict({key: val}) for val in values[:batch_size_diff]])
                values = values[batch_size_diff:]

            except ClientError as e:
                if e.response['Error']['Code'] not in ['ProvisionedThroughputExceededException', 'ThrottlingException']:
                    logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
                    logger.error("Unable to get items from {0}::{1}".format(self.table_name, e))
                    raise
            except Exception as ex:
                logger.error("General Exception occurred while accessing {0}::{1}".format(self.table_name, ex))
                raise

            # Exception if the number of retries has reached the max_retries limit
            if retries >= max_retries:
                raise Exception("Maximum limit of retries reached. max_retries={}".format(max_retries))

            # exponential backoff if returned unprocessed keys or throughput exception
            retries += 1
            time.sleep(2 ** retries * 0.1)
        return items

    def batch_write_items(self, items_to_put: list = None, items_to_delete: list = None,
                          max_items_per_batch: int = 25, max_retries: int = 2) -> dict:
        """
        Write a batch of items using DynamoDB BatchWriteItem, which can write up to 25 items per call

        :param items_to_put: List of items for PUT operations
        :param items_to_delete: List of item keys for DELETE operations
        :param max_items_per_batch: Maximum number of items to write with one call to BatchWriteItem.
        :param max_retries: Maximum number of retries for a single BatchWriteItem call.
        :return: Dict of unprocessed items with two lists, one with the item keys to delete
            and the other with items to put.
        Empty dict if all items were written
        """
        items_to_put = self._convert_to_list(items_to_put)
        items_to_delete = self._convert_to_list(items_to_delete)

        if max_items_per_batch > 25:
            raise ClientError({'Error': {'Code': 'Validation Error',
                                         'Message': 'The maximum number of items to write per batch is 25'}},
                              "batch_write_item")
        retries = 0
        dynamodb_dict_items = self._get_next_batch(max_items_per_batch, items_to_put, items_to_delete)
        while dynamodb_dict_items and retries <= max_retries:
            # exponential backoff before retry if a throughput or throttling exception occurred in the iteration before
            if retries > 0:
                time.sleep(2 ** retries * 0.1)
            try:
                response = dynamodb.get_client().batch_write_item(RequestItems={self.table_name: dynamodb_dict_items})

                # In case of Unprocessed Items, the next loop iteration will retry the batch_write_item call
                # after an exponential sleep time.
                if response["UnprocessedItems"]:
                    unprocessed_items = response["UnprocessedItems"][self.table_name]
                    retries += 1
                else:
                    unprocessed_items = []
                    retries = 0

                dynamodb_dict_items = self._get_next_batch(max_items_per_batch, items_to_put, items_to_delete,
                                                           unprocessed_items)
            except ClientError as e:
                if e.response['Error']['Code'] not in ['ProvisionedThroughputExceededException', 'ThrottlingException']:
                    logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
                    logger.error("Unable to write items to {0}::{1}".format(self.table_name, e))
                    raise
                # In case of failure to get a response due to either ProvisionedThroughputExceededException
                # or ThrottlingException,the next loop iteration will retry the batch_write_item call
                # after an exponential sleep time.
                retries += 1
            except Exception as e:
                logger.error("General Exception occurred while accessing {0}::{1}".format(self.table_name, e))
                raise

        # return the unprocessed items after the number of retries has reached the max_retries limit,
        # or an empty list if all items were processed.
        return self._get_unprocessed_items(dynamodb_dict_items, items_to_put, items_to_delete)

    @staticmethod
    def _convert_to_list(value):
        return value if value is not None else []

    def _get_next_batch(self, max_items: int, items_to_put: list, items_to_delete: list,
                        unprocessed_items: list = None) -> list:
        """
        Create the next batch of dynamodb dict items using the unprocessed items, the items to put and the
        ones to delete, in that order.

        :param max_items: Maximum number of transformed items to include in the batch
        :param items_to_put: List of items for PUT operations
        :param items_to_delete: List of items for DELETE operations
        :param unprocessed_items: Dynamodb record items not processed in the previous call of batch_write_items
        :return: List of transformed items to dynamodb dict that will be used in dynamodb batch_write_item
        """
        unprocessed_items = self._convert_to_list(unprocessed_items)

        dynamodb_dict_items = []
        dynamodb_dict_items.extend(unprocessed_items)

        for items, operation, key in [(items_to_put, 'PutRequest', 'Item'), (items_to_delete, 'DeleteRequest', 'Key')]:
            if not items:
                continue
            while len(items) > 0 and len(dynamodb_dict_items) < max_items:
                item = items.pop()
                if self.table_schema and operation != 'DeleteRequest':
                    self._validate_against_interface_schema(item)
                    dynamodb_dict_items.append(
                        {operation: {key: mapper.transform_to_dynamo_dict(item, read_only=False)}})
                else:
                    dynamodb_dict_items.append({operation: {key: mapper.transform_to_dynamo_dict(item)}})

        return dynamodb_dict_items

    @staticmethod
    def _get_unprocessed_items(dynamodb_dict_items: list, items_to_put: list, items_to_delete: list) -> dict:
        """
        Return the items not processed by batch_write_items after the number of retries reached the limit,
        or an empty dict if all items were processed.

        :param dynamodb_dict_items: Last transformed batch of items that were not processed
        :param items_to_put: List of items for PUT operations that were not processed
        :param items_to_delete: List of items for DELETE operations that were not processed
        :return: Dict of unprocessed items with two lists, one with the items to delete and the other with items to put.
        """
        unprocessed_items = {}
        if items_to_put:
            unprocessed_items['PutRequest'] = items_to_put
        if items_to_delete:
            unprocessed_items['DeleteRequest'] = items_to_delete

        for item in dynamodb_dict_items:
            # item is a dictionary with one operation: 'PutRequest' or 'DeleteRequest'
            operation = list(item.keys())[0]
            key = 'Item' if operation == 'PutRequest' else 'Key'
            if operation not in unprocessed_items:
                unprocessed_items[operation] = []
            unprocessed_items[operation].append(mapper.transform_to_client_dict(item[operation][key]))

        return unprocessed_items

    def item_exists(self, dict_key, **kwargs):
        """
        Check existence of an item by primary key.
        :param dict_key: (dict)Primary key of the item to be retrieved.
        :return: True if item exists, False otherwise.
        """
        try:
            response = self.get_item(dict_key, **kwargs)

            # return True if the response is not "empty", otherwise return False
            return True if response else False
        except ClientError as e:
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            logger.error("Unable to check existence of item from {0}::{1}".format(self.table_name, e))
            raise

    def get_all_items(self, max_items=None, pagination_token=None, **kwargs):
        """
        get all items in the table
        :return: List of items in table.Return empty list if table is empty.
        """
        try:
            paginator = dynamodb.get_client().get_paginator('scan')
            if max_items and max_items > 0:
                response = paginator.paginate(TableName=self.table_name,
                                              PaginationConfig={'MaxItems': max_items, 'PageSize': max_items,
                                                                'StartingToken': pagination_token},
                                              **kwargs).build_full_result()
            else:
                response = paginator.paginate(TableName=self.table_name, **kwargs).build_full_result()

            # set the pagination token every time since this is a singleton class (we don't want another lambda
            # running in the same container to use previous value)
            self.pagination_token = response.get('NextToken')

            if 'Items' not in response:
                return []
            else:
                client_response = []
                items = response['Items']
                for item in items:
                    client_response.append(mapper.transform_to_client_dict(item))
            return client_response

        except ClientError as e:
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            logger.error("Unable to get all items from {0}::{1}".format(self.table_name, e))
            raise

    def query_items(self, index=None, max_items=None, pagination_token=None, **kwargs):
        """
        Query a table , a local secondary index or a global secondary index.
        :param index: optional string represents the name of the index to query.
        :param max_items: optional integer to limit the number of results.
        :param pagination_token: optional string for getting additional items when previously called with max_items.
        :param kwargs: KeyConditionExpression and ExpressionAttributeValues required.
        :return: List of items.Return empty list if no results found.
        """
        # Requires KeyConditionExpression and ExpressionAttributeValues to be in **kwargs
        try:
            paginator = dynamodb.get_client().get_paginator('query')
            if max_items and max_items > 0:
                if index is None:
                    response = paginator.paginate(TableName=self.table_name,
                                                  PaginationConfig={'MaxItems': max_items, 'PageSize': max_items,
                                                                    'StartingToken': pagination_token},
                                                  **kwargs).build_full_result()
                else:
                    response = paginator.paginate(TableName=self.table_name,
                                                  IndexName=index,
                                                  PaginationConfig={'MaxItems': max_items, 'PageSize': max_items,
                                                                    'StartingToken': pagination_token},
                                                  **kwargs).build_full_result()
                self.pagination_token = response.get('NextToken')
            else:
                if index is None:
                    response = paginator.paginate(TableName=self.table_name,
                                                  **kwargs).build_full_result()
                else:
                    response = paginator.paginate(TableName=self.table_name,
                                                  IndexName=index,
                                                  **kwargs).build_full_result()

            # set the pagination token every time since this is a singleton class (we don't want another lambda
            # running in the same container to use previous value)
            self.pagination_token = response.get('NextToken')

            if 'Items' not in response:
                return []
            else:
                client_response = []
                items = response['Items']
                for item in items:
                    client_response.append(mapper.transform_to_client_dict(item))
            return client_response

        except ClientError as e:
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            logger.error("Unable to query items from {0}::{1}".format(self.table_name, e))
            raise

    def count_query_items(self, index=None, **kwargs):
        """
        Query a table and return count of items, can pass a local secondary index or a global secondary index.
        :param index: optional string represents the name of the index to query.
        :param kwargs: KeyConditionExpression and ExpressionAttributeValues required.
        :return: Count of items
        """
        # Requires KeyConditionExpression and ExpressionAttributeValues to be in **kwargs
        try:
            paginator = dynamodb.get_client().get_paginator('query')
            if index is None:
                response = paginator.paginate(TableName=self.table_name,
                                              Select='COUNT',
                                              **kwargs).build_full_result()
            else:
                response = paginator.paginate(TableName=self.table_name,
                                              IndexName=index,
                                              Select='COUNT',
                                              **kwargs).build_full_result()
            return response['Count']

        except ClientError as e:
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            logger.error("Unable to query items from {0}::{1}".format(self.table_name, e))
            raise

    def update_item(self, dict_items, condition=None):
        """
        Update an old item in the table.
        :param dict_items: Primary key of the item to be updated.
        :param condition:Conditional Expression.A condition that must be satisfied in order for a conditional
        Update to succeed.
        :return:None.
        """
        self.put_item(dict_items, condition)

    def update_counter(self, dict_key, counter_attribute_name, increment=1, counter_only=True):
        """
        Atomic counter increase by increment.
        :param dict_key: (dict)Primary key of the item to be used.
        :param counter_attribute_name: (String)Name of the attribute that's the atomic counter to be incremented.
        The attribute must be of type N in the dynamo table
        :param increment: (integer)Amount counter is incremented by. Defaults to 1 (one).
        Use negative values to decrement.
        :param counter_only - set to False to return all attributes from item instead of just the counter value.
        :return: integer represents the new counter value that was increased by one
        """
        # Note: We use the boto3 update_item call instead of our own because we need a true update and not a put
        if counter_only:
            return_values = 'UPDATED_NEW'
        else:
            return_values = 'ALL_NEW'

        try:
            response = dynamodb.get_client().update_item(TableName=self.table_name,
                                                         Key=dict_key,
                                                         UpdateExpression="ADD {0}:count".format(
                                                             counter_attribute_name),
                                                         ExpressionAttributeValues={":count": {"N": str(increment)}},
                                                         ReturnValues=return_values)
        except ClientError as e:
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            logger.error(
                "Unable to update counter {0} in {1}::{2}".format(counter_attribute_name, self.table_name, e))
            raise

        if counter_only:
            return int(response['Attributes'][counter_attribute_name]['N'])
        else:
            return mapper.transform_to_client_dict(response['Attributes'])

    def update_counters(self, dict_key, counter_attributes, counter_only=True, condition=None,
                        additional_attributes=None):
        """
        Atomic counter increase by increment.
        :param dict_key: (dict)Primary key of the item to be used.
        :param counter_attributes: (dict)Key Value pair of attribute name and value
        The attribute must be of type N in the dynamo table
        :param counter_only - set to False to return all attributes from item instead of just the counter value.
        :param condition - allows for the caller to provide a condition, for instance to prevent inserting a row with
        an update call.
        :param additional_attributes - any additional attributes to be SET
        :return: integer represents the new counter value that was increased by one
        """
        # Note: We use the boto3 update_item call instead of our own because we need a true update and not a put
        if counter_only:
            return_values = 'UPDATED_NEW'
        else:
            return_values = 'ALL_NEW'

        expression = "ADD"
        expression_values = {}
        for index, attribute in enumerate(counter_attributes.items()):
            index_val = ":item{0}".format(index)
            expression = ' {0} {1}{2},'.format(expression, attribute[0], index_val)
            expression_values[index_val] = {"N": str(attribute[1])}

        # pop off the last comma
        expression = expression[:-1]

        if additional_attributes:
            expression = '{0} {1}'.format(expression, "SET")
            for index, attribute in enumerate(additional_attributes.items()):
                index_val = ":val{0}".format(index)
                expression = ' {0} {1}={2},'.format(expression, attribute[0], index_val)
                expression_values[index_val] = attribute[1]

            # pop off the last comma
            expression = expression[:-1]

        try:
            if condition:
                response = dynamodb.get_client().update_item(TableName=self.table_name,
                                                             Key=dict_key,
                                                             UpdateExpression=expression,
                                                             ExpressionAttributeValues=expression_values,
                                                             ReturnValues=return_values,
                                                             ConditionExpression=condition)

            else:
                response = dynamodb.get_client().update_item(TableName=self.table_name,
                                                             Key=dict_key,
                                                             UpdateExpression=expression,
                                                             ExpressionAttributeValues=expression_values,
                                                             ReturnValues=return_values)

        except ClientError as e:
            logger.error("Dynamo RequestID: {0}".format(e.response['ResponseMetadata'].get('RequestId')))
            logger.error(
                "Unable to update counter {0} in {1}::{2}".format(counter_attributes, self.table_name, e))
            raise

        return mapper.transform_to_client_dict(response['Attributes'])

    def get_pagination_token(self):
        return self.pagination_token

    @staticmethod
    def _load_json_schema(path):
        logger.debug('Loading interface schema {0}'.format(path))
        with open(path) as file:
            try:
                return orjson.loads(file.read())
            except (TypeError, ValueError) as decode_error:
                message = "Error loading interface schema {0}::{1}".format(path, decode_error.msg)
                logger.exception(message)

                raise lng_datalake_dal.exceptions.SchemaLoadError(message)

    def _generate_schema_validator(self, interface_schema):
        try:
            return fastjsonschema.compile(interface_schema)
        except fastjsonschema.JsonSchemaDefinitionException as schema_error:
            logger.error('Operation failed validation due to invalid schema {}'.format(self.__schema_path))
            raise lng_datalake_dal.exceptions.SchemaError(
                "Failed to compile the schema {}".format(schema_error.message))

    def _validate_against_interface_schema(self, dict_items):
        try:
            self.schema_validator(dict_items)
        except fastjsonschema.JsonSchemaException as validation_error:
            logger.error('Items did not match interface schema {0}'.format(self.__schema_path))
            raise lng_datalake_dal.exceptions.SchemaValidationError(
                "Items did not match interface schema {0}".format(validation_error.message))

    @staticmethod
    def build_event_ids_list(existing_item: dict, event_id: str) -> list:
        """
        Builds a  list of up to 3 latest event-ids based on an existing_item and new event-id.
        Most recent event-id will be stored at the highest index.
        :param existing_item: existing item in table.
        :param event_id: new event-id to be stored in the table
        :return: list of event-ids with length <= 3
        """
        event_ids = []
        if 'event-ids' in existing_item:
            event_ids = existing_item['event-ids'].copy()
        event_ids.append(event_id)
        return event_ids[-3:]
