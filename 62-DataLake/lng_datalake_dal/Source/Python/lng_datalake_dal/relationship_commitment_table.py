import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Doug Heitkamp"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class RelationshipCommitmentTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("RELATIONSHIP_COMMITMENT_DYNAMODB_TABLE")
        interface_schema_name = 'RelationshipCommitmentInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    @staticmethod
    def generate_target_composite_key(target_type, target_id):
        return "{0}|{1}".format(target_type, target_id)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(RelationshipID) and '
                                                   'attribute_not_exists(TargetCompositeKey)')
        except ConditionError as e:
            e.args = e.args + (
                "RelationshipID {0} with TargetCompositeKey {1} already exists in Relationship Commitment table"
                    .format(dict_items["relationship-id"], dict_items["target-composite-key"]),)
            raise

    def update_item(self, dict_items, condition=None, expression_values=None):
        super().put_item(dict_items, condition, expression_values)

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def get_all_items(self, max_items=None, pagination_token=None, **kwargs):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'
    os.environ['RELATIONSHIP_COMMITMENT_DYNAMODB_TABLE'] = 'feature-djh-DataLake-RelationshipCommitmentTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().isoformat()

    test_key_dict = \
        {
            "relationship-id": "DALsRel",
            "target-composite-key": "object|test4ltest|dtestn01"
        }
    test_attrs_dict = \
        {
            "target-type": "Object",
            "target-id": "test4ltest|dtestn01",
            "collection-id": "test4ltest",
            "object-id": "dtestn01"
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    rct = RelationshipCommitmentTable()
    rct.put_item(test_dict)
    print(json.dumps(rct.get_item(test_key_dict), indent=4))
    print("All items:")
    print(json.dumps(rct.get_all_items(), indent=4))
    print("Relationship Query Result:")
    print(json.dumps(rct.query_items(
        KeyConditionExpression='RelationshipID = :rid and TargetCompositeKey = :tck',
        ExpressionAttributeValues={":rid": {"S": "DALsRel"}, ":tck": {"S": "object|test4ltest|dtestn01"}}), indent=4))
    print("Owner Query Result:")
    print(json.dumps(rct.query_items(
        index="relationship-commitment-by-target-id-index",
        KeyConditionExpression='TargetCompositeKey = :tck and TargetType = :tt',
        ExpressionAttributeValues={":tck": {"S": "object|test4ltest|dtestn01"}, ":tt": {"S": "Object"}}), indent=4))
    rct.delete_item(test_key_dict)
