import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"


class CatalogCollectionMappingTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE")
        interface_schema_name = 'CatalogCollectionMappingInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items,
                             condition='attribute_not_exists(CatalogID) AND attribute_not_exists(CollectionID)')
        except ConditionError as e:
            e.args = e.args + ("Mapping {0}::{1} already exists in Catalog table"
                               .format(dict_items["catalog-id"], dict_items["collection-id"]),)
            raise

    def get_all_items(self, max_items=None, pagination_token=None, **kwargs):
        raise NotImplementedError

    def update_counter(self, primary_key, counter_attribute_name=None):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    from datetime import datetime
    import json

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = 'feature-mas-DataLake-CatalogCollectionMappingTable'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    curr_timestamp = datetime.now().isoformat()

    test_key_dict = \
        {
            "catalog-id": "catalog-01",
            "collection-id": "collection-01"
        }
    test_attrs_dict = {}

    test_dict = {**test_key_dict, **test_attrs_dict}

    ct = CatalogCollectionMappingTable()
    ct.put_item(test_dict)
    print(json.dumps(ct.get_item(test_key_dict), indent=4))
    print(json.dumps(ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                                    ExpressionAttributeValues={":collection_id": {"S": "collection-01"}}), indent=4))
    ct.delete_item(test_key_dict)

    batch_to_add = [{'catalog-id': '101', 'collection-id': str(i)} for i in range(5)]
    batch_to_delete = list(batch_to_add)

    # PUT 5 items using batch_write_items
    ct.batch_write_items(items_to_put=batch_to_add)
    print(json.dumps(ct.query_items(index="collection-by-catalog-id-index",
                                    KeyConditionExpression='CatalogID = :catalog_id',
                                    ExpressionAttributeValues={":catalog_id": {"S": "101"}}), indent=4))

    # DELETE the 5 items using batch_write_items
    ct.batch_write_items(items_to_delete=batch_to_delete)
    print(json.dumps(ct.query_items(index="collection-by-catalog-id-index",
                                    KeyConditionExpression='CatalogID = :catalog_id',
                                    ExpressionAttributeValues={":catalog_id": {"S": "101"}}), indent=4))
