import os

from lng_datalake_dal.table import Table, TableCache


class AssetTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("ASSET_DYNAMODB_TABLE")
        interface_schema_name = 'AssetInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def query_items(self, index=None):
        raise NotImplementedError

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['ASSET_DYNAMODB_TABLE'] = 'feature-DataLake-AssetTable'
    session.set_session()

    test_key_dict = \
        {
            "asset-id": 66666
        }
    test_attrs_dict = \
        {

            "asset-name": "DatalakeTesting"
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    AssetTable().put_item(test_dict)
    print(AssetTable().get_item(test_key_dict))
    AssetTable().delete_item(test_key_dict)
