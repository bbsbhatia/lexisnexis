class ConditionError(Exception):
    pass


class SchemaError(Exception):
    pass


class SchemaValidationError(Exception):
    pass


class SchemaLoadError(Exception):
    pass
