import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "John Morelock"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"


class IngestionTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("INGESTION_DYNAMODB_TABLE")
        interface_schema_name = 'IngestionInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items, condition=None):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(IngestionID)')
        except ConditionError as e:
            e.args = e.args + ("IngestionID {0} already exists in Ingestion table"
                               .format(dict_items["ingestion-id"]),)
            raise

    def update_item(self, dict_items, condition=None):
        super().put_item(dict_items, condition)

    def update_counters(self, ingestion_id, counter_attributes, counter_only=True):
        return super().update_counters({"IngestionID": {"S": ingestion_id}}, counter_attributes, counter_only)

    def update_counter(self, counter_attribute_name):
        raise NotImplementedError

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    from lng_aws_clients import session
    import json
    import logging

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['INGESTION_DYNAMODB_TABLE'] = 'feature-DataLake-IngestionTable'

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    ingest_id = '0110f4da-314a-11e9-91bc-09464e75f09c'
    test_key_dict = \
        {
            'ingestion-id': ingest_id,
        }
    test_attrs_dict = \
        {
            'collection-id': 'collection_ingestion',
            'owner-id': 8675309,
            'ingestion-state': 'Pending',
            'ingestion-timestamp': '2019-02-26T09:19:59.592Z',
            'archive-format': 'zip',
            'upload-id': '00779706-356f-11e9-a580-ebafb70btest',
            'bucket-name': 'feature-dl-object-staging-acctid-use1',
            'object-count': 10,
            'object-processed-count': 0,
            'object-updated-count': 0,
            'pending-expiration-epoch': 1751190831
        }
    test_dict = {**test_key_dict, **test_attrs_dict}

    ingt = IngestionTable()
    ingt.put_item(test_dict)
    logger.debug(json.dumps(ingt.get_item(test_key_dict), indent=4))
    logger.debug(json.dumps(ingt.query_items(KeyConditionExpression='IngestionID = :ingest_id',
                                             ExpressionAttributeValues={
                                                 ":ingest_id": {"S": ingest_id}}),
                            indent=4))
    test_dict['ingestion-state'] = 'Completed'
    ingt.update_item(test_dict)
    logger.debug(json.dumps(ingt.get_item(test_key_dict), indent=4))
    ingt.delete_item(test_key_dict)
