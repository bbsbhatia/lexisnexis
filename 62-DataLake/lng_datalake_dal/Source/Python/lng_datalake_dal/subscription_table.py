import os

from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.table import Table, TableCache

__author__ = "Maen Nanaa, Aaron Belvo"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"


class SubscriptionTable(Table, metaclass=TableCache):
    def __init__(self):
        table_name = os.getenv("SUBSCRIPTION_DYNAMODB_TABLE")
        interface_schema_name = 'SubscriptionInterfaceSchema.json'
        super().__init__(table_name, interface_schema_name)

    def put_item(self, dict_items):
        try:
            super().put_item(dict_items, condition='attribute_not_exists(SubscriptionID)')
        except ConditionError as e:
            e.args = e.args + ("SubscriptionID " + dict_items["subscription-id"] + " already exists in table",)
            raise

    def update_counter(self, primary_key):
        raise NotImplementedError

    def update_counters(self, dict_key, counter_attributes, counter_only=True):
        raise NotImplementedError

    def update_item(self, dict_items, condition=None):
        super().put_item(dict_items, condition)

    def batch_write_items(self):
        raise NotImplementedError


if __name__ == '__main__':
    import logging
    from lng_aws_clients import session
    import json
    import time

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = 'DataLakeDynamo-SubscriptionTable-OKZKXS9GEFBM'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    st = SubscriptionTable()
    item = {'subscription-id': 99, 'subscription-state': 'Subscription::Create',
            'subscription-name': 'My Subscription 7', 'endpoint': '123@test.com', 'protocol': 'email',
            'filter': {'event-state': ['Object::Created'], 'collection-id': [10000]},
            'subscription-pending-confirmation': True, 'pending-expiration-epoch': 1525595334,
            'subscription-arn': 'pending confirmation'}
    st.put_item(item)
    time.sleep(5)
    print(json.dumps(st.get_item({'subscription-id': 99}), indent=4))
    print(json.dumps(st.query_items(index='subscription-endpoint-index',
                                    KeyConditionExpression='Endpoint=:subscription_key',
                                    ExpressionAttributeValues={
                                        ":subscription_key": {"S": "123@test.com"}}), indent=4))
    st.delete_item({'subscription-id': 99})
