DAL Basic Usage: 
The DAL support all the CRUD operations supported by dynamodb (get, put , delete,query)

To start using the DAL;Import the table you want to work with.

The example below shows importing the Collection table.   
from lng_datalake_dal.collection_table import CollectionTable

Start using the table by Creating instance of table class.In our example "Collection table" 
ct = CollectionTable()

Collection table primary key represented as a dict : 
test_key_dict = \
       {
           "collection-id": "274123"
       }

Collection table attributes as a dict : 
test_attrs_dict = \
        {
            "asset-id": 62,
            "collection-id": "274123",
            "description": "My first collection test",
            "owner-id": 101,
            "collection-state": "Collection::Created",
            "classification-type": "Content",
            "old-object-versions-to-keep": 3
        }

test_dict = {**test_key_dict, **test_attrs_dict}

Put item example:   
ct.put_item(test_dict)

Get item example : 
ct.get_item(test_key_dict)

Get all items in Collection table example :  
ct.get_all_items()

Query collection table example:
ct.query_items(KeyConditionExpression='CollectionID = :collection_id',
                                    ExpressionAttributeValues={":collection_id": {"N": "274123"}}), indent=4))
Delete item in collection table example: 
ct.delete_item(test_key_dict)

Note: 
To find the attributes supported by the collection table look under 'dal\Schemas\CollectionInterfaceSchema'.
The path "lng_datalake_dal\Schemas" has schema file for each table the dal supports. 
By looking at the schema file you will notice that the attributes are in hyphenated lowercase format.

