{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Creates the SQS queues for lambda retry and dead-letter-queue",
  "Outputs": {
    "RetryQueueArn": {
      "Value": {
        "Fn::GetAtt": [
          "RetryQueue",
          "Arn"
        ]
      }
    },
    "RetryQueueUrl": {
      "Value": {
        "Ref": "RetryQueue"
      }
    },
    "DeadLetterQueueArn": {
      "Description": "Arn of the DeadLetter Queue",
      "Value": {
        "Fn::GetAtt": [
          "DeadLetterQueue",
          "Arn"
        ]
      }
    },
    "DeadLetterQueueUrl": {
      "Description": "Url of the DeadLetter Queue",
      "Value": {
        "Ref": "DeadLetterQueue"
      }
    }
  },
  "Parameters": {
    "AssetID": {
      "Description": "Asset id of GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "62"
    },
    "AssetName": {
      "Description": "Short name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "DataLake"
    },
    "AssetAreaName": {
      "Description": "Short asset area name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "DataLake"
    },
    "AssetGroup": {
      "Description": "AssetGroup where this is deployed in",
      "Type": "String",
      "Default": "Global"
    }
  },
  "Resources": {
    "RedriveQueue": {
      "Type": "AWS::SQS::Queue",
      "Properties": {
        "DelaySeconds": 60,
        "MessageRetentionPeriod": 1209600,
        "VisibilityTimeout": 360,
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "DeadLetterQueue": {
      "Type": "AWS::SQS::Queue",
      "Properties": {
        "DelaySeconds": 60,
        "MessageRetentionPeriod": 1209600,
        "RedrivePolicy": {
          "deadLetterTargetArn": {
            "Fn::GetAtt": [
              "RedriveQueue",
              "Arn"
            ]
          },
          "maxReceiveCount": 100
        },
        "VisibilityTimeout": 360,
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "RetryQueue": {
      "Type": "AWS::SQS::Queue",
      "Properties": {
        "DelaySeconds": 60,
        "MessageRetentionPeriod": 1209600,
        "RedrivePolicy": {
          "deadLetterTargetArn": {
            "Fn::GetAtt": [
              "RedriveQueue",
              "Arn"
            ]
          },
          "maxReceiveCount": 100
        },
        "VisibilityTimeout": 360,
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "CloudMetadataExport": {
      "Type": "Custom::CloudMetadataExport",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "AssetID": {
          "Ref": "AssetID"
        },
        "AssetGroup": {
          "Ref": "AssetGroup"
        },
        "AssetAreaName": {
          "Ref": "AssetAreaName"
        },
        "Exports": {
          "RetryQueueArn": {
            "Fn::GetAtt": [
              "RetryQueue",
              "Arn"
            ]
          },
          "RetryQueueUrl": {
            "Ref": "RetryQueue"
          },
          "DeadLetterQueueArn": {
            "Fn::GetAtt": [
              "DeadLetterQueue",
              "Arn"
            ]
          }
        },
        "Version": "2"
      }
    }
  }
}