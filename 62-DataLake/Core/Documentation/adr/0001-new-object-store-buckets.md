# New S3 Object Store Buckets

Date: 2019-08-09

## Status:  Accepted

* Deciders: Wormhole Team

## Context
We currently have a flat object structure in our S3 object store buckets (primary and backup/replicated)
because Amazon instructed us to do so due to S3 performance concerns related
to partitioning of objects in S3. We would prefer to have objects organized
by collection (object key prefix/folder) to facilitate EMR processing of all
objects within a collection. The Amazon S3 SME has recently informed us that
we can safely organize our content using a prefix common to all objects
in a collection assuming there is variability in the prefixes. Creating new
object store buckets also allows us to do the following:
* Eliminate account number from bucket names for security reasons.
* Enable server side encryption (AES-256).
* Eliminate de-duping to fix issue with only having a single Content-Type on each object.

## Decision
Decision to create new S3 object store buckets (primary and backup/replicated). We will use the 
collection hash value that is currently used in the ObjectStore table for the object key prefix:

    <collection-hash>/<sha1>

We will also be calculating the SHA1 hash using the following to make sure S3 objects are
unique for each ObjectID version (to prevent duplicate S3 objects which allows us to 
safely delete S3 objects when versions expire):

	ObjectID
	ContentType
	Metadata (future)
    VersionTimestamp (need to make sure all versons of an ObjectID are unique)
	Body
	
Other considerations:
* We considered enabling bucket versioning so that only a single version of an
  object would be seen when listing S3 objects. This would require keeping track of
  VersionIds in the ObjectStoreVersion table but we elected to not do this because
  the VersionIds would be different on a replicated bucket.

## Consequences
CloudFront Distribution will probably have to have a new Origin Group with a
different path pattern (objects/store1?) for accessing objects in the new buckets. 

We will eventually have to migrate all objects from the existing S3 object store
buckets to the new S3 object store buckets in order for EMR to fully take advantage
of organizing S3 object by collection. This will have some cost.

Must maintain objects in 2 primary (us-east-1) buckets until all objects are migrated
to new buckets.

May have to work with Amazon to make sure that S3 bucket partitioning is working as
expected with collection hash prefixes and is not negatively affecting performance.

## Links
* [Version One Story S-74666](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story%3A828010)