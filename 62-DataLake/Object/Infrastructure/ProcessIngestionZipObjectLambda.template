{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "This template creates all the stack needed for process ingestion zip object Lambda",
  "Outputs": {
    "FunctionName": {
      "Description": "Name of function",
      "Value": {
        "Ref": "LambdaFunction"
      }
    },
    "FunctionArn": {
      "Description": "Arn of function",
      "Value": {
        "Fn::GetAtt": [
          "LambdaFunction",
          "Arn"
        ]
      }
    }
  },
  "Parameters": {
    "MemorySize": {
      "Description": "The size of memory in MB for lambda function, in multiple of 64, minimum 128, maximum 1536.",
      "Type": "Number",
      "Default": "128",
      "MinValue": "128",
      "MaxValue": "1536"
    },
    "Timeout": {
      "Description": "The timeout for lambda function stop executing in seconds.",
      "Type": "Number",
      "Default": "300",
      "MinValue": "1"
    },
    "BuildNumber": {
      "Description": "Jenkins Build Number",
      "Type": "String",
      "Default": "1"
    },
    "AssetID": {
      "Description": "Asset id of GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "62"
    },
    "AssetName": {
      "Description": "Short name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "DataLake"
    },
    "AssetAreaName": {
      "Description": "Short asset area name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "DataLake"
    },
    "AssetGroup": {
      "Description": "AssetGroup where this is deployed in",
      "Type": "String",
      "Default": "Global"
    },
    "LambdaBucket": {
      "Description": "S3 bucket of the lambda function",
      "Type": "String",
      "Default": "ccs-sandbox-lambda-deploys"
    },
    "LambdaS3Object": {
      "Description": "S3Key for the lambda function",
      "Type": "String",
      "Default": "DataLake/62-DataLake-Object.zip"
    },
    "LambdaHandler": {
      "Description": "Handler for the lambda function",
      "Type": "String",
      "Default": "process_ingestion_zip_object.lambda_handler"
    },
    "LambdaFunctionVersionV1": {
      "Description": "Version for the Lambda Function v1 alias",
      "Type": "String",
      "Default": "$LATEST"
    }
  },
  "Resources": {
    "CloudMetadata": {
      "Type": "Custom::CloudMetadata",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "AssetID": {
          "Ref": "AssetID"
        },
        "AssetGroup": {
          "Ref": "AssetGroup"
        },
        "AssetAreaName": {
          "Ref": "AssetAreaName"
        },
        "Filter": [
          "IngestionDynamoTable",
          "TrackingDynamoTable",
          "ObjectStoreDynamoTable",
          "EventStoreBackendDynamoTable",
          "TerminalErrorsBucketArn",
          "TerminalErrorsBucket",
          "DataLakeObjectBucket",
          "DataLakeObjectStoreBucket",
          "DataLakeObjectStagingBucketArn",
          "V1IngestionObjectQueueArn",
          "LatestIngestionObjectQueueArn",
          "IngestionCounterQueueArn",
          "IngestionCounterQueueUrl"
        ],
        "ExportsOnly": true,
        "Version": "2",
        "LastUpdate": {
          "Ref": "BuildNumber"
        }
      }
    },
    "LambdaFunctionVersion": {
      "Type": "Custom::LambdaVersion",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "LambdaName": {
          "Ref": "LambdaFunction"
        },
        "BuildNumber": {
          "Ref": "BuildNumber"
        }
      }
    },
    "V1LambdaAlias": {
      "Type": "AWS::Lambda::Alias",
      "Properties": {
        "Description": "v1 version of LambdaVersioning Custom Resource",
        "FunctionName": {
          "Ref": "LambdaFunction"
        },
        "FunctionVersion": {
          "Ref": "LambdaFunctionVersionV1"
        },
        "Name": "v1"
      }
    },
    "LatestLambdaAlias": {
      "Type": "AWS::Lambda::Alias",
      "Properties": {
        "Description": "LATEST version of LambdaVersioning Custom Resource",
        "FunctionName": {
          "Ref": "LambdaFunction"
        },
        "FunctionVersion": "$LATEST",
        "Name": "LATEST"
      }
    },
    "LambdaRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": [
                  "lambda.amazonaws.com"
                ]
              },
              "Action": [
                "sts:AssumeRole"
              ]
            }
          ]
        },
        "ManagedPolicyArns": [
          "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
          "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
        ],
        "Policies": [
          {
            "PolicyName": "DataLakeDynamoAccess",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowIngestionTableAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:GetItem"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.IngestionDynamoTable}"
                    }
                  ]
                },
                {
                  "Sid": "AllowEventStoreBackendTableAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:PutItem"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.EventStoreBackendDynamoTable}"
                    }
                  ]
                },
                {
                  "Sid": "AllowObjectStoreTableAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:GetItem"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.ObjectStoreDynamoTable}"
                    }
                  ]
                },
                {
                  "Sid": "AllowTrackingTableAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:Query",
                    "dynamodb:DeleteItem"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.TrackingDynamoTable}"
                    }
                  ]
                }
              ]
            }
          },
          {
            "PolicyName": "DataLakeS3Bucket",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowDataLakeS3Bucket",
                  "Effect": "Allow",
                  "Action": [
                    "s3:GetObject",
                    "s3:PutObject"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:s3:::${CloudMetadata.asset.outputs.DataLakeObjectBucket}-*/*"
                    },
                    {
                      "Fn::Sub": "arn:aws:s3:::${CloudMetadata.asset.outputs.DataLakeObjectStoreBucket}-*/*"
                    }
                  ]
                },
                {
                  "Sid": "AllowDataLakeS3ListBucket",
                  "Effect": "Allow",
                  "Action": [
                    "s3:ListBucket"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:s3:::${CloudMetadata.asset.outputs.DataLakeObjectBucket}-*"
                    },
                    {
                      "Fn::Sub": "arn:aws:s3:::${CloudMetadata.asset.outputs.DataLakeObjectStoreBucket}-*"
                    }
                  ]
                }
              ]
            }
          },
          {
            "PolicyName": "StagingS3Bucket",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowS3StagingBucket",
                  "Effect": "Allow",
                  "Action": [
                    "s3:GetObject"
                  ],
                  "Resource": {
                    "Fn::Sub": "${CloudMetadata.asset.outputs.DataLakeObjectStagingBucketArn}/*"
                  }
                }
              ]
            }
          },
          {
            "PolicyName": "QueueTriggerAccess",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowIngestionObjectQueueAccess",
                  "Effect": "Allow",
                  "Action": [
                    "sqs:DeleteMessage",
                    "sqs:ChangeMessageVisibility",
                    "sqs:ReceiveMessage",
                    "sqs:GetQueueAttributes",
                    "sqs:SendMessage"
                  ],
                  "Resource": [
                    {
                      "Fn::GetAtt": [
                        "CloudMetadata",
                        "asset.outputs.V1IngestionObjectQueueArn"
                      ]
                    },
                    {
                      "Fn::GetAtt": [
                        "CloudMetadata",
                        "asset.outputs.LatestIngestionObjectQueueArn"
                      ]
                    }
                  ]
                }
              ]
            }
          },
          {
            "PolicyName": "IngestionCounterQueueAccess",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowIngestionCounterQueueAccess",
                  "Effect": "Allow",
                  "Action": [
                    "sqs:SendMessage"
                  ],
                  "Resource": [
                    {
                      "Fn::GetAtt": [
                        "CloudMetadata",
                        "asset.outputs.IngestionCounterQueueArn"
                      ]
                    }
                  ]
                }
              ]
            }
          },
          {
            "PolicyName": "TerminalErrorsS3Bucket",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowTerminalErrorsS3Bucket",
                  "Effect": "Allow",
                  "Action": [
                    "s3:PutObject",
                    "s3:PutObjectAcl"
                  ],
                  "Resource": {
                    "Fn::Sub": "${CloudMetadata.asset.outputs.TerminalErrorsBucketArn}/*"
                  }
                }
              ]
            }
          }
        ],
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "LambdaFunction": {
      "Type": "AWS::Lambda::Function",
      "Properties": {
        "FunctionName": {
          "Fn::Sub": "${AssetGroup}-object-ProcessIngestionZipObject"
        },
        "Code": {
          "S3Bucket": {
            "Ref": "LambdaBucket"
          },
          "S3Key": {
            "Ref": "LambdaS3Object"
          }
        },
        "Description": "General Lambda Function",
        "Handler": {
          "Ref": "LambdaHandler"
        },
        "MemorySize": {
          "Ref": "MemorySize"
        },
        "Environment": {
          "Variables": {
            "DATALAKE_BUCKET_NAME": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.DataLakeObjectStoreBucket"
              ]
            },
            "INGESTION_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.IngestionDynamoTable"
              ]
            },
            "OBJECT_STORE_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.ObjectStoreDynamoTable"
              ]
            },
            "TRACKING_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.TrackingDynamoTable"
              ]
            },
            "EVENT_STORE_BACKEND_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.EventStoreBackendDynamoTable"
              ]
            },
            "INGESTION_COUNTER_QUEUE_URL": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.IngestionCounterQueueUrl"
              ]
            },
            "S3_TERMINAL_ERRORS_BUCKET_NAME": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.TerminalErrorsBucket"
              ]
            }
          }
        },
        "Role": {
          "Fn::GetAtt": [
            "LambdaRole",
            "Arn"
          ]
        },
        "Runtime": "python3.6",
        "Timeout": {
          "Ref": "Timeout"
        },
        "TracingConfig": {
          "Mode": "PassThrough"
        },
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "LambdaFunctionLogGroup": {
      "Type": "AWS::Logs::LogGroup",
      "Properties": {
        "LogGroupName": {
          "Fn::Sub": "/aws/lambda/${LambdaFunction}"
        },
        "RetentionInDays": "30"
      }
    },
    "V1IngestionObjectSqsSource": {
      "Type": "AWS::Lambda::EventSourceMapping",
      "Properties": {
        "BatchSize": 1,
        "Enabled": true,
        "EventSourceArn": {
          "Fn::GetAtt": [
            "CloudMetadata",
            "asset.outputs.V1IngestionObjectQueueArn"
          ]
        },
        "FunctionName": {
          "Ref": "V1LambdaAlias"
        }
      }
    },
    "LatestIngestionObjectSqsSource": {
      "Type": "AWS::Lambda::EventSourceMapping",
      "Properties": {
        "BatchSize": 1,
        "Enabled": true,
        "EventSourceArn": {
          "Fn::GetAtt": [
            "CloudMetadata",
            "asset.outputs.LatestIngestionObjectQueueArn"
          ]
        },
        "FunctionName": {
          "Ref": "LatestLambdaAlias"
        }
      }
    }
  }
}