import os
import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from folder_upload_dispatcher import lambda_handler, build_glue_argument

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "FolderUploadDispatcher")


class FolderUploadDispatcherTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'GLUE_DISPATCHER_TOPIC_ARN': 'test_topic_arn',
                             'DATALAKE_BUCKET_NAME': 'test_bucket_name'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + tests glue_dispatcher.lambda_handler
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    def test_lambda_handler(self, glue_mock, glue_region_mock):
        valid_event = io_utils.load_data_json('valid_event.json')
        valid_glue_resp = io_utils.load_data_json('glue_resp.json')
        glue_mock.return_value.start_job_run.return_value = valid_glue_resp
        glue_region_mock.return_value = 'us-east-1'
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), 'Success')

    # + tests glue_dispatcher.lambda_handler
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    def test_lambda_handler_update(self, glue_mock, glue_region_mock):
        valid_event = io_utils.load_data_json('valid_update_event.json')
        valid_glue_resp = io_utils.load_data_json('glue_resp.json')
        glue_mock.return_value.start_job_run.return_value = valid_glue_resp
        glue_region_mock.return_value = 'us-east-1'
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), 'Success')

    # - glue_dispatcher.lambda_handler - glue EndpointConnectionError
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    def test_lambda_handler_error_endpoint_connection_error_exception(self, glue_mock, glue_region_mock):
        valid_event = io_utils.load_data_json('valid_event.json')
        glue_region_mock.return_value = 'us-east-1'
        glue_mock.return_value.start_job_run.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, lambda_handler(valid_event, MockLambdaContext))

    # - glue_dispatcher.lambda_handler - glue client error
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    def test_lambda_handler_error_1(self, glue_mock, glue_region_mock):
        valid_event = io_utils.load_data_json('valid_event.json')
        glue_region_mock.return_value = 'us-east-1'
        glue_mock.return_value.start_job_run.side_effect = ClientError({'ResponseMetadata': {},
                                                                        'Error': {
                                                                            'Code': 'OTHER',
                                                                            'Message': 'This is a mock'}},
                                                                       "FAKE")
        self.assertRaises(ClientError, lambda_handler(valid_event, MockLambdaContext))

    # - glue_dispatcher.lambda_handler - glue exception raises TerminalErrorException
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    def test_lambda_handler_error_2(self, glue_mock, glue_region_mock):
        valid_event = io_utils.load_data_json('valid_event.json')
        glue_region_mock.return_value = 'us-east-1'
        glue_mock.return_value.start_job_run.side_effect = Exception()
        self.assertRaises(TerminalErrorException, lambda_handler(valid_event, MockLambdaContext))

    # - tests glue_dispatcher.lambda_handler - invalid event-name
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    def test_lambda_handler_invalid_event_name(self, glue_mock, glue_region_mock):
        valid_event = io_utils.load_data_json('invalid_name_event.json')
        valid_glue_resp = io_utils.load_data_json('glue_resp.json')
        glue_mock.return_value.start_job_run.return_value = valid_glue_resp
        glue_region_mock.return_value = 'us-east-1'
        self.assertRaisesRegex(TerminalErrorException, 'event-name doesnt match',
                               lambda_handler(valid_event, MockLambdaContext))

    # - tests glue_dispatcher.lambda_handler - invalid event-version
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    def test_lambda_handler_invalid_version(self, glue_mock, glue_region_mock):
        valid_event = io_utils.load_data_json('invalid_version_event.json')
        valid_glue_resp = io_utils.load_data_json('glue_resp.json')
        glue_mock.return_value.start_job_run.return_value = valid_glue_resp
        glue_region_mock.return_value = 'us-east-1'
        self.assertRaisesRegex(TerminalErrorException, "event-version does not match",
                               lambda_handler(valid_event, MockLambdaContext))

    # - tests glue_dispatcher.lambda_handler - invalid stage
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    def test_lambda_handler_invalid_stage(self, glue_mock, glue_region_mock):
        valid_event = io_utils.load_data_json('invalid_stage_event.json')
        valid_glue_resp = io_utils.load_data_json('glue_resp.json')
        glue_mock.return_value.start_job_run.return_value = valid_glue_resp
        glue_region_mock.return_value = 'us-east-1'
        self.assertRaisesRegex(TerminalErrorException, "stage not match",
                               lambda_handler(valid_event, MockLambdaContext))


    # + build_glue_argument - Valid event
    def test_build_glue_argument_valid(self):
        valid_event_message = io_utils.load_data_json('valid_event_message.json')
        bucket_list = ["None-use1",
                       "None-usw2"]
        expected_resp = {
            "version-id": "1",
            "staging-bucket": "feature-datalake-object-staging-store-288044017584",
            "object-id": "John-Test-123123",
            "collection-id": "830",
            "event-id": "d4a93a5d-a57f-11e8-934b-2b23982bccda",
            "stage": "LATEST",
            "request-time": "2018-08-21T20:21:48.209Z",
            "event-name": "Object::CreatePending",
            "event-version": 1,
            "old-object-versions-to-keep": -1,
            "staging-prefix": "folder-upload/1_adc226d1-a57c-11e8-a52a-15395f060ef1",
            "object-store-buckets": bucket_list,
            "completion-topic-arn": None
        }
        with patch('folder_upload_dispatcher.COMPLETION_TOPIC_ARN', None):
            self.assertDictEqual(build_glue_argument(valid_event_message, "Object::CreatePending", bucket_list), expected_resp)

if __name__ == '__main__':
    unittest.main()
