import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, NoSuchObjectRelationshipVersion, NoSuchObjectRelationship
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_object_relationship_command import lambda_handler,get_object_relationship,\
    get_latest_object_relationship, generate_response_json, unpack_composite_object_id

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetObjectRelationshipCommand')


class TestGetObjectRelationshipCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + test_lambda_handler - success
    @patch('get_object_relationship_command.get_object_relationship')
    @patch('service_commons.object_command.get_relationship_response')
    @patch('service_commons.object_command.get_object_version_response')
    @patch('service_commons.object_command.get_object_response')
    @patch('service_commons.object_command.get_collection_response')
    def test_lambda_handler(self, mock_validate_collection_id,
                            mock_validate_object_id,
                            mock_validate_object_version,
                            mock_validate_relationship_id,
                            mock_get_object_relationship):
        mock_validate_collection_id.return_value = None
        mock_validate_object_id.return_value = {
            'object-state': 'Created',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'Caselaw',
            'version-number': 2,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'targetObject',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-sxr-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']}
        mock_validate_object_version.return_value = None
        mock_validate_relationship_id.return_value = None
        mock_get_object_relationship.return_value = {
            'related-id': 'relatedObjectB|allNews|1',
            'object-relationship-state': 'Created',
            'object-relationship-timestamp': '2019-08-04T01:20:23.324Z',
            'relationship-key': 'judges|v00001',
            'target-id': 'targetObject|Caselaw|1'
        }
        input_event = io_util.load_data_json('apigateway.request.accepted_1.json')
        expected_response = io_util.load_data_json('apigateway.response.accepted_1.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(input_event, MockLambdaContext()), expected_response)

    # + test_lambda_handler - success - relationship version not provided
    @patch('get_object_relationship_command.get_latest_object_relationship')
    @patch('service_commons.object_command.get_relationship_response')
    @patch('service_commons.object_command.get_object_version_response')
    @patch('service_commons.object_command.get_object_response')
    @patch('service_commons.object_command.get_collection_response')
    def test_lambda_handler_2(self, mock_validate_collection_id,
                              mock_validate_object_id,
                              mock_validate_object_version,
                              mock_validate_relationship_id,
                              mock_get_latest_object_relationship):
        mock_validate_collection_id.return_value = None
        mock_validate_object_id.return_value = {
            'object-state': 'Created',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'Caselaw',
            'version-number': 2,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'targetObject',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-sxr-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']}
        mock_validate_object_version.return_value = None
        mock_validate_relationship_id.return_value = None
        mock_get_latest_object_relationship.return_value = {
            'related-id': 'relatedObject|allNews|1',
            'object-relationship-state': 'Created',
            'object-relationship-timestamp': '2019-08-03T01:20:23.324Z',
            'relationship-key': 'judges|v00002',
            'target-id': 'targetObject|Caselaw|1'
        }
        input_event = io_util.load_data_json('apigateway.request.accepted_2.json')
        expected_response = io_util.load_data_json('apigateway.response.accepted_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(input_event, MockLambdaContext()), expected_response)

    # + test_get_object_relationship - success
    @patch('lng_datalake_dal.object_relationship_table.ObjectRelationshipTable.get_item')
    def test_get_object_relationship(self, mock_get_item):
        expected_response = {
            'related-id': 'relatedObject|allNews|1',
            'object-relationship-state': 'Created',
            'object-relationship-timestamp': '2019-08-03T01:20:23.324Z',
            'relationship-key': 'judges|v00001',
            'target-id': 'targetObject|Caselaw|1'}
        mock_get_item.return_value = expected_response
        self.assertDictEqual(get_object_relationship('targetObject|Caselaw|1', 'judges', 1), expected_response)

    # - test_get_object_relationship: does not exist
    @patch('lng_datalake_dal.object_relationship_table.ObjectRelationshipTable.get_item')
    def test_get_object_relationship_fail(self, mock_get_item):
        mock_get_item.return_value = {}

        with self.assertRaisesRegex(NoSuchObjectRelationshipVersion, 'Invalid Object Relationship'):
            get_object_relationship('targetObject|Caselaw|1', 'judges', 1)

    # - test_get_object_relationship: ClientError->InternalError
    @patch('lng_datalake_dal.object_relationship_table.ObjectRelationshipTable.get_item')
    def test_get_object_relationship_fail_1(self, mock_get_item):
        mock_get_item.side_effect = ClientError({}, 'mock')

        with self.assertRaisesRegex(InternalError, 'mock'):
            get_object_relationship('targetObject|Caselaw|1', 'judges', 1)

    # - test_get_object_relationship: Exception->InternalError
    @patch('lng_datalake_dal.object_relationship_table.ObjectRelationshipTable.get_item')
    def test_get_object_relationship_fail_2(self, mock_get_item):
        mock_get_item.side_effect = Exception(
            'mock')

        with self.assertRaisesRegex(InternalError, 'mock'):
            get_object_relationship('targetObject|Caselaw|1', 'judges', 1)

    # + test_get_latest_object_relationship - success
    @patch('lng_datalake_dal.object_relationship_table.ObjectRelationshipTable.query_items')
    def test_get_latest_object_relationship(self, mock_query_item):
        expected_response = [{
            'related-id': 'relatedObject|allNews|1',
            'object-relationship-state': 'Created',
            'object-relationship-timestamp': '2019-08-03T01:20:23.324Z',
            'relationship-key': 'judges|v00002',
            'target-id': 'targetObject|Caselaw|1'
        },
            {
                'related-id': 'relatedObjectB|allNews|1',
                'object-relationship-state': 'Created',
                'object-relationship-timestamp': '2019-08-04T01:20:23.324Z',
                'relationship-key': 'judges|v00001',
                'target-id': 'targetObject|Caselaw|1'
            }]

        mock_query_item.return_value = expected_response
        self.assertDictEqual(get_latest_object_relationship('targetObject|Caselaw|1', 'judges'), {
            'related-id': 'relatedObject|allNews|1',
            'object-relationship-state': 'Created',
            'object-relationship-timestamp': '2019-08-03T01:20:23.324Z',
            'relationship-key': 'judges|v00002',
            'target-id': 'targetObject|Caselaw|1'
        })

    # - test_get_latest_object_relationship: does not exist
    @patch('lng_datalake_dal.object_relationship_table.ObjectRelationshipTable.query_items')
    def test_get_latest_object_relationship_fail(self, mock_query_item):
        mock_query_item.return_value = []

        with self.assertRaisesRegex(NoSuchObjectRelationship, 'Invalid Object Relationship'):
            get_latest_object_relationship('targetObject|Caselaw|1', 'judges')

    # - test_get_latest_object_relationship: ClientError->InternalError
    @patch('lng_datalake_dal.object_relationship_table.ObjectRelationshipTable.query_items')
    def test_get_latest_object_relationship_fail_1(self, mock_query_item):
        mock_query_item.side_effect = ClientError({}, 'mock')

        with self.assertRaisesRegex(InternalError, 'mock'):
            get_latest_object_relationship('targetObject|Caselaw|1', 'judges')

    # - test_get_latest_object_relationship: Exception->InternalError
    @patch('lng_datalake_dal.object_relationship_table.ObjectRelationshipTable.query_items')
    def test_get_latest_object_relationship_fail_2(self, mock_query_item):
        mock_query_item.side_effect = Exception(
            'mock')

        with self.assertRaisesRegex(InternalError, 'mock'):
            get_latest_object_relationship('targetObject|Caselaw|1', 'judges')

    # - generate_response_json - Missing required
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_missing_required(self, required_mock, opt_mock):
        input_dict = {
            "target-object": {
                "object-id": "targetObject",
                "collection-id": "Caselaw",
                "version-number": 1
            },
            "relationship-id": "judges",
            "relationship-version": 1

        }
        object_relationship = {
            'related-id': 'relatedObject|allNews|1',
            'object-relationship-state': 'Created',
            'relationship-key': 'judges|v00001',
            'target-id': 'targetObject|Caselaw|1'
        }

        required_mock.return_value = ['object-relationship-timestamp']
        opt_mock.return_value = []
        with self.assertRaisesRegex(InternalError, "InternalError||Missing required property"):
            generate_response_json(input_dict, object_relationship, 1)

    # +unpack_composite_object_id
    def test_unpack_composite_object_id(self):
        expected_output = {'object-id': 'obj', 'collection-id': 'col', 'version-number': 1}
        self.assertEqual(unpack_composite_object_id("obj|col|1"), expected_output)


if __name__ == '__main__':
    unittest.main()
