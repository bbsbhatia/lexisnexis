import base64
import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import SemanticError, InternalError, NotAllowedError, \
    InvalidRequestPropertyValue, NoSuchCollection, NoSuchRelationship, NoSuchObjectVersion, NoSuchObject, \
    NotAuthorizedError
from lng_datalake_constants import event_names
from lng_datalake_constants import object_status
from lng_datalake_testhelper.io_utils import IOUtils

import service_commons.object_common as common_object_module
from service_commons.object_command import get_object_content, unpack_folder_object_transaction_id, \
    create_multipart_upload, get_collection_response, get_object_response, get_object_version_response, \
    get_relationship_response, validate_create_event, determine_upsert_event_name, \
    get_previous_and_current_events, get_object_item, generate_sts_role_token, \
    validate_object_id, encode_md5, extend_event_ttl, put_object_to_staging_bucket, validate_object_metadata, \
    validate_and_get_changeset_id, get_changeset_object_item, get_changeset_object_response

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'ObjectCommand')


class ServiceCommonsObjectCommandTest(TestCase):
    session = None

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(common_object_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + get_object_content - Valid with utf8 encoding
    def test_get_object_content_valid__utf8_encoded(self):
        body = 'mock data'
        encoded = 'uTf-8'
        request = {'body': body, 'body-encoding': encoded}
        self.assertEqual(get_object_content(request), body.encode(encoded))

    # + get_object_content - Valid with base64 encoding
    def test_get_object_content_valid_base64_encoded(self):
        body = 'mock data'
        encoded = 'bAsE64'
        request = {'body': body, 'body-encoding': encoded}
        self.assertEqual(get_object_content(request), base64.b64decode(body))

    # + get_object_content - Valid without encoding
    def test_get_object_content_valid_not_encoded(self):
        body = 'mock data'
        request = {'body': body}
        self.assertEqual(get_object_content(request), body.encode('utf-8'))

    # + get_object_content - invalid encoding
    def test_get_object_content_fail_1(self):
        body = 'mock data'
        request = {'body': body, 'body-encoding': 'mock'}
        self.assertRaises(InternalError, get_object_content, request)

    # + unpack_folder_object_transaction_id - Valid
    def test_unpack_folder_object_transaction_id_valid(self):
        transaction_id = 'eyJ0cmFuc2FjdGlvbi1pZCI6ICJiYjY4OThkMS0yMDFmLTExZTktOTQyNS1iZjI1ZWYxMTkwNGUiLCAiY29sbGV' \
                         'jdGlvbi1pZCI6ICJ0ZXN0LWNvbGxlY3Rpb24iLCAib3duZXItaWQiOiAxMiwgInN0YWdlIjogInYxIn0='
        sample_dict = {'transaction-id': "bb6898d1-201f-11e9-9425-bf25ef11904e", 'collection-id': "test-collection",
                       'owner-id': 12, 'stage': "v1"}
        self.assertEqual(unpack_folder_object_transaction_id(transaction_id),
                         sample_dict)

    # + unpack_folder_object_transaction_id - invalid transaction
    def test_unpack_folder_object_transaction_id_invalid(self):
        transaction_id = 'invalid_folder_object_transaction_id'
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Transaction ID"):
            unpack_folder_object_transaction_id(transaction_id)

    # +create_multipart_upload - valid
    @patch("lng_aws_clients.s3.get_client")
    def test_create_multipart_upload(self, s3_client_mock):
        s3_client_mock.return_value.create_multipart_upload.return_value = io_utils.load_data_json(
            'multipart_resp.json')
        bucket_name = 'test'
        key = 'multipart-object/test'
        expected_resp = {'S3': {'Bucket': bucket_name, 'Key': key},
                         "UploadId": "vhKNEOXBg5its65T7YiSP480_EUr2uHFNHaA2XW9nkDXhBzJ5_mYn6Mk1Gu2U4fJeWbeHf8ufDuU0t"
                                     "3o02PverA62vCKQ6lPmoTikqZA3sK.pw4SYin.4RVK1ggeEkowXYz2a67WBWWy_v46r5y6pg--"}
        request_info = {'object-id': "foo", 'content-type': 'application/json',
                        'event-name': 'Object::Create', 'request-id': 'abcd', 'stage': 'LATEST',
                        'object-key': key,
                        'request-time': "2019-08-23T10:00:00.123Z"}

        self.assertEqual(create_multipart_upload(request_info, {'collection-id': 14, 'collection-hash': 'abcdefg',
                                                                'old-object-versions-to-keep': -1},
                                                 "2018-12-17T19:01:14Z", bucket_name), expected_resp)

    # + test_get_collection_response_success
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    def test_get_collection_response_success(self, mock_collection_table_get_item):
        collection_id = "1234"
        collection_item = io_utils.load_data_json('collectionTable.get_item.valid.json')
        mock_collection_table_get_item.return_value = collection_item
        self.assertDictEqual(get_collection_response(collection_id), collection_item)

    # - test_get_collection_response_semantic_error: SemanticError
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    def test_get_collection_response_semantic_error(self, mock_collection_table_get_item):
        collection_item = io_utils.load_data_json('collectionTable.get_item.suspended.valid.json')
        mock_collection_table_get_item.return_value = collection_item
        collection_id = collection_item['collection-id']
        err_msg = "Collection ID {0} is not set to {1}".format(
            collection_id, 'Created', collection_item['collection-state'])
        with self.assertRaisesRegex(SemanticError, err_msg):
            get_collection_response(collection_id)

    # - test_get_collection_response_internal_error: ClientError->InternalError
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    def test_get_collection_response_internal_error(self, mock_collection_table_get_item):
        collection_item = io_utils.load_data_json('collectionTable.get_item.suspended.valid.json')
        mock_collection_table_get_item.side_effect = ClientError({},
                                                                 'error test_get_collection_response_internal_error')
        collection_id = collection_item['collection-id']
        with self.assertRaisesRegex(InternalError, 'Unable to get item from Collection Table'):
            get_collection_response(collection_id)

    # - test_get_collection_response_internal_error_2: Exception->InternalError
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    def test_get_collection_response_internal_error_2(self, mock_collection_table_get_item):
        collection_item = io_utils.load_data_json('collectionTable.get_item.suspended.valid.json')
        mock_collection_table_get_item.side_effect = Exception('error test_get_collection_response_internal_error_2')
        collection_id = collection_item['collection-id']
        with self.assertRaisesRegex(InternalError, 'Unhandled exception occurred'):
            get_collection_response(collection_id)

    # - test_get_collection_response_invalid_property_value_error: InvalidRequestPropertyValue
    @patch("lng_datalake_dal.collection_table.CollectionTable.get_item")
    def test_get_collection_response_invalid_property_value_error(self, mock_collection_table_get_item):
        collection_id = "1234"
        err_msg = 'Invalid Collection ID {0}'.format(collection_id)
        mock_collection_table_get_item.return_value = None
        with self.assertRaisesRegex(NoSuchCollection, err_msg):
            get_collection_response(collection_id)

    # + test_get_object_response - success
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_response(self, mock_get_item, mock_get_request_time):
        expected_response = {
            'object-state': 'Created',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'Caselaw',
            'version-number': 1,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'targetObject',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-sxr-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']}
        mock_get_item.return_value = expected_response
        mock_get_request_time.return_value='2019-10-21T18:01:03.781Z'
        self.assertDictEqual(get_object_response("targetObject", "Caselaw"), expected_response)

    # + test_get_object_response - success
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_response_with_version(self, mock_get_item):
        expected_response = {
            'object-state': 'Created',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'Caselaw',
            'version-number': 3,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'targetObject',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-sxr-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']}
        mock_get_item.return_value = expected_response
        self.assertDictEqual(get_object_response("targetObject", "Caselaw", 3), expected_response)

    # - test_get_object_response - empty response
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_response_fail_1(self, mock_get_item):
        mock_get_item.return_value = {}
        with self.assertRaisesRegex(NoSuchObject, 'Invalid Object ID targetObject in Collection ID Caselaw'):
            get_object_response("targetObject", "Caselaw")

    # - test_get_object_response - invalid state
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_response_fail_2(self, mock_get_item):
        mock_get_item.return_value = {
            'object-state': 'Removed',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'Caselaw',
            'version-number': 1,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'targetObject',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-sxr-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']}
        with self.assertRaisesRegex(SemanticError, 'has been removed'):
            get_object_response("targetObject", "Caselaw")

    # - test_get_object_response - expired object
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_response_fail_3(self, mock_get_item, mock_get_request_time):
        mock_get_item.return_value = {
            'object-state': 'Createded',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'Caselaw',
            'version-number': 1,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'targetObject',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-sxr-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2'],
            'pending-expiration-epoch': 1571544000}

        mock_get_request_time.return_value='2019-10-21T18:01:03.781Z'
        with self.assertRaisesRegex(NoSuchObject, 'Invalid Object ID targetObject in Collection ID Caselaw'):
             get_object_response("targetObject", "Caselaw")

    # - test_get_object_response request the removed version of the object
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_response_fail_4(self, mock_get_item):
        mock_get_item.return_value = {
            'object-state': 'Removed',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'Caselaw',
            'version-number': 3,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'targetObject',
            'content-type': 'tombstone',
            'raw-content-length': 34,
            'bucket-name': 'feature-sxr-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']}
        with self.assertRaisesRegex(SemanticError, 'has been removed'):
            get_object_response("targetObject", "Caselaw", 3)

    # + test_get_object_version_response - success
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.get_item')
    def test_get_object_version_response(self, mock_get_item):
        expected_response = {
            "raw-content-length": 58,
            "version-timestamp": "2019-05-09T14:39:55.011Z",
            "bucket-name": "feature-sxr-dl-object-store-288044017584-use1",
            "version-number": 1,
            "object-key": "5c827a7e8d539c437a84bb309f1199782c12fb2a",
            "composite-key": "targetObject|Caselaw"
        }
        mock_get_item.return_value = expected_response
        self.assertDictEqual(get_object_version_response("targetObject", "Caselaw", 1), expected_response)

    # - test_get_object_version_response: does not exist
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.get_item')
    def test_get_object_version_response_fail_1(self, mock_get_item):
        mock_get_item.return_value = {}

        with self.assertRaisesRegex(NoSuchObjectVersion, 'Invalid Version Number 1 for Object ID targetObject'):
            get_object_version_response("targetObject", "Caselaw", 1)

    # - test_get_object_version_response: ClientError->InternalError
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.get_item')
    def test_get_object_version_response_fail_2(self, mock_get_item):
        mock_get_item.side_effect = ClientError({}, 'mock')

        with self.assertRaisesRegex(InternalError, 'mock'):
            get_object_version_response("targetObject", "Caselaw", 1)

    # - test_get_object_version_response: Exception->InternalError
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.get_item')
    def test_get_object_version_response_fail_3(self, mock_get_item):
        mock_get_item.side_effect = Exception(
            'mock')

        with self.assertRaisesRegex(InternalError, 'mock'):
            get_object_version_response("targetObject", "Caselaw", 1)

    # - test_get_changeset_object_response - success
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_get_changeset_object_response(self, mock_get_item):
        expected_response = {
            'object-state': 'Created',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'test-collection',
            'changeset-id': 'test-changeset',
            'version-number': 1,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'test-object',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-dan-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']
        }
        mock_get_item.return_value = expected_response
        self.assertDictEqual(get_changeset_object_response("test-object", "test-collection", "test-changeset", 1),
                             expected_response)

    # - test_get_changeset_object_response - empty response
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_changeset_object_response_fail_1(self, mock_get_item):
        mock_get_item.return_value = {}
        with self.assertRaisesRegex(NoSuchObject, 'Invalid Object ID test-object in Changeset ID test-changeset'):
            get_changeset_object_response("test-object", "test-collection", "test-changeset", 1)

    # - test_get_changeset_object_response - does not exist
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_changeset_object_response_fail_2(self, mock_get_item, mock_get_request_time):
        mock_get_item.return_value = {
            'object-state': 'Created',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'test-collection',
            'changeset-id': 'test-changeset',
            'version-number': 1,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'pending-expiration-epoch': 1571544000,
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'test-object',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-dan-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']
        }
        mock_get_request_time.return_value = '2019-10-21T18:01:03.781Z'
        with self.assertRaisesRegex(NoSuchObject, 'Invalid Object ID test-object in Changeset ID test-changeset'):
            get_changeset_object_response("test-object", "test-collection", "test-changeset", 1)

    # - test_get_changeset_object_response - invalid state
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_changeset_object_response_fail_3(self, mock_get_item, mock_get_request_time):
        mock_get_item.return_value = {
            'object-state': 'Removed',
            'collection-hash': 'fbcc183e558573589cc16eadf04acf928a8a57e7',
            'collection-id': 'test-collection',
            'changeset-id': 'test-changeset',
            'version-number': 1,
            'object-key': '05c32b220620bb6906a767abcd1a70eeb5de0676',
            'pending-expiration-epoch': 1671544000,
            'version-timestamp': '2019-06-24T18:10:25.319Z',
            'object-id': 'test-object',
            'content-type': 'text/plain',
            'raw-content-length': 34,
            'bucket-name': 'feature-dan-dl-object-store-288044017584-use1',
            'replicated-buckets': ['feature-sxr-dl-object-store-288044017584-usw2']
        }
        mock_get_request_time.return_value = '2019-10-21T18:01:03.781Z'
        with self.assertRaisesRegex(SemanticError, 'has been removed'):
            get_changeset_object_response("test-object", "test-collection", "test-changeset", 1)

    # + test_get_relationship_response - success
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_relationship_response(self, mock_get_item):
        expected_response = {
            'owner-id': 5,
            'relationship-id': 'judges',
            'relationship-type': 'Metadata',
            'relationship-state': 'Created'}
        mock_get_item.return_value = expected_response
        self.assertDictEqual(get_relationship_response('judges'), expected_response)

    # - test_get_relationship_response: invalid state
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_relationship_response_fail_1(self, mock_get_item):
        mock_get_item.return_value = {
            'owner-id': 5,
            'relationship-id': 'judges',
            'relationship-type': 'Metadata',
            'relationship-state': 'Removed'}

        with self.assertRaisesRegex(SemanticError, 'not in Created state'):
            get_relationship_response('judges')

    # - test_get_relationship_response: does not exist
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_relationship_response_fail_2(self, mock_get_item):
        mock_get_item.return_value = {}

        with self.assertRaisesRegex(NoSuchRelationship, 'Invalid Relationship ID'):
            get_relationship_response('judges')

    # - test_get_relationship_response: ClientError->InternalError
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_relationship_response_fail_3(self, mock_get_item):
        mock_get_item.side_effect = ClientError({}, 'mock')

        with self.assertRaisesRegex(InternalError, 'mock'):
            get_relationship_response('judges')

    # - test_get_relationship_response: Exception->InternalError
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_validate_relationship_id_fail_4(self, mock_get_item):
        mock_get_item.side_effect = Exception(
            'mock')

        with self.assertRaisesRegex(InternalError, 'mock'):
            get_relationship_response('judges')

    # + validate_create_event - success - Previous event is OBJECT REMOVE
    @patch('service_commons.object_command.get_previous_and_current_events')
    def test_validate_create_event_success_1(self, get_events_mock):
        previous_event = {'event-name': event_names.OBJECT_REMOVE}
        current_event = {'current-event': event_names.OBJECT_CREATE}
        get_events_mock.return_value = (previous_event, current_event)
        collection_id = 'collection-id'
        object_id = 'object-id'
        request_id = 'event-123'
        self.assertIsNone(validate_create_event(request_id, object_id, collection_id))

    # + validate_create_event - success - No previous event exist and object doesn't exist
    @patch('service_commons.object_command.get_previous_and_current_events')
    @patch('service_commons.object_command.get_object_item')
    def test_validate_create_event_success_2(self, get_object_mock, get_events_mock):
        previous_event = {}
        current_event = {'current-event': event_names.OBJECT_CREATE}
        get_object_mock.return_value = {}
        get_events_mock.return_value = (previous_event, current_event)
        collection_id = 'collection-id'
        object_id = 'object-id'
        request_id = 'event-123'
        self.assertIsNone(validate_create_event(request_id, object_id, collection_id))

    # - validate_create_event - fail - Previous event is OBJECT CREATE - duplicated create event
    @patch('service_commons.object_command.get_previous_and_current_events')
    def test_validate_create_event_fail_1(self, get_events_mock):
        previous_event = {'event-name': event_names.OBJECT_CREATE}
        current_event = {'current-event': event_names.OBJECT_CREATE}
        get_events_mock.return_value = (previous_event, current_event)
        collection_id = 'collection-id'
        object_id = 'object-id'
        request_id = 'event-123'
        with self.assertRaisesRegex(SemanticError,
                                    "Object ID {} already exist in Collection ID {}".format(object_id, collection_id)):
            self.assertIsNone(validate_create_event(request_id, object_id, collection_id))

    # - validate_create_event - fail - No previous event exist, but object state is Created
    @patch('service_commons.object_command.get_previous_and_current_events')
    @patch('service_commons.object_command.get_object_item')
    def test_validate_create_event_fail_2(self, get_object_mock, get_events_mock):
        previous_event = {}
        current_event = {'current-event': event_names.OBJECT_CREATE}
        get_object_mock.return_value = {'object-state': object_status.CREATED}
        get_events_mock.return_value = (previous_event, current_event)
        collection_id = 'collection-id'
        object_id = 'object-id'
        request_id = 'event-123'
        with self.assertRaisesRegex(SemanticError,
                                    "Object ID {} already exist in Collection ID {}".format(object_id, collection_id)):
            validate_create_event(request_id, object_id, collection_id)

    # +determine_upsert_event_name - Previous Pending Event in the Tracking Table is CREATE
    def test_determine_event_name_success_1(self):
        self.assertEqual(event_names.OBJECT_UPDATE,
                         determine_upsert_event_name(previous_event_name=event_names.OBJECT_CREATE, object_state='',
                                                     is_large_or_multipart=False))

    # +determine_upsert_event_name - Previous Pending Event in the Tracking Table is UPDATE
    def test_determine_event_name_success_2(self):
        self.assertEqual(event_names.OBJECT_UPDATE,
                         determine_upsert_event_name(previous_event_name=event_names.OBJECT_UPDATE, object_state='',
                                                     is_large_or_multipart=False))

    # +determine_upsert_event_name - Previous Pending Event in the Tracking Table is REMOVE
    def test_determine_event_name_success_4(self):
        self.assertEqual(event_names.OBJECT_CREATE,
                         determine_upsert_event_name(previous_event_name=event_names.OBJECT_REMOVE, object_state='',
                                                     is_large_or_multipart=False))

    # +determine_upsert_event_name - NO Previous Pending Event in the Tracking Table, and Object State = CREATED
    def test_determine_event_name_success_3(self):
        self.assertEqual(event_names.OBJECT_UPDATE,
                         determine_upsert_event_name(previous_event_name='', object_state=object_status.CREATED,
                                                     is_large_or_multipart=False))

    # +determine_upsert_event_name - NO Previous Pending Event in the Tracking Table, and Object State = REMOVED
    def test_determine_event_name_success_5(self):
        self.assertEqual(event_names.OBJECT_CREATE,
                         determine_upsert_event_name(previous_event_name='', object_state=object_status.REMOVED,
                                                     is_large_or_multipart=False))

    # +determine_upsert_event_name - NO Previous Pending Event in the Tracking Table, and NO Object exist
    def test_determine_event_name_success_6(self):
        self.assertEqual(event_names.OBJECT_CREATE,
                         determine_upsert_event_name(previous_event_name='', object_state='',
                                                     is_large_or_multipart=False))

    # +determine_upsert_event_name - NO Previous Pending Event in the Tracking Table,
    # and NO Object exist and is_large_or_multipart True
    def test_determine_event_name_success_7(self):
        self.assertEqual(event_names.OBJECT_CREATE_PENDING,
                         determine_upsert_event_name(previous_event_name='', object_state='',
                                                     is_large_or_multipart=True))

    # +get_previous_and_current_events - success
    @patch("lng_datalake_commons.tracking.tracker.generate_tracking_id")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_previous_and_current_events_success(self, query_items_mock, tracking_id_mock):
        current_event = {'tracking-id': 'object-id|collection-id_object',
                         'event-id': 'current_event_id',
                         'request-time': '2018-05-19T20:01:29.000Z',
                         'event-name': event_names.OBJECT_UPDATE}
        previous_event = {'tracking-id': 'object-id|collection-id_object',
                          'event-id': 'previous_event_id',
                          'request-time': '2018-05-19T19:31:17.000Z',
                          'event-name': event_names.OBJECT_CREATE}
        query_items_mock.return_value = [current_event, previous_event]
        tracking_id_mock.return_value = 'object-id|collection-id_object'

        self.assertEqual((previous_event, current_event),
                         get_previous_and_current_events('current_event_id', 'object-id',
                                                         'collection-id'))

    # +get_previous_and_current_events - no previous pending event exist
    @patch("lng_datalake_commons.tracking.tracker.generate_tracking_id")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_previous_and_current_events_not_existing_previous(self, query_items_mock, tracking_id_mock):
        current_event = {'tracking-id': 'object-id|collection-id_object',
                         'event-id': 'current_event_id',
                         'request-time': '2018-05-19T20:01:29.000Z',
                         'event-name': event_names.OBJECT_UPDATE}
        query_items_mock.return_value = [current_event]
        tracking_id_mock.return_value = 'object-id|collection-id_object'

        self.assertEqual(({}, current_event),
                         get_previous_and_current_events('current_event_id', 'object-id',
                                                         'collection-id'))

    # -get_previous_and_current_events - InternalError - failed to query Tracking Table
    @patch("lng_datalake_commons.tracking.tracker.generate_tracking_id")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_previous_and_current_events_failed_query(self, query_items_mock, tracking_id_mock):
        query_items_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        tracking_id_mock.return_value = 'object-id|collection-id_object'
        object_id = 'object-id'
        collection_id = 'collection-id'
        event_id = 'current_event_id'
        with self.assertRaisesRegex(InternalError,
                                    "Failed to query items in Tracking Table by Tracking ID {}|{}_object".format(
                                        object_id, collection_id)):
            get_previous_and_current_events(event_id, object_id, collection_id)

    # -get_previous_and_current_events - InternalError - unhandled exception when querying Tracking Table
    @patch("lng_datalake_commons.tracking.tracker.generate_tracking_id")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_previous_and_current_events_unhandled_exception(self, query_items_mock, tracking_id_mock):
        query_items_mock.side_effect = Exception
        tracking_id_mock.return_value = 'object-id|collection-id_object'
        object_id = 'object-id'
        collection_id = 'collection-id'
        event_id = 'current_event_id'
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_previous_and_current_events(event_id, object_id, collection_id)

    # -get_previous_and_current_events - InternalError - Event ID doesn't exist in the Tracking Table
    @patch("lng_datalake_commons.tracking.tracker.generate_tracking_id")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.query_items")
    def test_get_previous_and_current_events_no_event_id_tracked(self, query_items_mock, tracking_id_mock):
        query_items_mock.return_value = []
        tracking_id = 'object-id|collection-id_object'
        tracking_id_mock.return_value = tracking_id
        object_id = 'object-id'
        collection_id = 'collection-id'
        event_id = 'current_event_id'
        with self.assertRaisesRegex(InternalError,
                                    "Event ID {} doesn't exist in the Tracking Table for Tracking ID {}".format(
                                        event_id, tracking_id)):
            get_previous_and_current_events(event_id, object_id, collection_id)

    # +get_object_item - success
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_item_success(self, ost_get_mock):
        ost_item = io_utils.load_data_json('objectStoreTable.get_item.valid.json')
        ost_get_mock.return_value = ost_item
        object_id = '49cf62814f629ba0a679147d8784b516cde30a17'
        collection_id = '274'
        self.assertDictEqual(get_object_item(object_id, collection_id), ost_item)

    # -get_object_item - ClientError exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_item_fail_client_error(self, ost_get_mock):
        ost_get_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                "Error": {"Code": "mock error code",
                                                          "Message": "mock error message"}},
                                               "client-error-mock")
        with self.assertRaisesRegex(InternalError, 'Unable to get item from Object Store Table'):
            get_object_item("mock-object-id", "mock-collection-id")

    # -get_object_item - General Exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_item_fail_general_exception(self, ost_get_mock):
        ost_get_mock.side_effect = Exception
        with self.assertRaisesRegex(InternalError, 'Unhandled exception occurred'):
            get_object_item("mock-object-id", "mock-collection-id")

    # + get_changeset_object_item - success
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_get_changeset_object_item_success(self, cot_get_mock):
        cot_item = io_utils.load_data_json('changesetObjectTable.get_item.valid.json')
        cot_get_mock.return_value = cot_item
        object_id = 'test-object'
        collection_id = 'test-collection'
        changeset_id = 'test-changeset'
        version_number = 1
        self.assertDictEqual(get_changeset_object_item(object_id, collection_id, changeset_id, version_number),
                             cot_item)

    # - get_changeset_object_item - ClientError exception
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_get_changest_object_item_fail_client_error(self, cot_get_mock):
        cot_get_mock.side_effect = ClientError({"ResponseMetaData": {},
                                                "Error": {"Code": "mock error code",
                                                          "Message": "mock error message"}},
                                               "client-error-mock")
        with self.assertRaisesRegex(InternalError, "Unable to get item from Changeset Object Table"):
            get_changeset_object_item("mock-changeset-id", "mock-collection-id", "mock-changeset-id",
                                      "mock_version-number")

    # - get_changeset_object_item - General Exception
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_get_changeset_object_item_fail_general_exception(self, cot_get_mock):
        cot_get_mock.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_changeset_object_item("mock-changeset-id", "mock-collection-id", "mock-changeset-id",
                                      "mock_version-number")

    # - generate_sts_role_token - Failure
    @patch("lng_aws_clients.sts.get_client")
    def test_generate_sts_role_token_failure(self, mock_sts_client):
        mock_sts_client.side_effect = ClientError({"ResponseMetadata": {},
                                                   "Error": {"Code": "mock error code",
                                                             "Message": "mock error message"}},
                                                  "client-error-mock")
        with self.assertRaisesRegex(InternalError, "Unable to generate role credentials."):
            generate_sts_role_token("staging_bucket", "staging_prefix", "role_ARN", 123, "request_id")

    # + validate_object_id - Valid
    def test_validate_object_id_valid(self):
        self.assertIsNone(validate_object_id("foobar"))

    # + validate_object_id - Valid
    def test_validate_object_id_valid_2(self):
        self.assertIsNone(validate_object_id("_"))

    # - validate_object_id - Invalid
    def test_validate_object_id_fail(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Object ID"):
            validate_object_id('')

    # - validate_object_id - Invalid
    def test_validate_object_id_fail_2(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Object ID"):
            validate_object_id('+')

    # - encode_md5 - Exception
    def test_encode_md5_fail(self):
        with self.assertRaisesRegex(InternalError, "Failed to create an encoded md5 hash"):
            encode_md5(-1)

    # + extend_event_ttl - success - Extend the TTL of previous event and remove current event
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    @patch('service_commons.object_common.get_expiration_epoch_folder')
    def test_extend_event_ttl_success_1(self, exp_epoch_mock, batch_write_mock):
        batch_write_mock.return_value = None
        exp_epoch_mock.return_value = 999999
        previous_event = {'tracking-id': 'previous-event',
                          'request-time': '2019-03-14T19:48:03.967Z',
                          'pending-expiration-epoch': 1234567,
                          'event-id': 'event-123',
                          'event-name': event_names.OBJECT_CREATE_FOLDER}
        current_event = {'tracking-id': 'previous-event',
                         'request-time': '2019-04-14T20:01:09.202Z',
                         'event-name': event_names.OBJECT_CREATE_FOLDER,
                         'event-id': 'event-456'}
        self.assertIsNone(extend_event_ttl(previous_event, current_event))

        # new TTL assigned to previous event
        previous_event['pending-expiration-epoch'] = 999999
        batch_write_mock.call_once_with([previous_event], [current_event])

    # - extend_event_ttl - fail - ClientError
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    @patch('service_commons.object_common.get_expiration_epoch_folder')
    def test_extend_event_ttl_client_error(self, exp_epoch_mock, batch_write_mock):
        batch_write_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        exp_epoch_mock.return_value = 999999
        previous_event = {'tracking-id': 'previous-event',
                          'request-time': '2019-03-14T19:48:03.967Z',
                          'pending-expiration-epoch': 1234567,
                          'event-id': 'event-123',
                          'event-name': event_names.OBJECT_CREATE_FOLDER}
        current_event = {'tracking-id': 'previous-event',
                         'request-time': '2019-04-14T20:01:09.202Z',
                         'event-name': event_names.OBJECT_UPDATE_FOLDER,
                         'event-id': 'event-123'}
        with self.assertRaisesRegex(InternalError, "Unable to put item in Tracking Table"):
            extend_event_ttl(previous_event, current_event)

    # - extend_event_ttl - fail - General Exception
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    @patch('service_commons.object_common.get_expiration_epoch_folder')
    def test_extend_event_ttl_general_exception(self, exp_epoch_mock, batch_write_mock):
        batch_write_mock.side_effect = Exception
        exp_epoch_mock.return_value = 999999
        previous_event = {'tracking-id': 'previous-event',
                          'request-time': '2019-03-14T19:48:03.967Z',
                          'pending-expiration-epoch': 1234567,
                          'event-id': 'event-123',
                          'event-name': event_names.OBJECT_CREATE_FOLDER}
        current_event = {'tracking-id': 'previous-event',
                         'request-time': '2019-04-14T20:01:09.202Z',
                         'event-name': event_names.OBJECT_UPDATE_FOLDER,
                         'event-id': 'event-123'}
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            extend_event_ttl(previous_event, current_event)

    @patch('lng_aws_clients.s3.put_s3_object')
    def test_put_object_to_staging_bucket_client_error_fail(self, put_s3_object_mock):
        object_content = 'content'
        staging_bucket_name = 'test_bucket_use1'
        object_key = 'keykeykeykeykey'
        put_s3_object_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {
                                                          'Code': 'OTHER',
                                                          'Message': 'This is a mock'}},
                                                     "FAKE")

        with self.assertRaisesRegex(InternalError, "Unable to create S3 object*"):
            put_object_to_staging_bucket(object_content, staging_bucket_name, object_key, "text/plain")

    @patch('lng_aws_clients.s3.put_s3_object')
    def test_put_object_to_staging_bucket_general_exception_fail(self, put_s3_object_mock):
        object_content = 'content'
        staging_bucket_name = 'test_bucket_use1'
        object_key = 'keykeykeykeykey'
        put_s3_object_mock.side_effect = Exception

        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            put_object_to_staging_bucket(object_content, staging_bucket_name, object_key, "text/plain")

    # + empty metdata
    def test_validate_object_metadata_success1(self):
        object_metadata = {}
        self.assertIsNone(validate_object_metadata(object_metadata))

    # + success
    def test_validate_object_metadata_success2(self):
        object_metadata = {"name1": "value1",
                           "name2": "value2"}
        self.assertIsNone(validate_object_metadata(object_metadata))

    # - key starts with "dl-"
    def test_validate_object_metadata_fail1(self):
        object_metadata = {"dl-name1": "value1",
                           "name2": "value2"}
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid object metadata\|\|Object metadata keys cannot begin with dl-"):
            validate_object_metadata(object_metadata)

    # - metadata exceeds 1024 characters
    def test_validate_object_metadata_fail2(self):
        object_metadata = {"name111111111111111111111111111111111111111111111111111111111111111111111111111": "value1",
                           "name222222222222222222222222222222222222222222222222222222222222222222222222222": "value2",
                           "name333333333333333333333333333333333333333333333333333333333333333333333333333": "value3",
                           "name444444444444444444444444444444444444444444444444444444444444444444444444444": "value4",
                           "name555555555555555555555555555555555555555555555555555555555555555555555555555": "value5",
                           "name666666666666666666666666666666666666666666666666666666666666666666666666666": "value6",
                           "name777777777777777777777777777777777777777777777777777777777777777777777777777": "value7",
                           "name888888888888888888888888888888888888888888888888888888888888888888888888888": "value8",
                           "name999999999999999999999999999999999999999999999999999999999999999999999999999": "value9",
                           "nameAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA": "valueA",
                           "nameBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB": "valueB",
                           "nameCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC": "valueC",
                           "nameDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD": "valueD"
                           }
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid object metadata\|\|Object metadata length"):
            validate_object_metadata(object_metadata)

    # - validate_object_metadata - value is non ASCII
    def test_validate_object_metadata_fail3(self):
        # Note the space in name2 value isn't actually a space it's a non breaking space (\xa0)
        object_metadata = {"name1": "value1",
                           "name2": "BSAUpdateﾠ"}
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid object metadata\|\|Object metadata key/value must be US-ASCII"):
            validate_object_metadata(object_metadata)

    # - validate_and_get_changeset_id: Client Error
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_client_error(self, mock_changeset_get_item):
        mock_changeset_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                           'Error': {
                                                               'Code': 'OTHER',
                                                               'Message': 'This is a mock'}},
                                                          "FAKE")
        with self.assertRaisesRegex(InternalError, 'Unable to get item from Changeset Table'):
            validate_and_get_changeset_id('abc', 1)

    # - validate_and_get_changeset_id: General Exception
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_idexception(self, mock_changeset_get_item):
        mock_changeset_get_item.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(InternalError, 'Mock Exception'):
            validate_and_get_changeset_id('abc', 1)

    # - validate_and_get_changeset_id: invalid changeset id
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_invalid_changeset_id(self, mock_changeset_get_item):
        mock_changeset_get_item.return_value = {}
        with self.assertRaisesRegex(InvalidRequestPropertyValue, 'does not exist'):
            validate_and_get_changeset_id('abc', 1)

    # - validate_and_get_changeset_id: closed changeset
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_closed_changeset(self, mock_changeset_get_item):
        mock_changeset_get_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Closed'
        }
        with self.assertRaisesRegex(InvalidRequestPropertyValue, 'abc is in Closed not Opened state'):
            validate_and_get_changeset_id('abc', 100)

    # - validate_and_get_changeset_id: NotAuthorizedError
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_not_auth(self, mock_changeset_get_item):
        sample_changeset = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }
        mock_changeset_get_item.return_value = sample_changeset

        with self.assertRaisesRegex(NotAuthorizedError, 'does not have permission to modify Objects'):
            validate_and_get_changeset_id('jek-changeset', 1)

    # + validate_and_get_changeset_id: success
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_success(self, mock_changeset_get_item):
        sample_changeset = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }
        mock_changeset_get_item.return_value = sample_changeset

        self.assertDictEqual(validate_and_get_changeset_id('jek-changeset', 100), sample_changeset)


if __name__ == '__main__':
    unittest.main()
