import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils

import service_commons.object_common as common_object_module
from service_commons.object_common import validate_object_id, build_object_key_and_hashes, build_object_metadata, build_large_object_key, \
    build_multipart_object_key, build_multipart_ingestion_key, build_batch_key, build_object_key_url, \
    build_folder_object_transaction_id, generate_object_expiration_time, generate_object_store_buckets, \
    build_collection_id_hash, build_staging_prefix, is_valid_content_type, object_contents_match, is_expired_object

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__)


class ServiceCommonsObjectTest(TestCase):
    session = None

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(common_object_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # +validate_object_id - valid
    def test_validate_object_id_valid(self):
        self.assertEqual("", validate_object_id("my,valid-test(2)_rocks"))

    # -validate_object_id - invalid
    def test_validate_object_id_invalid_char(self):
        self.assertEqual("Object ID contains invalid characters", validate_object_id("my,invalid-test(2)_===rocks"))

    # -validate_object_id - invalid too short
    def test_validate_object_id_short(self):
        self.assertEqual("Object ID length be between 1 and 256 characters", validate_object_id(""))

    # -validate_object_id - invalid too long
    def test_validate_object_id_long(self):
        self.assertEqual("Object ID length be between 1 and 256 characters", validate_object_id(io_utils.generate_message_of_len(257)))

    # +build_object_key_from_content - valid
    def test_build_object_key_from_content_valid(self):
        self.assertEqual(build_object_key_and_hashes('mock data'.encode('utf-8'), "text/plain", "abc",
                                                     "2019-08-22T13:31:17.123Z", {"meta1": 1}, "abcdefg"),
                         ('21e7dcdaaedac9b71542a3a238b685de2e8f1aae',
                          '8d3a007e8f25a96f46fd820bce9b53ff13faa9eb',
                          'abcdefg/c100525a15ab052ef0797c6e09f0f8cdf957c9c0'))

    # +build_object_metadata - valid
    def test_build_object_metadata(self):
        collection_id = "collection"
        object_id = "object"
        version_timestamp = "2019-08-23T09:00:00.123Z"
        version_number = 1
        content_sha1 = "abcdefghijk"
        user_metadata = {
            "test": "123"
        }
        self.assertEqual(build_object_metadata(collection_id, object_id, version_timestamp, version_number,
                                               content_sha1, user_metadata),
                         {
                             "dl-collection-id": collection_id,
                             "dl-object-id": object_id,
                             "dl-version-timestamp": version_timestamp,
                             "dl-version-number": "1",
                             "dl-content-sha1": content_sha1,
                             "test": "123"
                         })

    # +build_large_object_key - valid
    def test_build_large_object_key(self):
        self.assertEqual(build_large_object_key("testing", 'stage'), "large-object/stage/testing")

    # +build_multipart_object_key - valid
    def test_build_multipart_object_key(self):
        self.assertEqual(build_multipart_object_key("testing", 'stage'), "multipart-object/stage/testing")

    # +build_multipart_ingestion_key - valid
    def test_build_multipart_ingestion_key(self):
        self.assertEqual(build_multipart_ingestion_key("testing", 'stage'), "ingestion/stage/testing")

    # +build_batch_key_success - valid
    def test_build_batch_key_success(self):
        self.assertEqual(build_batch_key("testing", 'stage'), "batch/stage/testing")

    def test_build_object_key_url(self):
        content_type = 'text/plain'
        object_key = 'object-key'
        self.assertEqual('https://datalake_url.com/objects/store/object-key',
                         build_object_key_url(object_key, content_type))

    def test_build_object_key_url_folder_object(self):
        content_type = 'multipart/mixed'
        object_key = 'folder-object-key'
        self.assertEqual('https://datalake_url.com/objects/store/folder-object-key/',
                         build_object_key_url(object_key, content_type))

    # + build_folder_object_transaction_id - Valid
    def test_build_folder_object_transaction_id_valid(self):
        transaction_id = "bb6898d1-201f-11e9-9425-bf25ef11904e"
        owner_id = 12
        collection_id = "test-collection"
        stage = "v1"
        # Base64 coded dict
        expected_id = "eyJ0cmFuc2FjdGlvbi1pZCI6ImJiNjg5OGQxLTIwMWYtMTFlOS05NDI1LWJmMjVlZjExOTA0ZSIsImNvbGxlY3Rpb24taW" \
                      "QiOiJ0ZXN0LWNvbGxlY3Rpb24iLCJvd25lci1pZCI6MTIsInN0YWdlIjoidjEifQ=="
        self.assertEqual(build_folder_object_transaction_id(transaction_id, collection_id, owner_id, stage, {}),
                         expected_id)

    # Need to figure out why Jenkins subtracts 4 hours when converting time using UTC
    # + test_generate_object_expiration_time - Valid
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_object_expiration_time(self, request_time_mock):
        request_time_mock.return_value = '2018-05-25T23:31:17.1Z'

        collection_dict = {
            "collection-id": "TestCollection",
            'object-expiration': '168h'
        }
        expected = 1527895877
        self.assertEqual(expected, generate_object_expiration_time(collection_dict))

    def test_generate_object_store_buckets(self):
        bucket_name = 'test-datalake-bucket'
        replicated_regions = generate_object_store_buckets(bucket_name)
        expected_regions = {
            'us-east-1': '{}-use1'.format(bucket_name),
            'us-west-2': '{}-usw2'.format(bucket_name),
        }
        self.assertDictEqual(replicated_regions, expected_regions)

    # +build_object_key_from_content - valid
    def test_build_collection_id_hash(self):
        self.assertEqual(build_collection_id_hash('mock data'),
                         '21e7dcdaaedac9b71542a3a238b685de2e8f1aae')

    # +build_staging_prefix - success
    def test_build_staging_prefix(self):
        self.assertEqual(build_staging_prefix("folder_prefix", "collection", "event", "object"),
                         "folder_prefix/collection_event_object")

    # +is_valid_content_type - valid
    def test_is_valid_content_type_valid(self):
        self.assertTrue(is_valid_content_type('application/json'))
        self.assertTrue(is_valid_content_type('image/jpeg; charset=iso-8859-1'))
        self.assertTrue(is_valid_content_type('application/rtf; charset=utf8'))
        self.assertTrue(is_valid_content_type('application/rtf'))
        self.assertTrue(is_valid_content_type(' application/msword '))

    # -is_valid_content_type - invalid
    def test_is_valid_content_type_invalid(self):
        self.assertFalse(is_valid_content_type('application/x-www-form-urlencoded; charset=iso-8859-1'))
        self.assertFalse(is_valid_content_type('application/jpeg; charset=iso-8859-1'))
        self.assertFalse(is_valid_content_type(''))

    # + object_contents_match
    def test_object_contents_match_pass(self):
        ost_item = {
            "object-id": "aaron",
            "collection-id": "274",
            "object-state": "Created",
            "bucket-name": "mock_datalake_bucket-use1",
            "object-key": "4ad4042dac8ccaa10f2401d2916f5f605d69bfa3a",
            "content-type": "text/plain",
            "version-timestamp": "2018-05-12T15:17:05.629Z",
            "raw-content-length": 17,
            "version-number": 2,
            "collection-hash": "431bf3b995a99c2cd6899b97187d1542a965cec9",
            "replicated-buckets": [
                "mock_datalake_bucket-usw2"
            ]
        }

        # OST item empty
        self.assertFalse(object_contents_match("xyz/c18g12f562c24df65f584f1f202b90d9d340f63a",
                                               "abc",
                                               "c17f62e562c24df65f584f1f202b90d9d340f63a",
                                               {}))

        # OST doesn't contain object-hash and content-sha1 matches object-key in OST
        self.assertFalse(object_contents_match("xyz/c18g12f562c24df65f584f1f202b90d9d340f63a",
                                               "abc",
                                               "4ad4042dac8ccaa10f2401d2916f5f605d69bfa3a",
                                               ost_item))

        # OST has object-hash and object-hash values match
        ost_item['object-hash'] = "abc"
        ost_item['content-sha1'] = "c17f62e562c24df65f584f1f202b90d9d340f63a"
        self.assertTrue(object_contents_match("xyz/c18g12f562c24df65f584f1f202b90d9d340f63a",
                                              "abc",
                                              "c17f62e562c24df65f584f1f202b90d9d340f63a",
                                              ost_item))

        # OST has object-hash and object-hash values do not match
        ost_item['object-hash'] = "abcd"
        self.assertFalse(object_contents_match("xyz/c18g12f562c24df65f584f1f202b90d9d340f63a",
                                               "abc",
                                               "c17f62e562c24df65f584f1f202b90d9d340f63a",
                                               ost_item))

        # OST status is Removed
        ost_item['object-state'] = "Removed"
        ost_item.pop('object-hash')
        self.assertFalse(object_contents_match("xyz/c18g12f562c24df65f584f1f202b90d9d340f63a",
                                               "abc",
                                               "c17f62e562c24df65f584f1f202b90d9d340f63a",
                                               ost_item))
    # -is_valid_content_type - invalid
    def test_is_expired_object(self):
        object_store_item = {
            "pending-expiration-epoch": 1571630400
            }
        self.assertTrue(is_expired_object(object_store_item, "2019-10-22T15:17:05.629923"))
        self.assertFalse(is_expired_object(object_store_item, "2019-10-19T15:17:05.629923"))

if __name__ == '__main__':
    unittest.main()
