import errno
import unittest
import zipfile
from unittest import TestCase
from unittest.mock import patch, Mock

from botocore.exceptions import ClientError, ReadTimeoutError, EndpointConnectionError
from botocore.response import StreamingBody
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import ingestion_status
from lng_datalake_testhelper.io_utils import IOUtils
from urllib3.exceptions import ProtocolError

from service_commons.ingestion import get_message, get_ingestion_data, update_ingestion_state, \
    validate_zip_magic_number, read_zip_central_directory, update_error_counter, \
    _read_streaming_body, is_object_in_datalake, delete_collection_blocker, create_object_backend_event_id

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'Ingestion')


class ServiceCommonsObjectTest(TestCase):
    session_patch = None

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    ####################################################################################################################
    # +test_get_message_success
    def test_get_message_success(self):
        event = io_utils.load_data_json('example.event.valid.json')
        expected_message = io_utils.load_data_json('get_message.message.valid.json')
        actual_message = get_message(event)
        self.assertDictEqual(actual_message, expected_message)

    # -test_get_message too many
    def test_get_message_failure(self):
        event = io_utils.load_data_json('example.event.invalid.json')
        with self.assertRaisesRegex(TerminalErrorException, "Ingestion Get Message only handles a single SQS message and not a batch"):
            get_message(event)

    # -test_get_message_key_error: KeyError->TerminalErrorException
    def test_get_message_key_error(self):
        event = io_utils.load_data_json('example.event.key_error.json')
        with self.assertRaisesRegex(TerminalErrorException, 'Error getting message from record'):
            get_message(event)

    # -test_get_message_value_type_error: ValueError/TypeError->TerminalErrorException
    @patch("orjson.loads")
    def test_get_message_value_type_error(self, json_loads_mock):
        event = io_utils.load_data_json('example.event.valid.json')
        for side_effect in [ValueError, TypeError]:
            json_loads_mock.side_effect = [{'Message': 'test'}, side_effect]
            with self.assertRaisesRegex(TerminalErrorException, 'Cannot load message'):
                get_message(event)

    ####################################################################################################################
    # +test_get_ingestion_data_success: Returns ingestion item
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_data_success(self, get_ingestion_item_mock):
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingestion_id = ingestion_item['ingestion-id']
        get_ingestion_item_mock.return_value = ingestion_item
        self.assertDictEqual(get_ingestion_data(ingestion_id), ingestion_item)
        expected_param = {'ingestion-id': ingestion_id}
        get_ingestion_item_mock.assert_called_once_with(expected_param, ConsistentRead=True)

    # -test_get_ingestion_data_client_error: EndpointConnectionError->RetryEventException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_data_endpoint_connection_error_exception(self, get_ingestion_data_mock):
        get_ingestion_data_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(RetryEventException, 'Unable to get item from Ingestion Table'):
            get_ingestion_data(ingestion_id)

    # -test_get_ingestion_data_client_error: ClientError->RetryEventException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_data_client_error(self, get_ingestion_data_mock):
        get_ingestion_data_mock.side_effect = ClientError({}, "client error message")
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(RetryEventException, 'Unable to get item from Ingestion Table'):
            get_ingestion_data(ingestion_id)

    # -test_get_ingestion_data_exception_error: Exception->TerminalErrorException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_data_exception_error(self, get_ingestion_data_mock):
        get_ingestion_data_mock.side_effect = Exception({}, "exception error message")
        with self.assertRaisesRegex(TerminalErrorException, 'exception error message'):
            get_ingestion_data('test-ingestion-id')

    # -test_get_ingestion_data_terminal_error: Ingestion item empty -> TerminalErrorException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_data_terminal_error(self, get_ingestion_data_mock):
        ingestion_id = 'test-ingestion-id'
        get_ingestion_data_mock.return_value = None
        with self.assertRaisesRegex(TerminalErrorException, 'Ingestion ID does not exist in Ingestion Table'):
            get_ingestion_data(ingestion_id)

    # -test_get_ingestion_data_retry_error: Ingestion item empty -> RetryEventException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_data_retry_error(self, get_ingestion_data_mock):
        ingestion_id = 'test-ingestion-id'
        get_ingestion_data_mock.return_value = None
        with self.assertRaisesRegex(RetryEventException, 'Ingestion ID does not exist in Ingestion Table'):
            get_ingestion_data(ingestion_id, True)

    ####################################################################################################################
    # +test_update_ingestion_state_success
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_update_ingestion_state_success(self, update_ingestion_mock):
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingestion_state = ingestion_status.COMPLETED
        self.assertIsNone(update_ingestion_state(ingestion_item, ingestion_state))
        ingestion_item['ingestion-state'] = ingestion_state
        update_ingestion_mock.assert_called_once_with(ingestion_item)

    # +test_update_ingestion_state_success with error description
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_update_ingestion_state_success2(self, update_ingestion_mock):
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingestion_state = ingestion_status.ERROR
        ingestion_error_description = "Sample error"
        self.assertIsNone(update_ingestion_state(ingestion_item, ingestion_state, ingestion_error_description))
        ingestion_item['ingestion-state'] = ingestion_state
        ingestion_item['error-description'] = ingestion_error_description
        update_ingestion_mock.assert_called_once_with(ingestion_item)

    # -test_update_ingestion_state_client_error: EndpointConnectionError->RetryEventException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_update_ingestion_state_endpoint_connection_error_exception(self, update_ingestion_mock):
        update_ingestion_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingestion_state = ingestion_status.COMPLETED
        with self.assertRaisesRegex(RetryEventException, 'Unable to update item in Ingestion Table'):
            update_ingestion_state(ingestion_item, ingestion_state)

    # -test_update_ingestion_state_client_error: ClientError->RetryEventException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_update_ingestion_state_client_error(self, update_ingestion_mock):
        update_ingestion_mock.side_effect = ClientError({}, "exception error message")
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingestion_state = ingestion_status.COMPLETED
        with self.assertRaisesRegex(RetryEventException, 'Unable to update item in Ingestion Table'):
            update_ingestion_state(ingestion_item, ingestion_state)

    # -test_update_ingestion_state_exception_error: Exception->TerminalErrorException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_update_ingestion_state_exception_error(self, update_ingestion_mock):
        update_ingestion_mock.side_effect = Exception("exception error message")
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingestion_state = ingestion_status.COMPLETED
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to update item in Ingestion Table'):
            update_ingestion_state(ingestion_item, ingestion_state)

    ####################################################################################################################
    # +test_validate_zip_magic_number_success
    @patch('lng_aws_clients.s3.get_client')
    def test_validate_zip_magic_nubmer_success(self, s3_get_client_mock):
        s3_get_client_mock.return_value.get_object.return_value['Body'].read.return_value = b'PK\x03\x04'
        self.assertIsNone(validate_zip_magic_number('test-bucket-name', 'test-object-key'))

    # -test_validate_zip_magic_number_endpoint_connection_error_exception: EndpointConnectionError->RetryException
    @patch('lng_aws_clients.s3.get_client')
    def test_validate_zip_magic_number_endpoint_connection_error_exception(self, s3_get_client_mock):
        s3_get_client_mock.return_value.get_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        bucket_name = 'test-bucket-name'
        object_key = 'test-object-key'
        error_msg = 'Unable to read S3 object {0} from bucket {1}'.format(object_key, bucket_name)
        with self.assertRaisesRegex(RetryEventException, error_msg):
            validate_zip_magic_number(bucket_name, object_key)

    # -test_validate_zip_magic_number_client_slowdown_error: ClientError->RetryException
    @patch('lng_aws_clients.s3.get_client')
    def test_validate_zip_magic_number_client_slowdown_error(self, s3_get_client_mock):
        s3_get_client_mock.return_value.get_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                              'Error': {
                                                                                  'Code': 'SlowDown',
                                                                                  'Message': 'SlowDown'}},
                                                                             "FAKE")
        bucket_name = 'test-bucket-name'
        object_key = 'test-object-key'
        error_msg = 'Unable to read S3 object {0} from bucket {1}'.format(object_key, bucket_name)
        with self.assertRaisesRegex(RetryEventException, error_msg):
            validate_zip_magic_number(bucket_name, object_key)

    # -test_validate_zip_magic_number_client_not_found_error: ClientError->TerminalErrorException
    @patch('lng_aws_clients.s3.get_client')
    def test_validate_zip_magic_number_client_not_found_error(self, s3_get_client_mock):
        s3_get_client_mock.return_value.get_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                              'Error': {
                                                                                  'Code': 'NoSuchKey',
                                                                                  'Message': 'Not Found'}},
                                                                             "FAKE")
        bucket_name = 'test-bucket-name'
        object_key = 'test-object-key'
        error_msg = 'S3 object {0} not found in bucket {1}'.format(object_key, bucket_name)
        with self.assertRaisesRegex(TerminalErrorException, error_msg):
            validate_zip_magic_number(bucket_name, object_key)

    # -test_validate_zip_magic_number_magic_number_error - magic number is not expected value - TerminalErrorException
    @patch('lng_aws_clients.s3.get_client')
    def test_validate_zip_magic_number_magic_number_error(self, s3_get_client_mock):
        # Mock read to return nulls
        s3_get_client_mock.return_value.get_object.return_value['Body'].read.return_value = b'PK\x00\x00'
        bucket_name = 'test-bucket-name'
        object_key = 'test-object-key'
        error_msg = 'Error reading zip file {}'.format(object_key)
        with self.assertRaisesRegex(TerminalErrorException, error_msg):
            validate_zip_magic_number(bucket_name, object_key)

    ####################################################################################################################
    # +test_read_zip_central_directory_success
    @patch('service_commons.ingestion.zipfile.ZipFile')
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_success(self, s3_get_client_mock, zip_file_mock):
        input_zip_len = 6786
        s3_get_client_mock.return_value.head_object.return_value = {'ContentLength': input_zip_len}
        body_mock = Mock()
        body_mock.read.return_value = b'zip file contents for unit test PK\x01\x02xxxxxxxxxxxxxxxxx'
        s3_get_client_mock.return_value.get_object.return_value = {'Body': body_mock}
        zip_file_mock.infolist.return_value = []
        (info_list, unread_len, cd_len) = read_zip_central_directory('bucket-name', 'object-key', input_zip_len)
        self.assertTrue(unread_len == 0)
        self.assertEqual(cd_len, 16352)

    # -test_read_zip_central_directory_zip: EndpointConnectionError->TerminalErrorException
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_zip_endpoint_connection_error_exception(self, s3_get_client_mock):
        input_zip_len = 6786
        s3_get_client_mock.return_value.get_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        bucket_name = 'fake-bucket-name'
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(RetryEventException, 'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            read_zip_central_directory(bucket_name, object_key, input_zip_len)

    # -test_read_zip_central_directory_zip_not_found_error: ClientError->TerminalErrorException
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_zip_not_found_error(self, s3_get_client_mock):
        input_zip_len = 6786
        s3_get_client_mock.return_value.get_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                              'Error': {
                                                                                  'Code': 'NoSuchKey',
                                                                                  'Message': 'Not Found'}},
                                                                             "FAKE")
        bucket_name = 'fake-bucket-name'
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(TerminalErrorException, 'S3 object {0} not found in bucket {1}' \
                .format(object_key, bucket_name)):
            read_zip_central_directory(bucket_name, object_key, input_zip_len)

    # -test_read_zip_central_directory_head_slowdown_error: ClientError->RetryEventException
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_head_slowdown_error(self, s3_get_client_mock):
        input_zip_len = 6786
        s3_get_client_mock.return_value.get_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                              'Error': {
                                                                                  'Code': 'SlowDown',
                                                                                  'Message': 'SlowDown'}},
                                                                             "FAKE")
        bucket_name = 'fake-bucket-name'
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object {0} from bucket {1}' \
                .format(object_key, bucket_name)):
            read_zip_central_directory(bucket_name, object_key, input_zip_len)

    # -test_read_zip_central_directory_read_zip_slowdown_error: ClientError->RetryEventException
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_read_zip_slowdown_error(self, s3_get_client_mock):
        input_zip_len = 20000
        body_mock = Mock()
        body_mock.read.return_value = b'zip file contents for unit test'
        s3_get_client_mock.return_value.get_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                              'Error': {
                                                                                  'Code': 'SlowDown',
                                                                                  'Message': 'SlowDown'}},
                                                                             "FAKE")
        bucket_name = 'fake-bucket-name'
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(RetryEventException,
                                    'Unable to read S3 object {0} from bucket {1}'.format(object_key, bucket_name)):
            read_zip_central_directory(bucket_name, object_key, input_zip_len)

    # # +test_read_zip_central_directory_large_zip_success
    # @patch('service_commons.ingestion.zipfile.ZipFile')
    # @patch('lng_aws_clients.s3.get_client')
    # def test_read_zip_central_directory_large_zip_success(self, s3_get_client_mock, zip_file_mock):
    #     input_zip_len = 20000  # Value larger than initital_read_length (16384)
    #     expected_unread_len = input_zip_len - 16384
    #
    #     s3_get_client_mock.return_value.head_object.return_value = {'ContentLength': input_zip_len}
    #     body_mock = Mock()
    #     body_mock.read.return_value = b'zip file contents for unit test'
    #     s3_get_client_mock.return_value.get_object.return_value = {'Body': body_mock}
    #     zip_file_mock.infolist.return_value = []
    #     (info_list, zip_len, unread_len) = read_zip_central_directory('bucket-name', 'object-key')
    #     self.assertTrue(zip_len == input_zip_len)
    #     self.assertTrue(unread_len == expected_unread_len)
    #
    # # -test_read_zip_central_directory_large_read_zip_slowdown_error: ClientError->RetryEventException
    # @patch('lng_aws_clients.s3.get_client')
    # def test_read_zip_central_directory_large_read_zip_slowdown_error(self, s3_get_client_mock):
    #     input_zip_len = 20000
    #     s3_get_client_mock.return_value.head_object.return_value = {'ContentLength': input_zip_len}
    #     body_mock = Mock()
    #     body_mock.read.return_value = b'zip file contents for unit test'
    #     s3_get_client_mock.return_value.get_object.side_effect = ClientError({'ResponseMetadata': {},
    #                                                                           'Error': {
    #                                                                               'Code': 'SlowDown',
    #                                                                               'Message': 'SlowDown'}},
    #                                                                          "FAKE")
    #     bucket_name = 'fake-bucket-name'
    #     object_key = 'fake-object-key'
    #     with self.assertRaisesRegex(RetryEventException,
    #                                 'Unable to read S3 object {0} from bucket {1}'.format(object_key, bucket_name)):
    #         read_zip_central_directory(bucket_name, object_key)

    # +test_read_zip_central_directory_value_error_success: ValueError
    # The ValueError returned by ZipFile contains how much additional zip file needs to be read
    # to find the central directory.
    @patch('service_commons.ingestion.re')
    @patch('service_commons.ingestion.zipfile.ZipFile')
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_value_error_success(self, s3_get_client_mock, zip_file_mock, re_mock):
        input_zip_len = 286052591
        input_extra_read_len = 5134248  # Returned by ZipFile via ValueError - additional bytes need to be read
        initial_read_length = 5721051
        expected_unread_len = input_zip_len - (initial_read_length + input_extra_read_len)

        s3_get_client_mock.return_value.get_object.return_value = {'ContentLength': input_zip_len}
        body_mock = Mock()
        body_mock.read.return_value = b'zip file contents for unit test PK\x01\x02xxxxxxxxxxxxxxxxx'
        # s3 Object is called twice with different offsets
        s3_get_client_mock.return_value.get_object.side_effect = [{'Body': body_mock}, {'Body': body_mock}]
        zip_file_mock.infolist.return_value = []
        zip_file_mock.side_effect = [ValueError('negative seek value -{}'.format(input_extra_read_len)), zip_file_mock]
        re_match_mock = Mock()
        re_match_mock.group.return_value = (str(input_extra_read_len))  # extra_read_length = Match group(1)
        re_mock.match.return_value = re_match_mock
        (info_list, actual_unread_len, cd_len) = read_zip_central_directory('bucket-name', 'object-key', input_zip_len)
        self.assertEqual(actual_unread_len, expected_unread_len)
        self.assertEqual(cd_len, 10855299)

    # -test_read_zip_central_directory_value_error_fail: TerminalErrorException
    # Thrown if ValueError does not contain the additional bytes to be read
    @patch('service_commons.ingestion.re')
    @patch('service_commons.ingestion.zipfile.ZipFile')
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_value_error_fail(self, s3_get_client_mock, zip_file_mock, re_mock):
        input_zip_len = 286052591
        input_extra_read_len = 5134248  # Returned by ZipFile via ValueError - additional bytes need to be read

        body_mock = Mock()
        body_mock.read.return_value = b'zip file contents for unit test'
        s3_get_client_mock.return_value.get_object.side_effect = [{'Body': body_mock}, {'Body': body_mock}]
        zip_file_mock.side_effect = ValueError('negative seek value -{}'.format(input_extra_read_len))
        re_mock.match.return_value = None

        bucket_name = 'fake-bucket-name'
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(TerminalErrorException, 'Error reading zip file {0}'.format(object_key)):
            read_zip_central_directory(bucket_name, object_key, input_zip_len)

    # -test_read_zip_central_directory_2nd_client_error: ClientError -> RetryEventException
    @patch('service_commons.ingestion.re')
    @patch('service_commons.ingestion.zipfile.ZipFile')
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_2nd_client_error(self, s3_get_client_mock, zip_file_mock, re_mock):
        input_zip_len = 286052591
        input_extra_read_len = 5134248  # Returned by ZipFile via ValueError - additional bytes need to be read

        s3_get_client_mock.return_value.get_object.return_value = {}
        body_mock = Mock()
        body_mock.read.return_value = b'zip file contents for unit test'
        client_error = ClientError({'ResponseMetadata': {},
                                    'Error': {
                                        'Code': 'SlowDown',
                                        'Message': 'SlowDown'}},
                                   "FAKE")
        s3_get_client_mock.return_value.get_object.side_effect = [{'Body': body_mock}, client_error]
        zip_file_mock.side_effect = [ValueError('negative seek value -{}'.format(input_extra_read_len)), zip_file_mock]
        re_match_mock = Mock()
        re_match_mock.group.return_value = (str(input_extra_read_len))  # extra_read_length = Match group(1)
        re_mock.match.return_value = re_match_mock

        bucket_name = 'fake-bucket-name'
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object {0} from bucket {1}' \
                .format(object_key, bucket_name)):
            read_zip_central_directory(bucket_name, object_key, input_zip_len)

    # -test_read_zip_central_directory_bad_zipfile_error_2: zipfile.BadZipFile -> TerminalErrorException
    @patch('service_commons.ingestion.re')
    @patch('service_commons.ingestion._read_streaming_body')
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_central_directory_bad_zipfile_error_2(self, s3_get_client_mock, read_streaming_body_mock,
                                                            re_mock):
        input_zip_len = 286052591
        input_extra_read_len = 5134248  # Returned by ZipFile via ValueError - additional bytes need to be read

        s3_get_client_mock.return_value.head_object.return_value = {'ContentLength': input_zip_len}
        body_mock = Mock()
        body_mock.read.return_value = b'zip file contents for unit test'
        # s3 Object is called twice with different offsets
        s3_get_client_mock.return_value.get_object.side_effect = [{'Body': body_mock}, {'Body': body_mock}]
        bucket_name = 'fake-bucket-name'
        object_key = 'fake-object-key'
        read_streaming_body_mock.side_effect = [ValueError('negative seek value -{}'.format(input_extra_read_len)),
                                                TerminalErrorException("Error reading zip file {0}".format(object_key))]
        re_match_mock = Mock()
        re_match_mock.group.return_value = (str(input_extra_read_len))  # extra_read_length = Match group(1)
        re_mock.match.return_value = re_match_mock

        with self.assertRaisesRegex(TerminalErrorException,
                                    'Error reading zip file {0}'.format(object_key)):
            read_zip_central_directory(bucket_name, object_key, input_zip_len)

    # - _read_streaming_body: zipfile.BadZipFile -> TerminalErrorException
    @patch('service_commons.ingestion.zipfile.ZipFile')
    def test_read_streaming_body_bad_zipfile_error(self, zip_file_mock):
        zip_file_mock.side_effect = zipfile.BadZipFile()
        body_mock = Mock()
        body_mock.read.return_value = b'zip file contents for unit test'
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(TerminalErrorException, 'Error reading zip file {0}'.format(object_key)):
            _read_streaming_body(body_mock, object_key)

    # - _read_streaming_body: exception.ReadTimeoutError -> RetryEventException
    @patch('service_commons.ingestion.StreamingBody.read')
    def test_read_streaming_body_read_timeout_error(self, streaming_body_read_mock):
        streaming_body_read_mock.side_effect = ReadTimeoutError(endpoint_url='mock_s3_url.com')
        body_mock = StreamingBody(raw_stream=None, content_length=0)
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object {0}'.format(object_key)):
            _read_streaming_body(body_mock, object_key)

    # - _read_streaming_body: exception.ConnectionError -> RetryEventException
    @patch('service_commons.ingestion.StreamingBody.read')
    def test_read_streaming_body_connection_error(self, streaming_body_read_mock):
        streaming_body_read_mock.side_effect = ProtocolError(errno.ECONNRESET, "Error attempting to read bytes from")
        body_mock = StreamingBody(raw_stream=None, content_length=0)
        object_key = 'fake-object-key'
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object {0}'.format(object_key)):
            _read_streaming_body(body_mock, object_key)

    ####################################################################################################################
    # + update_error_counter: valid
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_error_counter_success(self, mock_sqs):
        mock_sqs.return_value.send_message.return_value = None
        self.assertIsNone(update_error_counter("fake_ingestion_id"))

    # - update_error_counter: EndpointConnectionError
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_error_counter_endpoint_connection_error_exception(self, sqs_mock):
        sqs_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertIsNone(update_error_counter("fake_ingestion_id"))

    # - update_error_counter: invalid client error
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_error_counter_client_error(self, sqs_mock):
        sqs_mock.side_effect = ClientError({'ResponseMetadata': {},
                                            'Error': {
                                                'Code': 'OTHER',
                                                'Message': 'This is a mock'}},
                                           "FAKE")
        self.assertIsNone(update_error_counter("fake_ingestion_id"))

    # - update_error_counter: invalid general exception
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_error_counter_general_exception(self, sqs_mock):
        sqs_mock.side_effect = Exception
        self.assertIsNone(update_error_counter("fake_ingestion_id"))

    # + is_object_in_datalake - Data already in lake
    @patch("lng_aws_clients.s3.get_client")
    def test_is_object_in_datalake_valid_data_exists(self, mock_s3_client):
        mock_s3_client.return_value.head_object.return_value = {}
        self.assertTrue(is_object_in_datalake('mock_bucket', 'mock_object_key.gz'))

    # + is_object_in_datalake - Data not already in lake
    @patch("lng_aws_clients.s3.get_client")
    def test_is_object_in_datalake_valid_data_new(self, mock_s3_client):
        mock_s3_client.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                           'Error': {
                                                                               'Code': '404',
                                                                               'Message': 'Not Found'}},
                                                                          "FAKE")
        self.assertFalse(is_object_in_datalake('mock_bucket', 'mock_object_key.gz'))

    # - is_object_in_datalake - EndpointConnectionError
    @patch("lng_aws_clients.s3.get_client")
    def test_is_object_in_datalake_endpoint_connection_error_exception(self, mock_s3_client):
        mock_s3_client.return_value.head_object.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, is_object_in_datalake, 'mock_bucket', 'mock_object_key.gz')

    # - is_object_in_datalake - Client Error
    @patch("lng_aws_clients.s3.get_client")
    def test_is_object_in_datalake_fail_1(self, mock_s3_client):
        mock_s3_client.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                           'Error': {
                                                                               'Code': '400',
                                                                               'Message': 'mock error message'}},
                                                                          "FAKE")
        self.assertRaises(ClientError, is_object_in_datalake, 'mock_bucket', 'mock_object_key.gz')

    # Tests for delete collection blocker--------------------------------------------
    # + delete_collection_blocker - Valid Event
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_valid(self, mock_coll_blocker_delete):
        mock_coll_blocker_delete.return_value = None

        self.assertIsNone(delete_collection_blocker('marktest', '2019-03-17T00:32:41.326Z',
                                                    '2c86d989-484c-11e9-9d9c-cf31f437a894'))
        mock_coll_blocker_delete.assert_called_with({"collection-id": 'marktest',
                                                     "request-time": '2019-03-17T00:32:41.326Z'})

    # - delete_collection_blocker - Delete EndpointConnectionError
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_delete_endpoint_connection_error_exception(self, mock_coll_blocker_delete):
        mock_coll_blocker_delete.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')

        with self.assertRaisesRegex(EndpointConnectionError, 'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            delete_collection_blocker('marktest', '2019-03-17T00:32:41.326Z', '2c86d989-484c-11e9-9d9c-cf31f437a894')

    # - delete_collection_blocker - Delete Client Error
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_delete_client_error(self, mock_coll_blocker_delete):
        mock_coll_blocker_delete.side_effect = ClientError({}, 'client error message')

        with self.assertRaisesRegex(ClientError, 'client error message'):
            delete_collection_blocker('marktest', '2019-03-17T00:32:41.326Z', '2c86d989-484c-11e9-9d9c-cf31f437a894')

    # - delete_collection_blocker - Exception Client Error
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_delete_exception_error(self, mock_coll_blocker_delete):
        mock_coll_blocker_delete.side_effect = Exception

        with self.assertRaisesRegex(TerminalErrorException, 'Failed to delete item from CollectionBlocker Table'):
            delete_collection_blocker('marktest', '2019-03-17T00:32:41.326Z', '2c86d989-484c-11e9-9d9c-cf31f437a894')

    # + create_object_backend_event_id - Valid
    def test_create_object_backend_event_id(self):
        self.assertEqual(create_object_backend_event_id('abc', 10), "abc-0000010")

if __name__ == '__main__':
    unittest.main()
