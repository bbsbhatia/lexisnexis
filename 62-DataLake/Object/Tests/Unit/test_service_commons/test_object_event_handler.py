import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch, call

import lng_datalake_commons.publish_sns_topic as publish_sns_topic_module
from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons import publish_sns_topic
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_testhelper.io_utils import IOUtils

import service_commons.object_common as object_common_module
from service_commons.object_event_handler import copy_object_to_datalake, is_object_in_datalake, \
    process_staging_object, get_consistent_object_item, update_ingestion, validate_message, \
    publish_notifications, generate_object_store_version_item, insert_object_store_table, update_object_store_table, \
    insert_object_store_version_table, delete_object_store_version_table, get_notification_schema_props, \
    get_notification_schema_context_props, send_object_notifications, generate_object_notification, \
    build_notification_context, changeset_update, send_changeset_notifications, build_changeset_object_notification, \
    get_notification_schema_changeset_object_props

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'ObjectEventHandler')


class ServiceCommonsObjectEventHandlerTest(TestCase):
    session = None

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com',
                             'LAMBDA_TASK_ROOT': os.path.dirname(io_utils.schema_path)})
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(publish_sns_topic_module)
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + copy_object_to_datalake - Valid Input
    @patch('lng_aws_clients.s3.get_client')
    def test_copy_object_to_datalake_valid(self, mock_s3_client):
        message = io_utils.load_data_json('valid_message.json')
        mock_s3_client.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                           'Error': {
                                                                               'Code': '404',
                                                                               'Message': 'Not Found'}},
                                                                          "FAKE")
        mock_s3_client.return_value.copy.return_value = None
        self.assertIsNone(
            copy_object_to_datalake(message['bucket-name'], ['mock_bucket'], message['object-key'], message, 1))

    # + copy_object_to_datalake - object already exists
    @patch('lng_aws_clients.s3.get_client')
    def test_copy_object_to_datalake_object_exists(self, mock_s3_client):
        message = io_utils.load_data_json('valid_message.json')
        mock_s3_client.return_value.head_object.return_value = {}
        mock_s3_client.return_value.copy.return_value = None
        self.assertIsNone(
            copy_object_to_datalake(message['bucket-name'], ['mock_bucket'], message['object-key'], message, 1))

    # - copy_object_to_datalake - EndpointConnectionError
    @patch('lng_aws_clients.s3.get_client')
    def test_copy_object_to_datalake_endpoint_connection_error_exception(self, mock_s3_client):
        message = io_utils.load_data_json('valid_message.json')
        mock_s3_client.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, copy_object_to_datalake, message['bucket-name'], 'mock',
                          message['object-key'],
                          message, 1)

    # - copy_object_to_datalake - Client Error
    @patch('lng_aws_clients.s3.get_client')
    def test_copy_object_to_datalake_fail_1(self, mock_s3_client):
        message = io_utils.load_data_json('valid_message.json')
        mock_s3_client.side_effect = ClientError({'ResponseMetadata': {},
                                                  'Error': {
                                                      'Code': 'OTHER',
                                                      'Message': 'This is a mock'}},
                                                 "FAKE")
        self.assertRaises(ClientError, copy_object_to_datalake, message['bucket-name'], 'mock', message['object-key'],
                          message, 1)

    # - copy_object_to_datalake - Exception
    @patch("lng_aws_clients.s3.get_client")
    def test_copy_object_to_datalake_fail_2(self, mock_s3_client):
        message = io_utils.load_data_json('valid_message.json')
        mock_s3_client.side_effect = Exception
        self.assertRaises(TerminalErrorException, copy_object_to_datalake, message['bucket-name'], 'mock',
                          message['object-key'], message, 1)

    # + is_object_in_datalake - Data already in lake
    @patch("lng_aws_clients.s3.get_client")
    def test_is_object_in_datalake_valid_data_exists(self, mock_s3_client):
        mock_s3_client.return_value.head_object.return_value = {}
        self.assertTrue(is_object_in_datalake('mock_bucket', 'mock_object_key.gz'))

    # + is_object_in_datalake - Data not already in lake
    @patch("lng_aws_clients.s3.get_client")
    def test_is_object_in_datalake_valid_data_new(self, mock_s3_client):
        mock_s3_client.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                           'Error': {
                                                                               'Code': '404',
                                                                               'Message': 'Not Found'}},
                                                                          "FAKE")
        self.assertFalse(is_object_in_datalake('mock_bucket', 'mock_object_key.gz'))

    # - is_object_in_datalake - EndpointConnectionError
    @patch("lng_aws_clients.s3.get_client")
    def test_is_object_in_datalake_fail_endpoint_connection_error_exception(self, mock_s3_client):
        mock_s3_client.return_value.head_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        self.assertRaises(EndpointConnectionError, is_object_in_datalake, 'mock_bucket', 'mock_object_key.gz')

    # - is_object_in_datalake - Client Error
    @patch("lng_aws_clients.s3.get_client")
    def test_is_object_in_datalake_fail_1(self, mock_s3_client):
        mock_s3_client.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                           'Error': {
                                                                               'Code': '400',
                                                                               'Message': 'mock error message'}},
                                                                          "FAKE")
        self.assertRaises(ClientError, is_object_in_datalake, 'mock_bucket', 'mock_object_key.gz')


    # +get_consistent_object_item - success
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_consistent_object_item_success(self, ost_get_mock):
        ost_item = io_utils.load_data_json('objectStoreTable.get_item.valid.json')
        ost_get_mock.return_value = ost_item
        object_id = '49cf62814f629ba0a679147d8784b516cde30a17'
        collection_id = '274'
        self.assertDictEqual(get_consistent_object_item(object_id, collection_id), ost_item)

    # -get_consistent_object_item - EndpointConnectionError exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_consistent_object_item_endpoint_connection_error_exception(self, ost_get_mock):
        ost_get_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            get_consistent_object_item("mock-object-id", "mock-collection-id")

    # -get_consistent_object_item - ClientError exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_consistent_object_item_fail_client_error(self, ost_get_mock):
        ost_get_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                "Error": {"Code": "mock error code",
                                                          "Message": "mock error message"}},
                                               "client-error-mock")
        with self.assertRaises(ClientError):
            get_consistent_object_item("mock-object-id", "mock-collection-id")

    # -get_consistent_object_item - General Exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_consistent_object_item_fail_general_exception(self, ost_get_mock):
        ost_get_mock.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to get item from ObjectStoreTable.'):
            get_consistent_object_item("mock-object-id", "mock-collection-id")

    # + update ingestion: valid
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_ingestion_success(self, mock_sqs):
        message = io_utils.load_data_json("valid_message_ingestion.json")
        mock_sqs.return_value.send_message.return_value = None
        self.assertIsNone(update_ingestion(message))

    # - update ingestion: invalid EndpointConnectionError
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_ingestion_endpoint_connection_error_exception(self, sqs_mock):
        message = io_utils.load_data_json("valid_message_ingestion.json")
        sqs_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertIsNone(update_ingestion(message))

    # - update ingestion: invalid client error
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_ingestion_client_error(self, sqs_mock):
        message = io_utils.load_data_json("valid_message_ingestion.json")
        sqs_mock.side_effect = ClientError({'ResponseMetadata': {},
                                            'Error': {
                                                'Code': 'OTHER',
                                                'Message': 'This is a mock'}},
                                           "FAKE")
        self.assertIsNone(update_ingestion(message))

    # - update ingestion: invalid general exception
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_ingestion_general_exception(self, sqs_mock):
        message = io_utils.load_data_json("valid_message_ingestion.json")
        sqs_mock.side_effect = Exception
        self.assertIsNone(update_ingestion(message))

    # + get_notification_schema_props - success - schema version = v1
    def test_get_schema_props_success_v1(self):
        schema = io_utils.load_schema_json("Publish/v1/create-object.json")
        expected_required_props = ["object-id", "collection-id", "object-state", "content-type", "raw-content-length",
                                   "version-number", "version-timestamp", "event-name", "S3",
                                   "object-key-url"]
        expected_optional_props = ["object-expiration-date", "replicated-buckets", "object-metadata"]
        req_props, opt_props = get_notification_schema_props(schema, schema_version='v1')
        self.assertEqual(set(expected_required_props), set(req_props))
        self.assertEqual(set(expected_optional_props), set(opt_props))

    # + get_notification_schema_props - success - schema version = v0
    def test_get_schema_props_success_v0(self):
        schema = io_utils.load_schema_json("Publish/v0/create-object.json")
        expected_required_props = ["object-id", "collection-id", "item-state", "bucket-name", "object-key",
                                   "content-type", "raw-content-length", "version-number", "version-timestamp",
                                   "event-name", "S3", "object-key-url"]
        expected_optional_props = ["pending-expiration-epoch", "replicated-buckets"]
        req_props, opt_props = get_notification_schema_props(schema, schema_version='v0')
        self.assertEqual(set(expected_required_props), set(req_props))
        self.assertEqual(set(expected_optional_props), set(opt_props))

    # + get_notification_schema_context_props - success
    def test_get_schema_context_props_success(self):
        schema = io_utils.load_schema_json("Publish/v1/create-object.json")
        expected_required_props = ["request-time", "request-id"]
        expected_optional_props = []
        req_props, opt_props = get_notification_schema_context_props(schema)
        self.assertEqual(set(expected_required_props), set(req_props))
        self.assertEqual(set(expected_optional_props), set(opt_props))

    # + send_object_notifications - success
    @patch("service_commons.object_event_handler.publish_notifications")
    def test_send_object_notifications_success(self, mock_publish_message):
        mock_publish_message.return_value = None
        object_store_item = io_utils.load_data_json('object_store_item_with_expiration.json')
        sns_publish_data = {}
        schemas = ["Schemas/Publish/v0/create-object.json", "Schemas/Publish/v1/create-object.json"]
        catalog_ids = ['catalog-test1', 'catalog-test2']
        event = io_utils.load_data_json('valid_message.json')
        target_arn = "subscription arn"
        changeset_id = 'mock-changeset'
        self.assertIsNone(
            send_object_notifications(sns_publish_data, object_store_item, schemas, event, target_arn,
                                      catalog_ids, changeset_id))
        notifications = io_utils.load_data_json('valid_notification_messages.json')
        notification_attribute = {'event-name': 'Object::Create',
                                  'collection-id': ['274'],
                                  'catalog-id': ['catalog-test1', 'catalog-test2']}
        mock_publish_message.assert_called_with(notifications, notification_attribute, target_arn)

    # + send_object_notifications - success no-op event publishes to v1 only
    @patch("service_commons.object_event_handler.publish_notifications")
    def test_send_object_notifications_success_v1_only(self, mock_publish_message):
        mock_publish_message.return_value = None
        object_store_item = io_utils.load_data_json('object_store_item_with_expiration.json')
        sns_publish_data = {}
        schemas = ['Schemas/Publish/v0/update-object.json', 'Schemas/Publish/v1/update-object.json']
        catalog_ids = ['catalog-test1', 'catalog-test2']
        event = io_utils.load_data_json('valid_message_no_change.json')
        target_arn = "subscription arn"
        changeset_id = 'mock-changeset'
        self.assertIsNone(
            send_object_notifications(sns_publish_data, object_store_item, schemas, event, target_arn,
                                      catalog_ids, changeset_id))
        notifications = io_utils.load_data_json('valid_notification_messages_v1_only.json')
        notification_attribute = {'event-name': 'Object::UpdateNoChange',
                                  'collection-id': ['274'],
                                  'catalog-id': ['catalog-test1', 'catalog-test2']}
        mock_publish_message.assert_called_with(notifications, notification_attribute, target_arn)

    # + send_object_notifications - success ingestion event publishes to v1 only with correct request-id
    @patch("service_commons.object_event_handler.publish_notifications")
    def test_send_object_notifications_success_ingestion_v1_only(self, mock_publish_message):
        mock_publish_message.return_value = None
        object_store_item = io_utils.load_data_json('object_store_item_with_expiration.json')
        sns_publish_data = {}
        schemas = ['Schemas/Publish/v1/update-object.json']
        catalog_ids = ['catalog-test1', 'catalog-test2']
        event = io_utils.load_data_json('valid_message_ingestion.json')
        target_arn = "subscription arn"
        changeset_id = 'mock-changeset'
        self.assertIsNone(
            send_object_notifications(sns_publish_data, object_store_item, schemas, event, target_arn,
                                      catalog_ids, changeset_id))
        notifications = io_utils.load_data_json('valid_notification_messages_ingestion_v1_only.json')
        notification_attribute = {'event-name': 'Object::Create',
                                  'collection-id': ['274'],
                                  'catalog-id': ['catalog-test1', 'catalog-test2']}
        mock_publish_message.assert_called_with(notifications, notification_attribute, target_arn)

    # + send_object_notifications - success - sns publish data passed (when the event is retry)
    @patch("service_commons.object_event_handler.publish_notifications")
    def test_send_object_notifications_sns_publish_data(self, mock_publish_message):
        mock_publish_message.return_value = None
        object_store_item = io_utils.load_data_json('valid_message.json')
        notifications = io_utils.load_data_json('valid_notification_messages.json')
        notification_attribute = {'event-name': 'Object::Create',
                                  'collection-id': ['274'],
                                  'catalog-id': ['catalog-test1', 'catalog-test2']}
        sns_publish_data = {"notifications": notifications}
        schemas = ["Schemas/Publish/v0/create-object.json", "Schemas/Publish/v1/create-object.json"]
        catalog_ids = ['catalog-test1', 'catalog-test2']
        event = io_utils.load_data_json('valid_message.json')
        target_arn = "subscription arn"
        self.assertIsNone(send_object_notifications(sns_publish_data, object_store_item,
                                                    schemas, event, target_arn, catalog_ids, ''))

        mock_publish_message.assert_called_with(notifications, notification_attribute, target_arn)

    # - publish_notifications: EndpointConnectionError
    @patch('lng_datalake_commons.error_handling.error_handler.set_message_additional_attribute')
    @patch('lng_datalake_commons.publish_sns_topic.publish_to_topic')
    def test_publish_notifications_endpoint_connection_error_exception(self, mock_sns_publish, mock_error_handler):
        mock_sns_publish.side_effect = [None,
                                        EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')]
        mock_error_handler.return_value = None
        target_arn = "subscription arn"
        notifications = io_utils.load_data_json('valid_notification_messages.json')
        notification_attributes = {'event-name': 'Object::Create',
                                   'collection-id': '274',
                                   'catalog-id': ['catalog-test1', 'catalog-test2']}
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL'):
            publish_notifications(notifications, notification_attributes, target_arn)
        calls = [call(notifications[0], notification_attributes, target_arn),
                 call(notifications[1], notification_attributes, target_arn)]
        mock_sns_publish.assert_has_calls(calls)
        mock_error_handler.assert_called_with('sns-publish-data', {"notifications": notifications[1:]})

    # - publish_notifications: client error
    @patch('lng_datalake_commons.error_handling.error_handler.set_message_additional_attribute')
    @patch('lng_datalake_commons.publish_sns_topic.publish_to_topic')
    def test_publish_notifications(self, mock_sns_publish, mock_error_handler):
        mock_sns_publish.side_effect = [None, ClientError({'ResponseMetadata': {},
                                                           'Error': {
                                                               'Code': 'OTHER',
                                                               'Message': 'This is a mock'}},
                                                          "FAKE")]
        mock_error_handler.return_value = None
        target_arn = "subscription arn"
        notifications = io_utils.load_data_json('valid_notification_messages.json')
        notification_attributes = {'event-name': 'Object::Create',
                                   'collection-id': '274',
                                   'catalog-id': ['catalog-test1', 'catalog-test2']}
        with self.assertRaisesRegex(ClientError, "This is a mock"):
            publish_notifications(notifications, notification_attributes, target_arn)
        calls = [call(notifications[0], notification_attributes, target_arn),
                 call(notifications[1], notification_attributes, target_arn)]
        mock_sns_publish.assert_has_calls(calls)
        mock_error_handler.assert_called_with('sns-publish-data', {"notifications": notifications[1:]})

    # - generate_object_notification - Missing required properties to generate the notification message
    def test_generate_object_notification_missing_obj_props(self):
        object_store_item = io_utils.load_data_json('invalid_object_store_item.json')
        schema_file = "Schemas/Publish/v1/create-object.json"
        event = io_utils.load_data_json('valid_message.json')
        schema_version = "v1"
        with self.assertRaisesRegex(TerminalErrorException, "Failed to generate object notification. "
                                                            "Missing required property: 'object-key'"):
            generate_object_notification(schema_file, schema_version, object_store_item, event, '')

    # + build_notification_context - with optional schema key
    @patch('service_commons.object_event_handler.get_notification_schema_context_props')
    def test_build_notification_context_with_optional_schema_key(self, mock_get_notification_schema_context_props):
        # We mock an optional schema prop, because context currently does not have any
        mock_get_notification_schema_context_props.return_value = (
            ["request-id", "request-time"], ["pretend-optional-key"])

        schema = {"does not matter": "we mock the response"}
        event = io_utils.load_data_json("valid_message_with_pretend_optional_key.json")

        expected_notification_context = {
            "request-id": "2e6ee0b2-d5e9-4745-8af9-a3179e1e5ae6",
            "request-time": "2018-05-09T11:23:34.495Z",
            "pretend-optional-key": "when we give context a real optional key, change this test case to use it"
        }
        self.assertEqual(expected_notification_context,
                         build_notification_context(schema, event))

    # - build_notification_context - Missing required properties to generate the notification context
    def test_build_notification_context_missing_context_prop(self):
        schema_file = "Schemas/Publish/v1/create-object.json"
        schema = publish_sns_topic.load_json_schema(schema_file)
        event = io_utils.load_data_json('invalid_message_missing_key.json')
        with self.assertRaisesRegex(TerminalErrorException, "Failed to generate object notification context. "
                                                            "Missing required property: 'request-time'"):
            build_notification_context(schema, event)

    # + generate_object_store_version_item - Valid Request
    def test_generate_object_store_version_item_pass(self):
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        expected = io_utils.load_data_json('object_store_version_item_expected.json')
        self.maxDiff = None
        self.assertDictEqual(generate_object_store_version_item(new_item, 'test_event'), expected)

    # - generate_object_store_version_item - Missing required attribute
    def test_generate_object_store_version_item_fail_2(self):
        self.assertRaises(TerminalErrorException, generate_object_store_version_item, {}, 'test_event')

    # - validate_message: invalid message
    def test_validate_message_fail(self):
        message = io_utils.load_data_json("valid_message.json")
        message.pop("event-name")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to extract event'):
            validate_message(message, "Object::Create", 1,
                             'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # - validate_message: invalid event version
    def test_validate_message_fail2(self):
        message = io_utils.load_data_json("valid_message.json")
        with self.assertRaisesRegex(TerminalErrorException, 'event-version does not match'):
            validate_message(message, "Object::Create", 2,
                             'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # - validate_message: invalid stage
    def test_validate_message_fail3(self):
        message = io_utils.load_data_json("valid_message.json")
        with self.assertRaisesRegex(TerminalErrorException, 'stage does not match'):
            validate_message(message, "Object::Create", 1,
                             'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:v1')

    # + process_staging_object: valid
    @patch('service_commons.object_event_handler.copy_object_to_datalake')
    def test_process_staging_object(self, copy_object_to_datalake_mock):
        copy_object_to_datalake_mock.return_value = None
        message = io_utils.load_data_json("valid_message.json")
        self.assertIsNone(process_staging_object(message,
                                                 {'us-east-1': 'bucket-use1', 'us-west-2': 'bucket-usw2'}, 1))
        copy_object_to_datalake_mock.assert_called_once()

    # + process_staging_object: valid - part of ingestion
    @patch('service_commons.object_event_handler.copy_object_to_datalake')
    def test_process_staging_object_ingestion(self, copy_object_to_datalake_mock):
        copy_object_to_datalake_mock.return_value = None
        message = io_utils.load_data_json("valid_message.json")
        message["ingestion-id"] = "abcdefgh"
        self.assertIsNone(process_staging_object(message,
                                                 {'us-east-1': 'bucket-use1', 'us-west-2': 'bucket-usw2'}, 1))
        copy_object_to_datalake_mock.assert_not_called()

    # + process_staging_object: valid - large object
    @patch('service_commons.object_event_handler.copy_object_to_datalake')
    def test_process_staging_object_large(self, copy_object_to_datalake_mock):
        copy_object_to_datalake_mock.return_value = None
        message = io_utils.load_data_json("valid_message.json")
        message["is-large-object-request"] = True
        message["is-new-object"] = True
        self.assertIsNone(process_staging_object(message,
                                                 {'us-east-1': 'bucket-use1', 'us-west-2': 'bucket-usw2'}, 1))
        copy_object_to_datalake_mock.assert_not_called()

    # + insert_object_store_table: valid
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    def test_insert_object_store_table(self, ost_put_item):
        ost_put_item.return_value = None
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        self.assertFalse(insert_object_store_table(new_item))

    # + insert_object_store_table: no_object_changes
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    def test_insert_object_store_table_no_object_changes(self, ost_put_item):
        ost_put_item.side_effect = ConditionError()
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        with self.assertRaisesRegex(TerminalErrorException, "ConditionError inserting to OST"):
            insert_object_store_table(new_item)

    # - insert_object_store_table: EndpointConnectionError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    def test_insert_object_store_table_endpoint_connection_error_exception(self, ost_put_item):
        ost_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        self.assertRaises(EndpointConnectionError, insert_object_store_table, new_item)

    # - insert_object_store_table: ClientError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    def test_insert_object_store_table_client_error(self, ost_put_item):
        ost_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                'Error': {
                                                    'Code': 'OTHER',
                                                    'Message': 'This is a mock'}},
                                               "FAKE")
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        self.assertRaises(ClientError, insert_object_store_table, new_item)

    # - insert_object_store_table: Exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    def test_insert_object_store_table_exception(self, ost_put_item):
        ost_put_item.side_effect = Exception()
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        self.assertRaises(Exception, insert_object_store_table, new_item)

    # - insert_object_store_table: SchemaError
    def test_insert_object_store_table_schema_error(self):
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        new_item.pop('object-id')
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to insert item into Object Store table'):
            insert_object_store_table(new_item)

    # + update_object_store_table: valid
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_update_object_store_table(self, ost_put_item):
        ost_put_item.return_value = None
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        self.assertFalse(update_object_store_table(new_item))

    # + update_object_store_table: no_object_changes
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_update_object_store_table_no_object_changes(self, ost_put_item):
        ost_put_item.side_effect = ConditionError()
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        with self.assertRaisesRegex(TerminalErrorException, "ConditionError inserting to OST"):
            update_object_store_table(new_item)

    # - update_object_store_table: EndpointConnectionError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_update_object_store_table_endpoint_connection_error_exception(self, ost_put_item):
        ost_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        self.assertRaises(EndpointConnectionError, update_object_store_table, new_item)

    # - update_object_store_table: ClientError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_update_object_store_table_client_error(self, ost_put_item):
        ost_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                'Error': {
                                                    'Code': 'OTHER',
                                                    'Message': 'This is a mock'}},
                                               "FAKE")
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        self.assertRaises(ClientError, update_object_store_table, new_item)

    # - update_object_store_table: Exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_update_object_store_table_exception(self, ost_put_item):
        ost_put_item.side_effect = Exception()
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        self.assertRaises(Exception, update_object_store_table, new_item)

    # - update_object_store_table: SchemaError
    def test_update_object_store_table_schema_error(self):
        new_item = io_utils.load_data_json('object_store_item_expected.json')
        new_item.pop('object-id')
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to update item in Object Store table'):
            update_object_store_table(new_item)

    # + insert_object_store_version_table: valid
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    def test_insert_object_store_version_table(self, osvt_put_item):
        osvt_put_item.return_value = None
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        self.assertIsNone(insert_object_store_version_table(new_item))

    # - insert_object_store_version_table: EndpointConnectionError
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    def test_insert_object_store_version_table_endpoint_connection_error_exception(self, osvt_put_item):
        osvt_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        with self.assertRaises(EndpointConnectionError):
            insert_object_store_version_table(new_item)

    # - insert_object_store_version_table: ClientError
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    def test_insert_object_store_version_table_client_error(self, osvt_put_item):
        osvt_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                 'Error': {
                                                     'Code': 'OTHER',
                                                     'Message': 'This is a mock'}},
                                                "FAKE")
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        with self.assertRaises(ClientError):
            insert_object_store_version_table(new_item)

    # - insert_object_store_version_table: Exception
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    def test_insert_object_store_version_table_exception(self, osvt_put_item):
        osvt_put_item.side_effect = Exception()
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        with self.assertRaises(Exception):
            insert_object_store_version_table(new_item)

    # - insert_object_store_version_table: SchemaError
    def test_insert_object_store_version_table_schema_error(self):
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        new_item.pop('object-id')
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to insert item into Object Store Version table'):
            insert_object_store_version_table(new_item)

    # + delete_object_store_version_table: valid
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.delete_item')
    def test_delete_object_store_version_table(self, osvt_delete_item):
        osvt_delete_item.return_value = None
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        self.assertIsNone(delete_object_store_version_table(new_item, 1))

    # - delete_object_store_version_table: EndpointConnectionError
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.delete_item')
    def test_delete_object_store_version_table_endpoint_connection_error_exception(self, osvt_delete_item):
        osvt_delete_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        self.assertRaises(EndpointConnectionError, delete_object_store_version_table, new_item, 1)

    # - delete_object_store_version_table: ClientError
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.delete_item')
    def test_delete_object_store_version_table_client_error(self, osvt_delete_item):
        osvt_delete_item.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        self.assertRaises(ClientError, delete_object_store_version_table, new_item, 1)

    # - delete_object_store_version_table: Exception
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.delete_item')
    def test_delete_object_store_version_table_exception(self, osvt_delete_item):
        osvt_delete_item.side_effect = Exception()
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        self.assertRaises(Exception, delete_object_store_version_table, new_item, 1)

    # - delete_object_store_version_table: SchemaError
    def test_delete_object_store_version_table_schema_error(self):
        new_item = io_utils.load_data_json('object_store_version_item_expected.json')
        new_item.pop('object-id')
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to delete item from Object Store Version table'):
            delete_object_store_version_table(new_item, 1)

    # + changeset_update: success
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.put_item')
    def test_changeset_update(self, mock_changeset_object_put_item):
        mock_changeset_object_put_item.return_value = None
        object_store_record = {
            "object-id": "test-ob",
            "collection-id": "274",
            "object-state": "Created",
            "bucket-name": "mock_datalake_bucket-use1",
            "object-key": "keykeykey",
            "content-type": "text/plain",
            "raw-content-length": 10,
            "event-ids": ["uDNLqpfTNEvdHGLg"],
            "version-number": 1,
            "version-timestamp": "2018-05-09T11:23:34.495Z",
            "collection-hash": "431bf3b995a99c2cd6899b97187d1542a965cec9",
            "replicated-buckets": [
                "mock_datalake_bucket-usw2"
            ]
        }

        changeset_object_item = {
            'composite-key': 'test-ob|274|1',
            "object-id": "test-ob",
            "collection-id": "274",
            "object-state": "Created",
            "bucket-name": "mock_datalake_bucket-use1",
            "object-key": "jek-changeset/keykeykey",
            "content-type": "text/plain",
            "raw-content-length": 10,
            "event-id": "some-event",
            "version-number": 1,
            "version-timestamp": "2018-05-09T11:23:34.495Z",
            "collection-hash": "431bf3b995a99c2cd6899b97187d1542a965cec9",
            "replicated-buckets": [
                "mock_datalake_bucket-usw2"
            ],
            "pending-expiration-epoch": 1234567890,
            'changeset-id': 'jek-changeset'
        }

        self.assertIsNone(changeset_update(object_store_record,
                                           {'changeset-id': 'jek-changeset', 'changeset-expiration-epoch': 1234567890,
                                            "event-id": "some-event"}))
        mock_changeset_object_put_item.assert_called_with(dict_items=changeset_object_item)

    # - changeset_update: client error
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.put_item')
    def test_changeset_update_client_error(self, mock_changeset_object_put_item):
        mock_changeset_object_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                                  'Error': {
                                                                      'Code': 'OTHER',
                                                                      'Message': 'This is a mock'}},
                                                                 "Client Error")
        object_store_record = {
            "object-id": "test-ob",
            "collection-id": "274",
            "object-state": "Created",
            "bucket-name": "mock_datalake_bucket-use1",
            "object-key": "keykeykey",
            "content-type": "text/plain",
            "raw-content-length": 10,
            "event-ids": ["uDNLqpfTNEvdHGLg"],
            "version-number": 1,
            "version-timestamp": "2018-05-09T11:23:34.495Z",
            "collection-hash": "431bf3b995a99c2cd6899b97187d1542a965cec9",
            "replicated-buckets": [
                "mock_datalake_bucket-usw2"
            ]
        }

        with self.assertRaisesRegex(ClientError, 'Client Error'):
            changeset_update(object_store_record,
                             {'changeset-id': 'jek-changeset', 'changeset-expiration-epoch': 1234567890,
                              "event-id": "some-event"})

    # - changeset_update: general_exception
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.put_item')
    def test_changeset_update_general_exception(self, mock_changeset_object_put_item):
        mock_changeset_object_put_item.side_effect = Exception('General Exception')
        object_store_record = {
            "object-id": "test-ob",
            "collection-id": "274",
            "object-state": "Created",
            "bucket-name": "mock_datalake_bucket-use1",
            "object-key": "keykeykey",
            "content-type": "text/plain",
            "raw-content-length": 10,
            "event-ids": ["uDNLqpfTNEvdHGLg"],
            "version-number": 1,
            "version-timestamp": "2018-05-09T11:23:34.495Z",
            "collection-hash": "431bf3b995a99c2cd6899b97187d1542a965cec9",
            "replicated-buckets": [
                "mock_datalake_bucket-usw2"
            ]
        }

        with self.assertRaisesRegex(TerminalErrorException, 'General Exception'):
            changeset_update(object_store_record,
                             {'changeset-id': 'jek-changeset', 'changeset-expiration-epoch': 1234567890,
                              "event-id": "some-event"})

    # + test_send_changeset_notifications: success
    @patch("service_commons.object_event_handler.publish_notifications")
    def test_send_changeset_notifications(self, mock_publish_message):
        mock_publish_message.return_value = None
        changeset_item = io_utils.load_data_json('dynamodb.changeset_data_closed.json')
        sns_publish_data = {}
        schemas = ["Schemas/Publish/v1/close-changeset.json"]
        catalog_ids = ['catalog-test1', 'catalog-test2']
        collection_ids = ['coll-1', 'coll-2']
        event = io_utils.load_data_json('valid_changeset_close_event.json')
        target_arn = "subscription arn"
        self.assertIsNone(
            send_changeset_notifications(sns_publish_data, changeset_item, schemas, event, target_arn, collection_ids,
                                         catalog_ids))
        notification_attribute = {'event-name': 'Changeset::Close',
                                  'collection-id': collection_ids,
                                  'catalog-id': catalog_ids,
                                  'changeset-id': 'foo'}
        notifications = io_utils.load_data_json('valid_changeset_notification_messages_v1_only.json')
        mock_publish_message.assert_called_with(notifications, notification_attribute, target_arn)

    # + send_object_notifications - success no-op event publishes to v1 only no changeset
    @patch("service_commons.object_event_handler.publish_notifications")
    def test_send_object_notifications_success_v1_only_no_changeset(self, mock_publish_message):
        mock_publish_message.return_value = None
        object_store_item = io_utils.load_data_json('object_store_item_with_expiration.json')
        sns_publish_data = {}
        schemas = ['Schemas/Publish/v0/update-object.json', 'Schemas/Publish/v1/update-object.json']
        catalog_ids = ['catalog-test1', 'catalog-test2']
        event = io_utils.load_data_json('valid_message_no_change.json')
        target_arn = "subscription arn"
        changeset_id = ''
        self.assertIsNone(
            send_object_notifications(sns_publish_data, object_store_item, schemas, event, target_arn,
                                      catalog_ids, changeset_id))
        notifications = io_utils.load_data_json('valid_notification_messages_v1_only_no_changeset.json')
        notification_attribute = {'event-name': 'Object::UpdateNoChange',
                                  'collection-id': ['274'],
                                  'catalog-id': ['catalog-test1', 'catalog-test2']}
        mock_publish_message.assert_called_with(notifications, notification_attribute, target_arn)

    # + send_object_notifications - success ingestion event publishes to v1 only with correct request-id no changeset
    @patch("service_commons.object_event_handler.publish_notifications")
    def test_send_object_notifications_success_ingestion_v1_only_no_changeset(self, mock_publish_message):
        mock_publish_message.return_value = None
        object_store_item = io_utils.load_data_json('object_store_item_with_expiration.json')
        sns_publish_data = {}
        schemas = ['Schemas/Publish/v1/update-object.json']
        catalog_ids = ['catalog-test1', 'catalog-test2']
        event = io_utils.load_data_json('valid_message_ingestion.json')
        target_arn = "subscription arn"
        changeset_id = ''
        self.assertIsNone(
            send_object_notifications(sns_publish_data, object_store_item, schemas, event, target_arn,
                                      catalog_ids, changeset_id))
        notifications = io_utils.load_data_json('valid_notification_messages_ingestion_v1_only_no_changeset.json')
        notification_attribute = {'event-name': 'Object::Create',
                                  'collection-id': ['274'],
                                  'catalog-id': ['catalog-test1', 'catalog-test2']}
        mock_publish_message.assert_called_with(notifications, notification_attribute, target_arn)

    # + send_object_notifications - success
    @patch("service_commons.object_event_handler.publish_notifications")
    def test_send_object_notifications_success_no_changeset(self, mock_publish_message):
        mock_publish_message.return_value = None
        object_store_item = io_utils.load_data_json('object_store_item_with_expiration.json')
        sns_publish_data = {}
        schemas = ["Schemas/Publish/v0/create-object.json", "Schemas/Publish/v1/create-object.json"]
        catalog_ids = ['catalog-test1', 'catalog-test2']
        event = io_utils.load_data_json('valid_message.json')
        target_arn = "subscription arn"
        changeset_id = ''
        self.assertIsNone(
            send_object_notifications(sns_publish_data, object_store_item, schemas, event, target_arn,
                                      catalog_ids, changeset_id))
        notifications = io_utils.load_data_json('valid_notification_messages_no_changeset.json')
        notification_attribute = {'event-name': 'Object::Create',
                                  'collection-id': ['274'],
                                  'catalog-id': ['catalog-test1', 'catalog-test2']}
        mock_publish_message.assert_called_with(notifications, notification_attribute, target_arn)

    # + get_notification_schema_changeset_object_props - success
    def test_get_schema_changeset_object_props_success(self):
        schema = io_utils.load_schema_json("Publish/v1/create-object.json")
        expected_required_props = ["changeset-id", "S3", "object-key-url"]
        expected_optional_props = ["replicated-buckets"]
        req_props, opt_props = get_notification_schema_changeset_object_props(schema)
        self.assertEqual(set(expected_required_props), set(req_props))
        self.assertEqual(set(expected_optional_props), set(opt_props))

    # + build_changeset_object_notification - with optional schema key
    @patch('service_commons.object_event_handler.get_notification_schema_changeset_object_props')
    def test_build_changeset_object_notification_with_optional_schema_key(self,
                                                                          mock_get_notification_schema_changeset_object_props):
        # We mock an optional schema prop, because context currently does not have any
        mock_get_notification_schema_changeset_object_props.return_value = (
            ["changeset-id", "S3", "object-key-url"], ["replicated-buckets"])

        schema = {"does not matter": "we mock the response"}
        changeset_id = "mock_id"
        ost = io_utils.load_data_json("object_store_item_expected.json")
        expected_changeset_object = io_utils.load_data_json("valid_changeset_object.json")
        self.assertEqual(expected_changeset_object,
                         build_changeset_object_notification(changeset_id, schema, ost))

    # - build_changeset_object_notification - missing key terminal
    @patch('service_commons.object_event_handler.get_notification_schema_changeset_object_props')
    def test_build_changeset_object_notification_terminal_missing_key(self,
                                                                      mock_get_notification_schema_changeset_object_props):
        # We mock an optional schema prop, because context currently does not have any
        mock_get_notification_schema_changeset_object_props.return_value = (
            ["changeset-id", "S3", "object-key-url", "random-new-attribute"], ["replicated-buckets"])

        schema = {"does not matter": "we mock the response"}
        changeset_id = "mock_id"
        ost = io_utils.load_data_json("object_store_item_expected.json")
        with self.assertRaisesRegex(TerminalErrorException, "Failed to generate changeset object notification. "
                                                            "Missing required property: 'random-new-attribute'"):
            build_changeset_object_notification(changeset_id, schema, ost)


if __name__ == '__main__':
    unittest.main()
