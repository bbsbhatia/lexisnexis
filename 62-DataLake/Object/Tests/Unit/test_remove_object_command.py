import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NoSuchCollection, NotAuthorizedError, \
    InvalidRequestPropertyName, SemanticError
from lng_datalake_commons.tracking.exceptions import InvalidRequestPropertyName as TrackingInvalidRequestPropertyName
from lng_datalake_constants import collection_status, object_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from remove_object_command import remove_object_command, lambda_handler, generate_response_json

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveObjectCommand')


class TestRemoveObjectCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.collection_blocker_patch = patch(
            "lng_datalake_commons.tracking.tracker._validate_collection").start()
        cls.collection_blocker_patch.return_value = None

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # +Schema validation error using lambda_handler with command_wrapper decorator
    # - request missing object-id required property
    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    def test_command_decorator_fail_1(self, tt_put_item_mock):
        tt_put_item_mock.return_value = None
        request_input = io_util.load_data_json('apigateway.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaises(InvalidRequestPropertyName):
            lambda_handler(request_input, MockLambdaContext())

    # - request missing collection-id required property: Error caught in tracking
    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    def test_command_decorator_fail_2(self, tt_put_item_mock):
        tt_put_item_mock.return_value = None
        request_input = io_util.load_data_json('apigateway.failure_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaises(TrackingInvalidRequestPropertyName):
            lambda_handler(request_input, MockLambdaContext())

    # +Successful test of Lambda for collection that has object versions kept
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('service_commons.object_common.generate_object_expiration_time')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_remove_object_command_success_1(self, ct_get_item_mock, mock_owner_authorization,
                                             est_put_item_mock, time_helper_mock,
                                             mock_changeset_get_item, mock_tt_put):
        mock_tt_put.return_value = None
        mock_owner_authorization.return_value = {}
        time_helper_mock.return_value = 1527276677
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1,
                'object-expiration': "24h"
            }
        mock_changeset_get_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }
        est_put_item_mock.return_value = None

        request_input = io_util.load_data_json('apigateway.accepted_1.json')
        expected_response_output = \
            {
                "collection-id": '274',
                "collection-url": "/collections/LATEST/274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "object-url": "/objects/LATEST/49cf62814f629ba0a679147d8784b516cde30a17?collection-id=274",
                "object-expiration-date": "2018-05-25T19:31:17.000Z",
                "owner-id": 100,
                "asset-id": 62,
                "object-state": object_status.REMOVED,
                'changeset-id': 'jek-changeset'
            }
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            actual_resp = lambda_handler(request_input, MockLambdaContext())['response']
        self.assertDictEqual(actual_resp, expected_response_output)

    # -Failure test of Lambda for collection that gets ClientError
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_object_command_fail_2(self, session_mock, ct_get_item_mock):
        session_mock.return_value = None
        ct_get_item_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        request_input = \
            {
                "collection-id": '275',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }

        with self.assertRaisesRegex(InternalError, "Unable to get item from Collection Table"):
            remove_object_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # -Failure test collectionTable.get_item returns None
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_object_command_fail_3(self, session_mock, ct_get_item_mock):
        session_mock.return_value = None
        ct_get_item_mock.return_value = {}
        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }

        with self.assertRaisesRegex(NoSuchCollection, "Invalid Collection ID 274"):
            remove_object_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # -Failed test of Lambda - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_object_command_fail_4(self, session_mock, ct_get_item_mock, mock_owner_authorization):
        session_mock.return_value = None
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }

        with self.assertRaisesRegex(NotAuthorizedError, "API Key provided is not valid for Owner ID"):
            remove_object_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # -Failure test of Lambda for collection that not in created state
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_object_command_fail_5(self, session_mock, ct_get_item_mock):
        session_mock.return_value = None
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.TERMINATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }

        with self.assertRaisesRegex(SemanticError, "Collection ID 274 is not set to Created"):
            remove_object_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # - generate_response_json: no owner-id
    def test_generate_response_json_fail(self):
        with self.assertRaisesRegex(InternalError, 'Missing required property: owner-id'):
            generate_response_json({'collection-id': 'mock-collection-id'}, 'object-id', 'stage', 0, '')

if __name__ == '__main__':
    unittest.main()
