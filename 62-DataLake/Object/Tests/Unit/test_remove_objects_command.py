import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyName, InvalidRequestPropertyValue, InternalError
from lng_datalake_constants import event_names
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import remove_objects_command as remove_objects_command_lambda
from remove_objects_command import remove_objects_command, lambda_handler, validate_batch, remove_duplicates, \
    write_batch_to_staging, generate_response_json

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveObjectsCommand')


class TestRemoveObjectsCommand(unittest.TestCase):
    session_patch = None
    collection_blocker_patch = None

    @classmethod
    @patch.dict(os.environ, {'DATALAKE_STAGING_BUCKET_NAME': 'DATALAKE_STAGING_BUCKET_NAME_TEST'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.collection_blocker_patch = patch("lng_datalake_commons.tracking.tracker._put_collection_blocker").start()
        cls.collection_blocker_patch.return_value = None
        reload(remove_objects_command_lambda)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        cls.collection_blocker_patch.stop()

    # + lambda_handler - Success
    @patch('service_commons.object_command.validate_and_get_changeset_id')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch("remove_objects_command.write_batch_to_staging")
    @patch("remove_objects_command.validate_batch")
    @patch("lng_datalake_commands.owner_authorization.authorize")
    @patch('service_commons.object_command.get_collection_response')
    def test_lambda_handler_success(self, get_collection_mock, authorize_owner_mock, validate_batch_mock,
                                    write_batch_mock, put_event_mock, get_changeset_mock):
        get_collection_mock.return_value = io_util.load_data_json('collectionTable.get_item.valid.json')
        get_changeset_mock.return_value = io_util.load_data_json('changesetTable.get_item.valid.json')
        authorize_owner_mock.return_value = None
        validate_batch_mock.return_value = None
        write_batch_mock.return_value = None
        put_event_mock.return_value = None
        request_input = io_util.load_data_json('apigateway.request.accepted.json')
        response_dict = {'collection-id': '274',
                         'collection-url': '/collections/LATEST/274',
                         'owner-id': 100,
                         'asset-id': 62,
                         'object-count': 2,
                         'remove-timestamp': '2018-05-18T19:31:17.813Z',
                         'description': 'batch remove object description',
                         'changeset-id': 'jek-changeset'}
        event_dict = {'event-id': '1234-request-id-wxyz',
                      'request-time': '2018-05-18T19:31:17.813Z',
                      'event-name': event_names.OBJECT_BATCH_REMOVE,
                      'collection-id': '1234',
                      'event-version': 1,
                      'stage': 'LATEST',
                      'description': 'batch remove object description',
                      'changeset-id': 'jek-changeset',
                      'changeset-expiration-epoch': 1234567890}
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(response_dict, lambda_handler(request_input, MockLambdaContext())['response'])

        put_event_mock.call_once_with(event_dict)

    # - lambda_handler - Schema validation error. Request missing object-ids required property.
    @patch('lng_datalake_commons.tracking.tracker._delete_collection_blocker')
    def test_lambda_handler_schema_validation_fail(self, delete_blocker_mock):
        delete_blocker_mock.return_value = None
        request_input = io_util.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    # - lambda_handler - Schema validation error. Request missing collection-ids required property.
    def test_lambda_handler_tracking_fail(self):
        request_input = io_util.load_data_json('apigateway.request.failure_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(InvalidRequestPropertyName,
                                    r"data must contain \['collection-id', 'object-ids'\] properties"):
            lambda_handler(request_input, MockLambdaContext())

    # - remove_objects_command - Invalid description length
    def test_remove_objects_command_invalid_description(self):
        request_input = io_util.load_data_json('apigateway.request.failure_3.json')
        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid Description||Description must be less than 100 characters"):
            remove_objects_command(request, request_id, stage, api_key_id)

    # + validate_batch - Success
    @patch("service_commons.object_common.validate_object_id")
    def test_validate_batch_success(self, validate_obj_id_mock):
        validate_obj_id_mock.return_value = None
        object_ids = ['object-1', 'object-2']
        self.assertIsNone(validate_batch(object_ids))

    # - validate_batch - invalid batch with 0 object ids
    def test_validate_batch_invalid_batch_size_1(self):
        object_ids = []
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid batch size {}".format(len(object_ids))):
            validate_batch(object_ids)

    # - validate_batch - invalid batch with more than 20000 object ids
    def test_validate_batch_invalid_batch_size_2(self):
        object_ids = ['object-id-test'] * (remove_objects_command_lambda.max_remove_object_batch_size + 1)
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid batch size {}".format(len(object_ids))):
            validate_batch(object_ids)

    # - validate_batch - batch with invalid object id
    @patch("service_commons.object_common.validate_object_id")
    def test_validate_batch_invalid_object_id(self, validate_obj_id_mock):
        object_ids = ['object-1', 'object-id-with-invalid-character-%']
        validate_obj_id_mock.side_effect = ["", "Object ID contains invalid characters"]
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid object-id in batch at index {0}".format(1)):
            validate_batch(object_ids)

    # + remove_duplicates_success - no duplicates
    def test_remove_duplicates_success(self):
        object_ids = ['object_1', 'object_2', 'object3']
        unique_ids = remove_duplicates(object_ids)
        self.assertEqual(len(object_ids), len(unique_ids))

    # + remove_duplicates_has_dups_success - has duplicates
    def test_remove_duplicates_has_dups_success(self):
        object_ids = ['object_1', 'object_2', 'object_3', 'object_2']
        unique_ids = remove_duplicates(object_ids)
        self.assertEqual(len(object_ids) - 1, len(unique_ids))

    # + write_batch_to_staging - Success
    @patch("service_commons.object_common.build_batch_key")
    @patch("lng_aws_clients.s3.put_s3_object")
    def test_write_batch_to_staging_success(self, s3_put_object_mock, build_batch_key_mock):
        request_id = "request-id-test"
        stage = "LATEST"
        object_ids = ['object-1', 'object-2']
        description = "remove object description"
        build_batch_key_mock.return_value = "batch-key"
        s3_put_object_mock.return_value = None
        self.assertIsNone(write_batch_to_staging('batch-key', object_ids, description))
        s3_put_object_mock.called_once()

    # - write_batch_to_staging - ClientError when called put_s3_object
    @patch("service_commons.object_common.build_batch_key")
    @patch("lng_aws_clients.s3.put_s3_object")
    def test_write_batch_to_staging_client_error(self, s3_put_object_mock, build_batch_key_mock):
        request_id = "request-id-test"
        stage = "LATEST"
        object_ids = ['object-1', 'object-2']
        description = "remove object description"
        build_batch_key_mock.return_value = "batch-key"
        s3_put_object_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {
                                                          'Code': 'OTHER',
                                                          'Message': 'This is a mock'}},
                                                     "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to create S3 object"):
            write_batch_to_staging('batch-key', object_ids, description)

    # - write_batch_to_staging - Unhandled Exception when called put_s3_object
    @patch("service_commons.object_common.build_batch_key")
    @patch("lng_aws_clients.s3.put_s3_object")
    def test_write_batch_to_staging_unhandled_exception(self, s3_put_object_mock, build_batch_key_mock):
        request_id = "request-id-test"
        stage = "LATEST"
        object_ids = ['object-1', 'object-2']
        description = "remove object description"
        build_batch_key_mock.return_value = "batch-key"
        s3_put_object_mock.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            write_batch_to_staging('batch-key', object_ids, description)

    # - generate_response_json - missing required property
    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    def test_generate_response_json_fail(self, get_schema_mock):
        get_schema_mock.return_value = ['collection-id']
        collection_response = {}
        stage = "LATEST"
        batch_properties = {}
        with self.assertRaisesRegex(InternalError, "Missing required property:"):
            generate_response_json(batch_properties, collection_response, stage, 12)


if __name__ == '__main__':
    unittest.main()
