import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, InvalidRequestPropertyName
from lng_datalake_constants import collection_status, object_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from list_objects_command import list_objects_command, lambda_handler

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListObjectsCommand')


class TestListObjectsCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # -Schema validation error using lambda_handler with command_wrapper decorator
    # - request has max-results property that is too large
    def test_command_decorator_fail1(self):
        request_input = io_util.load_data_json('apigateway.request.failure_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    # +Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_decorator_success(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                }
            ]

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # +Successful test of Lambda for collection that supports versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_success_versions(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy1",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "d3fffd455df319026d01bc35cc6f8b533854fb2e",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy4",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "dae21321499962cc22f62fc301a884e11635e8e0",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                }
            ]

        request_input = \
            {
                "collection-id": '274'
            }
        expected_response_output = \
            {'item-count': 5,
             'objects': [{'S3': {'Bucket': 'ccs-sandbox-datalake-object-store',
                                 'Key': '25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d'},
                          'collection-id': '274',
                          'object-id': 'dummy2',
                          'object-key-url': 'https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d',
                          'object-state': 'Created',
                          'raw-content-length': 24},
                         {'S3': {'Bucket': 'ccs-sandbox-datalake-object-store',
                                 'Key': '25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d'},
                          'collection-id': '274',
                          'object-id': 'dummy3',
                          'object-key-url': 'https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d',
                          'object-state': 'Created',
                          'raw-content-length': 24},
                         {'S3': {'Bucket': 'ccs-sandbox-datalake-object-store',
                                 'Key': 'd3fffd455df319026d01bc35cc6f8b533854fb2e'},
                          'collection-id': '274',
                          'object-id': 'dummy1',
                          'object-key-url': 'https://datalake_url.com/objects/store/d3fffd455df319026d01bc35cc6f8b533854fb2e',
                          'object-state': 'Created',
                          'raw-content-length': 24},
                         {'S3': {'Bucket': 'ccs-sandbox-datalake-object-store',
                                 'Key': '49cf62814f629ba0a679147d8784b516cde30a17'},
                          'collection-id': '274',
                          'object-id': '49cf62814f629ba0a679147d8784b516cde30a17',
                          'object-key-url': 'https://datalake_url.com/objects/store/49cf62814f629ba0a679147d8784b516cde30a17',
                          'object-state': 'Created',
                          'raw-content-length': 24},
                         {'S3': {'Bucket': 'ccs-sandbox-datalake-object-store',
                                 'Key': 'dae21321499962cc22f62fc301a884e11635e8e0'},
                          'collection-id': '274',
                          'object-id': 'dummy4',
                          'object-key-url': 'https://datalake_url.com/objects/store/dae21321499962cc22f62fc301a884e11635e8e0',
                          'object-state': 'Created',
                          'raw-content-length': 24}]}

        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_success_1(self, ct_get_item_mock, ost_query_items_mock,mock_get_request_time):
        mock_get_request_time.return_value = '2019-10-18T18:01:03.781Z'
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312",
                    "pending-expiration-epoch": 1571544000
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy4",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312",
                    "pending-expiration-epoch": 1571284800
                }

            ]

        request_input = \
            {
                "collection-id": '274'
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy2",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy3",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        }
                    ],
                "item-count": 2
            }

        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # -Failure test of Lambda - exception when querying ObjectStoreTable
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_fail_1(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.side_effect = ClientError({}, "dummy")

        request_input = \
            {
                "collection-id": '274'
            }

        with self.assertRaisesRegex(InternalError, "Unable to query items from Object Store Table"):
            list_objects_command(request_input)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_success_2(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                }

            ]

        request_input = \
            {
                "collection-id": '274',
                "object-state": 'created'
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy2",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy3",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        }
                    ],
                "item-count": 2
            }

        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_success_3(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.REMOVED,
                    "collection-id": '274',
                    "object-id": "dummy34",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                }

            ]

        request_input = \
            {
                "collection-id": '274',
                "object-state": 'all'
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy2",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy3",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "collection-id": '274',
                            "object-id": "dummy34",
                            "object-state": object_status.REMOVED
                        }
                    ],
                "item-count": 3
            }

        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_all_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_success_4(self, ct_get_item_mock, ost_get_all_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_get_all_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '275',
                    "object-id": "dummy35",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                }

            ]

        request_input = \
            {
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy2",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy3",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '275',
                            "object-id": "dummy35",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        }
                    ],
                "item-count": 3
            }

        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # -Failure test of Lambda - exception when getting all items from ObjectStoreTable
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_all_items')
    def test_list_objects_command_fail_2(self, ost_get_all_items_mock):
        ost_get_all_items_mock.side_effect = ClientError({}, "dummy")

        request_input = \
            {
            }

        with self.assertRaisesRegex(InternalError, "Unable to get item from Object Store Table"):
            list_objects_command(request_input)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_all_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_success_6(self, ct_get_item_mock, ost_get_all_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_get_all_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.REMOVED,
                    "collection-id": '274',
                    "object-id": "dummy34",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '275',
                    "object-id": "dummy35",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                }

            ]

        request_input = \
            {
                "object-state": "all"
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy2",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy3",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "collection-id": '274',
                            "object-id": "dummy34",
                            "object-state": object_status.REMOVED
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '275',
                            "object-id": "dummy35",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        }
                    ],
                "item-count": 4
            }
        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # +Successful test of Lambda with max-results
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_pagination_token')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_success_max_results(self, ct_get_item_mock, ost_query_items_mock,
                                                      ost_pagination_token_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                }
            ]
        ost_pagination_token_mock.return_value = "abcd"

        request_input = \
            {
                "collection-id": '274',
                "max-items": 1
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy2",
                            "object-state": "Created",
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"

                        }
                    ],
                "next-token": "eyJtYXgtaXRlbXMiOjEsInBhZ2luYXRpb24tdG9rZW4iOiJhYmNkIn0=",
                "item-count": 1
            }

        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # +Successful test of Lambda with max-items
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_pagination_token')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_success_max_results_2(self, ct_get_item_mock, ost_query_items_mock,
                                                        ost_pagination_token_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                }
            ]
        ost_pagination_token_mock.return_value = "abcde"

        request_input = \
            {
                "collection-id": '274',
                "max-items": 1,
                "next-token": 'eyJtYXgtaXRlbXMiOiAxLCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkIn0='
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "object-id": "dummy2",
                            "object-state": "Created",
                            "raw-content-length": 24,
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"

                        }
                    ],
                "next-token": "eyJtYXgtaXRlbXMiOjEsInBhZ2luYXRpb24tdG9rZW4iOiJhYmNkZSJ9",
                "item-count": 1
            }

        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # -Failure test of Lambda for invalid max-items

    def test_list_objects_command_fail_invalid_max_results(self):
        request_input = \
            {
                "collection-id": '274',
                "max-items": 1001

            }

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid max-items value:"):
            list_objects_command(request_input)

    # -Failure test of Lambda for invalid max-items

    def test_list_objects_command_fail_invalid_max_results_2(self):
        request_input = \
            {
                "collection-id": '274',
                "max-items": 1,
                "next-token": "eyJtYXgtaXRlbXMiOiAzLCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkIn0=",

            }

        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Continuation request must pass the same max-items as initial request*"):
            list_objects_command(request_input)

    # -Failure test of Lambda for invalid max-items

    def test_list_objects_command_fail_invalid_object_states(self):
        request_input = \
            {
                "collection-id": '274',
                'state': 'xyz'
            }

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid state value:"):
            list_objects_command(request_input)

    # -Failure test of Lambda for collection that doesn't exist and exception when reading CollectionTable
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_fail_invalid_collection(self, ct_get_item_mock):
        ct_get_item_mock.return_value = {}

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number": 1
            }

        expected_response_output = {'objects': [], 'item-count': 0}
        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)

    # -Failure test of Lambda for collection that is terminated
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_list_objects_command_fail_collection_terminated(self, ct_get_item_mock,
                                                             ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.TERMINATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain"
                }
            ]

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number": 1
            }

        expected_response_output = \
            {'objects': [{'collection-id': '274', 'object-id': 'dummy2', 'object-state': 'Removed'},
                         {'collection-id': '274', 'object-id': 'dummy3', 'object-state': 'Removed'}],
             'item-count': 2}

        self.assertDictEqual(list_objects_command(request_input)['response-dict'], expected_response_output)


if __name__ == '__main__':
    unittest.main()
