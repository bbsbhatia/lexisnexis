import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, InvalidRequestPropertyName, \
    NotAuthorizedError, SemanticError, NoSuchCollection, UNSUPPORTED_MEDIA_TYPE
from lng_datalake_constants import collection_status, event_names, object_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
import update_object_command as update_object_command_lambda
from update_object_command import update_object_command, lambda_handler, generate_response_json, \
    process_large_object_request, generate_large_object_event_dict

__author__ = "Shekhar Ralhan"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateObjectCommand')


class TestUpdateObjectCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATALAKE_STAGING_BUCKET_NAME': 'DATALAKE_STAGING_BUCKET_NAME_TEST'})
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.collection_blocker_patch = patch("lng_datalake_commons.tracking.tracker._validate_collection").start()
        cls.collection_blocker_patch.return_value = None
        reload(update_object_command_lambda)
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        cls.collection_blocker_patch.stop()

    # Schema validation error using lambda_handler with command_wrapper decorator
    # - request missing object-id required property
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_command_schema_validation_fail(self, tt_put_mock):
        tt_put_mock.return_value = None
        request_input = io_util.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    # Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('service_commons.object_command.get_previous_and_current_events')
    @patch('service_commons.object_common.generate_object_expiration_time')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_object_command_success(self, ct_get_item_mock, mock_owner_authorization, ost_query_items_mock,
                                           ost_get_item__mock, put_s3_object_mock, est_put_item_mock, tt_put_mock,
                                           time_helper_mock, get_pending_event_mock, mock_get_changeset_item):
        mock_owner_authorization.return_value = {}
        time_helper_mock.return_value = 1527276677
        tt_put_mock.return_value = None
        ost_get_item__mock.return_value = {'object-state': object_status.CREATED,
                                           'object-key': 'f4f76a67a918839d4576072376fb7ef6644210d3_274',
                                           'content-type': 'text/plain',
                                           'pending-expiration-epoch': 123123123}
        get_pending_event_mock.return_value = ({}, {})
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1,
                'object-expiration': '7d'
            }
        mock_get_changeset_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }
        ost_query_items_mock.return_value = []
        put_s3_object_mock.return_value = {}

        request_input = io_util.load_data_json('apigateway.request.accepted_2.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with patch('lng_datalake_commands.command_wrapper._response_schema', io_util.load_schema_json(
                    'update_object_command-ResponseSchema.json')):
                self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)

    # Successful test using lambda_handler with command_wrapper decorator for large object
    @patch('service_commons.object_command.get_previous_and_current_events')
    @patch('service_commons.object_common.generate_object_expiration_time')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.item_exists')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_object_command_large_object_success(self, ct_get_item_mock, mock_owner_authorization,
                                                        ost_get_item_mock,
                                                        ost_item_exists_mock, put_s3_object_mock, s3_client_mock,
                                                        est_put_item_mock, tt_put_item, time_helper_mock,
                                                        get_pending_events_mock):
        mock_owner_authorization.return_value = {}
        time_helper_mock.return_value = 1527276677
        tt_put_item.return_value = False
        ost_item_exists_mock.return_value = False
        est_put_item_mock.return_value = False
        s3_client_mock.return_value.generate_presigned_url.return_value = "https://mock_presigned_url"
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1,
                'object-expiration': '7d'
            }
        ost_get_item_mock.return_value = {
            "collection-id": '274',
            "object-id": "UserProvidedObjectID",
            "content-type": "text/plain",
            'object-key': '123',
            'object-state': 'Created',
            'raw-content-length': 33,
            'pending-expiration-epoch': 818181

        }
        get_pending_events_mock.return_value = ({}, {})
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_largeobject_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_largeobject_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # -Failed test of Lambda - invalid content-type
    def test_update_object_command_invalid_content_type(self):
        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        request = request_input['request']
        request['content-type'] = 'invalid/content'
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        with self.assertRaisesRegex(UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"):
            update_object_command(request, request_id, stage, api_key_id)

    # -Failed test of Lambda - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_update_object_command_owner_auth(self, session_mock, ct_get_item_mock, mock_owner_authorization):
        session_mock.return_value = None
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        with self.assertRaisesRegex(NotAuthorizedError, "API Key provided is not valid for Owner ID"):
            update_object_command(request, request_id, stage, api_key_id)

    @patch('service_commons.object_command.get_previous_and_current_events')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_base64_encoding_success(self, ct_get_item_mock, mock_owner_authorization, ost_query_items_mock,
                                             ost_get_item__mock, put_s3_object_mock, est_put_item_mock, tt_put_mock,
                                             get_pending_events_mock):
        mock_owner_authorization.return_value = {}
        tt_put_mock.return_value = None
        ost_get_item__mock.return_value = {'object-state': object_status.CREATED,
                                           'object-key': 'f4f76a67a918839d4576072376fb7ef6644210d3_',
                                           'content-type': 'text/plain'}

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1
            }
        ost_query_items_mock.return_value = []
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}
        get_pending_events_mock.return_value = ({}, {})
        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        request_input['request']['body'] = "VGhpcyBpcyBzb21lIHRlc3QgdGV4dC4="
        request_input['request']['body-encoding'] = "base64"
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_object_command_collection_response_client_error(self, ct_get_item_mock):
        ct_get_item_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            request = request_input['request']
            request_id = request_input['context']['client-request-id']
            stage = request_input['context']['stage']
            api_key_id = request_input['context']['api-key-id']

            self.assertRaises(InternalError, update_object_command, request, request_id, stage, api_key_id)

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_object_command_collection_response_general_exception(self, ct_get_item_mock):
        ct_get_item_mock.side_effect = Exception

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            request = request_input['request']
            request_id = request_input['context']['client-request-id']
            stage = request_input['context']['stage']
            api_key_id = request_input['context']['api-key-id']

            self.assertRaises(InternalError, update_object_command, request, request_id, stage, api_key_id)

    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_object_command_collection_response_empty(self, ct_get_item_mock):
        ct_get_item_mock.return_value = []

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(NoSuchCollection, "Invalid Collection ID 274"):
                update_object_command(request, request_id, stage, api_key_id)

    # - collection not in Created state
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_object_command_collection_status_not_created(self, ct_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.TERMINATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            request = request_input['request']
            request_id = request_input['context']['client-request-id']
            stage = request_input['context']['stage']
            api_key_id = request_input['context']['api-key-id']

            self.assertRaises(SemanticError, update_object_command, request, request_id, stage, api_key_id)

    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_object_command_ost_get_item_client_error_fail(self, ct_get_item_mock,
                                                                  ost_get_item__mock):
        ost_get_item__mock.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {
                                                          'Code': 'OTHER',
                                                          'Message': 'This is a mock'}},
                                                     "FAKE")
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            request = request_input['request']
            request_id = request_input['context']['client-request-id']
            stage = request_input['context']['stage']
            api_key_id = request_input['context']['api-key-id']

            self.assertRaises(InternalError, update_object_command, request, request_id, stage, api_key_id)

    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_update_object_command_ost_get_item_general_exception_fail(self, ct_get_item_mock,
                                                                       ost_get_item__mock):
        ost_get_item__mock.side_effect = Exception

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            request = request_input['request']
            request_id = request_input['context']['client-request-id']
            stage = request_input['context']['stage']
            api_key_id = request_input['context']['api-key-id']

            self.assertRaises(InternalError, update_object_command, request, request_id, stage, api_key_id)

    # - generate_response_json - Missing required attribute
    def test_generate_response_json_fail(self):
        self.assertRaises(InternalError, generate_response_json, {}, 1, 1, 'Object::Update', "LATEST", {})

    # + tests process_large_object_request
    @patch("lng_aws_clients.s3.get_client")
    @patch('service_commons.object_common.build_large_object_key')
    @patch('service_commons.object_command.encode_md5')
    def test_process_large_object_request_success(self, encoding_mock, service_commons_mock, s3_mock):
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request_input = {
            'content-md5': 123,
            'content-type': 'application/json'
        }
        object_collection_response = {
            "collection-id": '3',
            "collection-state": collection_status.CREATED,
            "collection-timestamp": "2017-12-06T20:33:37.618Z",
            "collection-hash": 'abcdefg',
            "owner-id": 100,
            "asset-id": 62,
            "old-object-versions-to-keep": -1
        }
        event_prop = {'event-name': event_names.OBJECT_UPDATE_PENDING,
                      'event-id': 1}
        object_id = '2'
        collection_id = '3'
        encoding_mock.return_value = 321
        service_commons_mock.return_value = 789

        s3_mock.return_value.generate_presigned_url.return_value = 'mock_url'
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with patch('lng_datalake_commands.command_wrapper._response_schema', io_util.load_schema_json(
                    'update_object_command-ResponseSchema.json')):
                actual = process_large_object_request(request_input, event_prop, object_id, object_collection_response,
                                                      collection_id, 'LATEST', 0)
        expected = {
            "object": {
                "object-state": "Pending",
                "collection-id": '3',
                "collection-url": "/collections/LATEST/3",
                "object-id": '2',
                "object-url": "/objects/LATEST/2?collection-id=3",
                "owner-id": 100,
                "asset-id": 62,
                "temporary-object-key-url": ""
            },
            "large-upload": {
                'url': 'mock_url',
                'headers': {
                    'Content-Type': 'application/json',
                    'Content-MD5': 321
                }
            }
        }
        self.maxDiff = None
        self.assertDictEqual(expected, actual)

    # - tests process_large_object_request throwing an error for not finding required content-md5
    def test_process_large_object_request_no_md5(self):
        request_input = {}
        event_prop = {'event-name': event_names.OBJECT_UPDATE_PENDING,
                      'event-id': 1}
        with self.assertRaises(InvalidRequestPropertyValue):
            process_large_object_request(request_input, event_prop, 'object-id', {}, 'collection-id', 'LATEST', 0)

    # + test_generate_large_object_event_dict
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_large_object_event_dict(self, mock_command_wrapper):
        mock_command_wrapper.return_value = "2019-08-31T05:42:12.444Z"
        expected = {
            'event-id': '123',
            'request-time': '2019-08-31T05:42:12.444Z*',
            'event-name': "Object::UpdatePending",
            'object-id': '1',
            'collection-id': '3',
            "collection-hash": "abcdefg",
            'bucket-name': 'DATALAKE_STAGING_BUCKET_NAME_TEST',
            'content-type': 'application/json',
            'raw-content-length': 0,
            "old-object-versions-to-keep": -1,
            'is-large-object-request': True,
            'event-version': 1,
            'stage': 'LATEST'
        }
        object_id = '1'
        collection_response = {
            "collection-id": "3",
            "collection-hash": "abcdefg",
            "old-object-versions-to-keep": -1
        }

        event_prop = {
            'event-id': '123',
            'event-name': "Object::UpdatePending",
        }

        actual = generate_large_object_event_dict(object_id, collection_response, event_prop,
                                                  'application/json', 'LATEST', 0)
        self.assertDictEqual(expected, actual)


if __name__ == '__main__':
    unittest.main()
