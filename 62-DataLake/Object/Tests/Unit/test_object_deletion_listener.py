import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils

import object_deletion_listener_lambda

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "ObjectDeletionListener")


class ObjectDeletionListenerTest(TestCase):

    # + test_dispatch_deletes: threads match records
    @patch('object_deletion_listener_lambda.process_records')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_dispatch_deletes(self, mock_set_session, mock_sqs_client, mock_process_records):
        mock_set_session.return_value = None
        mock_sqs_client.return_value = None
        mock_process_records.return_value = None
        self.assertEqual(event_handler_status.SUCCESS, object_deletion_listener_lambda.dispatch_deletes(
            io_utils.load_data_json('dynamo_stream_multi_record_valid.json')))
        self.assertEqual(3, mock_process_records.call_count)

    # + test_process_records_sqs
    @patch("object_deletion_listener_lambda.delete_object")
    def test_process_records_sqs(self, mock_delete_object):
        input_dict = io_utils.load_data_json('sqs_message_valid.json')
        mock_delete_object.return_value = None
        self.assertIsNone(object_deletion_listener_lambda.process_records(input_dict["Records"]))

    # + test_process_records_sqs_too_many_retries: too many retries to the terminal bucket
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_process_records_sqs_too_many_retries(self, mock_error_handler):
        input_dict = io_utils.load_data_json('sqs_message_too_many_retries.json')
        object_deletion_listener_lambda.process_records(input_dict["Records"])
        mock_error_handler.assert_called_once()

    # + test_process_records_dynamo: records are split and dispatched to threads
    @patch('object_deletion_listener_lambda.delete_object')
    @patch('object_deletion_listener_lambda.is_valid_ddb_stream_event_type')
    @patch('object_deletion_listener_lambda.is_valid_image_type')
    def test_process_records_dynamo(self, mock_valid_image, mock_valid_event_type, mock_delete_object):
        input_dict = io_utils.load_data_json('dynamo_stream_valid.json')
        mock_valid_image.return_value = True
        mock_valid_event_type.return_value = True
        mock_delete_object.return_value = None
        self.assertIsNone(object_deletion_listener_lambda.process_records(input_dict['Records']))

    # + test_process_records_dynamo_modify: Modify record is processed
    @patch('object_deletion_listener_lambda.delete_object')
    @patch('object_deletion_listener_lambda.is_valid_ddb_stream_event_type')
    @patch('object_deletion_listener_lambda.is_valid_image_type')
    def test_process_records_dynamo_modify(self, mock_valid_image, mock_valid_event_type, mock_delete_object):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_valid_record.json')
        mock_valid_image.return_value = True
        mock_valid_event_type.return_value = False
        mock_delete_object.return_value = None
        self.assertIsNone(object_deletion_listener_lambda.process_records(input_dict))

    # + test_process_records_dynamo_invalid_event
    @patch('object_deletion_listener_lambda.delete_object')
    def test_process_records_dynamo_invalid_event(self, mock_delete_object):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_invalid_record_new_image.json')
        mock_delete_object.return_value = None
        self.assertIsNone(object_deletion_listener_lambda.process_records(input_dict))

    # + test_is_valid_image_type: Positive test
    def test_is_valid_image_type(self):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_valid_record.json')
        self.assertTrue(object_deletion_listener_lambda.is_valid_image_type(input_dict))

    # - is_valid_image_type: invalid record
    def test_is_valid_image_type_invalid_1(self):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_invalid_record_no_dynamodb_key.json')
        self.assertFalse(object_deletion_listener_lambda.is_valid_image_type(input_dict))

    # - test_valid_dynamo_stream_record: new image
    def test_is_valid_image_type_invalid_2(self):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_invalid_record_new_image.json')
        self.assertFalse(object_deletion_listener_lambda.is_valid_image_type(input_dict))

    # + test_is_valid_ddb_stream_event_type: Valid
    def test_is_valid_ddb_stream_event_type(self):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_valid_record.json')
        self.assertTrue(object_deletion_listener_lambda.is_valid_ddb_stream_event_type(input_dict))


    # - test_valid_dynamo_stream_record: no event name
    def test_is_valid_ddb_stream_event_type_invalid_2(self):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_invalid_record_no_event_name.json')
        self.assertFalse(object_deletion_listener_lambda.is_valid_ddb_stream_event_type(input_dict))

    # - test_valid_dynamo_stream_record: modify but keys are the same
    def test_is_valid_ddb_stream_event_type_invalid_3(self):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_modify_matching_keys.json')
        self.assertFalse(object_deletion_listener_lambda.is_valid_ddb_stream_event_type(input_dict))

    # - test_valid_dynamo_stream_record: tombstone delete version
    def test_is_valid_ddb_stream_event_type_invalid_4(self):
        input_dict = io_utils.load_data_json('dynamo_stream_transformed_tombstone_record.json')
        self.assertFalse(object_deletion_listener_lambda.is_valid_ddb_stream_event_type(input_dict))

    # + test_delete_object: Positive test
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    def test_delete_object_valid_1(self, s3_mock, sqs_mock):
        input_dict = {
            "bucket-name": "bucket",
            "object-id": "321",
            "collection-id": "collection",
            "object-key": "ob321",
            "object-hash": "abcd",
            "raw-content-length": 65,
            "version-number": 1,
            "version-timestamp": "2018-11-06T16:57:53.981Z",
            "replicated-buckets": [
                "bucket-2"
            ]
        }
        self.assertIsNone(object_deletion_listener_lambda.delete_object(input_dict, "REMOVE"))
        self.assertEqual(2, s3_mock.call_count)
        self.assertEqual(0, sqs_mock.call_count)

    # + test_delete_object: Positive test - Old object type dont remove
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    def test_delete_object_valid_2(self, s3_mock, sqs_mock):
        input_dict = {
            "bucket-name": "bucket",
            "object-id": "321",
            "collection-id": "collection",
            "object-key": "ob321",
            "raw-content-length": 65,
            "version-number": 1,
            "version-timestamp": "2018-11-06T16:57:53.981Z",
            "replicated-buckets": [
                "bucket-2"
            ]
        }
        self.assertIsNone(object_deletion_listener_lambda.delete_object(input_dict, "REMOVE"))
        self.assertEqual(0, s3_mock.call_count)
        self.assertEqual(0, sqs_mock.call_count)

    # + test_delete_object: Folder object
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    def test_delete_object_valid_3(self, s3_mock, sqs_mock):
        input_dict = {
            "bucket-name": "bucket",
            "composite-key": "321|collection",
            "object-id": "321",
            "collection-id": "collection",
            "object-key": "ob321/",
            "raw-content-length": 65,
            "content-type": "multipart/mixed",
            "replicated-buckets": ['bucket-usw2']
        }
        with patch('object_deletion_listener_lambda.FOLDER_OBJECT_DELETE_SQS_URL', "fake_sqs_url"):
            self.assertIsNone(object_deletion_listener_lambda.delete_object(input_dict, "REMOVE"))
        sqs_mock.assert_called_once()
        sqs_mock.return_value.send_message.assert_called_with(
            MessageBody='{\"object-key\":\"ob321/\",\"bucket-name\":\"bucket\",\"replicated-buckets\":[\"bucket-usw2\"]}',
            QueueUrl="fake_sqs_url")
        self.assertEqual(0, s3_mock.call_count)

    # - test_delete_object: ClientError on s3 deletion
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    def test_delete_object_s3_client_error(self,  s3_mock, sqs_mock):
        input_dict = {
            "bucket-name": "bucket",
            "composite-key": "321|collection",
            "object-id": "321",
            "collection-id": "collection",
            "object-key": "ob321",
            "object-hash": "absds",
            "raw-content-length": 65,
            "version-number": 1,
            "version-timestamp": "2018-11-06T16:57:53.981Z"
        }
        s3_mock.side_effect = ClientError({'ResponseMetadata': {},
                                           'Error': {
                                               'Code': 'OTHER',
                                               'Message': 'This is a mock'}},
                                          "FAKE")
        self.assertIsNone(object_deletion_listener_lambda.delete_object(input_dict, "REMOVE"))
        sqs_mock.assert_called_once()
        self.assertEqual(1, s3_mock.call_count)

    # - test_delete_object: ClientError on s3 deletion
    @patch('lng_aws_clients.s3.get_client')
    def test_delete_object_s3_exception(self, s3_mock):
        input_dict = {
            "bucket-name": "bucket",
            "composite-key": "321|collection",
            "object-id": "321",
            "collection-id": "collection",
            "object-key": "ob321",
            "object-hash": "abcd",
            "raw-content-length": 65,
            "version-number": 1,
            "version-timestamp": "2018-11-06T16:57:53.981Z"
        }
        s3_mock.side_effect = Exception
        with self.assertRaisesRegex(InternalError, 'Unhandled exception occurred'):
            object_deletion_listener_lambda.delete_object(input_dict, "REMOVE")

        self.assertEqual(1, s3_mock.call_count)


if __name__ == '__main__':
    unittest.main()
