import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyName, UNSUPPORTED_MEDIA_TYPE
from lng_datalake_constants import collection_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
import update_object_multipart_upload_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'UpdateObjectMultipartUploadCommand')


class TestUpdateObjectMultipartUploadCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATALAKE_STAGING_BUCKET_NAME': "bucket-name"})
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.collection_blocker_patch = patch(
            "lng_datalake_commons.tracking.tracker._validate_collection").start()
        cls.collection_blocker_patch.return_value = None
        cls.maxDiff = None
        reload(update_object_multipart_upload_command)
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # - test_command_schema_validation: Fail
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_command_schema_validation_fail(self, tt_put_mock):
        tt_put_mock.return_value = None
        request_input = io_utils.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                update_object_multipart_upload_command.lambda_handler(request_input, MockLambdaContext())

    # Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('service_commons.object_command.get_previous_and_current_events')
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_lambda_handler_success(self, ct_get_item_mock, owner_auth_mock,
                                    ost_get_item_mock, est_put_item_mock, tracking_table_mock,
                                    s3_client_mock,
                                    get_pending_events_mock,
                                    mock_get_changeset_item):
        tracking_table_mock.return_value = None
        owner_auth_mock.return_value = {}
        ost_get_item_mock.return_value = \
            {
                'object-id': 'UserProvidedObjectID',
                'collection-id': '274',
                'raw-content-length': 131,
                'bucket-name': 'test-bucket',
                'object-key': '321',
                'version-number': 2,
                'object-state': 'Created',
                'content-type': 'text/plain'
            }
        est_put_item_mock.return_value = False
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1
            }
        mock_get_changeset_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }

        s3_client_mock.return_value.create_multipart_upload.return_value = io_utils.load_data_json(
            'multipart_resp.json')
        get_pending_events_mock.return_value = ({}, {})

        request_input = io_utils.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_utils.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(
                update_object_multipart_upload_command.lambda_handler(request_input, MockLambdaContext()),
                response_output)

    # Successful test using lambda_handler with command_wrapper decorator (object-metadata in request)
    @patch('service_commons.object_command.get_previous_and_current_events')
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_lambda_handler_success2(self, ct_get_item_mock, owner_auth_mock,
                                     ost_get_item_mock, est_put_item_mock, tracking_table_mock,
                                     s3_client_mock,
                                     get_pending_events_mock):
        tracking_table_mock.return_value = None
        owner_auth_mock.return_value = {}
        ost_get_item_mock.return_value = \
            {
                'object-id': 'UserProvidedObjectID',
                'collection-id': '274',
                'raw-content-length': 131,
                'bucket-name': 'test-bucket',
                'object-key': '321',
                'version-number': 2,
                'object-state': 'Created',
                'content-type': 'text/plain'
            }
        est_put_item_mock.return_value = False
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1
            }

        s3_client_mock.return_value.create_multipart_upload.return_value = io_utils.load_data_json(
            'multipart_resp.json')
        get_pending_events_mock.return_value = ({}, {})

        request_input = io_utils.load_data_json('apigateway.request.accepted_2.json')
        response_output = io_utils.load_data_json('apigateway.response.accepted_2.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(
                update_object_multipart_upload_command.lambda_handler(request_input, MockLambdaContext()),
                response_output)

    # - test_test_generate_response_json: Missing required key
    def test_generate_response_json_no_collection_id(self):
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InternalError, "Missing required property: collection-id"):
                update_object_multipart_upload_command.generate_response_json({}, 'testObject', 'testRequestId',
                                                                              'testCollection', {}, "STAGE", {})

    # - test_invalid_content_type: invalid content-type
    def test_invalid_content_type(self):
        with self.assertRaisesRegex(UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"):
            update_object_multipart_upload_command.update_object_multipart_upload_command(
                {"collection-id": "foo", "content-type": ""}, "123-zxy", "LATEST", "testApiKeyId")


if __name__ == '__main__':
    unittest.main()
