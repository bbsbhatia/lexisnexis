import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, UnhandledException, NoSuchChangeset
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from close_changeset_command import lambda_handler, validate_and_get_changeset_id, generate_event_store_item

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'CloseChangesetCommand')


class TestCloseeChangesetCommand(unittest.TestCase):
    session_patch = None

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + test_close_changeset_command.lambda_handler: success
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_close_changeset_command_lambda_handler(self, tt_put_mock, mock_ct_get, mock_authorize, event_put_mock):
        event_put_mock.return_value = None
        tt_put_mock.return_value = None
        mock_ct_get.return_value = io_utils.load_data_json('dynamodb.changeset_data_valid.json')
        mock_authorize.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(
                lambda_handler(io_utils.load_data_json('apigateway.request.accepted_1.json'), MockLambdaContext()),
                io_utils.load_data_json('apigateway.response.accepted_1.json'))

    # + test_close_changeset_command.lambda_handler: schema failure
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_close_changeset_command_lambda_handler_failure(self, tt_put_mock, mock_ct_get, mock_authorize):
        tt_put_mock.return_value = None
        mock_ct_get.return_value = io_utils.load_data_json('dynamodb.changeset_data_invalid.json')
        mock_authorize.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InternalError, 'changeset-id'):
                lambda_handler(io_utils.load_data_json('apigateway.request.accepted_1.json'), MockLambdaContext())

    # + test_validate_and_get_changeset_id: success
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id(self, mock_ct_get):
        mock_ct_get.return_value = io_utils.load_data_json('dynamodb.changeset_data_valid.json')
        self.assertDictEqual(validate_and_get_changeset_id('foo'),
                             io_utils.load_data_json('dynamodb.changeset_data_valid.json'))

    # - test_validate_and_get_changeset_id: Client Error
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_client_error(self, mock_ct_get):
        mock_ct_get.side_effect = ClientError({'ResponseMetadata': {},
                                               'Error': {
                                                   'Code': 'OTHER',
                                                   'Message': 'This is a mock'}},
                                              "FAKE")
        with self.assertRaisesRegex(InternalError, 'Unable to get item from Changeset Table'):
            validate_and_get_changeset_id('foo')

    # - test_validate_and_get_changeset_id: General Exception
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_exception(self, mock_ct_get):
        mock_ct_get.side_effect = Exception('mock exception')
        with self.assertRaisesRegex(UnhandledException, 'mock exception'):
            validate_and_get_changeset_id('foo')

    # - test_validate_and_get_changeset_id: no such changeset
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_no_such(self, mock_ct_get):
        mock_ct_get.return_value = {}
        with self.assertRaisesRegex(NoSuchChangeset, 'Invalid Changeset ID'):
            validate_and_get_changeset_id('foo')

    # - test_validate_and_get_changeset_id: no such changeset - Expired
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_and_get_changeset_id_expired(self, mock_ct_get):
        mock_ct_get.return_value = io_utils.load_data_json('dynamodb.changeset_data_expired.json')
        with self.assertRaisesRegex(NoSuchChangeset, 'Invalid Changeset ID'):
            validate_and_get_changeset_id('foo')

    # + test_generate_event_store_item: success
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_event_store_item_success(self, mock_command_wrapper):
        mock_command_wrapper.return_value = "2019-08-31T05:42:12.444Z"
        changeset_item = {'changeset-id': 'test-changeset-id'}
        expected_response = {
            'changeset-id': "test-changeset-id",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'event-name': "Changeset::Close",
            'event-version': 1,
            'stage': 'v1'
        }
        self.assertDictEqual(expected_response, generate_event_store_item(changeset_item, '123', 'v1', ))


if __name__ == '__main__':
    unittest.main()
