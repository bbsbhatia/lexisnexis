import json
import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_event_handler as object_common_module
from remove_object_event_handler import lambda_handler, remove_object_event_handler, \
    generate_object_store_item

__author__ = "John Morelock, Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "RemoveObjectEventHandler")


class RemoveObjectEventHandlerTest(TestCase):
    session_patch = None
    tracker_oldest_item_patch = None
    tracker_is_oldest_patch = None
    tracker_delete_patch = None

    @classmethod
    #TODO: Do we need this envionment variable? What about DATA_LAKE_URL if we test publish?
    @patch.dict(os.environ, {
        'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'})
    def setUpClass(cls):  # NOSONAR
        # reload the remove_object_event_handler so that when it is instantiated it gets my arn is picked up
        # from the os.environ
        #reload(remove_object_event_handler)
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.tracker_oldest_item_patch = patch(
            "lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id").start()
        cls.tracker_is_oldest_patch = patch("lng_datalake_commons.tracking.tracker._is_oldest_event").start()
        cls.tracker_delete_patch = patch("lng_datalake_commons.tracking.tracker._delete_tracking_item").start()

        cls.tracker_oldest_item_patch.return_value = {'event-id': 1}
        cls.tracker_is_oldest_patch.return_value = True
        # reload module to update globals that are set from environment variables
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        cls.tracker_oldest_item_patch.stop()
        cls.tracker_is_oldest_patch.stop()
        cls.tracker_delete_patch.stop()

    # +remove_object_event_handler
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.put_item')
    @patch('service_commons.object_event_handler.get_consistent_object_item')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id')
    @patch('lng_datalake_commons.validate.is_valid_event_stage')
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_remove_object_event_handler_pass(self,
                                              ost_update_item_mock, osvt_put_item_mock,
                                              mock_is_valid_event_message,
                                              mock_is_valid_event_version,
                                              mock_is_valid_event_stage,
                                              mock_get_catalog_ids_by_collection_id,
                                              mock_send_object_notifications,
                                              mock_get_object_data,
                                              mock_put_changeset_object_item):
        mock_put_changeset_object_item.return_value = None
        mock_is_valid_event_message.return_value = True
        mock_is_valid_event_version.return_value = True
        mock_is_valid_event_stage.return_value = True
        mock_get_catalog_ids_by_collection_id.return_value = ['catalog-01']
        mock_send_object_notifications.return_value = None
        mock_get_object_data.return_value = io_utils.load_data_json("valid_dynamo_object_data.json")
        osvt_put_item_mock.return_value = None
        ost_update_item_mock.return_value = None

        event = io_utils.load_data_json("valid_cmd_event_with_changeset.json")
        with patch('remove_object_event_handler.target_arn', 'mock_target_arn'):
            self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
            mock_send_object_notifications.assert_called_once()
            mock_put_changeset_object_item.assert_called_once()

    # +remove_object_event_handler: no_object_changes
    @patch('service_commons.object_event_handler.get_consistent_object_item')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id')
    @patch('lng_datalake_commons.validate.is_valid_event_stage')
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_remove_object_event_handler_no_object_changes(self,
                                                           ost_update_item_mock, osvt_put_item_mock,
                                                           mock_is_valid_event_message,
                                                           mock_is_valid_event_version,
                                                           mock_is_valid_event_stage,
                                                           mock_get_catalog_ids_by_collection_id,
                                                           mock_send_object_notifications,
                                                           mock_get_object_data):
        mock_is_valid_event_message.return_value = True
        mock_is_valid_event_version.return_value = True
        mock_is_valid_event_stage.return_value = True
        mock_get_catalog_ids_by_collection_id.return_value = ['catalog-01']
        mock_get_object_data.return_value = io_utils.load_data_json("valid_dynamo_object_data.json")
        osvt_put_item_mock.return_value = None
        ost_update_item_mock.side_effect = ConditionError()

        event = io_utils.load_data_json("valid_cmd_event.json")
        with self.assertRaisesRegex(TerminalErrorException, "ConditionError inserting to OST"):
            remove_object_event_handler(event,'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')
        mock_send_object_notifications.assert_not_called()

    # +remove_object_event_handler: no_object_item - no-op - just return success
    @patch('service_commons.object_event_handler.get_consistent_object_item')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id')
    @patch('lng_datalake_commons.validate.is_valid_event_stage')
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_remove_object_event_handler_no_object_item(self,
                                                        ost_update_item_mock, osvt_put_item_mock,
                                                        mock_is_valid_event_message,
                                                        mock_is_valid_event_version,
                                                        mock_is_valid_event_stage,
                                                        mock_get_catalog_ids_by_collection_id,
                                                        mock_send_object_notifications,
                                                        mock_get_object_data):
        mock_is_valid_event_message.return_value = True
        mock_is_valid_event_version.return_value = True
        mock_is_valid_event_stage.return_value = True
        mock_get_catalog_ids_by_collection_id.return_value = ['catalog-01']
        mock_send_object_notifications.return_value = None
        object_item = io_utils.load_data_json("valid_dynamo_object_data.json")
        object_item['object-state'] = "Removed"
        mock_get_object_data.return_value = object_item
        osvt_put_item_mock.return_value = None
        ost_update_item_mock.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        self.assertEqual(
            remove_object_event_handler(
                event,
                'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'),
            event_handler_status.SUCCESS)
        mock_send_object_notifications.assert_not_called()
        ost_update_item_mock.assert_not_called()
        osvt_put_item_mock.assert_not_called()

    # +remove_object_event_handler: object_removed - no-op - just return success
    @patch('service_commons.object_event_handler.get_consistent_object_item')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id')
    @patch('lng_datalake_commons.validate.is_valid_event_stage')
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.publish_sns_topic.publish_to_topic')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.update_item')
    def test_remove_object_event_handler_object_removed(self,
                                                        ost_update_item_mock, osvt_put_item_mock,
                                                        mock_is_valid_event_message,
                                                        sns_publish_mock,
                                                        mock_is_valid_event_version,
                                                        mock_is_valid_event_stage,
                                                        mock_get_catalog_ids_by_collection_id,
                                                        mock_send_object_notifications,
                                                        mock_get_object_data):
        mock_is_valid_event_message.return_value = True
        sns_publish_mock.return_value = None
        mock_is_valid_event_version.return_value = True
        mock_is_valid_event_stage.return_value = True
        mock_get_catalog_ids_by_collection_id.return_value = ['catalog-01']
        mock_send_object_notifications.return_value = None
        mock_get_object_data.return_value = {}
        osvt_put_item_mock.return_value = None
        ost_update_item_mock.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        self.assertEqual(
            remove_object_event_handler(
                event,
                'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'),
            event_handler_status.SUCCESS)
        mock_send_object_notifications.assert_not_called()
        ost_update_item_mock.assert_not_called()
        osvt_put_item_mock.assert_not_called()

    # + generate_object_store_item
    def test_generate_object_store_item(self):
        input_msg = {'object-id': '193532d0-233b-11e9-87c8-f519dced1f97',
                     'collection-id': 'zero-versions-id-regression-1548639715',
                     'old-object-versions-to-keep': 0,
                     'event-id': '0a1428e2-50a5-11e9-a9e0-f37e41f6c62a', 'stage': 'LATEST',
                     'request-time': '2019-04-05T15:25:22.924Z',
                     'event-name': 'Object::Remove', 'event-version': 1, 'seen-count': 0}
        object_data = io_utils.load_data_json("valid_dynamo_object_data.json")
        object_data['replicated-buckets'] = ["bucket-1", "bucket-2"]
        expected_dict = {
            "object-state": "Removed",
            "collection-hash": "dacecc7341a9d7cc2bbdf50697f2ec39116323e5",
            "collection-id": "zero-versions-id-regression-1548639715",
            "version-number": 2,
            "object-key": "tombstone",
            "version-timestamp": "2019-04-05T15:25:22.924Z",
            "object-id": "193532d0-233b-11e9-87c8-f519dced1f97",
            "content-type": "tombstone",
            'event-ids': ['0a1428e2-50a5-11e9-a9e0-f37e41f6c62a'],
            "raw-content-length": -1,
            "bucket-name": "tombstone",
            "replicated-buckets": ['tombstone']
        }
        self.assertDictEqual(expected_dict, generate_object_store_item(input_msg, object_data))

    # - generate_object_store_item - missing required attribute
    def test_generate_object_store_item_fail(self):
        input_msg = {'object-id': '193532d0-233b-11e9-87c8-f519dced1f97',
                     'collection-id': 'zero-versions-id-regression-1548639715',
                     'old-object-versions-to-keep': 0,
                     'event-id': '0a1428e2-50a5-11e9-a9e0-f37e41f6c62a', 'stage': 'LATEST',
                     'request-time': '2019-04-05T15:25:22.924Z',
                     'event-name': 'Object::Remove', 'event-version': 1, 'seen-count': 0}
        input_msg.pop('request-time')
        self.assertRaisesRegex(TerminalErrorException, 'Missing required property', generate_object_store_item, input_msg,
                               {})

    # +remove_object_event_handler: retry=ost_get(1)
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.update_object_store_table')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_remove_object_event_handler_retry1(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item,
                                                mock_update_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications):
        mock_validate_message.return_value = True
        mock_update_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = io_utils.load_data_json("object_store_item_expected.json")
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 1}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_called_once()
        mock_update_object_store_table.assert_called_once()
        mock_insert_object_store_version_table.assert_called_once()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()

    # +remove_object_event_handler: retry=ost_get(1), no object changes
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.update_object_store_table')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_remove_object_event_handler_retry1_no_object_changes(self, mock_validate_message,
                                                                  mock_get_catalog_ids_by_collection_id,
                                                                  mock_get_consistent_object_item,
                                                                  mock_update_object_store_table,
                                                                  mock_insert_object_store_version_table,
                                                                  mock_delete_object_store_version_table,
                                                                  mock_send_object_notifications):
        mock_validate_message.return_value = True
        mock_update_object_store_table.return_value = True
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = io_utils.load_data_json("object_store_item_expected.json")
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 1}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_called_once()
        mock_update_object_store_table.assert_called_once()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_not_called()
        mock_send_object_notifications.assert_not_called()

    # +remove_object_event_handler: retry=ost_update(2)
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.update_object_store_table')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_remove_object_event_handler_retry2(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item,
                                                mock_update_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications):
        mock_validate_message.return_value = True
        mock_update_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        object_data = io_utils.load_data_json("object_store_item_expected.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 2,
                                            'object-data': object_data,
                                            'no-object-changes': False}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_update_object_store_table.assert_called_once()
        mock_insert_object_store_version_table.assert_called_once()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()

    # +remove_object_event_handler: retry=osvt_put(3)
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.update_object_store_table')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_remove_object_event_handler_retry3(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item,
                                                mock_update_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications):
        mock_validate_message.return_value = True
        mock_update_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        object_data = io_utils.load_data_json("object_store_item_expected.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 3,
                                            'object-data': object_data,
                                            'no-object-changes': False}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_update_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_called_once()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()

    # +remove_object_event_handler: retry=osvt_delete(4)
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.update_object_store_table')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_remove_object_event_handler_retry4(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item,
                                                mock_update_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications):
        mock_validate_message.return_value = True
        mock_update_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        object_data = io_utils.load_data_json("object_store_item_expected.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 4,
                                            'object-data': object_data,
                                            'no-object-changes': False}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_update_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()

    # +remove_object_event_handler: retry=sns_publish(5)
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.update_object_store_table')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_remove_object_event_handler_retry5(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item,
                                                mock_update_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications):
        mock_validate_message.return_value = True
        mock_update_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        object_data = io_utils.load_data_json("object_store_item_expected.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 5,
                                            'object-data': object_data,
                                            'no-object-changes': False,
                                            'sns-publish-data': {'notifications': {}}}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_update_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_not_called()
        mock_send_object_notifications.assert_called_once()

    # +remove_object_event_handler: retry=done(99)
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.update_object_store_table')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_remove_object_event_handler_retry99(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                 mock_get_consistent_object_item,
                                                 mock_update_object_store_table, mock_insert_object_store_version_table,
                                                 mock_delete_object_store_version_table,
                                                 mock_send_object_notifications):
        mock_validate_message.return_value = True
        mock_update_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        object_data = io_utils.load_data_json("object_store_item_expected.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 99,
                                            'object-data': object_data,
                                            'no-object-changes': False}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_update_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_not_called()
        mock_send_object_notifications.assert_not_called()


if __name__ == '__main__':
    unittest.main()
