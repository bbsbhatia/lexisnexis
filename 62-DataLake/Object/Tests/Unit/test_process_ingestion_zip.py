import json
import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import process_ingestion_zip

__author__ = "Prashant S, John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "ProcessIngestionZip")


class ZipInfo:
    def __init__(self, filename, compress_type, flag_bits, header_offset, file_size):
        self.filename = filename
        self.compress_type = compress_type
        self.flag_bits = flag_bits
        self.header_offset = header_offset
        self.file_size = file_size


class ProcessIngestionZip(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'INGESTION_FANOUT_NOTIFICATION_TOPIC_ARN': "topic-arn"})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(process_ingestion_zip)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + lambda_handler
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    @patch('process_ingestion_zip.publish_sns_messages')
    @patch('process_ingestion_zip.validate_files')
    @patch('process_ingestion_zip.read_zip_metadata')
    @patch('service_commons.ingestion.read_zip_central_directory')
    @patch('service_commons.ingestion.validate_zip_magic_number')
    @patch('service_commons.ingestion.update_ingestion_state')
    @patch('process_ingestion_zip.check_for_collection_blocker')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip(self, mock_get_ingestion_data, mock_check_for_collection_blocker,
                                   mock_update_ingestion_state, mock_validate_zip_magic_number,
                                   mock_read_zip_central_directory,
                                   mock_read_zip_metadata, mock_validate_files, mock_publish_sns_messages,
                                   mock_ingestion_table_update_item):
        mock_get_ingestion_data.return_value = io_utils.load_data_json(
            'ingestion.get_ingestion_data_response_valid.json')
        mock_check_for_collection_blocker.return_value = 0
        mock_update_ingestion_state.return_value = None
        mock_validate_zip_magic_number.return_value = None

        class ZipInfo:
            def __init__(self, filename):
                self.filename = filename

        mock_read_zip_central_directory.return_value = [ZipInfo("mock_1.xml"), ZipInfo("mock_2.xml")], 458, 0
        mock_read_zip_metadata.return_value = 'binary/octet-stream', 1, 0, "abcdefg", 'jek-changeset', 1234567890
        mock_validate_files.return_value = None
        mock_publish_sns_messages.return_value = None
        mock_ingestion_table_update_item.return_value = None

        input_message = io_utils.load_data_json("input_message_valid.json")
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = json.dumps(input_message)
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = json.dumps(input_message_body)
        self.assertEqual(process_ingestion_zip.lambda_handler(input_event, MockLambdaContext()),
                         event_handler_status.SUCCESS)

    # - lambda_handler: not oldest collection blocker - puts item back on SQS and returns success
    @patch('lng_aws_clients.sqs.get_client')
    @patch('process_ingestion_zip.check_for_collection_blocker')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_retry(self, mock_get_ingestion_data,
                                         mock_check_for_collection_blocker, mock_sqs_client):
        mock_get_ingestion_data.return_value = io_utils.load_data_json(
            'ingestion.get_ingestion_data_response_valid.json')
        mock_check_for_collection_blocker.return_value = 1
        mock_sqs_client.return_value.send_message.return_value = None

        input_message = io_utils.load_data_json("input_message_valid.json")
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = json.dumps(input_message)
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = json.dumps(input_message_body)
        self.assertEqual(process_ingestion_zip.lambda_handler(input_event, MockLambdaContext()),
                         None)
        mock_sqs_client.return_value.send_message.assert_called_once()

    # - lambda_handler: not oldest collection blocker and seen-count=1000
    #   - doesn't put item back on SQS, calls error_handler.terminal_error() and returns success
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('process_ingestion_zip.check_for_collection_blocker')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_terminal(self, mock_get_ingestion_data,
                                            mock_check_for_collection_blocker, mock_sqs_client,
                                            mock_error_handler_terminal_error):
        mock_get_ingestion_data.return_value = io_utils.load_data_json(
            'ingestion.get_ingestion_data_response_valid.json')
        mock_check_for_collection_blocker.return_value = 1
        mock_sqs_client.return_value.send_message.return_value = None
        mock_error_handler_terminal_error.return_value = None

        input_message = io_utils.load_data_json("input_message_valid.json")
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = json.dumps(input_message)
        input_event = io_utils.load_data_json("input_event_with_seen_count1000.json")
        input_event["Records"][0]["body"] = json.dumps(input_message_body)
        self.assertEqual(process_ingestion_zip.lambda_handler(input_event, MockLambdaContext()),
                         None)
        mock_sqs_client.return_value.send_message.assert_not_called()
        mock_error_handler_terminal_error.assert_called_once()

    # - missing S3 attributes - Retryable Exception
    @patch('service_commons.ingestion.get_message')
    def test_process_ingestion_zip_key_error(self, mock_get_message):
        mock_get_message.side_effect = RetryEventException('TEST RetryEventException')

        input_message = io_utils.load_data_json("input_message_valid.json")
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = json.dumps(input_message)
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = json.dumps(input_message_body)
        mock_context = MockLambdaContext()

        with self.assertRaisesRegex(RetryEventException, 'TEST RetryEventException'):
            process_ingestion_zip.process_ingestion_zip(input_event, mock_context)

    # - missing S3 attributes
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('service_commons.ingestion.get_message')
    def test_process_ingestion_zip_key_error(self, mock_get_message, mock_error_handler_terminal_error):
        message = io_utils.load_data_json('ingestion.get_message_response_valid.json')
        message["Records"][0]["s3"]["object"].pop("key")
        mock_get_message.return_value = message
        mock_error_handler_terminal_error.return_value = None

        input_message = io_utils.load_data_json("input_message_valid.json")
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = input_message
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = input_message_body
        error_message = 'TerminalErrorException||Error getting s3 attributes from message||Missing attribute:key'
        mock_context = MockLambdaContext()

        process_ingestion_zip.process_ingestion_zip(input_event, mock_context)
        mock_error_handler_terminal_error.assert_called_with(input_event, is_event=False, uid=None,
                                                             error_message=error_message, context=mock_context)

    # - S3 key doesn't include stage and ingestion-id
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('service_commons.ingestion.get_message')
    def test_process_ingestion_zip_missing_ingestion_id(self, mock_get_message, mock_error_handler_terminal_error):
        message = io_utils.load_data_json('ingestion.get_message_response_valid.json')
        message["Records"][0]["s3"]["object"]["key"] = "ingestion/LATEST"
        mock_get_message.return_value = message
        mock_error_handler_terminal_error.return_value = None

        input_message = io_utils.load_data_json("input_message_valid.json")
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = input_message
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = input_message_body
        error_message = 'TerminalErrorException||Cannot determine stage and Ingestion ID from key: ingestion/LATEST'
        mock_context = MockLambdaContext()

        process_ingestion_zip.process_ingestion_zip(input_event, mock_context)
        mock_error_handler_terminal_error.assert_called_with(input_event, is_event=False, uid=None,
                                                             error_message=error_message, context=mock_context)

    # - invalid ingestion state
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('service_commons.ingestion.get_ingestion_data')
    @patch('service_commons.ingestion.get_message')
    def test_process_ingestion_zip_invalid_ingestion_state(self, mock_get_message,
                                                           mock_get_ingestion_data,
                                                           mock_error_handler_terminal_error):
        mock_get_message.return_value = io_utils.load_data_json('ingestion.get_message_response_valid.json')
        ingestion_data = io_utils.load_data_json('ingestion.get_ingestion_data_response_valid.json')
        ingestion_data["ingestion-state"] = "Cancelled"
        mock_get_ingestion_data.return_value = ingestion_data
        mock_error_handler_terminal_error.return_value = None

        input_message = io_utils.load_data_json("input_message_valid.json")
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = input_message
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = input_message_body
        error_message = "TerminalErrorException||Ingestion ID 1524439.zip state is not in ('Pending', 'Validating', 'Processing', 'Error') (state=Cancelled)"
        mock_context = MockLambdaContext()

        process_ingestion_zip.process_ingestion_zip(input_event, mock_context)
        mock_error_handler_terminal_error.assert_called_with(input_event, is_event=False, uid="1524439.zip",
                                                             error_message=error_message, context=mock_context)

    # - terminal error returned from check_for_collection_blocker()
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    @patch('process_ingestion_zip.check_for_collection_blocker')
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('service_commons.ingestion.get_ingestion_data')
    @patch('service_commons.ingestion.get_message')
    def test_process_ingestion_zip_terminal_error(self, mock_get_message,
                                                  mock_get_ingestion_data,
                                                  mock_error_handler_terminal_error,
                                                  mock_check_for_collection_blocker,
                                                  mock_ingestion_table_update_item):
        mock_get_message.return_value = io_utils.load_data_json('ingestion.get_message_response_valid.json')
        ingestion_data = io_utils.load_data_json('ingestion.get_ingestion_data_response_valid.json')
        ingestion_data["ingestion-state"] = "Pending"
        mock_get_ingestion_data.return_value = ingestion_data
        mock_error_handler_terminal_error.return_value = None
        mock_check_for_collection_blocker.side_effect = TerminalErrorException("Some error")
        mock_ingestion_table_update_item.return_value = None

        input_message = io_utils.load_data_json("input_message_valid.json")
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = input_message
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = input_message_body
        error_message = "TerminalErrorException||Some error"
        mock_context = MockLambdaContext()

        process_ingestion_zip.process_ingestion_zip(input_event, mock_context)
        mock_error_handler_terminal_error.assert_called_with(input_event, is_event=False, uid="1524439.zip",
                                                             error_message=error_message, context=mock_context)
        ingestion_data['ingestion-state'] = "Error"
        ingestion_data['error-description'] = "TerminalErrorException||Some error"
        mock_ingestion_table_update_item.assert_called_with(ingestion_data)

    # + check_for_collection_blocker
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_for_collection_blocker(self, mock_collection_blocker_table_query_items):
        mock_collection_blocker_table_query_items.return_value = [{
            "collection-id": "151",
            "pending-expiration-epoch": 1554914221,
            "request-id": "1524439.zip",
            "request-time": "2019-03-11T16:37:01.874Z"
        }]

        expected_response = 0
        self.assertEqual(expected_response, process_ingestion_zip.check_for_collection_blocker("151", "1524439.zip"))

    # - check_for_collection_blocker - EndpointConnectionError
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_for_collection_blocker_endpoint_connection_error_exception(self,
                                                                              mock_collection_blocker_table_query_items):
        mock_collection_blocker_table_query_items.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')

        with self.assertRaisesRegex(RetryEventException, "Unable to query items in CollectionBlocker Table"):
            process_ingestion_zip.check_for_collection_blocker("151", "1524439.zip")

    # - check_for_collection_blocker - ClientError
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_for_collection_blocker_client_error(self, mock_collection_blocker_table_query_items):
        mock_collection_blocker_table_query_items.side_effect = ClientError({"ResponseMetadata": {},
                                                                             "Error": {"Code": "mock error code",
                                                                                       "Message": "mock error message"}},
                                                                            "client-error-mock")

        with self.assertRaisesRegex(RetryEventException, "Unable to query items in CollectionBlocker Table"):
            process_ingestion_zip.check_for_collection_blocker("151", "1524439.zip")

    # - check_for_collection_blocker - Exception
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_for_collection_blocker_exception(self, mock_collection_blocker_table_query_items):
        mock_collection_blocker_table_query_items.side_effect = Exception

        with self.assertRaisesRegex(TerminalErrorException, "Failed to query items in CollectionBlocker Table"):
            process_ingestion_zip.check_for_collection_blocker("151", "1524439.zip")

    # - check_for_collection_blocker - collection blocker not found
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_for_collection_empty_response(self, mock_collection_blocker_table_query_items):
        mock_collection_blocker_table_query_items.return_value = []
        with self.assertRaisesRegex(TerminalErrorException, "Collection blocker not found for Collection ID"):
            process_ingestion_zip.check_for_collection_blocker("151", "1524439.zip")

    # - check_for_collection_blocker - collection blocker not oldest
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_for_collection_not_oldest_request(self, mock_collection_blocker_table_query_items):
        mock_collection_blocker_table_query_items.return_value = [{
            "collection-id": "151",
            "pending-expiration-epoch": 1554914221,
            "request-id": "1524440.zip",
            "request-time": "2019-03-11T16:37:01.874Z"
        },
            {
                "collection-id": "151",
                "pending-expiration-epoch": 1554915221,
                "request-id": "1524439.zip",
                "request-time": "2019-03-11T17:37:01.874Z"
            }]
        self.assertEquals(process_ingestion_zip.check_for_collection_blocker("151", "1524439.zip"), 1)

    # + read_zip_metadata
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_metadata_success(self, mock_s3_client):
        mock_s3_client.return_value.head_object.return_value = io_utils.load_data_json(
            "s3_head_object_response_valid.json")
        expected_response = "binary/octet-stream", 1, 0, "abcdefg", 'changeset-test', 123456789
        self.assertEqual(expected_response,
                         process_ingestion_zip.read_zip_metadata("feature-jek-dl-object-staging-288044017584-use1",
                                                                 "ingestion/v1/dlyEdxOsZgZUEfSU"))

    # - read_zip_metadata - EndpointConnectionError
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_metadata_endpoint_connection_error_exception(self, mock_s3_client):
        mock_s3_client.return_value.head_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(RetryEventException, "Unable to head S3 object xyz.zip from bucket s3_bucket"):
            process_ingestion_zip.read_zip_metadata("s3_bucket", "xyz.zip")

    # - read_zip_metadata - ClientError
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_metadata_client_error(self, mock_s3_client):
        mock_s3_client.return_value.head_object.side_effect = ClientError({"ResponseMetadata": {},
                                                                           "Error": {"Code": "mock error code",
                                                                                     "Message": "mock error message"}},
                                                                          "client-error-mock")
        with self.assertRaisesRegex(RetryEventException, "Unable to head S3 object xyz.zip from bucket s3_bucket"):
            process_ingestion_zip.read_zip_metadata("s3_bucket", "xyz.zip")

    # - read_zip_metadata - empty
    @patch('lng_aws_clients.s3.get_client')
    def test_read_zip_metadata_key_error(self, mock_s3_client):
        mock_s3_client.return_value.head_object.return_value = {}
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Error getting S3 metadata from xyz.zip||Missing key: ContentType"):
            process_ingestion_zip.read_zip_metadata("s3_bucket", "xyz.zip")

    # + validate_files
    @patch('service_commons.object_common.validate_object_id')
    def test_validate_files(self, mock_validate_object_id2):
        mock_validate_object_id2.return_value = ""
        zip_list = [ZipInfo("mock_1.xml", 0, 0, 0, 1000), ZipInfo("mock_2.xml", 8, 0, 0, 1000)]
        self.assertIsNone(process_ingestion_zip.validate_files(zip_list, 458, 0, "xyz.zip"))

    # - validate_files - archive contains folders
    def test_validate_files_error_contains_folder(self):
        zip_list = [ZipInfo("abc/mock_1.xml", 0, 0, 0, 1000), ZipInfo("abc/mock_2.xml", 8, 0, 0, 1000)]
        with self.assertRaisesRegex(TerminalErrorException, "Validation error for Ingestion ID xyz.zip\|\|"
                                                            "Archive cannot contain folders"):
            process_ingestion_zip.validate_files(zip_list, 458, 0, "xyz.zip")

    # - validate_files - invalid file name
    @patch('service_commons.object_common.validate_object_id')
    def test_validate_error_invalid_object_char(self, mock_validate_object_id2):
        mock_validate_object_id2.return_value = "Object ID contains invalid characters"
        zip_list = [ZipInfo("mock#1.xml", 0, 0, 0, 1000), ZipInfo("mock_2.xml", 8, 0, 0, 1000)]
        with self.assertRaisesRegex(TerminalErrorException, "Validation error for Ingestion ID xyz.zip\|\|"
                                                            "Unsupported file name \(Object ID\) detected"):
            process_ingestion_zip.validate_files(zip_list, 458, 0, "xyz.zip")

    # - validate_files - invalid compression type
    @patch('service_commons.object_common.validate_object_id')
    def test_validate_error_invalid_compress_type(self, mock_validate_object_id2):
        mock_validate_object_id2.return_value = ""
        zip_list = [ZipInfo("mock_1.xml", 12, 0, 0, 1000), ZipInfo("mock_2.xml", 8, 0, 0, 1000)]
        with self.assertRaisesRegex(TerminalErrorException, "Validation error for Ingestion ID xyz.zip\|\|"
                                                            "Unsupported compression type 12 detected on file: mock_1.xml"):
            process_ingestion_zip.validate_files(zip_list, 458, 0, "xyz.zip")

    # - validate_files - archive contains encrypted file
    @patch('service_commons.object_common.validate_object_id')
    def test_validate_error_invalid_encrypted_file(self, mock_validate_object_id2):
        mock_validate_object_id2.return_value = ""
        zip_list = [ZipInfo("mock_1.xml", 0, 0x01, 0, 1000), ZipInfo("mock_2.xml", 8, 0, 0, 1000)]
        with self.assertRaisesRegex(TerminalErrorException, "Validation error for Ingestion ID xyz.zip\|\|"
                                                            "Encrypted file detected: mock_1.xml"):
            process_ingestion_zip.validate_files(zip_list, 458, 0, "xyz.zip")

    # - validate_files - header offset out of range
    @patch('service_commons.object_common.validate_object_id')
    def test_validate_error_invalid_header_offset_out_of_range(self, mock_validate_object_id2):
        mock_validate_object_id2.return_value = ""
        zip_list = [ZipInfo("mock_1.xml", 0, 0, 500, 1000), ZipInfo("mock_2.xml", 8, 0, 0, 1000)]
        with self.assertRaisesRegex(TerminalErrorException, "Validation error for Ingestion ID xyz.zip\|\|"
                                                            "Header offset out of range on file: mock_1.xml"):
            process_ingestion_zip.validate_files(zip_list, 458, 0, "xyz.zip")

    # - validate_files - invalid file size
    @patch('service_commons.object_common.validate_object_id')
    def test_validate_error_invalid_file_size(self, mock_validate_object_id2):
        mock_validate_object_id2.return_value = ""
        zip_list = [ZipInfo("mock_1.xml", 0, 0, 0, 1000), ZipInfo("mock_2.xml", 8, 0, 0, 700 * 2 ** 20 + 1)]
        with self.assertRaisesRegex(TerminalErrorException, "Validation error for Ingestion ID xyz.zip\|\|"
                                                            "File size {0} > max allowed size".format(
            700 * 2 ** 20 + 1)):
            process_ingestion_zip.validate_files(zip_list, 458, 0, "xyz.zip")

    # - validate_files - duplilcated file name
    @patch('service_commons.object_common.validate_object_id')
    def test_validate_error_duplicates(self, mock_validate_object_id2):
        mock_validate_object_id2.return_value = ""
        zip_list = [ZipInfo("mock_1.xml", 0, 0, 0, 1000), ZipInfo("mock_1.xml", 8, 0, 0, 1000)]
        with self.assertRaisesRegex(TerminalErrorException, "Validation error for Ingestion ID xyz.zip\|\|"
                                                            "Duplicate file names \(Object IDs\) detected"):
            process_ingestion_zip.validate_files(zip_list, 458, 0, "xyz.zip")

    # + publish_sns_messages valid event
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_sns_messages_success(self, mock_sns_client):
        ingestion_id = 'ingestion-id'
        zip_list = []
        zip_list.extend(range(2000))
        message = {'file-index': 123, 'file-count': 2000}
        stage = 'STAGE'
        mock_sns_client.return_value.publish.return_value = {'MessageId': "123456789"}
        self.assertIsNone(process_ingestion_zip.publish_sns_messages(ingestion_id, zip_list, message, stage, []))
        self.assertEqual(mock_sns_client.return_value.publish.call_count,
                         len(zip_list) // process_ingestion_zip.MAX_FILES_PER_PART)

    # - publish_sns_messages - Retry Exception
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_sns_messages_exception(self, mock_sns_client):
        mock_sns_client.return_value.publish.side_effect = [{'MessageId': "123456789"},
                                                            Exception,
                                                            {'MessageId': "123456789"},
                                                            Exception]

        ingestion_id = 'ingestion-id'
        zip_list = []
        zip_list.extend(range(400))
        stage = 'STAGE'

        message = {'file-index': 123, 'file-count': 2000}
        with self.assertRaisesRegex(RetryEventException, "Error publishing 2 messages for indexes \[101, 301\]"):
            process_ingestion_zip.publish_sns_messages(ingestion_id, zip_list, message, stage, [])
        self.assertIn('retry-indexes', error_handler.additional_attributes)
        self.assertEqual(error_handler.additional_attributes['retry-indexes'], [101, 301])

    # + successful process_retry_exception: no seen-count in messageAttributes
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_success1(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None

        self.assertIsNone(process_ingestion_zip.process_retry_exception(RetryEventException(""),
                                                                        io_utils.load_data_json('input_event.json'), 0))

    # + successful process_retry_exception: with seen-count in messageAttributes
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_success2(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None

        self.assertIsNone(process_ingestion_zip.process_retry_exception(RetryEventException(""),
                                                                        io_utils.load_data_json(
                                                                            'input_event_with_seen_count.json'), 0))

    # - fail process_retry_exception: with seen-count=1000 in messageAttributes
    def test_process_retry_exception_fail1(self):
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Event exceeded max retry count of 1000\|\|RetryEventException\|\|some issue'):
            process_ingestion_zip.process_retry_exception(RetryEventException("some issue"),
                                                          io_utils.load_data_json(
                                                              'input_event_with_seen_count1000.json'), 0)

    # - fail process_retry_exception: sqs send_message ClientError raises original RetryEventException
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_fail2(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.side_effect = ClientError({"Error": {"Code": 101,
                                                                                       "Message": "Mock Client Error"}},
                                                                            "MockError")
        with self.assertRaisesRegex(RetryEventException, "some issue"):
            process_ingestion_zip.process_retry_exception(RetryEventException("some issue"),
                                                          io_utils.load_data_json('input_event.json'), 0)

    # + successful process_terminal_error
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_process_terminal_error_success(self, mock_error_handler_terminal_error):
        mock_error_handler_terminal_error.return_value = None

        self.assertIsNone(process_ingestion_zip.process_terminal_error(RetryEventException(""),
                                                                       io_utils.load_data_json('input_event.json'),
                                                                       MockLambdaContext(),
                                                                       {}))
        mock_error_handler_terminal_error.assert_called_once()

    # + retry=sns_publish retry-indexes
    @patch('process_ingestion_zip.publish_sns_message')
    @patch('lng_aws_clients.sns.get_client')
    @patch('process_ingestion_zip.validate_files')
    @patch('process_ingestion_zip.read_zip_metadata')
    @patch('service_commons.ingestion.read_zip_central_directory')
    @patch('service_commons.ingestion.validate_zip_magic_number')
    @patch('service_commons.ingestion.update_ingestion_state')
    @patch('process_ingestion_zip.check_for_collection_blocker')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_retry_indexes1(self, mock_get_ingestion_data, mock_check_for_collection_blocker,
                                                  mock_update_ingestion_state, mock_validate_zip_magic_number,
                                                  mock_read_zip_central_directory,
                                                  mock_read_zip_metadata, mock_validate_files, mock_sns_client, mock_publish_sns_message):
        mock_get_ingestion_data.return_value = io_utils.load_data_json(
            'ingestion.get_ingestion_data_response_valid.json')
        mock_check_for_collection_blocker.return_value = 0
        mock_update_ingestion_state.return_value = None
        mock_validate_zip_magic_number.return_value = None
        mock_sns_client.return_value = None

        class ZipInfo:
            def __init__(self, filename):
                self.filename = filename

        mock_read_zip_central_directory.return_value = [ZipInfo("mock_1.xml"), ZipInfo("mock_2.xml")], 458, 0
        mock_read_zip_metadata.return_value = 'binary/octet-stream', 1, 0, "abcdefg", 'jek-changeset', 1234567890
        mock_validate_files.return_value = None
        mock_publish_sns_message.return_value = -1

        input_message = io_utils.load_data_json("input_message_valid.json")
        # retry index #2 doesn't exist but this tests code that will continue if index is not in retry-indexes
        input_message['additional-attributes'] = {'retry-indexes': [1]}
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = json.dumps(input_message)
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = json.dumps(input_message_body)
        self.assertEqual(process_ingestion_zip.lambda_handler(input_event, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_publish_sns_message.assert_called_once()

    # + retry=sns_publish retry-indexes (nothing to publish)
    @patch('process_ingestion_zip.publish_sns_message')
    @patch('lng_aws_clients.sns.get_client')
    @patch('process_ingestion_zip.validate_files')
    @patch('process_ingestion_zip.read_zip_metadata')
    @patch('service_commons.ingestion.read_zip_central_directory')
    @patch('service_commons.ingestion.validate_zip_magic_number')
    @patch('service_commons.ingestion.update_ingestion_state')
    @patch('process_ingestion_zip.check_for_collection_blocker')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_retry_indexes2(self, mock_get_ingestion_data, mock_check_for_collection_blocker,
                                                  mock_update_ingestion_state, mock_validate_zip_magic_number,
                                                  mock_read_zip_central_directory,
                                                  mock_read_zip_metadata, mock_validate_files, mock_sns_client, mock_publish_sns_message):
        mock_get_ingestion_data.return_value = io_utils.load_data_json(
            'ingestion.get_ingestion_data_response_valid.json')
        mock_check_for_collection_blocker.return_value = 0
        mock_update_ingestion_state.return_value = None
        mock_validate_zip_magic_number.return_value = None
        mock_sns_client.return_value = None

        class ZipInfo:
            def __init__(self, filename):
                self.filename = filename

        mock_read_zip_central_directory.return_value = [ZipInfo("mock_1.xml"), ZipInfo("mock_2.xml")], 458, 0
        mock_read_zip_metadata.return_value = 'binary/octet-stream', 1, 0, "abcdefg", 'jek-changeset', 1234567890
        mock_validate_files.return_value = None
        mock_publish_sns_message.return_value = -1

        input_message = io_utils.load_data_json("input_message_valid.json")
        # retry index #2 doesn't exist but this tests code that will continue if index is not in retry-indexes
        input_message['additional-attributes'] = {'retry-indexes': [2]}
        input_message_body = io_utils.load_data_json("input_message_body.json")
        input_message_body["Message"] = json.dumps(input_message)
        input_event = io_utils.load_data_json("input_event.json")
        input_event["Records"][0]["body"] = json.dumps(input_message_body)
        self.assertEqual(process_ingestion_zip.lambda_handler(input_event, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_publish_sns_message.assert_not_called()


if __name__ == '__main__':
    unittest.main()
