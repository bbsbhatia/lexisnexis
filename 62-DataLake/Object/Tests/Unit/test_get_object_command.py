import io
import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError
from botocore.response import StreamingBody
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NoSuchCollection, SemanticError, NotAllowedError, \
    InvalidRequestPropertyName, NoSuchObjectVersion, NoSuchObject, UnhandledException, InvalidRequestPropertyValue, \
    NoSuchChangeset
from lng_datalake_constants import collection_status, object_status, changeset_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from get_object_command import get_object_command, lambda_handler, is_utf8_content

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetObjectCommand')


class TestGetObjectCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # -Schema validation error using lambda_handler with command_wrapper decorator
    # - request missing object-id required property
    def test_command_decorator_fail(self):
        request_input = io_util.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    # +Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_decorator_success(self, ct_get_item_mock, ost_get_item_mock, s3_get_object_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain"
            }
        s3_get_object_mock.return_value.get_object.return_value = {
            'Body': StreamingBody(io.BytesIO(b"Raw file text"), 13),
            'Metadata': {}
        }
        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_success1(self, ct_get_item_mock, ost_get_item_mock,
                                         s3_get_object_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain"
            }
        s3_get_object_mock.return_value.get_object.return_value = {
            'Body': StreamingBody(io.BytesIO(b"Raw file text"), 13),
            'Metadata': {}
        }
        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }
        expected_response_output = \
            {
                "collection-id": '274',
                "collection-url": "/collections/LATEST/274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "object-url": "/objects/LATEST/49cf62814f629ba0a679147d8784b516cde30a17?collection-id=274",
                "owner-id": 100,
                "asset-id": 62,
                "object-state": object_status.CREATED,
                "raw-content-length": 24,
                "content-type": "text/plain",
                "object-key-url": "https://datalake_url.com/objects/store/49cf62814f629ba0a679147d8784b516cde30a17",
                "object-body": "Raw file text"
            }

        self.assertEqual(get_object_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Test of get object Lambda for large object
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_large_object(self, ct_get_item_mock, ost_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": "274",
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 7777777777,
                "content-type": "text/plain"
            }

        request_input = \
            {
                "collection-id": "274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }
        expected_response_output = \
            {
                "collection-id": "274",
                "collection-url": "/collections/LATEST/274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "object-url": "/objects/LATEST/49cf62814f629ba0a679147d8784b516cde30a17?collection-id=274",
                "owner-id": 100,
                "asset-id": 62,
                "object-state": object_status.CREATED,
                "raw-content-length": 7777777777,
                "content-type": "text/plain",
                "object-key-url": "https://datalake_url.com/objects/store/49cf62814f629ba0a679147d8784b516cde30a17",
                "object-redirect": True
            }
        self.assertEqual(get_object_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # Test of get object Lambda for binary object
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_binary_object(self, ct_get_item_mock, ost_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": "274",
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 100,
                "content-type": "application/octet-stream"
            }

        request_input = \
            {
                "collection-id": "274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }
        expected_response_output = \
            {
                "collection-id": "274",
                "collection-url": "/collections/LATEST/274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "object-url": "/objects/LATEST/49cf62814f629ba0a679147d8784b516cde30a17?collection-id=274",
                "owner-id": 100,
                "asset-id": 62,
                "object-state": object_status.CREATED,
                "raw-content-length": 100,
                "content-type": "application/octet-stream",
                "object-key-url": "https://datalake_url.com/objects/store/49cf62814f629ba0a679147d8784b516cde30a17",
                "object-redirect": True
            }
        self.assertEqual(get_object_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # +Successful test of Lambda for collection that supports versions
    # - no version-number in request so returns latest version
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_success2(self, ct_get_item_mock, ost_get_item_mock,
                                         s3_get_object_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": "274",
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }
        s3_get_object_mock.return_value.get_object.return_value = {
            'Body': StreamingBody(io.BytesIO(b"Raw file text"), 13),
            'Metadata': {}
        }
        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }
        expected_response_output = \
            {
                "collection-id": '274',
                "collection-url": "/collections/LATEST/274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "object-url": "/objects/LATEST/49cf62814f629ba0a679147d8784b516cde30a17?collection-id=274",
                "owner-id": 100,
                "asset-id": 62,
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "object-key-url": "https://datalake_url.com/objects/store/49cf62814f629ba0a679147d8784b516cde30a17",
                "object-state": object_status.CREATED,
                "object-body": "Raw file text"
            }

        self.assertEqual(get_object_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # +Successful test of Lambda for collection that supports versions
    # - version-number in request (not latest version)
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.get_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_success3(self, ct_get_item_mock, ost_get_item_mock,
                                         osvt_get_item_mock, s3_get_object_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 2
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }
        osvt_get_item_mock.return_value = \
            {
                "composite-key": "49cf62814f629ba0a679147d8784b516cde30a17|274",
                "version-number": 1,
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-timestamp": "2018-02-07T08:40:59.592Z",
                "raw-content-length": 24,
                "content-type": "text/plain",
            }
        s3_get_object_mock.return_value.get_object.return_value = {
            'Body': StreamingBody(io.BytesIO(b"Raw file text"), 13),
            'Metadata': {}
        }
        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number": 1
            }
        expected_response_output = \
            {
                "collection-id": '274',
                "collection-url": "/collections/LATEST/274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "object-url": "/objects/LATEST/49cf62814f629ba0a679147d8784b516cde30a17?collection-id=274",
                "owner-id": 100,
                "asset-id": 62,
                "version-number": 1,
                "version-timestamp": "2018-02-07T08:40:59.592Z",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "object-key-url": "https://datalake_url.com/objects/store/49cf62814f629ba0a679147d8784b516cde30a17",
                "object-state": object_status.CREATED,
                "object-body": "Raw file text"
            }

        self.assertEqual(get_object_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # +Successful test of Lambda for object with user defined metadata
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_success4(self, ct_get_item_mock, ost_get_item_mock,
                                         s3_get_object_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": "274",
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }
        s3_get_object_mock.return_value.get_object.return_value = {
            'Body': StreamingBody(io.BytesIO(b"Raw file text"), 13),
            'Metadata': {
                "dl-version-timestamp": "2018-02-07T09:40:59.592Z",
                "dl-content-sha1": "943012227688fa7ebaa24761378119f9d50aac53",
                "dl-object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "dl-version-number": "2",
                "dl-collection-id": "274",
                "name1": "value1",
                "name2": "value2"
            }
        }
        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }
        expected_response_output = \
            {
                "collection-id": '274',
                "collection-url": "/collections/LATEST/274",
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "object-url": "/objects/LATEST/49cf62814f629ba0a679147d8784b516cde30a17?collection-id=274",
                "owner-id": 100,
                "asset-id": 62,
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "object-key-url": "https://datalake_url.com/objects/store/49cf62814f629ba0a679147d8784b516cde30a17",
                "object-state": object_status.CREATED,
                "object-metadata": {
                    "dl-version-timestamp": "2018-02-07T09:40:59.592Z",
                    "dl-content-sha1": "943012227688fa7ebaa24761378119f9d50aac53",
                    "dl-object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                    "dl-version-number": "2",
                    "dl-collection-id": "274",
                    "name1": "value1",
                    "name2": "value2"
                },
                "object-body": "Raw file text"
            }

        self.assertEqual(get_object_command(request_input, "LATEST")['response-dict'], expected_response_output)

    # -Failure test of Lambda for collection that doesn't exist and ClientError when reading CollectionTable
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail1(self, ct_get_item_mock):
        ct_get_item_mock.return_value = {}

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number": 1
            }

        with self.assertRaisesRegex(NoSuchCollection, "Invalid Collection ID 274"):
            get_object_command(request_input, "LATEST")

        ct_get_item_mock.side_effect = ClientError({}, "dummy")

        with self.assertRaisesRegex(InternalError, "Unable to get item from Collection Table"):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for collection that doesn't exist and Exception when reading CollectionTable
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail1b(self, ct_get_item_mock):
        ct_get_item_mock.return_value = {}

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number": 1
            }

        with self.assertRaisesRegex(NoSuchCollection, "Invalid Collection ID 274"):
            get_object_command(request_input, "LATEST")

        ct_get_item_mock.side_effect = Exception()

        with self.assertRaises(UnhandledException):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for collection that is terminated
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail2(self, ct_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.TERMINATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number": 1
            }

        with self.assertRaisesRegex(SemanticError, "Collection ID 274 is terminated"):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for object that doesn't exist and exception when reading ObjectStoreTable
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail4(self, ct_get_item_mock, ost_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = {}

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number": 1
            }

        with self.assertRaisesRegex(NoSuchObject,
                                    "Invalid Object ID 49cf62814f629ba0a679147d8784b516cde30a17 in Collection ID 274"):
            get_object_command(request_input, "LATEST")

        ost_get_item_mock.side_effect = ClientError({}, "dummy")

        with self.assertRaisesRegex(InternalError, "Unable to get item from Object Store Table"):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for object that have been removed
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail5(self, ct_get_item_mock, ost_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.REMOVED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            }

        with self.assertRaisesRegex(SemanticError,
                                    "Object ID 49cf62814f629ba0a679147d8784b516cde30a17"
                                    " in Collection ID 274 has been removed"):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for version number that doesn't exist for object
    # and exception when reading ObjectStoreVersionTable
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.get_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail6(self, ct_get_item_mock, ost_get_item_mock,
                                      osvt_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }
        osvt_get_item_mock.return_value = {}

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number": 3
            }

        with self.assertRaisesRegex(NoSuchObjectVersion,
                                    "Invalid Version Number 3 for Object ID 49cf62814f629ba0a679147d8784b516cde30a17"
                                    " in Collection ID 274"):
            get_object_command(request_input, "LATEST")

        osvt_get_item_mock.side_effect = ClientError({}, "dummy")

        with self.assertRaisesRegex(InternalError, "Unable to get item from Object Store Version Table"):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for error when reading S3 object (ClientError)
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail7(self, ct_get_item_mock, ost_get_item_mock,
                                      s3_get_client_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }
        s3_get_client_mock.return_value.get_object.side_effect = ClientError({}, "dummy")

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }

        with self.assertRaisesRegex(InternalError, "Unable to read S3 object 49cf62814f629ba0a679147d8784b516cde30a17"
                                                   " from bucket ccs-sandbox-datalake-object-store"):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for error when reading S3 object (Exception)
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail7b(self, ct_get_item_mock, ost_get_item_mock,
                                       s3_get_client_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }
        s3_get_client_mock.return_value.get_object.side_effect = Exception()

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }

        with self.assertRaises(UnhandledException):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for object that is a multipart/* content type
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail8(self, ct_get_item_mock, ost_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "multipart/mixed",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17"
            }

        with self.assertRaisesRegex(NotAllowedError,
                                    "Object ID 49cf62814f629ba0a679147d8784b516cde30a17"
                                    " is a multipart object and must be retrieved through the object-key-url"):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for request for version object that have been removed
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_object_command_fail9(self, ct_get_item_mock, ost_get_item_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_get_item_mock.return_value = \
            {
                "object-state": object_status.REMOVED,
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "collection-id": '274',
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                "raw-content-length": 24,
                "content-type": "tombstone",
                "version-number": 2,
                "version-timestamp": "2018-02-07T09:40:59.592Z"
            }

        request_input = \
            {
                "collection-id": '274',
                "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                "version-number" : 2
            }

        with self.assertRaisesRegex(SemanticError,
                                    "Object ID 49cf62814f629ba0a679147d8784b516cde30a17"
                                    " in Collection ID 274 has been removed"):
            get_object_command(request_input, "LATEST")

    # -Failure test of Lambda for object that contains changeset ID but no version number
    def test_get_object_command_fail_changeset_no_version(self):
        request_input = {
            "collection-id": '274',
            "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            "changeset-id": "test-changeset"
        }
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Version number does not exist in the request"):
            get_object_command(request_input, "LATEST")

    # -Failure test of lambda for object that contains non existent changeset
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_object_command_fail_changeset_not_exist(self, mock_get_changeset):
        mock_get_changeset.return_value = {}
        request_input = {
            "collection-id": '274',
            "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            "changeset-id": "test-changeset",
            "version-number": 1
        }
        with self.assertRaisesRegex(NoSuchChangeset, "Invalid Changeset ID test-changeset"):
            get_object_command(request_input, "LATEST")

    # -Failure test of lambda for object that contains expired changeset
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_object_command_fail_changeset_expired(self, mock_get_changeset):
        mock_get_changeset.return_value = {
            "changeset-id": "test-changeset",
            "changeset-state": changeset_status.EXPIRED
        }
        request_input = {
            "collection-id": '274',
            "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            "changeset-id": "test-changeset",
            "version-number": 1
        }
        with self.assertRaisesRegex(NoSuchChangeset, "Invalid Changeset ID test-changeset"):
            get_object_command(request_input, "LATEST")

    # -Failure test of lambda for object that contains expired changeset
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_object_command_fail_changeset_client_error(self, mock_get_changeset):
        mock_get_changeset.side_effect = ClientError({"ResponseMetadata": {},
                                                      "Error": {"Code": "mock error code",
                                                                "Message": "mock error message"}},
                                                     "client-error-mock")
        request_input = {
            "collection-id": '274',
            "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            "changeset-id": "test-changeset",
            "version-number": 1
        }
        with self.assertRaisesRegex(InternalError, "Unable to get item from Changeset Table"):
            get_object_command(request_input, "LATEST")

            # -Failure test of lambda for object that contains expired changeset
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_object_command_fail_changeset_unhandled_error(self, mock_get_changeset):
        mock_get_changeset.side_effect = Exception('mock error')

        request_input = {
            "collection-id": '274',
            "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            "changeset-id": "test-changeset",
            "version-number": 1
        }
        with self.assertRaisesRegex(UnhandledException, "mock error"):
            get_object_command(request_input, "LATEST")

    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_object_command_success_changeset(self, mock_get_changeset, mock_get_collection, mock_get_changeset_object_item, mock_s3_get_object):
        mock_get_changeset.return_value = {
            "changeset-id": "test-changeset",
            "changeset-state": changeset_status.OPENED
        }
        mock_get_collection.return_value = {
            "collection-id": '274',
            "collection-state": collection_status.CREATED,
            "collection-timestamp": "2017-12-06T20:33:37.618Z",
            "collection-name": "test-collection",
            "owner-id": 100,
            "asset-id": 62,
            'old-object-versions-to-keep': 0
        }

        mock_get_changeset_object_item.return_value = {
            "object-state": "Created",
            "collection-id": "test-collection",
            "changeset-id": "test-changeset",
            "version-number": 1,
            "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
            "version-timestamp": "2019-06-24T18:10:25.319Z",
            "object-id": "test-object",
            "content-type": "text/plain",
            "raw-content-length": 34,
            "bucket-name": "feature-dan-dl-object-store-288044017584-use1",
            "replicated-buckets": ["feature-dan-dl-object-store-288044017584-usw2"]
        }
        mock_s3_get_object.return_value.get_object.return_value = {
            'Body': StreamingBody(io.BytesIO(b"Raw file text"), 13),
            'Metadata': {}
        }
        request_input = {
            "collection-id": '274',
            "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            "changeset-id": "test-changeset",
            "version-number": 1
        }
        expected_response_output = {
            "collection-id": '274',
            "collection-url": "/collections/LATEST/274",
            "changeset-id": "test-changeset",
            "version-number": 1,
            "version-timestamp": "2019-06-24T18:10:25.319Z",
            "object-id": "test-object",
            "object-url": "/objects/LATEST/test-object?collection-id=274",
            "owner-id": 100,
            "asset-id": 62,
            "object-state": object_status.CREATED,
            "raw-content-length": 34,
            "content-type": "text/plain",
            "object-key-url": "https://datalake_url.com/objects/store/49cf62814f629ba0a679147d8784b516cde30a17",
            "object-body": "Raw file text"
        }
        self.assertEqual(get_object_command(request_input, "LATEST")['response-dict'], expected_response_output)



    # + Valid types of utf8
    def test_is_utf8_content_valid(self):
        self.assertTrue(is_utf8_content('application/json'))
        self.assertTrue(is_utf8_content('application/json;'))
        self.assertTrue(is_utf8_content('application/json; charset=utf-8'))
        self.assertTrue(is_utf8_content('application/xml; charset=UTF-8'))
        self.assertTrue(is_utf8_content('application/xml;  charset=utf-8 '))

    # - InValid types of utf8
    def test_is_utf8_content_invalid(self):
        self.assertFalse(is_utf8_content('application/json; charset=utf8'))
        self.assertFalse(is_utf8_content('application/json; charset=utf-16'))
        self.assertFalse(is_utf8_content('application/xml; charset=ISO'))

if __name__ == '__main__':
    unittest.main()
