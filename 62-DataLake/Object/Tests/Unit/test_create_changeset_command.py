import unittest
from datetime import datetime
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyName, InternalError, InvalidRequestPropertyValue
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_changeset_command
from create_changeset_command import generate_changeset_expiration_epoch, generate_event_store_item

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'CreateChangesetCommand')


class TestCreateChangesetCommand(unittest.TestCase):
    session_patch = None

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(create_changeset_command)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_command_schema_validation_fail(self, ot_query_items_mock):
        ot_query_items_mock.return_value = io_utils.load_data_json('ownerTable.query.item.response.json')
        request_input = io_utils.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InvalidRequestPropertyName, "must contain only specified properties"):
                create_changeset_command.lambda_handler(request_input, MockLambdaContext())

    # -create_changeset_command - Invalid Description Len
    def test_create_changeset_command_invalid_description(self):

        request_input = io_utils.load_data_json('apigateway.request.invalid_len.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Description"):
                create_changeset_command.lambda_handler(request_input, MockLambdaContext())

    # Successful test using lambda_handler with command_wrapper decorator
    @patch('create_changeset_command.generate_changeset_expiration_epoch')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_create_changeset_command_success(self, ot_query_items_mock, est_put_item_mock,
                                              changeset_expiration_date_mock):
        changeset_expiration_date_mock.return_value = datetime(2019, 1, 1)
        est_put_item_mock.return_value = None
        ot_query_items_mock.return_value = io_utils.load_data_json('ownerTable.query.item.response.json')

        request_input = io_utils.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_utils.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(
                create_changeset_command.lambda_handler(request_input, MockLambdaContext()),
                response_output)

    # - test_test_generate_response_json: Missing required key
    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    def test_generate_response_json_failed_required_attribute(self, mock_required_schema_keys):
        mock_required_schema_keys.return_value = ["changeset-id",
                                                  "owner-id",
                                                  "changeset-url",
                                                  "changeset-state",
                                                  "changeset-timestamp",
                                                  "changeset-expiration-date", "changeset_missing"]

        with self.assertRaisesRegex(InternalError, "Missing required property: changeset_missing"):
            create_changeset_command.generate_response_json({"description": "chanegset description"},
                                                            "123", 109, 125469, 'LATEST')

    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_event_store_item(self, mock_command_wrapper):
        mock_command_wrapper.return_value = "2019-08-31T05:42:12.444Z"
        changeset_response = {'owner-id': 109, 'description': 'changeset description'}
        expected_response = {
            'changeset-id': "123",
            'owner-id': changeset_response['owner-id'],
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'pending-expiration-epoch': 252460800,
            'event-name': "Changeset::Open",
            'event-version': 1,
            'stage': 'v1',
            'description': 'changeset description'
        }
        self.assertDictEqual(expected_response, generate_event_store_item(changeset_response, "123", 252460800, 'v1'))

    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_changeset_expiration_epoch(self, mock_command_wrapper):
        mock_command_wrapper.return_value = "2019-08-31T05:42:12.444Z"
        changeset_expiration_date = generate_changeset_expiration_epoch()
        self.assertEqual(changeset_expiration_date, 1569822132)


if __name__ == '__main__':
    unittest.main()
