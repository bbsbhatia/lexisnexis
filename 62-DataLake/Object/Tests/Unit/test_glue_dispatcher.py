import json
import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

import orjson
from botocore.exceptions import ClientError, ReadTimeoutError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import glue_dispatcher
from glue_dispatcher import lambda_handler, get_s3_object_metadata, is_oldest_tracking_item, modify_tracking_item, \
    get_tracking_item, update_tracking_item, get_object_store_item, move_object_in_lambda, start_glue_job, \
    transform_message_and_publish_to_topic, handle_exceptions, process_retry_exception

__author__ = "Shekhar Ralhan, John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "GlueDispatcher")


class GlueDispatcherTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'GLUE_DISPATCHER_TOPIC_ARN': 'test_topic_arn',
                             'DATALAKE_BUCKET_NAME': 'test_bucket_name',
                             'GLUE_LARGE_OBJECT_PROCESSOR_JOB_NAME': 'test_glue_job_name'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.error_handler_patch = patch('lng_datalake_commons.error_handling.error_handler.terminal_error').start()
        reload(glue_dispatcher)
        cls.mock_metadata = {"object-id": "fake-object", "collection-id": "123",
                             "event-id": "abcdefg", 'request-time': '2018-05-19T19:31:17.000Z',
                             'old-object-versions-to-keep': '0', 'pending-expiration-epoch': '123',
                             'collection-hash': 'collection-hash-123'}
        cls.mock_metadata_with_changeset = {"object-id": "fake-object", "collection-id": "123",
                                            "event-id": "abcdefg", 'request-time': '2018-05-19T19:31:17.000Z',
                                            'old-object-versions-to-keep': '0', 'pending-expiration-epoch': '123',
                                            'collection-hash': 'collection-hash-123',
                                            'changeset-id': 'jek-changeset', 'changeset-expiration-epoch': '1234567890'}
        cls.mock_tracking_record = {'tracking-id': 'fake-object|123|object', 'event-id': 'abcdefg',
                                    'request-time': '2018-05-19T19:31:17.000Z',
                                    'event-name': 'Object::Create'}
        cls.mock_tracking_record_with_ttl = {'tracking-id': 'fake-object|123|object', 'event-id': 'abcdefg',
                                             'request-time': '2018-05-19T19:31:17.000Z',
                                             'event-name': 'Object::Create', 'pending-expiration-epoch': 123}
        cls.mock_object_store_item = {"object-id": "fake-object", "collection-id": "123",
                                      "object-hash": "abcdefg", 'version-timestamp': '2018-05-19T19:31:17.000Z',
                                      'object-state': "Created", 'object-key': 'some-key'}

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        cls.error_handler_patch.stop()

    # + success glue_dispatcher.lambda_handler - no OST entry
    @patch("lng_aws_clients.glue.get_client")
    @patch("glue_dispatcher.get_object_store_item")
    @patch("glue_dispatcher.is_oldest_tracking_item")
    @patch('glue_dispatcher.update_tracking_item')
    @patch('glue_dispatcher.get_tracking_item')
    @patch("glue_dispatcher.get_s3_object_metadata")
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_success1(self, mock_s3_client,  # NOSONAR
                                     get_s3_object_metadata_mock,
                                     get_tracking_item_mock,
                                     update_tracking_item_mock,
                                     is_oldest_tracking_item_mock,
                                     get_object_store_item_mock,
                                     glue_mock):
        mock_s3_client.return_value = None

        get_s3_object_metadata_mock.return_value = self.mock_metadata_with_changeset.copy()
        get_tracking_item_mock.return_value = self.mock_tracking_record_with_ttl.copy()
        update_tracking_item_mock.return_value = None
        is_oldest_tracking_item_mock.return_value = True

        valid_event = io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')
        valid_glue_resp = io_utils.load_data_json('glue_resp.json')
        get_object_store_item_mock.return_value = {}
        glue_mock.return_value.start_job_run.return_value = valid_glue_resp
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)
        get_tracking_item_mock.assert_called_once()
        update_tracking_item_mock.assert_called_once()

    # + success glue_dispatcher.lambda_handler - OST entry
    @patch("lng_aws_clients.glue.get_client")
    @patch("glue_dispatcher.get_object_store_item")
    @patch("glue_dispatcher.is_oldest_tracking_item")
    @patch('glue_dispatcher.update_tracking_item')
    @patch('glue_dispatcher.get_tracking_item')
    @patch("glue_dispatcher.get_s3_object_metadata")
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_success2(self, mock_s3_client,  # NOSONAR
                                     get_s3_object_metadata_mock,
                                     get_tracking_item_mock,
                                     update_tracking_item_mock,
                                     is_oldest_tracking_item_mock,
                                     get_object_store_item_mock,
                                     glue_mock):
        mock_s3_client.return_value = None

        get_s3_object_metadata_mock.return_value = self.mock_metadata.copy()
        get_tracking_item_mock.return_value = self.mock_tracking_record_with_ttl.copy()
        update_tracking_item_mock.return_value = None
        is_oldest_tracking_item_mock.return_value = True

        valid_event = io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')
        valid_glue_resp = io_utils.load_data_json('glue_resp.json')
        get_object_store_item_mock.return_value = io_utils.load_data_json('valid_object_item.json')
        glue_mock.return_value.start_job_run.return_value = valid_glue_resp
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)

    # + success glue_dispatcher.lambda_handler - Move Object Within Lambda
    @patch("glue_dispatcher.transform_message_and_publish_to_topic")
    @patch('glue_dispatcher.move_object_in_lambda')
    @patch("glue_dispatcher.get_object_store_item")
    @patch("glue_dispatcher.is_oldest_tracking_item")
    @patch('glue_dispatcher.update_tracking_item')
    @patch('glue_dispatcher.get_tracking_item')
    @patch("glue_dispatcher.get_s3_object_metadata")
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_success3(self, mock_s3_client,  # NOSONAR
                                     get_s3_object_metadata_mock,
                                     get_tracking_item_mock,
                                     update_tracking_item_mock,
                                     is_oldest_tracking_item_mock,
                                     get_object_store_item_mock,
                                     move_object_in_lambda_mock,
                                     transform_and_publish_mock):
        mock_s3_client.return_value = None

        get_s3_object_metadata_mock.return_value = self.mock_metadata.copy()
        get_tracking_item_mock.return_value = self.mock_tracking_record_with_ttl.copy()
        update_tracking_item_mock.return_value = None
        is_oldest_tracking_item_mock.return_value = True

        valid_event = io_utils.load_data_json('valid_s3_event_move_in_lambda.json')
        get_object_store_item_mock.return_value = io_utils.load_data_json('valid_object_item.json')
        move_object_in_lambda_mock.return_value = self.mock_metadata.copy()
        transform_and_publish_mock.return_value = None
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)
        transform_and_publish_mock.assert_called_once()

    # + success glue_dispatcher.lambda_handler - Retry event
    @patch("glue_dispatcher.transform_message_and_publish_to_topic")
    @patch('glue_dispatcher.move_object_in_lambda')
    @patch("glue_dispatcher.is_oldest_tracking_item")
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_success4(self, mock_s3_client,  # NOSONAR
                                     is_oldest_tracking_item_mock,
                                     move_object_in_lambda_mock,
                                     transform_and_publish_mock):
        mock_s3_client.return_value = None

        is_oldest_tracking_item_mock.return_value = True

        valid_event = io_utils.load_data_json('valid_s3_event_with_retry_data.json')
        move_object_in_lambda_mock.return_value = self.mock_metadata.copy()
        transform_and_publish_mock.return_value = None
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)
        transform_and_publish_mock.assert_called_once()

    # + success glue_dispatcher.lambda_handler - Retry event: just publish message
    @patch("glue_dispatcher.transform_message_and_publish_to_topic")
    @patch("glue_dispatcher.is_oldest_tracking_item")
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_success5(self, mock_s3_client,
                                     is_oldest_tracking_item_mock,
                                     transform_and_publish_mock):
        mock_s3_client.return_value = None

        is_oldest_tracking_item_mock.return_value = True

        valid_event = io_utils.load_data_json('valid_s3_event_with_retry_data_2.json')
        transform_and_publish_mock.return_value = None
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)
        transform_and_publish_mock.assert_called_once()

    # + success glue_dispatcher.lambda_handler - Move Object Within Lambda, object has metadata
    @patch("glue_dispatcher.transform_message_and_publish_to_topic")
    @patch('glue_dispatcher.move_object_in_lambda')
    @patch("glue_dispatcher.get_object_store_item")
    @patch("glue_dispatcher.is_oldest_tracking_item")
    @patch('glue_dispatcher.update_tracking_item')
    @patch('glue_dispatcher.get_tracking_item')
    @patch("glue_dispatcher.get_s3_object_metadata")
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_success6(self, mock_s3_client,  # NOSONAR
                                     get_s3_object_metadata_mock,
                                     get_tracking_item_mock,
                                     update_tracking_item_mock,
                                     is_oldest_tracking_item_mock,
                                     get_object_store_item_mock,
                                     move_object_in_lambda_mock,
                                     transform_and_publish_mock):
        mock_s3_client.return_value = None

        metadata = self.mock_metadata.copy()
        metadata['object-metadata'] = '{"name1":"value1","name2":"value2"}'
        get_s3_object_metadata_mock.return_value = metadata
        get_tracking_item_mock.return_value = self.mock_tracking_record_with_ttl.copy()
        update_tracking_item_mock.return_value = None
        is_oldest_tracking_item_mock.return_value = True

        valid_event = io_utils.load_data_json('valid_s3_event_move_in_lambda.json')
        get_object_store_item_mock.return_value = io_utils.load_data_json('valid_object_item.json')
        move_object_in_lambda_mock.return_value = self.mock_metadata.copy()
        transform_and_publish_mock.return_value = None
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)
        transform_and_publish_mock.assert_called_once()

    # - glue_dispatcher.lambda_handler - glue exception raises TerminalErrorException
    @patch("lng_aws_clients.glue.get_client")
    @patch("glue_dispatcher.get_object_store_item")
    @patch("glue_dispatcher.is_oldest_tracking_item")
    @patch('glue_dispatcher.update_tracking_item')
    @patch('glue_dispatcher.get_tracking_item')
    @patch("glue_dispatcher.get_s3_object_metadata")
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_general_error(self, mock_s3_client,  # NOSONAR
                                          get_s3_object_metadata_mock,
                                          get_tracking_item_mock,
                                          update_tracking_item_mock,
                                          is_oldest_tracking_item_mock,
                                          get_object_store_item_mock,
                                          glue_mock):
        mock_s3_client.return_value = None
        valid_event = io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')
        get_s3_object_metadata_mock.return_value = self.mock_metadata.copy()
        get_tracking_item_mock.return_value = self.mock_tracking_record_with_ttl.copy()
        update_tracking_item_mock.return_value = None
        is_oldest_tracking_item_mock.return_value = True
        get_object_store_item_mock.return_value = {}
        glue_mock.return_value.start_job_run.side_effect = Exception()
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)
        self.error_handler_patch.assert_called_once()
        self.error_handler_patch.reset_mock()

    # - glue_dispatcher.lambda_handler - glue client error retry: ResourceNumberLimitExceededException
    @patch('glue_dispatcher.process_retry_exception')
    @patch("lng_aws_clients.glue.get_client")
    @patch("glue_dispatcher.get_object_store_item")
    @patch("glue_dispatcher.is_oldest_tracking_item")
    @patch('glue_dispatcher.update_tracking_item')
    @patch('glue_dispatcher.get_tracking_item')
    @patch("glue_dispatcher.get_s3_object_metadata")
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_error_retry(self, mock_s3_client,  # NOSONAR
                                        get_s3_object_metadata_mock,
                                        get_tracking_item_mock,
                                        update_tracking_item_mock,
                                        is_oldest_tracking_item_mock,
                                        get_object_store_item_mock,
                                        glue_mock,
                                        process_retry_exception_mock):
        mock_s3_client.return_value = None
        valid_event = io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')
        get_s3_object_metadata_mock.return_value = self.mock_metadata.copy()
        get_tracking_item_mock.return_value = self.mock_tracking_record_with_ttl.copy()
        update_tracking_item_mock.return_value = None
        is_oldest_tracking_item_mock.return_value = True
        get_object_store_item_mock.return_value = {}
        glue_mock.return_value.start_job_run.side_effect = [ClientError({'ResponseMetadata': {},
                                                                         'Error': {
                                                                             'Code': 'ResourceNumberLimitExceededException',
                                                                             'Message': 'This is a mock'}},
                                                                        "FAKE"),
                                                            io_utils.load_data_json('glue_resp.json')]
        process_retry_exception_mock.return_value = None
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)
        process_retry_exception_mock.assert_called_once()

    # + successful get_s3_object_metadata
    @patch("lng_aws_clients.s3.get_client")
    def test_get_s3_object_metadata_success(self, s3_client_mock):
        s3_client_mock.return_value.head_object.return_value = {
            'Metadata': {"object-id": "object",
                         "collection-id": "123",
                         "event-id": "abcdefg"}}
        self.assertEqual(get_s3_object_metadata("bucket", "key"),
                         ({"object-id": "object", "collection-id": "123", "event-id": "abcdefg"}))

    # - unsuccessful get_s3_object_metadata: EndpointConnectionError
    @patch("lng_aws_clients.s3.get_client")
    def test_get_s3_object_metadata_item_endpoint_connection_error_exception(self, s3_client_mock):
        s3_client_mock.return_value.head_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(RetryEventException, 'Unable to head S3 object'):
            get_s3_object_metadata("bucket", "key")

    # - unsuccessful get_s3_object_metadata: Client Error
    @patch("lng_aws_clients.s3.get_client")
    def test_get_s3_object_metadata_item_client_error(self, s3_client_mock):
        s3_client_mock.return_value.head_object.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        with self.assertRaisesRegex(RetryEventException, 'Unable to head S3 object'):
            get_s3_object_metadata("bucket", "key")

    # - unsuccessful get_s3_object_metadata: Client Error (Not Found)
    @patch("lng_aws_clients.s3.get_client")
    def test_get_s3_object_metadata_item_not_found(self, s3_client_mock):
        s3_client_mock.return_value.head_object.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Not Found"}}, "MockError")
        with self.assertRaisesRegex(TerminalErrorException, 'Object .* not found in bucket'):
            get_s3_object_metadata("bucket", "key")

    # - unsuccessful get_s3_object_metadata: General Error
    @patch("lng_aws_clients.s3.get_client")
    def test_get_s3_object_metadata_item_terminal_error_exception(self, s3_client_mock):
        s3_client_mock.return_value.head_object.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to do a head object on Bucket'):
            get_s3_object_metadata("bucket", "key")

    # + successful modify_tracking_item: already gotten record
    @patch('glue_dispatcher.update_tracking_item')
    def test_modify_tracking_item_success_already_gotten_tracking_item(self, update_tracking_item_mock):
        additional_attributes = {'retry-state': 30, 'tracking-data': self.mock_tracking_record_with_ttl}
        self.assertIsNone(modify_tracking_item(30, self.mock_metadata, additional_attributes))
        update_tracking_item_mock.called_once()

    # + successful get_tracking_item
    @patch('lng_datalake_dal.tracking_table.TrackingTable.get_item')
    def test_get_tracking_item_success(self, tt_get_item_mock):
        tt_get_item_mock.return_value = self.mock_tracking_record_with_ttl
        self.assertEqual(get_tracking_item(self.mock_metadata['object-id'],
                                           self.mock_metadata['collection-id'], self.mock_metadata['request-time']),
                         self.mock_tracking_record_with_ttl)

    # - unsuccessful get_tracking_item: Client Error
    @patch('lng_datalake_dal.tracking_table.TrackingTable.get_item')
    def test_get_tracking_item_client_error(self, tt_mock_get):
        tt_mock_get.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        with self.assertRaisesRegex(RetryEventException,
                                    'Unable to get item from the Tracking Table'):
            get_tracking_item(self.mock_metadata['object-id'],
                              self.mock_metadata['collection-id'], self.mock_metadata['request-time'])

    # - unsuccessful get_tracking_item: General Error
    @patch('lng_datalake_dal.tracking_table.TrackingTable.get_item')
    def test_get_tracking_item_general_exception(self, tt_mock_get):
        tt_mock_get.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to get item from Tracking table due to unknown reasons.'):
            get_tracking_item(self.mock_metadata['object-id'],
                              self.mock_metadata['collection-id'], self.mock_metadata['request-time'])

    # - unsuccessful get_tracking_item: no tracking item
    @patch('lng_datalake_dal.tracking_table.TrackingTable.get_item')
    def test_get_tracking_item_no_tracking_item(self, tt_mock_get):
        tt_mock_get.return_value = {}
        with self.assertRaisesRegex(TerminalErrorException,
                                    'No tracking record found'):
            get_tracking_item(self.mock_metadata['object-id'],
                              self.mock_metadata['collection-id'], self.mock_metadata['request-time'])

    # - unsuccessful get_tracking_item: tracking item has no pending-expiration-epoch
    @patch('lng_datalake_dal.tracking_table.TrackingTable.get_item')
    def test_get_tracking_item_no_ttl(self, tt_get_item_mock):
        tt_get_item_mock.return_value = self.mock_tracking_record
        with self.assertRaisesRegex(TerminalErrorException, 'has no pending-expiration-epoch'):
            get_tracking_item(self.mock_metadata['object-id'],
                              self.mock_metadata['collection-id'], self.mock_metadata['request-time'])

    # + successful update_tracking_item with ttl
    @patch('lng_datalake_dal.tracking_table.TrackingTable.update_item')
    def test_update_tracking_item_success(self, tt_update_item_mock):
        tt_update_item_mock.return_value = None
        self.assertIsNone(update_tracking_item(self.mock_tracking_record_with_ttl.copy()))

    # - unsuccessful update_tracking_item no ttl
    @patch('lng_datalake_dal.tracking_table.TrackingTable.update_item')
    def test_update_tracking_item_no_ttl(self, tt_update_item_mock):
        tt_update_item_mock.side_effect = ConditionError("Failed condition")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Condition Error while trying to remove PendingExpirationEpoch'):
            update_tracking_item(self.mock_tracking_record_with_ttl.copy())

    # - unsuccessful update_tracking_item: Client Error
    @patch('lng_datalake_dal.tracking_table.TrackingTable.update_item')
    def test_update_tracking_item_client_error(self, tt_update_item_mock):
        tt_update_item_mock.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        with self.assertRaisesRegex(RetryEventException,
                                    'Unable to update the Tracking Table'):
            update_tracking_item(self.mock_tracking_record_with_ttl.copy())

    # - unsuccessful update_tracking_item: General Error
    @patch('lng_datalake_dal.tracking_table.TrackingTable.update_item')
    def test_update_tracking_item_general_exception(self, tt_update_item_mock):
        tt_update_item_mock.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to update item in the Tracking Table due to unknown reasons.'):
            update_tracking_item(self.mock_tracking_record_with_ttl.copy())

    # + successful check_tracking_items with event id matching oldest tracking item
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_success(self, mock_get_oldest_tt_item):
        mock_get_oldest_tt_item.return_value = {'tracking-id': 'object-id.fake_object', 'event-id': 'fake_event_id_1',
                                                'request-time': '2018-05-19T19:31:17.000Z',
                                                'event-name': 'Object::Create'}

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_1"
        self.assertIsNone(is_oldest_tracking_item(object_id, collection_id, event_id))

    # - fail check_tracking_items with event id not matching oldest tracking item
    @patch('lng_datalake_commons.tracking.tracker._is_oldest_event')
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_fail(self, mock_get_oldest_tt_item, mock_is_oldest_event):
        mock_is_oldest_event.return_value = False
        mock_get_oldest_tt_item.return_value = {'tracking-id': 'object-id.fake_object', 'event-id': 'fake_event_id_1',
                                                'request-time': '2018-05-19T19:31:17.000Z',
                                                'event-name': 'Object::Create'}

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_2"
        with self.assertRaisesRegex(RetryEventException, 'Oldest Tracking item'):
            is_oldest_tracking_item(object_id, collection_id, event_id)

    # - fail check_tracking_items: ClientError on _get_oldest_item_by_tracking_id()
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_fail2(self, mock_get_oldest_tt_item):
        mock_get_oldest_tt_item.side_effect = ClientError({"Error": {"Code": 101,
                                                                     "Message": "Mock Client Error"}},
                                                          "MockError")

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_2"
        with self.assertRaisesRegex(RetryEventException, 'Unable to query items in Tracking Table'):
            is_oldest_tracking_item(object_id, collection_id, event_id)

    # - fail check_tracking_items: TerminalError on _get_oldest_item_by_tracking_id()
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_fail3(self, mock_get_oldest_tt_item):
        mock_get_oldest_tt_item.side_effect = TerminalErrorException("Unable to query items in Tracking Table")

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_2"
        with self.assertRaisesRegex(TerminalErrorException, 'Unable to query items in Tracking Table'):
            is_oldest_tracking_item(object_id, collection_id, event_id)

    # - unsuccess check_tracking_items with no tracking items for object
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_no_tracking(self, mock_get_oldest_tt_item):
        mock_get_oldest_tt_item.return_value = {}

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_2"
        with self.assertRaisesRegex(TerminalErrorException, 'No tracking record found'):
            is_oldest_tracking_item(object_id, collection_id, event_id)

    # - unsuccessful get_object_store_item: EndpointConnectionError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_store_item_endpoint_connection_error_exception(self, ost_mock_get):
        ost_mock_get.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        object_id = 'object-id.fake'
        collection_id = 'fake-collection'
        with self.assertRaisesRegex(RetryEventException, 'Unable to get item from ObjectStore Table'):
            get_object_store_item(object_id, collection_id)

    # - unsuccessful get_object_store_item: Client Error
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_store_item_client_error(self, ost_mock_get):
        ost_mock_get.side_effect = ClientError({"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        object_id = 'object-id.fake'
        collection_id = 'fake-collection'
        with self.assertRaisesRegex(RetryEventException, 'Unable to get item from ObjectStore Table'):
            get_object_store_item(object_id, collection_id)

    # - unsuccessful get_object_store_item: General Error
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_store_item_general_exception(self, ost_mock_get):
        ost_mock_get.side_effect = Exception("Mock Exception")
        object_id = 'object-id.fake'
        collection_id = 'fake-collection'
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to get item from ObjectStore Table'):
            get_object_store_item(object_id, collection_id)

    # + move_object_in_lambda: New updated object
    @patch('service_commons.object_common.object_contents_match')
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda(self, mock_s3_client, mock_object_contents_match):
        mock_s3_object = {'Body': MockByteIterable(True), 'ContentLength': 123, 'ContentType': "text/plain"}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object
        mock_s3_client.return_value.copy.return_value = None
        mock_object_contents_match.return_value = False
        expected_dict = {'collection-hash': 'collection-hash-123',
                         'collection-id': '123',
                         'content-sha1': '0748fe5a27e19197802e12ca36fedefdae04b090',
                         'content-type': 'text/plain',
                         'event-id': 'abcdefg',
                         'is-new-object': True,
                         'object-hash': 'd087b702243872f0d9fd22f2e2741286d1c5b092',
                         'object-id': 'fake-object',
                         'object-key': 'collection-hash-123/0603e7a8d7ebb772f608e3f65930eac40b562d29',
                         'old-object-versions-to-keep': 0,
                         'raw-content-length': 123,
                         'pending-expiration-epoch': 123,
                         'request-time': '2018-05-19T19:31:17.000Z',
                         'staging-bucket': 'mock_bucket_name',
                         'staging-key': 'mock_key',
                         'version-number': 1,
                         'changeset-id': 'jek-changeset',
                         'changeset-expiration-epoch': 1234567890}
        self.assertDictEqual(
            move_object_in_lambda(self.mock_metadata_with_changeset, 'mock_bucket_name', 'mock_key',
                                  self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2']), expected_dict)

    # + move_object_in_lambda: New updated object with object metadata
    @patch('service_commons.object_common.object_contents_match')
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda_with_metadata(self, mock_s3_client, mock_object_contents_match):
        mock_s3_object = {'Body': MockByteIterable(True), 'ContentLength': 123, 'ContentType': "text/plain"}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object
        mock_s3_client.return_value.copy.return_value = None
        mock_object_contents_match.return_value = False
        object_metadata = {'name1': 'value1', 'name2': 'value2'}
        expected_dict = {'collection-hash': 'collection-hash-123',
                         'collection-id': '123',
                         'content-sha1': '0748fe5a27e19197802e12ca36fedefdae04b090',
                         'content-type': 'text/plain',
                         'event-id': 'abcdefg',
                         'is-new-object': True,
                         'object-hash': '39c09da3091757229ab747474684f6ceae54f359',
                         'object-id': 'fake-object',
                         'object-key': 'collection-hash-123/d2c061b0b745ee2090242daa73e1ddbfbb3dfadd',
                         'old-object-versions-to-keep': 0,
                         'raw-content-length': 123,
                         'pending-expiration-epoch': 123,
                         'request-time': '2018-05-19T19:31:17.000Z',
                         'staging-bucket': 'mock_bucket_name',
                         'staging-key': 'mock_key',
                         'version-number': 1,
                         'object-metadata': object_metadata}
        metadata = self.mock_metadata.copy()
        metadata['object-metadata'] = orjson.dumps(object_metadata).decode()
        self.maxDiff = None
        self.assertDictEqual(
            move_object_in_lambda(metadata, 'mock_bucket_name', 'mock_key', self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2']), expected_dict)

    # + move_object_in_lambda: New updated object
    @patch('service_commons.object_common.object_contents_match')
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda_existing_object(self, mock_s3_client, mock_object_contents_match):
        mock_s3_object = {'Body': MockByteIterable(True), 'ContentLength': 123, 'ContentType': "text/plain"}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object
        mock_s3_client.return_value.copy.return_value = None
        mock_object_contents_match.return_value = True
        expected_dict = {'collection-hash': 'collection-hash-123',
                         'collection-id': '123',
                         'content-sha1': '0748fe5a27e19197802e12ca36fedefdae04b090',
                         'content-type': 'text/plain',
                         'event-id': 'abcdefg',
                         'is-new-object': False,
                         'object-hash': 'd087b702243872f0d9fd22f2e2741286d1c5b092',
                         'object-id': 'fake-object',
                         'object-key': 'collection-hash-123/0603e7a8d7ebb772f608e3f65930eac40b562d29',
                         'old-object-versions-to-keep': 0,
                         'raw-content-length': 123,
                         'pending-expiration-epoch': 123,
                         'request-time': '2018-05-19T19:31:17.000Z',
                         'staging-bucket': 'mock_bucket_name',
                         'staging-key': 'mock_key',
                         'version-number': 1}
        self.assertEqual(
            move_object_in_lambda(self.mock_metadata, 'mock_bucket_name', 'mock_key', self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2']), expected_dict)

    # - move_object_in_lambda: Client Error: Not Found
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda_client_error_not_found(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Not Found"}}, "MockError")
        object_key = 'mock_key'
        bucket_name = 'mock_bucket_name'
        with self.assertRaisesRegex(TerminalErrorException, 'Object {0} not found in bucket {1}'.format(object_key,
                                                                                                        bucket_name)):
            move_object_in_lambda(self.mock_metadata, bucket_name, object_key, self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2'])

    # - move_object_in_lambda: Client Error: Throttle
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda_client_error_throttle(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Throttle"}}, "MockError")
        with self.assertRaisesRegex(RetryEventException, 'MockError'):
            move_object_in_lambda(self.mock_metadata, 'mock_bucket_name', 'mock_key', self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2'])

    # - move_object_in_lambda: EndpointConnectionError
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda_endpoint_connection_error(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(RetryEventException, 'https://fake.content.aws.lexis.com'):
            move_object_in_lambda(self.mock_metadata, 'mock_bucket_name', 'mock_key', self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2'])

    # - move_object_in_lambda: General Exception
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda_general_exception(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = Exception('Mock Error')
        with self.assertRaisesRegex(TerminalErrorException, 'Mock Error'):
            move_object_in_lambda(self.mock_metadata, 'mock_bucket_name', 'mock_key', self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2'])

    # - move_object_in_lambda: retryable error while building hash
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda_error_reading_stream(self, mock_s3_client):
        mock_s3_object = {'Body': MockByteIterable(True, generate_exception=True), 'ContentLength': 123,
                          'ContentType': "text/plain"}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object

        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object'):
            move_object_in_lambda(self.mock_metadata, 'mock_bucket_name', 'mock_key', self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2'])

    # - move_object_in_lambda: retryable error while copying object
    @patch('service_commons.object_event_handler.move_object_to_datalake_bucket')
    @patch('lng_aws_clients.s3.get_client')
    def test_move_object_in_lambda_error_copy_object(self, mock_s3_client, mock_move_object):
        mock_s3_object = {'Body': MockByteIterable(True), 'ContentLength': 123, 'ContentType': "text/plain"}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object
        mock_move_object.side_effect = ClientError(
            {"Error": {"Code": "InternalError", "Message": "Weencounteredaninternalerror.Pleasetryagain."}}, "MockError")

        with self.assertRaisesRegex(RetryEventException, 'Unable to copy S3 object'):
            move_object_in_lambda(self.mock_metadata, 'mock_bucket_name', 'mock_key', self.mock_object_store_item,
                                  ['mock_bucket_1', 'mock_bucket_2'])

    # + successful start_glue_job
    @patch('lng_aws_clients.glue.get_client')
    def test_start_glue_job_success(self, mock_glue_client):
        mock_glue_client.return_value.start_job_run.return_value = {'JobRunId': '1234567890'}
        with patch('glue_dispatcher.GLUE_JOB_NAME', 'mock_glue_job_name'):
            self.assertIsNone(start_glue_job([], "*", "*", self.mock_object_store_item))
            mock_glue_client.return_value.start_job_run.assert_called_with(JobName='mock_glue_job_name',
                                                                           Arguments={
                                                                               '--event_message': '{"completion-topic-arn":"test_topic_arn","object-store-buckets":[],"staging-bucket":"*","staging-object-key":"*","version-number":1,"object-state":"Created","object-key":"some-key","object-hash":"abcdefg","version-timestamp":"2018-05-19T19:31:17.000Z"}'})

    # - unsuccessful start_glue_job: Client Error
    @patch('lng_aws_clients.glue.get_client')
    def test_start_glue_job_client_error(self, mock_glue_client):
        mock_glue_client.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        with self.assertRaisesRegex(RetryEventException, 'MockError'):
            start_glue_job([], "*", "*", self.mock_object_store_item)

    # - unsuccessful start_glue_job: General Exception
    @patch('lng_aws_clients.glue.get_client')
    def test_start_glue_job_general_exception(self, mock_glue_client):
        mock_glue_client.side_effect = Exception("Mock General Error")
        with self.assertRaisesRegex(TerminalErrorException, 'Mock General Error'):
            start_glue_job([], "*", "*", self.mock_object_store_item)

    # + successful transform_message_and_publish_to_topic
    @patch('lng_aws_clients.sns.get_client')
    def test_transform_message_and_publish_to_topic_success(self, mock_sns_client):
        mock_sns_client.return_value.publish.return_value = None
        self.assertIsNone(transform_message_and_publish_to_topic({'message': 'text'}, 'test_arn', 'stage'))

    # - unsuccessful transform_message_and_publish_to_topic: client error
    @patch('lng_aws_clients.sns.get_client')
    def test_transform_message_and_publish_to_topic_client_error(self, mock_sns_client):
        mock_sns_client.return_value.publish.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        with self.assertRaisesRegex(RetryEventException, 'Mock Client Error'):
            transform_message_and_publish_to_topic({'some-string': 'value'}, 'test_arn', 'stage')

    # - unsuccessful transform_message_and_publish_to_topic: general exception
    @patch('lng_aws_clients.sns.get_client')
    def test_transform_message_and_publish_to_topic_general_exception(self, mock_sns_client):
        mock_sns_client.return_value.publish.side_effect = Exception("Mock General Exception")
        with self.assertRaisesRegex(TerminalErrorException, 'due to unknown reasons. Mock General Exception'):
            transform_message_and_publish_to_topic({'some-string': 'value'}, 'test_arn', 'stage')

    # + successful handle_exceptions
    @patch('lng_aws_clients.sqs.get_client')
    def test_handle_exceptions_success(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None
        unloaded_message = json.loads(
            io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')['Records'][0]['Sns']['Message'])

        with patch('glue_dispatcher.LATEST_MULTIPART_QUEUE_URL', 'fake_arn'):
            self.assertIsNone(handle_exceptions(RetryEventException('Some Error'), unloaded_message, 'stage', {}))
            self.error_handler_patch.not_called()
            self.error_handler_patch.reset_mock()

    # - unsuccessful handle_exceptions: failed to write to sqs
    @patch('lng_aws_clients.sqs.get_client')
    def test_handle_exceptions_failed_writing_to_sqs(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        unloaded_message = io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')

        with patch('glue_dispatcher.LATEST_MULTIPART_QUEUE_URL', 'fake_arn'):
            with self.assertRaisesRegex(RetryEventException, 'Original Error'):
                handle_exceptions(RetryEventException('Original Error'), unloaded_message, 'stage', {})
                self.error_handler_patch.not_called()
                self.error_handler_patch.reset_mock()

    # - unsuccessful handle_exceptions: too many seen
    @patch('lng_aws_clients.sqs.get_client')
    def test_handle_exceptions_too_many_retries(self, mock_sqs_client):
        mock_sqs_client.side_effect = Exception('Mock Error')
        retry_error = RetryEventException('Retry Error')
        unloaded_message = io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')
        with patch('glue_dispatcher.LATEST_MULTIPART_QUEUE_URL', 'fake_arn'):
            with patch('lng_datalake_commons.error_handling.error_handler.additional_attributes', {'seen-count': 1001}):
                self.assertIsNone(handle_exceptions(retry_error, unloaded_message, 'stage', {}))
                self.error_handler_patch.assert_called_once()
                self.error_handler_patch.reset_mock()

    # - unsuccessful handle_exceptions: general error
    def test_handle_exceptions_general_error(self):
        self.error_handler_patch.terminal_error.return_value = None
        self.assertIsNone(handle_exceptions(Exception('Mock Error'), {}, 'stage', {}))
        self.error_handler_patch.assert_called_once()
        self.error_handler_patch.reset_mock()

    # - unsuccessful handle_exceptions: terminal error
    def test_handle_exceptions_terminal_error(self):
        self.error_handler_patch.terminal_error.return_value = None
        self.assertIsNone(handle_exceptions(TerminalErrorException('Mock Error'), {}, 'stage', {}))
        self.error_handler_patch.assert_called_once()
        self.error_handler_patch.reset_mock()

    # + successful process_retry_exception
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_success(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None
        unloaded_message = io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')
        with patch('glue_dispatcher.LATEST_MULTIPART_QUEUE_URL', 'fake_arn'):
            self.assertIsNone(process_retry_exception(RetryEventException('Some Error'), unloaded_message, 'stage'))

    # - unsuccessful process_retry_exception: Exception sending sqs message
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_general_exception(self, mock_sqs_client):
        mock_sqs_client.side_effect = Exception('Mock Error')
        retry_error = RetryEventException('Retry Error')
        unloaded_message = io_utils.load_data_json('valid_s3_event_dispatch_to_glue.json')
        with patch('glue_dispatcher.LATEST_MULTIPART_QUEUE_URL', 'fake_arn'):
            with self.assertRaisesRegex(RetryEventException, 'Retry Error'):
                process_retry_exception(retry_error, unloaded_message, 'stage')


class MockByteIterable:

    def __init__(self, use_bytes, generate_exception=False):
        if use_bytes:
            self.sample_bytes = b'\xb3\xb1\xaf\xc8\xcdQ(K-*\xce\xcc\xcf\xb3U2\xd43PRH\xcdK\xceO\xc9\xccK\xb7U\n\rq\xd3\xb5P\xb2\xb7\xe3\xe5\xb2\tN\xcc-\xc8I\x05\xb2\x14\x14l\\sR\x81H!/17\xd5V)(1/%\x1f\xc8U\x02\xcb\x01e\x9332sR|\xf3\x8bK\xa0\n\xc0|C%\x05}\x88f}\x88n\xb2M2\xc2b\x92\x8d>\xd4y\x00P'
        else:
            self.sample_bytes = 123
        self.generate_exception = generate_exception

    def iter_chunks(self, bytes):
        if self.generate_exception:
            raise ReadTimeoutError(endpoint_url='mock_s3_url.com')
        keep_going = True
        while keep_going:
            current_chunk = self.sample_bytes
            keep_going = False
            yield current_chunk


if __name__ == '__main__':
    unittest.main()
