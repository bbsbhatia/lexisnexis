import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_ingestion_event_handler

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CreateIngestionEventHandler")


class CreateIngestionEventHandlerTest(TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # +create_ingestion_event_handler - object already in datalake
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.put_item')
    def test_create_object_event_handler_pass_existing_object(self, mock_put_item):
        mock_put_item.return_value = None
        event = io_utils.load_data_json("valid_cmd_event.json")
        self.assertEqual(create_ingestion_event_handler.lambda_handler(event, MockLambdaContext()),
                         event_handler_status.SUCCESS)

    # - create_ingestion_event_handler - message incorrect event name
    @patch("lng_aws_clients.session.set_session")
    def test_create_ingestion_event_handler_fail_1(self, mocked_set_session):
        mocked_set_session.return_value = None
        event = io_utils.load_data_json("invalid_event_name.json")
        with self.assertRaisesRegex(TerminalErrorException, 'event-name doesnt match'):
            create_ingestion_event_handler.create_ingestion_event_handler(event,
                                                                          'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # - create_ingestion_event_handler - message incorrect event version
    @patch("lng_aws_clients.session.set_session")
    def test_create_ingestion_event_handler_fail_2(self, mocked_set_session):
        mocked_set_session.return_value = None
        event = io_utils.load_data_json("invalid_event_version.json")
        with self.assertRaisesRegex(TerminalErrorException, 'event-version does not match'):
            create_ingestion_event_handler.create_ingestion_event_handler(event,
                                                                          'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # - create_ingestion_event_handler - message incorrect stage
    @patch("lng_aws_clients.session.set_session")
    def test_create_ingestion_event_handler_fail_3(self, mocked_set_session):
        mocked_set_session.return_value = None
        event = io_utils.load_data_json("invalid_stage_event.json")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to process event'):
            create_ingestion_event_handler.create_ingestion_event_handler(event,
                                                                          'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # test_generate_ingestion_item: missing required property
    def test_create_ingestion_event_handler_schema_failure(self):
        with self.assertRaisesRegex(TerminalErrorException, 'Missing required property: ingestion-id'):
            create_ingestion_event_handler.generate_ingestion_item({})

    # test_put_ingestion_item: EndpointConnectionError
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.put_item')
    def test_create_ingestion_put_item_connection_error_exception(self, cbt_mock_put_item):
        cbt_mock_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            create_ingestion_event_handler.put_ingestion_item({})

    # test_put_ingestion_item: client error
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.put_item')
    def test_create_ingestion_put_item_client_error(self, cbt_mock_put_item):
        cbt_mock_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                     'Error': {
                                                         'Code': 'OTHER',
                                                         'Message': 'This is a mock'}},
                                                    "FAKE")
        with self.assertRaisesRegex(ClientError, 'This is a mock'):
            create_ingestion_event_handler.put_ingestion_item({})

    # test_put_ingestion_item: general exception
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.put_item')
    def test_create_ingestion_put_item_fail(self, cbt_mock_put_item):
        cbt_mock_put_item.side_effect = Exception("Some Error")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to put item to Ingestion Table due to unknown reasons.'):
            create_ingestion_event_handler.put_ingestion_item({})


if __name__ == '__main__':
    unittest.main()
