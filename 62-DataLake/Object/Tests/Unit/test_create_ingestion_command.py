import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyName, InvalidRequestPropertyValue, \
    UNSUPPORTED_MEDIA_TYPE
from lng_datalake_constants import collection_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_ingestion_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'CreateIngestionCommand')


class TestCreateIngestionCommand(unittest.TestCase):
    session_patch = None

    @classmethod
    @patch.dict(os.environ, {'DATALAKE_STAGING_BUCKET_NAME': "bucket-name"})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(create_ingestion_command)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.put_item')
    def test_command_schema_validation_fail(self, cbt_put_mock, cbt_delete_mock):
        cbt_put_mock.return_value = None
        cbt_delete_mock.return_value = None
        request_input = io_utils.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(
                    InvalidRequestPropertyName,
                    r"data must contain \['collection-id', 'content-type', 'archive-format'\] properties"):
                create_ingestion_command.lambda_handler(request_input, MockLambdaContext())

    # -create_ingestion_command - Invalid Description Len
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.put_item')
    def test_create_ingestion_command_invalid_description(self, cbt_put_mock, cbt_delete_mock):
        cbt_put_mock.return_value = None
        cbt_delete_mock.return_value = None
        request_input = io_utils.load_data_json('apigateway.request.invalid_len.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Description"):
                create_ingestion_command.lambda_handler(request_input, MockLambdaContext())

    # Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.put_item')
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.put_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_ingestion_command_success(self, ct_get_item_mock, owner_auth_mock,
                                              ingestion_put_item_mock, cbt_put_item_mock,
                                              s3_client_mock, est_put_item_mock, changeset_get_item_mock):
        est_put_item_mock.return_value = None
        owner_auth_mock.return_value = None
        ingestion_put_item_mock.return_value = False
        cbt_put_item_mock.return_value = False
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "collection-hash": "abcdefg",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1,
                "object-expiration": '2h'
            }

        changeset_get_item_mock.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }

        s3_client_mock.return_value.create_multipart_upload.return_value = io_utils.load_data_json(
            'multipart_resp.json')

        request_input = io_utils.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_utils.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(
                create_ingestion_command.lambda_handler(request_input, MockLambdaContext()),
                response_output)

    # - test_test_generate_response_json: Missing required key
    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    def test_generate_response_json_no_S3(self, mock_required_schema_keys):
        mock_required_schema_keys.return_value = ['S3', 'UploadID']
        with self.assertRaisesRegex(InternalError, "Missing required property: S3"):
            create_ingestion_command.generate_response_json({}, {})

    # - test_test_generate_ingestion_response: Missing required key
    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    def test_generate_ingestion_return_json_no_collection_id(self, mock_required_schema_keys):
        mock_required_schema_keys.return_value = ["ingestion-state", "collection-id"]
        with self.assertRaisesRegex(InternalError, "Missing required property: collection-id"):
            create_ingestion_command.generate_ingestion_response_json({"ingestion-state": "Pending"}, "STAGE", {})

    # - test_invalid_content_type: invalid content-type
    def test_invalid_content_type(self):
        with self.assertRaisesRegex(UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"):
            create_ingestion_command.create_ingestion_command(
                {"collection-id": "foo", "content-type": "", "archive-format": "zip"}, "123-zxy", "LATEST",
                "testApiKeyId")


if __name__ == '__main__':
    unittest.main()
