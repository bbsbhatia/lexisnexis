import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException, \
    NoChangeEventException
from lng_datalake_constants.event_handler_status import SUCCESS
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_testhelper.io_utils import IOUtils

import close_changeset_event_handler as ccev
from close_changeset_event_handler import close_changeset_event_handler, get_changeset_item, generate_changeset_item, \
    update_changeset_item, check_collection_blocker, check_tracking, get_collections_from_changeset

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CloseChangesetEventHandler")


class CreateChangesetEventHandlerTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com',
                             'LAMBDA_TASK_ROOT': os.path.dirname(io_utils.schema_path),
                             'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'mock_arn'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.tracker_oldest_item_patch = patch(
            "lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id").start()
        cls.tracker_is_oldest_patch = patch("lng_datalake_commons.tracking.tracker._is_oldest_event").start()
        cls.tracker_delete_patch = patch("lng_datalake_commons.tracking.tracker._delete_tracking_item").start()

        cls.tracker_oldest_item_patch.return_value = {'event-id': 1}
        cls.tracker_is_oldest_patch.return_value = True
        reload(ccev)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        cls.tracker_oldest_item_patch.stop()
        cls.tracker_is_oldest_patch.stop()
        cls.tracker_delete_patch.stop()

    # + test_close_changeset_event_handler: success
    @patch('service_commons.object_event_handler.send_changeset_notifications')
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch('close_changeset_event_handler.get_collections_from_changeset')
    @patch('close_changeset_event_handler.check_tracking')
    @patch('close_changeset_event_handler.check_collection_blocker')
    @patch('close_changeset_event_handler.update_changeset_item')
    @patch('close_changeset_event_handler.get_changeset_item')
    @patch('service_commons.object_event_handler.validate_message')
    def test_close_changeset_event_handler(self, mock_validate_message, mock_get_changeset,
                                           mock_update_changeset, mock_check_blocker,
                                           mock_check_tracker, mock_get_collections,
                                           mock_get_catalog_ids_by_collection_id,
                                           mock_send_notifications):
        mock_validate_message.return_value = None
        mock_get_changeset.return_value = io_utils.load_data_json('dynamodb.changeset_data_valid.json')
        mock_update_changeset.return_value = None
        mock_check_blocker.return_value = None
        mock_check_tracker.return_value = None
        mock_get_collections.return_value = ['coll-1', 'coll-2', 'coll-3']
        mock_get_catalog_ids_by_collection_id.side_effect = [['cat-1', 'cat-2', 'cat-3'], [], []]
        mock_send_notifications.return_value = None
        valid_message = io_utils.load_data_json('sns.event_message_valid.json')
        self.assertEqual(
            close_changeset_event_handler(valid_message, 'mock-arn'), SUCCESS)

    # + test_close_changeset_event_handler: retry
    @patch('service_commons.object_event_handler.send_changeset_notifications')
    @patch('service_commons.object_event_handler.validate_message')
    def test_close_changeset_event_handler_retry(self, mock_validate_message, mock_send_notifications):
        mock_validate_message.return_value = None
        mock_send_notifications.return_value = None
        valid_message = io_utils.load_data_json('sns.event_message_retry.json')
        self.assertEqual(
            close_changeset_event_handler(valid_message, 'mock-arn'), SUCCESS)

    # + test_get_changeset_item: success
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_item_success(self, cbt_mock_get_item):
        cbt_mock_get_item.return_value = io_utils.load_data_json('dynamodb.changeset_data_valid.json')
        self.assertDictEqual(get_changeset_item('foo'), io_utils.load_data_json('dynamodb.changeset_data_valid.json'))

    # - test_get_changeset_item: expired
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_item_expired(self, cbt_mock_get_item):
        cbt_mock_get_item.return_value = io_utils.load_data_json('dynamodb.changeset_data_expired.json')
        with self.assertRaisesRegex(TerminalErrorException,
                                    'attempted to be closed in Expired state'):
            get_changeset_item('foo')

    # - test_get_changeset_item: closing
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_item_closing(self, cbt_mock_get_item):
        cbt_mock_get_item.return_value = io_utils.load_data_json('dynamodb.changeset_data_closing.json')
        with self.assertRaisesRegex(NoChangeEventException,
                                    'Changeset foo in Closing state. No change event'):
            get_changeset_item('foo')

    # - test_get_changeset_item: client error
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_item_client_error(self, cbt_mock_get_item):
        cbt_mock_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                     'Error': {
                                                         'Code': 'OTHER',
                                                         'Message': 'This is a mock'}},
                                                    "FAKE")
        with self.assertRaisesRegex(ClientError, 'This is a mock'):
            get_changeset_item('foo')

    # - test_get_changeset_item: general exception
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_item_exception(self, cbt_mock_get_item):
        cbt_mock_get_item.side_effect = Exception("Some Error")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to get item from Changeset Table.'):
            get_changeset_item('foo')

    # + test_generate_changeset_item: success
    def test_test_generate_changeset_item(self):
        valid_item = generate_changeset_item(io_utils.load_data_json('dynamodb.changeset_data_valid.json'), 'event-2')
        expected = io_utils.load_data_json('dynamodb.changeset_data_closing.json')
        self.assertDictEqual(valid_item, expected)

    # - test_generate_changeset_item: missing required property
    def test_generate_changeset_item_schema_failure(self):
        with self.assertRaisesRegex(TerminalErrorException, 'Missing required property: changeset-id'):
            generate_changeset_item({}, 'bogus_request')

    # - test_update_changeset_item: client error
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.update_item')
    def test_update_changeset_item_client_error(self, cbt_mock_update_item):
        cbt_mock_update_item.side_effect = ClientError({'ResponseMetadata': {},
                                                        'Error': {
                                                            'Code': 'OTHER',
                                                            'Message': 'This is a mock'}},
                                                       "FAKE")
        with self.assertRaisesRegex(ClientError, 'This is a mock'):
            update_changeset_item({}, 'foo')

    # - test_update_changeset_item: condition error
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.update_item')
    def test_update_changeset_item_condition_error(self, cbt_mock_update_item):
        cbt_mock_update_item.side_effect = ConditionError("Some Condition Exception")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to update item in Changeset Table because existing item in incorrect state.'):
            update_changeset_item({}, 'foo')

    # - test_update_changeset_item: general exception
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.update_item')
    def test_update_changeset_item_exception(self, cbt_mock_update_item):
        cbt_mock_update_item.side_effect = Exception("Some Error")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to update item in Changeset Table due to unknown reasons.'):
            update_changeset_item({}, 'foo')

    # - test_check_collection_blocker: client error
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_collection_blocker_client_error(self, cbt_mock_query):
        cbt_mock_query.side_effect = ClientError({'ResponseMetadata': {},
                                                  'Error': {
                                                      'Code': 'OTHER',
                                                      'Message': 'This is a mock'}},
                                                 "FAKE")
        with self.assertRaisesRegex(ClientError, 'This is a mock'):
            check_collection_blocker('foo')

    # - test_check_collection_blocker: general exception
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_collection_blocker_exception(self, cbt_mock_query):
        cbt_mock_query.side_effect = Exception("Some Error")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to query Collection Blocker Table due to unknown reasons.'):
            check_collection_blocker('foo')

    # - test_check_tracking: existing tracking record
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items')
    def test_check_collection_blocker_retry(self, cbt_mock_query):
        cbt_mock_query.return_value = [
            {'collection-id': 'xyz', 'request-time': 'mock-request-time', 'request-id': 'mock-event',
             'event-name': 'Object::Create', 'changeset-id': 'foo'}]
        with self.assertRaisesRegex(RetryEventException,
                                    'Changeset still has Collection Blockers'):
            check_collection_blocker('foo')

    # - test_check_tracking: client error
    @patch('lng_datalake_dal.tracking_table.TrackingTable.query_items')
    def test_check_tracking_client_error(self, tracking_mock_query):
        tracking_mock_query.side_effect = ClientError({'ResponseMetadata': {},
                                                       'Error': {
                                                           'Code': 'OTHER',
                                                           'Message': 'This is a mock'}},
                                                      "FAKE")
        with self.assertRaisesRegex(ClientError, 'This is a mock'):
            check_tracking('foo')

    # - test_check_tracking: general exception
    @patch('lng_datalake_dal.tracking_table.TrackingTable.query_items')
    def test_check_tracking_exception(self, tracking_mock_query):
        tracking_mock_query.side_effect = Exception("Some Error")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to query Tracking Table due to unknown reasons.'):
            check_tracking('foo')

    # - test_check_tracking: existing tracking record
    @patch('lng_datalake_dal.tracking_table.TrackingTable.query_items')
    def test_check_tracking_retry(self, tracking_mock_query):
        tracking_mock_query.return_value = [
            {'tracking-id': '123|xyz|object', 'request-time': 'mock-request-time', 'request-id': 'mock-event',
             'event-name': 'Object::Create', 'changeset-id': 'foo'}]
        with self.assertRaisesRegex(RetryEventException,
                                    'Changeset still has objects in the tracking table'):
            check_tracking('foo')

    # - test_get_collections_from_changeset: client error
    @patch('lng_datalake_dal.changeset_collection_table.ChangesetCollectionTable.query_items')
    def test_get_collections_from_changeset_client_error(self, cct_mock_query):
        cct_mock_query.side_effect = ClientError({'ResponseMetadata': {},
                                                  'Error': {
                                                      'Code': 'OTHER',
                                                      'Message': 'This is a mock'}},
                                                 "FAKE")
        with self.assertRaisesRegex(ClientError, 'This is a mock'):
            get_collections_from_changeset('foo')

    # - test_get_collections_from_changeset: general exception
    @patch('lng_datalake_dal.changeset_collection_table.ChangesetCollectionTable.query_items')
    def test_get_collections_from_changeset_exception(self, cct_mock_query):
        cct_mock_query.side_effect = Exception("Some Error")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to query Changeset Collection Table due to unknown reasons.'):
            get_collections_from_changeset('foo')


if __name__ == '__main__':
    unittest.main()
