import json
import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from cancel_ingestion_event_handler import lambda_handler, cancel_ingestion_event_handler, get_ingestion_item, \
    abort_multipart_upload, remove_collection_blocker, get_collection_blocker_item, delete_collection_blocker_item, \
    cancel_ingestion

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'CancelIngestionEventHandler')


class TestCancelIngestionEventHandler(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        TableCache.clear()

    # +test_cancel_ingestion_event_handler_success
    @patch("cancel_ingestion_event_handler.cancel_ingestion")
    @patch("cancel_ingestion_event_handler.remove_collection_blocker")
    @patch("cancel_ingestion_event_handler.abort_multipart_upload")
    @patch("cancel_ingestion_event_handler.get_ingestion_item")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_event_handler_success(self,
                                                    mock_event_message,
                                                    mock_event_version,
                                                    mock_event_stage,
                                                    mock_get_ingestion_item,
                                                    mock_abort_multipart,
                                                    mock_remove_collection_blocker,
                                                    mock_cancel_ingestion):
        mock_event_message.return_value = True
        mock_event_version.return_value = True
        mock_event_stage.return_value = True
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        mock_get_ingestion_item.return_value = ingestion_item
        mock_abort_multipart.return_value = None
        mock_remove_collection_blocker.return_value = None
        mock_cancel_ingestion.return_value = None

        event = io_utils.load_data_json("cmd_event.valid.json")
        ingestion_id = json.loads(event['Records'][0]['Sns']['Message'])['ingestion-id']
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_ingestion_item.assert_called_once_with(ingestion_id)
        object_key = 'ingestion/LATEST/{}'.format(ingestion_id)
        mock_abort_multipart.assert_called_once_with(ingestion_item['bucket-name'], object_key,
                                                     ingestion_item['upload-id'])
        mock_remove_collection_blocker.assert_called_once_with(ingestion_id)
        ingestion_item['ingestion-state'] = 'Cancelled'
        mock_cancel_ingestion.assert_called_once_with(ingestion_item, 'YoZSKZJKwRIcpawv')

    # +test_cancel_ingestion_event_handler_retry2_success: retry_state = 2
    # Skips abort multipart upload
    @patch("cancel_ingestion_event_handler.cancel_ingestion")
    @patch("cancel_ingestion_event_handler.remove_collection_blocker")
    @patch("cancel_ingestion_event_handler.abort_multipart_upload")
    @patch("cancel_ingestion_event_handler.get_ingestion_item")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_event_handler_retry2_success(self,
                                                           mock_event_message,
                                                           mock_event_version,
                                                           mock_event_stage,
                                                           mock_get_ingestion_item,
                                                           mock_abort_multipart,
                                                           mock_remove_collection_blocker,
                                                           mock_cancel_ingestion):
        mock_event_message.return_value = True
        mock_event_version.return_value = True
        mock_event_stage.return_value = True
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        mock_get_ingestion_item.return_value = ingestion_item
        mock_abort_multipart.return_value = None
        mock_remove_collection_blocker.return_value = None
        mock_cancel_ingestion.return_value = None

        event = io_utils.load_data_json("cmd_event.retry_state_2.valid.json")
        ingestion_id = json.loads(event['Records'][0]['Sns']['Message'])['ingestion-id']
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_ingestion_item.assert_called_once_with(ingestion_id)
        # Skip abort multipart upload
        mock_abort_multipart.assert_not_called()
        mock_remove_collection_blocker.assert_called_once_with(ingestion_id)
        ingestion_item['ingestion-state'] = 'Cancelled'
        mock_cancel_ingestion.assert_called_once_with(ingestion_item, 'YoZSKZJKwRIcpawv')

    # +test_cancel_ingestion_event_handler_retry3_success: retry_state = 3
    # Skips abort multipart upload and remove collection blocker
    @patch("cancel_ingestion_event_handler.cancel_ingestion")
    @patch("cancel_ingestion_event_handler.remove_collection_blocker")
    @patch("cancel_ingestion_event_handler.abort_multipart_upload")
    @patch("cancel_ingestion_event_handler.get_ingestion_item")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_event_handler_retry3_success(self,
                                                           mock_event_message,
                                                           mock_event_version,
                                                           mock_event_stage,
                                                           mock_get_ingestion_item,
                                                           mock_abort_multipart,
                                                           mock_remove_collection_blocker,
                                                           mock_cancel_ingestion):
        mock_event_message.return_value = True
        mock_event_version.return_value = True
        mock_event_stage.return_value = True
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        mock_get_ingestion_item.return_value = ingestion_item
        mock_abort_multipart.return_value = None
        mock_remove_collection_blocker.return_value = None
        mock_cancel_ingestion.return_value = None

        event = io_utils.load_data_json("cmd_event.retry_state_3.valid.json")
        ingestion_id = json.loads(event['Records'][0]['Sns']['Message'])['ingestion-id']
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_ingestion_item.assert_called_once_with(ingestion_id)
        # Skip abort multipart upload and remove collection blocker
        mock_abort_multipart.assert_not_called()
        mock_remove_collection_blocker.assert_not_called()
        ingestion_item['ingestion-state'] = 'Cancelled'
        mock_cancel_ingestion.assert_called_once_with(ingestion_item, 'YoZSKZJKwRIcpawv')

    # -test_cancel_ingestion_event_message: TerminalErrorException
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_event_message(self,
                                            mock_event_message):
        mock_event_message.return_value = False

        event = io_utils.load_data_json("cmd_event.valid.json")
        self.assertRaisesRegex(TerminalErrorException, "Failed to extract event*",
                               cancel_ingestion_event_handler, event, MockLambdaContext().invoked_function_arn)

    # -test_cancel_ingestion_invalid_event_version: TerminalErrorException
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_invalid_event_version(self,
                                                    mock_event_message,
                                                    mock_event_version):
        mock_event_version.return_value = False
        mock_event_message.return_value = True

        event = io_utils.load_data_json("cmd_event.valid.json")
        self.assertRaisesRegex(TerminalErrorException, "Failed to process event*",
                               cancel_ingestion_event_handler, event, MockLambdaContext().invoked_function_arn)

    # -test_cancel_ingestion_invalid_event_stage: TerminalErrorException
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_invalid_event_stage(self,
                                                  mock_event_message,
                                                  mock_event_version,
                                                  mock_event_stage):
        mock_event_version.return_value = True
        mock_event_message.return_value = True
        mock_event_stage.return_value = False

        event = io_utils.load_data_json("cmd_event.valid.json")
        self.assertRaisesRegex(TerminalErrorException, "Failed to process event*",
                               cancel_ingestion_event_handler, event, MockLambdaContext().invoked_function_arn)

    # -test_cancel_ingestion_get_ingestion_item: TerminalErrorException
    @patch("cancel_ingestion_event_handler.get_ingestion_item")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_get_ingestion_item(self,
                                                 mock_event_message,
                                                 mock_event_version,
                                                 mock_event_stage,
                                                 mock_get_ingestion_item):
        mock_event_message.return_value = True
        mock_event_version.return_value = True
        mock_event_stage.return_value = True
        mock_get_ingestion_item.return_value = None

        event = io_utils.load_data_json("cmd_event.valid.json")
        ingestion_id = json.loads(event['Records'][0]['Sns']['Message'])['ingestion-id']
        self.assertRaisesRegex(TerminalErrorException, "Ingestion item not found {0}".format(ingestion_id),
                               cancel_ingestion_event_handler, event, MockLambdaContext().invoked_function_arn)

    # +test_cancel_ingestion_event_handler_cancelled
    @patch("cancel_ingestion_event_handler.cancel_ingestion")
    @patch("cancel_ingestion_event_handler.remove_collection_blocker")
    @patch("cancel_ingestion_event_handler.abort_multipart_upload")
    @patch("cancel_ingestion_event_handler.get_ingestion_item")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_event_handler_cancelled(self,
                                                      mock_event_message,
                                                      mock_event_version,
                                                      mock_event_stage,
                                                      mock_get_ingestion_item,
                                                      mock_abort_multipart,
                                                      mock_remove_collection_blocker,
                                                      mock_cancel_ingestion):
        mock_event_message.return_value = True
        mock_event_version.return_value = True
        mock_event_stage.return_value = True
        ingestion_item = io_utils.load_data_json('ingestionTable.item.cancelled.valid.json')
        mock_get_ingestion_item.return_value = ingestion_item

        event = io_utils.load_data_json("cmd_event.valid.json")
        ingestion_id = json.loads(event['Records'][0]['Sns']['Message'])['ingestion-id']
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_ingestion_item.assert_called_once_with(ingestion_id)
        mock_abort_multipart.assert_not_called()
        mock_remove_collection_blocker.assert_not_called()
        mock_cancel_ingestion.assert_not_called()

    # -test_cancel_ingestion_event_handler_created: TerminalErrorException
    @patch("cancel_ingestion_event_handler.cancel_ingestion")
    @patch("cancel_ingestion_event_handler.remove_collection_blocker")
    @patch("cancel_ingestion_event_handler.abort_multipart_upload")
    @patch("cancel_ingestion_event_handler.get_ingestion_item")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_cancel_ingestion_event_handler_created(self,
                                                    mock_event_message,
                                                    mock_event_version,
                                                    mock_event_stage,
                                                    mock_get_ingestion_item,
                                                    mock_abort_multipart,
                                                    mock_remove_collection_blocker,
                                                    mock_cancel_ingestion):
        mock_event_message.return_value = True
        mock_event_version.return_value = True
        mock_event_stage.return_value = True
        ingestion_item = io_utils.load_data_json('ingestionTable.item.created.valid.json')
        mock_get_ingestion_item.return_value = ingestion_item

        event = io_utils.load_data_json("cmd_event.valid.json")
        ingestion_id = json.loads(event['Records'][0]['Sns']['Message'])['ingestion-id']
        self.assertRaisesRegex(TerminalErrorException, "Failed to process event.*ingestion cannot be cancelled while" \
                                                       " in state Created",
                               cancel_ingestion_event_handler, event, MockLambdaContext().invoked_function_arn)
        mock_get_ingestion_item.assert_called_once_with(ingestion_id)
        mock_abort_multipart.assert_not_called()
        mock_remove_collection_blocker.assert_not_called()
        mock_cancel_ingestion.assert_not_called()

    # +test_get_ingestion_item_success: Returns ingestion item
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_item_success(self, get_ingestion_item_mock):
        ingestion_item = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingestion_id = ingestion_item['ingestion-id']
        get_ingestion_item_mock.return_value = ingestion_item
        self.assertDictEqual(get_ingestion_item(ingestion_id), ingestion_item)
        expected_param = {'ingestion-id': ingestion_id}
        get_ingestion_item_mock.assert_called_once_with(expected_param)

    # -test_get_ingestion_item_endpoint_connection_error_exception: EndpointConnectionError
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_item__endpoint_connection_error_exception(self, get_ingestion_item_mock):
        get_ingestion_item_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            get_ingestion_item(ingestion_id)

    # -test_get_ingestion_item_client_error: ClientError
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_item_client_error(self, get_ingestion_item_mock):
        get_ingestion_item_mock.side_effect = ClientError({}, "client error message")
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(ClientError, 'client error message'):
            get_ingestion_item(ingestion_id)

    # -test_get_ingestion_item_exception_error: Exception->TerminalErrorException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_item_exception_error(self, get_ingestion_item_mock, test_ingestion_id='test-ingestion-id'):
        get_ingestion_item_mock.side_effect = Exception({}, "exception error message")
        ingestion_id = test_ingestion_id
        with self.assertRaisesRegex(TerminalErrorException, 'exception error message'):
            get_ingestion_item(ingestion_id)

    # +test_abort_multipart_upload_success: Returns ingestion item
    @patch('lng_aws_clients.s3.get_client')
    def test_abort_multipart_upload_success(self, s3_client_mock):
        bucket_name = 'test-bucket-name'
        object_key = 'test-object-key'
        upload_id = 'test-upload-id'
        s3_client_mock.return_value.abort_multipart_upload.return_value = None
        self.assertIsNone(abort_multipart_upload(bucket_name, object_key, upload_id))
        s3_client_mock.return_value.abort_multipart_upload.assert_called_once_with(Bucket=bucket_name, Key=object_key,
                                                                                   UploadId=upload_id)

    # -test_abort_multipart_upload_EndpointConnectionError
    @patch('lng_aws_clients.s3.get_client')
    def test_abort_multipart_upload_endpoint_connection_error_exception(self, s3_client_mock):
        bucket_name = 'test-bucket-name'
        object_key = 'test-object-key'
        upload_id = 'test-upload-id'

        s3_client_mock.return_value.abort_multipart_upload.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            abort_multipart_upload(bucket_name, object_key, upload_id)
        s3_client_mock.return_value.abort_multipart_upload.assert_called_once_with(Bucket=bucket_name, Key=object_key,
                                                                                   UploadId=upload_id)

    # -test_abort_multipart_upload_client_error: ClientError
    @patch('lng_aws_clients.s3.get_client')
    def test_abort_multipart_upload_client_error(self, s3_client_mock):
        bucket_name = 'test-bucket-name'
        object_key = 'test-object-key'
        upload_id = 'test-upload-id'
        error_msg = 'test error message'
        s3_client_mock.return_value.abort_multipart_upload.side_effect = ClientError({}, error_msg)
        with self.assertRaisesRegex(ClientError, error_msg):
            abort_multipart_upload(bucket_name, object_key, upload_id)
        s3_client_mock.return_value.abort_multipart_upload.assert_called_once_with(Bucket=bucket_name, Key=object_key,
                                                                                   UploadId=upload_id)

    # -test_abort_multipart_upload_client_error: Exception->TerminalErrorException
    @patch('lng_aws_clients.s3.get_client')
    def test_abort_multipart_upload_exception_error(self, s3_client_mock):
        bucket_name = 'test-bucket-name'
        object_key = 'test-object-key'
        upload_id = 'test-upload-id'
        s3_client_mock.return_value.abort_multipart_upload.side_effect = Exception()
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception occurred:*'):
            abort_multipart_upload(bucket_name, object_key, upload_id)
        s3_client_mock.return_value.abort_multipart_upload.assert_called_once_with(Bucket=bucket_name,
                                                                                   Key=object_key,
                                                                                   UploadId=upload_id)

    # +test_remove_collection_blocker_success
    @patch('cancel_ingestion_event_handler.delete_collection_blocker_item')
    @patch('cancel_ingestion_event_handler.get_collection_blocker_item')
    def test_remove_collection_blocker_success(self, get_collection_blocker_mock, delete_collection_blocker_mock):
        cb_items = [io_utils.load_data_json('collectionBlockerTable.item.valid.json')]
        # Returns one item
        get_collection_blocker_mock.return_value = cb_items
        ingestion_id = 'test-ingestion-id'
        self.assertIsNone(remove_collection_blocker(ingestion_id))
        get_collection_blocker_mock.assert_called_once_with(ingestion_id)
        delete_collection_blocker_mock.assert_called_once_with(cb_items[0])

    # -test_remove_collection_blocker_duplicate_items: TerminalErrorException
    @patch('cancel_ingestion_event_handler.delete_collection_blocker_item')
    @patch('cancel_ingestion_event_handler.get_collection_blocker_item')
    def test_remove_collection_blocker_duplicate_items(self, get_collection_blocker_mock,
                                                       delete_collection_blocker_mock):
        cb_item = io_utils.load_data_json('collectionBlockerTable.item.valid.json')
        # Returns more than one item
        get_collection_blocker_mock.return_value = [cb_item, cb_item]
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(TerminalErrorException, 'Duplicate items found in Collection Blocker Table*'):
            remove_collection_blocker(ingestion_id)
        get_collection_blocker_mock.assert_called_once_with(ingestion_id)
        delete_collection_blocker_mock.assert_not_called()

    # -test_remove_collection_blocker_no_items: TerminalErrorException
    @patch('cancel_ingestion_event_handler.delete_collection_blocker_item')
    @patch('cancel_ingestion_event_handler.get_collection_blocker_item')
    def test_remove_collection_blocker_no_items(self, get_collection_blocker_mock, delete_collection_blocker_mock):
        # Returns zero items.
        get_collection_blocker_mock.return_value = []
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(TerminalErrorException, 'Collection Blocker item not found*'):
            remove_collection_blocker(ingestion_id)
        get_collection_blocker_mock.assert_called_once_with(ingestion_id)
        delete_collection_blocker_mock.assert_not_called()

    # +test_get_collection_blocker_item_success
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_all_items')
    def test_get_collection_blocker_item_success(self, col_block_get_all_items_mock):
        cb_item_list = [io_utils.load_data_json('collectionBlockerTable.item.valid.json')]
        col_block_get_all_items_mock.return_value = cb_item_list
        ingestion_id = 'test-ingestion-id'
        self.assertListEqual(get_collection_blocker_item(ingestion_id), cb_item_list)

    # -test_get_collection_blocker_item_connection_error_exception: EndpointConnectionError
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_all_items')
    def test_get_collection_blocker_item_connection_error_exception(self, col_block_get_all_items_mock):
        col_block_get_all_items_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(EndpointConnectionError, 'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            get_collection_blocker_item(ingestion_id)

    # -test_get_collection_blocker_item_client_error: ClientError
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_all_items')
    def test_get_collection_blocker_item_client_error(self, col_block_get_all_items_mock):
        err_msg = 'client error message'
        col_block_get_all_items_mock.side_effect = ClientError({}, err_msg)
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(ClientError, err_msg):
            get_collection_blocker_item(ingestion_id)

    # -test_get_collection_blocker_item_exception_error: Exception->TerminalErrorException
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_all_items')
    def test_get_collection_blocker_item_exception_error(self, col_block_get_all_items_mock):
        err_msg = 'exception error message'
        col_block_get_all_items_mock.side_effect = Exception(err_msg)
        ingestion_id = 'test-ingestion-id'
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception deleting collection blocker'):
            get_collection_blocker_item(ingestion_id)
        col_block_get_all_items_mock.assert_called_once()

    # +test_delete_collection_blocker_item_success
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_item_success(self, col_block_delete_item_mock):
        cb_item = io_utils.load_data_json('collectionBlockerTable.item.valid.json')
        self.assertIsNone(delete_collection_blocker_item(cb_item))
        col_block_delete_item_mock.assert_called_once()

    # -test_delete_collection_blocker_item_connection_error_exception: EndpointConnectionError
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_item_connection_error_exception(self, col_block_delete_item_mock):
        cb_item = io_utils.load_data_json('collectionBlockerTable.item.valid.json')
        col_block_delete_item_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError, 'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            delete_collection_blocker_item(cb_item)
        col_block_delete_item_mock.assert_called_once()

    # -test_delete_collection_blocker_item_client_error: ClientError
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_item_client_error(self, col_block_delete_item_mock):
        cb_item = io_utils.load_data_json('collectionBlockerTable.item.valid.json')
        err_msg = 'client error message'
        col_block_delete_item_mock.side_effect = ClientError({}, err_msg)
        with self.assertRaisesRegex(ClientError, err_msg):
            delete_collection_blocker_item(cb_item)
        col_block_delete_item_mock.assert_called_once()

    # -test_delete_collection_blocker_item_terminal_error: Exception->TerminalErrorException
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item')
    def test_delete_collection_blocker_item_terminal_error(self, col_block_delete_item_mock):
        cb_item = io_utils.load_data_json('collectionBlockerTable.item.valid.json')
        col_block_delete_item_mock.side_effect = Exception()
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception deleting collection blocker*'):
            delete_collection_blocker_item(cb_item)
        col_block_delete_item_mock.assert_called_once()

    # +test_cancel_ingestion_success
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_cancel_ingestion_success(self, ingest_table_update_item_mock):
        ingest_item_pending = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingest_item_cancelled = io_utils.load_data_json('ingestionTable.item.cancelled.valid.json')
        self.assertDictEqual(cancel_ingestion(ingest_item_pending, 'test_event_2'), ingest_item_cancelled)
        ingest_table_update_item_mock.assert_called_once_with(ingest_item_cancelled)

    # -test_cancel_ingestion_connection_error_exception: EndpointConnectionError
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_cancel_ingestion_endpoint_connection_error_exception(self, ingest_table_update_item_mock):
        ingest_item_pending = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingest_table_update_item_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError, 'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            cancel_ingestion(ingest_item_pending, 'test_event')

    # -test_cancel_ingestion_client_error: ClientError
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_cancel_ingestion_client_error(self, ingest_table_update_item_mock):
        ingest_item_pending = io_utils.load_data_json('ingestionTable.item.valid.json')
        err_msg = 'client error message'
        ingest_table_update_item_mock.side_effect = ClientError({}, err_msg)
        with self.assertRaisesRegex(ClientError, err_msg):
            cancel_ingestion(ingest_item_pending, 'test_event')

    # -test_cancel_ingestion_terminal_error: Exception->TerminalErrorException
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.update_item')
    def test_cancel_ingestion_terminal_error(self, ingest_table_update_item_mock):
        ingest_item_pending = io_utils.load_data_json('ingestionTable.item.valid.json')
        ingest_table_update_item_mock.side_effect = Exception()
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception updating ingestion ID'):
            cancel_ingestion(ingest_item_pending, 'test_event')


if __name__ == '__main__':
    unittest.main()
