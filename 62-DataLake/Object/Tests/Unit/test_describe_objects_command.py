import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError, ParamValidationError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, \
    InvalidRequestPropertyName, SemanticError
from lng_datalake_constants import collection_status, object_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from describe_objects_command import describe_objects_command, lambda_handler, get_collection, \
    get_object_collection_filter, get_object_filter, query_table, get_collection_filter, \
    get_changeset_id_object_filter, get_changeset_id_collection_filter, get_changeset_id_filter, is_valid_changeset_id

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'DescribeObjectsCommand')


class TestDescribeObjectsCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # -Schema validation error using lambda_handler with command_wrapper decorator
    # - request has max-results property that is too large

    def test_command_decorator_fail1(self):
        request_input = io_util.load_data_json('apigateway.request.failure_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    # +Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_decorator_success(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": "2019-01-29T04:04:48.937Z"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": "2019-01-29T04:04:48.937Z",
                    'replicated-buckets': ['bucket2', 'bucket-3']
                }
            ]

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # +Successful test using lambda_handler with command_wrapper decorator object and collection
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_decorator_success_object_collection(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "foobar",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }
            ]

        request_input = io_util.load_data_json('apigateway.request.accepted_2.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # +Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_command_decorator_success_object(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.side_effect = \
            [{
                "collection-id": '2',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2018-12-06T20:33:37.618Z",
                "collection-name": "First ",
                "owner-id": 1,
                "asset-id": 2,
                'old-object-versions-to-keep': 3
            },
                {
                    "collection-id": '274',
                    "collection-state": collection_status.CREATED,
                    "collection-timestamp": "2017-12-06T20:33:37.618Z",
                    "collection-name": "Test Collection",
                    "owner-id": 100,
                    "asset-id": 62,
                    'old-object-versions-to-keep': 0
                }]
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '2',
                    "object-id": "foobar",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",

                    "version-number": 1,
                    "version-timestamp": "2019-01-29T04:04:48.937Z"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "foobar",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": "2019-01-29T04:04:48.937Z"

                }
            ]

        request_input = io_util.load_data_json('apigateway.request.accepted_3.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_3.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # +Successful test of Lambda for collection that is terminated
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_terminated(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.TERMINATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 0,
                    "content-type": "multipart/mixed",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy1",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "d3fffd455df319026d01bc35cc6f8b533854fb2e",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy4",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "dae21321499962cc22f62fc301a884e11635e8e0",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                }
            ]

        request_input = \
            {
                "collection-id": '274'
            }
        expected_response_output = io_util.load_data_json('expected_terminated_response.json')

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda for collection that supports versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_versions(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 0,
                    "content-type": "multipart/mixed",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy1",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "d3fffd455df319026d01bc35cc6f8b533854fb2e",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy4",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "dae21321499962cc22f62fc301a884e11635e8e0",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                }
            ]

        request_input = \
            {
                "collection-id": '274'
            }
        expected_response_output = io_util.load_data_json('expected_response.json')

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda with versions objectid and collectionid
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.query_items')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_version_1(self, ct_get_item_mock, ost_query_items_mock,
                                                        osvt_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "foo",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                }

            ]
        osvt_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "foo",
                    "bucket-name": "ccs-sandbox-datalake-object-store-updated",
                    "object-key": "2d410797c0a7aae166330ed9d37cf6d5ac54444e",
                    "raw-content-length": 80,
                    "version-number": 9,
                    "version-timestamp": "2017-02-07T08:40:59.592312"
                }

            ]

        request_input = \
            {
                "collection-id": '274',
                "object-id": "foo",
                "version-number": 9
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "2d410797c0a7aae166330ed9d37cf6d5ac54444e",
                                "Bucket": "ccs-sandbox-datalake-object-store-updated"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "foo",
                            "object-url": "/objects/LATEST/foo?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 80,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 9,
                            "version-timestamp": "2017-02-07T08:40:59.592312",
                            "object-key-url": "https://datalake_url.com/objects/store/2d410797c0a7aae166330ed9d37cf6d5ac54444e"
                        }
                    ],
                "item-count": 1
            }

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda with versions objectid and collectionid, version doesnt exist
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.query_items')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_version_2(self, ct_get_item_mock, ost_query_items_mock,
                                                        osvt_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "foo",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312"
                }

            ]
        osvt_query_items_mock.return_value = \
            [

            ]

        request_input = \
            {
                "collection-id": '274',
                "object-id": "foo",
                "version-number": 9
            }
        expected_response_output = \
            {
                "objects":
                    [
                    ],
                "item-count": 0
            }

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda with versions objectid and collectionid, object doesnt exist
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.query_items')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_version_3(self, ct_get_item_mock, ost_query_items_mock,
                                                        osvt_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [

            ]
        osvt_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "foo",
                    "bucket-name": "ccs-sandbox-datalake-object-store-updated",
                    "object-key": "2d410797c0a7aae166330ed9d37cf6d5ac54444e",
                    "raw-content-length": 80,
                    "version-number": 9,
                    "version-timestamp": "2017-02-07T08:40:59.592312"
                }

            ]

        request_input = \
            {
                "collection-id": '274',
                "object-id": "foo",
                "version-number": 9
            }
        expected_response_output = \
            {
                "objects":
                    [
                    ],
                "item-count": 0
            }

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_1(self, ct_get_item_mock, ost_query_items_mock, mock_get_request_time):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy4",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.938Z',
                    "pending-expiration-epoch": 1571371200
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 2,
                    "version-timestamp": "2018-02-07T08:40:59.592312",
                    "pending-expiration-epoch": 1571716800
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z',
                    "pending-expiration-epoch": 1571716800
                }
            ]

        mock_get_request_time.return_value = '2019-10-21T18:01:03.781Z'

        request_input = \
            {
                "collection-id": '274'
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy2",
                            "object-url": "/objects/LATEST/dummy2?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 2,
                            "version-timestamp": "2018-02-07T08:40:59.592312",
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                            "object-expiration-date": '2019-10-22T04:00:00.000Z'
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy3",
                            "object-url": "/objects/LATEST/dummy3?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                            "object-expiration-date": '2019-10-22T04:00:00.000Z'
                        }
                    ],
                "item-count": 2
            }

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # Successful test with request that includes changesetid, object_id, collection_id and version_number
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_1(self, cst_get_item_mock, csot_get_item_mock, ct_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        csot_get_item_mock.return_value = \
            {
                "object-state": object_status.CREATED,
                "collection-id": '274',
                'changeset-id': "test-changeset1",
                "object-id": "dummy1",
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                "raw-content-length": 24,
                "content-type": "text/plain",
                "version-number": 1,
                "version-timestamp": '2019-01-29T04:04:48.937Z'
            }
        request_input = io_util.load_data_json('changeset_object_request_1.json')
        response_output = io_util.load_data_json('expected_changeset_response_1.json')
        self.assertEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                         response_output)

    #  Successful test with request that includes changesetid and collection_id
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.query_items')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_2(self, cst_get_item_mock, csot_query_items_mock, ct_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        csot_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "changeset-id": "test-changeset1",
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "changeset-id": "test-changeset1",
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }
            ]
        request_input = io_util.load_data_json('changeset_object_request_2.json')
        response_output = io_util.load_data_json('expected_changeset_response_2.json')
        self.assertEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                         response_output)

    #  Successful test with request that includes only a changesetid
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.query_items')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_3(self, cst_get_item_mock, csot_query_items_mock, ct_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        csot_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    'changeset-id': "test-changeset1",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "changeset-id": "test-changeset1",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }
            ]
        request_input = io_util.load_data_json('changeset_object_request_3.json')
        response_output = io_util.load_data_json('expected_changeset_response_3.json')
        self.assertEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                         response_output)

    #  Successful test with request that includes an expired changesetid
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_4(self, cst_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Expired'
        }
        request_input = io_util.load_data_json('changeset_object_expired_request_1.json')
        response_output = io_util.load_data_json('expected_changeset_response_4.json')
        self.assertEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                         response_output)

    #  Successful test with request that includes an invalid object-id
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_5(self, cst_get_item_mock, csot_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }
        csot_get_item_mock.return_value = {}
        request_input = io_util.load_data_json('changeset_object_request_1.json')
        response_output = io_util.load_data_json('expected_changeset_response_4.json')
        self.assertEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                         response_output)

    # - describe_objects_command - with changesetid, objectid, version number but no collectionid
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_fail_1(self, cst_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }
        request_input = io_util.load_data_json('changeset_object_request_fail_1.json')
        with self.assertRaisesRegex(SemanticError, "Invalid request, missing version-number and/or collection-id"):
            describe_objects_command(request_input, "LATEST")

    # - describe_objects_command - with changesetid, objectid, collectionid but no version_number
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_fail_2(self, cst_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }
        request_input = io_util.load_data_json('changeset_object_request_fail_2.json')
        with self.assertRaisesRegex(SemanticError, "Invalid request, missing version-number and/or collection-id"):
            describe_objects_command(request_input, "LATEST")

    # - describe_objects_command - with changesetid, objectid only
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_fail_3(self, cst_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }
        request_input = io_util.load_data_json('changeset_object_request_fail_3.json')
        with self.assertRaisesRegex(SemanticError, "Invalid request, missing version-number and/or collection-id"):
            describe_objects_command(request_input, "LATEST")

    # - describe_objects_command - with changesetid, version_number only
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_fail_4(self, cst_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }
        request_input = io_util.load_data_json('changeset_object_request_fail_4.json')
        with self.assertRaisesRegex(SemanticError, "Invalid request, missing object-id and/or collection-id"):
            describe_objects_command(request_input, "LATEST")

    # - describe_objects_command - with changesetid, version_number and collection_id only
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_describe_changeset_object_fail_5(self, cst_get_item_mock):
        cst_get_item_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Opened'
        }
        request_input = io_util.load_data_json('changeset_object_request_fail_5.json')
        with self.assertRaisesRegex(SemanticError, "Invalid request, missing object-id and/or collection-id"):
            describe_objects_command(request_input, "LATEST")

    # -Failure test of Lambda - exception when querying ObjectStoreTable
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_fail_1(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.side_effect = ClientError({}, "dummy")

        request_input = \
            {
                "collection-id": '274'
            }

        with self.assertRaisesRegex(InternalError, "Unable to query items from Object Store Table"):
            describe_objects_command(request_input, "LATEST")

    # - describe_objects_command - InternalError - ParamValidationError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_fail_param_validation(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        exception = ParamValidationError(
            report={'report': "Unknown: abc, type: <class 'str'>, valid types: <class 'dict'>"})
        ost_query_items_mock.side_effect = exception

        request_input = \
            {
                "collection-id": '274'
            }

        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            describe_objects_command(request_input, "LATEST")

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_2(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }

            ]

        request_input = \
            {
                "collection-id": '274'
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy2",
                            "object-url": "/objects/LATEST/dummy2?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy3",
                            "object-url": "/objects/LATEST/dummy3?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        }
                    ],
                "item-count": 2
            }

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_3(self, ct_get_item_mock, ost_query_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.REMOVED,
                    "collection-id": '274',
                    "object-id": "dummy34",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }

            ]

        request_input = \
            {
                "collection-id": '274'
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy2",
                            "object-url": "/objects/LATEST/dummy2?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy3",
                            "object-url": "/objects/LATEST/dummy3?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy34",
                            "object-url": "/objects/LATEST/dummy34?collection-id=274",
                            "object-state": object_status.REMOVED,
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z'
                        }
                    ],
                "item-count": 3
            }

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_all_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_4(self, ct_get_item_mock, ost_get_all_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_get_all_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '275',
                    "object-id": "dummy35",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }

            ]

        request_input = \
            {
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy2",
                            "object-url": "/objects/LATEST/dummy2?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy3",
                            "object-url": "/objects/LATEST/dummy3?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '275',
                            "collection-url": "/collections/LATEST/275",
                            "object-id": "dummy35",
                            "object-url": "/objects/LATEST/dummy35?collection-id=275",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        }
                    ],
                "item-count": 3
            }

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # -Failure test of Lambda - exception when getting all items from ObjectStoreTable
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_all_items')
    def test_describe_objects_command_fail_2(self, ost_get_all_items_mock):
        ost_get_all_items_mock.side_effect = ClientError({}, "dummy")

        request_input = \
            {
            }

        with self.assertRaisesRegex(InternalError, "Unable to query items from Object Store Table"):
            describe_objects_command(request_input, "LATEST")

    # - describe_objects_command - InternalError - ParamValidationError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_all_items')
    def test_describe_objects_command_fail_param_fail_3(self, ost_get_all_items_mock):
        exception = ParamValidationError(
            report={'report': "Unknown, value: abc, type: <class 'str'>, valid types: <class 'dict'>"})
        ost_get_all_items_mock.side_effect = exception

        request_input = \
            {
            }

        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            describe_objects_command(request_input, "LATEST")

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_all_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_5(self, ct_get_item_mock, ost_get_all_items_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_get_all_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '275',
                    "object-id": "dummy35",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }

            ]

        request_input = \
            {
                "object-state": "created"
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy2",
                            "object-url": "/objects/LATEST/dummy2?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy3",
                            "object-url": "/objects/LATEST/dummy3?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '275',
                            "collection-url": "/collections/LATEST/275",
                            "object-id": "dummy35",
                            "object-url": "/objects/LATEST/dummy35?collection-id=275",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        }
                    ],
                "item-count": 3
            }

        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda for collection that doesn't support versions
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_all_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_6(self, ct_get_item_mock, ost_get_all_items_mock):
        ct_get_item_mock.side_effect = \
            [{
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }, {
                "collection-id": '275',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection Two",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }, {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }]
        ost_get_all_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '275',
                    "object-id": "dummy3",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.REMOVED,
                    "collection-id": '274',
                    "object-id": "dummy34",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                },
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy35",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }

            ]

        request_input = \
            {
                "object-state": "all"
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy2",
                            "object-url": "/objects/LATEST/dummy2?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '275',
                            "collection-url": "/collections/LATEST/275",
                            "object-id": "dummy3",
                            "object-url": "/objects/LATEST/dummy3?collection-id=275",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        },
                        {
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy34",
                            "object-url": "/objects/LATEST/dummy34?collection-id=274",
                            "object-state": object_status.REMOVED,
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z'
                        },
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy35",
                            "object-url": "/objects/LATEST/dummy35?collection-id=274",
                            "object-state": object_status.CREATED,
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"
                        }
                    ],
                "item-count": 4
            }
        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda with max-results
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_pagination_token')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_max_results(self, ct_get_item_mock, ost_query_items_mock,
                                                          ost_pagination_token_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }
            ]
        ost_pagination_token_mock.return_value = "abcd"

        request_input = \
            {
                "collection-id": '274',
                "max-items": 1
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy2",
                            "object-url": "/objects/LATEST/dummy2?collection-id=274",
                            "object-state": "Created",
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "owner-id": 100,
                            "asset-id": 62,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"

                        }
                    ],
                "next-token": "eyJtYXgtaXRlbXMiOjEsInBhZ2luYXRpb24tdG9rZW4iOiJhYmNkIn0=",
                "item-count": 1
            }
        self.maxDiff = None
        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # +Successful test of Lambda with max-items
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_pagination_token')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_describe_objects_command_success_max_results_2(self, ct_get_item_mock, ost_query_items_mock,
                                                            ost_pagination_token_mock):
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': 0
            }
        ost_query_items_mock.return_value = \
            [
                {
                    "object-state": object_status.CREATED,
                    "collection-id": '274',
                    "object-id": "dummy2",
                    "bucket-name": "ccs-sandbox-datalake-object-store",
                    "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                    "raw-content-length": 24,
                    "content-type": "text/plain",
                    "version-number": 1,
                    "version-timestamp": '2019-01-29T04:04:48.937Z'
                }
            ]
        ost_pagination_token_mock.return_value = "abcde"

        request_input = \
            {
                "collection-id": '274',
                "max-items": 1,
                "next-token": 'eyJtYXgtaXRlbXMiOiAxLCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkIn0='
            }
        expected_response_output = \
            {
                "objects":
                    [
                        {
                            "S3": {
                                "Key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                                "Bucket": "ccs-sandbox-datalake-object-store"
                            },
                            "collection-id": '274',
                            "collection-url": "/collections/LATEST/274",
                            "object-id": "dummy2",
                            "object-url": "/objects/LATEST/dummy2?collection-id=274",
                            "object-state": "Created",
                            "raw-content-length": 24,
                            "content-type": "text/plain",
                            "asset-id": 62,
                            "owner-id": 100,
                            "version-number": 1,
                            "version-timestamp": '2019-01-29T04:04:48.937Z',
                            "object-key-url": "https://datalake_url.com/objects/store/25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d"

                        }
                    ],
                "next-token": "eyJtYXgtaXRlbXMiOjEsInBhZ2luYXRpb24tdG9rZW4iOiJhYmNkZSJ9",
                "item-count": 1
            }
        self.maxDiff = None
        self.assertDictEqual(describe_objects_command(request_input, "LATEST")['response-dict'],
                             expected_response_output)

    # -Failure test of Lambda for invalid max-items

    def test_describe_objects_command_fail_invalid_max_results(self):
        request_input = \
            {
                "collection-id": '274',
                "max-items": 1001

            }

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid max-items value:"):
            describe_objects_command(request_input, "LATEST")

    # -Failure test of Lambda for invalid max-items

    def test_describe_objects_command_fail_invalid_max_results_2(self):
        request_input = \
            {
                "collection-id": '274',
                "max-items": 1,
                "next-token": "eyJtYXgtaXRlbXMiOiAzLCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkIn0=",

            }

        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Continuation request must pass the same max-items as initial request*"):
            describe_objects_command(request_input, "LATEST")

    # -get_collection - Failure test of Lambda for an exception when reading CollectionTable
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_fail(self, ct_get_item_mock):
        ct_get_item_mock.return_value = {}

        ct_get_item_mock.side_effect = ClientError({}, "dummy")

        with self.assertRaisesRegex(InternalError, "Unable to get item from Collection Table"):
            get_collection("12")

    # - get_collection - General Exception
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_exception(self, ct_get_mock):
        ct_get_mock.side_effect = Exception
        with self.assertRaisesRegex(Exception, "Unhandled exception occurred"):
            get_collection("12")

    # - get_collection - Client Error
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_get_collection_client_exception(self, ct_get_mock):
        ct_get_mock.side_effect = ClientError({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unable to get item from Collection Table"):
            get_collection("12")

    # - is_valid_changeset_id ChangesetTable().is_valid_changeset_id - Client Error
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_is_valid_changeset_id_client_exception(self, cst_get_mock):
        cst_get_mock.side_effect = ClientError({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unable to get item from Changeset Table"):
            is_valid_changeset_id("dummyChangeSet")

    # - is_valid_changeset_id ChangesetTable().is_valid_changeset_id - False
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_is_valid_changeset_id_false(self, cst_get_mock):
        cst_get_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Expired'
        }
        self.assertEqual(is_valid_changeset_id("test-changeset1"), False)

    # - is_valid_changeset_id ChangesetTable().is_valid_changeset_id - False
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_is_valid_changeset_id_invalid_id(self, cst_get_mock):
        cst_get_mock.return_value = {}
        self.assertEqual(is_valid_changeset_id("test-changeset1"), False)

    # - is_valid_changeset_id ChangesetTable().is_valid_changeset_id - True
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_is_valid_changeset_id_true(self, cst_get_mock):
        cst_get_mock.return_value = {
            'changeset-id': "test-changeset1",
            'event-id': "123",
            'request-time': "2019-08-31T05:42:12.444Z",
            'changeset-state': 'Created'
        }
        self.assertEqual(is_valid_changeset_id("test-changeset1"), True)

    # - is_valid_changeset_id ChangesetTable().is_valid_changeset_id - UnhandledException
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.item_exists')
    def test_is_valid_changeset_id_exception(self, cst_get_mock):
        cst_get_mock.side_effect = Exception({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            is_valid_changeset_id("dummyChangeSet")

    # - get_changeset_id_object_filter ChangesetObjectTable().query_items - Client Error
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_get_changeset_id_object_filter_client_exception(self, csot_get_mock):
        csot_get_mock.side_effect = ClientError({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unable to get item from Changeset Object Store Table"):
            get_changeset_id_object_filter("dummyChangeSet", "dummyObjectId", 1, "122")

    # - get_changeset_id_object_filter ChangesetObjectTable().query_items - UnhandledException
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.get_item')
    def test_get_changeset_id_object_filter_exception(self, csot_get_mock):
        csot_get_mock.side_effect = Exception({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_changeset_id_object_filter("dummyChangeSet", "dummyObjectId", 1, "122")

    # - get_changeset_id_collection_filter ChangesetObjectTable().query_items - Client Error
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.query_items')
    def test_get_changeset_id_collection_filter_client_exception(self, csot_get_mock):
        csot_get_mock.side_effect = ClientError({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unable to query items from Changeset Object Store Table"):
            get_changeset_id_collection_filter("dummyChangeSet", "dummyCollection", 5, "pageToken_1")

    # - get_changeset_id_collection_filter ChangesetObjectTable().query_items - UnhandledException
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.query_items')
    def test_get_changeset_id_collection_filter_exception(self, csot_get_mock):
        csot_get_mock.side_effect = Exception({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_changeset_id_collection_filter("dummyChangeSet", "dummyCollection", 5, "pageToken_1")

    # - get_changeset_id_filter ChangesetObjectTable().query_items - Client Error
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.query_items')
    def test_get_changeset_id_filter_client_exception(self, csot_get_mock):
        csot_get_mock.side_effect = ClientError({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unable to query items from Changeset Object Store Table"):
            get_changeset_id_filter("dummyChangeSet", 6, "pageToken_1")

    # - get_changeset_id_filter ChangesetObjectTable().query_items - UnhandledException
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.query_items')
    def test_get_changeset_id_filter_exception(self, csot_get_mock):
        csot_get_mock.side_effect = Exception({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_changeset_id_filter("dummyChangeSet", 6, "pageToken_1")

    # +get_object_collection_filter - Valid
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_get_object_collection_filter_valid(self, mock_ost_query):
        resp = [
            {
                "object-state": object_status.CREATED,
                "collection-id": '274',
                "object-id": "dummy2",
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                "raw-content-length": 24,
                "content-type": "text/plain"
            }
        ]
        mock_ost_query.return_value = resp
        self.assertEqual(get_object_collection_filter("obj", "12", None), resp)

    # -get_object_collection_filter - ClientError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_get_object_collection_filter_error(self, mock_ost_query):
        mock_ost_query.side_effect = ClientError({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unable to query items from Object Store Table"):
            get_object_collection_filter("obj", "12", None)

    # -get_object_collection_filter - Exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_get_object_collection_filter_exception(self, mock_ost_query):
        mock_ost_query.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_object_collection_filter("obj", "12", None)

    # +get_object_filter - Valid
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_get_object_filter_valid(self, mock_ost_query):
        resp = [
            {
                "object-state": object_status.CREATED,
                "collection-id": '274',
                "object-id": "dummy2",
                "bucket-name": "ccs-sandbox-datalake-object-store",
                "object-key": "25e4392bbedad369ca7c8cfd33aa0dd6cf19f46d",
                "raw-content-length": 24,
                "content-type": "text/plain"
            }
        ]
        mock_ost_query.return_value = resp

        self.assertEqual(get_object_filter("obj", 10, "12"), resp)

    # -get_object_filter - ClientError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_get_object_filter_exception_client(self, mock_ost_query):
        mock_ost_query.side_effect = ClientError({}, "dummy")
        with self.assertRaisesRegex(InternalError, "Unable to query items from Object Store Table"):
            get_object_filter("obj", 10, "12")

    # -get_object_filter - Exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_get_object_filter_exception(self, mock_ost_query):
        mock_ost_query.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_object_filter("obj", 10, "12")

    # -get_object_filter - ParamValidation Unknown
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_get_object_filter_param_error_unknown(self, mock_ost_query):
        exception = ParamValidationError(
            report={'report': "Unknown: abc, type: <class 'str'>, valid types: <class 'dict'>"})
        mock_ost_query.side_effect = exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_object_filter("obj", 10, "12")

    # -get_collection_filter - Exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_get_collection_filter_exception(self, mock_ost_query):
        mock_ost_query.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_collection_filter("obj", 10, "12")

    # -query_table - Exception
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_query_table_exception(self, mock_ost_query):
        mock_ost_query.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            query_table(10, "12")


if __name__ == '__main__':
    unittest.main()
