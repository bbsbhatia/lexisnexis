import unittest
from unittest.mock import patch, Mock

from botocore.exceptions import ClientError, ReadTimeoutError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_constants import event_names, object_status, ingestion_status
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from process_ingestion_zip_object import create_event_store_item, remove_tracking_item, get_event_name, \
    get_file_offset, decompress_file, copy_object_to_datalake, lambda_handler, process_ingestion_zip_object, \
    remove_tracking_item_if_event_processed, check_tracking_item, get_object_store_item, process_retry_exception, \
    process_terminal_error

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ProcessIngestionZipObject')


class TestProcessIngestionZipObject(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + test lambda_handler success
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('process_ingestion_zip_object.create_event_store_item')
    @patch('process_ingestion_zip_object.get_event_name')
    @patch('process_ingestion_zip_object.copy_object_to_datalake')
    @patch('service_commons.object_common.object_contents_match')
    @patch('process_ingestion_zip_object.get_object_store_item')
    @patch('process_ingestion_zip_object.decompress_file')
    @patch('process_ingestion_zip_object.get_file_offset')
    @patch('process_ingestion_zip_object.check_tracking_item')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_object_success(self, mock_get_ingestion_data, mock_check_tracking_item,
                                                  mock_get_file_offset, mock_decompress, mock_get_object_store_item,
                                                  mock_object_contents_match,
                                                  mock_copy_object_to_datalake,
                                                  mock_get_event_name,
                                                  mock_create_event_store_item, mock_dynamodb_client,
                                                  mock_sqs_client, mock_s3_client):
        ingestion_item = {'ingestion-timestamp': '2018-05-19T19:31:17.000Z',
                          'ingestion-state': ingestion_status.PROCESSING,
                          "ingestion-id": "fake-ingestion-id",
                          'collection-id': 'fake-collection-id',
                          }
        mock_dynamodb_client.return_value = None
        mock_sqs_client.return_value = None
        mock_s3_client.return_value = None
        mock_get_ingestion_data.return_value = ingestion_item
        mock_check_tracking_item.return_value = None
        mock_get_file_offset.return_value = 999
        mock_decompress.return_value = 'abcdefg/11ca41afd0aed57e7c13059a910c57e274f7cf0c', \
                                       'f948e84e719e85fc009cb73c280ad3c7b6fb6382', \
                                       '81a01db36cb75f1ba47434269771a5bcdac6accc', "dataobject"
        mock_get_object_store_item.return_value = {}
        mock_object_contents_match.return_value = False
        mock_copy_object_to_datalake.return_value = None
        mock_get_event_name.return_value = event_names.OBJECT_CREATE
        mock_create_event_store_item.return_value = True
        self.assertEqual(lambda_handler(io_util.load_data_json('valid_event.json'), MockLambdaContext()),
                         event_handler_status.SUCCESS)
        self.assertEqual(mock_copy_object_to_datalake.call_count, 2)

    # - lambda_handler: not current tracking - puts item back on SQS and returns success
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('process_ingestion_zip_object.check_tracking_item')
    @patch('service_commons.ingestion.get_ingestion_data')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_process_ingestion_zip_object_retry(self, mock_dynamodb_client, mock_get_ingestion_data, mock_check_tracking_item,
                                                mock_sqs_client, mock_s3_client):
        ingestion_item = {'ingestion-timestamp': '2018-05-19T19:31:17.000Z',
                          'ingestion-state': ingestion_status.PROCESSING,
                          "ingestion-id": "fake-ingestion-id",
                          'collection-id': 'fake-collection-id',
                          }
        mock_dynamodb_client.return_value = None
        mock_s3_client.return_value = None
        mock_get_ingestion_data.return_value = ingestion_item
        mock_check_tracking_item.side_effect = RetryEventException("test")
        mock_sqs_client.return_value.send_message.return_value = None
        self.assertEqual(lambda_handler(io_util.load_data_json('valid_event.json'), MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_sqs_client.return_value.send_message.assert_called_once()

    # - lambda_handler: not current tracking and seen-count=1000
    #   - doesn't put item back on SQS, calls error_handler.terminal_error() and returns success
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('process_ingestion_zip_object.check_tracking_item')
    @patch('service_commons.ingestion.get_ingestion_data')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_process_ingestion_zip_object_terminal(self, mock_dynamodb_client, mock_get_ingestion_data, mock_check_tracking_item,
                                                   mock_sqs_client, mock_s3_client, mock_error_handler_terminal_error):
        ingestion_item = {'ingestion-timestamp': '2018-05-19T19:31:17.000Z',
                          'ingestion-state': ingestion_status.PROCESSING,
                          "ingestion-id": "fake-ingestion-id",
                          'collection-id': 'fake-collection-id',
                          }
        mock_dynamodb_client.return_value = None
        mock_s3_client.return_value = None
        mock_get_ingestion_data.return_value = ingestion_item
        mock_check_tracking_item.side_effect = RetryEventException("test")
        mock_sqs_client.return_value.send_message.return_value = None
        mock_error_handler_terminal_error.return_value = None
        self.assertEqual(lambda_handler(io_util.load_data_json('valid_event_with_seen_count1000.json'),
                                        MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_sqs_client.return_value.send_message.assert_called_with(MessageBody='{"fake-ingestion-id":{"ObjectErrorCount":1}}', QueueUrl=None)
        mock_error_handler_terminal_error.assert_called_once()

    # - test process_ingestion_zip_object failure: invalid state
    @patch('service_commons.ingestion.update_error_counter')
    @patch('service_commons.ingestion.get_ingestion_data')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_process_ingestion_zip_object_fail_state_error(self, mock_dynamodb_client, mock_sqs_client, mock_s3_client ,mock_get_ingestion_data, mock_update_error_counter):
        ingestion_item = {'ingestion-timestamp': '2018-05-19T19:31:17.000Z',
                          'ingestion-state': ingestion_status.ERROR,
                          "ingestion-id": "fake-ingestion-id",
                          'collection-id': 'fake-collection-id',
                          }
        mock_dynamodb_client.return_value = None
        mock_sqs_client.return_value = None
        mock_s3_client.return_value = None
        mock_get_ingestion_data.return_value = ingestion_item
        mock_update_error_counter.return_value = None
        self.assertEqual(process_ingestion_zip_object(io_util.load_data_json('valid_event.json'), MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_update_error_counter.assert_called_once()

    # - test process_ingestion_zip_object fail: key error, no object-id
    def test_process_ingestion_zip_object_key_error(self):
        self.assertEqual(process_ingestion_zip_object(io_util.load_data_json('invalid_event_missing_key.json'),
                                                      MockLambdaContext()),
                         event_handler_status.SUCCESS)

    # + successful check_tracking_items with event id matching oldest tracking item
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_success(self, mock_get_oldest_tt_item):
        mock_get_oldest_tt_item.return_value = {'tracking-id': 'object-id.fake_object', 'event-id': 'fake_event_id_1',
                                                'request-time': '2018-05-19T19:31:17.000Z',
                                                'event-name': event_names.OBJECT_CREATE}

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_1"
        self.assertIsNone(check_tracking_item(object_id, collection_id, event_id))

    # - fail check_tracking_items with event id not matching oldest tracking item
    @patch('lng_datalake_commons.tracking.tracker._is_oldest_event')
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_fail(self, mock_get_oldest_tt_item, mock_is_oldest_event):
        mock_is_oldest_event.return_value = False
        mock_get_oldest_tt_item.return_value = {'tracking-id': 'object-id.fake_object', 'event-id': 'fake_event_id_1',
                                                'request-time': '2018-05-19T19:31:17.000Z',
                                                'event-name': event_names.OBJECT_CREATE}

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_2"
        with self.assertRaisesRegex(RetryEventException, 'Oldest Tracking item'):
            check_tracking_item(object_id, collection_id, event_id)

    # - fail check_tracking_items: ClientError on _get_oldest_item_by_tracking_id()
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_fail2(self, mock_get_oldest_tt_item):
        mock_get_oldest_tt_item.side_effect = ClientError({"Error": {"Code": 101,
                                                                     "Message": "Mock Client Error"}},
                                                          "MockError")

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_2"
        with self.assertRaisesRegex(RetryEventException, 'Unable to query items in Tracking Table'):
            check_tracking_item(object_id, collection_id, event_id)

    # - fail check_tracking_items: TerminalError on _get_oldest_item_by_tracking_id()
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_fail3(self, mock_get_oldest_tt_item):
        mock_get_oldest_tt_item.side_effect = TerminalErrorException("Unable to query items in Tracking Table")

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_2"
        with self.assertRaisesRegex(TerminalErrorException, 'Unable to query items in Tracking Table'):
            check_tracking_item(object_id, collection_id, event_id)

    # - success check_tracking_items with no tracking items for object
    @patch('lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id')
    def test_check_tracking_item_no_tracking(self, mock_get_oldest_tt_item):
        mock_get_oldest_tt_item.return_value = {}

        object_id = 'object-id.fake_object'
        collection_id = 'fake-collection'
        event_id = "fake_event_id_2"
        self.assertIsNone(check_tracking_item(object_id, collection_id, event_id))

    # + successful get_file_offset: header offset
    @patch('lng_aws_clients.s3.get_client')
    def test_get_file_offset(self, mock_s3_client):
        body_mock = Mock()
        body_mock.read.return_value = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00testPK\x01\x02xxxxxxxxxxxxxxxxx'
        mock_s3_client.return_value.get_object.return_value = {'Body': body_mock}
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        header_offset = 5000000000
        self.assertEqual(get_file_offset(bucket_name, zip_key, header_offset), 5000000034)

    # - unsuccessful get_file_offset: EndpointConnectionError
    @patch('lng_aws_clients.s3.get_client')
    def test_get_file_offset_endpoint_connection_error_exception(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        header_offset = 5000000000
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object'):
            get_file_offset(bucket_name, zip_key, header_offset)

    # - unsuccessful get_file_offset: ClientError
    @patch('lng_aws_clients.s3.get_client')
    def test_get_file_offset_client_error(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        header_offset = 5000000000
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object'):
            get_file_offset(bucket_name, zip_key, header_offset)

    # - unsuccessful get_file_offset: ClientError (Not Found)
    @patch('lng_aws_clients.s3.get_client')
    def test_get_file_offset_not_found(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = ClientError(
            {"Error": {"Code": "NoSuchKey", "Message": "Not Found"}}, "MockError")
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        header_offset = 5000000000
        with self.assertRaisesRegex(TerminalErrorException, 'S3 object {} not found in bucket'.format(zip_key)):
            get_file_offset(bucket_name, zip_key, header_offset)

    # + successful decompress_file: with compressed object
    @patch('lng_aws_clients.s3.get_client')
    def test_decompress_file_compressed_file(self, mock_s3_client):
        mock_s3_object = {'Body': MockByteIterable(True)}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        object_id = 'SampleEleEle1.xml'
        file_offset = 47
        file_compressed_size = 108
        file_compression_type = 8
        file_crc = 3148000754
        object_properties = {"object-id": object_id,
                             "content-type": "text/plain",
                             "version-timestamp": "2018-05-19T19:31:17.000Z",
                             "collection-hash": "abcdefg"}
        object_key, content_sha1, object_hash, _ = decompress_file(bucket_name, zip_key, object_properties,
                                                                   file_offset, file_compressed_size,
                                                                   file_compression_type, file_crc)
        self.assertEqual(object_key, 'abcdefg/11ca41afd0aed57e7c13059a910c57e274f7cf0c')
        self.assertEqual(content_sha1, 'f948e84e719e85fc009cb73c280ad3c7b6fb6382')
        self.assertEqual(object_hash, '81a01db36cb75f1ba47434269771a5bcdac6accc')

    # - unsuccessful decompress_file: EndpointConnectionError
    @patch('lng_aws_clients.s3.get_client')
    def test_decompress__endpoint_connection_error_exception(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        object_id = 'SampleEleEle1.xml'
        file_offset = 47
        file_compressed_size = 108
        file_compression_type = 8
        file_crc = 3148000754
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object'):
            decompress_file(bucket_name, zip_key, object_id, file_offset, file_compressed_size, file_compression_type,
                            file_crc)

    # - unsuccessful decompress_file: ClientError
    @patch('lng_aws_clients.s3.get_client')
    def test_decompress_client_error(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        object_id = 'SampleEleEle1.xml'
        file_offset = 47
        file_compressed_size = 108
        file_compression_type = 8
        file_crc = 3148000754
        object_properties = {"object-id": object_id,
                             "content-type": "text/plain",
                             "version-timestamp": "2018-05-19T19:31:17.000Z",
                             "collection-hash": "abcdefg"}
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object'):
            decompress_file(bucket_name, zip_key, object_properties, file_offset, file_compressed_size,
                            file_compression_type, file_crc)

    # - unsuccessful decompress_file: ClientError (Not Found)
    @patch('lng_aws_clients.s3.get_client')
    def test_decompress_not_found(self, mock_s3_client):
        mock_s3_client.return_value.get_object.side_effect = ClientError(
            {"Error": {"Code": "NoSuchKey", "Message": "Not Found"}}, "MockError")
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        object_id = 'SampleEleEle1.xml'
        file_offset = 47
        file_compressed_size = 108
        file_compression_type = 8
        file_crc = 3148000754
        object_properties = {"object-id": object_id,
                             "content-type": "text/plain",
                             "version-timestamp": "2018-05-19T19:31:17.000Z",
                             "collection-hash": "abcdefg"}
        with self.assertRaisesRegex(TerminalErrorException, 'S3 object {} not found in bucket'.format(zip_key)):
            decompress_file(bucket_name, zip_key, object_properties, file_offset, file_compressed_size,
                            file_compression_type, file_crc)

    # - unsuccessful decompress_file: exception.ReadTimeoutError -> RetryEventException
    @patch('lng_aws_clients.s3.get_client')
    def test_decompress_read_timeout_error(self, mock_s3_client):
        mock_s3_object = {'Body': MockByteIterable(True, generate_exception=True)}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        object_id = 'SampleEleEle1.xml'
        file_offset = 47
        file_compressed_size = 108
        file_compression_type = 8
        file_crc = 3148000754
        object_properties = {"object-id": object_id,
                             "content-type": "text/plain",
                             "version-timestamp": "2018-05-19T19:31:17.000Z",
                             "collection-hash": "abcdefg"}
        with self.assertRaisesRegex(RetryEventException, 'Unable to read S3 object {0}'.format(zip_key)):
            decompress_file(bucket_name, zip_key, object_properties, file_offset, file_compressed_size,
                            file_compression_type, file_crc)

    # - unsuccessful decompress_file: error
    @patch('lng_aws_clients.s3.get_client')
    def test_decompress_file_compression_error(self, mock_s3_client):
        mock_s3_object = {'Body': MockByteIterable(False)}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object
        mock_s3_client.return_value.upload_fileobj.return_value = None
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        object_id = 'SampleEleEle1.xml'
        file_offset = 47
        file_compressed_size = 108
        file_compression_type = 8
        file_crc = 3148000754
        object_properties = {"object-id": object_id,
                             "content-type": "text/plain",
                             "version-timestamp": "2018-05-19T19:31:17.000Z",
                             "collection-hash": "abcdefg"}
        with self.assertRaisesRegex(TerminalErrorException, 'Decompress error on file'):
            decompress_file(bucket_name, zip_key, object_properties, file_offset, file_compressed_size,
                            file_compression_type, file_crc)

    # - unsuccessful decompress_file: bad crc
    @patch('lng_aws_clients.s3.get_client')
    def test_decompress_file_compression_error_bad_crc(self, mock_s3_client):
        mock_s3_object = {'Body': MockByteIterable(True)}
        mock_s3_client.return_value.get_object.return_value = mock_s3_object
        mock_s3_client.return_value.upload_fileobj.return_value = None
        bucket_name = 'fake-bucket-name'
        zip_key = 'zip_key.zip'
        object_id = 'SampleEleEle1.xml'
        file_offset = 47
        file_compressed_size = 108
        file_compression_type = 1
        file_crc = 314800075
        object_properties = {"object-id": object_id,
                             "content-type": "text/plain",
                             "version-timestamp": "2018-05-19T19:31:17.000Z",
                             "collection-hash": "abcdefg"}
        with self.assertRaisesRegex(TerminalErrorException, 'CRC error on file'):
            decompress_file(bucket_name, zip_key, object_properties, file_offset, file_compressed_size,
                            file_compression_type, file_crc)

    # + successful copy_object_to_datalake
    @patch('lng_aws_clients.s3.get_client')
    @patch('service_commons.ingestion.is_object_in_datalake')
    def test_copy_object_to_datalake_success(self, mock_is_object_in_datalake, mock_s3_client):
        mock_is_object_in_datalake.return_value = False
        mock_s3_client.return_value.upload_fileobj.return_value = None
        object_properties = {
            'content-type': "text/plain",
            'collection-id': "123",
            'object-id': "object",
            'version-timestamp': "2018-05-19T19:31:17.000Z",
            'version-number': 1
        }
        self.assertIsNone(copy_object_to_datalake("input", "abc", object_properties, "abcdefg"))

    # - unsuccessful copy_object_to_datalake: upload EndpointConnectionError
    @patch('lng_aws_clients.s3.get_client')
    @patch('service_commons.ingestion.is_object_in_datalake')
    def test_copy_object_to_datalake_endpoint_connection_error_exception(self, mock_is_object_in_datalake,
                                                                         mock_s3_client):
        mock_is_object_in_datalake.return_value = False
        mock_s3_client.return_value.put_object.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        object_properties = {
            'content-type': "text/plain",
            'collection-id': "123",
            'object-id': "object",
            'version-timestamp': "2018-05-19T19:31:17.000Z",
            'version-number': 1
        }
        with self.assertRaisesRegex(RetryEventException, 'Unable to create S3 object'):
            copy_object_to_datalake("input", "abc", object_properties, "abcdefg")

    # - unsuccessful copy_object_to_datalake: upload client error
    @patch('lng_aws_clients.s3.get_client')
    @patch('service_commons.ingestion.is_object_in_datalake')
    def test_copy_object_to_datalake_client_error(self, mock_is_object_in_datalake, mock_s3_client):
        mock_is_object_in_datalake.return_value = False
        mock_s3_client.return_value.put_object.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        object_properties = {
            'content-type': "text/plain",
            'collection-id': "123",
            'object-id': "object",
            'version-timestamp': "2018-05-19T19:31:17.000Z",
            'version-number': 1
        }
        with self.assertRaisesRegex(RetryEventException, 'Unable to create S3 object'):
            copy_object_to_datalake("input", "abc", object_properties, "abcdefg")

    # - unsuccessful copy_object_to_datalake: upload general error
    @patch('lng_aws_clients.s3.get_client')
    @patch('service_commons.ingestion.is_object_in_datalake')
    def test_copy_object_to_datalake_general_error(self, mock_is_object_in_datalake, mock_s3_client):
        mock_is_object_in_datalake.return_value = False
        mock_s3_client.return_value.put_object.side_effect = Exception("Mock Exception")
        object_properties = {
            'content-type': "text/plain",
            'collection-id': "123",
            'object-id': "object",
            'version-timestamp': "2018-05-19T19:31:17.000Z",
            'version-number': 1
        }
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to create S3 object'):
            copy_object_to_datalake("input", "abc", object_properties, "abcdefg")

    # + successful get_event_name with object store item: update
    def test_get_event_name_success_object_store(self):
        ost_item = {'object-id': 'object-id.fake', 'object-state': object_status.CREATED}
        self.assertEqual(get_event_name(ost_item), event_names.OBJECT_UPDATE)

    # + successful get_event_name with no object store item: create
    def test_get_event_name_success_new_tracking_items(self):
        ost_item = {}
        self.assertEqual(get_event_name(ost_item), event_names.OBJECT_CREATE)

    # - unsuccessful get_object_store_item: EndpointConnectionError
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_store_item_endpoint_connection_error_exception(self, ost_mock_get):
        ost_mock_get.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        object_id = 'object-id.fake'
        collection_id = 'fake-collection'
        with self.assertRaisesRegex(RetryEventException, 'Unable to get item from ObjectStore Table'):
            get_object_store_item(object_id, collection_id)

    # - unsuccessful get_object_store_item: Client Error
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_store_item_client_error(self, ost_mock_get):
        ost_mock_get.side_effect = ClientError({"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        object_id = 'object-id.fake'
        collection_id = 'fake-collection'
        with self.assertRaisesRegex(RetryEventException, 'Unable to get item from ObjectStore Table'):
            get_object_store_item(object_id, collection_id)

    # - unsuccessful get_object_store_item: General Error
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_get_object_store_item_general_exception(self, ost_mock_get):
        ost_mock_get.side_effect = Exception("Mock Exception")
        object_id = 'object-id.fake'
        collection_id = 'fake-collection'
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to get item from ObjectStore Table'):
            get_object_store_item(object_id, collection_id)

    # + successful create_event_store_item
    @patch('lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item')
    def test_create_event_store_item_success(self, est_mock):
        event_dict = {"event-id": "fake_event_id"}
        est_mock.return_value = None
        self.assertTrue(create_event_store_item(event_dict))
        est_mock.assert_called_with(event_dict)

    # - unsuccessful create_event_store_item: condition error
    @patch('lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item')
    def test_create_event_store_item_condition_error(self, est_mock):
        event_dict = {"event-id": "fake_event_id"}
        est_mock.side_effect = ConditionError({"Mock Condition Error"})
        self.assertFalse(create_event_store_item(event_dict))
        est_mock.assert_called_with(event_dict)

    # - unsuccessful create_event_store_item: EndpointConnectionError
    @patch('lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item')
    def test_create_event_store_item_endpoint_connection_error_exception(self, est_mock):
        event_dict = {"event-id": "fake_event_id"}
        est_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(RetryEventException,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            create_event_store_item(event_dict)

    # - unsuccessful create_event_store_item: client error
    @patch('lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item')
    def test_create_event_store_item_client_error(self, est_mock):
        event_dict = {"event-id": "fake_event_id"}
        est_mock.side_effect = ClientError({"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        with self.assertRaisesRegex(RetryEventException, 'Mock Client Error'):
            create_event_store_item(event_dict)

    # - unsuccessful create_event_store_item: general error
    @patch('lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item')
    def test_create_event_store_item_general_error(self, est_mock):
        event_dict = {"event-id": "fake_event_id"}
        est_mock.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(TerminalErrorException, 'Mock Exception'):
            create_event_store_item(event_dict)

    # + successful remove_tracking_item
    @patch('lng_datalake_dal.tracking_table.TrackingTable.delete_item')
    def test_remove_tracking_item_success(self, tt_mock_delete):
        tt_mock_delete.return_value = None
        event_id = 'fake_event_id'
        tracking_id = 'object-id.fake|abc_object'
        request_time = '2018-05-19T19:31:17.000Z'
        remove_tracking_item(tracking_id, request_time, event_id, "reason")
        tt_mock_delete.assert_called_with({"tracking-id": tracking_id,
                                           "request-time": request_time})

    # - unsuccessful remove_tracking_item: general exception
    @patch('lng_datalake_dal.tracking_table.TrackingTable.delete_item')
    def test_remove_tracking_item_general_error(self, tt_mock_delete):
        tt_mock_delete.side_effect = Exception("Mock Exception")
        event_id = 'fake_event_id'
        tracking_id = 'object-id.fake|abc_object'
        request_time = '2018-05-19T19:31:17.000Z'
        remove_tracking_item(tracking_id, request_time, event_id, "reason")
        tt_mock_delete.assert_called_with({"tracking-id": tracking_id,
                                           "request-time": request_time})

    # + successful remove_tracking_item_if_event_processed: no ObjectStoreTable item
    @patch('process_ingestion_zip_object.remove_tracking_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_remove_tracking_item_if_event_processed_success1(self, ost_mock_get, remove_tracking_item_mock):
        ost_mock_get.return_value = {}
        remove_tracking_item_mock.return_value = None
        object_id = 'object-id.fake'
        collection_id = 'abc'
        version_timestamp = '2018-05-19T19:31:17.000Z'
        event_id = "fake_event_id"
        self.assertIsNone(
            remove_tracking_item_if_event_processed(object_id, collection_id, version_timestamp, event_id))
        remove_tracking_item_mock.assert_not_called()

    # + successful remove_tracking_item_if_event_processed: ObjectStoreTable item with version-timestamp>request_time
    @patch('process_ingestion_zip_object.remove_tracking_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_remove_tracking_item_if_event_processed_success2(self, ost_mock_get, remove_tracking_item_mock):
        ost_mock_get.return_value = {
            "object-state": object_status.CREATED,
            "object-id": "49cf62814f629ba0a679147d8784b516cde30a17",
            "collection-id": '274',
            "bucket-name": "ccs-sandbox-datalake-object-store",
            "object-key": "49cf62814f629ba0a679147d8784b516cde30a17",
            "raw-content-length": 24,
            "content-type": "text/plain",
            "version-timestamp": "2018-05-24T15:00:00.000Z"
        }
        remove_tracking_item_mock.return_value = None
        object_id = 'object-id.fake'
        collection_id = 'abc'
        version_timestamp = '2018-05-19T19:31:17.000Z'
        event_id = "fake_event_id"
        self.assertIsNone(
            remove_tracking_item_if_event_processed(object_id, collection_id, version_timestamp, event_id))
        remove_tracking_item_mock.assert_called_once()

    # + successful process_retry_exception: no seen-count in messageAttributes
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_success1(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None

        self.assertIsNone(process_retry_exception(RetryEventException(""),
                                                  io_util.load_data_json('valid_event.json')))

    # + successful process_retry_exception: with seen-count in messageAttributes
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_success2(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None

        self.assertIsNone(process_retry_exception(RetryEventException(""),
                                                  io_util.load_data_json('valid_event_with_seen_count.json')))

    # - fail process_retry_exception: with seen-count=1000 in messageAttributes
    def test_process_retry_exception_fail1(self):
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Event exceeded max retry count of 1000\|\|RetryEventException\|\|some issue'):
            process_retry_exception(RetryEventException("some issue"),
                                    io_util.load_data_json('valid_event_with_seen_count1000.json'))

    # - fail process_retry_exception: sqs send_message ClientError raises original RetryEventException
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_fail2(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.side_effect = ClientError({"Error": {"Code": 101,
                                                                                       "Message": "Mock Client Error"}},
                                                                            "MockError")
        with self.assertRaisesRegex(RetryEventException, "some issue"):
            process_retry_exception(RetryEventException("some issue"), io_util.load_data_json('valid_event.json'))

    # + successful process_terminal_error
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_process_terminal_error_success(self, mock_error_handler_terminal_error):
        mock_error_handler_terminal_error.return_value = None

        self.assertIsNone(process_terminal_error(RetryEventException(""),
                                                 io_util.load_data_json('valid_event.json'),
                                                 MockLambdaContext(),
                                                 "123456789", {}))
        mock_error_handler_terminal_error.assert_called_once()


class MockByteIterable:
    def __init__(self, use_bytes, generate_exception=False):
        if use_bytes:
            self.sample_bytes = b'\xb3\xb1\xaf\xc8\xcdQ(K-*\xce\xcc\xcf\xb3U2\xd43PRH\xcdK\xceO\xc9\xccK\xb7U\n\rq\xd3\xb5P\xb2\xb7\xe3\xe5\xb2\tN\xcc-\xc8I\x05\xb2\x14\x14l\\sR\x81H!/17\xd5V)(1/%\x1f\xc8U\x02\xcb\x01e\x9332sR|\xf3\x8bK\xa0\n\xc0|C%\x05}\x88f}\x88n\xb2M2\xc2b\x92\x8d>\xd4y\x00P'
        else:
            self.sample_bytes = 123
        self.generate_exception = generate_exception

    def iter_chunks(self, bytes):
        if self.generate_exception:
            raise ReadTimeoutError(endpoint_url='mock_s3_url.com')
        keep_going = True
        while keep_going:
            current_chunk = self.sample_bytes
            keep_going = False
            yield current_chunk


if __name__ == '__main__':
    unittest.main()
