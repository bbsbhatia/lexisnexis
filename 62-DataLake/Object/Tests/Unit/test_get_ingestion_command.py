import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyName, InternalError, NoSuchIngestion
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import get_ingestion_command as get_ingestion_module
from get_ingestion_command import lambda_handler, get_ingestion_command, generate_response_json

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'GetIngestionCommand')


class TestCreateIngestionCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(get_ingestion_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # - Schema validation error using lambda_handler with command_wrapper decorator
    # - request missing ingestion-id required property
    def test_command_schema_validation_fail(self):
        request_input = io_utils.load_data_json('apigateway.request.failure.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InvalidRequestPropertyName, r"data must contain \['ingestion-id'\] properties"):
                lambda_handler(request_input, MockLambdaContext())

    # + Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_lambda_handler_success(self, get_ingestion_mock):
        get_ingestion_mock.return_value = io_utils.load_data_json('valid_ingestion_item.json')
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        response_output = io_utils.load_data_json('apigateway.response.accepted.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(response_output, lambda_handler(request_input, MockLambdaContext()))

    # + create_ingestion_command - success
    @patch('get_ingestion_command.generate_response_json')
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_create_ingestion_command_success(self, get_ingestion_mock, generate_response_mock):
        get_ingestion_mock.return_value = io_utils.load_data_json('valid_ingestion_item.json')
        response_json = io_utils.load_data_json('apigateway.response.accepted.json')["response"]
        generate_response_mock.return_value = response_json
        request_input = {"ingestion-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb"}
        response_output = {"response-dict": response_json}
        self.assertEqual(response_output, get_ingestion_command(request_input, MockLambdaContext()))

    # - get_ingestion_command - ClientError occurred when getting item from Ingestion Table
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_command_client_error(self, get_ingestion_mock):
        get_ingestion_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {
                                                          'Code': 'OTHER',
                                                          'Message': 'This is a mock'}},
                                                     "FAKE")
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        with self.assertRaisesRegex(InternalError, "Unable to get item from Ingestion Table"):
            get_ingestion_command(request_input['request'], stage='LATEST')

    # - get_ingestion_command - Unhandled exception occurred when getting item from Ingestion Table
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_command_general_exception(self, get_ingestion_mock):
        get_ingestion_mock.side_effect = Exception
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_ingestion_command(request_input['request'], stage='LATEST')

    # - get_ingestion_command - no ingestion item found
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_command_not_existing_ingestion(self, get_ingestion_mock):
        get_ingestion_mock.return_value = None
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        ingestion_id = request_input['request']['ingestion-id']
        with self.assertRaisesRegex(NoSuchIngestion, "Invalid Ingestion ID {0}".format(ingestion_id)):
            get_ingestion_command(request_input['request'], stage='LATEST')

    # + generate_response_json - success
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_success(self, get_required_schema_mock, get_optional_schema_mock):
        ingestion_data = {"ingestion-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb",
                          "collection-id": "collection-id-test",
                          "ingestion-state": "Completed",
                          "description": "Ingestion description for test",
                          "pending-expiration-epoch": 1565985041}
        get_required_schema_mock.return_value = ["ingestion-id", "collection-id", "ingestion-state",
                                                 "collection-url", "ingestion-url"]
        get_optional_schema_mock.return_value = ["description"]
        expected_response = {"ingestion-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb",
                             "collection-id": "collection-id-test",
                             "ingestion-state": "Completed",
                             "description": "Ingestion description for test",
                             "collection-url": "/collections/LATEST/collection-id-test",
                             "ingestion-url": "/objects/LATEST/ingestion/0083e8d8-54d6-11e9-89a1-33281b9325bb"}

        self.assertEqual(expected_response, generate_response_json(ingestion_data, stage='LATEST'))

    # - generate_response_json - missing required property
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_missing_required_property(self, get_required_schema_mock):
        ingestion_data = {"ingestion-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb",
                          "collection-id": "collection-id-test",
                          "ingestion-state": "Completed"}
        get_required_schema_mock.return_value = ["ingestion-id", "collection-id", "ingestion-state",
                                                 "ingestion-timestamp"]
        with self.assertRaisesRegex(InternalError, "Missing required property:"):
            generate_response_json(ingestion_data, stage='LATEST')


if __name__ == '__main__':
    unittest.main()
