import datetime
import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NotAllowedError
from lng_datalake_constants import event_names
from lng_datalake_constants import object_status
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from create_folder_upload_command import lambda_handler, generate_object_response, process_event

__author__ = "John Konderla, Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateFolderUploadCommand')


class TestCreateFolderUploadCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.maxDiff = None

    # +lambda_handler - valid
    @patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'})
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('create_folder_upload_command.process_event')
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_aws_clients.session.set_session')
    def test_create_folder_upload_command(self, session_mock, mock_owner_authorization, mock_dynamodb_get_client,
                                          mock_process_event, mock_changeset_get_item):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamodb.collection_data_valid.json')
            elif TableName == 'fake_objectstore_table':
                return io_util.load_data_json('dynamodb.objectstore_data_valid.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner_data_valid.json')
            else:
                return None

        mock_owner_authorization.return_value = {}

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        credentials = {
            "AccessKeyId": "AccessKeyIdVal",
            "SecretAccessKey": "SecretAccessKeyVal",
            "SessionToken": "SessionTokenVal",
            "Expiration": "2018-08-23T00:33:27.000Z"}

        mock_process_event.return_value = 'po-IZv-QUSLIm-mmABa', \
                                          'folder-upload/test-collection_po-IZv-QUSLIm-mmABa_1010101', credentials

        mock_changeset_get_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }

        # The mock-outs
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_valid.json')

        with patch('create_folder_upload_command.staging_bucket_name', 'bucket_name'):
            with patch('create_folder_upload_command.staging_bucket_arn', 'bucket_arn'):
                with patch('create_folder_upload_command.assume_role_arn', 'test_ARN'):
                    with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                        self.assertEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                                         expected_response)

    # - test_test_generate_response_json: Missing required key
    def test_generate_response_json_no_collection_id(self):
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertRaisesRegex(InternalError, "Missing required property",
                                   generate_object_response, {}, "object_id",
                                   "pending_expiration_epoch", "STAGE", '')

    # +process_event - success - Previous event with TTL and name = OBJECT_CREATE_FOLDER
    @patch("lng_aws_clients.sts.get_client")
    @patch('service_commons.object_command.extend_event_ttl')
    @patch('service_commons.object_command.get_previous_and_current_events')
    def test_process_event_success_1(self, get_events_mock, update_event_mock, mock_sts_client):
        update_event_mock.return_value = None
        mock_event_id = 'mock-event'
        previous_event = {'event-name': event_names.OBJECT_CREATE_FOLDER,
                          'pending-expiration-epoch': 11111,
                          'event-id': mock_event_id,
                          'tracking-id': '123-tracking',
                          'request-time': '123'}
        current_event = {'event-name': event_names.OBJECT_CREATE_FOLDER,
                         'tracking-id': '123-tracking',
                         'request-time': '123'}
        get_events_mock.return_value = (previous_event, current_event)
        sts_credentials = {
            "Credentials": {
                "AccessKeyId": "AccessKeyIdVal",
                "SecretAccessKey": "SecretAccessKeyVal",
                "SessionToken": "SessionTokenVal",
                "Expiration": datetime.datetime(2018, 8, 23, 00, 33, 27)}}
        mock_sts_client.return_value.assume_role.return_value = sts_credentials

        collection_id = 'collection-id'
        object_id = 'object-id'
        folder_prefix = 'folder-upload/{0}_{1}_{2}'.format(collection_id, mock_event_id, object_id)

        self.assertEqual(
            process_event('request-id', '', object_id, collection_id),
            (mock_event_id, folder_prefix, sts_credentials['Credentials']))

        update_event_mock.assert_called_with(previous_event, current_event)

    # +process_event - success - No previous event and object state = Removed
    @patch("lng_aws_clients.sts.get_client")
    @patch('service_commons.object_command.extend_event_ttl')
    @patch('service_commons.object_command.get_previous_and_current_events')
    def test_process_event_success_2(self, get_events_mock, update_event_mock, mock_sts_client):
        update_event_mock.return_value = None
        previous_event = {}
        current_event = {'event-name': event_names.OBJECT_CREATE_FOLDER,
                         'tracking-id': '123-tracking',
                         'request-time': '123'}
        get_events_mock.return_value = (previous_event, current_event)
        sts_credentials = {
            "Credentials": {
                "AccessKeyId": "AccessKeyIdVal",
                "SecretAccessKey": "SecretAccessKeyVal",
                "SessionToken": "SessionTokenVal",
                "Expiration": datetime.datetime(2018, 8, 23, 00, 33, 27)}}
        mock_sts_client.return_value.assume_role.return_value = sts_credentials

        mock_event_id = 'mock-event-id'
        collection_id = 'collection-id'
        object_id = 'object-id'
        folder_prefix = 'folder-upload/{0}_{1}_{2}'.format(collection_id, mock_event_id, object_id)

        self.assertEqual(
            process_event(mock_event_id, object_status.REMOVED, object_id, collection_id),
            (mock_event_id, folder_prefix, sts_credentials['Credentials']))

        update_event_mock.assert_not_called()

    # -process_event - fail - Previous event with No TTL and name = OBJECT_CREATE_FOLDER - repeated create event
    @patch("lng_aws_clients.sts.get_client")
    @patch('service_commons.object_command.extend_event_ttl')
    @patch('service_commons.object_command.get_previous_and_current_events')
    def test_process_event_fail(self, get_events_mock, update_event_mock, mock_sts_client):
        update_event_mock.return_value = None
        previous_event = {'event-name': event_names.OBJECT_CREATE_FOLDER, 'event-id': 'event-123'}
        current_event = {'event-name': event_names.OBJECT_CREATE_FOLDER, 'event-id': 'event-456'}
        get_events_mock.return_value = (previous_event, current_event)

        mock_event_id = 'mock-event'
        collection_id = 'collection-id'
        object_id = 'object-id'

        with self.assertRaisesRegex(NotAllowedError,
                                    "Duplicate requests not allowed"):
            process_event(mock_event_id, object_status.REMOVED, object_id, collection_id)


if __name__ == '__main__':
    unittest.main()
