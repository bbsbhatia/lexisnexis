import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_changeset_event_handler import lambda_handler, create_changeset_event_handler, generate_changeset_item, \
    put_changeset_item

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CreateChangesetEventHandler")


class CreateChangesetEventHandlerTest(TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # +create_changeset_event_handler
    @patch('service_commons.object_event_handler.send_changeset_notifications')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_create_object_event_handler(self, mock_put_item, mock_send_notifications):
        mock_put_item.return_value = None
        mock_send_notifications.return_value = None
        event = io_utils.load_data_json("valid_cmd_event.json")
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)

    # - create_changeset_event_handler - message incorrect event name
    def test_create_changeset_event_handler_fail_1(self):
        event = io_utils.load_data_json("invalid_event_name.json")
        with self.assertRaisesRegex(TerminalErrorException, 'event-name does not match'):
            create_changeset_event_handler(event,
                                           'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # - create_changeset_event_handler - message incorrect event version
    def test_create_changeset_event_handler_fail_2(self):
        event = io_utils.load_data_json("invalid_event_version.json")
        with self.assertRaisesRegex(TerminalErrorException, 'event-version does not match'):
            create_changeset_event_handler(event,
                                           'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # - create_changeset_event_handler - message incorrect stage
    def test_create_changeset_event_handler_fail_3(self):
        event = io_utils.load_data_json("invalid_stage_event.json")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to process event'):
            create_changeset_event_handler(event,
                                           'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # test_generate_changeset_item: missing required property
    def test_create_changeset_event_handler_schema_failure(self):
        with self.assertRaisesRegex(TerminalErrorException, 'Missing required property: changeset-id'):
            generate_changeset_item({})

    # test_put_changeset_item: EndpointConnectionError
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_create_changeset_put_item_connection_error_exception(self, cbt_mock_put_item):
        cbt_mock_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            put_changeset_item({})

    # test_put_changeset_item: client error
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_create_changeset_put_item_client_error(self, cbt_mock_put_item):
        cbt_mock_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                     'Error': {
                                                         'Code': 'OTHER',
                                                         'Message': 'This is a mock'}},
                                                    "FAKE")
        with self.assertRaisesRegex(ClientError, 'This is a mock'):
            put_changeset_item({})

    # test_put_changeset_item: general exception
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_create_changeset_put_item_fail(self, cbt_mock_put_item):
        cbt_mock_put_item.side_effect = Exception("Some Error")
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to put item to Changeset Table due to unknown reasons.'):
            put_changeset_item({})


if __name__ == '__main__':
    unittest.main()
