import os
import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from process_ingestion_counter import update_ingestion_counters, update_ingestion_status_to_completed, \
    build_update_record, lambda_handler, retry_message

__author__ = "Prashant S, Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, "ProcessIngestionCounter")


class ProcessIngestionCounter(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'INGESTION_COUNTER_QUEUE_URL': "counter-queue-url",
                             'INGESTION_COUNTER_REDRIVE_QUEUE_URL': "counter-queue-url"})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # Tests for lambda_handler--------------------------------------------
    # + lambda_handler: valid
    @patch("process_ingestion_counter.update_ingestion_status_to_completed")
    @patch("service_commons.ingestion.delete_collection_blocker")
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_lambda_handler(self, mock_ingestion_update_counters, mock_delete_cb, mock_update_status):
        input = io_util.load_data_json('valid_event.json')
        mock_ingestion_update_counters.return_value = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-tracked-count': 10,
            'object-updated-count': 8,
            'object-error-count': 2,
            'object-count': 10}
        mock_delete_cb.return_value = None
        mock_update_status.return_value = None
        self.assertEqual(lambda_handler(input, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_ingestion_update_counters.assert_called_once()

    # - lambda_handler: no ingestion data
    @patch("process_ingestion_counter.update_ingestion_status_to_completed")
    @patch("service_commons.ingestion.delete_collection_blocker")
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_lambda_handler(self, mock_ingestion_update_counters, mock_delete_cb, mock_update_status):
        input = io_util.load_data_json('valid_event.json')
        mock_ingestion_update_counters.return_value = None
        mock_delete_cb.return_value = None
        mock_update_status.return_value = None
        self.assertEqual(lambda_handler(input, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_update_status.assert_not_called()
        mock_delete_cb.assert_not_called()

    # TODO many of these mocks are broken so that the path we think we're testing isn't actually getting hit. We need more assert_called_once()
    # - lambda_handler: EndpointConnectionError retry exception
    @patch("process_ingestion_counter.retry_message")
    @patch("process_ingestion_counter.update_ingestion_status_to_completed")
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_all_items')
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_lambda_handler_endpoint_connection_error_retry_exception(self, mock_ingestion_update_counters,
                                                                      mock_delete_cb, mock_update_status,
                                                                      mock_retry):
        input = io_util.load_data_json('valid_event.json')
        mock_ingestion_update_counters.return_value = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-tracked-count': 10,
            'object-updated-count': 8,
            'object-error-count': 2,
            'object-count': 10}
        mock_delete_cb.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        mock_update_status.return_value = None
        mock_retry.return_value = None
        self.assertEqual(lambda_handler(input, MockLambdaContext()), event_handler_status.SUCCESS)

    # - lambda_handler: retry exception
    @patch("process_ingestion_counter.retry_message")
    @patch("process_ingestion_counter.update_ingestion_status_to_completed")
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_all_items')
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_lambda_handler_retry_exception(self, mock_ingestion_update_counters, mock_delete_cb, mock_update_status,
                                            mock_retry):
        input = io_util.load_data_json('valid_event.json')
        mock_ingestion_update_counters.return_value = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-tracked-count': 10,
            'object-updated-count': 8,
            'object-error-count': 2,
            'object-count': 10}
        mock_delete_cb.side_effect = ClientError({'ResponseMetadata': {},
                                                  'Error': {
                                                      'Code': 'OTHER',
                                                      'Message': 'This is a mock'}},
                                                 "FAKE")
        mock_update_status.return_value = None
        mock_retry.return_value = None
        self.assertEqual(lambda_handler(input, MockLambdaContext()), event_handler_status.SUCCESS)

    # - lambda_handler: terminal exception
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    @patch("process_ingestion_counter.retry_message")
    @patch("process_ingestion_counter.update_ingestion_status_to_completed")
    @patch('lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.get_all_items')
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_lambda_handler_terminal_exception(self, mock_ingestion_update_counters, mock_delete_cb, mock_update_status,
                                               mock_retry, mock_terminal):
        input = io_util.load_data_json('valid_event.json')
        mock_ingestion_update_counters.return_value = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-tracked-count': 10,
            'object-updated-count': 8,
            'object-error-count': 2,
            'object-count': 10}
        mock_delete_cb.side_effect = Exception
        mock_update_status.return_value = None
        mock_retry.return_value = None
        mock_terminal.return_value = None
        self.assertEqual(lambda_handler(input, MockLambdaContext()), event_handler_status.SUCCESS)

    # - lambda_handler: exception
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    @patch("process_ingestion_counter.retry_message")
    @patch("process_ingestion_counter.update_ingestion_status_to_completed")
    @patch("service_commons.ingestion.delete_collection_blocker")
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_lambda_handler_exception_1(self, mock_ingestion_update_counters, mock_delete_cb, mock_update_status,
                                        mock_retry, mock_terminal):
        input = io_util.load_data_json('valid_event.json')
        mock_ingestion_update_counters.return_value = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-tracked-count': 10,
            'object-updated-count': 8,
            'object-error-count': 2,
            'object-count': 10}
        mock_delete_cb.side_effect = Exception("Some error")
        mock_update_status.return_value = None
        mock_retry.return_value = None
        mock_terminal.return_value = None
        self.assertEqual(lambda_handler(input, MockLambdaContext()), event_handler_status.SUCCESS)

    # Tests for update_ingestion_counters--------------------------------------------
    # + update_ingestion_counters: valid
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_update_ingestion_counters_success(self, mock_ingestion_update_counters):
        expected_response = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-updated-count': 8,
            'object-tracked-count': 10,
            'object-error-count': 2,
            'object-count': 10}
        mock_ingestion_update_counters.return_value = expected_response

        self.assertDictEqual(update_ingestion_counters("ingestion-01", {'ObjectUpdatedCount': 1}), expected_response)

    # - update_ingestion_counters: EndpointConnectionError
    @patch("process_ingestion_counter.retry_message")
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_update_ingestion_counters_endpoint_connection_error_exception(self, mock_ingestion_update_counters,
                                                                           mock_retry):
        mock_ingestion_update_counters.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')

        mock_retry.return_value = None
        self.assertIsNone(update_ingestion_counters("ingestion-01", {'ObjectUpdatedCount': 1}))

    # - update_ingestion_counters: client error
    @patch("process_ingestion_counter.retry_message")
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_counters")
    def test_update_ingestion_counters_client_error(self, mock_ingestion_update_counters, mock_retry):
        mock_ingestion_update_counters.side_effect = ClientError({'ResponseMetadata': {},
                                                                  'Error': {
                                                                      'Code': 'OTHER',
                                                                      'Message': 'This is a mock'}},
                                                                 "FAKE")

        mock_retry.return_value = None
        self.assertIsNone(update_ingestion_counters("ingestion-01", {'ObjectUpdatedCount': 1}))

    # Tests for update_ingestion_status_to_completed--------------------------------------------
    # + update_ingestion_status_to_completed: success
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_item")
    def test_update_ingestion_status_to_completed_success(self, mock_ingestion_update_item):
        ingestion_data = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-processed-count': 8,
            'object-tracked-count': 10,
            'object-updated-count': 8,
            'object-error-count': 2,
            'object-count': 10}

        mock_ingestion_update_item.return_value = None
        self.assertIsNone(update_ingestion_status_to_completed(ingestion_data, {'ObjectUpdatedCount': 1}))

    # - update_ingestion_status_to_completed: EndpointConnectionError
    @patch("process_ingestion_counter.retry_message")
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_item")
    def test_update_ingestion_status_to_completed_endpoint_connection_error_exception(self, mock_ingestion_update_item,
                                                                                      retry_mock):
        ingestion_data = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-processed-count': 8,
            'object-tracked-count': 10,
            'object-updated-count': 8,
            'object-error-count': 2,
            'object-count': 10}

        mock_ingestion_update_item.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        retry_mock.return_value = None
        self.assertIsNone(update_ingestion_status_to_completed(ingestion_data, {'ObjectUpdatedCount': 1}))

    # - update_ingestion_status_to_completed: client error
    @patch("process_ingestion_counter.retry_message")
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_item")
    def test_update_ingestion_status_to_completed_client_error(self, mock_ingestion_update_item, retry_mock):
        ingestion_data = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-processed-count': 8,
            'object-tracked-count': 10,
            'object-updated-count': 8,
            'object-error-count': 2,
            'object-count': 10}

        mock_ingestion_update_item.side_effect = ClientError({'ResponseMetadata': {},
                                                              'Error': {
                                                                  'Code': 'OTHER',
                                                                  'Message': 'This is a mock'}},
                                                             "FAKE")
        retry_mock.return_value = None
        self.assertIsNone(update_ingestion_status_to_completed(ingestion_data, {'ObjectUpdatedCount': 1}))

    # - update_ingestion_status_to_completed: exception
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    @patch("lng_datalake_dal.ingestion_table.IngestionTable.update_item")
    def test_update_ingestion_status_to_completed_exception(self, mock_ingestion_update_item, terminal_mock):
        ingestion_data = {
            'ingestion-id': 'ingestion-01',
            'ingestion-state': 'Processing',
            'object-processed-count': 12,
            'object-tracked-count': 10,
            'object-updated-count': 10,
            'object-error-count': 2,
            'object-count': 10}

        mock_ingestion_update_item.side_effect = Exception
        terminal_mock.return_value = None
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception occurred'):
            update_ingestion_status_to_completed(ingestion_data, {'ObjectUpdatedCount': 1})

    # Tests for build_update_record--------------------------------------------
    # + build_update_record - Valid Event
    def test_build_update_record_valid(self):
        input = io_util.load_data_json('valid_event.json')
        expected_response = {'567': {'ObjectUpdatedCount': 0}, '1234': {'ObjectTrackedCount': 5}}
        self.assertDictEqual(build_update_record(input), expected_response)

    # Tests for retry_message--------------------------------------------
    # + retry_message - Valid Event
    @patch('lng_aws_clients.sqs.get_client')
    def test_retry_message_valid(self, mock_sqs):
        mock_sqs.return_value.send_message.return_value = None
        self.assertIsNone(retry_message("ingestion-01", {"ObjecUpdatedCount": 1}, True))

    # - retry_message - Exception
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    @patch('lng_aws_clients.sqs.get_client')
    def test_retry_message_exception(self, mock_sqs, mock_terminal):
        mock_sqs.return_value.send_message.side_effect = Exception
        mock_terminal.return_value = None
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to retry ingestion'):
            retry_message("ingestion-01", {"ObjecUpdatedCount": 1}, True)


if __name__ == '__main__':
    unittest.main()
