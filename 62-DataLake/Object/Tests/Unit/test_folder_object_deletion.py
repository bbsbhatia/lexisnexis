import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
from lng_datalake_testhelper.io_utils import IOUtils

import folder_object_deletion_lambda

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "FolderObjectDeletion")


class FolderObjectDeletionTest(TestCase):

    # + test_folder_object_deletion: Valid
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_folder_object_deletion(self, mock_set_session, mock_s3_get_client, mock_sqs_get_client):
        mock_set_session.return_value = None
        mock_s3_get_client.return_value.get_paginator.return_value.paginate.return_value = \
            [io_utils.load_data_json('s3.valid_list.json')]
        mock_s3_get_client.return_value.delete_objects.return_value = None
        input_dict = io_utils.load_data_json('sqs_message_valid.json')
        self.assertEqual(event_handler_status.SUCCESS,
                         folder_object_deletion_lambda.folder_object_deletion(input_dict, MockLambdaContext()))
        self.assertEqual(2, mock_s3_get_client.return_value.delete_objects.call_count)

    # + test_folder_object_deletion: Valid
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_folder_object_deletion_1001_objects(self, mock_set_session, mock_s3_get_client, mock_sqs_get_client):
        mock_set_session.return_value = None
        mock_s3_response = io_utils.load_data_json('s3.over_1000_valid_list.json')
        for i in range(8):
            mock_s3_response['Contents'].extend(mock_s3_response['Contents'])
        mock_s3_get_client.return_value.get_paginator.return_value.paginate.return_value = [mock_s3_response]
        mock_s3_get_client.return_value.delete_objects.return_value = None
        input_dict = io_utils.load_data_json('sqs_message_valid.json')
        self.assertEqual(event_handler_status.SUCCESS,
                         folder_object_deletion_lambda.folder_object_deletion(input_dict, MockLambdaContext()))
        self.assertEqual(4, mock_s3_get_client.return_value.delete_objects.call_count)

    # - test_folder_object_deletion: no bucket
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_folder_object_deletion_no_bucket(self, mock_set_session, mock_sqs, mock_error_handler):
        mock_set_session.return_value = None
        mock_sqs.return_value = None
        input_dict = io_utils.load_data_json('sqs_message_no_bucket.json')
        folder_object_deletion_lambda.folder_object_deletion(input_dict, MockLambdaContext())
        mock_error_handler.assert_called_once()

    # - test_folder_object_deletion: no bucket
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_folder_object_deletion_no_key(self, mock_set_session, mock_sqs, mock_error_handler):
        mock_set_session.return_value = None
        mock_sqs.return_value = None
        input_dict = io_utils.load_data_json('sqs_message_no_key.json')
        folder_object_deletion_lambda.folder_object_deletion(input_dict, MockLambdaContext())
        mock_error_handler.assert_called_once()

    # - test_folder_object_deletion: too many retries
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_folder_object_deletion_too_many_retries(self, mock_set_session, mock_sqs, mock_error_handler):
        mock_set_session.return_value = None
        mock_sqs.return_value = None
        input_dict = io_utils.load_data_json('sqs_message_too_many_retries.json')
        folder_object_deletion_lambda.folder_object_deletion(input_dict, MockLambdaContext())
        mock_error_handler.assert_called_once()

    # - test_folder_object_deletion: ClientError
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_folder_object_deletion_client_error(self, mock_set_session, mock_s3_client, mock_sqs):
        mock_set_session.return_value = None
        mock_sqs.return_value.send_message.return_value = None
        mock_s3_client.side_effect = ClientError({'ResponseMetadata': {},
                                                  'Error': {
                                                      'Code': 'OTHER',
                                                      'Message': 'This is a mock'}},
                                                 "FAKE")
        input_dict = io_utils.load_data_json('sqs_message_valid.json')
        folder_object_deletion_lambda.folder_object_deletion(input_dict, MockLambdaContext())
        expected_sqs = io_utils.load_data_json('sqs_message_retry_valid.json')['body']
        mock_sqs.return_value.send_message.assert_called_with(MessageBody=expected_sqs, QueueUrl=None)
        mock_sqs.return_value.send_message.assert_called_once()

    # - test_folder_object_deletion: Exception
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_folder_object_deletion_exception(self, mock_set_session, mock_s3_client, mock_sqs, mock_error_handler):
        mock_set_session.return_value = None
        mock_sqs.return_value.send_message.return_value = None
        mock_s3_client.side_effect = Exception
        input_dict = io_utils.load_data_json('sqs_message_valid.json')
        folder_object_deletion_lambda.folder_object_deletion(input_dict, MockLambdaContext())
        mock_error_handler.assert_called_once()

    # - test_delete_object_list: no objects
    @patch('lng_aws_clients.s3.get_client')
    def test_delete_object_list_no_objects(self, mock_s3_get_client):
        mock_s3_get_client.return_value.get_paginator.return_value.paginate.return_value = \
            [io_utils.load_data_json('s3.invalid_list.json')]
        self.assertIsNone(folder_object_deletion_lambda.delete_object_list('bucket-name', 'object-key'))

    # - test_delete_object_list: ClientError
    @patch('lng_aws_clients.s3.get_client')
    def test_delete_object_list_client_error(self, mock_s3_get_client):
        mock_s3_get_client.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {
                                                          'Code': 'OTHER',
                                                          'Message': 'This is a mock'}},
                                                     "FAKE")
        self.assertRaisesRegex(ClientError, 'This is a mock', folder_object_deletion_lambda.delete_object_list,
                               'bucket-name', 'object-key')


if __name__ == '__main__':
    unittest.main()
