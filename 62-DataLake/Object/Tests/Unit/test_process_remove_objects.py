import unittest
from unittest.mock import patch, call

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException, \
    IgnoreEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_constants import event_names
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from process_remove_objects import process_retry_exception, delete_collection_blocker_by_key, \
    unlock_collection_blocker_row, generate_event_item, generate_tracking_item, do_work, \
    process_object_ids, get_batch, lock_collection_blocker_row, check_collection_blocker_item, lambda_handler

__author__ = "Mark Seitter, Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'ProcessRemoveObjects')


class TestProcessRemoveObjects(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + success lambda_handler SUCCESS
    @patch("process_remove_objects.logger")
    @patch("process_remove_objects.delete_collection_blocker_by_key")
    @patch("process_remove_objects.process_object_ids")
    @patch("process_remove_objects.get_batch")
    @patch("process_remove_objects.check_collection_blocker_item")
    def test_lambda_handler(self, mock_check_collection_blocker_item, mock_get_batch, mock_process_object_ids,
                            mock_delete_collection_blocker_by_key, mock_logger):
        mock_check_collection_blocker_item.return_value = None
        mock_get_batch.return_value = {"object-ids": ["test_1", "test_2"]}
        mock_process_object_ids.return_value = None
        mock_delete_collection_blocker_by_key.return_value = None
        self.assertEqual(lambda_handler(io_utils.load_data_json('valid_event.json'), MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_logger.debug.assert_called_once_with('Found total of 2 items to remove')

    # + success lambda_handler SUCCESS
    @patch("process_remove_objects.logger")
    @patch("process_remove_objects.delete_collection_blocker_by_key")
    @patch("process_remove_objects.process_object_ids")
    @patch("process_remove_objects.get_batch")
    @patch("process_remove_objects.check_collection_blocker_item")
    def test_lambda_handler_retry(self, mock_check_collection_blocker_item, mock_get_batch, mock_process_object_ids,
                                  mock_delete_collection_blocker_by_key, mock_logger):
        mock_check_collection_blocker_item.return_value = None
        mock_get_batch.return_value = {"object-ids": ["test_1", "test_2"]}
        mock_process_object_ids.return_value = None
        mock_delete_collection_blocker_by_key.return_value = None
        self.assertEqual(
            lambda_handler(io_utils.load_data_json('valid_event_with_additional_attributes.json'), MockLambdaContext()),
            event_handler_status.SUCCESS)
        mock_logger.debug.assert_called_once_with('Found total of 2 items to remove')

    # - failed lambda_handler: not current tracking - puts item back on SQS and returns None
    @patch('lng_aws_clients.sqs.get_client')
    @patch("process_remove_objects.logger")
    @patch("process_remove_objects.check_collection_blocker_item")
    def test_lambda_handler_retry_error(self, mock_check_collection_blocker_item, mock_logger, mock_sqs_client):
        re = RetryEventException("test")
        mock_check_collection_blocker_item.side_effect = re
        mock_sqs_client.return_value.send_message.return_value = None
        self.assertEqual(lambda_handler(io_utils.load_data_json('valid_event.json'), MockLambdaContext()),
                         None)
        mock_sqs_client.return_value.send_message.assert_called_once()
        mock_logger.warning.assert_called_once_with(re)

    # - failed lambda_handler ignore event failure
    @patch("lng_datalake_commons.error_handling.error_handler")
    @patch('lng_aws_clients.sqs.get_client')
    @patch("process_remove_objects.logger")
    @patch("process_remove_objects.check_collection_blocker_item")
    def test_lambda_handler_ignore_error(self, mock_check_collection_blocker_item,
                                         mock_logger, mock_sqs_client, mock_error_handler):
        iee = IgnoreEventException("Test Ignore")
        mock_error_handler.return_value = None
        mock_check_collection_blocker_item.side_effect = iee
        mock_sqs_client.return_value.send_message.return_value = None
        self.assertEqual(lambda_handler(io_utils.load_data_json('valid_event.json'), MockLambdaContext()),
                         None)
        mock_logger.debug.assert_called_with("IgnoreEventException raised for event {0} throwing away : {1}".format(
            io_utils.load_data_json('valid_event.json'), iee))

    # - failed lambda_handler ignore event failure
    @patch("lng_datalake_commons.error_handling.error_handler")
    @patch('lng_aws_clients.sqs.get_client')
    @patch("process_remove_objects.logger")
    @patch("process_remove_objects.check_collection_blocker_item")
    def test_lambda_handler_fatal_error(self, mock_check_collection_blocker_item,
                                        mock_logger, mock_sqs_client, mock_error_handler):
        te = TerminalErrorException("Test Ignore")
        mock_error_handler.return_value = None
        mock_check_collection_blocker_item.side_effect = te
        mock_sqs_client.return_value.send_message.return_value = None
        self.assertEqual(lambda_handler(io_utils.load_data_json('valid_event.json'), MockLambdaContext()),
                         None)
        mock_logger.error.assert_called_with(te)

    # - failed lambda_handler terminal failure
    @patch("lng_datalake_commons.error_handling.error_handler")
    @patch('lng_aws_clients.sqs.get_client')
    @patch("process_remove_objects.logger")
    @patch("process_remove_objects.unlock_collection_blocker_row")
    @patch("process_remove_objects.check_collection_blocker_item")
    def test_lambda_handler_terminal_error(self, mock_check_collection_blocker_item, mock_unlock_collection_blocker_row,
                                           mock_logger, mock_sqs_client, mock_error_handler):
        re = RetryEventException("test")
        te = TerminalErrorException("Unhandled exception occurred")
        mock_error_handler.return_value = None
        mock_check_collection_blocker_item.side_effect = re
        mock_sqs_client.return_value.send_message.return_value = None
        mock_unlock_collection_blocker_row.side_effect = te
        self.assertEqual(lambda_handler(io_utils.load_data_json('valid_event.json'), MockLambdaContext()),
                         None)
        mock_logger.error.assert_called_with(te)

    # - failed check collection blocker terminal exception
    @patch("process_remove_objects.logger")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_check_collection_blocker_item_no_blocker(self, mock_coll_blocker_table, mock_logger):
        mock_coll_blocker_table.return_value = None
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Collection blocker not found for "
                                    "Collection ID {}. Event-id: {}".format("test", "different_id")):
            check_collection_blocker_item("test", "different_id", "2019")
        mock_logger.error.assert_called_once_with("Collection blocker not found for "
                                                  "Collection ID {}. Event-id: {}".format("test", "different_id"))

    # - failed check collection blocker terminal exception
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_check_collection_blocker_item_terminal_query(self, mock_coll_blocker_table):
        exception = Exception(
            {"Error": {"Code": "Fatal", "Message": "Mock Client Error"}}, "MockError")
        mock_coll_blocker_table.side_effect = exception
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to query items in CollectionBlocker "
                                    "Table by collection-id: {} for event-id: {}||{}".format(
                                        "test", "different_id", exception)):
            check_collection_blocker_item("test", "different_id", "2019")

    # - failed check collection blocker retry exception
    @patch("process_remove_objects.logger")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_check_collection_blocker_item_retry_query(self, mock_coll_blocker_table, mock_logger):
        ee = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        mock_coll_blocker_table.side_effect = ee
        with self.assertRaisesRegex(RetryEventException,
                                    "Retry exception thrown while querying "
                                    "Collection Blocker for {0} {1}".format("test", ee)):
            check_collection_blocker_item("test", "different_id", "2019")
        mock_logger.debug.assert_called_once_with("Retry exception thrown while query "
                                                  "collection blocker table: {0}".format(ee))

    # - failed check collection blocker retry exception different event id
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_check_collection_blocker_item_retry_event(self, mock_coll_blocker_table):
        item = {"collection-id": "test", "event-status": "NotLocked", "request-id": "fake_id", "request-time": "2019",
                "event-name": "NotIngestionCreate"}
        mock_coll_blocker_table.return_value = [item]
        with self.assertRaisesRegex(RetryEventException,
                                    'Collection {0} not ready to be processed, Retrying'.format("test")):
            check_collection_blocker_item("test", "different_id", "2019")

    # - failed check collection blocker retry exception
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_check_collection_blocker_item_retry(self, mock_coll_blocker_table):
        item = {"collection-id": "test", "event-status": "NotLocked", "request-id": "fake_id", "request-time": "2019",
                "event-name": "Object::IngestionCreate"}
        mock_coll_blocker_table.return_value = [item]
        with self.assertRaisesRegex(RetryEventException,
                                    'Collection {0} not ready to be processed, Retrying'.format("test")):
            check_collection_blocker_item("test", "different_id", "2019")

    # - failed check collection blocker terminal request time
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_check_collection_blocker_item_terminal_time(self, mock_coll_blocker_table):
        item = {"collection-id": "test", "event-status": "NotLocked", "request-id": "fake_id", "request-time": "2019"}
        mock_coll_blocker_table.return_value = [item]
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Current collection blocker timestamp is greater than "
                                    "requests OldestTime: {0} EventsTime: {1}".format("2019", "2018")):
            check_collection_blocker_item("test", "different_id", "2018")

    # + success check collection blocker
    @patch("process_remove_objects.lock_collection_blocker_row")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.query_items")
    def test_check_collection_blocker_item(self, mock_coll_blocker_table, mock_locker):
        item = {"collection-id": "test", "event-status": "NotLocked", "request-id": "fake_id"}
        locked_item = {"collection-id": "test", "event-status": "Locked", "request-id": "fake_id"}
        mock_coll_blocker_table.return_value = [item]
        mock_locker.return_value = locked_item
        self.assertDictEqual(check_collection_blocker_item("test", "fake_id", "req_time"), locked_item)

    # - failed lock collection blocker row condition error
    @patch("process_remove_objects.logger")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.update_item")
    def test_lock_collection_blocker_row_terminal_error(self, mock_coll_blocker_table, mock_logger):
        exception = Exception(
            {"Error": {"Code": "Fatal", "Message": "Mock Client Error"}}, "MockError")
        item = {"collection-id": "test", "event-status": "NotLocked"}
        mock_coll_blocker_table.side_effect = exception
        with self.assertRaisesRegex(TerminalErrorException,
                                    "TerminalErrorException||Terminal Error thrown while locking the "
                                    "CollectionBlocker table: {0}".format(exception)):
            lock_collection_blocker_row(item, "fake_id")
        mock_logger.debug.assert_called_once_with("Attempting to lock {0}".format({"collection-id": "test",
                                                                                   "event-status": "NotLocked"}))

    # - failed lock collection blocker row condition error retry
    @patch("process_remove_objects.logger")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.update_item")
    def test_lock_collection_blocker_row_retry_error(self, mock_coll_blocker_table, mock_logger):
        ee = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        item = {"collection-id": "test", "event-status": "NotLocked"}
        mock_coll_blocker_table.side_effect = ee
        with self.assertRaisesRegex(RetryEventException,
                                    "Retry exception thrown while attempting to lock "
                                    "the CollectionBlocker table {0}".format(ee)):
            lock_collection_blocker_row(item, "fake_id")
        mock_logger.debug.assert_has_calls([call("Attempting to lock {0}".format({"collection-id": "test",
                                                                                  "event-status": "NotLocked"})),
                                            call("Retry exception thrown while attempting "
                                                 "to lock the CollectionBlocker table: {0}".format(ee))])

    # - failed lock collection blocker row condition error
    @patch("process_remove_objects.logger")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.update_item")
    def test_lock_collection_blocker_row_condition_error(self, mock_coll_blocker_table, mock_logger):
        item = {"collection-id": "test", "event-status": "NotLocked"}
        mock_coll_blocker_table.side_effect = ConditionError({"Mock Condition Error"})
        with self.assertRaisesRegex(IgnoreEventException,
                                    "Locking CollectionBlocker failed due to conditional exception."):
            lock_collection_blocker_row(item, "fake_id")
        mock_logger.debug.assert_has_calls([call("Attempting to lock {0}".format({"collection-id": "test",
                                                                                  "event-status": "NotLocked"})),
                                            call("Unable to lock CollectionBlocker for Event: {0}".format("fake_id"))])

    # - failed lock collection blocker row duplicate event
    @patch("process_remove_objects.logger")
    def test_lock_collection_blocker_row_duplicate(self, mock_logger):
        item = {"collection-id": "test", "event-status": "Locked"}
        with self.assertRaisesRegex(IgnoreEventException,
                                    "CollectionBlocker is locked. Duplicate event"):
            lock_collection_blocker_row(item, "fake_id")
        mock_logger.error.assert_called_once_with(
            "Unable to lock CollectionBlocker item {0} for event={1}".format(item, "fake_id"))

    # + success lock collection blocker row
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.update_item")
    def test_lock_collection_blocker_row(self, mock_coll_blocker_table):
        item = {"collection-id": "test", "event-status": "NotLocked"}
        mock_coll_blocker_table.return_value = None
        self.assertDictEqual(lock_collection_blocker_row(item, "fake_id"),
                             {"event-status": "Locked", "collection-id": "test"})

    # - failed get batch client error terminal
    @patch('lng_aws_clients.s3.read_s3_object')
    def test_get_batch_client_error_terminal(self, mock_s3_read):
        exception = Exception(
            {"Error": {"Code": "Fatal", "Message": "Mock Client Error"}}, "MockError")
        mock_s3_read.side_effect = exception
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Unhandled exception occurred||{}".format(exception)):
            get_batch("test_bucket", "batch_key")

    # - failed get batch client error terminal
    @patch('lng_aws_clients.s3.read_s3_object')
    def test_get_batch_client_error_retry(self, mock_s3_read):
        ee = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        mock_s3_read.side_effect = ee
        with self.assertRaisesRegex(RetryEventException,
                                    "Unable to read batch s3://{}/{}||{}".format("test_bucket", "batch_key", ee)):
            get_batch("test_bucket", "batch_key")

    # - failed get batch client error terminal
    @patch('lng_aws_clients.s3.read_s3_object')
    def test_get_batch_client_error_client_terminal(self, mock_s3_read):
        ce = ClientError({"Error": {"Code": "NoSuchKey",
                                    "Message": "Mock Client Error"}},
                         "MockError")
        mock_s3_read.side_effect = ce
        with self.assertRaisesRegex(TerminalErrorException,
                                    "S3 object {} not found in bucket {}".format("batch_key", "test_bucket")):
            get_batch("test_bucket", "batch_key")

    # - failed get batch client error
    @patch('lng_aws_clients.s3.read_s3_object')
    def test_get_batch_client_error(self, mock_s3_read):
        ce = ClientError({"Error": {"Code": 101,
                                    "Message": "Mock Client Error"}},
                         "MockError")
        mock_s3_read.side_effect = ce
        with self.assertRaisesRegex(RetryEventException,
                                    "Unable to read batch s3://{}/{}||{}".format("test_bucket", "batch_key", ce)):
            get_batch("test_bucket", "batch_key")

    # + success get batch
    @patch("process_remove_objects.logger")
    @patch('lng_aws_clients.s3.read_s3_object')
    def test_get_batch(self, mock_s3_read, mock_logger):
        mock_s3_read.return_value = "{\"description\": \"test\"}"
        self.assertEqual({"description": "test"}, get_batch("test_bucket", "batch_key"))
        mock_logger.info.assert_has_calls(
            [call("Read batch from s3://test_bucket/batch_key"), call("Batch Remove Description: test")])

    # - failed process object ids terminal
    @patch("process_remove_objects.do_work")
    def test_process_object_ids_retry_terminal(self, mock_do_work):
        exception = TerminalErrorException("Unhandled exception occurred")
        mock_do_work.side_effect = exception
        message = io_utils.load_data_json('message.json')
        with self.assertRaisesRegex(TerminalErrorException, "Unhandled exception occurred"):
            process_object_ids(["blah"], 1, message)

    # - failed process object ids retry
    @patch("lng_datalake_commons.error_handling.error_handler.set_message_additional_attribute")
    @patch("process_remove_objects.do_work")
    def test_process_object_ids_retry(self, mock_do_work, mock_error_handler):
        mock_do_work.return_value = 2
        message = io_utils.load_data_json('message.json')
        with self.assertRaisesRegex(RetryEventException, "RetryEventException||"
                                                         "Error processing indexes [{}] in chunk {}".format(2, 1)):
            process_object_ids(["blah"], 1, message)
        mock_do_work.assert_called_once_with("blah", 0, 1000, message)
        mock_error_handler.assert_has_calls([call("retry-indexes", [2]), call("start-chunk", 1)])

    # + success process object ids success
    @patch("lng_datalake_commons.error_handling.error_handler.set_message_additional_attribute")
    @patch("process_remove_objects.do_work")
    def test_process_object_ids(self, mock_do_work, mock_error_handler):
        mock_do_work.return_value = -1
        message = io_utils.load_data_json('message.json')
        self.assertIsNone(process_object_ids(["blah"], 1, message))
        mock_do_work.assert_called_once_with("blah", 0, 1000, message)
        mock_error_handler.assert_has_calls([call("retry-indexes", []), call("start-chunk", 1)])

    # - failed do work condition error
    # - failed do work condition error
    @patch("process_remove_objects.generate_event_item")
    @patch("process_remove_objects.generate_tracking_item")
    @patch("lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    def test_do_work_terminal_error(self, mock_tracking_table, mock_event_backend, mock_generate_tracking_item,
                                    mock_generate_event_item):
        exception = Exception(
            {"Error": {"Code": "Fatal", "Message": "Mock Client Error"}}, "MockError")
        mock_event_backend.return_value = None
        mock_tracking_table.side_effect = exception
        mock_generate_tracking_item.return_value = None
        mock_generate_event_item.return_value = None
        with self.assertRaisesRegex(TerminalErrorException, "Unhandled exception occurred||{0}".format(exception)):
            do_work("blah", 1, 1, {"fake": "test"})

    # - failed do work condition error
    @patch("process_remove_objects.logger")
    @patch("process_remove_objects.generate_event_item")
    @patch("process_remove_objects.generate_tracking_item")
    @patch("lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    def test_do_work_retry_error(self, mock_tracking_table, mock_event_backend, mock_generate_tracking_item,
                                 mock_generate_event_item, mock_logger):
        ce = ClientError({"Error": {"Code": 101,
                                    "Message": "Mock Client Error"}},
                         "MockError")
        mock_event_backend.return_value = None
        mock_tracking_table.side_effect = ce
        mock_generate_tracking_item.return_value = None
        mock_generate_event_item.return_value = None
        self.assertEqual(1, do_work("blah", 1, 1, {"fake": "test"}))
        mock_logger.error.assert_called_once_with(
            "Error creating tracking and event store backend records for {0}||{1}".format("blah", ce))

    # - failed do work condition error
    @patch("process_remove_objects.logger")
    @patch("process_remove_objects.generate_event_item")
    @patch("process_remove_objects.generate_tracking_item")
    @patch("lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    def test_do_work_condition_error(self, mock_tracking_table, mock_event_backend, mock_generate_tracking_item,
                                     mock_generate_event_item, mock_logger):
        mock_event_backend.return_value = None
        mock_tracking_table.side_effect = ConditionError({"Mock Condition Error"})
        mock_generate_tracking_item.return_value = None
        mock_generate_event_item.return_value = None
        do_work("blah", 1, 1, {"fake": "test"})
        mock_logger.error.assert_called_once_with(
            "ConditionError: object_id {0}||{1}".format("blah", ConditionError({"Mock Condition Error"})))

    # + successful do work
    @patch("process_remove_objects.generate_event_item")
    @patch("process_remove_objects.generate_tracking_item")
    @patch("lng_datalake_dal.event_store_backend_table.EventStoreBackendTable.put_item")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    def test_do_work(self, mock_tracking_table, mock_event_backend, mock_generate_tracking_item,
                     mock_generate_event_item):
        mock_event_backend.return_value = None
        mock_tracking_table.return_value = None
        mock_generate_tracking_item.return_value = None
        mock_generate_event_item.return_value = None
        self.assertEqual(-1, do_work("blah", 1, 1, {"fake": "test"}))

    # + successful generate tracking item
    def test_generate_tracking_item(self):
        message = io_utils.load_data_json('message.json')
        self.assertDictEqual(generate_tracking_item(message, "smallobj5", 1),
                             io_utils.load_data_json("tracking_item.json"))

    # + successful generate event item
    def test_generate_event_item(self):
        message = io_utils.load_data_json('message.json')
        self.assertDictEqual(generate_event_item(message, "smallobj5", 1), io_utils.load_data_json("event_dict.json"))


    # + successful update collection blocker row
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.update_item")
    def test_unlock_collection_blocker_row(self, mock_collection_blocker_table):
        mock_collection_blocker_table.return_value = None
        item = {'event-status': 'test', 'collection-id': 'collection_id',
                'request-time': '2019-02-26T09:19:59.592Z', 'request-id': '0110f4da-314a-11e9-91bc-09464e75f09c',
                'event-name': 'Object::Update',
                'pending-expiration-epoch': 1561190831}
        self.assertIsNone(unlock_collection_blocker_row(item))
        self.assertIsNone(item.get('event-status'))

    # + successful update collection blocker row
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.update_item")
    def test_unlock_collection_blocker_row_no_status(self, mock_collection_blocker_table):
        mock_collection_blocker_table.return_value = None
        item = {'collection-id': 'collection_id',
                'request-time': '2019-02-26T09:19:59.592Z', 'request-id': '0110f4da-314a-11e9-91bc-09464e75f09c',
                'event-name': 'Object::Update',
                'pending-expiration-epoch': 1561190831}
        self.assertIsNone(unlock_collection_blocker_row(item))
        mock_collection_blocker_table.assert_not_called()
        self.assertIsNone(item.get('event-status'))

    # - failed update collection blocker row
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.update_item")
    def test_unlock_collection_blocker_row_exception(self, mock_collection_blocker_table):
        mock_collection_blocker_table.side_effect = Exception(
            {"Error": {"Code": "Fatal", "Message": "Mock Client Error"}}, "MockError")
        item = {'event-status': 'test', 'collection-id': 'collection_id',
                'request-time': '2019-02-26T09:19:59.592Z', 'request-id': '0110f4da-314a-11e9-91bc-09464e75f09c',
                'event-name': 'Object::Update',
                'pending-expiration-epoch': 1561190831}
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Update collection blocker table failed | Retry Status: {} ".format("test")):
            self.assertIsNone(unlock_collection_blocker_row(item))

    # + successful delete_collection_blocker_by_key
    @patch("process_remove_objects.logger")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_delete_collection_blocker_by_key(self, mock_collection_blocker_table, mock_logger):
        mock_collection_blocker_table.return_value = None
        self.assertIsNone(delete_collection_blocker_by_key("collection_id", "req_time", "event_id", "stage"))
        primary_key = {"collection-id": "collection_id", "request-time": "req_time"}
        mock_logger.info.assert_called_once_with(
            "Successfully removed collection blocker item: {}".format(primary_key))

    # - failed delete_collection_blocker_by_key non retry error
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_delete_collection_blocker_by_key_non_retry_error(self, mock_collection_blocker_table):
        mock_collection_blocker_table.side_effect = ClientError(
            {"Error": {"Code": 101, "Message": "Mock Client Error"}}, "MockError")
        with self.assertRaisesRegex(TerminalErrorException, "Failed to delete item from collection blocker table."):
            delete_collection_blocker_by_key("collection_id", "req_time", "event_id", "stage")

    # - failed delete_collection_blocker_by_key retry client error
    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_delete_collection_blocker_by_key_client_retry_error(self, mock_collection_blocker_table,
                                                                 mock_throttle_error):
        mock_throttle_error.return_value = None
        mock_collection_blocker_table.side_effect = ClientError(
            {"Error": {"Code": "500", "Message": ""}}, "delete")
        delete_collection_blocker_by_key("collection_id", "req_time", "event_id", "stage")
        mock_throttle_error.assert_called_with(
            {"collection-id": "collection_id", "event-id": "event_id", "stage": "stage",
             "request-time": "req_time", "event-name": event_names.TRACKING_REMOVE},
            service="Tracking")

    # - failed delete_collection_blocker_by_key retry client error
    @patch("lng_datalake_commons.error_handling.error_handler.throttle_error")
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_delete_collection_blocker_by_key_retry_error(self, mock_collection_blocker_table,
                                                          mock_throttle_error):
        mock_throttle_error.return_value = None
        mock_collection_blocker_table.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        delete_collection_blocker_by_key("collection_id", "req_time", "event_id", "stage")
        mock_throttle_error.assert_called_with(
            {"collection-id": "collection_id", "event-id": "event_id", "stage": "stage",
             "request-time": "req_time", "event-name": event_names.TRACKING_REMOVE},
            service="ConnectionError")

    # - failed delete_collection_blocker_by_key terminal exception
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_delete_collection_blocker_by_key_terminal(self, mock_collection_blocker_table):
        mock_collection_blocker_table.side_effect = Exception(
            {"Error": {"Code": "Fatal", "Message": "Mock Client Error"}}, "MockError")
        with self.assertRaisesRegex(TerminalErrorException, "Failed to delete item from collection blocker table."):
            delete_collection_blocker_by_key("collection_id", "req_time", "event_id", "stage")

    # + successful process_retry_exception: no seen-count in messageAttributes
    @patch("lng_aws_clients.sqs.get_client")
    def test_process_retry_exception_success_unseen(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None
        expected_call = "{\"Message\": \"{\\\"collection-id\\\": \\\"batchRemoveCollection16\\\", \\\"bucket-name\\\": \\\"unit_test_bucket\\\", \\\"event-id\\\": \\\"EUwVOXbuVpHLbB16\\\", \\\"object-key\\\": \\\"batch/LATEST/EUwVOXbuVpHLbB36\\\", \\\"request-time\\\": \\\"2019-10-29T19:10:24.517Z\\\", \\\"event-name\\\": \\\"Object::BatchRemove\\\", \\\"description\\\": \\\"Batch Remove Collection\\\", \\\"event-version\\\": 1, \\\"stage\\\": \\\"LATEST\\\", \\\"old-object-versions-to-keep\\\": 3}\"}"

        self.assertIsNone(process_retry_exception(RetryEventException(""),
                                                  io_utils.load_data_json('valid_event.json')))

        mock_sqs_client.return_value.send_message.assert_called_with(DelaySeconds=120, MessageAttributes={
            'seen-count': {'DataType': 'Number', 'StringValue': '1'}},
                                                                     MessageBody=expected_call,
                                                                     QueueUrl='https://sqs.us-east-1.amazonaws.com/123456789012/mock-queue')

    # + successful process_retry_exception: with seen-count in messageAttributes
    @patch("lng_aws_clients.sqs.get_client")
    def test_process_retry_exception_success_seen(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None
        expected_call = "{\"Message\": \"{\\\"collection-id\\\": \\\"batchRemoveCollection16\\\", \\\"bucket-name\\\": \\\"unit_test_bucket\\\", \\\"event-id\\\": \\\"EUwVOXbuVpHLbB16\\\", \\\"object-key\\\": \\\"batch/LATEST/EUwVOXbuVpHLbB36\\\", \\\"request-time\\\": \\\"2019-10-29T19:10:24.517Z\\\", \\\"event-name\\\": \\\"Object::BatchRemove\\\", \\\"description\\\": \\\"Batch Remove Collection\\\", \\\"event-version\\\": 1, \\\"stage\\\": \\\"LATEST\\\", \\\"old-object-versions-to-keep\\\": 3}\"}"
        self.assertIsNone(process_retry_exception(RetryEventException(""),
                                                  io_utils.load_data_json('valid_event_with_seen_count.json')))

        mock_sqs_client.return_value.send_message.assert_called_with(DelaySeconds=126, MessageAttributes={
            'seen-count': {'DataType': 'Number', 'StringValue': '7'}},
                                                                     MessageBody=expected_call,
                                                                     QueueUrl='https://sqs.us-east-1.amazonaws.com/123456789012/mock-queue')

    # + successful process_retry_exception: with additional attributes
    @patch("lng_aws_clients.sqs.get_client")
    def test_process_retry_exception_success_additional_attributes(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None
        expected_call = "{\"Message\":\"{\\\"collection-id\\\":\\\"batchRemoveCollection16\\\",\\\"bucket-name\\\":\\\"unit_test_bucket\\\",\\\"event-id\\\":\\\"EUwVOXbuVpHLbB16\\\",\\\"object-key\\\":\\\"batch/LATEST/EUwVOXbuVpHLbB36\\\",\\\"request-time\\\":\\\"2019-10-29T19:10:24.517Z\\\",\\\"event-name\\\":\\\"Object::BatchRemove\\\",\\\"description\\\":\\\"Batch Remove Collection\\\",\\\"event-version\\\":1,\\\"stage\\\":\\\"LATEST\\\",\\\"old-object-versions-to-keep\\\":3,\\\"additional-attributes\\\":{\\\"retry-indexes\\\":[1]}}\"}"
        with patch('lng_datalake_commons.error_handling.error_handler.additional_attributes', {'retry-indexes': [1]}):
            self.assertIsNone(process_retry_exception(RetryEventException(""),
                                                      io_utils.load_data_json('valid_event_with_seen_count.json')))
            mock_sqs_client.return_value.send_message.assert_called_with(DelaySeconds=126, MessageAttributes={
                'seen-count': {'DataType': 'Number', 'StringValue': '7'}},
                                                                         MessageBody=expected_call,
                                                                         QueueUrl='https://sqs.us-east-1.amazonaws.com/123456789012/mock-queue')

    # - fail process_retry_exception: with seen-count=1000 in messageAttributes
    def test_process_retry_exception_fail1(self):
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Event exceeded max retry count of 1000||RetryEventException||some issue'):
            process_retry_exception(RetryEventException("some issue"),
                                    io_utils.load_data_json('valid_event_with_seen_count_1000.json'))

    # - fail process_retry_exception: sqs send_message ClientError raises original RetryEventException
    @patch("lng_aws_clients.sqs.get_client")
    def test_process_retry_exception_fail2(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.side_effect = ClientError({"Error": {"Code": 101,
                                                                                       "Message": "Mock Client Error"}},
                                                                            "MockError")
        with self.assertRaisesRegex(RetryEventException, "some issue"):
            process_retry_exception(RetryEventException("some issue"), io_utils.load_data_json('valid_event.json'))


if __name__ == '__main__':
    print(unittest.main())
