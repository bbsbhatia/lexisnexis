import json
import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_commons import publish_sns_topic
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from create_object_event_handler import create_object_event_handler, lambda_handler, generate_object_store_item

__author__ = "Shekhar Ralhan, John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CreateObjectEventHandler")


class CreateObjectEventHandlerTest(TestCase):
    session_patch = None
    tracker_oldest_item_patch = None
    tracker_is_oldest_patch = None
    tracker_delete_patch = None

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com',
                             'LAMBDA_TASK_ROOT': os.path.dirname(io_utils.schema_path)})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.tracker_oldest_item_patch = patch(
            "lng_datalake_commons.tracking.tracker._get_oldest_item_by_tracking_id").start()
        cls.tracker_is_oldest_patch = patch("lng_datalake_commons.tracking.tracker._is_oldest_event").start()
        cls.tracker_delete_patch = patch("lng_datalake_commons.tracking.tracker._delete_tracking_item").start()

        cls.tracker_oldest_item_patch.return_value = {'event-id': 1}
        cls.tracker_is_oldest_patch.return_value = True
        # reload module to update globals that are set from environment variables
        reload(object_common_module)
        reload(publish_sns_topic)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        cls.tracker_oldest_item_patch.stop()
        cls.tracker_is_oldest_patch.stop()
        cls.tracker_delete_patch.stop()

    # +create_object_event_handler - new object for the datalake
    @patch('lng_datalake_dal.changeset_object_table.ChangesetObjectTable.put_item')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.copy_object_to_datalake')
    def test_create_object_event_handler_pass_new_object(self, copy_to_lake_mock, sns_publish_mock, mock_validate,
                                                         osvt_put_item_mock, ost_put_item_mock,
                                                         mock_get_catalog_ids_by_collection_id, mock_get_object_item,
                                                         mock_put_changeset_object):
        mock_get_object_item.return_value = {}
        copy_to_lake_mock.return_value = None
        osvt_put_item_mock.return_value = None
        ost_put_item_mock.return_value = None
        sns_publish_mock.return_value = None
        mock_put_changeset_object.return_value = None
        mock_validate.return_value = True
        mock_get_catalog_ids_by_collection_id.return_value = []
        event = io_utils.load_data_json("valid_cmd_changeset_event.json")
        with patch('create_object_event_handler.datalake_bucket_name', 'mock_datalake_bucket'), patch(
                'create_object_event_handler.target_arn', 'mock_target_arn'):
            self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
            self.assertEqual(copy_to_lake_mock.call_count, 2)
            sns_publish_data = {}
            schemas = ['Schemas/Publish/v0/create-object.json', 'Schemas/Publish/v1/create-object.json']
            message = io_utils.load_data_json("valid_message_matches_cmd_changeset_event.json")
            object_item = io_utils.load_data_json('valid_object_item.json')
            catalog_ids = []
            changeset_id = 'jek-changeset'
            sns_publish_mock.assert_called_with(sns_publish_data, object_item, schemas, message, 'mock_target_arn',
                                                catalog_ids, changeset_id)

    # +create_object_event_handler - new object for the datalake - with ingestion id
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.copy_object_to_datalake')
    def test_create_object_event_handler_pass_new_object_ingestion(self, copy_to_lake_mock,
                                                                   sns_publish_mock, mock_validate, osvt_put_item_mock,
                                                                   ost_put_item_mock,
                                                                   mock_get_catalog_ids_by_collection_id,
                                                                   update_ingestion_mock, mock_get_object_item):
        mock_get_object_item.return_value = {}
        copy_to_lake_mock.return_value = None
        osvt_put_item_mock.return_value = None
        ost_put_item_mock.return_value = None
        sns_publish_mock.return_value = None
        mock_validate.return_value = True
        mock_get_catalog_ids_by_collection_id.return_value = []
        update_ingestion_mock.return_value = None
        event = io_utils.load_data_json("valid_cmd_event_with_ingestion_id.json")
        with patch('create_object_event_handler.datalake_bucket_name', 'mock_datalake_bucket'), patch(
                'create_object_event_handler.target_arn', 'mock_target_arn'):
            self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
            copy_to_lake_mock.assert_not_called()
            sns_publish_data = {}
            schemas = ['Schemas/Publish/v0/create-object.json', 'Schemas/Publish/v1/create-object.json']
            message = io_utils.load_data_json("valid_message_matches_valid_cmd_event_with_ingestion_id.json")
            object_item = io_utils.load_data_json('valid_object_item.json')
            catalog_ids = []
            sns_publish_mock.assert_called_with(sns_publish_data, object_item, schemas, message, 'mock_target_arn',
                                                catalog_ids, '')

    # +create_object_event_handler - new object for the datalake - large object
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch('lng_datalake_dal.object_store_version_table.ObjectStoreVersionTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch('lng_aws_clients.sns.is_valid_topic_arn')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.put_item')
    @patch('lng_aws_clients.s3.get_client')
    def test_create_object_event_handler_pass_large_object(self, s3_get_client_mock,
                                                           ost_put_item_mock, mock_sns_publish,
                                                           is_valid_topic_arn_mock,
                                                           mock_validate, mock_ost_put_item,
                                                           mock_osvt_put_item,
                                                           mock_get_catalog_ids_by_collection_id,
                                                           mock_get_object_item):
        s3_get_client_mock.return_value.copy.return_value = None
        ost_put_item_mock.return_value = None
        is_valid_topic_arn_mock.return_value = True
        mock_validate.return_value = True
        mock_ost_put_item.return_value = None
        mock_osvt_put_item.return_value = None
        mock_get_object_item.return_value = {}
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_sns_publish.return_value = None
        event = io_utils.load_data_json("valid_cmd_event_large_object.json")
        self.assertEqual(
            create_object_event_handler(event, 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'),
            event_handler_status.SUCCESS)

    # + generate_object_store_item - Valid Request
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    def test_generate_object_store_item_pass(self, mock_get_object_item):
        mock_get_object_item.return_value = {}
        input_event = io_utils.load_data_json('valid_message.json')
        expected = io_utils.load_data_json('object_store_item_expected.json')
        bucket_name = "mock_datalake_bucket-use1"
        replicated_buckets = {'us-east-1': 'mock_datalake_bucket-use1', 'us-west-2': 'mock_datalake_bucket-usw2'}
        with patch('create_object_event_handler.datalake_bucket_name', bucket_name):
            self.assertDictEqual(generate_object_store_item(input_event, {}, replicated_buckets),
                                 expected)

    # - generate_object_store_item - missing required attribute
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    def test_generate_object_store_item_fail(self, mock_get_object_item):
        mock_get_object_item.return_value = {}
        input_event = io_utils.load_data_json('valid_message.json')
        input_event.pop('object-key')
        replicated_buckets = {'us-east-1': "bucket_name-use1"}
        self.assertRaisesRegex(TerminalErrorException, 'Missing required property', generate_object_store_item,
                               input_event, {}, replicated_buckets)

    # +create_object_event_handler: retry=ost_get(1)
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry1(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item, mock_process_staging_object,
                                                mock_insert_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications,
                                                mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 1}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_called_once()
        mock_process_staging_object.assert_called_once()
        mock_insert_object_store_table.assert_called_once()
        mock_insert_object_store_version_table.assert_called_once()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()
        mock_update_ingestion.assert_called_once()

    # +create_object_event_handler: retry=ost_get(1), no object changes
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry1_no_object_changes(self, mock_validate_message,
                                                                  mock_get_catalog_ids_by_collection_id,
                                                                  mock_get_consistent_object_item,
                                                                  mock_process_staging_object,
                                                                  mock_insert_object_store_table,
                                                                  mock_insert_object_store_version_table,
                                                                  mock_delete_object_store_version_table,
                                                                  mock_send_object_notifications,
                                                                  mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = True
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 1}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_called_once()
        mock_process_staging_object.assert_called_once()
        mock_insert_object_store_table.assert_called_once()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_not_called()
        mock_send_object_notifications.assert_not_called()
        mock_update_ingestion.assert_called_once()

    # +create_object_event_handler: retry=s3_copy(2)
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry2(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item, mock_process_staging_object,
                                                mock_insert_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications,
                                                mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 2,
                                            'object-data': {}}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_process_staging_object.assert_called_once()
        mock_insert_object_store_table.assert_called_once()
        mock_insert_object_store_version_table.assert_called_once()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()
        mock_update_ingestion.assert_called_once()

    # +create_object_event_handler: retry=ost_put(3)
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry3(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item, mock_process_staging_object,
                                                mock_insert_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications,
                                                mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 3,
                                            'object-data': {}}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_process_staging_object.assert_not_called()
        mock_insert_object_store_table.assert_called_once()
        mock_insert_object_store_version_table.assert_called_once()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()
        mock_update_ingestion.assert_called_once()

    # +create_object_event_handler: retry=osvt_put(4)
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry4(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item, mock_process_staging_object,
                                                mock_insert_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications,
                                                mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 4,
                                            'object-data': {}}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_process_staging_object.assert_not_called()
        mock_insert_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_called_once()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()
        mock_update_ingestion.assert_called_once()

    # +create_object_event_handler: retry=osvt_delete(5)
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry5(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item, mock_process_staging_object,
                                                mock_insert_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications,
                                                mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 5,
                                            'object-data': {}}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_process_staging_object.assert_not_called()
        mock_insert_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_called_once()
        mock_send_object_notifications.assert_called_once()
        mock_update_ingestion.assert_called_once()

    # +create_object_event_handler: retry=sns_publish(6)
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry6(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item, mock_process_staging_object,
                                                mock_insert_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications,
                                                mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 6,
                                            'object-data': {},
                                            'sns-publish-data': {'notifications': {}}}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_process_staging_object.assert_not_called()
        mock_insert_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_not_called()
        mock_send_object_notifications.assert_called_once()
        mock_update_ingestion.assert_called_once()

    # +create_object_event_handler: retry=ingestion_update(7)
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry7(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                mock_get_consistent_object_item, mock_process_staging_object,
                                                mock_insert_object_store_table, mock_insert_object_store_version_table,
                                                mock_delete_object_store_version_table,
                                                mock_send_object_notifications,
                                                mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 7,
                                            'object-data': {}}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_process_staging_object.assert_not_called()
        mock_insert_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_not_called()
        mock_send_object_notifications.assert_not_called()
        mock_update_ingestion.assert_called_once()

    # +create_object_event_handler: retry=done(99)
    @patch('service_commons.object_event_handler.update_ingestion')
    @patch('service_commons.object_event_handler.send_object_notifications')
    @patch('service_commons.object_event_handler.delete_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_version_table')
    @patch('service_commons.object_event_handler.insert_object_store_table')
    @patch('service_commons.object_event_handler.process_staging_object')
    @patch("service_commons.object_event_handler.get_consistent_object_item")
    @patch("lng_datalake_commons.publish_sns_topic.get_catalog_ids_by_collection_id")
    @patch("service_commons.object_event_handler.validate_message")
    def test_create_object_event_handler_retry99(self, mock_validate_message, mock_get_catalog_ids_by_collection_id,
                                                 mock_get_consistent_object_item, mock_process_staging_object,
                                                 mock_insert_object_store_table, mock_insert_object_store_version_table,
                                                 mock_delete_object_store_version_table,
                                                 mock_send_object_notifications,
                                                 mock_update_ingestion):
        mock_validate_message.return_value = True
        mock_process_staging_object.return_value = None
        mock_insert_object_store_table.return_value = False
        mock_get_catalog_ids_by_collection_id.return_value = []
        mock_get_consistent_object_item.return_value = {}
        mock_insert_object_store_version_table.return_value = None
        mock_delete_object_store_version_table.return_value = None
        mock_send_object_notifications.return_value = None
        mock_update_ingestion.return_value = None

        event = io_utils.load_data_json("valid_cmd_event.json")
        message = json.loads(event['Records'][0]['Sns']['Message'])
        message['additional-attributes'] = {'retry-state': 99,
                                            'object-data': {}}
        event['Records'][0]['Sns']['Message'] = json.dumps(message)

        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_get_consistent_object_item.assert_not_called()
        mock_process_staging_object.assert_not_called()
        mock_insert_object_store_table.assert_not_called()
        mock_insert_object_store_version_table.assert_not_called()
        mock_delete_object_store_version_table.assert_not_called()
        mock_send_object_notifications.assert_not_called()
        mock_update_ingestion.assert_not_called()


if __name__ == '__main__':
    unittest.main()
