import json
import os
import unittest
import zipfile
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from process_ingestion_zip_part import lambda_handler, process_ingestion_zip_part, create_tracking_records, \
    update_tracked_count, publish_sns_messages, process_retry_exception, process_terminal_error

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ProcessIngestionZipPart')


class TestProcessIngestionZipPart(unittest.TestCase):
    session_patch = None

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + process_ingestion_zip_part
    @patch('process_ingestion_zip_part.publish_sns_messages')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('process_ingestion_zip_part.create_tracking_records')
    @patch('service_commons.ingestion.read_zip_central_directory')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_part_valid_remove_blocker(self, mock_get_ingestion, mock_read_zip,
                                                             mock_create_tracking, mock_sqs,
                                                             mock_publish_message):
        event = io_util.load_data_json('valid_event_with_changeset.json')
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        mock_get_ingestion.return_value = io_util.load_data_json('get_ingestion_resp.json')
        mock_read_zip.return_value = z.infolist(), 12, 85022
        mock_create_tracking.return_value = None
        mock_sqs.return_value.send_message.return_value = None
        mock_publish_message.return_value = None

        self.assertEqual(process_ingestion_zip_part(event, MockLambdaContext()), event_handler_status.SUCCESS)

    # - process_ingestion_zip_part - inValid Event pending state
    @patch('service_commons.ingestion.update_error_counter')
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_part_invalid_pending(self, mock_get_ingestion, mock_terminal_error,
                                                        mock_error_counter):
        event = io_util.load_data_json('valid_event.json')
        zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        mock_get_ingestion.return_value = io_util.load_data_json('invalid_get_ingestion_resp_pending.json')
        mock_terminal_error.return_value = None
        mock_error_counter.return_value = None

        self.assertEqual(process_ingestion_zip_part(event, MockLambdaContext()), None)

        mock_terminal_error.assert_called_once()
        mock_error_counter.assert_called_once()

    # - process_ingestion_zip_part - Retryable Exception
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_part_retryable_exception(self, mock_get_ingestion):
        event = io_util.load_data_json('valid_event.json')

        mock_get_ingestion.side_effect = RetryEventException('TEST RetryEventException')

        with self.assertRaisesRegex(RetryEventException, "TEST RetryEventException"):
            process_ingestion_zip_part(event, MockLambdaContext())

    # - lambda_handler - Missing Key
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_lambda_handler_missing_key(self, mock_terminal_error):
        event = io_util.load_data_json('invalid_event.json')
        mock_terminal_error.return_value = None

        self.assertEqual(lambda_handler(event, MockLambdaContext()), None)

        mock_terminal_error.assert_called_once()

    # + create_tracking_records - Valid Event
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    def test_create_tracking_records_valid(self, mock_tracking_batch_write):
        mock_tracking_batch_write.return_value = {}
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        self.assertIsNone(create_tracking_records(z.infolist(), "dltest.zip", '2019-03-17T00:32:41.326Z', 1, 3, "abc", ''))

    # + create_tracking_records - Valid large Event
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    def test_create_tracking_records_valid_large(self, mock_tracking_batch_write):
        mock_tracking_batch_write.return_value = {}
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "largebundle.zip"))

        self.assertIsNone(create_tracking_records(z.infolist(), "largebundle.zip", '2019-03-17T00:32:41.326Z', 1, 51,
                                                  "abc", 'changeset-test'))
        self.assertEqual(mock_tracking_batch_write.call_count, 3)

    # - create_tracking_records - Unprocessed Items
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    def test_create_tracking_records_unprocessed(self, mock_tracking_batch_write):
        mock_tracking_batch_write.return_value = {'PutRequest': {'tracking-id': 'abc|101_object',
                                                                 'request-time': '2019-04-19T21:32:41.326Z',
                                                                 'event-id': '123456789',
                                                                 'event-name': 'Object::Update'}}
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        with self.assertRaisesRegex(RetryEventException, 'Unable to create all Tracking Table items'):
            create_tracking_records(z.infolist(), "dltest.zip", '2019-03-17T00:32:41.326Z', 1, 3, "abc", '')

    # - create_tracking_records - EndpointConnectionError
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    def test_create_tracking_records_endpoint_connection_error_exception(self, mock_tracking_batch_write):
        mock_tracking_batch_write.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        with self.assertRaisesRegex(RetryEventException, 'Unable to create items in Tracking Table'):
            create_tracking_records(z.infolist(), "dltest.zip", '2019-03-17T00:32:41.326Z', 1, 3, "abc", '')

    # - create_tracking_records - Client Error
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    def test_create_tracking_records_client_error(self, mock_tracking_batch_write):
        mock_tracking_batch_write.side_effect = ClientError({'ResponseMetadata': {},
                                                             'Error': {
                                                                 'Code': 'OTHER',
                                                                 'Message': 'This is a mock'}},
                                                            "FAKE")
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        with self.assertRaisesRegex(RetryEventException, 'Unable to create items in Tracking Table'):
            create_tracking_records(z.infolist(), "dltest.zip", '2019-03-17T00:32:41.326Z', 1, 3, "abc", '')

    # - create_tracking_records - Exception Error
    @patch('lng_datalake_dal.tracking_table.TrackingTable.batch_write_items')
    def test_create_tracking_records_exception(self, mock_tracking_batch_write):
        mock_tracking_batch_write.side_effect = Exception
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        with self.assertRaisesRegex(TerminalErrorException, 'Failed to create items in Tracking Table'):
            create_tracking_records(z.infolist(), "dltest.zip", '2019-03-17T00:32:41.326Z', 1, 3, "abc", '')

    # + update_tracked_count - valid update
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_track_count_valid(self, mock_sqs):
        mock_sqs.return_value.send_message.return_value = None
        self.assertIsNone(update_tracked_count('2c86d989-484c-11e9-9d9c-cf31f437a894', 3))

    # - update_tracked_count - invalid EndpointConnectionError
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_track_count_endpoint_connection_error_exception(self, sqs_mock):
        sqs_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(RetryEventException, 'Unable to publish'):
            update_tracked_count('2c86d989-484c-11e9-9d9c-cf31f437a894', 3)

    # - update_tracked_count - invalid client error
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_track_count_client_error(self, sqs_mock):
        sqs_mock.side_effect = ClientError({'ResponseMetadata': {},
                                            'Error': {
                                                'Code': 'OTHER',
                                                'Message': 'This is a mock'}},
                                           "FAKE")
        with self.assertRaisesRegex(RetryEventException, 'Unable to publish'):
            update_tracked_count('2c86d989-484c-11e9-9d9c-cf31f437a894', 3)

    # - update_tracked_count - invalid general exception
    @patch('lng_aws_clients.sqs.get_client')
    def test_update_track_count_general_exception(self, sqs_mock):
        sqs_mock.side_effect = Exception
        self.assertIsNone(update_tracked_count('2c86d989-484c-11e9-9d9c-cf31f437a894', 3))

    # + publish_sns_messages - Valid Event
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_sns_messages(self, mock_sns_client):
        mock_sns_client.return_value.publish.return_value = {'MessageId': "123456789"}

        ingestion_id = "dltest.zip"
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        message = {'ingestion-id': 'dltest.zip',
                   'bucket-name': 'feature-jek-dl-object-staging-288044017584-use1',
                   'zip-key': 'ingestion/LATEST/dltest.zip',
                   'content-type': 'application/xml',
                   'old-object-versions-to-keep': 5,
                   'pending-expiration-epoch': 0,
                   'stage': 'LATEST'}

        self.assertIsNone(publish_sns_messages(ingestion_id, z.infolist(), 1, 3, 12345, message, []))

    # - publish_sns_messages - Retry Exception
    @patch('lng_aws_clients.sns.get_client')
    def test_publish_sns_messages_exception(self, mock_sns_client):
        mock_sns_client.return_value.publish.side_effect = [{'MessageId': "123456789"},
                                                            Exception,
                                                            Exception]

        ingestion_id = "dltest.zip"
        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        message = {'ingestion-id': 'dltest.zip',
                   'bucket-name': 'feature-jek-dl-object-staging-288044017584-use1',
                   'zip-key': 'ingestion/LATEST/dltest.zip',
                   'content-type': 'application/xml',
                   'old-object-versions-to-keep': 5,
                   'pending-expiration-epoch': 0,
                   'stage': 'LATEST'}
        with self.assertRaisesRegex(RetryEventException, "Error publishing 2 messages for indexes \[2, 3\]"):
            publish_sns_messages(ingestion_id, z.infolist(), 1, 3, 12345, message, [])
        self.assertIn('retry-indexes', error_handler.additional_attributes)
        self.assertEqual(error_handler.additional_attributes['retry-indexes'], [2, 3])

    # + successful process_retry_exception: no seen-count in messageAttributes
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_success1(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None

        self.assertIsNone(process_retry_exception(RetryEventException(""),
                                                  io_util.load_data_json('valid_event.json')))

    # + successful process_retry_exception: with seen-count in messageAttributes
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_success2(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.return_value = None

        self.assertIsNone(process_retry_exception(RetryEventException(""),
                                                  io_util.load_data_json('valid_event_with_seen_count.json')))

    # - fail process_retry_exception: with seen-count=1000 in messageAttributes
    def test_process_retry_exception_fail1(self):
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Event exceeded max retry count of 1000\|\|RetryEventException\|\|some issue'):
            process_retry_exception(RetryEventException("some issue"),
                                    io_util.load_data_json('valid_event_with_seen_count1000.json'))

    # - fail process_retry_exception: sqs send_message ClientError raises original RetryEventException
    @patch('lng_aws_clients.sqs.get_client')
    def test_process_retry_exception_fail2(self, mock_sqs_client):
        mock_sqs_client.return_value.send_message.side_effect = ClientError({"Error": {"Code": 101,
                                                                                       "Message": "Mock Client Error"}},
                                                                            "MockError")
        with self.assertRaisesRegex(RetryEventException, "some issue"):
            process_retry_exception(RetryEventException("some issue"), io_util.load_data_json('valid_event.json'))

    # + successful process_terminal_error
    @patch('lng_datalake_commons.error_handling.error_handler.terminal_error')
    def test_process_terminal_error_success(self, mock_error_handler_terminal_error):
        mock_error_handler_terminal_error.return_value = None

        self.assertIsNone(process_terminal_error(RetryEventException(""),
                                                 io_util.load_data_json('valid_event.json'),
                                                 MockLambdaContext(),
                                                 {}, 0, 0))
        mock_error_handler_terminal_error.assert_called_once()

    # + retry=sns_publish(2) with retry-indexes
    @patch('process_ingestion_zip_part.publish_sns_message')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('process_ingestion_zip_part.create_tracking_records')
    @patch('service_commons.ingestion.read_zip_central_directory')
    @patch('service_commons.ingestion.get_ingestion_data')
    def test_process_ingestion_zip_part_retry2(self, mock_get_ingestion, mock_read_zip,
                                               mock_create_tracking, mock_sqs, mock_sns_client,
                                               mock_publish_message):
        event = io_util.load_data_json('valid_event.json')
        # add additional-attributes to event
        message_body = json.loads(event["Records"][0]["body"])
        message = json.loads(message_body["Message"])
        message['additional-attributes'] = {'retry-state': 20, 'retry-indexes': [1]}
        message_body["Message"] = json.dumps(message)
        event["Records"][0]["body"] = json.dumps(message_body)

        z = zipfile.ZipFile(os.path.join(io_util.data_path, "dltest.zip"))

        mock_get_ingestion.return_value = io_util.load_data_json('get_ingestion_resp.json')
        mock_read_zip.return_value = z.infolist(), 12, 85022
        mock_create_tracking.return_value = None
        mock_sqs.return_value.send_message.return_value = None
        mock_sns_client.return_value = None
        mock_publish_message.return_value = -1

        self.assertEqual(process_ingestion_zip_part(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_create_tracking.assert_not_called()
        mock_publish_message.assert_called_once()


if __name__ == '__main__':
    unittest.main()
