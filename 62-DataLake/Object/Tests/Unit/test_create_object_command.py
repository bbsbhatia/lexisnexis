import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, NotAuthorizedError, \
    SemanticError, InvalidRequestPropertyName, NoSuchCollection, UNSUPPORTED_MEDIA_TYPE
from lng_datalake_constants import collection_status, event_names
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from create_object_command import create_object_command, lambda_handler, process_large_object_request, \
    generate_response_json

__author__ = "Shekhar Ralhan"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateObjectCommand')


class TestCreateObjectCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        cls.collection_blocker_patch = patch(
            "lng_datalake_commons.tracking.tracker._validate_collection").start()
        cls.collection_blocker_patch.return_value = None
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # Schema validation error using lambda_handler with command_wrapper decorator
    # - request missing object-id required property
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_command_schema_validation_fail(self, tt_put_mock):
        tt_put_mock.return_value = None
        request_input = io_util.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    # + Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('service_commons.object_command.validate_create_event')
    def test_create_object_command_success(self, validate_event_mock, tt_put_mock, ct_get_item_mock,
                                           mock_owner_authorization, ost_get_item_mock, put_s3_object_mock,
                                           est_put_item_mock, mock_changeset_get_item):
        tt_put_mock.return_value = None
        validate_event_mock.return_value = None
        mock_owner_authorization.return_value = {}
        ost_get_item_mock.return_value = {}
        est_put_item_mock.return_value = False
        ct_get_item_mock.return_value = \
            {
                "collection-id": "274",
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1
            }
        mock_changeset_get_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # + Successful test using lambda_handler with command_wrapper decorator for large object
    @patch('service_commons.object_command.validate_create_event')
    @patch('service_commons.object_common.generate_object_expiration_time')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_create_object_command_large_object_success(self, tt_put_mock, ct_get_item_mock, mock_owner_authorization,
                                                        ost_get_item_mock, put_s3_object_mock, s3_client_mock,
                                                        est_put_item_mock,
                                                        time_helper_mock,
                                                        validate_event_mock):
        tt_put_mock.return_value = None
        mock_owner_authorization.return_value = {}
        validate_event_mock.return_value = None
        time_helper_mock.return_value = 1527276677
        ost_get_item_mock.return_value = {}
        est_put_item_mock.return_value = False
        s3_client_mock.return_value.generate_presigned_url.return_value = "https://mock_presigned_url"
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1,
                'object-expiration': '7d'
            }
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_largeobject_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_largeobject_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # + base64 encoding
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('service_commons.object_command.validate_create_event')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_create_object_command_base64_encoding_success(self, tt_put_mock, ct_get_item_mock,
                                                           mock_owner_authorization,
                                                           ost_get_item_mock, put_s3_object_mock, est_put_item_mock,
                                                           validate_event_mock, mock_changeset_get_item):
        tt_put_mock.return_value = None

        mock_owner_authorization.return_value = {}
        ost_get_item_mock.return_value = {}
        est_put_item_mock.return_value = False
        validate_event_mock.return_value = None

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                "old-object-versions-to-keep": -1
            }
        mock_changeset_get_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        request_input['request']['body'] = "VGhpcyBpcyBzb21lIHRlc3QgdGV4dC4="
        request_input['request']['body-encoding'] = "base64"
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        response_output['response']['object']['temporary-object-key-url'] = "https://datalake_url.com/objects/temp/431bf3b995a99c2cd6899b97187d1542a965cec9/88cfa74de25b964b6e596db713b411aaf5cf0eef"
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                             response_output)

    # -Failed test of Lambda - invalid content-typ
    def test_create_object_command_invalid_content_type(self):
        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        request = request_input['request']
        request['content-type'] = 'invalid/content'
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        with self.assertRaisesRegex(UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"):
            create_object_command(request, request_id, stage, api_key_id)

    # -Failed test of Lambda - owner authorization error (wrong API Key)
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_object_command_owner_auth(self, ct_get_item_mock, mock_owner_authorization,
                                              mock_changeset_get_item):
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        mock_changeset_get_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        with self.assertRaisesRegex(NotAuthorizedError, "API Key provided is not valid for Owner ID"):
            create_object_command(request, request_id, stage, api_key_id)

    @patch('service_commons.object_command.validate_create_event')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_object_command_encoding_fail(self, ct_get_item_mock, mock_owner_authorization, ost_get_item_mock,
                                                 validate_event_mock):
        mock_owner_authorization.return_value = {}
        ost_get_item_mock.return_value = {"object-id": "fake_object",
                                          "object-state": "Created"}
        validate_event_mock.return_value = None
        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }

        request_input = io_util.load_data_json('apigateway.request.bad_encoding.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        self.assertRaises(InternalError, create_object_command, request, request_id, stage, api_key_id)

    # - Large object no md5 header
    def test_process_large_object_request_no_md5_header(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Content-MD5 does not exist in the request"):
            process_large_object_request({'request': 'blank'}, 'object-id', {}, 'object-key', 'abc-xyz', 'LATEST', 0)

    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.item_exists')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_object_command_collection_response_client_error(self, ct_get_item_mock, mock_owner_authorization,
                                                                    ost_query_items_mock, ost_item_exists_mock,
                                                                    put_s3_object_mock):
        mock_owner_authorization.return_value = {}
        ost_item_exists_mock.return_value = False

        ct_get_item_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        ost_query_items_mock.return_value = []
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        self.assertRaises(InternalError, create_object_command, request, request_id, stage, api_key_id)

    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.item_exists')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_object_command_collection_response_general_exception(self, ct_get_item_mock,
                                                                         mock_owner_authorization,
                                                                         ost_query_items_mock, ost_item_exists_mock,
                                                                         put_s3_object_mock):
        mock_owner_authorization.return_value = {}
        ost_item_exists_mock.return_value = False

        ct_get_item_mock.side_effect = Exception
        ost_query_items_mock.return_value = []
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        self.assertRaises(InternalError, create_object_command, request, request_id, stage, api_key_id)

    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.item_exists')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_object_command_collection_response_empty(self, ct_get_item_mock, mock_owner_authorization,
                                                             ost_query_items_mock,
                                                             ost_item_exists_mock, put_s3_object_mock):
        mock_owner_authorization.return_value = {}
        ost_item_exists_mock.return_value = False

        ct_get_item_mock.return_value = []

        ost_query_items_mock.return_value = []
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        self.assertRaises(NoSuchCollection, create_object_command, request, request_id, stage, api_key_id)

    # - create_object_command: object already exists
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    @patch('service_commons.object_command.get_previous_and_current_events')
    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_object_command_ost_item_exists_fail(self, ct_get_item_mock, mock_owner_authorization,
                                                        ost_get_item_mock, put_s3_object_mock,
                                                        mock_get_previous_pendng_event, mock_changeset_get_item):
        mock_owner_authorization.return_value = {}
        ost_get_item_mock.return_value = {"object-id": "fake_object", "object-state": "Created"}
        mock_get_previous_pendng_event.return_value = {}, {'tracking-id': 'object-id.fake_object',
                                                           'event-id': 'current_event_id',
                                                           'request-time': '2018-05-19T20:01:29.000Z',
                                                           'event-name': event_names.OBJECT_CREATE}

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        mock_changeset_get_item.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': 'Opened'
        }
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        self.assertRaises(SemanticError, create_object_command, request, request_id, stage, api_key_id)

    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.item_exists')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_object_command_ost_item_exists_client_error_fail(self, ct_get_item_mock, mock_owner_authorization,
                                                                     ost_query_items_mock, ost_item_exists_mock,
                                                                     put_s3_object_mock):
        mock_owner_authorization.return_value = {}
        ost_item_exists_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                        'Error': {
                                                            'Code': 'OTHER',
                                                            'Message': 'This is a mock'}},
                                                       "FAKE")

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_query_items_mock.return_value = []
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        self.assertRaises(InternalError, create_object_command, request, request_id, stage, api_key_id)

    @patch('lng_aws_clients.s3.put_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.item_exists')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_create_object_command_ost_item_exists_general_exception_fail(self, ct_get_item_mock,
                                                                          mock_owner_authorization,
                                                                          ost_query_items_mock, ost_item_exists_mock,
                                                                          put_s3_object_mock):
        mock_owner_authorization.return_value = {}
        ost_item_exists_mock.side_effect = Exception

        ct_get_item_mock.return_value = \
            {
                "collection-id": '274',
                "collection-state": collection_status.CREATED,
                "collection-timestamp": "2017-12-06T20:33:37.618Z",
                "collection-name": "Test Collection",
                "owner-id": 100,
                "asset-id": 62,
                'old-object-versions-to-keep': -1
            }
        ost_query_items_mock.return_value = []
        put_s3_object_mock.return_value = {'VersionId': 'versionId'}

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')

        request = request_input['request']
        request_id = request_input['context']['client-request-id']
        stage = request_input['context']['stage']
        api_key_id = request_input['context']['api-key-id']

        self.assertRaises(InternalError, create_object_command, request, request_id, stage, api_key_id)

    # - generate_response_json - Missing required attribute
    def test_generate_response_json_fail(self):
        with self.assertRaisesRegex(InternalError, 'Missing required property'):
            generate_response_json({}, '1', 'abc', '1', {})


if __name__ == '__main__':
    unittest.main()
