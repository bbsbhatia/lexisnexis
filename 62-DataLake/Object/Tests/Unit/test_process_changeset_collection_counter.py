import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from process_changeset_collection_counter import retry_message,\
    update_changeset_collection_item, process_retry_message, build_and_process_changeset_collection_items, \
    build_changeset_collection_counters, build_records_to_process, lambda_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, "ProcessChangesetCollectionCounter")


class TestProcessChangesetCollectionCounter(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + lambda_handler: success dynamo event
    @patch("process_changeset_collection_counter.build_and_process_changeset_collection_items")
    @patch("process_changeset_collection_counter.build_changeset_collection_counters")
    @patch("process_changeset_collection_counter.build_records_to_process")
    def test_lambda_handler_dynamo(self, mock_build_records_to_process,
                                   mock_build_changeset_collection_counters,
                                   mock_build_and_process_changeset_collection_items):
        event = io_util.load_data_json('dynamo_stream_event.json')
        mock_build_records_to_process.return_value = io_util.load_data_json('records_to_process_valid.json')
        mock_build_changeset_collection_counters.return_value = {"1|274|1": 2}
        mock_build_and_process_changeset_collection_items.return_value = None
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)

    # + lambda_handler: success sqs event
    @patch("process_changeset_collection_counter.process_retry_message")
    def test_lambda_handler_sqs(self, mock_process_retry_message):
        event = io_util.load_data_json('sqs_event.json')
        mock_process_retry_message.return_value = None
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)

    # - lambda_handler: terminal exception
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    @patch("process_changeset_collection_counter.build_records_to_process")
    def test_lambda_handler_terminal(self, mock_build_records_to_process, mock_terminal_error):
        event = io_util.load_data_json('dynamo_stream_event.json')
        mock_build_records_to_process.side_effect = Exception("Mock Exception")
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_terminal_error.assert_called_once()

    # + build_records_to_process: dynamo stream
    def test_build_records_to_process_dynamo(self):
        event = io_util.load_data_json('dynamo_stream_event.json')
        records = event["Records"]
        expected_response = io_util.load_data_json('records_to_process_valid.json')
        self.assertListEqual(build_records_to_process(records), expected_response)

    # + build_records_to_process: sqs
    @patch("process_changeset_collection_counter.process_retry_message")
    def test_build_records_to_process_sqs(self, mock_process_retry_message):
        event = io_util.load_data_json('sqs_event.json')
        records = event["Records"]
        mock_process_retry_message.return_value = None
        self.assertListEqual(build_records_to_process(records), [])
        mock_process_retry_message.assert_called_with(records[0])

    # - build_records_to_process: exception not a dynamostream or sqs record
    def test_build_records_to_process_exception(self):
        event = io_util.load_data_json('invalid_event.json')
        records = event["Records"]
        with self.assertRaisesRegex(TerminalErrorException, 'message not in acceptable format'):
            build_records_to_process(records)

    # - build_records_to_process: exception
    def test_build_records_to_process_exception_2(self):
        event = io_util.load_data_json('dynamo_stream_event_invalid.json')
        records = event["Records"]
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception occurred'):
            build_records_to_process(records)

    # + build_changeset_collection_counters: success
    def test_build_changeset_collection_counters(self):
        records_to_process = io_util.load_data_json('records_to_process_valid.json')
        expected_response = {"1|274|1": 2}
        self.assertDictEqual(build_changeset_collection_counters(records_to_process), expected_response)

    # - build_changeset_collection_counters: exception
    def test_build_changeset_collection_counters_exception(self):
        records_to_process = io_util.load_data_json('records_to_process_invalid.json')
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception occurred'):
            build_changeset_collection_counters(records_to_process)

    # + build_and_process_changeset_collection_items: success
    @patch("process_changeset_collection_counter.update_changeset_collection_item")
    def test_build_and_process_changeset_collection_items(self, mock_update):
        changeset_collection_counters = {"2|277|123": 3,
                                         "2|276|146": 5}
        mock_update.return_value = None
        self.assertIsNone(build_and_process_changeset_collection_items(changeset_collection_counters))

    # - build_and_process_changeset_collection_items: retry exception
    @patch("process_changeset_collection_counter.retry_message")
    @patch("process_changeset_collection_counter.update_changeset_collection_item")
    def test_build_and_process_changeset_collection_items_retry(self, mock_update, mock_retry_message):
        changeset_collection_counters = {"2|277|123": 3}
        mock_update.side_effect = ClientError({'ResponseMetadata': {},
                                               'Error': {
                                                   'Code': 'OTHER',
                                                   'Message': 'This is a mock'}},
                                              "FAKE")
        self.assertIsNone(build_and_process_changeset_collection_items(changeset_collection_counters))
        changeset_collection_item = {
            'changeset-id': '2',
            'collection-id': '277',
            'objects-processed': 3,
            'pending-expiration-epoch': '123'
        }
        mock_retry_message.assert_called_with(changeset_collection_item)

    # - build_and_process_changeset_collection_items: exception
    @patch("process_changeset_collection_counter.retry_message")
    @patch("process_changeset_collection_counter.update_changeset_collection_item")
    def test_build_and_process_changeset_collection_items_exception(self, mock_update, mock_retry_message):
        changeset_collection_counters = {"2|277|123": 3}
        mock_update.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception occurred'):
            build_and_process_changeset_collection_items(changeset_collection_counters)

    # + process_retry_message: success
    @patch("process_changeset_collection_counter.update_changeset_collection_item")
    def test_process_retry_message(self, mock_update):
        retry_msg = io_util.load_data_json('sqs_message_valid.json')
        mock_update.return_value = None
        self.assertIsNone(process_retry_message(retry_msg))

    # - process_retry_message: too many retries
    def test_process_retry_message_retry_limit_reached(self):
        retry_msg = io_util.load_data_json('sqs_message_retry_limit_reached.json')
        with self.assertRaisesRegex(TerminalErrorException, 'Max retry limit reached'):
            process_retry_message(retry_msg)

    # - process_retry_message: retry exception
    @patch("process_changeset_collection_counter.retry_message")
    @patch("process_changeset_collection_counter.update_changeset_collection_item")
    def test_process_retry_message_retry_exception(self, mock_update, mock_retry_message):
        retry_msg = io_util.load_data_json('sqs_message_valid.json')
        mock_update.side_effect = ClientError({'ResponseMetadata': {},
                                               'Error': {
                                                   'Code': 'OTHER',
                                                   'Message': 'This is a mock'}},
                                              "FAKE")
        changeset_collection_item = {
            'changeset-id': '2',
            'collection-id': '277',
            'objects-processed': 1,
            'pending-expiration-epoch': 1
        }
        self.assertIsNone(process_retry_message(retry_msg))
        mock_retry_message.assert_called_with(changeset_collection_item, 3)

    # - process_retry_message: exception
    @patch("process_changeset_collection_counter.update_changeset_collection_item")
    def test_process_retry_message_exception(self, mock_update):
        retry_msg = io_util.load_data_json('sqs_message_valid.json')
        mock_update.side_effect = Exception("Mock exception")
        with self.assertRaisesRegex(TerminalErrorException, 'Mock exception'):
            process_retry_message(retry_msg)

    # + update_changeset_collection_item: success
    @patch("lng_datalake_dal.changeset_collection_table.ChangesetCollectionTable.update_counters")
    def test_update_changeset_collection_item(self, mock_changeset_collection_update):
        changeset_collection_item = {
            'changeset-id': '2',
            'collection-id': '277',
            'objects-processed': 1,
            'pending-expiration-epoch': 123
        }
        mock_changeset_collection_update.return_value = None
        self.assertIsNone(update_changeset_collection_item(changeset_collection_item))

    # + retry_message - Valid Event
    @patch('lng_aws_clients.sqs.get_client')
    def test_retry_message_valid(self, mock_sqs):
        mock_sqs.return_value.send_message.return_value = None
        self.assertIsNone(retry_message({"changeset-id": "1", "collection-id": "275", "objects-processed": 1}))

    # - retry_message - Exception
    @patch('lng_aws_clients.sqs.get_client')
    def test_retry_message_exception(self, mock_sqs):
        mock_sqs.return_value.send_message.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to send retry message'):
            retry_message({"changeset-id": "1", "collection-id": "275", "objects-processed": 1})


if __name__ == '__main__':
    unittest.main()
