import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, SemanticError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import service_commons.object_common as object_common_module
import finish_folder_upload_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'FinishFolderUploadCommand')


class TestFinishFolderUploadCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        TableCache.clear()
        reload(finish_folder_upload_command)
        reload(object_common_module)
        cls.maxDiff = None

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()
        reload(finish_folder_upload_command)

    # +finish_folder_upload_command - Valid CreateObject
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.query_items')
    @patch('service_commons.object_common.generate_object_expiration_time')
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_aws_clients.session.set_session')
    def test_finish_folder_upload_command_create(self, session_mock, mock_owner_authorization, mock_collection_get,
                                                 mock_s3_get_client, time_helper_mock, mock_tracking_query,
                                                 mock_tracking_put, mock_est_put):
        time_helper_mock.return_value = 1527276677
        mock_est_put.return_value = None
        # Fake function for mocking multiple get_item calls to different table

        mock_owner_authorization.return_value = {}

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        # The mock-outs
        session_mock.return_value = None
        mock_collection_get.return_value = io_util.load_data_json('dynamodb.collection_data_valid.json')
        mock_tracking_query.return_value = [
            {"tracking_id": "1010101|274_object", "request-time": "123", "event-id": "123",
             "pending-expiration-epoch": 123, 'event-name': "Object::FolderUploadCreate"}]
        mock_tracking_put.return_value = None
        mock_s3_get_client.return_value.list_objects_v2.return_value = io_util.load_data_json('s3.valid_list.json')
        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_valid.json')

        with patch('finish_folder_upload_command.staging_bucket_name', 'bucket_name'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                self.assertDictEqual(
                    finish_folder_upload_command.lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                    expected_response)

    # - get_tracking_record - Invalid ClientError
    @patch('lng_datalake_dal.tracking_table.TrackingTable.query_items')
    def test_get_tracking_record_client_error(self, mock_tracking_query):
        mock_tracking_query.side_effect = ClientError({"ResponseMetadata": {}, "Error": {"Code": "mock error code",
                                                                                         "Message": "mock error message"}},
                                                      "client-error-mock")
        with self.assertRaisesRegex(InternalError, 'Unable to query items in Tracking Table by Object ID '):
            finish_folder_upload_command.get_tracking_record('object-id', 'collection-id', "mock_event")

    # - get_tracking_record - Invalid Exception
    @patch('lng_datalake_dal.tracking_table.TrackingTable.query_items')
    def test_get_tracking_record_general_exception(self, mock_tracking_query):
        mock_tracking_query.side_effect = Exception("mock error")
        with self.assertRaisesRegex(InternalError, 'Unhandled exception occurred'):
            finish_folder_upload_command.get_tracking_record('object-id', 'collection-id', 'mock_event')

    # - get_tracking_record - Invalid no tracking record
    @patch('lng_datalake_dal.tracking_table.TrackingTable.query_items')
    def test_get_tracking_record_no_tracking_record(self, mock_tracking_query):
        mock_tracking_query.return_value = []
        with self.assertRaisesRegex(SemanticError, 'No open transactions for Object ID'):
            finish_folder_upload_command.get_tracking_record('object-id', 'collection-id', 'mock_event')

    # -check_upload_directory - Invalid no objects in directory
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_check_upload_directory_error_1(self, session_mock, mock_s3_get_client):
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        # The mock-outs
        session_mock.return_value = None

        mock_s3_get_client.return_value.list_objects_v2.return_value = {}

        with patch('finish_folder_upload_command.staging_bucket_name', 'bucket_name'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                with self.assertRaisesRegex(SemanticError, "No objects have been uploaded for transaction-id:"):
                    finish_folder_upload_command.check_upload_directory("staging_key", "trans-id")

    # -check_upload_directory - Invalid
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_check_upload_directory_error_2(self, session_mock, mock_s3_get_client):
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        # The mock-outs
        session_mock.return_value = None

        mock_s3_get_client.side_effect = ClientError({"ResponseMetadata": {}, "Error": {"Code": "mock error code",
                                                                                        "Message": "mock error message"}},
                                                     "client-error-mock")

        with patch('finish_folder_upload_command.staging_bucket_name', 'bucket_name'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                with self.assertRaisesRegex(InternalError, "Unable to read s3 location"):
                    finish_folder_upload_command.check_upload_directory("staging_key", "trans-id")

    # -check_upload_directory - Unhandled Exception
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_check_upload_directory_error_3(self, session_mock, mock_s3_get_client):
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        # The mock-outs
        session_mock.return_value = None

        mock_s3_get_client.side_effect = Exception("Mock error")

        with patch('finish_folder_upload_command.staging_bucket_name', 'bucket_name'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                with self.assertRaisesRegex(InternalError, "Mock error"):
                    finish_folder_upload_command.check_upload_directory("staging_key", "trans-id")

    # - finish_folder_upload_command - Stages not matching
    @patch('lng_aws_clients.session.set_session')
    def test_finish_folder_upload_command_invalid_stage(self, mock_session):
        mock_session.return_value = None
        request_input = {
            "transaction-id": "eyJ0cmFuc2FjdGlvbi1pZCI6ICJwb0ladlFVU0xJbW1tQUJhIiwgImNvbGxlY3Rpb24taWQiOiAiMSIsICJvd25lci1pZCI6IDEwMCwgInN0YWdlIjogIkxBVEVTVCJ9",
            "object-id": "foo"
        }
        with self.assertRaisesRegex(SemanticError, "does not match the stage of the transaction creation"):
            finish_folder_upload_command.finish_folder_upload_command(request_input, 'v1', "testApiKeyId")

    # - generate_response_dict: key error on collection-id
    def test_generate_response_missing_collection_id(self):
        with self.assertRaisesRegex(InternalError, "Missing required property: 'collection-id'"):
            finish_folder_upload_command.generate_response_dict({}, 'object-id', 'STAGE', {}, 'staging-prefix')

    # - put_updated_tracking_record: exception
    @patch('lng_datalake_dal.tracking_table.TrackingTable.update_item')
    def test_put_updated_tracking_record_exception(self, mock_tracking_update):
        mock_tracking_update.side_effect = Exception("Mock error")
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            finish_folder_upload_command.update_tracking_record({"tracking-id": "mock"})

    # - put_updated_tracking_record: client error
    @patch('lng_datalake_dal.tracking_table.TrackingTable.update_item')
    def test_put_updated_tracking_record_client_error(self, mock_tracking_update):
        mock_tracking_update.side_effect = ClientError({"ResponseMetadata": {}, "Error": {"Code": "mock error code",
                                                                                          "Message": "mock error message"}},
                                                       "client-error-mock")
        with self.assertRaisesRegex(InternalError, "client-error-mock"):
            finish_folder_upload_command.update_tracking_record({"tracking-id": "mock"})


if __name__ == '__main__':
    unittest.main()
