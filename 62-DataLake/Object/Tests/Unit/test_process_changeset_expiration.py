import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
from lng_datalake_testhelper.io_utils import IOUtils
from process_changeset_expiration import lambda_handler, filter_records_to_process, process_changeset_items,\
    process_retry_messages, retry_message

__author__ = "Daniel Wang"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, "ProcessChangesetExpiration")


class TestProcessChangesetExpiration(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + lambda_handler: success dynamo event
    @patch("process_changeset_expiration.process_retry_messages")
    @patch("process_changeset_expiration.process_changeset_items")
    @patch("process_changeset_expiration.filter_records_to_process")
    def test_lambda_handler_dynamo(self, mock_filter_records_to_process, mock_process_changeset_items, mock_process_retry_messages):
        event = io_util.load_data_json('dynamo_stream_event.json')
        fake_records_to_process = io_util.load_data_json('records_to_process_valid.json')
        mock_filter_records_to_process.return_value = (fake_records_to_process, [])
        mock_process_changeset_items.return_value = None
        mock_process_retry_messages.return_value = None
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)

    # + lambda_handler: success sqs event
    @patch("process_changeset_expiration.process_retry_messages")
    @patch("process_changeset_expiration.process_changeset_items")
    @patch("process_changeset_expiration.filter_records_to_process")
    def test_lambda_handler_sqs(self, mock_filter_records_to_process, mock_process_changeset_items, mock_process_retry_messages):
        event = io_util.load_data_json('sqs_event.json')
        fake_messages_to_process = io_util.load_data_json('messages_to_process_valid.json')
        mock_filter_records_to_process.return_value = ([], fake_messages_to_process)
        mock_process_changeset_items.return_value = None
        mock_process_retry_messages.return_value = None
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)

    # - lambda_handler: terminal exception
    @patch("lng_datalake_commons.error_handling.error_handler.terminal_error")
    @patch("process_changeset_expiration.filter_records_to_process")
    def test_lambda_handler_terminal(self, mock_filter_records_to_process, mock_terminal_error):
        event = io_util.load_data_json('dynamo_stream_event.json')
        mock_filter_records_to_process.side_effect = Exception("Mock Exception")
        self.assertEqual(lambda_handler(event, MockLambdaContext()), event_handler_status.SUCCESS)
        mock_terminal_error.assert_called_once()

    # + filter_records_to_process: dynamo stream
    def test_filter_records_to_process_dynamo(self):
        event = io_util.load_data_json('dynamo_stream_event.json')
        records = event["Records"]
        expected_response = (io_util.load_data_json('records_to_process_valid.json'), [])
        self.assertTupleEqual(filter_records_to_process(records, MockLambdaContext().aws_request_id), expected_response)

    # + filter_records_to_process: sqs
    @patch("process_changeset_expiration.process_retry_messages")
    def test_filter_records_to_process_sqs(self, mock_process_retry_messages):
        event = io_util.load_data_json('sqs_event.json')
        records = event["Records"]
        expected_response = ([], io_util.load_data_json('messages_to_process_valid.json'))
        self.assertTupleEqual(filter_records_to_process(records, MockLambdaContext().aws_request_id), expected_response)

    # - filter_records_to_process: exception not a dynamostream or sqs record
    def test_filter_records_to_process_exception(self):
        event = io_util.load_data_json('invalid_event.json')
        records = event["Records"]
        with self.assertRaisesRegex(TerminalErrorException, 'Input must be in the form of'):
            filter_records_to_process(records, MockLambdaContext().aws_request_id)

    # - filter_records_to_process: exception
    def test_filter_records_to_process_unhandled_exception(self):
        event = io_util.load_data_json('dynamo_stream_event_invalid.json')
        records = event["Records"]
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception occurred'):
            filter_records_to_process(records, MockLambdaContext().aws_request_id)

    # + process_changeset_items: success
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_process_changeset_items(self, mock_put):
        records_to_process = io_util.load_data_json('records_to_process_valid.json')
        mock_put.return_value = None
        self.assertIsNone(process_changeset_items(records_to_process))

    # - process_changeset_items: retry exception
    @patch('process_changeset_expiration.retry_message')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_process_changeset_items_retry(self, mock_put, mock_retry_message):
        records_to_process = io_util.load_data_json('records_to_process_valid.json')
        mock_put.side_effect = ClientError({'ResponseMetadata': {},
                                            'Error': {
                                                'Code': 'OTHER',
                                                'Message': 'This is a mock'
                                            }}, "FAKE")
        self.assertIsNone(process_changeset_items(records_to_process))
        changeset_item = {
            "changeset-timestamp": "2019-06-03T16:09:33.861Z",
            "changeset-id": "test-changeset1",
            "changeset-state": "Expired",
            "description": "test description",
            "owner-id": 1677,
            "pending-expiration-epoch": 1605445785,
            "event-ids": ["14dc7e9e-2d81-41e0-bd81-303053250ec9", "563r1jug9i"],
        }
        mock_retry_message.assert_called_with(changeset_item)

    # - process_changeset_items: exception
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_process_changeset_items_exception(self, mock_put):
        records_to_process = io_util.load_data_json('records_to_process_valid.json')
        mock_put.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(TerminalErrorException, "Unhandled exception occurred"):
            process_changeset_items(records_to_process)

    # + process_retry_messages: success
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_process_retry_messages(self, mock_put):
        retry_msg = io_util.load_data_json('sqs_message_valid.json')
        mock_put.return_value = None
        self.assertIsNone(process_retry_messages(retry_msg))

    # - process_retry_messages: too many retries
    def test_process_retry_messages_retry_limit_reached(self):
        retry_msg = io_util.load_data_json('sqs_message_retry_limit_reached.json')
        with self.assertRaisesRegex(TerminalErrorException, 'Max retry limit reached'):
            process_retry_messages(retry_msg)

    # - process_retry_message: retry exception
    @patch('process_changeset_expiration.retry_message')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_process_retry_message_retry_exception(self, mock_put, mock_retry_message):
        retry_msg = io_util.load_data_json('sqs_message_valid.json')
        mock_put.side_effect = ClientError({'ResponseMetadata': {},
                                            'Error': {
                                                'Code': 'OTHER',
                                                'Message': 'This is a mock'}},
                                           "FAKE")
        self.assertIsNone(process_retry_messages(retry_msg))
        changeset_item = {
            "changeset-timestamp": "2019-10-28T05:11:21.047Z",
            "changeset-id": "test-changeset1",
            "changeset-state": "Expired",
            "description": "test description",
            "owner-id": 1677,
            "pending-expiration-epoch": 1573909785,
            "event-ids": ["14dc7e9e-2d81-41e0-bd81-303053250ec8", "563r1jug9i"]
        }
        mock_retry_message.assert_called_with(changeset_item, 5)

    # - process_retry_message: exception
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_process_retry_message_exception(self, mock_put):
        retry_msg = io_util.load_data_json('sqs_message_valid.json')
        mock_put.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(TerminalErrorException, 'Unhandled exception occurred'):
            process_retry_messages(retry_msg)

    # + retry_message: valid event
    @patch('lng_aws_clients.sqs.get_client')
    def test_retry_message_valid(self, mock_sqs):
        mock_sqs.return_value.send_message.return_value = None
        self.assertIsNone(retry_message({"changeset-id": "1",
                                         "changeset-state": "Expired",
                                         "pending-expiration-epoch": 1605445785}))

    # - retry_message: exception
    @patch('lng_aws_clients.sqs.get_client')
    def test_retry_message_exception(self, mock_sqs):
        mock_sqs.side_effect = Exception("Mock Exception")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to send retry message'):
            retry_message({"changeset-id": "1",
                           "changeset-state": "Expired",
                           "pending-expiration-epoch": 1605445785})


if __name__ == '__main__':
    unittest.main()
