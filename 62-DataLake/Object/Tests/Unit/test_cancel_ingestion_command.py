import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyName, ForbiddenStateTransitionError, InternalError, \
    NoSuchIngestion
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import cancel_ingestion_command

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'CancelIngestionCommand')


class TestCancelIngestionCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # Schema validation error using lambda_handler with command_wrapper decorator
    # -test_command_schema_validation_fail: missing regression-id required property
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    def test_command_schema_validation_fail(self, tt_put_mock):
        tt_put_mock.return_value = None
        request_input = io_utils.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                cancel_ingestion_command.lambda_handler(request_input, MockLambdaContext())

    # +test_cancel_ingestion_command_success: using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('cancel_ingestion_command.get_ingestion_item')
    def test_cancel_ingestion_command_success(self,
                                              get_ingestion_item_mock,
                                              owner_auth_mock,
                                              est_put_item_mock
                                              ):
        get_ingestion_item_mock.return_value = io_utils.load_data_json('dynamodb.ingestionTable.item.valid.json')
        owner_auth_mock.return_value = None
        est_put_item_mock.return_value = False
        request_input = io_utils.load_data_json('apigateway.request.accepted_1.json')
        expected_response = io_utils.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            actual_response = cancel_ingestion_command.lambda_handler(request_input, MockLambdaContext())
            self.assertDictEqual(actual_response, expected_response)

    # -test_cancel_ingestion_missing_ingestion_id: NoSuchIngestion
    @patch('cancel_ingestion_command.get_ingestion_item')
    def test_cancel_ingestion_missing_ingestion_id(self, get_ingestion_item_mock):
        get_ingestion_item_mock.return_value = None
        event = io_utils.load_data_json('apigateway.request.accepted_1.json')
        err_msg = 'Invalid Ingestion ID {0}'.format(event['request']['ingestion-id'])
        with self.assertRaisesRegex(NoSuchIngestion, err_msg):
            cancel_ingestion_command.cancel_ingestion_command(event['request'],
                                                              event['context']['client-request-id'],
                                                              event['context']['stage'],
                                                              event['context']['api-key-id'])

    # -test_cancel_ingestion_item_not_pending: ForbiddenStateTransitionError
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('cancel_ingestion_command.get_ingestion_item')
    def test_cancel_ingestion_item_not_pending(self, get_ingestion_item_mock, owner_auth_mock):
        get_ingestion_item_mock.return_value = io_utils.load_data_json(
            'dynamodb.ingestionTable.item_not_pending.fail.json')
        owner_auth_mock.return_value = None
        event = io_utils.load_data_json('apigateway.request.accepted_1.json')
        err_msg = 'Cannot cancel ingestion in Created state'
        with self.assertRaisesRegex(ForbiddenStateTransitionError, err_msg):
            cancel_ingestion_command.cancel_ingestion_command(event['request'],
                                                              event['context']['client-request-id'],
                                                              event['context']['stage'],
                                                              event['context']['api-key-id'])

    # +test_get_ingestion_item_success: Returns ingestion item
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_item_success(self, get_ingestion_item_mock):
        ingestion_item = io_utils.load_data_json('dynamodb.ingestionTable.item.valid.json')
        get_ingestion_item_mock.return_value = ingestion_item
        ingestion_id = ingestion_item['ingestion-id']
        self.assertDictEqual(cancel_ingestion_command.get_ingestion_item(ingestion_id), ingestion_item)
        get_ingestion_item_mock.assert_called_once_with({'ingestion-id': 'a69d8274-436c-11e9-9701-055af6e7f46b'},
                                                        ConsistentRead=True)

    # -test_get_ingestion_item_client_error: ClientError->InternalError
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_item_client_error(self, get_ingestion_item_mock):
        get_ingestion_item_mock.side_effect = ClientError({}, "client error message")
        with self.assertRaisesRegex(InternalError, 'client error message'):
            cancel_ingestion_command.get_ingestion_item('dummy-ingest-id')

    # -test_get_ingestion_item_exception_error: Exception->InternalError
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.get_item')
    def test_get_ingestion_item_exception_error(self, get_ingestion_item_mock):
        get_ingestion_item_mock.side_effect = Exception({}, "exception error message")
        with self.assertRaisesRegex(InternalError, 'exception error message'):
            cancel_ingestion_command.get_ingestion_item('dummy-ingest-id')


if __name__ == '__main__':
    unittest.main()
