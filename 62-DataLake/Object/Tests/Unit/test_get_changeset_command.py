import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyName, InternalError, NoSuchChangeset
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_changeset_command import lambda_handler, get_changeset_command, generate_response_json

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'GetChangesetCommand')


class TestGetChangesetCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # - Schema validation error using lambda_handler with command_wrapper decorator
    # - request missing changeset-id required property
    def test_command_schema_validation_fail(self):
        request_input = io_utils.load_data_json('apigateway.request.failure.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InvalidRequestPropertyName, r"data must contain \['changeset-id'\] properties"):
                lambda_handler(request_input, MockLambdaContext())

    # + Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_lambda_handler_success(self, get_changeset_mock):
        get_changeset_mock.return_value = io_utils.load_data_json('valid_changeset_item.json')
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        response_output = io_utils.load_data_json('apigateway.response.accepted.json')
        command_wrapper.WORKING_DIRECTORY = io_utils.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(response_output, lambda_handler(request_input, MockLambdaContext()))

    # + create_changeset_command - success
    @patch('get_changeset_command.generate_response_json')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_command_success(self, get_changeset_mock, generate_response_mock):
        get_changeset_mock.return_value = io_utils.load_data_json('valid_changeset_item.json')
        response_json = io_utils.load_data_json('apigateway.response.accepted.json')["response"]
        generate_response_mock.return_value = response_json
        request_input = {"changeset-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb"}
        response_output = {"response-dict": response_json}
        self.assertEqual(response_output, get_changeset_command(request_input, MockLambdaContext()))

    # - get_changeset_command - ClientError occurred when getting item from Changeset Table
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_command_client_error(self, get_changeset_mock):
        get_changeset_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {
                                                          'Code': 'OTHER',
                                                          'Message': 'This is a mock'}},
                                                     "FAKE")
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        with self.assertRaisesRegex(InternalError, "Unable to get item from Changeset Table"):
            get_changeset_command(request_input['request'], stage='LATEST')

    # - get_changeset_command - Unhandled exception occurred when getting item from Changeset Table
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_command_general_exception(self, get_changeset_mock):
        get_changeset_mock.side_effect = Exception
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_changeset_command(request_input['request'], stage='LATEST')

    # - get_changeset_command - no changeset item found
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_command_not_existing_changeset(self, get_changeset_mock):
        get_changeset_mock.return_value = None
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        changeset_id = request_input['request']['changeset-id']
        with self.assertRaisesRegex(NoSuchChangeset, "Invalid Changeset ID {0}".format(changeset_id)):
            get_changeset_command(request_input['request'], stage='LATEST')

    # - get_changeset_command - expired changeset item
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_get_changeset_command_expired_changeset(self, get_changeset_mock):
        get_changeset_mock.return_value = io_utils.load_data_json('expired_changeset_item.json')
        request_input = io_utils.load_data_json('apigateway.request.accepted.json')
        changeset_id = request_input['request']['changeset-id']
        with self.assertRaisesRegex(NoSuchChangeset, "Invalid Changeset ID {0}".format(changeset_id)):
            get_changeset_command(request_input['request'], stage='LATEST')


    # + generate_response_json - success
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_success(self, get_required_schema_mock, get_optional_schema_mock):
        changeset_data = {"changeset-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb",
                          "changeset-state": "Open",
                          "description": "Changeset description for test",
                          "pending-expiration-epoch": 1565985041}
        get_required_schema_mock.return_value = ["changeset-id", "changeset-state",
                                                 "changeset-url"]
        get_optional_schema_mock.return_value = ["description"]
        expected_response = {"changeset-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb",

                             "changeset-state": "Open",
                             "description": "Changeset description for test",
                             "changeset-url": "/objects/LATEST/changeset/0083e8d8-54d6-11e9-89a1-33281b9325bb"}

        self.assertEqual(expected_response, generate_response_json(changeset_data, stage='LATEST'))

    # - generate_response_json - missing required property
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_missing_required_property(self, get_required_schema_mock):
        changeset_data = {"changeset-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb",
                          "changeset-state": "Completed"}
        get_required_schema_mock.return_value = ["changeset-id", "collection-id", "changeset-state",
                                                 "changeset-timestamp"]
        with self.assertRaisesRegex(InternalError, "Missing required property:"):
            generate_response_json(changeset_data, stage='LATEST')


if __name__ == '__main__':
    unittest.main()
