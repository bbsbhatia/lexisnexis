import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from process_large_object import lambda_handler, process_large_object, \
    write_to_event_store

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ProcessLargeObject')


class TestProcessLargeObject(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + lambda_handler update event - Success
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_lambda_handler_update_event(self, get_item_mock, put_item_mock):
        get_item_mock.return_value = io_util.load_data_json('object_store_get_item.json')
        put_item_mock.return_value = None
        event = io_util.load_data_json('valid_update_sns_message_with_changeset.json')
        self.assertEqual(event_handler_status.SUCCESS, lambda_handler(event, MockLambdaContext))

    # + lambda_handler update event - Success
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.get_item')
    def test_lambda_handler_update_event_fail(self, get_item_mock, put_item_mock):
        get_item = io_util.load_data_json('object_store_get_item.json')
        event = io_util.load_data_json('valid_update_sns_message_dupe_key.json')
        get_item_mock.return_value = get_item
        put_item_mock.return_value = None
        self.assertEqual(event_handler_status.SUCCESS, lambda_handler(event, MockLambdaContext))

    # + process_large_object - Success
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.s3.read_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_process_large_object_success1(self, ost_query_mock, s3_read_mock, event_store_put_mock):
        ost_query_mock.return_value = []
        s3_read_mock.return_value = {'Body': b'Mock Datas'}
        event_store_put_mock.return_value = None
        event = io_util.load_data_json('valid_event.json')
        self.assertEqual(event_handler_status.SUCCESS, process_large_object(event))
        event_store_put_mock.assert_called_with(io_util.load_data_json('valid_event_store_input.json'))

    # + process_large_object - Success with object metadata
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.s3.read_s3_object')
    @patch('lng_datalake_dal.object_store_table.ObjectStoreTable.query_items')
    def test_process_large_object_success2(self, ost_query_mock, s3_read_mock, event_store_put_mock):
        ost_query_mock.return_value = []
        s3_read_mock.return_value = {'Body': b'Mock Datas'}
        event_store_put_mock.return_value = None
        event = io_util.load_data_json('valid_event_with_object_metadata.json')
        self.assertEqual(event_handler_status.SUCCESS, process_large_object(event))
        event_store_put_mock.assert_called_with(io_util.load_data_json('valid_event_store_input_with_object_metadata.json'))

    # - process_large_object - unknown event
    def test_process_large_object_fail_unknown(self):
        event = io_util.load_data_json('invalid_event_name.json')
        with self.assertRaisesRegex(TerminalErrorException, "Unknown event"):
            process_large_object(event)

    # - write_to_event_store - EndpointConnectionError
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    def test_write_to_event_store_endpoint_connection_error_exception(self, event_store_mock):
        event_store_mock.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            write_to_event_store({"mock": "data"})

    # - write_to_event_store - Client Error
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    def test_write_to_event_store_fail(self, event_store_mock):
        event_store_mock.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        with self.assertRaises(ClientError):
            write_to_event_store({"mock": "data"})

    # - write_to_event_store - General Error
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    def test_write_to_event_store_fail_1(self, event_store_mock):
        event_store_mock.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Unhandled exception occurred"):
            write_to_event_store({"mock": "data"})


if __name__ == '__main__':
    unittest.main()
