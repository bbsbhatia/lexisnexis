import datetime
import logging
import os
import unittest

from lng_datalake_client.Object.update_object import update_object
# from lng_datalake_client.Utils.generate_content import generate_content
from lng_datalake_constants import collection_status

__author__ = "Chuck Nelson, John Konderla, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


class TestUpdateObject(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.maxDiff = None

    default_multi_version_collection_id = os.getenv("COLLECTION_ID", '109')
    default_invalid_collection_id = os.getenv("INVALID_COLLECTION_ID", 'suspended_collection')
    default_invalid_collection_state = os.getenv("INVALID_COLLECTION_STATE", collection_status.SUSPENDED)
    default_multi_version_owner_id = os.getenv("MULTI_VERSION_OWNER_ID", 181)
    default_multi_version_asset_id = os.getenv("MULTI_VERSION_ASSET_ID", 1936)
    default_zero_version_collection_id = os.getenv("ZERO_VERSION_COLLECTION_ID", '2')
    default_object_id = os.getenv("DEFAULT_OBJECT_ID", "default_object_id_for_update_object")
    default_duplicate_object_id = os.getenv("DEFAULT_DUPLICATE_OBJECT_ID", "default_object_id_for_duplicate_update")
    default_duplicate_body = os.getenv("DEFAULT_DUPLICATE_BODY", "duplicate body")
    default_content_md5 = os.getenv('CONTENT_MD5', '202cb962ac59075b964b07152d234b70')
    default_content_type = 'application/json'
    api_gateway_limit_retry_count = 0
    lambda_limit_retry_count = 0
    max_retries = 30

    # + Test the 202 status code
    def test_update_object(self, input_dict=None, test_collection=True):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_multi_version_collection_id,
                "body": "updated content coming from create object with object id test {}".format(
                    datetime.datetime.now().isoformat()),
                "object-id": self.default_object_id,
                "owner-id": self.default_multi_version_owner_id,
                "asset-id": self.default_multi_version_asset_id,
            }
        stage = os.environ['STAGE']

        expected_response = {
            'object-state': 'Created',
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'object-id': request_input['object-id'],
            'object-url': '/objects/{0}/{1}?collection-id={2}'.format(stage, request_input['object-id'],
                                                                      request_input['collection-id']),
            'asset-id': request_input['asset-id']
        }
        response = update_object(request_input)
        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("object", response_dict)

        resp_obj_props = response_dict["object"]
        self.assertTrue(isinstance(resp_obj_props.pop('owner-id'), int))
        if test_collection:
            self.assertTrue(isinstance(resp_obj_props.pop('object-expiration-date'), str))

        # TODO: Needs more validation
        self.assertTrue(isinstance(resp_obj_props['temporary-object-key-url'], str))
        self.assertTrue(resp_obj_props.pop('temporary-object-key-url').startswith("https://"))

        # check for object metadata in request and copy to expected response
        object_metadata = {}
        if 'headers' in request_input:
            for key, value in request_input['headers'].items():
                if key.startswith("x-dl-meta-"):
                    object_metadata[key[10:]] = value
        if object_metadata:
            expected_response['object-metadata'] = object_metadata

        self.assertDictEqual(expected_response, resp_obj_props)
        return response

    # # We commented out these tests, as a result of an investigation - Story S-80078
    # # The first test started failing often, and we decided the two aren't doing enough for us currently.
    # # TODO: We want to revisit these in the future, possibly after restricting payload limit using WAF
    #
    # # - Update object larger than the Lambda payload limit (5MB) without the large object parameters
    # def test_update_object_invalid_exceed_payload_limit_lambda(self, input_dict=None):
    #     if input_dict:
    #         request_input = input_dict
    #     else:
    #         request_input = {
    #             'object-id': self.default_object_id,
    #             "collection-id": self.default_multi_version_collection_id,
    #             "body": generate_content('', 5000000)
    #         }
    #
    #     response = update_object(request_input)
    #
    #     if response.status_code in [500, 504] and self.lambda_limit_retry_count < self.max_retries:
    #         self.lambda_limit_retry_count += 1
    #         logger.info("Received status code {} from lambda test_update_object_invalid_exceed_payload_limit_lambda. "
    #                     "Attempting retry {}".format(response.status_code, self.lambda_limit_retry_count))
    #         self.test_update_object_invalid_exceed_payload_limit_lambda(input_dict)
    #         return
    #
    #     self.assertEqual(413, response.status_code)
    #     # test the response message
    #     json_error_resp = response.json()['error']
    #     self.assertEqual(json_error_resp['type'], "INTEGRATION_FAILURE")
    #     self.assertEqual(json_error_resp['message'], "Request Too Long")
    #
    # # - Update object larger than the API payload limit (10MB) without the large object parameters
    # def test_update_object_invalid_exceed_payload_limit_api(self, input_dict=None):
    #     if input_dict:
    #         request_input = input_dict
    #     else:
    #         request_input = {
    #             'object-id': self.default_object_id,
    #             "collection-id": self.default_multi_version_collection_id,
    #             "body": generate_content('', 10000000)
    #         }
    #
    #     response = update_object(request_input)
    #     if response.status_code in [500, 504] and self.api_gateway_limit_retry_count < self.max_retries:
    #         self.api_gateway_limit_retry_count += 1
    #         logger.info("Received status code {} from lambda test_update_object_invalid_exceed_payload_limit_api. "
    #                     "Attempting retry {}".format(response.status_code, self.api_gateway_limit_retry_count))
    #         self.test_update_object_invalid_exceed_payload_limit_api(input_dict)
    #         return
    #
    #     self.assertEqual(400, response.status_code)
    #     # test the response message
    #     json_error_resp = response.json()['error']
    #     # TODO: Consider mapping this error
    #     self.assertEqual(json_error_resp['type'], "DEFAULT_4XX")
    #     self.assertEqual(json_error_resp['message'],
    #                      "Request exceeded 10485760 character maximum during transformation")

    # - update_object with non-existent collection id --404
    def test_update_object_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz',
                'object-id': self.default_object_id,
                'body': "file content coming from invalid collection id regression "
                        "test {}".format(datetime.datetime.now().isoformat())
            }

        response = update_object(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - Collection with invalid state - 422
    def test_update_object_invalid_collection_state(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_invalid_collection_id,
                    'object-id': self.default_object_id,
                    'body': "file content coming from collection with invalid state "
                            "test {}".format(datetime.datetime.now().isoformat())
                }
        collection_state = input_dict.get("collection-state", self.default_invalid_collection_state)

        response = update_object(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Collection ID {0} is not set to {1} (state={2})".format(request_input['collection-id'],
                                                                                  collection_status.CREATED,
                                                                                  collection_state))

    # - Object Metadata with non ascii - 400
    def test_update_object_invalid_metadata_character(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_multi_version_collection_id,
                    'body': "empty body",
                    'object-id': self.default_object_id
                }
        if 'headers' in request_input:
            request_input['headers']['x-dl-meta-invalid'] = "Invalid Char: ì"
        else:
            request_input['headers'] = {'x-dl-meta-invalid': "Invalid Char: ì"}

        response = update_object(request_input)
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid object metadata")
        self.assertEqual(json_error_resp['corrective-action'], "Object metadata key/value must be US-ASCII")



    # - test update object - invalid content type - 415
    def test_update_object_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "object-id": self.default_object_id,
            "collection-id": self.default_object_id
        }
        response = update_object(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_update_object_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },

            "object-id": self.default_object_id,
            "collection-id": self.default_multi_version_collection_id,
            "body": "content from create object test {}".format(datetime.datetime.now().isoformat())
        }

        response = update_object(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
