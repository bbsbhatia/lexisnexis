import os
import requests
import unittest

from lng_datalake_client.Object.describe_objects import describe_objects
from lng_datalake_client.Utils.get_s3_object import get_s3_object

__author__ = "Kiran G, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestDescribeObjects(unittest.TestCase):
    default_collection_id = os.getenv("COLLECTION_ID", '2')
    default_owner_id = os.getenv("OWNER_ID", 34)
    default_asset_id = os.getenv("ASSET_ID", 62)
    default_object_id = os.getenv("OBJECT_ID", "default_object_id_for_get_object")
    default_removed_object_id = os.getenv("REMOVED_OBJECT_ID", "default_test_removed_object_id")
    default_zero_version_collection_id = os.getenv("ZERO_VERSION_COLLECTION_ID", '2')
    default_multi_version_collection_id = os.getenv("MULTI_VERSION_COLLECTION_ID", '109')
    default_multi_version_owner_id = os.getenv("MULTI_VERSION_OWNER_ID", 181)
    default_multi_version_asset_id = os.getenv("MULTI_VERSION_ASSET_ID", 1936)
    default_zero_version_object_id = os.getenv("ZERO_VERSION_OBJECT_ID", "object_id_for_zero_version_and_versionid")
    default_object_version_number = os.getenv("OBJECT_VERSION_NUMBER", 1)
    default_changeset_id = os.getenv("CHANGESET_ID", "14b49929-4568-487b-8d4c-facb8149e26a")
    default_object_content_type = os.getenv("CONTENT_TYPE", "application/json")
    default_object_body = os.getenv("OBJECT_BODY", "TEST OBJECT BODY")
    default_object_key = os.getenv('OBJECT_KEY', '')
    default_object_key_url = os.getenv('OBJECT_KEY_URL', '')
    default_replicated_bucket = os.getenv("REPLICATED_BUCKET", 'feature-jek-dl-object-store-288044017584-usw2')

    # + describe object
    def test_describe_object(self, input_dict=None, test_collection=True):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "object-id": self.default_object_id,
                "owner-id": self.default_owner_id,
                "asset-id": self.default_asset_id,
                "content-type": self.default_object_content_type
            }
        stage = os.environ['STAGE']

        expected_response = {
            'object-state': 'Created',
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'object-id': request_input['object-id'],
            'object-url': '/objects/{0}/{1}?collection-id={2}'.format(stage, request_input['object-id'],
                                                                      request_input['collection-id']),
            'content-type': request_input['content-type'],
            'owner-id': request_input['owner-id'],
            'asset-id': request_input['asset-id'],
        }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 1)
        self.assertEqual(response_dict['item-count'], 1)
        self.assertNotIn("next-token", response_dict)

        resp_obj_props = response_dict["objects"][0]
        self.assertTrue(isinstance(resp_obj_props.get('object-key-url'), str))
        self.assertIn("S3", resp_obj_props)
        self.assertTrue(isinstance(resp_obj_props.get('raw-content-length'), int))
        self.assertTrue(isinstance(resp_obj_props.get('version-timestamp'), str))
        self.assertTrue(isinstance(resp_obj_props.get('version-number'), int))
        self.assertTrue(isinstance(resp_obj_props.get('replicated-buckets'), list))
        if test_collection:
            self.assertTrue(isinstance(resp_obj_props.pop('object-expiration-date'), str))
        resp_obj_props.pop('raw-content-length')
        resp_obj_props.pop('version-timestamp')
        resp_obj_props.pop('version-number')
        expected_response['S3'] = resp_obj_props['S3']
        expected_response['object-key-url'] = resp_obj_props['object-key-url']
        expected_response['replicated-buckets'] = resp_obj_props['replicated-buckets']
        if 'object-metadata' in request_input:
            expected_response['object-metadata'] = request_input['object-metadata']
        self.assertDictEqual(expected_response, resp_obj_props)

        return resp_obj_props

    # + describe object called with changeset-id, object-id, collection-id and version-number
    def test_describe_object_changeset_id(self, input_dict=None, test_collection=True):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "object-id": self.default_object_id,
                "owner-id": self.default_owner_id,
                "asset-id": self.default_asset_id,
                "content-type": self.default_object_content_type,
                "changeset-id": self.default_changeset_id,
                "version-number": self.default_object_version_number
            }
        stage = os.environ['STAGE']

        expected_response = {
            'object-state': 'Created',
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'object-id': request_input['object-id'],
            'object-url': '/objects/{0}/{1}?collection-id={2}'.format(stage, request_input['object-id'],
                                                                      request_input['collection-id']),
            'content-type': request_input['content-type'],
            'owner-id': request_input['owner-id'],
            'asset-id': request_input['asset-id'],
        }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 1)
        self.assertEqual(response_dict['item-count'], 1)
        self.assertNotIn("next-token", response_dict)

        resp_obj_props = response_dict["objects"][0]
        self.assertTrue(isinstance(resp_obj_props.get('object-key-url'), str))
        self.assertIn("S3", resp_obj_props)
        self.assertTrue(isinstance(resp_obj_props.get('raw-content-length'), int))
        self.assertEqual(resp_obj_props['changeset-id'], request_input['changeset-id'])
        self.assertTrue(isinstance(resp_obj_props.get('version-timestamp'), str))
        self.assertTrue(isinstance(resp_obj_props.get('version-number'), int))
        self.assertTrue(isinstance(resp_obj_props.get('replicated-buckets'), list))
        if test_collection:
            self.assertTrue(isinstance(resp_obj_props.pop('object-expiration-date'), str))
        resp_obj_props.pop('raw-content-length')
        resp_obj_props.pop('version-timestamp')
        resp_obj_props.pop('version-number')
        expected_response['S3'] = resp_obj_props['S3']
        expected_response['object-key-url'] = resp_obj_props['object-key-url']
        expected_response['replicated-buckets'] = resp_obj_props['replicated-buckets']
        if 'object-metadata' in request_input:
            expected_response['object-metadata'] = request_input['object-metadata']
        self.assertDictEqual(expected_response, resp_obj_props)

    # - invalid object id
    def test_describe_object_invalid_object_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": self.default_collection_id,
                             "object-id": 'regression-abcdefghijklmnopqrstuvwxyz',
                             }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertEqual([], response_dict["objects"])

        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.getenv('STAGE'), response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 0)
        self.assertEqual(response_dict['item-count'], 0)
        self.assertNotIn('next-token', response_dict)

    # - invalid collection id
    def test_describe_object_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": 'regression-abcdefghijklmnopqrstuvwxyz',
                             "object-id": self.default_object_id
                             }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertEqual([], response_dict["objects"])

        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.getenv('STAGE'), response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 0)
        self.assertEqual(response_dict['item-count'], 0)
        self.assertNotIn('next-token', response_dict)

    # - invalid changeset-id
    def test_describe_object_invalid_changeset_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"changeset-id": 'invalid-changeset-id'
                             }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertEqual([], response_dict["objects"])

        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.getenv('STAGE'), response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 0)
        self.assertEqual(response_dict['item-count'], 0)
        self.assertNotIn('next-token', response_dict)

    # + describe removed object
    def test_describe_object_object_removed(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": self.default_multi_version_collection_id,
                             "object-id": self.default_removed_object_id,
                             "owner-id": self.default_owner_id,
                             "asset-id": self.default_asset_id
                             }

        stage = os.environ['STAGE']

        expected_response = {
            'object-state': 'Removed',
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'object-id': request_input['object-id'],
            'object-url': '/objects/{0}/{1}?collection-id={2}'.format(stage, request_input['object-id'],
                                                                      request_input['collection-id']),
            'owner-id': request_input['owner-id'],
            'asset-id': request_input['asset-id'],
        }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 1)
        self.assertEqual(response_dict['item-count'], 1)
        self.assertNotIn('next-token', response_dict)

        resp_obj_props = response_dict["objects"][0]
        expected_response['version-timestamp'] = resp_obj_props['version-timestamp']
        expected_response['version-number'] = resp_obj_props['version-number']
        self.assertDictEqual(expected_response, resp_obj_props)

    # + describe removed object
    def test_describe_object_object_removed_version(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": self.default_multi_version_collection_id,
                             "object-id": self.default_removed_object_id,
                             "owner-id": self.default_owner_id,
                             "asset-id": self.default_asset_id
                             }

        stage = os.environ['STAGE']

        expected_response = {
            'object-state': 'Removed',
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'object-id': request_input['object-id'],
            'object-url': '/objects/{0}/{1}?collection-id={2}'.format(stage, request_input['object-id'],
                                                                      request_input['collection-id']),
            'owner-id': request_input['owner-id'],
            'asset-id': request_input['asset-id'],
            'version-number' : request_input['version-number']
        }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 1)
        self.assertEqual(response_dict['item-count'], 1)
        self.assertNotIn('next-token', response_dict)

        resp_obj_props = response_dict["objects"][0]
        expected_response['version-timestamp'] = resp_obj_props['version-timestamp']
        self.assertDictEqual(expected_response, resp_obj_props)

    # + list objects
    def test_describe_objects_all(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id
            }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)
        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertGreater(len(response_dict["objects"]), 0)
        self.assertTrue(isinstance(response_dict["item-count"], int))

    # + Test describe objects with pagination
    def test_describe_objects_pagination(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "max-items": 1
            }
        response_1 = describe_objects(request_input)
        self.assertEqual(200, response_1.status_code)
        response_dict_1 = response_1.json()
        self.assertIn("context", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_dict_1["context"]["stage"])
        self.assertIn("objects", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["item-count"], int))
        self.assertEqual(request_input["max-items"], len(response_dict_1['objects']))
        self.assertEqual(request_input['max-items'], response_dict_1['item-count'])

        self.assertTrue(isinstance(response_dict_1["next-token"], str))

        # make a request again with the pagination token ------
        request_input["next-token"] = response_dict_1['next-token']
        response_2 = describe_objects(request_input)
        self.assertEqual(200, response_2.status_code)
        response_dict_2 = response_2.json()
        self.assertIn("context", response_dict_2)
        self.assertTrue(isinstance(response_dict_2["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_dict_2["context"]["stage"])
        self.assertIn("objects", response_dict_2)
        self.assertEqual(request_input["max-items"], len(response_dict_2['objects']))
        self.assertEqual(request_input['max-items'], response_dict_2['item-count'])
        self.assertTrue(isinstance(response_dict_2["item-count"], int))

        self.assertNotEqual(response_dict_1["objects"], response_dict_2["objects"])

    # + Test describe objects with pagination and changeset_id
    def test_describe_objects_pagination_changeset_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "max-items": 1
            }
        response_1 = describe_objects(request_input)
        self.assertEqual(200, response_1.status_code)
        response_dict_1 = response_1.json()
        self.assertIn("context", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_dict_1["context"]["stage"])
        self.assertIn("objects", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["item-count"], int))
        self.assertEqual(request_input["max-items"], len(response_dict_1['objects']))
        self.assertEqual(request_input['max-items'], response_dict_1['item-count'])

        self.assertTrue(isinstance(response_dict_1["next-token"], str))

        # make a request again with the pagination token ------
        request_input["next-token"] = response_dict_1['next-token']
        response_2 = describe_objects(request_input)
        self.assertEqual(200, response_2.status_code)
        response_dict_2 = response_2.json()
        self.assertIn("context", response_dict_2)
        self.assertTrue(isinstance(response_dict_2["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_dict_2["context"]["stage"])
        self.assertIn("objects", response_dict_2)
        self.assertEqual(request_input["max-items"], len(response_dict_2['objects']))
        self.assertEqual(request_input['max-items'], response_dict_2['item-count'])
        self.assertTrue(isinstance(response_dict_2["item-count"], int))

        self.assertNotEqual(response_dict_1["objects"], response_dict_2["objects"])

    # + get object from s3 location
    def test_get_s3_object(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "body": self.default_object_body,
                "object-key": self.default_object_key,
                "replicated-buckets": [
                    self.default_replicated_bucket
                ]
            }
        for bucket in request_input['replicated-buckets']:
            response = get_s3_object(bucket, request_input['object-key'])
            self.assertEqual(200, response.status_code)
            self.assertEqual(request_input['body'], response.content.decode("utf-8"))
            if 'object-metadata' in request_input:
                for key, value in request_input['object-metadata'].items():
                    header_name = "x-amz-meta-{}".format(key)
                    self.assertIn(header_name, response.headers)
                    self.assertEqual(response.headers[header_name], value)

    # + get object from s3 object key url
    def test_get_s3_object_from_object_key_url(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "body": self.default_object_body,
                "object-key-url": self.default_object_key_url
            }

        response = requests.get(request_input['object-key-url'])
        self.assertEqual(200, response.status_code)
        self.assertEqual(request_input['body'], response.content.decode("utf-8"))
        if 'object-metadata' in request_input:
            for key, value in request_input['object-metadata'].items():
                header_name = "x-dl-meta-{}".format(key)
                self.assertIn(header_name, response.headers)
                self.assertEqual(response.headers[header_name], value)

    # + describe objects invalid collection id
    def test_describe_objects_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz'
            }

        response = describe_objects(request_input)
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertEqual([], response_dict["objects"])

        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.getenv('STAGE'), response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 0)
        self.assertEqual(response_dict['item-count'], 0)
        self.assertNotIn('next-token', response_dict)

    # - describe objects with invalid max-results value
    def test_describe_invalid_objects_pagination(self):
        input_dict = {
            "max-items": 11
        }
        response = describe_objects(input_dict)
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid max-items value: 11")
        self.assertEqual(json_error_resp['corrective-action'], "Value must be between 1 and 10")

    # - describe objects with invalid max-results value
    def test_describe_invalid_objects_pagination_diff_max_items(self):
        input_dict = {
            "max-items": 1,
            "next-token": "eyJtYXgtaXRlbXMiOiA0LCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkPSJ9"
        }
        response = describe_objects(input_dict)
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Continuation request must pass the same max-items as initial request")
        self.assertEqual(json_error_resp['corrective-action'], "Set max-items to 4 and try again")

    # - test describe object - invalid content type - 415
    def test_describe_objects_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id
        }
        response = describe_objects(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - test describe object - missing parameters raises 422
    #  object-id without version-number and collection-id
    def test_describe_objects_changeset_id_missing_collection_id(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id
        }
        response = describe_objects(input_dict)
        # test the status code 422
        self.assertEqual(422, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "Invalid request, missing version-number and/or collection-id")

    # - test describe object - missing parameters raises 422
    def test_describe_objects_changeset_id_missing_object_id(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "version-number": self.default_object_version_number
        }
        response = describe_objects(input_dict)
        # test the status code 422
        self.assertEqual(422, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "Invalid request, missing object-id and/or collectionid")

    # - no x-api-key, auth error
    def test_describe_objects_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "collection-id": self.default_collection_id
        }

        response = describe_objects(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
