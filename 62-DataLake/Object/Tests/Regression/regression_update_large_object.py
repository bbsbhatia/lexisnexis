import logging
import os
import unittest

from lng_datalake_client.Object.update_large_object import update_large_object

__author__ = "Chuck Nelson, John Konderla, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


class TestUpdateLargeObject(unittest.TestCase):
    default_multi_version_collection_id = os.getenv("COLLECTION_ID", '109')
    default_multi_version_owner_id = os.getenv("MULTI_VERSION_OWNER_ID", 181)
    default_multi_version_asset_id = os.getenv("MULTI_VERSION_ASSET_ID", 1936)
    default_zero_version_collection_id = os.getenv("ZERO_VERSION_COLLECTION_ID", '2')
    default_object_id = os.getenv("DEFAULT_OBJECT_ID", "default_object_id_for_update_object")
    default_duplicate_object_id = os.getenv("DEFAULT_DUPLICATE_OBJECT_ID", "default_object_id_for_duplicate_update")
    default_duplicate_body = os.getenv("DEFAULT_DUPLICATE_BODY", "duplicate body")
    default_content_md5 = os.getenv('CONTENT_MD5', '202cb962ac59075b964b07152d234b70')
    default_content_type = 'application/json'
    api_gateway_limit_retry_count = 0
    lambda_limit_retry_count = 0
    max_retries = 5

    # + Test the 202 status code
    def test_update_large_object(self, input_dict=None, test_collection=True):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                'collection-id': self.default_multi_version_collection_id,
                'object-id': self.default_object_id,
                'Content-MD5': self.default_content_md5,
                "asset-id": self.default_multi_version_asset_id,
            }
        stage = os.environ['STAGE']

        expected_response = {
            'object-state': 'Pending',
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'object-id': request_input['object-id'],
            'object-url': '/objects/{0}/{1}?collection-id={2}'.format(stage, request_input['object-id'],
                                                                      request_input['collection-id']),
            'owner-id': request_input['owner-id'],
            'asset-id': request_input['asset-id']
        }
        response = update_large_object(request_input)
        self.assertEqual(201, response.status_code)

        response_dict = response.json()

        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("object", response_dict)

        resp_obj_props = response_dict["object"]

        if test_collection:
            self.assertTrue(isinstance(resp_obj_props.pop('object-expiration-date'), str))

        self.assertEqual(resp_obj_props.pop('temporary-object-key-url'), "")

        self.assertDictEqual(expected_response, resp_obj_props)

        self.assertIn("large-upload", response_dict)
        resp_large_upload_props = response_dict['large-upload']
        self.assertIn("headers", resp_large_upload_props)
        self.assertTrue(isinstance(resp_large_upload_props.get('url'), str))
        return response

    # - update_large_object with non-existent collection id --404
    def test_update_large_object_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz',
                'object-id': self.default_object_id,
                "Content-MD5": self.default_content_md5
            }

        response = update_large_object(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - test update_large_object - invalid content type - 415
    def test_update_large_object_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "object-id": self.default_object_id,
            "collection-id": self.default_object_id,
            "Content-MD5": self.default_content_md5
        }
        response = update_large_object(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_update_large_object_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "object-id": self.default_object_id,
            "collection-id": self.default_multi_version_collection_id,
            "Content-MD5": self.default_content_md5
        }

        response = update_large_object(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
