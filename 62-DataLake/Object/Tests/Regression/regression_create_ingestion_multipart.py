import os
import unittest

from lng_datalake_client.Object.create_ingestion_multipart import create_ingestion_multipart
from lng_datalake_constants import collection_status

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestCreateIngestionMultipart(unittest.TestCase):
    default_collection_id = os.getenv("COLLECTION_ID", 'JohnK')
    default_invalid_collection_id = os.getenv("INVALID_COLLECTION_ID", 'suspended_collection')
    default_content_type = os.getenv("CONTENT_TYPE", "application/zip")
    default_owner_id = int(os.getenv("OWNER_ID", '1'))
    default_asset_id = int(os.getenv("ASSET_ID", '62'))
    default_invalid_collection_state = os.getenv("INVALID_COLLECTION_STATE", collection_status.SUSPENDED)

    # + Create multipart object with id
    def test_create_ingestion_multipart_success(self, input_dict=None, test_collection=True):
        if not input_dict:
            input_dict = {}
        stage = os.environ['STAGE']
        request_input = \
            {
                "collection-id": input_dict.get("collection-id", self.default_collection_id),
                "content-type": input_dict.get("content-type", self.default_content_type)
            }

        expected_response = \
            {
                'archive-format': 'zip',
                "collection-id": request_input['collection-id'],
                "ingestion-state": "Pending",
                "collection-url": "/collections/{0}/{1}".format(stage, request_input['collection-id']),
                "asset-id": input_dict.get("asset-id", self.default_asset_id),
            }
        if "description" in input_dict:
            request_input["description"] = input_dict["description"]
            expected_response["description"] = input_dict["description"]

        response = create_ingestion_multipart(request_input)
        self.assertEqual(201, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("ingestion", response_dict)
        self.assertIn("multipart-upload", response_dict)
        resp_multipart_props = response_dict["multipart-upload"]
        self.assertTrue(isinstance(resp_multipart_props.get('UploadId'), str))
        self.assertIn("S3", resp_multipart_props)
        self.assertTrue(isinstance(resp_multipart_props['S3']['Bucket'], str))
        self.assertTrue(isinstance(resp_multipart_props['S3']['Key'], str))

        resp_multipart_ingestion_props = response_dict['ingestion']
        self.assertTrue(isinstance(resp_multipart_ingestion_props.pop('owner-id'), int))
        self.assertIn('ingestion-id', resp_multipart_ingestion_props)
        self.assertTrue(isinstance(resp_multipart_ingestion_props['ingestion-id'], str))
        expected_response['ingestion-id'] = resp_multipart_ingestion_props['ingestion-id']
        expected_response['ingestion-url'] = '/objects/{0}/ingestion/{1}'.format(stage,
                                                                                 expected_response.get('ingestion-id'))
        self.assertIn('ingestion-timestamp', resp_multipart_ingestion_props)
        self.assertTrue(isinstance(resp_multipart_ingestion_props['ingestion-timestamp'], str))
        expected_response['ingestion-timestamp'] = resp_multipart_ingestion_props['ingestion-timestamp']
        self.assertIn('ingestion-expiration-date', resp_multipart_ingestion_props)
        self.assertTrue(isinstance(resp_multipart_ingestion_props['ingestion-expiration-date'], str))
        expected_response['ingestion-expiration-date'] = resp_multipart_ingestion_props['ingestion-expiration-date']
        if test_collection:
            self.assertTrue(isinstance(resp_multipart_ingestion_props.pop('object-expiration-date'), str))
        self.assertDictEqual(expected_response, resp_multipart_ingestion_props)
        return response

    # - Create multipart object - invalid collection-id 404
    def test_create_ingestion_multipart_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": 'regression-abcdefghijklmnopqrstuvwxyz',
                    "content-type": self.default_content_type
                }

        response = create_ingestion_multipart(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - Create ingestion - collection with invalid state
    def test_create_ingestion_multipart_invalid_collection_state(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_invalid_collection_id,
                    "content-type": self.default_content_type,
                }
        collection_state = input_dict.get("collection-state", self.default_invalid_collection_state)

        response = create_ingestion_multipart(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Collection ID {0} is not set to {1} (state={2})".format(request_input['collection-id'],
                                                                                  collection_status.CREATED,
                                                                                  collection_state))

    # - invalid content type - 415
    def test_create_ingestion_multipart_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "content-type": self.default_content_type
        }
        response = create_ingestion_multipart(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - invalid content type - 415
    def test_create_ingestion_multipart_invalid_content_type_2(self):
        input_dict = {
            "headers": {
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "stored-content-type": "application/xml+xml"
        }
        response = create_ingestion_multipart(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_create_ingestion_multipart_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/zip'
            },
            "collection-id": self.default_collection_id,
            "content-type": self.default_content_type
        }

        response = create_ingestion_multipart(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
