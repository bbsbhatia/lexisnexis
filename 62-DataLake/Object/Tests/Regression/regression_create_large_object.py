import datetime
import logging
import os
import unittest

from lng_datalake_client.Object.create_large_object import create_large_object

__author__ = "Jason Feng, John Konderla, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


class TestCreateLargeObject(unittest.TestCase):

    default_collection_id = os.getenv("COLLECTION_ID", '2')
    default_asset_id = os.getenv("ASSET_ID", 62)
    default_owner_id = os.getenv("OWNER_ID")
    default_duplicated_object_id = os.getenv("DUPLICATED_OBJECT_ID", 'duplication-object-id')
    default_content_md5 = os.getenv('CONTENT_MD5', '202cb962ac59075b964b07152d234b70')
    lambda_limit_retry_count = 0
    api_gateway_limit_retry_count = 0
    max_retries = 5

    # + Create Large Object
    def test_create_large_object(self, input_dict=None, test_collection=True):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "asset-id": self.default_asset_id,
                "owner-id": self.default_owner_id,
                'object-id': "regression_large_object_id_{}".format(datetime.datetime.now().isoformat()),
                'Content-MD5': self.default_content_md5
            }
        stage = os.environ['STAGE']

        expected_response = {
            'object-state': 'Pending',
            'asset-id': request_input['asset-id'],
            'owner-id': request_input['owner-id'],
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'object-id': request_input['object-id'],
            'object-url': '/objects/{0}/{1}?collection-id={2}'.format(stage, request_input['object-id'],
                                                                      request_input['collection-id'])
        }
        response = create_large_object(request_input)
        self.assertEqual(201, response.status_code)

        response_dict = response.json()

        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("object", response_dict)

        resp_obj_props = response_dict["object"]
        if test_collection:
            self.assertTrue(isinstance(resp_obj_props.pop('object-expiration-date'), str))

        self.assertEqual(resp_obj_props.pop('temporary-object-key-url'), "")

        self.assertDictEqual(expected_response, resp_obj_props)

        self.assertIn("large-upload", response_dict)
        resp_large_upload_props = response_dict['large-upload']
        self.assertIn("headers", resp_large_upload_props)
        self.assertTrue(isinstance(resp_large_upload_props.get('url'), str))

        return response

    # - Collection ID does not exist 404
    def test_create_large_object_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz',
                'Content-MD5': self.default_content_md5,
                'object-id': self.default_duplicated_object_id
            }

        response = create_large_object(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - test list object - invalid content type - 415
    def test_create_large_object_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": '1',
            "Content-MD5": self.default_content_md5,
            'object-id': self.default_duplicated_object_id
        }
        response = create_large_object(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_create_large_object_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "Content-MD5": self.default_content_md5,
            "collection-id": self.default_collection_id,
            'object-id': self.default_duplicated_object_id
        }

        response = create_large_object(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
