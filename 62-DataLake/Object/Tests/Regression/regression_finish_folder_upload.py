import os
import unittest

from lng_datalake_client.Object.finish_folder_upload import finish_folder_upload
from lng_datalake_client.Utils import transaction_id

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestFinishFolderUpload(unittest.TestCase):
    default_collection_id = os.getenv("COLLECTION_ID", '109')
    default_object_id = os.getenv("OBJECT_ID", "test_folder_upload")
    default_transaction_id = os.getenv("TRANSACTION_ID",
                                       "eyJ0cmFuc2FjdGlvbi1pZCI6ICJwb0ladlFVU0xJbW1tQUJhIiwgImNvbGxlY3Rpb24taWQiOiAiM"
                                       "SIsICJvd25lci1pZCI6IDEwMCwgInN0YWdlIjogIkxBVEVTVCJ9")

    # + Finish folder upload
    def test_finish_folder_upload_success(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "object-id": self.default_object_id,
                    "transaction-id": self.default_transaction_id,
                }

        # TODO: Determine how to get all this data later
        # object_expected_response = \
        #     {
        #         "object-id": request_input['object-id'],
        #         "object-state": "Pending",
        #         "collection-id": "124",
        #         "collection-url": "/collections/LATEST/124",
        #         "object-url": "/objects/LATEST/{0}?collection-id=124".format(request_input['object-id']),
        #         "owner-id": 29,
        #         "asset-id": 1
        #     }
        # folder_expected_response = \
        #     {
        #         "transaction-id": request_input['transaction-id']
        #     }

        response = finish_folder_upload(request_input)
        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])
        self.assertIn("object", response_dict)
        resp_object_props = response_dict["object"]
        self.assertTrue(isinstance(resp_object_props.pop('object-expiration-date'), str))

        # TODO: Needs more validation
        self.assertTrue(isinstance(resp_object_props['temporary-object-key-url'], str))
        self.assertTrue(resp_object_props.pop('temporary-object-key-url').startswith("https://"))

        # TODO: Add this assertion in later
        # self.assertDictEqual(object_expected_response, resp_object_props)

        return response

    # - Finish folder upload metadata - invalid collection-id
    def test_finish_folder_upload_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
            _, collection_id, _, _ = transaction_id.unpack_folder_object_transaction_id(request_input["transaction-id"])
        else:
            collection_id = 'regression-abcdefghijklmnopqrstuvwxyz'
            parts = transaction_id.unpack_folder_object_transaction_id(self.default_transaction_id)
            bad_collection_transaction = transaction_id.build_folder_object_transaction_id(parts[0], collection_id,
                                                                                           parts[2], parts[3])
            request_input = \
                {
                    "object-id": self.default_object_id,
                    "transaction-id": bad_collection_transaction
                }

        response = finish_folder_upload(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "No open transactions for Object ID: {0} in Collection ID: {1}"
                         .format(request_input.get('object-id', self.default_object_id),
                                 request_input.get('collection-id', collection_id)))
        self.assertEqual(json_error_resp['corrective-action'], "Create a new transaction")

    # - Finish folder upload - invalid object-id
    def test_finish_folder_upload_invalid_object_id(self, input_dict=None):
        non_existent_object = 'regression-abcdefghijklmnopqrstuvwxyz'
        if input_dict:
            request_input = input_dict
            _, collection_id, *_ = transaction_id.unpack_folder_object_transaction_id(
                request_input.get('transaction-id',
                                  self.default_transaction_id))
        else:
            collection_id = self.default_collection_id
            request_input = \
                {
                    "object-id": non_existent_object,
                    "transaction-id": self.default_transaction_id
                }

        response = finish_folder_upload(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "No open transactions for Object ID: {0} in Collection ID: {1}"
                         .format(request_input.get('object-id', non_existent_object),
                                 collection_id))
        self.assertEqual(json_error_resp['corrective-action'], "Create a new transaction")

    # - Finish folder upload - invalid transaction-id
    def test_finish_folder_upload_invalid_transaction_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
            _, collection_id, *_ = transaction_id.unpack_folder_object_transaction_id(
                request_input.get('transaction-id',
                                  self.default_transaction_id))
        else:
            invalid_request_id = 'abc-invalid'
            _, collection_id, owner_id, stage = transaction_id.unpack_folder_object_transaction_id(
                self.default_transaction_id)
            bad_transaction = transaction_id.build_folder_object_transaction_id(invalid_request_id, collection_id,
                                                                                owner_id,
                                                                                stage)
            request_input = \
                {
                    "object-id": self.default_object_id,
                    "transaction-id": bad_transaction
                }

        response = finish_folder_upload(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "No open transactions for Object ID: {0} in Collection ID: {1}"
                         .format(request_input.get('object-id', self.default_object_id),
                                 collection_id))
        self.assertEqual(json_error_resp['corrective-action'], "Create a new transaction")

    # - invalid content type - 415
    def test_finish_folder_upload_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id,
            "transaction-id": self.default_transaction_id,
        }
        response = finish_folder_upload(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_finish_folder_upload_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id,
            "transaction-id": self.default_transaction_id,
        }

        response = finish_folder_upload(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
