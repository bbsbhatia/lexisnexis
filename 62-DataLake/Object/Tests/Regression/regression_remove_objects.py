import os
import unittest

from lng_datalake_client.Object.remove_objects import remove_objects
from lng_datalake_constants import collection_status

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestRemoveObjects(unittest.TestCase):
    default_collection_id = os.getenv("COLLECTION_ID", "two-version-id-regression-155070821")
    default_invalid_state_collection_id = os.getenv("INVALID_COLLECTION_ID", 'ingestion-regression-1560346975')
    default_invalid_collection_state = os.getenv("INVALID_COLLECTION_STATE", collection_status.SUSPENDED)
    default_description = os.getenv("REMOVE_DESCRIPTION", "description of regression remove objects")
    default_owner_id = os.getenv("OWNER_ID", 1)
    default_asset_id = os.getenv("ASSET_ID", 62)
    default_object_id = os.getenv("DEFAULT_OBJECT_ID", "object_id_for_remove_object")

    # + remove objects
    def test_remove_objects(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "body": {
                    "object-ids": [self.default_object_id],
                    "description": self.default_description
                },
                "owner-id": self.default_owner_id,
                "asset-id": self.default_asset_id
            }
        stage = os.environ['STAGE']

        expected_response = {
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'owner-id': request_input['owner-id'],
            'asset-id': request_input['asset-id'],
            'object-count': len(request_input['body']['object-ids']),
            'description': request_input['body']['description']
        }
        response = remove_objects(request_input)
        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("batch", response_dict)

        resp_remove_props = response_dict["batch"]
        self.assertIn("remove-timestamp", resp_remove_props)
        self.assertTrue(isinstance(resp_remove_props["remove-timestamp"], str))
        resp_remove_props.pop("remove-timestamp")
        self.assertDictEqual(expected_response, resp_remove_props)
        return response

    # - remove objects - invalid description length (maximum allowed is 100 characters) - 400
    def test_remove_object_invalid_description_length(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "body": {
                    "object-ids": [self.default_object_id],
                    "description": self.default_description * 3
                }
            }
        response = remove_objects(request_input)
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid Description")
        self.assertEqual(json_error_resp['corrective-action'], "Description must be less than 100 characters")

    # - remove objects - invalid collection id - 404
    def test_remove_object_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": 'regression-abcdefghijklmnopqrstuvwxyz',
                "body": {
                    "object-ids": [self.default_object_id],
                    "description": self.default_description
                }
            }
        response = remove_objects(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - remove objects - collection with invalid state - 422
    def test_remove_object_invalid_collection_state(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_invalid_state_collection_id,
                "body": {
                    "object-ids": [self.default_object_id],
                    "description": self.default_description
                }
            }
        collection_state = request_input.get("collection-state", self.default_invalid_collection_state)

        response = remove_objects(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Collection ID {0} is not set to {1} (state={2})".format(request_input['collection-id'],
                                                                                  collection_status.CREATED,
                                                                                  collection_state))

    # - remove objects - invalid object id in the batch - 400
    def test_remove_object_invalid_object_id_in_batch(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "body": {
                    "object-ids": [self.default_object_id, "non_alphanum_object_id_$@"],
                    "description": self.default_description
                }
            }
        index_non_alphanum_obj_id = request_input.get("index_non_alphanum_object_id", 1)

        response = remove_objects(request_input)
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid object-id in batch at index {0}".format(index_non_alphanum_obj_id))
        self.assertEqual(json_error_resp['corrective-action'], "Object ID contains invalid characters")

    # - remove objects - invalid batch size (maximum batch size allowed is 20000)- 400
    def test_remove_object_invalid_batch_size(self, input_dict=None):
        max_batch_size = 20000
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "body": {
                    "object-ids": [self.default_object_id] * (max_batch_size + 1),
                    "description": self.default_description
                }
            }
        batch_size = len(request_input["body"]["object-ids"])

        response = remove_objects(request_input)
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid batch size {0}".format(batch_size))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Batch size must be between 1 and {0}".format(max_batch_size))

    # - remove objects - invalid content type - 415
    def test_remove_object_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "body": {
                "object-ids": [self.default_object_id],
                "description": self.default_description
            }
        }
        response = remove_objects(input_dict)
        self.assertEqual(415, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error - 403
    def test_remove_object_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "collection-id": self.default_collection_id,
            "body": {
                "object-ids": [self.default_object_id],
                "description": self.default_description
            }
        }

        response = remove_objects(request_dict)
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
