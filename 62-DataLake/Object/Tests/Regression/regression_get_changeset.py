import os
import unittest

from lng_datalake_client.Object.get_changeset import get_changeset

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestGetChangeset(unittest.TestCase):
    default_invalid_changeset_id = os.getenv("INVALID_CHANGESET_ID", "invalid-changeset-id-0123")
    default_changeset_id = os.getenv("CHANGESET_ID", "fcc9f02d-0e6e-490c-aa80-c886ca7a6e7b")
    default_changeset_state = os.getenv("CHANGESET_STATE", "Opened")
    default_changeset_url = os.getenv("CHANGESET_URL", "/objects/{}/changeset/{}").format('LATEST',
                                                                                          default_changeset_id)
    default_owner_id = int(os.getenv("OWNER_ID", 130))
    default_changeset_timestamp = os.getenv("CHANGESET_TIMESTAMP", "2019-10-29T22:43:52.991Z")
    default_changeset_expiration_date = os.getenv("CHANGESET_EXPIRATION_DATE", "2019-11-28T22:43:52.000Z")

    # + Test the 200 status code and changeset properties for GET_CHANGESET
    def test_get_changeset(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        changeset_id = input_dict.get('changeset-id', self.default_changeset_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "changeset-id": changeset_id
            }

        # changeset required properties
        expected_response = \
            {
                "changeset-state": input_dict.get("changeset-state", self.default_changeset_state),
                "changeset-id": changeset_id,
                "changeset-url": input_dict.get("changeset-url", self.default_changeset_url),
                "owner-id": input_dict.get("owner-id", self.default_owner_id),
                "changeset-timestamp": input_dict.get("changeset-timestamp", self.default_changeset_timestamp),
                "changeset-expiration-date": input_dict.get("changeset-expiration-date",
                                                            self.default_changeset_expiration_date)
            }
        optional_properties = ["description"]

        # Add any additional attributes to validate against the changeset
        for key, value in input_dict.items():
            if key not in expected_response and key in optional_properties:
                expected_response[key] = value

        response = get_changeset(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("changeset", response_dict)

        # Copy from the response the optional properties not existing in the expected response.
        # Validate the properties and their types
        for key, value in response_dict['changeset'].items():
            if key not in expected_response:
                self.assertIn(key, optional_properties)
                self.assertTrue(isinstance(value, str))
                expected_response[key] = value

        # test the response body
        self.assertDictEqual(expected_response, response_dict["changeset"])
        return response_dict["changeset"]

    # - get_changeset with invalid changeset id - 404 status code
    def test_get_changeset_invalid_changeset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        invalid_changeset_id = input_dict.get('changeset-id', self.default_invalid_changeset_id)
        request_dict = {
            "changeset-id": invalid_changeset_id
        }

        response = get_changeset(request_dict)

        # verify the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchChangeset")
        self.assertEqual(json_error_resp['message'], "Invalid Changeset ID {}".format(invalid_changeset_id))
        self.assertEqual(json_error_resp['corrective-action'], "Changeset ID does not exist")

    # - get_changeset with invalid x-api-key - 403 status code
    def test_get_changeset_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "changeset-id": self.default_changeset_id
        }

        response = get_changeset(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - get_changeset with invalid content-type - 415 status code
    def test_get_changeset_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "changeset-id": self.default_changeset_id
        }

        response = get_changeset(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
