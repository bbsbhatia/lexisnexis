import os
import unittest

from lng_datalake_client.Object.create_changeset import create_changeset

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestCreateChangeset(unittest.TestCase):
    # TODO: Add this to regression orchestration

    def test_create_changeset(self, input_data=None):
        if not input_data:
            input_data = {}
        stage = os.environ['STAGE']
        request_dict = {}
        # Add optional attributes
        if "description" in input_data:
            request_dict["description"] = input_data["description"]

        response = create_changeset(request_dict)

        # test the status code - 202
        self.assertEqual(202, response.status_code)
        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("changeset", response_dict)

        resp_changeset = response_dict["changeset"]

        self.assertIn('changeset-state', resp_changeset)
        self.assertEqual(resp_changeset['changeset-state'], "Opened")

        self.assertIn('owner-id', resp_changeset)
        self.assertTrue(isinstance(resp_changeset['owner-id'], int))

        self.assertIn('changeset-id', resp_changeset)
        self.assertTrue(isinstance(resp_changeset['changeset-id'], str))

        expected_changeset_url = '/objects/{0}/changeset/{1}'.format(stage, resp_changeset.get('changeset-id'))
        self.assertEqual(resp_changeset["changeset-url"], expected_changeset_url)

        self.assertIn('changeset-timestamp', resp_changeset)
        self.assertTrue(isinstance(resp_changeset['changeset-timestamp'], str))

        self.assertIn('changeset-expiration-date', resp_changeset)
        self.assertTrue(isinstance(resp_changeset['changeset-expiration-date'], str))

        if input_data.get("description"):
            self.assertIn('description', resp_changeset)
            self.assertEqual(resp_changeset['description'], input_data["description"])

    def test_create_changeset_invalid_x_api_key(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },

            "body": {
                "description": "regression testing .. "
            }
        }

        response = create_changeset(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    def test_create_changeset_unsupported_media(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            'headers': {
                'Content-type': 'application/xml',
                'x-api-key': key
            },

            "body": {
                "description": "regression testing .. "
            }
        }

        response = create_changeset(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
