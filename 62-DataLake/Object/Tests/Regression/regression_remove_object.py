import os
import unittest

from lng_datalake_client.Object.remove_object import remove_object
from lng_datalake_constants import collection_status

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestRemoveObject(unittest.TestCase):
    default_multi_version_collection_id = os.getenv("MULTI_VERSION_COLLECTION_ID", '109')
    default_invalid_collection_id = os.getenv("INVALID_COLLECTION_ID", 'suspended_collection')
    default_invalid_collection_state = os.getenv("INVALID_COLLECTION_STATE", collection_status.SUSPENDED)
    default_multi_version_owner_id = os.getenv("MULTI_VERSION_OWNER_ID", 181)
    default_multi_version_asset_id = os.getenv("MULTI_VERSION_ASSET_ID", 1936)
    default_object_id = os.getenv("DEFAULT_OBJECT_ID", "default_object_id_for_remove_object_3")

    # + remove object
    def test_remove_object(self, input_dict=None, test_collection=True):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_multi_version_collection_id,
                "object-id": self.default_object_id,
                "owner-id": self.default_multi_version_owner_id,
                "asset-id": self.default_multi_version_asset_id,
            }
        stage = os.environ['STAGE']

        expected_response = {
            'object-state': 'Removed',
            'collection-id': request_input['collection-id'],
            'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
            'object-id': request_input['object-id'],
            'object-url': '/objects/{0}/{1}?collection-id={2}'.format(stage, request_input['object-id'],
                                                                      request_input['collection-id']),
            'owner-id': request_input['owner-id'],
            'asset-id': request_input['asset-id'],

        }
        response = remove_object(request_input)
        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("object", response_dict)

        resp_obj_props = response_dict["object"]
        if test_collection:
            self.assertTrue(isinstance(resp_obj_props.pop('object-expiration-date'), str))
        self.assertDictEqual(expected_response, resp_obj_props)
        return response

    # - remove object - invalid collection id - 404
    def test_remove_object_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz',
                'object-id': self.default_object_id
            }

        response = remove_object(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - remove object - collection with invalid state - 422
    def test_remove_object_invalid_collection_state(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_invalid_collection_id,
                    'object-id': self.default_object_id
                }
        collection_state = input_dict.get("collection-state", self.default_invalid_collection_state)

        response = remove_object(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Collection ID {0} is not set to {1} (state={2})".format(request_input['collection-id'],
                                                                                  collection_status.CREATED,
                                                                                  collection_state))

    # - test list object - invalid content type - 415
    def test_remove_object_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            'object-id': self.default_object_id,
            "collection-id": self.default_multi_version_collection_id,
        }
        response = remove_object(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_remove_object_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },

            'object-id': self.default_object_id,
            "collection-id": self.default_multi_version_collection_id,
        }

        response = remove_object(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
