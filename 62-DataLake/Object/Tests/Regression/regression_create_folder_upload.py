import os
import unittest

from lng_datalake_client.Object.create_folder_upload import create_folder_upload
from lng_datalake_constants import collection_status

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestCreateFolderUpload(unittest.TestCase):
    default_collection_id = os.getenv("COLLECTION_ID", '109')
    default_object_id = os.getenv("OBJECT_ID", "default_object_id_for_folder")
    default_duplicate_object_id = os.getenv("DUPLICATE_OBJECT_ID", "test_folder_upload")
    default_invalid_collection_id = os.getenv("INVALID_COLLECTION_ID", 'suspended_collection')
    default_invalid_collection_state = os.getenv("INVALID_COLLECTION_STATE", collection_status.SUSPENDED)

    # + Create folder upload
    def test_create_folder_upload_success(self, input_dict=None, test_collection=True):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_collection_id,
                    "object-id": self.default_object_id
                }

        stage = os.environ['STAGE']
        expected_obj_response = \
            {
                "object-id": request_input['object-id'],
                'object-state': 'Pending',
                'collection-id': request_input['collection-id'],
                'collection-url': '/collections/{0}/{1}'.format(stage, request_input['collection-id']),
                "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, request_input['object-id'],
                                                                          request_input['collection-id'])
            }

        response = create_folder_upload(request_input)
        self.assertEqual(201, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("folder-upload", response_dict)
        resp_folder_props = response_dict["folder-upload"]
        self.assertTrue(isinstance(resp_folder_props.get('transaction-id'), str))
        self.assertIn("S3", resp_folder_props)
        self.assertTrue(isinstance(resp_folder_props['S3']['Bucket'], str))
        self.assertTrue(isinstance(resp_folder_props['S3']['Prefix'], str))
        self.assertIn("Credentials", resp_folder_props)
        self.assertTrue(isinstance(resp_folder_props['Credentials']['AccessKeyId'], str))
        self.assertTrue(isinstance(resp_folder_props['Credentials']['SecretAccessKey'], str))
        self.assertTrue(isinstance(resp_folder_props['Credentials']['SessionToken'], str))
        self.assertTrue(isinstance(resp_folder_props['Credentials']['Expiration'], str))
        resp_folder_props.pop("transaction-id")
        resp_folder_props.pop("S3")
        resp_folder_props.pop("Credentials")
        self.assertDictEqual({}, resp_folder_props)

        self.assertIn("object", response_dict)
        self.assertTrue(isinstance(response_dict['object']['asset-id'], int))
        self.assertTrue(isinstance(response_dict['object']['owner-id'], int))
        if test_collection:
            self.assertTrue(isinstance(response_dict['object'].pop('object-expiration-date'), str))

        response_dict['object'].pop('asset-id')
        response_dict['object'].pop('owner-id')

        self.assertDictEqual(response_dict['object'], expected_obj_response)
        return response

    # - Create folder upload - duplicate object id
    def test_create_folder_upload_duplicate_object_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_collection_id,
                    "object-id": self.default_duplicate_object_id,
                }

        response = create_folder_upload(request_input)
        self.assertEqual(405, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAllowedError")
        self.assertEqual(json_error_resp['message'],
                         "Object ID {} in Collection ID {} already exists or there is a pending request".format(
                             request_input['object-id'], request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Duplicate requests not allowed")

    # - Create folder upload - invalid collection-id 404
    def test_create_folder_upload_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": 'regression-abcdefghijklmnopqrstuvwxyz',
                    "object-id": self.default_object_id,
                }

        response = create_folder_upload(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - Collection with invalid state - 422
    def test_create_folder_upload_invalid_collection_state(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_invalid_collection_id,
                    "object-id": self.default_object_id,
                }
        collection_state = input_dict.get("collection-state", self.default_invalid_collection_state)

        response = create_folder_upload(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Collection ID {0} is not set to {1} (state={2})".format(request_input['collection-id'],
                                                                                  collection_status.CREATED,
                                                                                  collection_state))

    # - invalid content type - 415
    def test_create_folder_upload_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id,

        }
        response = create_folder_upload(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_create_folder_upload_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id,
        }

        response = create_folder_upload(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
