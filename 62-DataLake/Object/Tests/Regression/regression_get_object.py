import os
import unittest

from lng_datalake_client.Object.get_object import get_object

__author__ = "Chuck Nelson, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestGetObject(unittest.TestCase):
    default_collection_id = os.getenv("COLLECTION_ID", '2')
    default_object_id = os.getenv("OBJECT_ID", "default_object_id_for_get_object")
    default_object_body = os.getenv("OBJECT_BODY", "TEST OBJECT BODY")
    default_changeset_id = os.getenv("CHANGESET_ID", "default_test_changeset")
    default_removed_changeset_id = os.getenv("REMOVED_CHANGESET_ID", "default_test_removed_changeset_id")
    default_removed_changeset_object_id = os.getenv("REMOVED_CHANGESET_OBJECT_ID",
                                                    "default_removed_changeset_object_id")
    default_large_object_id = os.getenv("LARGE_OBJECT_ID", "default_large_object_id_for_get_object")
    default_large_object_body = os.getenv("LARGE_OBJECT_BODY", "TEST LARGE OBJECT BODY")
    default_folder_object_id = os.getenv("FOLDER_OBJECT_ID", "test_folder_upload")
    default_removed_object_id = os.getenv("REMOVED_OBJECT_ID", "default_test_removed_object_id")
    default_zero_version_collection_id = os.getenv("ZERO_VERSION_COLLECTION_ID", '2')
    default_multi_version_collection_id = os.getenv("MULTI_VERSION_COLLECTION_ID", '109')
    default_zero_version_object_id = os.getenv("ZERO_VERSION_OBJECT_ID", "object_id_for_zero_version_and_versionid")
    default_object_content_type = os.getenv("CONTENT_TYPE", "application/json")

    # + get object
    def test_get_object(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": self.default_collection_id,
                             "object-id": self.default_object_id,
                             "body": self.default_object_body,
                             "content-type": self.default_object_content_type
                             }

        response = get_object(request_input)

        self.assertEqual(200, response.status_code)
        if isinstance(request_input['body'], bytes):
            self.assertEqual(request_input['body'], response.content)
        else:
            self.assertEqual(request_input['body'], response.content.decode("utf-8"))
        self.assertEqual(request_input['content-type'], response.headers['content-type'])
        self.assertIn("x-dl-meta-dl-collection-id", response.headers)
        self.assertEqual(response.headers['x-dl-meta-dl-collection-id'], request_input['collection-id'])
        self.assertIn("x-dl-meta-dl-object-id", response.headers)
        self.assertEqual(response.headers['x-dl-meta-dl-object-id'], request_input['object-id'])
        self.assertIn("x-dl-meta-dl-version-number", response.headers)
        self.assertIn("x-dl-meta-dl-version-timestamp", response.headers)
        self.assertIn("x-dl-meta-dl-content-sha1", response.headers)
        if 'object-metadata' in request_input:
            for key, value in request_input['object-metadata'].items():
                header_name = "x-dl-meta-{}".format(key)
                self.assertIn(header_name, response.headers)
                self.assertEqual(response.headers[header_name], value)
        self.assertGreater(len(response.content), 0)
        return response

    # + get changeset object
    def test_get_changeset_object(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": self.default_collection_id,
                             "object-id": self.default_object_id,
                             "version-number": 1,
                             "changeset-id": self.default_changeset_id,
                             "body": self.default_object_body,
                             "content-type": self.default_object_content_type
                             }

        response = get_object(request_input)

        self.assertEqual(200, response.status_code)
        if isinstance(request_input['body'], bytes):
            self.assertEqual(request_input['body'], response.content)
        else:
            self.assertEqual(request_input['body'], response.content.decode("utf-8"))
        self.assertEqual(request_input['content-type'], response.headers['content-type'])
        self.assertIn("x-dl-meta-dl-collection-id", response.headers)
        self.assertEqual(response.headers['x-dl-meta-dl-collection-id'], request_input['collection-id'])
        self.assertIn("x-dl-meta-dl-object-id", response.headers)
        self.assertEqual(response.headers['x-dl-meta-dl-object-id'], request_input['object-id'])
        self.assertIn("x-dl-meta-dl-version-number", response.headers)
        self.assertIn("x-dl-meta-dl-version-timestamp", response.headers)
        self.assertIn("x-dl-meta-dl-content-sha1", response.headers)
        if 'object-metadata' in request_input:
            for key, value in request_input['object-metadata'].items():
                header_name = "x-dl-meta-{}".format(key)
                self.assertIn(header_name, response.headers)
                self.assertEqual(response.headers[header_name], value)
        self.assertGreater(len(response.content), 0)
        return response

    def test_get_object_removed_version(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            # Object has been removed as of version 2, still able to get version 1
            request_input = {"collection-id": self.default_collection_id,
                             "object-id": self.default_object_id,
                             "body": self.default_object_body,
                             "content-type": self.default_object_content_type,
                             "version-number": 1
                             }

        response = get_object(request_input)
        self.assertEqual(200, response.status_code)
        if isinstance(request_input['body'], bytes):
            self.assertEqual(request_input['body'], response.content)
        else:
            self.assertEqual(request_input['body'], response.content.decode("utf-8"))
        self.assertEqual(request_input['content-type'], response.headers['content-type'])
        self.assertIn("x-dl-meta-dl-collection-id", response.headers)
        self.assertEqual(response.headers['x-dl-meta-dl-collection-id'], request_input['collection-id'])
        self.assertIn("x-dl-meta-dl-object-id", response.headers)
        self.assertEqual(response.headers['x-dl-meta-dl-object-id'], request_input['object-id'])
        self.assertIn("x-dl-meta-dl-version-number", response.headers)
        self.assertIn("x-dl-meta-dl-version-timestamp", response.headers)
        self.assertIn("x-dl-meta-dl-content-sha1", response.headers)
        if 'object-metadata' in request_input:
            for key, value in request_input['object-metadata'].items():
                header_name = "x-dl-meta-{}".format(key)
                self.assertIn(header_name, response.headers)
                self.assertEqual(response.headers[header_name], value)
        self.assertGreater(len(response.content), 0)
        return response

    # - folder object id
    def test_get_object_invalid_folder(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": self.default_multi_version_collection_id,
                             "object-id": self.default_folder_object_id
                             }
        stage = os.environ['STAGE']

        response = get_object(request_input)
        self.assertEqual(405, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAllowedError")
        self.assertEqual(json_error_resp['message'],
                         "Object ID {0} is a multipart object and must be retrieved through the object-key-url".format(
                             request_input['object-id']))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Multipart object can be accessed directly using object-key-url from GET /objects/{1}/describe"
                         "/?collection-id={3}&object-id={2}".format(request_input['object-id'], stage,
                                                                    request_input['object-id'],
                                                                    request_input['collection-id']))

    # - invalid object id 404
    def test_get_object_invalid_object_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_collection_id,
                    "object-id": 'regression-abcdefghijklmnopqrstuvwxyz'
                }

        response = get_object(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchObject")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Object ID {0} in Collection ID {1}".format(request_input['object-id'],
                                                                             request_input['collection-id']))

    # - invalid collection id 404
    def test_get_object_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": 'regression-abcdefghijklmnopqrstuvwxyz',
                             "object-id": self.default_object_id
                             }

        response = get_object(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - invalid get removed object
    def test_get_object_removed(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": self.default_multi_version_collection_id,
                             "object-id": self.default_removed_object_id
                             }

        response = get_object(request_input)
        self.assertEqual(422, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Object ID {0} in Collection ID {1} has been removed".format(request_input['object-id'],
                                                                                      request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Removed objects cannot be retrieved")

    def test_get_object_removed_version_fail(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {"collection-id": self.default_multi_version_collection_id,
                             "object-id": self.default_removed_object_id,
                             }

        response = get_object(request_input)
        self.assertEqual(422, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Object ID {0} in Collection ID {1} has been removed".format(request_input['object-id'],
                                                                                      request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Removed objects cannot be retrieved")

    # - get object 400 - invalid version #
    def test_get_object_invalid_version_number(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_zero_version_collection_id,
                "object-id": self.default_object_id,
                "version-number": 0
            }
        response = get_object(request_input)
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyName")
        self.assertEqual(json_error_resp['message'], "Request did not match JSON schema")

    # - get changeset no version number provided
    def test_get_changeset_object_no_version_number(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "object-id": self.default_object_id,
                "changeset-id": self.default_changeset_id
            }
        response = get_object(request_input)
        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Version number does not exist in the request")

    # - get changeset object 404 - non-existent changeset
    def test_get_changeset_object_no_changeset(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "object-id": self.default_object_id,
                "changeset-id": 'non-existent-changeset',
                "version-number": 1
            }
        response = get_object(request_input)
        self.assertEqual(404, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchChangeset")
        self.assertEqual(json_error_resp['message'], "Invalid Changeset ID {0}".format(request_input['changeset-id']))

    # - get changeset object 404 - non-existent object
    def test_get_changeset_object_no_object(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "object-id": 'non-existent-object',
                "changeset-id": self.default_changeset_id,
                "version-number": 1
            }
        response = get_object(request_input)
        self.assertEqual(404, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchObject")
        self.assertEqual(json_error_resp['message'], "Invalid Object ID {0} in Changeset ID {1}"
                         .format(request_input['object-id'],
                                 request_input['changeset-id']))

    def test_get_changeset_object_removed(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "object-id": self.default_removed_changeset_object_id,
                "changeset-id": self.default_changeset_id,
                "version-number": 3
            }
        response = get_object(request_input)
        self.assertEqual(422, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "Object ID {0} in Changeset ID {1} has been removed"
                         .format(request_input['object-id'],
                                 request_input['changeset-id']))

    # - get object 404 - deleted version
    def test_get_object_version_not_found(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            # Version #1 is the oldest version
            request_input = {
                "collection-id": self.default_zero_version_collection_id,
                "object-id": self.default_object_id,
                "version-number": 1
            }

        response = get_object(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchObjectVersion")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Version Number {0} for Object ID {1} in Collection ID {2}".format(
                             request_input['version-number'],
                             request_input['object-id'],
                             request_input['collection-id']))

    # - test get object - invalid content type - 415
    def test_get_object_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id,
        }
        response = get_object(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_get_object_authentication_error(self):
        request_input = {
            'headers': {
                'Content-type': 'application/json'
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id
        }

        response = get_object(request_input)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
