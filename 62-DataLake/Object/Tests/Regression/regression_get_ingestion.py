import os
import unittest

from lng_datalake_client.Object.get_ingestion import get_ingestion

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestGetIngestion(unittest.TestCase):
    default_invalid_ingestion_id = os.getenv("INVALID_INGESTION_ID", "invalid-ingestion-id-0123")
    default_ingestion_id = os.getenv("INGESTION_ID", "1524439.zip")
    default_ingestion_state = os.getenv("INGESTION_STATE", "Processing")
    default_ingestion_url = os.getenv("INGESTION_URL", "/objects/LATEST/ingestion/1524439.zip")
    default_collection_id = os.getenv("COLLECTION_ID", "151")
    default_collection_url = os.getenv("COLLECTION_URL", "/collections/LATEST/151")
    default_owner_id = int(os.getenv("OWNER_ID", 426))
    default_archive_format = os.getenv("ARCHIVE_FORMAT", "zip")
    default_ingestion_timestamp = os.getenv("INGESTION_TIMESTAMP", "2019-03-06T18:01:49.119Z")
    default_bucket_name = os.getenv("BUCKET_NAME", "feature-jek-dl-object-staging-288044017584-use1")
    default_ingestion_expiration_date = os.getenv("INGESTION_EXPIRATION_DATE", "2020-07-20T17:25:51.000Z")
    default_upload_id = os.getenv("UPLOAD_ID",
                                  "0_pRcfcLi3QM3hH0osC.ynlng5AzMBtXzK63bkTrWJ2eDNCJC.YG0dJbsJTGVkng9XS7QdgLL_RWQM4ZLHfi"
                                  "3XAA4CXzOca5WQvTeB5J..tM2jq3WriUz74rkPOF9iFmSWossVpgtfsyaf1VnZ7iSF0lCo.0s6Y5Z_0K7Tc9"
                                  ".rCbpxXhqgJ2Jif7BTwDv2RhtTriHNMAf1uqlmYE9k4C4g--")

    # + Test the 200 status code and ingestion properties for GET_INGESTION
    def test_get_ingestion(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        ingestion_id = input_dict.get('ingestion-id', self.default_ingestion_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "ingestion-id": ingestion_id
            }

        # ingestion required properties
        expected_response = \
            {
                "ingestion-state": input_dict.get("ingestion-state", self.default_ingestion_state),
                "collection-id": input_dict.get("collection-id", self.default_collection_id),
                "collection-url": input_dict.get("collection-url", self.default_collection_url),
                "ingestion-id": ingestion_id,
                "ingestion-url": input_dict.get("ingestion-url", self.default_ingestion_url),
                "owner-id": input_dict.get("owner-id", self.default_owner_id),
                "archive-format": input_dict.get("archive-format", self.default_archive_format),
                "ingestion-timestamp": input_dict.get("ingestion-timestamp", self.default_ingestion_timestamp),
                "upload-id": input_dict.get("upload-id", self.default_upload_id),
                "bucket-name": input_dict.get("bucket-name", self.default_bucket_name),
                "ingestion-expiration-date": input_dict.get("ingestion-expiration-date",
                                                            self.default_ingestion_expiration_date)
            }
        optional_properties = ["object-count", "object-tracked-count", "object-processed-count", "object-updated-count",
                               "object-error-count", "error-description", "description"]

        # Add any additional attributes to validate against the ingestion
        for key, value in input_dict.items():
            if key not in expected_response and key in optional_properties:
                expected_response[key] = value

        response = get_ingestion(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("ingestion", response_dict)

        # Copy from the response the optional properties not existing in the expected response.
        # Validate the properties and their types
        for key, value in response_dict['ingestion'].items():
            if key not in expected_response:
                self.assertIn(key, optional_properties)
                if 'count' in key:
                    self.assertTrue(isinstance(value, int))
                else:
                    self.assertTrue(isinstance(value, str))
                expected_response[key] = value

        # test the response body
        self.assertDictEqual(expected_response, response_dict["ingestion"])
        return response_dict["ingestion"]

    # - get_ingestion with invalid ingestion id - 404 status code
    def test_get_ingestion_invalid_ingestion_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        invalid_ingestion_id = input_dict.get('ingestion-id', self.default_invalid_ingestion_id)
        request_dict = {
            "ingestion-id": invalid_ingestion_id
        }

        response = get_ingestion(request_dict)

        # verify the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchIngestion")
        self.assertEqual(json_error_resp['message'], "Invalid Ingestion ID {}".format(invalid_ingestion_id))
        self.assertEqual(json_error_resp['corrective-action'], "Ingestion ID does not exist")

    # - get_ingestion with invalid x-api-key - 403 status code
    def test_get_ingestion_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "ingestion-id": self.default_ingestion_id
        }

        response = get_ingestion(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - get_ingestion with invalid content-type - 415 status code
    def test_get_ingestion_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "ingestion-id": self.default_ingestion_id
        }

        response = get_ingestion(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
