import os
import unittest

from lng_datalake_client.Object.cancel_ingestion import cancel_ingestion
from lng_datalake_constants import ingestion_status

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestCancelIngestion(unittest.TestCase):
    default_invalid_ingestion_id = os.getenv("INVALID_INGESTION_ID", "invalid-ingestion-id-0123")
    default_ingestion_id = os.getenv("INGESTION_ID", "1524439.zip")
    default_ingestion_url = os.getenv("INGESTION_URL", "/objects/LATEST/ingestion/1524439.zip")
    default_collection_id = os.getenv("COLLECTION_ID", "151")
    default_collection_url = os.getenv("COLLECTION_URL", "/collections/LATEST/151")
    default_owner_id = int(os.getenv("OWNER_ID", 426))
    default_archive_format = os.getenv("ARCHIVE_FORMAT", "zip")
    default_ingestion_timestamp = os.getenv("INGESTION_TIMESTAMP", "2019-03-06T18:01:49.119Z")
    default_ingestion_expiration_date = os.getenv("INGESTION_EXPIRATION_DATE", "2020-07-20T17:25:51.000Z")

    # + Test the 202 status code and ingestion properties for CANCEL_INGESTION
    def test_cancel_ingestion(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        ingestion_id = input_dict.get('ingestion-id', self.default_ingestion_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "ingestion-id": ingestion_id
            }

        # ingestion required properties
        expected_response = \
            {
                "ingestion-state": ingestion_status.CANCELLED,
                "collection-id": input_dict.get("collection-id", self.default_collection_id),
                "collection-url": input_dict.get("collection-url", self.default_collection_url),
                "ingestion-id": ingestion_id,
                "ingestion-url": input_dict.get("ingestion-url", self.default_ingestion_url),
                "owner-id": input_dict.get("owner-id", self.default_owner_id),
                "archive-format": input_dict.get("archive-format", self.default_archive_format),
                "ingestion-timestamp": input_dict.get("ingestion-timestamp", self.default_ingestion_timestamp),
                "ingestion-expiration-date": input_dict.get("ingestion-expiration-date",
                                                            self.default_ingestion_expiration_date)
            }

        response = cancel_ingestion(request_dict)

        # test the status code 202
        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("ingestion", response_dict)

        # test the response body
        self.assertDictEqual(expected_response, response_dict["ingestion"])
        return response_dict["ingestion"]

    # - cancel_ingestion with invalid ingestion id - 404 status code
    def test_cancel_ingestion_invalid_ingestion_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        invalid_ingestion_id = input_dict.get('ingestion-id', self.default_invalid_ingestion_id)
        request_dict = {
            "ingestion-id": invalid_ingestion_id
        }

        response = cancel_ingestion(request_dict)

        # verify the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchIngestion")
        self.assertEqual(json_error_resp['message'], "Invalid Ingestion ID {0}".format(invalid_ingestion_id))
        self.assertEqual(json_error_resp['corrective-action'], "Ingestion ID does not exist")


# - cancel_ingestion with invalid state - 409 status code
    def test_cancel_ingestion_invalid_state(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        ingestion_id = input_dict.get('ingestion-id', self.default_ingestion_id)
        request_dict = {
            "ingestion-id": ingestion_id
        }

        response = cancel_ingestion(request_dict)

        # verify the status code 409
        self.assertEqual(409, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "ForbiddenStateTransitionError")
        self.assertEqual(json_error_resp['message'], "Cannot cancel ingestion in Cancelled state")
        self.assertEqual(json_error_resp['corrective-action'],
                         "Only a {0} ingestion can be cancelled".format(ingestion_status.PENDING))

    # - cancel_ingestion with invalid x-api-key - 403 status code
    def test_cancel_ingestion_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "ingestion-id": self.default_ingestion_id
        }

        response = cancel_ingestion(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - cancel_ingestion with invalid content-type - 415 status code
    def test_cancel_ingestion_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "ingestion-id": self.default_ingestion_id
        }

        response = cancel_ingestion(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
