import os
import unittest

from lng_datalake_client.Object.list_objects import list_objects

__author__ = "Kiran G, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestListObjects(unittest.TestCase):
    default_collection_id = os.getenv("COLLECTION_ID", '32')

    # + list objects
    def test_list_objects_all(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id
            }

        response = list_objects(request_input)
        self.assertEqual(200, response.status_code)
        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertGreater(len(response_dict["objects"]), 0)
        self.assertTrue(isinstance(response_dict["item-count"], int))

    # + list objects - with object state filter
    def test_list_objects_with_object_state_filter(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "state": 'Created'
            }

        response = list_objects(request_input)
        self.assertEqual(200, response.status_code)
        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertGreater(len(response_dict["objects"]), 0)
        self.assertTrue(isinstance(response_dict["item-count"], int))
        # check if all the object states are as per filter
        for obj in response_dict["objects"]:
            self.assertEqual(obj["object-state"].lower(), request_input['state'].lower())

    # + Test list objects with pagination
    def test_list_objects_pagination(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "collection-id": self.default_collection_id,
                "max-items": 1
            }
        response_1 = list_objects(request_input)
        self.assertEqual(200, response_1.status_code)
        response_dict_1 = response_1.json()
        self.assertIn("context", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_dict_1["context"]["stage"])
        self.assertIn("objects", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["item-count"], int))
        self.assertEqual(request_input["max-items"], len(response_dict_1['objects']))
        self.assertEqual(response_dict_1['item-count'], request_input['max-items'])

        self.assertTrue(isinstance(response_dict_1["next-token"], str))

        # make a request again with the pagination token ------
        request_input["next-token"] = response_dict_1['next-token']
        response_2 = list_objects(request_input)
        self.assertEqual(200, response_2.status_code)
        response_dict_2 = response_2.json()
        self.assertIn("context", response_dict_2)
        self.assertTrue(isinstance(response_dict_2["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_dict_2["context"]["stage"])
        self.assertIn("objects", response_dict_2)
        self.assertEqual(request_input["max-items"], len(response_dict_2['objects']))
        self.assertTrue(isinstance(response_dict_2["item-count"], int))
        self.assertEqual(response_dict_2['item-count'], request_input['max-items'])

        self.assertNotEqual(response_dict_1["objects"], response_dict_2["objects"])

    # - list objects with invalid max-results value
    def test_list_invalid_objects_pagination(self):
        input_dict = {
            "collection-id": self.default_collection_id,
            "max-items": 1001
        }
        response = list_objects(input_dict)
        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid max-items value: 1001")
        self.assertEqual(json_error_resp['corrective-action'], "Value must be between 1 and 1000")

    # - list objects with invalid max-results value
    def test_list_invalid_objects_pagination_diff_max_items(self):
        input_dict = {
            "collection-id": self.default_collection_id,
            "max-items": 1,
            "next-token": "eyJtYXgtaXRlbXMiOiA0LCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkPSJ9"
        }
        response = list_objects(input_dict)
        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Continuation request must pass the same max-items as initial request")
        self.assertEqual(json_error_resp['corrective-action'], "Set max-items to 4 and try again")

    # - list objects with invalid state filter
    def test_list_objects_invalid_state_filter(self):
        input_dict = {
            "state": 'XYZ'
        }
        response = list_objects(input_dict)
        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid state value: XYZ")

    # - list object invalid collection id
    def test_list_objects_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz'
            }

        response = list_objects(request_input)
        self.assertEqual(200, response.status_code)
        # test the empty response
        response_dict = response.json()
        self.assertEqual([], response_dict["objects"])

        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.getenv('STAGE'), response_dict['context']['stage'])

        self.assertIn("objects", response_dict)
        self.assertEqual(len(response_dict["objects"]), 0)
        self.assertEqual(response_dict['item-count'], 0)
        self.assertNotIn('next-token', response_dict)

    # - test list object - invalid content type - 415
    def test_list_objects_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id
        }
        response = list_objects(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_list_objects_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "collection-id": self.default_collection_id
        }

        response = list_objects(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
