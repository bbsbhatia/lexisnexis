import os
import unittest

from lng_datalake_client.Object.close_changeset import close_changeset

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestCloseChangeset(unittest.TestCase):
    # TODO: Add this to regression orchestration
    default_invalid_changeset_id = os.getenv("INVALID_CHANGESET_ID", "invalid-changeset-id-0123")
    default_changeset_id = os.getenv("CHANGESET_ID", "14b49929-4568-487b-8d4c-facb8149e26a")
    default_changeset_state = os.getenv("CHANGESET_STATE", "Closing")
    default_owner_id = int(os.getenv("OWNER_ID", 31))

    default_changeset_url = os.getenv("CHANGESET_URL", "/objects/{}/changeset/{}").format('LATEST',
                                                                                          default_changeset_id)
    default_changeset_timestamp = os.getenv("CHANGESET_TIMESTAMP", "2019-11-06T02:28:51.525Z")
    default_changeset_expiration_date = os.getenv("CHANGESET_EXPIRATION_DATE", "2019-12-06T02:28:51.000Z")

    # + close_changeset - 202
    def test_close_changeset(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "changeset-id": self.default_changeset_id
            }

        stage = os.environ['STAGE']

        # changeset required properties
        expected_response = \
            {
                "changeset-state": request_input.get("changeset-state", self.default_changeset_state),
                "changeset-id": request_input["changeset-id"],
                "changeset-url": request_input.get("changeset-url", self.default_changeset_url),
                "owner-id": request_input.get("owner-id", self.default_owner_id),
                "changeset-timestamp": request_input.get("changeset-timestamp", self.default_changeset_timestamp),
                "changeset-expiration-date": request_input.get("changeset-expiration-date",
                                                            self.default_changeset_expiration_date)
            }

        optional_properties = ["description"]
        # Add any additional attributes to validate against the changeset
        for key, value in request_input.items():
            if key not in expected_response and key in optional_properties:
                expected_response[key] = value

        response = close_changeset(request_input)

        # test the status code - 202
        self.assertEqual(202, response.status_code)
        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("changeset", response_dict)

        # Copy from the response the optional properties not existing in the expected response.
        # Validate the properties and their types
        for key, value in response_dict['changeset'].items():
            if key not in expected_response:
                self.assertIn(key, optional_properties)
                self.assertTrue(isinstance(value, str))
                expected_response[key] = value

        # test the response body
        self.assertDictEqual(expected_response, response_dict["changeset"])
        return response_dict["changeset"]

    # - close_changeset - changeset id does not exist - 404 status code
    def test_close_changeset_does_not_exist(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = {
                "changeset-id": self.default_invalid_changeset_id
            }

        response = close_changeset(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchChangeset")
        self.assertEqual(json_error_resp['message'], "Invalid Changeset ID {}".format(request_input['changeset-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Changeset ID does not exist")

    # - close_changeset with invalid x-api-key - 403 status code
    def test_close_changeset_invalid_x_api_key(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "changeset-id": self.default_changeset_id
        }

        response = close_changeset(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - close_changeset with invalid content-type - 415 status code
    def test_close_changeset_unsupported_media(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            'headers': {
                'Content-type': 'application/xml',
                'x-api-key': key
            },
            "changeset-id": self.default_changeset_id
        }

        response = close_changeset(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
