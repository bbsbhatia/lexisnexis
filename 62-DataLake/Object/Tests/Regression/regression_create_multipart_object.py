import os
import unittest

from lng_datalake_client.Object.create_multipart_object import create_multipart_object
from lng_datalake_constants import collection_status

__author__ = "Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

unittest.TestCase.maxDiff = None


class TestCreateMultipartObject(unittest.TestCase):
    default_collection_id = os.getenv("COLLECTION_ID", '1')
    default_invalid_collection_id = os.getenv("INVALID_COLLECTION_ID", 'suspended_collection')
    default_invalid_collection_state = os.getenv("INVALID_COLLECTION_STATE", collection_status.SUSPENDED)
    default_object_id = os.getenv("OBJECT_ID", "prashant-20")
    default_content_type = os.getenv("CONTENT_TYPE", "text/plain")
    default_owner_id = int(os.getenv("OWNER_ID", '10'))
    default_asset_id = int(os.getenv("ASSET_ID", '62'))
    default_duplicate_object_id = os.getenv("DUPLICATE_OBJECT_ID", "test_multipart_object")

    # + Create multipart object with id
    def test_create_multipart_object_success(self, input_dict=None, test_collection=True):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_collection_id,
                    "object-id": self.default_object_id,
                    "content-type": self.default_content_type
                }

        stage = os.environ['STAGE']
        expected_response = \
            {
                "collection-id": request_input['collection-id'],
                "object-id": request_input['object-id'],
                "object-state": "Pending",
                "collection-url": "/collections/{0}/{1}".format(stage, request_input['collection-id']),
                "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, request_input['object-id'],
                                                                          request_input['collection-id']),
                "asset-id": input_dict.get("asset-id", self.default_asset_id)
            }

        response = create_multipart_object(request_input)
        self.assertEqual(201, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("multipart-upload", response_dict)
        resp_multipart_props = response_dict["multipart-upload"]
        self.assertTrue(isinstance(resp_multipart_props.get('UploadId'), str))
        self.assertIn("S3", resp_multipart_props)
        self.assertTrue(isinstance(resp_multipart_props['S3']['Bucket'], str))
        self.assertTrue(isinstance(resp_multipart_props['S3']['Key'], str))

        self.assertIn("object", response_dict)

        resp_multipart_obj_props = response_dict['object']
        self.assertTrue(isinstance(resp_multipart_obj_props.pop('owner-id'), int))
        if test_collection:
            self.assertTrue(isinstance(resp_multipart_obj_props.pop('object-expiration-date'), str))

        # TODO: Needs more validation
        self.assertTrue(isinstance(resp_multipart_obj_props['temporary-object-key-url'], str))
        self.assertTrue(resp_multipart_obj_props.pop('temporary-object-key-url').startswith("https://"))

        self.maxDiff = None

        # check for object metadata in request and copy to expected response
        object_metadata = {}
        if 'headers' in request_input:
            for key, value in request_input['headers'].items():
                if key.startswith("x-dl-meta-"):
                    object_metadata[key[10:]] = value
        if object_metadata:
            expected_response['object-metadata'] = object_metadata

        self.assertDictEqual(expected_response, resp_multipart_obj_props)
        return response

    # - Create multipart object - duplicate object id
    def test_create_multipart_object_duplicate_object_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_collection_id,
                    "object-id": self.default_duplicate_object_id,
                    "content-type": self.default_content_type
                }

        response = create_multipart_object(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Object ID {} already exist in Collection ID {}".format(request_input['object-id'],
                                                                                 request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Duplicated Object IDs within the same Collection ID are not allowed")

    # - Create multipart object - invalid collection-id 404
    def test_create_multipart_object_invalid_collection_id(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": 'regression-abcdefghijklmnopqrstuvwxyz',
                    "object-id": self.default_object_id,
                    "content-type": self.default_content_type
                }

        response = create_multipart_object(request_input)
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {}".format(request_input['collection-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - Collection with invalid state - 422
    def test_create_multipart_object_invalid_collection_state(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_invalid_collection_id,
                    "object-id": self.default_object_id,
                    "content-type": self.default_content_type
                }
        collection_state = input_dict.get("collection-state", self.default_invalid_collection_state)

        response = create_multipart_object(request_input)
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Collection ID {0} is not set to {1} (state={2})".format(request_input['collection-id'],
                                                                                  collection_status.CREATED,

                                                                                  collection_state))

    # - Object Metadata with non ascii - 400
    def test_create_multipart_object_invalid_metadata_character(self, input_dict=None):
        if input_dict:
            request_input = input_dict
        else:
            request_input = \
                {
                    "collection-id": self.default_collection_id,
                    'body': "empty body",
                    'object-id': self.default_object_id
                }
        if 'headers' in request_input:
            request_input['headers']['x-dl-meta-invalid'] = "Invalid Char: ì"
        else:
            request_input['headers'] = {'x-dl-meta-invalid': "Invalid Char: ì"}
        print(request_input)
        response = create_multipart_object(request_input)
        print(response.json())
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid object metadata")
        self.assertEqual(json_error_resp['corrective-action'], "Object metadata key/value must be US-ASCII")


    # - invalid content type - 415
    def test_create_multipart_object_invalid_content_type(self):
        input_dict = {
            "headers": {
                'Content-type': 'application/xml+xml',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id
        }
        response = create_multipart_object(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - invalid content type - 415
    def test_create_multipart_object_invalid_content_type_2(self):
        input_dict = {
            "headers": {
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id,
            "stored-content-type": "application/xml+xml"
        }
        response = create_multipart_object(input_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - no x-api-key, auth error
    def test_create_multipart_object_authentication_error(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "collection-id": self.default_collection_id,
            "object-id": self.default_object_id
        }

        response = create_multipart_object(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")


if __name__ == '__main__':
    unittest.main()
