import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_changeset_command import lambda_handler

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetChangesetCommand')


class TestGetChangesetCommand(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        TableCache.clear()

    # +lambda_handler - valid
    @patch.dict(os.environ, {'CHANGESET_DYNAMODB_TABLE': 'fake_changeset_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_get_changset_command(self, dynamodb_get_client_mock):
        dynamodb_get_client_mock.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.changesetTable.getItem.json')

        request_input = io_util.load_data_json('apigateway.request.json')
        expected_response = io_util.load_data_json('apigateway.response.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response, lambda_handler(request_input, MockLambdaContext()))

        dynamodb_get_client_mock.return_value.get_item.assert_called_with(Key={'ChangesetID': {'S': '007ab2c8-c687-48a7-ace3-5428605a9345'}}, TableName='fake_changeset_table')


if __name__ == '__main__':
    unittest.main()
