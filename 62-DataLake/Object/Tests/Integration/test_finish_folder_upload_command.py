import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import service_commons.object_common as object_common_module
import finish_folder_upload_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'FinishFolderUploadCommand')


class TestFinishFolderUploadCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        TableCache.clear()
        reload(finish_folder_upload_command)
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()
        reload(finish_folder_upload_command)

    # +lambda_handler - valid
    @patch("lng_aws_clients.s3.get_client")
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_aws_clients.session.set_session')
    def test_finish_folder_upload_command(self, session_mock, mock_owner_authorization,
                                          mock_dynamodb_get_client, mock_s3_get_client):

        mock_owner_authorization.return_value = {}

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        # The mock-outs
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json('dynamodb.collection_data_valid.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.tracking_data_valid.json')

        mock_s3_get_client.return_value.list_objects_v2.return_value = \
            io_util.load_data_json('s3.valid_list.json')

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_valid.json')

        with patch('finish_folder_upload_command.staging_bucket_name', 'bucket_name'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                self.assertEqual(finish_folder_upload_command.lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                                 expected_response)


if __name__ == '__main__':
    unittest.main()
