import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_commons import publish_sns_topic
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import close_changeset_event_handler

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CloseChangesetEventHandler")

topic_arn = 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'


class CloseChangesetEventHandlerTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': topic_arn,
                             'LAMBDA_TASK_ROOT': os.path.dirname(io_utils.schema_path)})
    def setUpClass(cls):  # NOSONAR
        TableCache.clear()
        reload(close_changeset_event_handler)
        reload(publish_sns_topic)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()
        reload(close_changeset_event_handler)

    @patch.dict(os.environ, {"CHANGESET_DYNAMODB_TABLE": 'fake_changeset_table',
                             "TRACKING_DYNAMODB_TABLE": 'fake_tracking_table',
                             "COLLECTION_BLOCKER_DYNAMODB_TABLE": 'fake_collection_blocker_table'})
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_close_changeset_event_handler_success(self, mock_dynamodb_client, aws_session_mock,
                                                   mock_sns_publish, mock_helper):
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None

        def mock_get_item(TableName, Key, ConsistentRead):  # NOSONAR
            if TableName == 'fake_changeset_table':
                return io_utils.load_data_json('dynamodb.changesetTable.getItem.json')
            else:
                return None

        mock_dynamodb_client.return_value.get_item = mock_get_item

        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_utils.load_data_json('tracking.query.json'), io_utils.load_data_json('dynamodb.query_items_empty.json'),
            io_utils.load_data_json('dynamodb.query_items_empty.json'),
            io_utils.load_data_json('dynamodb.changesetCollectionTable.query.json'),
            io_utils.load_data_json('dynamodb.query_items_empty.json')
        ]

        mock_dynamodb_client.return_value.put_item.return_value = io_utils.load_data_json(
            'dynamodb.put_item.json')
        mock_dynamodb_client.return_value.delete_item.return_value = io_utils.load_data_json(
            'dynamodb.delete_item.json')
        mock_sns_publish.return_value.publish.return_value = io_utils.load_data_json('sns.publish.json')
        event = io_utils.load_data_json('valid_cmd_event.json')
        self.assertEqual(close_changeset_event_handler.lambda_handler(event, MockLambdaContext),
                         event_handler_status.SUCCESS)
        self.assertEqual(mock_dynamodb_client.return_value.put_item.call_count, 2)
        notification = io_utils.load_data_json('notification_v1.json')
        mock_sns_publish.return_value.publish.assert_called_with(TargetArn=topic_arn, Message=notification["message"],
                                                                 MessageStructure="json",
                                                                 MessageAttributes=notification["message_attributes"])


if __name__ == '__main__':
    unittest.main()
