import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import cancel_ingestion_command

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CancelIngestionCommand')
table_dict = {
    'INGESTION_DYNAMODB_TABLE': 'fake_ingestion_table',
    'COLLECTION_BLOCKER_DYNAMODB_TABLE': 'fake_collection_blocker_table',
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table'
}


@patch.dict(os.environ, table_dict)
class TestCancelIngestionCommand(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(cancel_ingestion_command)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_ingestion_multipart_upload_command_success(self, session_mock, dynamodb_client_mock):

        session_mock.return_value = None

        def mock_get_item(TableName, Key, **kwargs):  # NOSONAR
            if TableName == 'fake_ingestion_table':
                return io_util.load_data_json('dynamodb.ingestionTable.getItem.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.ownerTable.getItem.json')
            else:
                return None

        dynamodb_client_mock.return_value.get_item = mock_get_item

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(
                cancel_ingestion_command.lambda_handler(request_input, MockLambdaContext()),
                response_output)


if __name__ == '__main__':
    unittest.main()
