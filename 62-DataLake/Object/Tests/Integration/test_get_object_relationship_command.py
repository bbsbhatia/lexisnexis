import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_command as object_command
from get_object_relationship_command import lambda_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetObjectRelationshipCommand')


class IntegrationTestGetObjectRelationshipCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                             'OBJECT_STORE_VERSION_DYNAMODB_TABLE': 'fake_objectstore_version_table',
                             'RELATIONSHIP_OWNER_DYNAMODB_TABLE': 'fake_relationship_owner_table',
                             'OBJECT_RELATIONSHIP_DYNAMODB_TABLE': 'fake_object_relationship_table'
                             })
    def setUpClass(cls):  # NOSONAR
        reload(object_command)

    # relationship_version provided
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_integration_get_object_relationship_command(self, session_mock, dynamodb_get_client_mock):
        session_mock.return_value = None
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.side_effect = [
            io_util.load_data_json('dynamodb.collectionTable.getItem.json'),
            io_util.load_data_json('dynamodb.objectStoreTable.getItem.json'),
            io_util.load_data_json('dynamodb.objectStoreVersionTable.getItem.json'),
            io_util.load_data_json('dynamodb.relationshipOwnerTable.getItem.json'),
            io_util.load_data_json('dynamodb.objectRelationshipTable.getItem.json')]
        request_input = io_util.load_data_json('apigateway.request.json')
        response_output = io_util.load_data_json('apigateway.response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)

    # relationship_version not provided
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_integration_get_object_relationship_command_2(self, session_mock, dynamodb_get_client_mock):
        session_mock.return_value = None
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.side_effect = [
            io_util.load_data_json('dynamodb.collectionTable.getItem.json'),
            io_util.load_data_json('dynamodb.objectStoreTable.getItem.json'),
            io_util.load_data_json('dynamodb.objectStoreVersionTable.getItem.json'),
            io_util.load_data_json('dynamodb.relationshipOwnerTable.getItem.json')]
        # object store table query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = io_util.load_data_json(
            'dynamodb.objectRelationshipTable.queryItems.json')

        request_input = io_util.load_data_json('apigateway.request2.json')
        response_output = io_util.load_data_json('apigateway.response2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)


if __name__ == '__main__':
    unittest.main()
