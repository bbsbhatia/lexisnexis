import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import close_changeset_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CloseChangesetCommand')
table_dict = {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
              "CHANGESET_DYNAMODB_TABLE": 'fake_changeset_table'}


@patch.dict(os.environ, table_dict)
class TestCloseChangesetCommand(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.maxDiff = None
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_close_changeset_command_success(self, dynamodb_client_mock):

        def mock_get_item(TableName, Key, ConsistentRead=False):  # NOSONAR
            if TableName == 'fake_owner_table':
                self.assertFalse(ConsistentRead)
                return io_util.load_data_json('dynamodb.ownerTable.getItem.json')
            elif TableName == 'fake_changeset_table':
                self.assertTrue(ConsistentRead)
                return io_util.load_data_json('dynamodb.changesetTable.getItem.json')
            else:
                return None

        dynamodb_client_mock.return_value.get_item = mock_get_item
        dynamodb_client_mock.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(
                close_changeset_command.lambda_handler(request_input, MockLambdaContext()),
                response_output)
        self.assertEqual(dynamodb_client_mock.return_value.put_item.call_count, 2)


if __name__ == '__main__':
    unittest.main()
