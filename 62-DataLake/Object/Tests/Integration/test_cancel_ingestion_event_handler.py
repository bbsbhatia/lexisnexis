import os
import unittest
from importlib import reload
from unittest.mock import patch
import json

from lng_datalake_constants import event_handler_status
from botocore.exceptions import ClientError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import cancel_ingestion_event_handler

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CancelIngestionEventHandler')
table_dict = {
    'INGESTION_DYNAMODB_TABLE': 'fake_ingestion_table',
    'COLLECTION_BLOCKER_DYNAMODB_TABLE': 'fake_collection_blocker_table',
    'DATALAKE_STAGING_BUCKET_NAME': "bucket-name"
}
mock_queue_url = 'https://sqs.us-east-1.amazonaws.com/123456789012/SuperTestQueue'


@patch.dict(os.environ, table_dict)
class TestCancelIngestionEventHandler(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(cancel_ingestion_event_handler)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + Successful test using lambda_handler
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_ingestion_multipart_upload_success(self, session_mock, dynamodb_client_mock,
                                                       s3_client_mock):
        session_mock.return_value = None

        s3_client_mock.return_value.create_multipart_upload.return_value = io_util.load_data_json(
            'multipart_resp.json')
        dynamodb_client_mock.return_value.get_item.return_value \
            = io_util.load_data_json('dynamodb.ingestionTable.getItem.json')
        dynamodb_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value \
            = io_util.load_data_json('dynamodb.collectionBlockerTable.paginate.json')
        dynamodb_client_mock.return_value.delete_item.return_value \
            = io_util.load_data_json('dynamodb.collectionBlockerTable.delete_item.json')
        dynamodb_client_mock.return_value.put_item.return_value \
            = io_util.load_data_json('dynamodb.ingestionTable.update_item.json')

        event_input = io_util.load_data_json('sns_event.json')
        self.assertEqual(cancel_ingestion_event_handler.lambda_handler(event_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

    # - Test writing retry state to retry queue
    @patch("lng_aws_clients.sqs.get_client")
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_ingestion_multipart_upload_retry_exception(self, session_mock, dynamodb_client_mock,
                                                               s3_client_mock, mock_helper, sqs_client_mock):
        session_mock.return_value = None

        mock_helper.return_value = ['us-east-1']

        s3_client_mock.return_value.create_multipart_upload.return_value = io_util.load_data_json(
            'multipart_resp.json')
        dynamodb_client_mock.return_value.get_item.return_value \
            = io_util.load_data_json('dynamodb.ingestionTable.getItem.json')
        dynamodb_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value \
            = io_util.load_data_json('dynamodb.collectionBlockerTable.paginate.json')
        dynamodb_client_mock.return_value.delete_item.return_value \
            = io_util.load_data_json('dynamodb.collectionBlockerTable.delete_item.json')
        dynamodb_client_mock.return_value.put_item.side_effect \
            = ClientError(io_util.load_data_json('dynamodb.ClientError.json'), "PutItem")

        sqs_client_mock.return_value.send_message.return_value = io_util.load_data_json(
            'sqs.send_message.response.json')

        event_input = io_util.load_data_json('sns_event.json')
        expected_retry_message = io_util.load_data_json('sns_event.json')
        message_body = json.loads(expected_retry_message['Records'][0]['Sns']['Message'])
        message_body['additional-attributes'] = {"retry-state": 3}
        expected_retry_message['Records'][0]['Sns']['Message'] = json.dumps(message_body)
        stringified_expected_retry_message = json.dumps(expected_retry_message).replace(' ', '')
        with patch('lng_datalake_commons.error_handling.error_handler.retry_queue_url',
                   mock_queue_url):
            self.assertIsNone(cancel_ingestion_event_handler.lambda_handler(event_input, MockLambdaContext()))
            sqs_client_mock.return_value.send_message.assert_called_with(
                MessageAttributes={"ThrottleType": {"StringValue": "DynamoDB", "DataType": "String"}},
                MessageBody=stringified_expected_retry_message, QueueUrl=mock_queue_url)


if __name__ == '__main__':
    unittest.main()
