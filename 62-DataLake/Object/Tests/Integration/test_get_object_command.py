import io
import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.response import StreamingBody
from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from get_object_command import lambda_handler

__author__ = "Jason Feng"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetObjectCommand')


@patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                         'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                         'CHANGESET_DYNAMODB_TABLE': 'fake_changeset_table',
                         'CHANGESET_OBJECT_DYNAMODB_TABLE': 'fake_changeset_object_table'})
class IntegrationTestGetObjectCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': "https://datalake_url.com"})
    def setUpClass(cls):  # NOSONAR
        TableCache.clear()
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_get_object_command(self, dynamodb_get_client_mock, s3_get_object_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.side_effect = [
            io_util.load_data_json('dynamodb.collectionTable.getItem.json'),
            io_util.load_data_json('dynamodb.objectStoreTable.getItem.json')]
        # s3:  get object
        s3_get_object_mock.return_value.get_object.return_value = {
            'Body': StreamingBody(io.BytesIO(b"Raw file content"), 16),
            'Metadata': {}
        }
        request_input = io_util.load_data_json('apigateway.request.json')
        response_output = io_util.load_data_json('apigateway.response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)

    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_get_object_command_changeset(self, dynamodb_get_client_mock, s3_get_object_mock):

        def mock_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamodb.changesetTable.getItem.json')
            elif TableName == 'fake_changeset_object_table':
                return io_util.load_data_json('dynamodb.changesetObjectTable.getItem.json')
            elif TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamodb.collectionTable.getItem.json')
            else:
                return None

        s3_get_object_mock.return_value.get_object.return_value = {
            'Body': StreamingBody(io.BytesIO(b"Raw file content"), 16),
            'Metadata': {}
        }
        dynamodb_get_client_mock.return_value.get_item = mock_get_item

        request_input = io_util.load_data_json('apigateway.changeset.request.json')
        response_output = io_util.load_data_json('apigateway.changeset.response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(response_output, lambda_handler(request_input, MockLambdaContext()))


if __name__ == '__main__':
    unittest.main()
