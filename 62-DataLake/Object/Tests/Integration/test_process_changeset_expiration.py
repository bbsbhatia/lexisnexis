import os
import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import process_changeset_expiration

__author__ = "Daniel Wang"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "ProcessChangesetExpiration")


class TestProcessChangesetExpiration(TestCase):

    @classmethod
    @patch.dict(os.environ, {'CHANGESET_EXPIRATION_DYNAMODB_TABLE': 'fake_table',
                             'CHANGESET_EXPIRATION_RETRY_QUEUE_URL': 'fake_url'})
    def setUpClass(cls):    # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):     # NOSONAR
        cls.session_patch.stop()

    # + lambda handler: dynamo stream - put item
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_dynamo(self, mock_dynamo_client):
        input_dict = io_utils.load_data_json('dynamo_stream_event.json')
        mock_dynamo_client.return_value.put_item.return_value = io_utils.load_data_json('dynamodb.put_item.json')
        self.assertEqual(event_handler_status.SUCCESS, process_changeset_expiration.lambda_handler(
            input_dict, MockLambdaContext()))

    # + lambda handler: sqs - put item
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_sqs(self, mock_dynamo_client):
        input_dict = io_utils.load_data_json('sqs_event.json')
        mock_dynamo_client.return_value.put_item.return_value = io_utils.load_data_json('dynamodb.put_item.json')
        self.assertEqual(event_handler_status.SUCCESS, process_changeset_expiration.lambda_handler(
            input_dict, MockLambdaContext()))

    # + lambda handler dynamo stream - send to retry queue
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_retry(self, mock_dynamo_client, mock_sqs_client):
        input_dict = io_utils.load_data_json('dynamo_stream_event.json')
        throttle_error = io_utils.load_data_json('error.throttle.json')
        mock_dynamo_client.return_value.put_item.side_effect = ClientError(throttle_error, 'PutItem')
        mock_sqs_client.return_value.send_message.return_value = None
        self.assertEqual(event_handler_status.SUCCESS, process_changeset_expiration.lambda_handler(
            input_dict, MockLambdaContext()))


if __name__ == '__main__':
    unittest.main()
