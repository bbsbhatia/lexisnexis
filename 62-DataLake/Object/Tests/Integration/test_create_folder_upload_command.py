import datetime
import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import create_folder_upload_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateFolderUploadCommand')


class TestCreateFolderUploadCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.maxDiff = None
        TableCache.clear()
        reload(create_folder_upload_command)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()
        reload(create_folder_upload_command)

    # +lambda_handler - valid
    @patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'COLLECTION_BLCOKER_TABLE': 'fake_collection_blocker_table',
                             "CHANGESET_DYNAMODB_TABLE": 'fake_changeset_table'})
    @patch("lng_aws_clients.sts.get_client")
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_aws_clients.session.set_session')
    def test_create_folder_upload_command(self, session_mock, mock_owner_authorization, mock_dynamodb_get_client, mock_sts_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if (TableName == 'fake_collection_table'):
                return io_util.load_data_json('dynamodb.collection_data_valid.json')
            elif (TableName == 'fake_objectstore_table'):
                return io_util.load_data_json('dynamodb.objectstore_data_valid.json')
            elif TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamodb.changesetTable.getItem.json')
            else:
                return None


        mock_owner_authorization.return_value = {}

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        mock_sts_client.return_value.assume_role.return_value = {
            "Credentials": {
                "AccessKeyId": "AccessKeyIdVal",
                "SecretAccessKey": "SecretAccessKeyVal",
                "SessionToken": "SessionTokenVal",
                "Expiration": datetime.datetime(2018, 8, 23, 00, 33, 27)}

        }

        # The mock-outs
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_util.load_data_json('cb_query_items.json'),
                           io_util.load_data_json('tracking_query_items.json')]

        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')



        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_valid.json')

        with patch('create_folder_upload_command.staging_bucket_name', 'bucket_name'):
            with patch('create_folder_upload_command.staging_bucket_arn', 'bucket_arn'):
                with patch('create_folder_upload_command.assume_role_arn', 'test_ARN'):
                    with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                        self.assertEqual(create_folder_upload_command.lambda_handler(request_input,
                                                                                     mock_lambda_context.MockLambdaContext()),
                                         expected_response)


if __name__ == '__main__':
    unittest.main()
