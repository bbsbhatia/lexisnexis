import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_changeset_command

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateChangesetCommand')
table_dict = {
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table'
}


@patch.dict(os.environ, table_dict)
class TestCreateChangesetCommand(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.maxDiff = None
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # Successful test using lambda_handler with command_wrapper decorator
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_create_changeset_command_success(self, dynamodb_client_mock):
        dynamodb_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = \
            io_util.load_data_json('ownerTable.query.item.response.json')

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(
                create_changeset_command.lambda_handler(request_input, MockLambdaContext()),
                response_output)
        dynamodb_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result.assert_called_once()


if __name__ == '__main__':
    unittest.main()
