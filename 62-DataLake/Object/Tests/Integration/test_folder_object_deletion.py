import os
import unittest
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import folder_object_deletion_lambda

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "FolderObjectDeletion")


class FolderObjectDeletionTest(TestCase):

    # + test_folder_object_deletion: Valid
    @patch.dict(os.environ, {'FOLDER_DELETE_SQS_URL': ""})
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_folder_object_deletion(self, mock_set_session, mock_s3_get_client, mock_sqs_get_client):
        mock_set_session.return_value = None
        mock_s3_get_client.return_value.get_paginator.return_value.paginate.return_value = \
            [io_utils.load_data_json('s3.valid_list.json')]
        mock_s3_get_client.return_value.delete_objects.return_value = None
        input_dict = io_utils.load_data_json('sqs_message_valid.json')
        self.assertEqual(event_handler_status.SUCCESS,
                         folder_object_deletion_lambda.lambda_handler(input_dict, MockLambdaContext))
        self.assertEqual(1, mock_s3_get_client.return_value.delete_objects.call_count)


if __name__ == '__main__':
    unittest.main()
