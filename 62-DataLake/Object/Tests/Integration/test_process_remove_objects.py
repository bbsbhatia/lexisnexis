import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import process_remove_objects

__author__ = "John Morelock, Shekhar Ralhan"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'ProcessRemoveObjects')


class TestProcessRemoveObjects(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATALAKE_STAGING_BUCKET_NAME': 'mock_staging_bucket',
                             'TRACKING_DYNAMODB_TABLE': 'mock_tracking_table',
                             'COLLECTION_BLOCKER_DYNAMODB_TABLE': 'mock_collection_blocker_table',
                             'EVENT_STORE_DYNAMODB_TABLE': 'mock_event_store_table',
                             'BATCH_SIZE': '10',
                             'WRITE_BATCH_SIZE': '2'
                             })
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        # reload the remove_object_event_handler so that when it is instantiated it gets my arn is picked up
        # from the os.environ
        reload(process_remove_objects)
        TableCache.clear()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        # reload the process_remove_objects so that when it is instantiated it gets unloads my values
        # from the os.environ
        reload(process_remove_objects)

    # + lambda_handler - happy path
    @patch.dict(os.environ, {
    })
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.s3.read_s3_object')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler(self,
                            mock_dynamodb_get_client,
                            mock_helper,
                            mock_read_s3_object,
                            mock_s3_get_client,
                            mock_sqs):
        mock_helper.return_value = ['us-east-1']
        mock_sqs.return_value.send_message.return_value = None

        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_utils.load_data_json('collection_blocker_query.json')]
        mock_dynamodb_get_client.return_value.delete_item.return_value \
            = io_utils.load_data_json('collection_blocker_delete.json')

        request_input = io_utils.load_data_json('valid_sqs_event.json')
        mock_read_s3_object.return_value = io_utils.load_txt('valid_s3_batch.json')
        resp = process_remove_objects.lambda_handler(request_input, MockLambdaContext())
        self.assertEqual(resp, event_handler_status.SUCCESS)

    # + lambda_handler - happy path
    @patch.dict(os.environ, {
    })
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.s3.read_s3_object')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler(self,
                            mock_dynamodb_get_client,
                            mock_helper,
                            mock_read_s3_object,
                            mock_s3_get_client,
                            mock_sqs):
        mock_helper.return_value = ['us-east-1']
        mock_sqs.return_value.send_message.return_value = None

        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_utils.load_data_json('collection_blocker_query.json')]
        mock_dynamodb_get_client.return_value.delete_item.return_value \
            = io_utils.load_data_json('collection_blocker_delete.json')

        request_input = io_utils.load_data_json('valid_sqs_event.json')
        mock_read_s3_object.return_value = io_utils.load_txt('valid_s3_batch.json')
        resp = process_remove_objects.lambda_handler(request_input, MockLambdaContext())
        self.assertEqual(resp, event_handler_status.SUCCESS)


if __name__ == '__main__':
    unittest.main()
