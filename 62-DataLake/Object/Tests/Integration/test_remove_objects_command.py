import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import remove_objects_command

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveObjectsCommand')


class TestRemoveObjectsCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATALAKE_STAGING_BUCKET_NAME': "bucket-name"})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(remove_objects_command)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # +lambda_handler - valid
    @patch.dict(os.environ, {'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'CHANGESET_DYNAMODB_TABLE': 'fake_changeset_table'})
    @patch('lng_aws_clients.s3.get_client')
    @patch("lng_aws_clients.dynamodb.get_client")
    def test_remove_object_command(self, mock_dynamodb_get_client, s3_put_object_mock):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamodb.collection_data_valid.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner_data_valid.json')
            elif TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamodb.changesetTable.getItem.json')

        s3_put_object_mock.return_value.put_object.return_value = io_util.load_data_json('s3.put_object.json')
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'put_item_valid_response.json')

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_valid.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(expected_response,
                             remove_objects_command.lambda_handler(request_input,
                                                                   mock_lambda_context.MockLambdaContext()))


if __name__ == '__main__':
    unittest.main()
