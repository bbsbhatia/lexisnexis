import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_ingestion_command import lambda_handler

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetIngestionCommand')


class TestGetIngestionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +lambda_handler - valid
    @patch.dict(os.environ, {'INGESTION_DYNAMODB_TABLE': 'fake_ingestion_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_integration_get_object_command(self, session_mock, dynamodb_get_client_mock):
        session_mock.return_value = None
        dynamodb_get_client_mock.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.ingestionTable.getItem.json')

        request_input = io_util.load_data_json('apigateway.request.json')
        expected_response = io_util.load_data_json('apigateway.response.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response, lambda_handler(request_input, MockLambdaContext()))


if __name__ == '__main__':
    unittest.main()
