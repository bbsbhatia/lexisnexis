import os
import unittest
from unittest.mock import patch

from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from process_large_object import lambda_handler

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ProcessLargeObject')

env_dict = {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
            'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'}


class TestProcessLargeObject(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        TableCache.clear()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        TableCache.clear()

    # + lambda_handler update event - Success
    @patch.dict(os.environ, env_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_update_event(self, dynamo_mock):
        dynamo_mock.return_value.get_item.return_value = io_util.load_data_json('object_store_get_item.json')
        dynamo_mock.return_value.put_item.return_value = io_util.load_data_json('dynamo_db_put_response.json')
        event = io_util.load_data_json('valid_update_sns_message.json')
        self.assertEqual('Success', lambda_handler(event, MockLambdaContext))

    # + lambda_handler update event duplicate key - Success
    @patch.dict(os.environ, env_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_update_event_fail(self, dynamo_mock):
        dynamo_mock.return_value.get_item.return_value = io_util.load_data_json('object_store_get_item.json')
        dynamo_mock.return_value.put_item.return_value = io_util.load_data_json('dynamo_db_put_response.json')
        event = io_util.load_data_json('valid_update_sns_message_dupe_key.json')
        self.assertEqual('Success', lambda_handler(event, MockLambdaContext))


if __name__ == '__main__':
    unittest.main()
