import unittest
from unittest import TestCase
from unittest.mock import patch
from importlib import reload
from lng_datalake_dal.table import TableCache

from botocore.exceptions import ClientError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_ingestion_event_handler

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CreateIngestionEventHandler")


class CreateIngestionEventHandlerTest(TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        TableCache.clear()
        reload(create_ingestion_event_handler)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()
        reload(create_ingestion_event_handler)

    @patch('lng_aws_clients.session.set_session')
    @patch('lng_datalake_dal.ingestion_table.IngestionTable.put_item')
    def test_create_ingestion_event_handler_success(self, mock_ingestion_put_item, aws_session_mock):
        aws_session_mock.return_value = None
        mock_ingestion_put_item.return_value = io_utils.load_data_json('dynamodb.put_item.json')
        event = io_utils.load_data_json('valid_cmd_event.json')
        self.assertEqual(create_ingestion_event_handler.lambda_handler(event, MockLambdaContext), event_handler_status.SUCCESS)


if __name__ == '__main__':
    unittest.main()
