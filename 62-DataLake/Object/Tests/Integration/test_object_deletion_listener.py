import gzip
import json
import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import object_deletion_listener_lambda

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "ObjectDeletionListener")


class ObjectDeletionListenerTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATALAKE_METADATA_BUCKET_NAME': "test-metadata-bucket"})
    def setUpClass(cls):
        reload(object_deletion_listener_lambda)

    # + test_object_deletion_listener lambda handler: Valid
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_object_deletion_listener(self, mock_set_session, mock_sqs_client, mock_s3_get_client):
        mock_set_session.return_value = None
        mock_sqs_client.return_value = None
        mock_s3_get_client.return_value.delete_object.return_value = None
        self.assertEqual(event_handler_status.SUCCESS, object_deletion_listener_lambda.lambda_handler(
            io_utils.load_data_json('dynamo_stream_multi_record_valid.json'), MockLambdaContext))

    # + test_object_deletion_listener_modify lambda handler: Valid
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_object_deletion_listener_modify(self, mock_set_session, mock_sqs_client, mock_s3_get_client):
        mock_set_session.return_value = None
        mock_sqs_client.return_value = None
        mock_s3_get_client.return_value.delete_object.return_value = None
        self.assertEqual(event_handler_status.SUCCESS, object_deletion_listener_lambda.lambda_handler(
            io_utils.load_data_json('dynamo_stream_multi_record_with_modify_valid.json'), MockLambdaContext))

    # + test_object_deletion_listener: Validate that retry events are
    # triggered and do not interfere with other records
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_object_deletion_listener_client_error(self, mock_set_session, mock_sqs_client, mock_s3_client):
        mock_set_session.return_value = None
        mock_sqs_client.return_value.send_message.return_value = None
        mock_s3_client.return_value.delete_object.side_effect = [{}, ClientError({'ResponseMetadata': {"RequestId": "Mock-321"},
                                                                                                            'Error': {
                                                                                                                'Code': 'OTHER',
                                                                                                                'Message': 'This is a mock'}},
                                                                                                           "FAKE")]
        self.assertEqual(event_handler_status.SUCCESS, object_deletion_listener_lambda.lambda_handler(
            io_utils.load_data_json('dynamo_stream_multi_record_valid_2.json'), MockLambdaContext))
        self.assertEqual(2, mock_sqs_client.return_value.send_message.call_count)


if __name__ == '__main__':
    unittest.main()
