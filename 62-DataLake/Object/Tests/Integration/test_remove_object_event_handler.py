import json
import os
import unittest
from importlib import reload
from unittest.mock import patch, call

from botocore.exceptions import ClientError
from lng_datalake_commons import publish_sns_topic
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import remove_object_event_handler

__author__ = "Chuck Nelson, Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveObjectEventHandler')

topic_arn = 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'
mock_queue_url = 'https://sqs.us-east-1.amazonaws.com/123456789012/SuperTestQueue'

table_patch = {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': topic_arn,
               'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
               'OBJECT_STORE_VERSION_DYNAMODB_TABLE': 'fake_objectstoreversion_table',
               'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
               'TRACKING_DYNAMODB_TABLE': 'fake_tracking_table',
               "CHANGESET_OBJECT_DYNAMODB_TABLE": "fake_changeset_object_table",
               'COLLECTION_BLOCKER_DYNAMODB_TABLE': 'fake_collection_blocker_table',
               'LAMBDA_TASK_ROOT': os.path.dirname(io_util.schema_path)}


class TestRemoveObjectEventHandler(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, table_patch)
    def setUpClass(cls):  # NOSONAR
        # reload the remove_object_event_handler so that when it is instantiated it gets my arn is picked up
        # from the os.environ
        reload(remove_object_event_handler)
        TableCache.clear()
        reload(publish_sns_topic)

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': ''})
    def tearDownClass(cls):  # NOSONAR
        # reload the remove_object_event_handler so that when it is instantiated it gets unloads my values
        # from the os.environ
        reload(remove_object_event_handler)

    # +lambda_handler - happy path - old-object-versions-to-keep = 0
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_old_objects_to_keep_0(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock,
                                                  mock_helper):
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json')]
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json('dynamodb.get_item.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')

        request_input = io_util.load_data_json("sns_event.json")
        self.assertEqual(remove_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

        # validate that one message for each schema version was published and one for the concatenation
        # in the following order: v0, v1, v0v1
        calls = []
        for file in ["notification_v0.json", "notification_v1.json", "notification_v0v1.json"]:
            notification = io_util.load_data_json(file)
            calls.append(call(TargetArn=topic_arn, Message=notification["message"], MessageStructure="json",
                              MessageAttributes=notification["message_attributes"]))
        mock_sns_get_client.return_value.publish.assert_has_calls(calls, any_order=False)

    # +lambda_handler - happy path - old-object-versions-to-keep = 4
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_old_objects_to_keep_4(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock,
                                                  mock_helper):
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json')]
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json('dynamodb.get_item.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')

        request_input = io_util.load_data_json("sns_event_4.json")
        self.assertEqual(remove_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_sns_get_client.return_value.publish.assert_called()

    # +lambda_handler - happy path - Object does not exist in table
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_no_object_item(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock,
                                           mock_helper):
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json')]
        mock_dynamodb_get_client.return_value.get_item.return_value = {}
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')

        request_input = io_util.load_data_json("sns_event_4.json")
        self.assertEqual(remove_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        # Verify no object table put - nothing to update in this case
        mock_dynamodb_get_client.return_value.put_item.assert_not_called()
        # Verify no SNS message sent - no object to notify on
        mock_sns_get_client.assert_not_called()

    # +lambda_handler - happy path - object state removed
    @patch.dict(os.environ, table_patch)
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_object_state_removed(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock,
                                                 mock_helper, mock_timestamp):
        mock_timestamp.return_value = "2019-04-10T12:00:00.000Z"
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json')]
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.get_item_removed.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')

        request_input = io_util.load_data_json("sns_event_5.json")
        self.assertEqual(remove_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        # Verify no object table put - object state is removed already but we do put the object in the
        # changeset object table
        mock_dynamodb_get_client.return_value.put_item.assert_called_once()
        mock_dynamodb_get_client.return_value.put_item.assert_called_with(
            Item=io_util.load_data_json('dynamodb.expected_changeset_object.json'),
            TableName='fake_changeset_object_table')
        # Verify no SNS message sent - message was sent previously
        mock_sns_get_client.assert_not_called()

    # +lambda_handler- sns-publish-only
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_sns_publish_only(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock,
                                             mock_helper):
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_util.load_data_json('tracking.query.json'),
                           io_util.load_data_json('dynamodb.mapping_table.respond.json')]
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')

        request_input = io_util.load_data_json("sns_event_sns_publish_only.json")
        self.assertEqual(remove_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_sns_get_client.return_value.publish.assert_called()

    # -lambda_handler - throttle ost
    @patch("lng_aws_clients.sqs.get_client")
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_throttle_ost(self, mock_dynamodb_get_client, aws_session_mock,
                                         mock_helper, sqs_client_mock):
        mock_helper.return_value = ['us-east-1']
        sqs_client_mock.return_value.send_message.return_value = io_util.load_data_json(
            'sqs.send_message.response.json')
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json')]
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json('dynamodb.get_item.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_dynamodb_get_client.return_value.put_item.side_effect \
            = ClientError(io_util.load_data_json('dynamodb.ClientError.json'), "PutItem")

        request_input = io_util.load_data_json("sns_event_4.json")

        existing_object_store_item = io_util.load_data_json('existing_object_store_item.json')

        expected_retry_message = io_util.load_data_json('sns_event_4.json')
        message_body = json.loads(expected_retry_message['Records'][0]['Sns']['Message'])
        message_body['additional-attributes'] = {"retry-state": 2, 'object-data': existing_object_store_item}
        expected_retry_message['Records'][0]['Sns']['Message'] = json.dumps(message_body)
        stringified_expected_retry_message = json.dumps(expected_retry_message).replace(' ', '')

        with patch('lng_datalake_commons.error_handling.error_handler.retry_queue_url',
                   mock_queue_url):
            self.assertIsNone(remove_object_event_handler.lambda_handler(request_input, MockLambdaContext()))
            sqs_client_mock.return_value.send_message.assert_called_with(
                MessageAttributes={"ThrottleType": {"StringValue": "DynamoDB", "DataType": "String"}},
                MessageBody=stringified_expected_retry_message, QueueUrl=mock_queue_url)

    # +lambda_handler - happy path - changeset request
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_old_objects_to_keep_0_changeset(self, mock_sns_get_client, mock_dynamodb_get_client,
                                                            aws_session_mock,
                                                            mock_helper):
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json')]
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json('dynamodb.get_item.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')

        request_input = io_util.load_data_json("sns_event_changeset.json")
        self.assertEqual(remove_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

        # validate that one message for each schema version was published and one for the concatenation
        # in the following order: v0, v1, v0v1
        # changeset information will not appear in v0
        calls = []
        for file in ["notification_changeset_v0.json", "notification_changeset_v1.json",
                     "notification_changeset_v0v1.json"]:
            notification = io_util.load_data_json(file)
            calls.append(call(TargetArn=topic_arn, Message=notification["message"], MessageStructure="json",
                              MessageAttributes=notification["message_attributes"]))
        mock_sns_get_client.return_value.publish.assert_has_calls(calls, any_order=False)

        # we require 3 puts here because 1 is for the OST, 1 for the OSVT, and 1 for the COT (changeset object table)
        self.assertEqual(mock_dynamodb_get_client.return_value.put_item.call_count, 3)


if __name__ == '__main__':
    unittest.main()
