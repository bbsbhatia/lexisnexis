import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_ingestion_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateIngestionCommand')
table_dict = {'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
              'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
              'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_store_table',
              "CHANGESET_DYNAMODB_TABLE": 'fake_changeset_table'
              }


@patch.dict(os.environ, table_dict)
class TestCreateObjectCommand(unittest.TestCase):
    @classmethod
    @patch.dict(os.environ, {'DATALAKE_STAGING_BUCKET_NAME': "bucket-name"})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(create_ingestion_command)
        TableCache.clear()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # Successful test using lambda_handler with command_wrapper decorator
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_ingestion_multipart_upload_command_success(self, session_mock, dynamodb_client_mock,
                                                               s3_client_mock):

        session_mock.return_value = None

        def mock_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':  # collection table: get item
                return io_util.load_data_json('dynamodb.collectionTable.getItem.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.ownerTable.getItem.json')
            elif TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamodb.changesetTable.getItem.json')
            else:
                return None

        dynamodb_client_mock.return_value.get_item = mock_get_item
        dynamodb_client_mock.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')

        s3_client_mock.return_value.create_multipart_upload.return_value = io_util.load_data_json(
            'multipart_resp.json')

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(
                create_ingestion_command.lambda_handler(request_input, MockLambdaContext()),
                response_output)


if __name__ == '__main__':
    unittest.main()
