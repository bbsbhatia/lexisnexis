import os
import unittest
from unittest.mock import patch

from lng_datalake_testhelper import mock_lambda_context

from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_commands import command_wrapper
from remove_object_command import lambda_handler

__author__ = "Chuck Nelson"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveObjectCommand')


class TestRemoveObjectCommand(unittest.TestCase):

    # +lambda_handler - valid
    @patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'CHANGESET_DYNAMODB_TABLE': "fake_changeset_table"})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_remove_object_command(self, session_mock, mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamodb.collection_data_valid.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner_data_valid.json')
            elif TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamodb.changesetTable.getItem.json')

        session_mock.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.\
            side_effect = [io_util.load_data_json('dynamodb.collectionBlockerTable.paginate.json')]
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('put_item_valid_response.json')

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_valid.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()), expected_response)


if __name__ == '__main__':
    unittest.main()
