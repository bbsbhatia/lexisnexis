import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import service_commons.object_common as object_common_module
from update_object_command import lambda_handler

__author__ = "Chuck Nelson"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateObjectCommand')


class TestUpdateObjectCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        reload(object_common_module)
        TableCache.clear()

    # +lambda_handler - valid
    @patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_table',
                             'CHANGESET_DYNAMODB_TABLE': 'fake_changeset_table'})
    @patch("lng_aws_clients.s3.get_client")
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_update_object_command(self, session_mock, mock_dynamodb_get_client, mock_s3_get_client):

        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_util.load_data_json('cb_query_items.json'),
                           io_util.load_data_json('tracking_query_items.json')]

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamodb.collection_data_valid.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner_data_valid.json')
            elif TableName == 'fake_objectstore_table':
                return io_util.load_data_json('dynamodb.objectstore_data_valid.json')
            elif TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamodb.changeset_data_valid.json')
            else:
                return None

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        # The mock-outs
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_s3_get_client.return_value.put_object.return_value = io_util.load_data_json('s3.putobject_data_valid.json')

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_valid.json')

        with patch('lng_datalake_commons.time_helper.get_current_timestamp') as mock_timestamp:
            mock_timestamp.return_value = "2019-04-10T12:00:00.000Z"
            with patch('update_object_command.staging_bucket_name', 'ccs-sandbox-datalake-staging-object-store'):
                with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                    self.assertEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                                     expected_response)
            mock_dynamodb_get_client.return_value.put_item.assert_called_with(
                ConditionExpression='attribute_not_exists(EventID) and attribute_not_exists(RequestTime)',
                Item=io_util.load_data_json('expected_event_store_put_without_expiration_epoch.json'),
                TableName='fake_event_table')

    # +lambda_handler - valid
    @patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_table'})
    @patch("lng_aws_clients.s3.get_client")
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_update_object_command_large_update(self, session_mock, mock_dynamodb_get_client, mock_s3_get_client):

        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_util.load_data_json('cb_query_items.json'),
                           io_util.load_data_json('tracking_query_items.json')]

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamodb.collection_data_valid.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner_data_valid.json')
            elif TableName == 'fake_objectstore_table':
                return io_util.load_data_json('dynamodb.objectstore_data_valid.json')
            else:
                return None

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        # The mock-outs
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_s3_get_client.return_value.generate_presigned_url.return_value = "https://mock_presigned_url"

        request_input = io_util.load_data_json('apigateway.request.accepted_largeobject_1.json')
        expected_response = io_util.load_data_json('apigateway.response.accepted_largeobject_1.json')

        with patch('update_object_command.staging_bucket_name', 'ccs-sandbox-datalake-staging-object-store'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                self.assertEqual(lambda_handler(request_input, mock_lambda_context.MockLambdaContext()),
                                 expected_response)


if __name__ == '__main__':
    unittest.main()
