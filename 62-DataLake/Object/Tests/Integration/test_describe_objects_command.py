import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from describe_objects_command import lambda_handler

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'DescribeObjectsCommand')


@patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_object_store_table',
                         'OBJECT_STORE_VERSION_DYNAMODB_TABLE': 'fake_object_store_version_table',
                         'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                         "CHANGESET_DYNAMODB_TABLE": 'fake_changeset_table',
                         'CHANGESET_OBJECT_DYNAMODB_TABLE': 'fake_changeset_object_table'})
class IntegrationTestDescribeObjectsCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': "https://datalake_url.com"})
    def setUpClass(cls):  # NOSONAR
        TableCache.clear()
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(object_common_module)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # +Successful describe object by version, object-id and collection-id
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_describe_objects_command_1(self, dynamodb_get_client_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.return_value = \
            io_util.load_data_json('dynamodb.collectionTable.getItem_1.json')

        # object store and object store version tables query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_util.load_data_json('dynamodb.objectStoreTable.paginate_1.json'),
                           io_util.load_data_json('dynamodb.objectStoreVersionTable.paginate.json')]

        request_input = io_util.load_data_json('apigateway.request_1.json')
        response_output = io_util.load_data_json('apigateway.response_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(response_output, lambda_handler(request_input, MockLambdaContext()))

    # +Successful describe object by object-id
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_describe_objects_command_2(self, dynamodb_get_client_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.side_effect = \
            [io_util.load_data_json('dynamodb.collectionTable.getItem_1.json'),
             io_util.load_data_json('dynamodb.collectionTable.getItem_2.json')]

        # object store and object store version tables query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.objectStoreTable.paginate_2.json')

        request_input = io_util.load_data_json('apigateway.request_2.json')
        response_output = io_util.load_data_json('apigateway.response_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(response_output, lambda_handler(request_input, MockLambdaContext()))

    # +Successful describe object by collection-id
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_describe_objects_command_3(self, dynamodb_get_client_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.return_value = \
            io_util.load_data_json('dynamodb.collectionTable.getItem_1.json')

        # object store and object store version tables query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.objectStoreTable.paginate_1.json')

        request_input = io_util.load_data_json('apigateway.request_3.json')
        response_output = io_util.load_data_json('apigateway.response_3.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(response_output, lambda_handler(request_input, MockLambdaContext()))

    # Successful describe object with changeset-id, collection-id, ojbect-id and version-number
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_describe_objects_command_4(self, dynamodb_get_client_mock):
        def mock_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_changeset_table':  # changeset table: get item
                return io_util.load_data_json('dynamodb.changesetTable.getItem_1.json')
            elif TableName == 'fake_changeset_object_table':
                return io_util.load_data_json('dynamodb.changesetObjectTable.getItem_1.json')
            elif TableName == 'fake_collection_table':  # collection: get item
                return io_util.load_data_json('dynamodb.collectionTable.getItem_3.json')
            else:
                return None

        dynamodb_get_client_mock.return_value.get_item = mock_get_item
        request_input = io_util.load_data_json('apigateway.request_4.json')
        response_output = io_util.load_data_json('apigateway.response_4.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(response_output, lambda_handler(request_input, MockLambdaContext()))

    # Successful describe object with changeset-id, collection-id
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_describe_objects_command_5(self, dynamodb_get_client_mock):

        def mock_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_changeset_table':  # changeset table: get item
                return io_util.load_data_json('dynamodb.changesetTable.getItem_1.json')
            elif TableName == 'fake_collection_table':  # collection: get item
                return io_util.load_data_json('dynamodb.collectionTable.getItem_3.json')
            else:
                return None

        dynamodb_get_client_mock.return_value.get_item = mock_get_item
        # changeset object table query_items
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.changesetObjectTable.queryitems_1.json')

        request_input = io_util.load_data_json('apigateway.request_5.json')
        response_output = io_util.load_data_json('apigateway.response_4.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(response_output, lambda_handler(request_input, MockLambdaContext()))

    # Successful describe object with changeset-id
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_describe_objects_command_6(self, dynamodb_get_client_mock):

        def mock_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_changeset_table':  # changeset table: get item
                return io_util.load_data_json('dynamodb.changesetTable.getItem_1.json')
            elif TableName == 'fake_collection_table':  # collection: get item
                return io_util.load_data_json('dynamodb.collectionTable.getItem_3.json')
            else:
                return None

        dynamodb_get_client_mock.return_value.get_item = mock_get_item

        # changeset object table query_items
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.changesetObjectTable.queryitems_1.json')

        request_input = io_util.load_data_json('apigateway.request_6.json')
        response_output = io_util.load_data_json('apigateway.response_4.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(response_output, lambda_handler(request_input, MockLambdaContext()))


if __name__ == '__main__':
    unittest.main()
