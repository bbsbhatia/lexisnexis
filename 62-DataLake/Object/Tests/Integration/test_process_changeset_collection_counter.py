import os
import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import process_changeset_collection_counter

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "ProcessChangesetCollectionCounter")


class TestProcessChangesetCollectionCounter(TestCase):

    @classmethod
    @patch.dict(os.environ, {'CHANGESET_COLLECTION_DYNAMODB_TABLE': 'fake_table',
                             'RETRY_QUEUE_URL': 'fake_url'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + lambda handler: dyanmo stream - update item
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_1(self, mock_dynamo_client):
        input = io_utils.load_data_json('dynamo_stream_event.json')
        mock_dynamo_client.return_value.update_item.return_value = io_utils.load_data_json('dynamodb.update_item.json')
        self.assertEqual(event_handler_status.SUCCESS, process_changeset_collection_counter.lambda_handler(
            input, MockLambdaContext))

    # + lambda handler: sqs - update item
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_3(self, mock_dynamo_client):
        input = io_utils.load_data_json('sqs_event.json')
        mock_dynamo_client.return_value.update_item.return_value = io_utils.load_data_json('dynamodb.update_item.json')
        self.assertEqual(event_handler_status.SUCCESS, process_changeset_collection_counter.lambda_handler(
            input, MockLambdaContext))

    # + lambda handler: dyanmo stream - send to retry queue
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_5(self, mock_dynamo_client, mock_sqs_client):
        input = io_utils.load_data_json('dynamo_stream_event.json')
        throttle_error = io_utils.load_data_json('error.throttle.json')
        mock_dynamo_client.return_value.update_item.side_effect = ClientError(throttle_error, 'UpdateItem')
        mock_sqs_client.return_value.send_message.return_value = None
        self.assertEqual(event_handler_status.SUCCESS, process_changeset_collection_counter.lambda_handler(
            input, MockLambdaContext))


if __name__ == '__main__':
    unittest.main()
