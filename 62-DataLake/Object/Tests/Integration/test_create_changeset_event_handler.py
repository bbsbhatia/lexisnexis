import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_commons import publish_sns_topic
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_changeset_event_handler

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CreateChangesetEventHandler")

topic_arn = 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'


class CreateChangesetEventHandlerTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': topic_arn,
                             'LAMBDA_TASK_ROOT': os.path.dirname(io_utils.schema_path)})
    def setUpClass(cls):  # NOSONAR
        TableCache.clear()
        reload(create_changeset_event_handler)
        reload(publish_sns_topic)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()
        reload(create_changeset_event_handler)

    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.put_item')
    def test_create_changeset_event_handler_success(self, mock_changeset_put_item, aws_session_mock,
                                                    mock_sns_publish, mock_helper):
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_sns_publish.return_value.publish.return_value = io_utils.load_data_json('sns.publish.json')
        mock_changeset_put_item.return_value = io_utils.load_data_json('dynamodb.put_item.json')
        event = io_utils.load_data_json('valid_cmd_event.json')
        self.assertEqual(create_changeset_event_handler.lambda_handler(event, MockLambdaContext), event_handler_status.SUCCESS)


if __name__ == '__main__':
    unittest.main()
