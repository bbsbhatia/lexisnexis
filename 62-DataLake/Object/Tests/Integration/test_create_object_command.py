import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from create_object_command import lambda_handler

__author__ = "Jason Feng"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateObjectCommand')
table_dict = {'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
              'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
              'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
              "CHANGESET_DYNAMODB_TABLE": 'fake_changeset_table'
              }


@patch.dict(os.environ, table_dict)
class IntegrationTestCreateObjectCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        TableCache.clear()
        cls.maxDiff = None
        reload(object_common_module)

    # + successful lambda_handler test for small object - with object id
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_integration_create_object_command_with_obj_id(self, session_mock, dynamodb_get_client_mock,
                                                           s3_put_object_mock):
        session_mock.return_value = None
        # object store table: query items by pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_util.load_data_json('cb_query_items.json'),
                           io_util.load_data_json('tracking_query_items.json'),
                           io_util.load_data_json('dynamodb.objectStoreTable.paginate.json')]

        def mock_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':  # collection table: get item
                return io_util.load_data_json('dynamodb.collectionTable.getItem.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.ownerTable.getItem.json')
            elif TableName == 'fake_objectstore_table':  # object store table: check exist with get empty item
                return io_util.load_data_json('dynamodb.objectStoreTable.getItem.empty.json')
            elif TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamodb.changesetTable.getItem.json')
            else:
                return None

        dynamodb_get_client_mock.return_value.get_item = mock_get_item
        dynamodb_get_client_mock.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')

        # s3: put object
        s3_put_object_mock.return_value.put_object.return_value = io_util.load_data_json('s3.put_object.json')
        request_input_with_object_id = io_util.load_data_json('apigateway.request.object-id.json')
        response_output_with_object_id = io_util.load_data_json('apigateway.response.object-id.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with patch('create_object_command.staging_bucket_name', 'staging_bucket_name'):
                self.assertEqual(response_output_with_object_id,
                                 lambda_handler(request_input_with_object_id, MockLambdaContext()))

    # + successful lambda_handler test for small object - no object id
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_integration_create_object_command_no_obj_id(self, session_mock, dynamodb_get_client_mock,
                                                         s3_put_object_mock):
        session_mock.return_value = None
        # object store table: query items by pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_util.load_data_json('cb_query_items.json'),
                           io_util.load_data_json('tracking_query_items.json'),
                           io_util.load_data_json('dynamodb.objectStoreTable.paginate.json')]

        def mock_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':  # collection table: get item
                return io_util.load_data_json('dynamodb.collectionTable.getItem.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.ownerTable.getItem.json')
            elif TableName == 'fake_objectstore_table':  # object store table: check exist with get empty item
                return io_util.load_data_json('dynamodb.objectStoreTable.getItem.empty.json')
            else:
                return None

        dynamodb_get_client_mock.return_value.get_item = mock_get_item
        dynamodb_get_client_mock.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')

        # s3: put object
        s3_put_object_mock.return_value.put_object.return_value = io_util.load_data_json('s3.put_object.json')
        request_input = io_util.load_data_json('apigateway.request.json')
        response_output = io_util.load_data_json('apigateway.response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with patch('create_object_command.staging_bucket_name', 'staging_bucket_name'):
                self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                     response_output)


if __name__ == '__main__':
    unittest.main()
