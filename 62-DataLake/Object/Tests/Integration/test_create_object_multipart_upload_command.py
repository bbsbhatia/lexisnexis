import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
import create_object_multipart_upload_command

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateObjectMultipartUploadCommand')

table_dict = {'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
              'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_table',
              'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
              'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
              'TRACKING_DYNAMODB_TABLE': 'fake_tracking_table',
              "CHANGESET_DYNAMODB_TABLE": 'fake_changeset_table'
              }


@patch.dict(os.environ, table_dict)
class TestCreateObjectMultipartUploadCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'DATALAKE_STAGING_BUCKET_NAME': "bucket-name"})
    @patch.dict(os.environ, {'DATA_LAKE_URL': 'https://datalake_url.com'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(create_object_multipart_upload_command)
        reload(object_common_module)
        TableCache.clear()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # Successful test using lambda_handler with command_wrapper decorator
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_create_object_multipart_upload_command_success(self, dynamodb_get_client_mock, s3_client_mock):

        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            side_effect = [io_util.load_data_json('cb_query_items.json'),
                           io_util.load_data_json('tracking_query_items.json'),
                           io_util.load_data_json('dynamodb.objectStoreTable.paginate.json')]

        def mock_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':  # collection table: get item
                return io_util.load_data_json('dynamodb.collectionTable.getItem.json')
            elif TableName == 'fake_objectstore_table':  # object store table: check exist with get empty item
                return io_util.load_data_json('dynamodb.objectStoreTable.getItem.empty.json')
            elif TableName in ('fake_event_table', 'fake_tracking_table'):
                return io_util.load_data_json('dynamodb.put_item.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner_data_valid.json')
            elif TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamodb.changesetTable.getItem.json')
            else:
                return None

        dynamodb_get_client_mock.return_value.get_item = mock_get_item

        s3_client_mock.return_value.create_multipart_upload.return_value = io_util.load_data_json(
            'multipart_resp.json')

        request_input = io_util.load_data_json('apigateway.request.accepted_1.json')
        response_output = io_util.load_data_json('apigateway.response.accepted_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(create_object_multipart_upload_command.lambda_handler(request_input, MockLambdaContext()),
                             response_output)


if __name__ == '__main__':
    unittest.main()
