import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache

from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
import orjson

import glue_dispatcher

__author__ = "Shekhar Ralhan, John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "GlueDispatcher")
job_name = 'mock_job_name'
table_dict = {
    'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
    "TRACKING_DYNAMODB_TABLE": 'fake_tracking_table'
}


@patch.dict(os.environ, table_dict)
class GlueDispatcherTest(TestCase):

    @classmethod
    @patch.dict(os.environ,
                {'GLUE_DISPATCHER_TOPIC_ARN': 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic',
                 'DATALAKE_BUCKET_NAME': 'test_bucket_name'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(glue_dispatcher)
        TableCache.clear()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()
        TableCache.clear()

    # + tests glue_dispatcher.lambda_handler
    @patch("lng_aws_clients.glue.get_client")
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_datalake_commons.publish_sns_topic.publish_to_topic')
    def test_lambda_handler(self, sns_publish_mock, mock_s3, mock_dynamodb_client, mock_glue):
        valid_event = io_utils.load_data_json('valid_s3_event.json')
        sns_publish_mock.return_value.publish.return_value = io_utils.load_data_json('sns.publish.json')
        mock_s3.return_value.head_object.return_value = io_utils.load_data_json('s3.head_object.json')

        def mock_get_item(TableName, Key, ConsistentRead=False):  # NOSONAR
            if TableName == 'fake_tracking_table':  # tracking table: get item
                return io_utils.load_data_json('dynamodb.trackingTable.getItem.json')
            elif TableName == 'fake_objectstore_table':  # object store table: check exist with get empty item
                return io_utils.load_data_json('dynamodb.objectStoreTable.getItem.empty.json')
            else:
                return None

        mock_dynamodb_client.return_value.get_item = mock_get_item
        mock_dynamodb_client.return_value.put_item.return_value = io_utils.load_data_json('dynamodb.put_item.json')
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value \
            = io_utils.load_data_json('dynamodb.tracking.query_items.json')
        with patch('glue_dispatcher.GLUE_JOB_NAME', job_name):
            self.assertEqual(glue_dispatcher.lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)
            mock_glue.return_value.start_job_run.assert_called_with(
                Arguments={'--event_message': orjson.dumps(io_utils.load_data_json('glue_sample_args.json')).decode()},
                JobName=job_name)


if __name__ == '__main__':
    unittest.main()
