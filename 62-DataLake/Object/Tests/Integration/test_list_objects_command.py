import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
from list_objects_command import lambda_handler

__author__ = "Jason Feng, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListObjectsCommand')


class IntegrationTestListObjectsCommand(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'DATA_LAKE_URL': "https://datalake_url.com"})
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        reload(object_common_module)
        cls.maxDiff = None

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_list_objects_command_1(self, dynamodb_get_client_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.return_value = \
            io_util.load_data_json('dynamodb.collectionTable.getItem.json')
        # object store table query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.objectStoreTable.paginate_1.json')
        request_input = io_util.load_data_json('apigateway.request_1.json')
        response_output = io_util.load_data_json('apigateway.response_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)

    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_list_objects_command_2(self, dynamodb_get_client_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.return_value = \
            io_util.load_data_json('dynamodb.collectionTable.getItem.json')
        # object store table query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.objectStoreTable.paginate_2.json')
        request_input = io_util.load_data_json('apigateway.request_2.json')
        response_output = io_util.load_data_json('apigateway.response_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)

    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_list_objects_command_4(self, dynamodb_get_client_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.return_value = \
            io_util.load_data_json('dynamodb.collectionTable.getItem.json')
        # object store table query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.objectStoreTable.paginate_4.json')
        request_input = io_util.load_data_json('apigateway.request_4.json')
        response_output = io_util.load_data_json('apigateway.response_4.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)

    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_list_objects_command_5(self, dynamodb_get_client_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.return_value = \
            io_util.load_data_json('dynamodb.collectionTable.getItem.json')
        # object store table query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.objectStoreTable.paginate_5.json')
        request_input = io_util.load_data_json('apigateway.request_5.json')
        response_output = io_util.load_data_json('apigateway.response_5.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)

    @patch('lng_aws_clients.dynamodb.get_client')
    def test_integration_list_objects_command_7(self, dynamodb_get_client_mock):
        # collection table get item
        dynamodb_get_client_mock.return_value.get_item.return_value = \
            io_util.load_data_json('dynamodb.collectionTable.getItem.json')
        # object store table query item using pagination
        dynamodb_get_client_mock.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.objectStoreTable.paginate_7.json')
        request_input = io_util.load_data_json('apigateway.request_7.json')
        response_output = io_util.load_data_json('apigateway.response_7.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            print(response_output)
            print(lambda_handler(request_input, MockLambdaContext()))
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()),
                                 response_output)


if __name__ == '__main__':
    unittest.main()
