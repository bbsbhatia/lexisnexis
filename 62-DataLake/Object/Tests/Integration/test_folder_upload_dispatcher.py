import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import folder_upload_dispatcher

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "FolderUploadDispatcher")


class FolderUploadDispatcherTest(TestCase):

    @classmethod
    @patch.dict(os.environ,
                {'GLUE_DISPATCHER_TOPIC_ARN': 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic',
                 'DATALAKE_BUCKET_NAME': 'test_bucket_name'})
    def setUpClass(cls):  # NOSONAR
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(folder_upload_dispatcher)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session_patch.stop()

    # + tests glue_dispatcher.lambda_handler
    @patch("lng_aws_clients.glue.get_client_region")
    @patch("lng_aws_clients.glue.get_client")
    @patch('lng_datalake_commons.publish_sns_topic.publish_to_topic')
    def test_lambda_handler(self, sns_publish_mock, mock_glue, mock_glue_region):
        valid_event = io_utils.load_data_json('valid_event.json')
        sns_publish_mock.return_value.publish.return_value = io_utils.load_data_json('sns.publish.json')
        self.assertEqual(folder_upload_dispatcher.lambda_handler(valid_event, MockLambdaContext), 'Success')


if __name__ == '__main__':
    unittest.main()
