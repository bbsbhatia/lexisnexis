import json
import os
import unittest
from importlib import reload
from unittest.mock import patch, call

from botocore.exceptions import ClientError
from lng_datalake_commons import publish_sns_topic
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import service_commons.object_common as object_common_module
import update_object_event_handler

__author__ = "Chuck Nelson"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateObjectEventHandler')

topic_arn = 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'
mock_queue_url = 'https://sqs.us-east-1.amazonaws.com/123456789012/SuperTestQueue'


@patch.dict(os.environ, {'OBJECT_STORE_DYNAMODB_TABLE': 'fake_objectstore_table',
                         'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                         'TRACKING_DYNAMODB_TABLE': 'fake_tracking_table',
                         'COLLECTION_BLOCKER_DYNAMODB_TABLE': 'fake_collection_blocker_table',
                         "CHANGESET_OBJECT_DYNAMODB_TABLE": 'fake_changeset_object_table'})
class TestUpdateObjectEventHandler(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {
        'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': topic_arn,
        'DATALAKE_BUCKET_NAME': 'mock_datalake_bucket',
        'DATA_LAKE_URL': "https://datalake_url.com",
        'LAMBDA_TASK_ROOT': os.path.dirname(io_util.schema_path)
    })
    def setUpClass(cls):  # NOSONAR
        # reload the update_object_event_handler so that when it is instantiated it gets my arn is picked up
        # from the os.environ
        reload(update_object_event_handler)
        reload(object_common_module)
        reload(publish_sns_topic)

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': '',
                             'DATALAKE_BUCKET_NAME': ''})
    def tearDownClass(cls):  # NOSONAR
        # reload the update_object_event_handler so that when it is instantiated it gets unloads my values
        # from the os.environ
        reload(update_object_event_handler)

    # +lambda_handler- happy path
    @patch('lng_aws_clients.sqs.get_client')
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock, mock_helper,
                            mock_s3, mock_sqs):
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.ost.get_item.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('tracking.collectionblocker.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json'),
                                             io_util.load_data_json('dynamodb.osvt.query.json')]
        mock_s3.return_value.copy.return_value = None
        mock_sqs.return_value.send_message.return_value = None
        mock_s3.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                    'Error': {
                                                                        'Code': '404',
                                                                        'Message': 'Not Found'}},
                                                                   "FAKE")
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')
        mock_dynamodb_get_client.return_value.update_item.return_value = io_util.load_data_json(
            'dynamodb.ingestion.update_item.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')
        request_input = io_util.load_data_json("sns_event.json")
        self.assertEqual(update_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

        # validate that one message for each schema version was published and one for the concatenation
        # in the following order: v0, v1, v0v1
        calls = []
        for file in ["notification_v0.json", "notification_v1.json", "notification_v0v1.json"]:
            notification = io_util.load_data_json(file)
            calls.append(call(TargetArn=topic_arn, Message=notification["message"], MessageStructure="json",
                              MessageAttributes=notification["message_attributes"]))
        mock_sns_get_client.return_value.publish.assert_has_calls(calls, any_order=False)

    # +test_lambda_handler_2_versions_kept - happy path
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_2_versions_kept(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock,
                                            mock_helper, mock_s3):
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.ost.get_item.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('tracking.collectionblocker.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json'),
                                             io_util.load_data_json('dynamodb.osvt.query.json')]
        mock_s3.return_value.copy.return_value = None
        mock_s3.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                    'Error': {
                                                                        'Code': '404',
                                                                        'Message': 'Not Found'}},
                                                                   "FAKE")
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')
        request_input = io_util.load_data_json("sns_event_2_versions_kept.json")
        self.assertEqual(update_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_sns_get_client.return_value.publish.assert_called()

    # +lambda_handler- happy path
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.s3.get_client')
    def test_lambda_handler_large_object(self, mock_s3_get_client, mock_sns_get_client, mock_dynamodb_get_client,
                                         aws_session_mock, mock_helper):
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.ost.get_item.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('tracking.collectionblocker.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json'),
                                             io_util.load_data_json('dynamodb.osvt.query.json')]

        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None

        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')
        request_input = io_util.load_data_json("sns_event_large_object.json")
        self.assertEqual(update_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_sns_get_client.return_value.publish.assert_called()

    # +test_lambda_handler: sns publish only
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_sns_publish_only(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock,
                                             mock_helper):
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('tracking.collectionblocker.query.json')]
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')
        request_input = io_util.load_data_json("sns_event_publish_only.json")
        self.assertEqual(update_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_sns_get_client.return_value.publish.assert_called()
        mock_dynamodb_get_client.return_value.put_item.assert_called()

    # - test_lambda_handler_- write retry message
    @patch("lng_aws_clients.sqs.get_client")
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_lambda_handler_throttle_ost(self, mock_dynamodb_get_client, aws_session_mock,
                                         mock_helper, mock_s3, sqs_client_mock):
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.ost.get_item.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('tracking.collectionblocker.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json'),
                                             io_util.load_data_json('dynamodb.osvt.query.json')]
        mock_s3.return_value.copy.return_value = None
        mock_s3.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                    'Error': {
                                                                        'Code': '404',
                                                                        'Message': 'Not Found'}},
                                                                   "FAKE")
        sqs_client_mock.return_value.send_message.return_value = io_util.load_data_json(
            'sqs.send_message.response.json')
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.put_item.side_effect \
            = ClientError(io_util.load_data_json('dynamodb.ClientError.json'), "PutItem")
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        request_input = io_util.load_data_json("sns_event_2_versions_kept.json")

        existing_object_store_item = io_util.load_data_json('existing_object_store_item.json')
        expected_retry_message = io_util.load_data_json('sns_event_2_versions_kept.json')
        message_body = json.loads(expected_retry_message['Records'][0]['Sns']['Message'])
        message_body['additional-attributes'] = {"retry-state": 3, 'object-data': existing_object_store_item}
        expected_retry_message['Records'][0]['Sns']['Message'] = json.dumps(message_body)
        stringified_expected_retry_message = json.dumps(expected_retry_message).replace(' ', '')

        with patch('lng_datalake_commons.error_handling.error_handler.retry_queue_url',
                   mock_queue_url):
            self.assertIsNone(update_object_event_handler.lambda_handler(request_input, MockLambdaContext()))
            sqs_client_mock.return_value.send_message.assert_called_with(
                MessageAttributes={"ThrottleType": {"StringValue": "DynamoDB", "DataType": "String"}},
                MessageBody=stringified_expected_retry_message, QueueUrl=mock_queue_url)

    # +test_lambda_handler: duplicate content with changeset
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    @patch("lng_aws_clients.s3.get_client")
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_duplicate_content(self, mock_sns_get_client, mock_dynamodb_get_client, aws_session_mock,
                                              mock_helper, mock_s3, mock_timestamp):
        mock_timestamp.return_value = "2019-12-06T04:31:58.818Z"
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.ost.get_item.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('tracking.collectionblocker.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json')]
        mock_s3.return_value.copy.return_value = None
        mock_s3.return_value.delete_object_tagging.return_value = None
        mock_s3.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                    'Error': {
                                                                        'Code': '404',
                                                                        'Message': 'Not Found'}},
                                                                   "FAKE")
        mock_helper.return_value = ['us-east-1']
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json('dynamodb.put_item.json')
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')
        request_input = io_util.load_data_json("sns_event_duplicate_content_with_changeset.json")
        self.assertEqual(update_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_sns_get_client.return_value.publish.assert_called()
        expected_changeset_object = io_util.load_data_json('dynamodb.changeset_object_item.json')
        mock_dynamodb_get_client.return_value.put_item.assert_called_with(Item=expected_changeset_object,
                                                                          TableName='fake_changeset_object_table')
        self.assertEqual(mock_s3.return_value.copy.call_count, 2)


    # +test lambda_handler: changeset request
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.s3.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler_changeset_request(self, mock_sns_get_client, mock_dynamodb_get_client, mock_s3_get_client,
                                              aws_session_mock, mock_helper, mock_sqs):
        mock_helper.return_value = ['us-east-1']
        mock_sqs.return_value.send_message.return_value = None
        aws_session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.ost.get_item.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [io_util.load_data_json('tracking.query.json'),
                                             io_util.load_data_json('tracking.collectionblocker.query.json'),
                                             io_util.load_data_json('dynamodb.mapping_table.respond.json'),
                                             io_util.load_data_json('dynamodb.osvt.query.json')]
        mock_s3_get_client.return_value.copy.return_value = None
        mock_s3_get_client.return_value.head_object.side_effect = ClientError({'ResponseMetadata': {},
                                                                               'Error': {
                                                                                   'Code': '404',
                                                                                   'Message': 'Not Found'}},
                                                                              "FAKE")
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.put_item.json')
        mock_dynamodb_get_client.return_value.update_item.return_value = io_util.load_data_json(
            'dynamodb.ingestion.update_item.json')
        mock_sns_get_client.return_value.publish.return_value = io_util.load_data_json('sns.publish.json')
        request_input = io_util.load_data_json("sns_event_0_versions_kept_changesets.json")
        mock_dynamodb_get_client.return_value.delete_item.return_value = io_util.load_data_json('tracking.delete.json')

        self.assertEqual(update_object_event_handler.lambda_handler(request_input, MockLambdaContext()),
                         event_handler_status.SUCCESS)

        # validate that one message for each schema version was published and one for the concatenation
        # in the following order: v0, v1, v0v1
        # changeset information will not appear in v0
        calls = []
        for file in ["notification_changeset_v0.json", "notification_changeset_v1.json",
                     "notification_changeset_v0v1.json"]:
            notification = io_util.load_data_json(file)
            calls.append(call(TargetArn=topic_arn, Message=notification["message"], MessageStructure="json",
                              MessageAttributes=notification["message_attributes"]))
        mock_sns_get_client.return_value.publish.assert_has_calls(calls, any_order=False)

        # we require 3 puts here because 1 is for the OST, 1 for the OSVT, and 1 for the COT (changeset object table)
        self.assertEqual(mock_dynamodb_get_client.return_value.put_item.call_count, 3)


if __name__ == '__main__':
    unittest.main()
