import logging
import os
import unittest
from datetime import datetime

import requests
from lng_datalake_client.Admin.remove_owner import remove_owner
from lng_datalake_client.Collection.create_collection import create_collection
from lng_datalake_client.Collection.get_collection import get_collection
from lng_datalake_client.Collection.remove_collection import remove_collection
from lng_datalake_client.Collection.update_collection import suspend_collection
from lng_datalake_client.Object.describe_objects import describe_objects
from lng_datalake_client.Object.get_ingestion import get_ingestion
from lng_datalake_client.Object.get_object import get_object
from lng_datalake_client.Object.upload_large_object import upload_large_object
from lng_datalake_client.Object.upload_multipart_object import upload_multipart_object
from lng_datalake_client.Utils import setup_utils, teardown_utils
from lng_datalake_client.Utils import transaction_id
from lng_datalake_client.Utils.generate_content import generate_content, generate_hash
from lng_datalake_client.Utils.upload_content_to_s3 import upload_content_to_s3
from lng_datalake_constants import ingestion_status
from time import sleep

__author__ = "Kiran G, Brandon S, Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

my_location = os.path.join(os.path.dirname(__file__), 'Tests', 'Regression', 'Data')


class TestObjectOrchestration(unittest.TestCase):
    wait_time = int(os.getenv('WAITING_TIME', 5))
    owner_loop_times = int(os.getenv('OWNER_LOOP_TIMES', 10))
    collection_loop_times = int(os.getenv('COLLECTION_LOOP_TIMES', 10))
    object_loop_times = int(os.getenv('OBJECT_LOOP_TIMES', 10))
    large_folder_object_loop_times = int(os.getenv('LARGE_FOLDER_OBJECT_LOOP_TIMES', 200))
    ingestion_loop_times = int(os.getenv('INGESTION_LOOP_TIMES', 10))
    default_object_content_type = os.getenv('OBJECT_CONTENT_TYPE', 'application/json')
    default_binary_object_content_type = os.getenv('BINARY_OBJECT_CONTENT_TYPE', 'image/jpeg')
    default_asset_id = int(os.getenv('ASSET_ID', 62))

    owners_created = []
    collections_created = []
    suspended_collection_id = ''
    # For version tests we need to save a version of each body for obj_with_id in two version collection
    obj_with_id_body_ver = []
    # Not using for now
    objects_created = []

    @classmethod
    def setUpClass(cls):  # NOSONAR
        logger.info("### Setting up resources for {} ###".format(cls.__name__))
        cls.maxDiff = None
        # Create Owner---------------------
        owner_body_input = \
            {
                "owner-name": "regression-collection.{}".format(datetime.now().isoformat()),
                "email-distribution": ["regression-collection@lexisnexis.com"]
            }
        logger.info('Creating owner: {}'.format(owner_body_input))
        owner_response = setup_utils.setup_create_owner(owner_body_input, cls.owner_loop_times, cls.wait_time)
        cls.owner_id = owner_response['owner']['owner-id']
        cls.api_key = owner_response['owner']['api-key']
        os.environ['X-API-KEY'] = cls.api_key
        logger.info("Successfully created owner-id: {}.".format(cls.owner_id))

        cls.owners_created.append(owner_response['owner'])

        logger.info('Creating collections')
        # Create Collection zero versions - similar to immutable collection but allows update ---------------------
        # old-versions-to-keep not specified - defaults to zero
        zero_versions_collection_input = \
            {
                'body': {
                    "asset-id": cls.default_asset_id,
                    'classification-type': 'Test'
                },
                'collection-id': "zero-versions-id-regression-{}".format(datetime.timestamp(datetime.now()))[:-7]
            }
        cls.zero_versions_collection_id = cls.createAndVerifyCollection(zero_versions_collection_input, 'zero versions')

        # Create Collection two versions (current and up to two old) -------------------
        two_versions_collection_input = \
            {
                'body': {
                    "asset-id": cls.default_asset_id,
                    'old-object-versions-to-keep': 2,
                    'classification-type': 'Test'
                },
                'collection-id': "two-version-id-regression-{}".format(
                    datetime.timestamp(datetime.now()))[:-7]
            }
        cls.two_versions_collection_id = cls.createAndVerifyCollection(two_versions_collection_input, 'two versions')

        # Create Collection for Ingestion tests -------------------
        collection_for_ingestion_input = \
            {
                'body': {
                    "asset-id": cls.default_asset_id,
                    'old-object-versions-to-keep': 1,
                    'classification-type': 'Test'
                },
                'collection-id': "ingestion-regression-{}".format(
                    datetime.timestamp(datetime.now()))[:-7]
            }
        cls.collection_for_ingestion = cls.createAndVerifyCollection(collection_for_ingestion_input, 'ingestion')

        # Create a collection and suspend it for negative test cases ------------------
        suspended_collection_input = \
            {
                'body': {
                    "asset-id": cls.default_asset_id,
                    'classification-type': 'Test'
                },
                'collection-id': "suspended-id-regression-{}".format(
                    datetime.timestamp(datetime.now()))[:-7]
            }
        cls.suspended_collection_id = cls.createAndVerifyCollection(suspended_collection_input, 'suspended')
        suspend_collection({'collection-id': cls.suspended_collection_id})
        teardown_utils.validate_collection_state('Suspended', {'collection-id': cls.suspended_collection_id},
                                                 cls.__class__, cls.collection_loop_times, cls.wait_time)

    @classmethod
    def createAndVerifyCollection(cls, collection_input: dict, collection_descr: str) -> str:
        logger.info('Creating {} collection'.format(collection_descr))
        collection_response = create_collection(collection_input)
        if collection_response.status_code != 202:
            if 'error' in collection_response.json():
                logger.error("Unable to create {} collection. Message=[{}].".format(
                    collection_descr, collection_response.json()['error']))
            raise Exception("Unable to create {} versions collection; status={}".format(
                collection_descr, collection_response.status_code))

        collection_id = collection_response.json()['collection']['collection-id']

        cls.waitForCollection(collection_id)

        logger.info('Successfully created a {} collection with id: {}'.format(collection_descr, collection_id))
        cls.collections_created.append(collection_response.json()['collection'])
        return collection_id

    @classmethod
    def waitForCollection(cls, collection_id):
        for i in range(0, cls.collection_loop_times):
            sleep(cls.wait_time)
            collection_response = get_collection({'collection-id': collection_id})
            if collection_response.status_code == 200:
                break
            if i == cls.collection_loop_times - 1:
                raise Exception("Unable to create collection; status={}".format(collection_response.status_code))

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        sleep(cls.wait_time)
        logger.info("### Cleaning up resources for {} ###".format(cls.__name__))
        # objects removed in remove_object function

        # suspend all collections
        logger.info("Suspending collections")
        for c in cls.collections_created:
            if c['collection-id'] != cls.suspended_collection_id:
                suspend_collection(c)

        logger.info("Checking that all collections were suspended")
        expected_state = 'Suspended'
        for c in cls.collections_created:
            teardown_utils.validate_collection_state(
                expected_state, c, cls.__class__, cls.collection_loop_times, cls.wait_time)

        # remove all collections
        logger.info("Terminating collections")
        for c in cls.collections_created:
            remove_collection(c)

        logger.info("Checking that all collections were removed")
        expected_state = 'Terminated'
        for c in cls.collections_created:
            teardown_utils.validate_collection_state(
                expected_state, c, cls.__class__, cls.collection_loop_times, cls.wait_time)

        # remove owners
        logger.info("Removing owners")
        for o in cls.owners_created:
            remove_owner(o)

    def test_orchestration(self):
        logger.info("### Running Object Regression tests ###")

        # Ingestion create tests
        first_ingestion, second_ingestion, ingestion_to_cancel = self.create_ingestion()

        # Complete multipart upload for the first ingestion
        self.upload_multipart_ingestion(first_ingestion,
                                        os.path.join(my_location, 'CreateIngestion', "first_ingestion.zip"))

        # Folder object create and upsert tests
        folder_object, finish_folder_upload_object = self.create_finish_object_folder()
        folder_object_upserted, finish_folder_upload_upserted_object = self.upsert_finish_object_folder()

        # Multipart create and upsert tests
        multipart_object = self.create_multipart_object()
        multipart_object_upserted = self.upsert_multipart_object()

        # Large object create and upsert tests
        large_object = self.create_large_object()
        large_object_upserted = self.upsert_large_object()

        # Small object create and upsert tests
        object_two_vers_with_metadata, object_two_vers_with_id, object_binary, object_zero_vers, object_dupe_body_diff_id = self.create_object()
        object_upserted, object_upserted_dupe_body = self.upsert_object()

        # Verify large objects
        self.verify_created_large_object(large_object)
        self.verify_created_large_object(large_object_upserted)

        # Verify multipart_object_upserted
        self.verify_created_large_object(multipart_object_upserted)

        self.describe_objects(object_two_vers_with_id, object_zero_vers, object_two_vers_with_metadata)
        self.list_objects()

        # Verify folder objects were created before using them for GET
        self.verify_created_object_folder(finish_folder_upload_object)
        self.verify_created_object_folder(finish_folder_upload_upserted_object)

        objects = {
            "object-two-versions-with-metadata": object_two_vers_with_metadata,
            "object-two-versions-with-id": object_two_vers_with_id,
            "object-binary": object_binary,
            "object-zero-versions": object_zero_vers,
            "object-dupe-body-diff-id": object_dupe_body_diff_id,
            "object-upserted": object_upserted,
            "object-upserted-dupe-body": object_upserted_dupe_body,
            "multipart-object": multipart_object,
            "multipart-object-upserted": multipart_object_upserted,
            "large-object": large_object,
            "large-object-upserted": large_object_upserted,
            "folder-object": folder_object,
            "folder-object-upserted": folder_object_upserted
        }

        # Placed get_object() as late as possible to minimize time wasted in "verify_()" retry loop
        self.get_object(objects)

        # Get Ingestion tests
        self.get_ingestion(ingestion_to_cancel)

        # Cancel Ingestion tests
        self.cancel_ingestion(ingestion_to_cancel['ingestion'])

        # Complete the second ingestion here to avoid being blocked by the first one, which add 2min delay in retry
        self.upload_multipart_ingestion(second_ingestion,
                                        os.path.join(my_location, 'CreateIngestion', "second_ingestion.zip"))

        # Update object tests
        large_object_updated_content, large_object_updated_props = self.update_large_object(large_object)
        self.update_object(object_two_vers_with_id, object_zero_vers)
        self.update_object_folder(folder_object)
        self.update_multipart_object(multipart_object)

        # Placed verify_updated_large_object() as late as possible to minimize time wasted in "verify_()" retry loop
        self.verify_updated_large_object(large_object_updated_content, large_object_updated_props)

        # Remove object tests
        self.remove_object(objects)
        object_removed_then_created, object_removed_then_upserted = self.remake_object(object_two_vers_with_id,
                                                                                       object_binary)
        self.remove_object_remade(object_removed_then_created, object_removed_then_upserted)

        # Verify objects created by Ingestion
        object_ids = self.verify_object_created_by_ingestion(first_ingestion, second_ingestion)

        # Remove objects created by ingestion using batch remove object
        self.batch_remove_objects(object_ids, self.collection_for_ingestion)
        logger.info("Successfully tested Object Regression")

    def create_finish_object_folder(self):
        logger.info("### Running CreateFinishObjectFolder tests ###")
        from Tests.Regression.regression_create_folder_upload import TestCreateFolderUpload
        from Tests.Regression.regression_finish_folder_upload import TestFinishFolderUpload

        # + Create Folder
        folder_obj_id = "regression-object-id-{}".format(datetime.now().isoformat())
        tst_create_folder = TestCreateFolderUpload()
        create_folder_resp = tst_create_folder.test_create_folder_upload_success({
            "collection-id": self.two_versions_collection_id,
            "object-id": folder_obj_id
        })

        # + Upload to s3
        create_folder_resp_obj = create_folder_resp.json()['folder-upload']
        create_folder_resp_obj['object-id'] = create_folder_resp.json()['object']['object-id']
        upload_to_s3_resp = upload_content_to_s3(create_folder_resp_obj,
                                                 os.path.join(my_location, 'CreateFolder'))
        status_code = upload_to_s3_resp["ResponseMetadata"]["HTTPStatusCode"]
        if status_code != 200:
            raise Exception("Folder upload failed; status={}".format(status_code))
        logger.info("Successfully uploaded folder objects to s3")

        # + Confirm that a duplicate request to Create Folder Object will continue to use the same prefix/transaction-id
        extended_folder_transaction = tst_create_folder.test_create_folder_upload_success({
            "collection-id": self.two_versions_collection_id,
            "object-id": folder_obj_id
        })
        extended_folder_resp_obj = extended_folder_transaction.json()['folder-upload']

        if extended_folder_resp_obj['S3'].items() != create_folder_resp_obj['S3'].items() or \
                extended_folder_resp_obj['transaction-id'] != create_folder_resp_obj['transaction-id']:
            raise Exception("Folder transaction not extended: Original: \n\t{0}\nSecond Request: "
                            "\n\t{1}".format(create_folder_resp_obj, extended_folder_resp_obj))
        logger.info(
            'Folder object transaction successfully extended: {}'.format(extended_folder_resp_obj['transaction-id']))

        # + Finish folder
        tst_finish_folder = TestFinishFolderUpload()
        finish_folder_upload_resp = tst_finish_folder.test_finish_folder_upload_success(
            create_folder_resp_obj)
        logger.info("Successfully tested finish folder upload: success")
        logger.info('Folder Object ID: {}'.format(finish_folder_upload_resp.json()['object']['object-id']))

        # - Create folder upload tests
        tst_create_folder.test_create_folder_upload_invalid_collection_id({
            "collection-id": 'regression-abcdefghijklmnopqrstuvwxyz',
            "object-id": folder_obj_id
        })
        logger.info("Successfully tested create folder upload: invalid collection id")
        tst_create_folder.test_create_folder_upload_invalid_collection_state({
            "collection-id": self.suspended_collection_id,
            "object-id": folder_obj_id
        })
        logger.info("Successfully tested create folder upload: invalid collection state")
        tst_create_folder.test_create_folder_upload_invalid_content_type()
        logger.info("Successfully tested create folder upload: invalid content type")
        tst_create_folder.test_create_folder_upload_authentication_error()
        logger.info("Successfully tested create folder upload: invalid authentication")

        # - Create finish folder upload tests
        parts = transaction_id.unpack_folder_object_transaction_id(
            extended_folder_transaction.json()["folder-upload"]["transaction-id"])

        collection_id = 'regression-abcdefghijklmnopqrstuvwxyz'
        bad_collection_transaction = transaction_id.build_folder_object_transaction_id(parts[0], collection_id,
                                                                                       parts[2], parts[3])
        tst_finish_folder.test_finish_folder_upload_invalid_collection_id({
            "object-id": folder_obj_id,
            "transaction-id": bad_collection_transaction
        })
        logger.info("Successfully tested finish folder upload: invalid collection id")

        tst_finish_folder.test_finish_folder_upload_invalid_object_id({
            "object-id": 'regression-abcdefghijklmnopqrstuvwxyz',
            "transaction-id": create_folder_resp_obj["transaction-id"]
        })
        logger.info("Successfully tested finish folder upload: invalid object id")

        parts = transaction_id.unpack_folder_object_transaction_id(create_folder_resp_obj["transaction-id"])

        bad_transaction = transaction_id.build_folder_object_transaction_id("abc-invalid", parts[1], parts[2],
                                                                            parts[3])
        tst_finish_folder.test_finish_folder_upload_invalid_transaction_id({
            "object-id": folder_obj_id,
            "transaction-id": bad_transaction
        })
        logger.info("Successfully tested finish folder upload: invalid transaction id")
        tst_finish_folder.test_finish_folder_upload_invalid_content_type()
        logger.info("Successfully tested finish folder upload: invalid content type")
        tst_finish_folder.test_finish_folder_upload_authentication_error()
        logger.info("Successfully tested finish folder upload: invalid authentication")

        create_folder_resp_obj['asset-id'] = self.default_asset_id
        create_folder_resp_obj['collection-id'] = self.two_versions_collection_id
        create_folder_resp_obj['owner-id'] = self.owner_id
        return create_folder_resp_obj, finish_folder_upload_resp.json()["object"]

    def upsert_finish_object_folder(self):
        logger.info("### Running Upsert Object Folder tests ###")
        from Tests.Regression.regression_update_folder_upload import TestUpdateFolderUpload
        from Tests.Regression.regression_finish_folder_upload import TestFinishFolderUpload
        tst_update_folder = TestUpdateFolderUpload()
        tst_finish_folder = TestFinishFolderUpload()

        # + Upsert folder upload
        folder_obj_id = "regression-object-id-{}".format(datetime.now().isoformat())
        upsert_folder_resp = tst_update_folder.test_update_folder_upload_success({
            "collection-id": self.two_versions_collection_id,
            "object-id": folder_obj_id
        })
        folder_upload_prop = upsert_folder_resp.json()["folder-upload"]
        folder_upload_prop['object-id'] = upsert_folder_resp.json()['object']['object-id']
        upload_to_s3_resp = upload_content_to_s3(folder_upload_prop,
                                                 os.path.join(my_location, 'UpdateFolder'))
        status_code = upload_to_s3_resp["ResponseMetadata"]["HTTPStatusCode"]
        if status_code != 200:
            raise Exception("Folder upload failed; status={}".format(status_code))
        logger.info("Successfully uploaded folder objects to s3")
        finish_update_folder_upload_resp = tst_finish_folder.test_finish_folder_upload_success(folder_upload_prop)
        logger.info("Successfully tested finish folder upload: success")
        finish_update_folder_upload_resp_object_props = finish_update_folder_upload_resp.json()
        logger.info("Successfully tested upsert folder upload: success")

        # add keys for future tests
        folder_upload_prop['asset-id'] = self.default_asset_id
        folder_upload_prop['collection-id'] = self.two_versions_collection_id
        folder_upload_prop['owner-id'] = self.owner_id
        finish_update_folder_upload_resp_object_props['object-id'] = folder_obj_id
        finish_update_folder_upload_resp_object_props['owner-id'] = self.owner_id
        return folder_upload_prop, finish_update_folder_upload_resp_object_props

    def create_large_object(self):
        logger.info("### Running Create Large Object tests ###")
        from Tests.Regression.regression_create_large_object import TestCreateLargeObject
        tst_create_large_obj = TestCreateLargeObject()
        create_large_object_content = generate_content('create', 5000000)
        request_input_large_obj = {
            "collection-id": self.two_versions_collection_id,
            "asset-id": self.default_asset_id,
            "owner-id": self.owner_id,
            'object-id': "regression-large-object-id-{}".format(datetime.now().isoformat()),
            'Content-MD5': generate_hash(create_large_object_content)
        }
        logger.info('Creating a large object: {}'.format(request_input_large_obj))
        create_large_obj_resp = tst_create_large_obj.test_create_large_object(request_input_large_obj)
        create_large_obj_resp_obj = create_large_obj_resp.json()
        create_large_obj_resp_props = create_large_obj_resp_obj["large-upload"]
        logger.info('Create large object response: {}'.format(create_large_obj_resp_props))

        upload_large_object_resp = upload_large_object(create_large_obj_resp_props, create_large_object_content)
        if upload_large_object_resp.status_code != 200:
            raise Exception("Large object upload failed; status={}".format(upload_large_object_resp.status_code))
        logger.info("Successfully uploaded large object to pre-signed url")

        # - Create object tests
        logger.info("Running invalid create object tests")

        tst_create_large_obj.test_create_large_object_invalid_collection_id({
            'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz',
            'object-id': 'foobar',
            'Content-MD5': '202cb962ac59075b964b07152d234b70'
        })
        logger.info("Successfully tested create object: invalid collection id")

        tst_create_large_obj.test_create_large_object_invalid_content_type()
        logger.info("Successfully tested create object: invalid content type")
        tst_create_large_obj.test_create_large_object_authentication_error()
        logger.info("Successfully tested create object: authentication error")

        # add keys for future tests
        create_large_obj_resp_obj['object']["content-type"] = self.default_object_content_type
        create_large_obj_resp_obj['object']["body"] = create_large_object_content
        return create_large_obj_resp_obj['object']

    def upsert_large_object(self):
        logger.info("### Running Upsert Large Object tests ###")
        from Tests.Regression.regression_update_large_object import TestUpdateLargeObject
        tst_update_large_obj = TestUpdateLargeObject()

        # + Upsert large object
        upsert_large_object_content = generate_content('upsert', 5000000)
        request_input_large_obj = {
            "collection-id": self.two_versions_collection_id,
            "asset-id": self.default_asset_id,
            "owner-id": self.owner_id,
            'object-id': "regression-upsert-large-object-id-{}".format(datetime.now().isoformat()),
            'Content-MD5': generate_hash(upsert_large_object_content)
        }
        logger.info('Upserting a large object: {}'.format(request_input_large_obj))
        upsert_large_obj_resp = tst_update_large_obj.test_update_large_object(request_input_large_obj)
        upsert_large_obj_resp_obj = upsert_large_obj_resp.json()
        upsert_large_obj_resp_props = upsert_large_obj_resp_obj["large-upload"]
        logger.info('Upsert large object response: {}'.format(upsert_large_obj_resp_props))

        upload_large_object_resp = upload_large_object(upsert_large_obj_resp_props, upsert_large_object_content)
        if upload_large_object_resp.status_code != 200:
            raise Exception("Large object upload failed; status={}".format(upload_large_object_resp.status_code))
        logger.info("Successfully uploaded large object to pre-signed url")

        # add keys for future tests
        upsert_large_obj_resp_obj['object']["content-type"] = self.default_object_content_type
        upsert_large_obj_resp_obj['object']["body"] = upsert_large_object_content
        return upsert_large_obj_resp_obj['object']

    def create_object(self):
        logger.info("### Running Create Object tests ###")
        from Tests.Regression.regression_create_object import TestCreateObject
        tst_create_obj = TestCreateObject()
        # + Create object with object metadata
        body_1 = "content from create object with object metadata test {}".format(datetime.now().isoformat())
        request_input_with_metadata = {
            "headers": {
                'Content-type': self.default_object_content_type,
                'x-dl-meta-name1': 'value1',
                'x-dl-meta-name2': 'value2',
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.two_versions_collection_id,
            "asset-id": self.default_asset_id,
            "owner-id": self.owner_id,
            "body": body_1,
            "object-id": "regression-object-metadata-{}".format(datetime.now().isoformat())
        }
        logger.info('Creating a object with object metadata: {}'.format(request_input_with_metadata))

        create_obj_resp_with_metadata = tst_create_obj.test_create_object_success(request_input_with_metadata)
        create_obj_resp_with_metadata_props = create_obj_resp_with_metadata.json()["object"]
        logger.info('Create object with with object metadata response: {}'.format(create_obj_resp_with_metadata_props))
        create_obj_resp_with_metadata_props["content-type"] = self.default_object_content_type
        create_obj_resp_with_metadata_props["body"] = body_1

        # + Create normal object
        body_2 = "content coming from create object test {}".format(datetime.now().isoformat())
        request_input_with_obj_id = {
            "collection-id": self.two_versions_collection_id,
            "asset-id": self.default_asset_id,
            "body": body_2,
            "object-id": "regression-object-id-{}".format(datetime.now().isoformat())
        }
        logger.info('Creating a object with input object id: {}'.format(request_input_with_obj_id))
        create_obj_resp_with_id = tst_create_obj.test_create_object_success(request_input_with_obj_id)
        # TODO: Start here, verify we actually create
        create_obj_resp_with_id_props = create_obj_resp_with_id.json()["object"]
        logger.info('Create object with input object id response: {}'.format(create_obj_resp_with_id_props))
        create_obj_resp_with_id_props["content-type"] = self.default_object_content_type
        create_obj_resp_with_id_props["body"] = body_2
        # Save version #1 of body for version tests in update section
        self.obj_with_id_body_ver.append(body_2)

        # + Create binary object
        with open(os.path.join(my_location, 'CreateObject', "lake.jpg"), "rb") as binary_file:
            body_3 = binary_file.read()
        request_input_binary_obj = {
            "headers": {
                'Content-type': self.default_binary_object_content_type,
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": self.two_versions_collection_id,
            "asset-id": self.default_asset_id,
            "owner-id": self.owner_id,
            "body": body_3,
            "object-id": "regression-binary-{}".format(datetime.now().isoformat())
        }
        logger.info('Creating a binary object: {}'.format(request_input_binary_obj))
        create_binary_obj_resp = tst_create_obj.test_create_object_success(request_input_binary_obj)
        create_binary_obj_resp_props = create_binary_obj_resp.json()["object"]
        logger.info('Create object with input object id response: {}'.format(create_binary_obj_resp_props))
        create_binary_obj_resp_props["content-type"] = self.default_binary_object_content_type
        create_binary_obj_resp_props["body"] = body_3

        # + Create object with id - zero object versions collection
        body_4 = "content from create object test {}".format(datetime.now().isoformat())
        request_input_obj_zero_versions_inp = {
            "collection-id": self.zero_versions_collection_id,
            "asset-id": self.default_asset_id,
            "owner-id": self.owner_id,
            "body": body_4,
            "object-id": "regression-0versions-{}".format(datetime.now().isoformat())
        }
        logger.info('Creating a object with input object id: {}'.format(request_input_obj_zero_versions_inp))
        create_obj_zero_versions_resp = tst_create_obj.test_create_object_success(request_input_obj_zero_versions_inp)
        create_obj_zero_versions_resp_props = create_obj_zero_versions_resp.json()["object"]
        logger.info('Create object with input object id response: {}'.format(create_obj_zero_versions_resp_props))
        create_obj_zero_versions_resp_props["content-type"] = self.default_object_content_type
        create_obj_zero_versions_resp_props["body"] = body_4

        # Save object-id for later upsert removed object test
        request_input_obj_zero_versions_inp["object-id"] = create_obj_zero_versions_resp_props["object-id"]

        # + Create object with duplicate body and metadata, different id
        request_input_dupe_body_diff_id = request_input_with_obj_id.copy()
        request_input_dupe_body_diff_id["object-id"] = "regression-object-dupe-body-different-id-{}".format(
            datetime.now().isoformat())
        logger.info('Creating a object with input object id: {}'.format(request_input_with_metadata))
        create_obj_dupe_body_diff_id_resp = tst_create_obj.test_create_object_success(
            request_input_dupe_body_diff_id)
        # TODO: Start here, verify we actually create
        create_obj_dupe_body_diff_id_resp_props = create_obj_dupe_body_diff_id_resp.json()["object"]
        logger.info('Create object with input object id response: {}'.format(create_obj_dupe_body_diff_id_resp_props))
        create_obj_dupe_body_diff_id_resp_props["content-type"] = self.default_object_content_type
        create_obj_dupe_body_diff_id_resp_props["body"] = body_2

        # - Create object tests
        logger.info("Running invalid create object tests")

        # # Comment explaining why we've commented these out located in regression_create_object.py
        # tst_create_obj.test_create_object_invalid_exceed_payload_limit_lambda({
        #     "collection-id": self.two_versions_collection_id,
        #     "body": generate_content('', 5000000)
        # })
        # logger.info("Successfully tested create object: invalid lambda payload limit")
        # tst_create_obj.test_create_object_invalid_exceed_payload_limit_api({
        #     "collection-id": self.two_versions_collection_id,
        #     "body": generate_content('', 10000000)
        # })
        # logger.info("Successfully tested create object: invalid api payload limit")

        tst_create_obj.test_create_object_invalid_collection_id({
            'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz',
            'object-id': "regression-create-object-id-{}".format(datetime.now().isoformat()),
            'body': "regression-abcdefghijklmnopqrstuvwxyz"
        })
        logger.info("Successfully tested create object: invalid collection id")
        tst_create_obj.test_create_object_invalid_collection_state({
            'collection-id': self.suspended_collection_id,
            'object-id': "regression-create-object-id-{}".format(datetime.now().isoformat()),
            'body': "regression-abcdefghijklmnopqrstuvwxyz"
        })
        logger.info("Successfully tested create object: invalid collection state")
        tst_create_obj.test_create_object_invalid_metadata_character({
            'collection-id': self.zero_versions_collection_id,
            'object-id': 'foobar',
            'body': 'regression-abcdefghijklmnopqrstuvwxyz',
            'headers': {'Content-Type': 'application/json', 'x-api-key': os.environ['X-API-KEY']}
        })
        logger.info("Successfully tested create object: invalid metadata character")
        tst_create_obj.test_create_object_invalid_content_type()
        logger.info("Successfully tested create object: invalid content type")
        tst_create_obj.test_create_object_authentication_error()
        logger.info("Successfully tested create object: authentication error")

        # + Verifying objects were created
        self.check_obj_200_response(create_obj_resp_with_metadata_props, self.object_loop_times)
        self.check_obj_200_response(create_obj_resp_with_id_props, self.object_loop_times)
        self.check_obj_200_response(create_binary_obj_resp_props, self.object_loop_times)
        self.check_obj_200_response(create_obj_zero_versions_resp_props, self.object_loop_times)
        self.check_obj_200_response(create_obj_dupe_body_diff_id_resp_props, self.object_loop_times)

        # - duplicate check must happen AFTER we confirm the object has been created.
        tst_create_obj.test_create_object_invalid_duplicated_object_id({
            'collection-id': self.two_versions_collection_id,
            'object-id': create_obj_resp_with_id_props["object-id"],
            'body': "regression-abcdefghijklmnopqrstuvwxyz"
        })
        logger.info("Successfully tested create object: duplicate object id")

        return (create_obj_resp_with_metadata_props, create_obj_resp_with_id_props, create_binary_obj_resp_props,
                create_obj_zero_versions_resp_props, create_obj_dupe_body_diff_id_resp_props)

    def upsert_object(self):
        logger.info("### Running Upsert Object tests ###")
        from Tests.Regression.regression_update_object import TestUpdateObject
        tst_update_obj = TestUpdateObject()

        # + Upsert object with id
        logger.info("Upserting object with id")
        body = "content coming from upsert object with object id test {}".format(datetime.now().isoformat())
        request_input_with_obj_id = {
            "collection-id": self.two_versions_collection_id,
            "asset-id": self.default_asset_id,
            "body": body,
            "object-id": "regression-upsert-object-id-{}".format(datetime.now().isoformat())
        }
        upsert_object_resp_props = tst_update_obj.test_update_object(request_input_with_obj_id).json()["object"]
        logger.info(
            'Successful upsert an object with input object id: {}'.format(request_input_with_obj_id['object-id']))

        # + Upsert object with duplicate body, different id
        logger.info("Upserting object with duplicate body, different id")
        request_input_with_obj_id["object-id"] = "regression-upsert-object-with-duplicate-body-id-{}".format(
            datetime.now().isoformat())
        upsert_object_dupe_body_resp_props = tst_update_obj.test_update_object(request_input_with_obj_id).json()[
            "object"]
        logger.info('Successful upsert an object with duplicate body, different id - input object id: {}'.format(
            request_input_with_obj_id['object-id']))

        # + Verifying objects were created
        self.check_obj_200_response(upsert_object_resp_props, self.object_loop_times)
        self.check_obj_200_response(upsert_object_dupe_body_resp_props, self.object_loop_times)

        # add keys for future tests
        upsert_object_resp_props['body'] = body
        upsert_object_resp_props['content-type'] = self.default_object_content_type
        upsert_object_dupe_body_resp_props["body"] = body
        upsert_object_dupe_body_resp_props['content-type'] = self.default_object_content_type
        return upsert_object_resp_props, upsert_object_dupe_body_resp_props

    def check_obj_200_response(self, obj_props, loop_times):
        for i in range(0, loop_times):
            sleep(self.wait_time)
            describe_objects_response = describe_objects(obj_props)
            if describe_objects_response.status_code == 200 and len(describe_objects_response.json()['objects']) == 1:
                break
            if i == loop_times - 1:
                raise Exception("Object not found; obj={} status={} reason={}".format(obj_props,
                                                                                      describe_objects_response.status_code,
                                                                                      describe_objects_response.text))
        logger.info("Successfully describe object with object-id: {}".format(obj_props["object-id"]))
        self.objects_created.append(obj_props)

    def check_obj_and_body_200_response(self, obj_props, expected_body, loop_times):
        for i in range(0, loop_times):
            sleep(self.wait_time)
            get_object_response = get_object(obj_props)
            response_body = get_object_response.content
            if get_object_response.status_code == 200:
                if not isinstance(expected_body, bytes):
                    response_body = get_object_response.content.decode("utf-8")
                if response_body == expected_body:
                    break
            if i == loop_times - 1:
                if get_object_response.status_code != 200:
                    raise Exception("Object not found; status={}".format(get_object_response.status_code))
                else:
                    raise Exception("Object body not as expected; expected: {}; found: {}; status={}".format(
                        expected_body, response_body, get_object_response.status_code))
        logger.info("Successfully get object with object-id: {}".format(obj_props["object-id"]))
        self.objects_created.append(obj_props)

    def create_multipart_object(self):
        logger.info("### Running CreateMultipartObject tests ###")
        from Tests.Regression.regression_create_multipart_object import TestCreateMultipartObject
        test_create_mp_object = TestCreateMultipartObject()
        object_content = generate_content("multipart_upload", 5242880)

        # + Create multipart object with id
        request_input_multipart_obj = {
            "collection-id": self.two_versions_collection_id,
            "object-id": "regression-multipart-object-id-{}".format(datetime.now().isoformat()),
            "stored-content-type": self.default_object_content_type,
            "asset-id": self.default_asset_id
        }
        logger.info('Creating a multipart object: {}'.format(request_input_multipart_obj))
        create_mp_object_resp = test_create_mp_object.test_create_multipart_object_success(request_input_multipart_obj)
        create_mp_obj_resp_props = create_mp_object_resp.json()
        logger.info('Create multipart object response: {}'.format(create_mp_obj_resp_props))
        upload_multipart_object_resp, create_mp_obj_resp_props['object']["body"] = upload_multipart_object(
            create_mp_obj_resp_props['multipart-upload'], "object-with-id-{}".format(object_content))
        if upload_multipart_object_resp.status_code != 200:
            raise Exception(
                'Multipart object upload failed; status_code={}'.format(upload_multipart_object_resp.status_code))
        logger.info("Successfully uploaded multipart object to S3")
        create_mp_obj_resp_props['object']["content-type"] = self.default_object_content_type
        self.check_obj_200_response(create_mp_obj_resp_props['object'], self.large_folder_object_loop_times)

        # - Create multipart object tests
        logger.info("Running invalid create multipart object tests")

        test_create_mp_object.test_create_multipart_object_duplicate_object_id({
            "collection-id": request_input_multipart_obj["collection-id"],
            "object-id": request_input_multipart_obj["object-id"],
            "stored-content-type": request_input_multipart_obj["stored-content-type"]
        })
        logger.info("Successfully tested create multipart object: duplicate object-id")

        test_create_mp_object.test_create_multipart_object_invalid_collection_id({
            "collection-id": "regression-abcdefghijklmnopqrstuvwxyz",
            "object-id": request_input_multipart_obj["object-id"],
            "stored-content-type": request_input_multipart_obj["stored-content-type"]
        })
        logger.info("Successfully tested create multipart object: invalid collection-id")

        test_create_mp_object.test_create_multipart_object_invalid_collection_state({
            "collection-id": self.suspended_collection_id,
            "object-id": request_input_multipart_obj["object-id"],
            "stored-content-type": request_input_multipart_obj["stored-content-type"]
        })
        logger.info("Successfully tested create multipart object: invalid collection state")

        test_create_mp_object.test_create_multipart_object_invalid_metadata_character({
            "collection-id": self.zero_versions_collection_id,
            "object-id": "foobar",
            "stored-content-type": self.default_object_content_type,
            'headers': {
                'x-api-key': os.environ['X-API-KEY']
            }
        })
        logger.info("Successfully tested create multipart object: invalid object metadata character")

        test_create_mp_object.test_create_multipart_object_invalid_content_type()
        logger.info("Successfully tested create multipart object: invalid Content-Type")
        test_create_mp_object.test_create_multipart_object_invalid_content_type_2()
        logger.info("Successfully tested create multipart object: invalid Content-Type 2")

        test_create_mp_object.test_create_multipart_object_authentication_error()
        logger.info("Successfully tested create multipart object: invalid authentication")
        return create_mp_obj_resp_props['object']

    def upsert_multipart_object(self):
        logger.info("### Running UpsertMultipartObject tests ###")
        from Tests.Regression.regression_update_multipart_object import TestUpdateMultipartObject
        test_update_mp_obj = TestUpdateMultipartObject()

        # + Upsert multipart object with id
        request_input_upsert_multipart_obj = {
            "collection-id": self.two_versions_collection_id,
            "object-id": "regression-upsert-multipart-object-id-{}".format(datetime.now().isoformat()),
            "content-type": self.default_object_content_type,
            "asset-id": self.default_asset_id
        }
        upserted_object_body_part = "upserted_{}".format(generate_content("multipart_upload", 80 * 1024 * 1024))
        logger.info('Upserting a multipart object: {}'.format(request_input_upsert_multipart_obj))
        upsert_mp_object_resp = test_update_mp_obj.test_update_multipart_object_success(
            request_input_upsert_multipart_obj)
        upsert_mp_resp_props = upsert_mp_object_resp.json()
        upsert_mp_obj_resp_props = upsert_mp_resp_props['object']
        upsert_mp_upload_resp_props = upsert_mp_resp_props['multipart-upload']
        logger.info('Upsert multipart object response: {}'.format(upsert_mp_obj_resp_props))

        upload_multipart_object_resp, upserted_object_body = upload_multipart_object(upsert_mp_upload_resp_props,
                                                                                     upserted_object_body_part,
                                                                                     parts=10)
        if upload_multipart_object_resp.status_code != 200:
            raise Exception(
                "Multipart upload object upsert failed; status={}".format(upload_multipart_object_resp.status_code))
        logger.info("Successfully uploaded upserted multipart object to S3")

        # add keys for get_object test
        upsert_mp_obj_resp_props['content-type'] = self.default_object_content_type
        upsert_mp_obj_resp_props['body'] = upserted_object_body
        return upsert_mp_obj_resp_props

    def create_ingestion(self) -> tuple:
        logger.info("### Running Create Ingestion Regression Tests###")
        from Tests.Regression.regression_create_ingestion_multipart import TestCreateIngestionMultipart
        tst_create_ingestion = TestCreateIngestionMultipart()

        # create the first ingestion
        input_dict = {'collection-id': self.collection_for_ingestion,
                      'content-type': 'application/zip',
                      'description': "first ingestion for regression test {}".format(datetime.now().isoformat())}
        first_ingestion = tst_create_ingestion.test_create_ingestion_multipart_success(input_dict).json()

        # create the second ingestion for the same collection
        input_dict['description'] = "second ingestion for regression test {}".format(datetime.now().isoformat())
        second_ingestion = tst_create_ingestion.test_create_ingestion_multipart_success(input_dict).json()

        # create a third ingestion for the Cancel Ingestion tests
        input_dict['description'] = "ingestion to cancel for regression test {}".format(datetime.now().isoformat())
        ingestion_to_cancel = tst_create_ingestion.test_create_ingestion_multipart_success(input_dict).json()
        logger.info("Successfully tested create ingestion multipart")

        # - Create Ingestion tests
        tst_create_ingestion.test_create_ingestion_multipart_invalid_collection_id()
        logger.info("Successfully tested create ingestion multipart: invalid collection id")

        input_dict = {'collection-id': self.suspended_collection_id, 'content-type': 'application/zip'}
        tst_create_ingestion.test_create_ingestion_multipart_invalid_collection_state(input_dict)
        logger.info("Successfully tested create ingestion multipart: invalid collection state")

        tst_create_ingestion.test_create_ingestion_multipart_invalid_content_type()
        logger.info("Successfully tested create ingestion multipart: invalid content type")
        tst_create_ingestion.test_create_ingestion_multipart_invalid_content_type_2()
        logger.info("Successfully tested create ingestion multipart: invalid content type 2")

        tst_create_ingestion.test_create_ingestion_multipart_authentication_error()
        logger.info("Successfully tested create ingestion multipart: invalid authentication")

        # verify each ingestion was created
        for ingestion in (first_ingestion, second_ingestion, ingestion_to_cancel):
            self.verify_ingestion_state(ingestion['ingestion'], ingestion_status.PENDING)

        return first_ingestion, second_ingestion, ingestion_to_cancel

    def verify_ingestion_state(self, ingestion: dict, ingestion_state: str):
        for i in range(0, self.ingestion_loop_times):
            # exponential backoff (5sec...10min) before retry when checking for completed.
            if ingestion_state == ingestion_status.COMPLETED:
                wait = 1.7 ** i * self.wait_time
                logger.info("Checking state COMPLETED for Ingestion. Exponential backoff sleeping {}sec".format(wait))
                sleep(wait)
            else:
                sleep(self.wait_time)
            get_ingestion_response = get_ingestion(ingestion)
            if get_ingestion_response.status_code == 200 and \
                    get_ingestion_response.json()['ingestion']['ingestion-state'] == ingestion_state:
                break
            if i == self.ingestion_loop_times - 1:
                if get_ingestion_response.status_code != 200:
                    raise Exception("Ingestion not found; status={}".format(get_ingestion_response.status_code))
                else:
                    raise Exception("Ingestion state not as expected; expected: {}; found: {}".format(
                        ingestion_state, get_ingestion_response.json()['ingestion']['ingestion-state']))
        logger.info(
            "Successfully get ingestion with ingestion-id: {} and ingestion-state: {}".format(ingestion["ingestion-id"],
                                                                                              ingestion_state))

    @staticmethod
    def upload_multipart_ingestion(ingestion: dict, path: str):
        bucket = ingestion['multipart-upload']['S3']['Bucket']
        key = ingestion['multipart-upload']['S3']['Key']
        upload_id = ingestion['multipart-upload']['UploadId']

        url = "https://{0}.s3.amazonaws.com/{1}".format(bucket, key)
        root_xml = "<CompleteMultipartUpload>"

        part_content = open(path, 'r+b')
        query_params = {
            "partNumber": 1,
            "uploadId": upload_id
        }
        upload_response = requests.put(url=url, params=query_params, data=part_content)
        part_xml = "<Part><PartNumber>1</PartNumber><ETag>{0}</ETag></Part>".format(upload_response.headers["ETag"])
        root_xml = "{}{}".format(root_xml, part_xml)

        complete_xml = "{}</CompleteMultipartUpload>".format(root_xml)
        # complete multipart upload
        query_params = {
            "uploadId": upload_id
        }

        complete_response = requests.post(url, params=query_params, data=complete_xml)
        if complete_response.status_code != 200:
            raise Exception('Multipart ingestion upload failed; status_code={}'.format(complete_response.status_code))
        logger.info("Successfully uploaded multipart ingestion file to S3")

    def get_object(self, objects: dict) -> None:
        logger.info("### Running Get Object tests ###")
        from Tests.Regression.regression_get_object import TestGetObject
        tst_get_obj = TestGetObject()

        for object_description, object_dict in objects.items():
            # TODO: add valid test for folder object
            if object_description not in ["folder-object", "folder-object-upserted"]:
                tst_get_obj.test_get_object(object_dict)
                logger.info("Successfully tested get object: success {}".format(object_description))

        # - Get object tests
        tst_get_obj.test_get_object_invalid_object_id({
            "collection-id": objects["object-two-versions-with-id"]["collection-id"],
            "object-id": "regression-abcdefghijklmnopqrstuvwxyz"
        })
        logger.info("Successfully tested get object: invalid object id")
        tst_get_obj.test_get_object_invalid_collection_id({
            "collection-id": "regression-abcdefghijklmnopqrstuvwxyz",
            "object-id": objects["object-two-versions-with-id"]["object-id"]
        })
        logger.info("Successfully tested get object: invalid collection id")
        tst_get_obj.test_get_object_invalid_version_number({
            "collection-id": self.zero_versions_collection_id,
            "object-id": objects["object-zero-versions"]["object-id"],
            "version-number": 0
        })
        logger.info("Successfully tested get object: invalid version #0")
        tst_get_obj.test_get_object_invalid_content_type()
        logger.info("Successfully tested get object: invalid content type")
        tst_get_obj.test_get_object_authentication_error()
        logger.info("Successfully tested get object: invalid authentication")

        tst_get_obj.test_get_object_invalid_folder({
            "collection-id": objects["folder-object"]["collection-id"],
            "object-id": objects["folder-object"]["object-id"]
        })
        tst_get_obj.test_get_object_invalid_folder({
            "collection-id": objects["folder-object-upserted"]["collection-id"],
            "object-id": objects["folder-object-upserted"]["object-id"]
        })
        logger.info("Successfully tested get object: invalid folder object")

    def describe_objects(self, describe_obj_two_vers_inp: dict, describe_obj_zero_vers_inp: dict,
                         describe_obj_two_vers_with_metadata_inp: dict):
        logger.info("### Running Describe Object tests ###")
        from Tests.Regression.regression_describe_objects import TestDescribeObjects
        tst_describe_obj = TestDescribeObjects()

        tst_describe_obj.test_describe_object({
            "collection-id": describe_obj_two_vers_inp["collection-id"],
            "object-id": describe_obj_two_vers_inp["object-id"],
            "owner-id": describe_obj_two_vers_inp["owner-id"],
            "asset-id": describe_obj_two_vers_inp["asset-id"],
            "content-type": self.default_object_content_type
        })
        logger.info("Successfully tested describe object of classification-type Test: success")

        tst_describe_obj.test_describe_object({
            "collection-id": describe_obj_zero_vers_inp["collection-id"],
            "object-id": describe_obj_zero_vers_inp["object-id"],
            "owner-id": describe_obj_zero_vers_inp["owner-id"],
            "asset-id": describe_obj_zero_vers_inp["asset-id"],
            "content-type": self.default_object_content_type})
        logger.info("Successfully tested describe object of classification-type Content: success")

        obj_two_vers_with_metadata_describe_resp = tst_describe_obj.test_describe_object({
            "collection-id": describe_obj_two_vers_with_metadata_inp["collection-id"],
            "object-id": describe_obj_two_vers_with_metadata_inp["object-id"],
            "owner-id": describe_obj_two_vers_with_metadata_inp["owner-id"],
            "asset-id": describe_obj_two_vers_with_metadata_inp["asset-id"],
            "content-type": self.default_object_content_type,
            "object-metadata": describe_obj_two_vers_with_metadata_inp["object-metadata"]})
        logger.info("Successfully tested describe object with metadata: success")

        tst_describe_obj.test_describe_objects_invalid_collection_id({
            "collection-id": "regression-abcdefghijklmnopqrstuvwxyz"
        })
        logger.info("Successfully tested describe objects from invalid collection-id: 200 success")

        tst_describe_obj.test_describe_objects_all({
            "collection-id": self.two_versions_collection_id
        })
        logger.info("Successfully tested describe objects: success")
        tst_describe_obj.test_describe_objects_pagination({
            "collection-id": self.two_versions_collection_id,
            "max-items": 1
        })
        logger.info("Successfully tested describe objects: success with pagination")

        # check S3 object
        tst_describe_obj.test_get_s3_object({
            'object-key': obj_two_vers_with_metadata_describe_resp['S3']['Key'],
            'body': describe_obj_two_vers_with_metadata_inp['body'],
            'replicated-buckets': obj_two_vers_with_metadata_describe_resp['replicated-buckets'],
            'object-metadata': obj_two_vers_with_metadata_describe_resp['object-metadata']
        })
        logger.info('Successfully tested get object from replicated s3 location(s): {}'
                    .format(obj_two_vers_with_metadata_describe_resp['replicated-buckets']))

        # check S3 object from datalake url
        tst_describe_obj.test_get_s3_object_from_object_key_url({
            'object-key-url': obj_two_vers_with_metadata_describe_resp['object-key-url'],
            'body': describe_obj_two_vers_with_metadata_inp['body'],
            'object-metadata': obj_two_vers_with_metadata_describe_resp['object-metadata']
        })
        logger.info('Successfully tested get object from object key url: {}'
                    .format(obj_two_vers_with_metadata_describe_resp['object-key-url']))

        # - Failure test cases
        tst_describe_obj.test_describe_invalid_objects_pagination()
        logger.info("Successfully tested describe objects: invalid pagination")
        tst_describe_obj.test_describe_invalid_objects_pagination_diff_max_items()
        logger.info("Successfully tested describe objects: invalid pagination different max items")

        tst_describe_obj.test_describe_object_invalid_object_id({
            "collection-id": describe_obj_two_vers_inp["collection-id"],
            "object-id": "regression-abcdefghijklmnopqrstuvwxyz",
            "owner-id": describe_obj_two_vers_inp["owner-id"],
            "asset-id": describe_obj_two_vers_inp["asset-id"],
            "content-type": self.default_object_content_type
        })
        logger.info("Successfully tested describe object: invalid object id")

        tst_describe_obj.test_describe_object_invalid_collection_id({
            "collection-id": "regression-abcdefghijklmnopqrstuvwxyz",
            "object-id": describe_obj_two_vers_inp["object-id"],
            "owner-id": describe_obj_two_vers_inp["owner-id"],
            "asset-id": describe_obj_two_vers_inp["asset-id"],
            "content-type": self.default_object_content_type
        })
        logger.info("Successfully tested describe object: invalid collection id")
        tst_describe_obj.test_describe_objects_invalid_content_type()
        logger.info("Successfully tested describe object: invalid content type")
        tst_describe_obj.test_describe_objects_authentication_error()
        logger.info("Successfully tested describe object: invalid authentication")

    def list_objects(self):
        logger.info("### Running List Objects tests ###")
        from Tests.Regression.regression_list_objects import TestListObjects
        tst_list_objs = TestListObjects()
        tst_list_objs.test_list_objects_all({
            "collection-id": self.two_versions_collection_id
        })
        tst_list_objs.test_list_objects_with_object_state_filter({
            "collection-id": self.two_versions_collection_id,
            "state": 'created'
        })
        logger.info("Successfully tested list objects: success")
        tst_list_objs.test_list_objects_pagination({
            "collection-id": self.two_versions_collection_id,
            "max-items": 1
        })
        logger.info("Successfully tested list objects: success with pagination")
        tst_list_objs.test_list_invalid_objects_pagination()
        logger.info("Successfully tested list objects: invalid pagination")
        tst_list_objs.test_list_invalid_objects_pagination_diff_max_items()
        logger.info("Successfully tested list objects: invalid pagination different max items")
        tst_list_objs.test_list_objects_invalid_state_filter()
        logger.info("Successfully tested list objects: invalid state filter")
        tst_list_objs.test_list_objects_invalid_collection_id()
        logger.info("Successfully tested list objects: invalid collection id")
        tst_list_objs.test_list_objects_invalid_content_type()
        logger.info("Successfully tested list objects: invalid content type")
        tst_list_objs.test_list_objects_authentication_error()
        logger.info("Successfully tested list objects: invalid authentication")

    def verify_created_large_object(self, large_obj_inp: dict):
        # Split off from update_large_object to minimize time spend in "verify_" retry loop
        logger.info("### Verifying created large object ###")
        self.check_obj_200_response(large_obj_inp, self.large_folder_object_loop_times)
        logger.info("Successfully verify created large object")

    @staticmethod
    def get_ingestion(ingestion: dict):
        logger.info("### Running Get Ingestion tests ###")
        from Tests.Regression.regression_get_ingestion import TestGetIngestion
        tst_get_ingestion = TestGetIngestion()

        # + Get Ingestion test
        ingestion_prop = ingestion['ingestion']
        ingestion_prop['bucket-name'] = ingestion['multipart-upload']['S3']['Bucket']
        ingestion_prop['upload-id'] = ingestion['multipart-upload']['UploadId']
        tst_get_ingestion.test_get_ingestion(ingestion_prop)
        logger.info("Successfully tested get ingestion")

        # - Get Ingestion tests
        tst_get_ingestion.test_get_ingestion_invalid_ingestion_id()
        logger.info("Successfully tested get ingestion: invalid ingestion id")
        tst_get_ingestion.test_get_ingestion_invalid_content_type()
        logger.info("Successfully tested get ingestion: invalid content type")
        tst_get_ingestion.test_get_ingestion_invalid_x_api_key()
        logger.info("Successfully tested get ingestion: invalid API key")

    def cancel_ingestion(self, ingestion: dict):
        logger.info("### Running Cancel Ingestion tests ###")
        from Tests.Regression.regression_cancel_ingestion import TestCancelIngestion
        tst_cancel_ingestion = TestCancelIngestion()

        # + Cancel Ingestion test
        tst_cancel_ingestion.test_cancel_ingestion(ingestion)
        logger.info("Successfully tested cancel ingestion")

        self.verify_ingestion_state(ingestion, ingestion_status.CANCELLED)
        ingestion["ingestion-state"] = ingestion_status.CANCELLED

        # - Cancel Ingestion tests
        tst_cancel_ingestion.test_cancel_ingestion_invalid_ingestion_id()
        logger.info("Successfully tested cancel ingestion: invalid ingestion id")
        tst_cancel_ingestion.test_cancel_ingestion_invalid_state(ingestion)
        logger.info("Successfully tested cancel ingestion: invalid ingestion state")
        tst_cancel_ingestion.test_cancel_ingestion_invalid_content_type()
        logger.info("Successfully tested cancel ingestion: invalid content type")
        tst_cancel_ingestion.test_cancel_ingestion_invalid_x_api_key()
        logger.info("Successfully tested cancel ingestion: invalid API key")

    @staticmethod
    def update_large_object(update_large_obj_inp: dict):
        logger.info("### Running Update Large Object tests ###")
        from Tests.Regression.regression_update_large_object import TestUpdateLargeObject
        tst_update_large_obj = TestUpdateLargeObject()

        # + Update large object
        update_large_object_content = generate_content('update', 5000000)
        update_large_obj_resp = tst_update_large_obj.test_update_large_object({
            "collection-id": update_large_obj_inp["collection-id"],
            "Content-MD5": generate_hash(update_large_object_content),
            "object-id": update_large_obj_inp["object-id"],
            "owner-id": update_large_obj_inp["owner-id"],
            "asset-id": update_large_obj_inp["asset-id"]
        })
        update_large_obj_resp_props = update_large_obj_resp.json()
        upload_updated_large_object_resp = upload_large_object(update_large_obj_resp_props['large-upload'],
                                                               update_large_object_content)
        if upload_updated_large_object_resp.status_code != 200:
            raise Exception(
                "Large object upload failed; status={}".format(upload_updated_large_object_resp.status_code))
        logger.info("Successfully uploaded updated large object to pre-signed url")

        # - Update large object tests
        tst_update_large_obj.test_update_large_object_invalid_collection_id({
            "collection-id": 'regression-invalid-collection-id',
            "Content-MD5": generate_hash(update_large_object_content),
            "object-id": update_large_obj_inp["object-id"],
            "owner-id": update_large_obj_inp["owner-id"],
            "asset-id": update_large_obj_inp["asset-id"]
        })
        logger.info("Successfully tested update large object: invalid collection id")
        tst_update_large_obj.test_update_large_object_invalid_content_type()
        logger.info("Successfully tested update large object: invalid content type")
        tst_update_large_obj.test_update_large_object_authentication_error()
        logger.info("Successfully tested update large object: invalid authentication")

        return update_large_object_content, update_large_obj_resp_props['object']

    def update_object(self, update_obj_with_id_inp: dict, update_zero_vers_obj_inp: dict):
        logger.info("### Running Update Object tests ###")
        # update_obj_with_id_inp is the same as object_two_vers_with_id_props
        from Tests.Regression.regression_update_object import TestUpdateObject
        tst_update_obj = TestUpdateObject()

        # Validate update object functionality - creates version 2, 3 & 4 of object in two version collection
        # so at the end of this loop V4 will be current version, V3 will be first old and V2 will be second
        # and V1 should not exist.
        update_obj_inp = {
            "collection-id": update_obj_with_id_inp["collection-id"],
            "object-id": update_obj_with_id_inp["object-id"],
            "asset-id": update_obj_with_id_inp["asset-id"]
        }
        for obj_version in range(2, 5):
            # Update existing object creating new versions with each iteration
            updated_body = "updated content coming from create object with object id test {}".format(
                datetime.now().isoformat())
            update_obj_inp["body"] = updated_body
            update_object_resp_props = tst_update_obj.test_update_object(update_obj_inp)

            # Validate version (defaults to current version when no version specified)
            self.verify_updated_object_body(update_object_resp_props.json()["object"], updated_body, obj_version)

            # Save each version of body so it can be verified below
            self.obj_with_id_body_ver.append(updated_body)

        # Re-validate each version and version body
        logger.info("Verifying each version of object's body")
        object_ver_props = {
            "collection-id": update_obj_with_id_inp["collection-id"],
            "object-id": update_obj_with_id_inp["object-id"],
            "asset-id": update_obj_with_id_inp["asset-id"],
        }

        # Validate version 2, 3 & 4 exist and body content is accurate
        for ver in range(2, 5):
            # Here we specify the version to retrieve
            object_ver_props["version-number"] = ver
            get_object_response = get_object(object_ver_props)
            if get_object_response.status_code != 200:
                raise Exception("Unable to retrieve version {} from "
                                "two version collection: status={}".format(ver, get_object_response.status_code))
            if get_object_response.content.decode("utf-8") != self.obj_with_id_body_ver[ver - 1]:
                raise Exception("Unable to retrieve version {} from "
                                "two version collection: status={}".format(ver, get_object_response.status_code))
            logger.info("Successfully verified body of object version {}".format(ver))

        # Validate that version 1 no longer exists
        logger.info("Verifying version #1 has been deleted")
        from Tests.Regression.regression_get_object import TestGetObject
        tst_get_obj = TestGetObject()
        tst_get_obj.test_get_object_version_not_found({
            "collection-id": update_obj_with_id_inp["collection-id"],
            "object-id": update_obj_with_id_inp["object-id"],
            "version-number": 1
        })
        logger.info("Successfully verified all versions of object with id {}".
                    format(update_obj_with_id_inp["object-id"]))

        # Test Object with zero versions to ensure that it updates and does not create old versions
        updated_body = "updated content coming from zero versions object {}".format(
            datetime.now().isoformat())

        # Update the object creating version #2. E1 created in previous tests
        update_obj_zero_versions = tst_update_obj.test_update_object({
            "collection-id": update_zero_vers_obj_inp["collection-id"],
            "body": updated_body,
            "object-id": update_zero_vers_obj_inp["object-id"],
            "asset-id": update_zero_vers_obj_inp["asset-id"]})

        # Validate current version updated correctly
        update_obj_zero_version_props = update_obj_zero_versions.json()["object"]
        self.check_obj_and_body_200_response(update_obj_zero_version_props, updated_body, self.object_loop_times)
        logger.info("Successfully tested update object {}: success".format(update_obj_zero_version_props['object-id']))

        # After update version number should be 2 so retrieving version #1 should return 404 - not found
        update_obj_zero_version_props["version-number"] = 1
        get_object_response = get_object(update_obj_zero_version_props)
        if get_object_response.status_code != 404:
            raise Exception("Version validation failed. GetObject Ver #1 Expected 404(Not Found), actual status={}".
                            format(get_object_response.status_code))
        logger.info("Successfully tested zero version update: version #1 does not exist")

        # Test retrieving current version #2
        update_obj_zero_version_props["version-number"] = 2
        get_object_response = get_object(update_obj_zero_version_props)
        if get_object_response.status_code != 200:
            raise Exception("Error getting current object version #2 in zero version collection: status={}".format(
                get_object_response.status_code))
        logger.info("Successfully tested zero version update: get current version 2 of object")

        # + Update existing object with duplicate body. Verify this does not increment version
        tst_update_obj.test_update_object(update_obj_inp)

        # We need to sleep to make sure that the duplicate body event (that will be ignored) gets processed before we
        # attempt to update the body another time. If these events go in at the same time it is possible the second
        # will be retried leading to a 5 minute delay in processing.
        sleep(self.wait_time)

        # let's update object with different body to confirm version increments when it should
        updated_body = "updated content coming from update existing object with duplicate body test {}".format(
            datetime.now().isoformat())
        update_obj_inp["body"] = updated_body
        update_obj_resp_props = tst_update_obj.test_update_object(update_obj_inp)
        update_obj_props = update_obj_resp_props.json()["object"]

        expected_version = 5
        update_obj_props["version-number"] = expected_version
        self.check_obj_and_body_200_response(update_obj_props, updated_body, self.object_loop_times)
        logger.info("Successfully test update object with duplicate body: version does not increment")

        # + Update object with different content-type
        updated_body = "updated content coming from update object with different content type {}".format(
            datetime.now().isoformat())
        update_obj_inp = {
            "headers": {
                'Content-type': self.default_binary_object_content_type,
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": update_obj_with_id_inp["collection-id"],
            "body": updated_body,
            "object-id": update_obj_with_id_inp["object-id"],
            "asset-id": update_obj_with_id_inp["asset-id"]
        }
        update_object_resp_props = tst_update_obj.test_update_object(update_obj_inp)
        update_obj_props = update_object_resp_props.json()["object"]

        # Validate version, verify content-type changed
        expected_version = 6
        update_obj_props['version-number'] = expected_version
        for j in range(0, self.object_loop_times):
            sleep(self.wait_time)
            get_object_response = get_object(update_obj_props)
            if (get_object_response.status_code == 200
                    and get_object_response.content.decode("utf-8") == updated_body
                    and get_object_response.headers['content-type'] == self.default_binary_object_content_type):
                break
            if j == self.object_loop_times - 1:
                raise Exception("Object content type not updated; content-type={}; status={}".format(
                    get_object_response.headers['content-type'], get_object_response.status_code))
        logger.info("Successfully tested update object {} version #{}: success"
                    .format(update_obj_props['object-id'], expected_version))

        # TODO: Update object with different content-type without changing body (currently 202s and no-ops)

        # - Update object tests
        logger.info("Running invalid update object tests")

        # # Comment explaining why we've commented these out located in regression_update_object.py
        # tst_update_obj.test_update_object_invalid_exceed_payload_limit_lambda({
        #     "collection-id": update_obj_with_id_inp["collection-id"],
        #     "body": generate_content('', 5000000),
        #     "object-id": update_obj_with_id_inp["object-id"]
        # })
        # logger.info("Successfully tested update object: invalid payload limit lambda")
        # tst_update_obj.test_update_object_invalid_exceed_payload_limit_api({
        #     "collection-id": update_obj_with_id_inp["collection-id"],
        #     "body": generate_content('', 10000000),
        #     "object-id": update_obj_with_id_inp["object-id"]
        # })

        logger.info("Successfully tested update object: invalid payload limit api")
        tst_update_obj.test_update_object_invalid_collection_id({
            'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz',
            'object-id': update_obj_with_id_inp["object-id"],
            'body': "file content coming from invalid collection id regression"
        })
        logger.info("Successfully tested update object: invalid collection id")
        tst_update_obj.test_update_object_invalid_collection_state({
            'collection-id': self.suspended_collection_id,
            'object-id': update_obj_with_id_inp["object-id"],
            'body': "file content coming from invalid collection state regression"
        })
        logger.info("Successfully tested update object: invalid collection state")
        tst_update_obj.test_update_object_invalid_metadata_character({
            'collection-id': self.zero_versions_collection_id,
            'object-id': "foobar",
            'body': "regression invalid metadata",
            "headers": {
                'Content-type': self.default_object_content_type,
                'x-api-key': os.environ['X-API-KEY']
            }
        })
        logger.info("Successfully tested update object: invalid object metadata character")
        tst_update_obj.test_update_object_invalid_collection_id({
            'collection-id': 'regression-abcdefghijklmnopqrstuvwxyz',
            'object-id': update_zero_vers_obj_inp["object-id"],
            'body': "file content coming for zero version collection id regression"
        })
        logger.info("Successfully tested update object: invalid zero version collection")
        tst_update_obj.test_update_object_invalid_content_type()
        logger.info("Successfully tested update object: invalid content type")
        tst_update_obj.test_update_object_authentication_error()
        logger.info("Successfully tested update object: invalid authentication")

    def verify_updated_object_body(self, update_obj_props, updated_body, version):
        for j in range(0, self.object_loop_times):
            sleep(self.wait_time)
            get_object_response = get_object(update_obj_props)
            if get_object_response.status_code == 200 and \
                    get_object_response.content.decode("utf-8") == updated_body:
                break
            if j == self.object_loop_times - 1:
                raise Exception("Object not updated; status={}".format(get_object_response.status_code))
        logger.info(
            "Successfully tested update object {} version #{}: success".format(update_obj_props['object-id'], version))

    def update_multipart_object(self, update_multipart_object_input: dict):
        logger.info("### Running Update MultipartObject tests ###")
        from Tests.Regression.regression_update_multipart_object import TestUpdateMultipartObject
        from Tests.Regression.regression_describe_objects import TestDescribeObjects
        test_update_mp_obj = TestUpdateMultipartObject()
        test_describe_obj = TestDescribeObjects()

        # + Update multipart object with new content and content-type
        request_input_multipart_obj = {
            "headers": {
                'Content-type': self.default_binary_object_content_type,
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": update_multipart_object_input["collection-id"],
            "object-id": update_multipart_object_input["object-id"],
            "asset-id": update_multipart_object_input["asset-id"]
        }
        updated_object_body_part = "updated_{}".format(generate_content("multipart_upload", 5242880))
        logger.info('Updating a multipart object: {}'.format(request_input_multipart_obj))
        update_mp_object_resp = test_update_mp_obj.test_update_multipart_object_success(request_input_multipart_obj)
        update_mp_resp_props = update_mp_object_resp.json()
        update_mp_obj_resp_props = update_mp_resp_props['object']
        update_mp_upload_resp_props = update_mp_resp_props['multipart-upload']
        logger.info('Update multipart object response: {}'.format(update_mp_obj_resp_props))

        upload_multipart_object_resp, updated_object_body = upload_multipart_object(update_mp_upload_resp_props,
                                                                                    updated_object_body_part)
        if upload_multipart_object_resp.status_code != 200:
            raise Exception(
                "Multipart upload object failed; status={}".format(upload_multipart_object_resp.status_code))
        logger.info("Successfully uploaded updated multipart object to S3")

        self.check_obj_and_body_200_response(update_mp_obj_resp_props, updated_object_body,
                                             self.large_folder_object_loop_times)
        # check that multipart object content-type updated
        test_describe_obj.test_describe_object({"object-id": update_mp_obj_resp_props["object-id"],
                                                "collection-id": update_mp_obj_resp_props["collection-id"],
                                                "version-number": 2,
                                                "content-type": self.default_binary_object_content_type,
                                                "owner-id": self.owner_id,
                                                "asset-id": self.default_asset_id})
        logger.info("Successfully tested update multipart upload object")

        # - Update multipart object tests
        logger.info("Running invalid update multipart object tests")

        test_update_mp_obj.test_update_multipart_object_invalid_collection_id({
            "collection-id": "regression-abcdefghijklmnopqrstuvwxyz",
            "object-id": update_multipart_object_input["object-id"],
            "stored-content-type": update_multipart_object_input["content-type"]
        })
        logger.info("Successfully tested update multipart object: invalid collection id")

        test_update_mp_obj.test_update_multipart_object_invalid_collection_state({
            "collection-id": self.suspended_collection_id,
            "object-id": update_multipart_object_input["object-id"],
            "stored-content-type": update_multipart_object_input["content-type"]
        })
        logger.info("Successfully tested update multipart object: invalid collection state")

        test_update_mp_obj.test_update_multipart_object_invalid_metadata_character({
            "collection-id": self.zero_versions_collection_id,
            "object-id": "foobar",
            "stored-content-type": self.default_object_content_type,
            "headers": {
                'x-api-key': os.environ['X-API-KEY']
            }
        })
        logger.info("Successfully tested update multipart object: invalid object metadata character")

        test_update_mp_obj.test_update_multipart_object_invalid_content_type()
        logger.info("Successfully tested update multipart object: invalid Content-Type")
        test_update_mp_obj.test_update_multipart_object_invalid_content_type_2()
        logger.info("Successfully tested update multipart object: invalid Content-Type 2")

        test_update_mp_obj.test_update_multipart_object_authentication_error()
        logger.info("Successfully tested update multipart object: invalid authentication")

    def verify_created_object_folder(self, folder_obj_inp: dict):
        from Tests.Regression.regression_create_folder_upload import TestCreateFolderUpload
        logger.info("Verifying that create_object_folder() created object folder")
        for i in range(0, self.large_folder_object_loop_times):
            sleep(self.wait_time)
            describe_object_response = describe_objects(folder_obj_inp)
            if describe_object_response.status_code == 200 and len(describe_object_response.json()['objects']) > 0:
                break
            if i == self.large_folder_object_loop_times - 1:
                raise Exception("Folder Object not created; status={}".format(describe_object_response.status_code))
        logger.info("Successfully verify create_object_folder() created object folder - object-id: {}".format(
            folder_obj_inp["object-id"]))
        self.objects_created.append(folder_obj_inp)

        # - create duplicate folder upload test (this one must go after initial folder object gets created)
        tst_create_folder = TestCreateFolderUpload()
        tst_create_folder.test_create_folder_upload_duplicate_object_id({
            "collection-id": self.two_versions_collection_id,
            "object-id": folder_obj_inp["object-id"]
        })
        logger.info("Successfully tested create folder upload: invalid duplicate object id")

    def update_object_folder(self, update_folder_inp: dict):
        logger.info("### Running Update ObjectFolder tests ###")
        from Tests.Regression.regression_update_folder_upload import TestUpdateFolderUpload
        from Tests.Regression.regression_finish_folder_upload import TestFinishFolderUpload

        # + Update object folder
        tst_update_folder = TestUpdateFolderUpload()
        update_folder_resp = tst_update_folder.test_update_folder_upload_success({
            "collection-id": update_folder_inp["collection-id"],
            "object-id": update_folder_inp["object-id"]
        })
        update_folder_resp_obj = update_folder_resp.json()['folder-upload']

        # + Put to s3
        folder_upload_prop = update_folder_resp.json()["folder-upload"]
        folder_upload_prop['object-id'] = update_folder_resp.json()['object']['object-id']
        upload_to_s3_resp = upload_content_to_s3(folder_upload_prop,
                                                 os.path.join(my_location, 'UpdateFolder'))
        status_code = upload_to_s3_resp["ResponseMetadata"]["HTTPStatusCode"]
        if status_code != 200:
            raise Exception("Folder upload failed; status={}".format(status_code))
        logger.info("Successfully uploaded folder objects to s3")

        # + Confirm that a duplicate request to Update Folder Object will continue to use the same prefix/transaction-id
        extended_folder_transaction = tst_update_folder.test_update_folder_upload_success({
            "collection-id": update_folder_inp["collection-id"],
            "object-id": update_folder_inp["object-id"]
        })
        extended_folder_resp_obj = extended_folder_transaction.json()['folder-upload']

        if extended_folder_resp_obj['S3'].items() != update_folder_resp_obj['S3'].items() or \
                extended_folder_resp_obj['transaction-id'] != update_folder_resp_obj['transaction-id']:
            raise Exception("Folder transaction not extended: Original: \n\t{0}\nSecond Request: "
                            "\n\t{1}".format(extended_folder_transaction, extended_folder_resp_obj))
        logger.info(
            'Folder object transaction successfully extended: {}'.format(extended_folder_resp_obj['transaction-id']))

        # + Finish update folder upload
        tst_finish_folder = TestFinishFolderUpload()
        finish_update_folder_upload_resp = tst_finish_folder.test_finish_folder_upload_success(folder_upload_prop)
        logger.info("Successfully tested finish folder upload: success")

        finish_update_folder_upload_resp_object_props = finish_update_folder_upload_resp.json()
        finish_update_folder_upload_resp_object_props['version-number'] = 2
        for i in range(0, self.large_folder_object_loop_times):
            sleep(self.wait_time)
            describe_object_response = describe_objects(finish_update_folder_upload_resp_object_props)
            if describe_object_response.status_code == 200 and len(describe_object_response.json()['objects']) > 0:
                break
            if i == self.large_folder_object_loop_times - 1:
                raise Exception("Folder Object not updated; status={}".format(describe_object_response.status_code))
        logger.info("Successfully tested update folder upload: success")

        # - Update object folder tests
        tst_update_folder.test_update_folder_upload_invalid_collection_id({
            "collection-id": "regression-abcdefghijklmnopqrstuvwxyz",
            "object-id": update_folder_inp["object-id"]
        })
        logger.info("Successfully tested update folder upload: invalid collection id")
        tst_update_folder.test_update_folder_upload_invalid_collection_state({
            "collection-id": self.suspended_collection_id,
            "object-id": update_folder_inp["object-id"]
        })
        logger.info("Successfully tested update folder upload: invalid collection state")
        tst_update_folder.test_update_folder_upload_invalid_content_type()
        logger.info("Successfully tested update folder upload: invalid content type")
        tst_update_folder.test_update_folder_upload_authentication_error()
        logger.info("Successfully tested update folder upload: invalid authentication")

    def verify_updated_large_object(self, update_large_object_content_inp, update_large_obj_resp_inp):
        # Split off from update_large_object to minimize time spend in "verify_" retry loop
        logger.info("### Verifying that update_large_object() updated large object ###")
        self.check_obj_and_body_200_response(update_large_obj_resp_inp, update_large_object_content_inp,
                                             self.large_folder_object_loop_times)
        logger.info("Successfully tested update object: success large object")

    def remove_object(self, objects):
        logger.info("### Running Remove Object tests ###")
        from Tests.Regression.regression_remove_object import TestRemoveObject
        from Tests.Regression.regression_get_object import TestGetObject
        from Tests.Regression.regression_describe_objects import TestDescribeObjects
        tst_remove_obj = TestRemoveObject()
        tst_get_obj = TestGetObject()
        tst_describe_obj = TestDescribeObjects()

        for object_description, object_dict in objects.items():
            tst_remove_obj.test_remove_object(object_dict)
        logger.info("Successfully tested remove object: Success")

        tst_remove_obj.test_remove_object(objects["object-two-versions-with-metadata"])
        logger.info("Successfully tested remove object that was already removed (no-op): Success")

        # - Remove object tests
        tst_remove_obj.test_remove_object_invalid_collection_id({
            "collection-id": "regression-abcdefghijklmnopqrstuvwxyz",
            "object-id": objects["object-two-versions-with-metadata"]["object-id"]
        })
        logger.info("Successfully tested remove object: invalid collection id")
        tst_remove_obj.test_remove_object_invalid_collection_state({
            "collection-id": self.suspended_collection_id,
            "object-id": objects["object-two-versions-with-metadata"]["object-id"]
        })
        logger.info("Successfully tested remove object: invalid collection state")
        tst_remove_obj.test_remove_object({
            "owner-id": objects["object-two-versions-with-metadata"]["owner-id"],
            "asset-id": objects["object-two-versions-with-metadata"]["asset-id"],
            "collection-id": objects["object-two-versions-with-metadata"]["collection-id"],
            "object-id": "regression-abcdefghijklmnopqrstuvwxyz"
        })
        logger.info("Successfully tested remove object: invalid object id")
        tst_remove_obj.test_remove_object_invalid_content_type()
        logger.info("Successfully tested remove object: invalid content type")
        tst_remove_obj.test_remove_object_authentication_error()
        logger.info("Successfully tested remove object: invalid authentication")

        # + test get and list on removed object
        tst_get_obj.test_get_object_removed(objects["object-two-versions-with-metadata"])
        logger.info("Successfully tested get object: invalid object removed")
        tst_describe_obj.test_describe_object_object_removed(objects["object-two-versions-with-metadata"])
        logger.info("Successfully tested describe object: invalid object removed")
        tst_get_obj.test_get_object_removed_version({
            "owner-id": objects["object-two-versions-with-metadata"]["owner-id"],
            "asset-id": objects["object-two-versions-with-metadata"]["asset-id"],
            "body": objects["object-two-versions-with-metadata"]["body"],
            "content-type": objects["object-two-versions-with-metadata"]["content-type"],
            "collection-id": objects["object-two-versions-with-metadata"]["collection-id"],
            "object-id": objects["object-two-versions-with-metadata"]["object-id"],
            "version-number": 1
        })
        logger.info("Successfully tested get object for removed object: Retrieved version 1 of removed object")
        tst_describe_obj.test_describe_object_object_removed_version({
            "owner-id": objects["object-two-versions-with-metadata"]["owner-id"],
            "asset-id": objects["object-two-versions-with-metadata"]["asset-id"],
            "collection-id": objects["object-two-versions-with-metadata"]["collection-id"],
            "object-id": objects["object-two-versions-with-metadata"]["object-id"],
            "version-number": 1
        })
        logger.info("Successfully tested describe object for remvoved object: Described version 1 removed object")
        tst_get_obj.test_get_object_removed_version_fail({
            "owner-id": objects["object-two-versions-with-metadata"]["owner-id"],
            "asset-id": objects["object-two-versions-with-metadata"]["asset-id"],
            "collection-id": objects["object-two-versions-with-metadata"]["collection-id"],
            "object-id": objects["object-two-versions-with-metadata"]["object-id"],
            "version-number": 2
        })
        logger.info("Successfully tested get object for removed object: invalid object version 2 is removed object")

    def remake_object(self, object_two_vers_with_id_inp, object_binary_inp):
        logger.info("### Running Recreate and Reupsert tests ###")
        from Tests.Regression.regression_create_object import TestCreateObject
        from Tests.Regression.regression_update_object import TestUpdateObject
        tst_create_obj = TestCreateObject()
        tst_update_obj = TestUpdateObject()

        # + Recreate object: same body, same id
        create_removed_obj_resp = tst_create_obj.test_create_object_success(object_two_vers_with_id_inp)
        create_removed_obj_resp_props = create_removed_obj_resp.json()["object"]
        logger.info("Successfully tested create object that was removed")
        create_removed_obj_resp_props["content-type"] = self.default_object_content_type
        create_removed_obj_resp_props["body"] = object_two_vers_with_id_inp["body"]

        # + Reupsert object: same body, same id
        request_input_binary_obj = {
            "headers": {
                'Content-type': object_binary_inp['content-type'],
                'x-api-key': os.environ['X-API-KEY']
            },
            "collection-id": object_binary_inp['collection-id'],
            "asset-id": object_binary_inp['asset-id'],
            "owner-id": object_binary_inp['owner-id'],
            "object-id": object_binary_inp['object-id'],
            "body": object_binary_inp["body"]
        }
        upsert_removed_obj_resp = tst_update_obj.test_update_object(request_input_binary_obj)
        upsert_removed_obj_resp_props = upsert_removed_obj_resp.json()["object"]
        self.check_obj_and_body_200_response(
            upsert_removed_obj_resp_props, object_binary_inp["body"], self.object_loop_times)
        logger.info("Successfully tested upsert object that was removed")
        upsert_removed_obj_resp_props["content-type"] = object_binary_inp["content-type"]
        upsert_removed_obj_resp_props["body"] = object_binary_inp["body"]

        # + Verifying objects were created
        self.check_obj_200_response(create_removed_obj_resp_props, self.object_loop_times)
        self.check_obj_200_response(upsert_removed_obj_resp_props, self.object_loop_times)

        return create_removed_obj_resp_props, upsert_removed_obj_resp_props

    def remove_object_remade(self, remove_obj_removed_then_created_inp, remove_obj_removed_then_upserted_inp):
        logger.info("### Running Remove Recreated and Reupserted Object tests ###")
        from Tests.Regression.regression_remove_object import TestRemoveObject
        tst_remove_obj = TestRemoveObject()

        tst_remove_obj.test_remove_object(remove_obj_removed_then_created_inp)
        tst_remove_obj.test_remove_object(remove_obj_removed_then_upserted_inp)

        for i in range(0, self.object_loop_times):
            sleep(self.wait_time)
            get_object_response = get_object(remove_obj_removed_then_created_inp)
            if get_object_response.status_code == 422:
                break
            if i == self.object_loop_times - 1:
                raise Exception("Object not removed; status={}".format(get_object_response.status_code))
        logger.info("Successfully tested remove object that was removed and created again")

    def verify_object_created_by_ingestion(self, first_ingestion: dict, second_ingestion: dict) -> list:
        logger.info("### Validating the objects created by Ingestion... ###")

        # verify each ingestion was completed
        for ingestion in (first_ingestion, second_ingestion):
            self.verify_ingestion_state(ingestion['ingestion'], ingestion_status.COMPLETED)

        objects = describe_objects(input_dict={"collection-id": self.collection_for_ingestion}).json()["objects"]
        self.assertEqual(len(objects), 3)

        # verify that two objects were created by the second ingestion and one by the first ingestion
        first_ingestion_object_id = None
        second_ingestion_object_ids = []
        versions = set()
        for obj in objects:
            if not first_ingestion_object_id and \
                    obj['version-timestamp'] == first_ingestion['ingestion']['ingestion-timestamp']:
                self._validate_object_content(obj['object-key-url'], obj["object-id"], "first_ingestion_objects")
                first_ingestion_object_id = obj['object-id']
            elif obj['version-timestamp'] == second_ingestion['ingestion']['ingestion-timestamp']:
                self._validate_object_content(obj['object-key-url'], obj["object-id"], "second_ingestion_objects")
                second_ingestion_object_ids.append(obj['object-id'])
                versions.add(obj["version-number"])

        if not first_ingestion_object_id or len(second_ingestion_object_ids) != 2:
            raise Exception("Objects {} not created by the expected Ingestion {}".format(objects, (first_ingestion,
                                                                                                   second_ingestion)))

        # verify that only one of the two objects created by the second ingestion has version number = 2
        self.assertEqual({1, 2}, set(versions))
        logger.info("Successfully created objects by ingestion")
        return second_ingestion_object_ids + [first_ingestion_object_id]

    def _validate_object_content(self, object_key_url: str, object_file_name: str, ingestion_file_path: str) -> None:
        with open(os.path.join(my_location, 'CreateIngestion', ingestion_file_path, object_file_name)) as obj_file:
            expected_content = obj_file.read()
        response = requests.get(object_key_url)
        if response.status_code != 200:
            raise Exception("Unable to get the object content from: {}, status code: {}".format(object_key_url,
                                                                                                response.status_code))
        # Object content from S3 has an additional `\r`
        self.assertEqual(expected_content.split('\n'), response.content.decode("utf-8").split('\r\n'))

    def batch_remove_objects(self, object_ids, collection_id):
        logger.info("### Running Remove Objects tests... ###")
        from Tests.Regression.regression_remove_objects import TestRemoveObjects
        tst_remove_objects = TestRemoveObjects()

        input_dict = {
            "collection-id": collection_id,
            "owner-id": self.owner_id,
            "asset-id": self.default_asset_id,
            "body": {
                "object-ids": object_ids,
                "description": "description for remove objects regression test"
            }
        }
        tst_remove_objects.test_remove_objects(input_dict)

        # - Remove objects tests
        input_dict = {"collection-id": collection_id,
                      "body": {"object-ids": object_ids,
                               "description": "this is an invalid description with more than 100 characters" * 2}}
        tst_remove_objects.test_remove_object_invalid_description_length(input_dict)
        logger.info("Successfully tested batch remove objects: invalid description length")

        input_dict = {"collection-id": 'regression-abcdefghijklmnopqrstuvwxyz', "body": {"object-ids": object_ids}}
        tst_remove_objects.test_remove_object_invalid_collection_id(input_dict)
        logger.info("Successfully tested batch remove objects: invalid collection id")

        input_dict = {"collection-id": self.suspended_collection_id, "body": {"object-ids": object_ids}}
        tst_remove_objects.test_remove_object_invalid_collection_state(input_dict)
        logger.info("Successfully tested batch remove objects: invalid collection state")

        input_dict = {"collection-id": collection_id,
                      "body": {"object-ids": object_ids + ["non_alphanum_object_id_$@"]},
                      "index_non_alphanum_object_id": len(object_ids)}
        tst_remove_objects.test_remove_object_invalid_object_id_in_batch(input_dict)
        logger.info("Successfully tested batch remove objects: non alphanumeric object id in the batch")

        input_dict = {"collection-id": collection_id, "body": {"object-ids": ["obj-id"] * (20000 + 1)}}
        tst_remove_objects.test_remove_object_invalid_batch_size(input_dict)
        logger.info("Successfully tested batch remove objects: invalid batch size")

        tst_remove_objects.test_remove_object_invalid_content_type()
        logger.info("Successfully tested batch remove objects: invalid content type")

        tst_remove_objects.test_remove_object_authentication_error()
        logger.info("Successfully tested batch remove objects: invalid API Key")

        # + Verify Ingestion Objects Removed
        for i in range(0, self.ingestion_loop_times):
            sleep(self.wait_time)

            object_ids_confirmed_removed = []
            for object_id in object_ids:
                get_object_response = get_object({"object-id": object_id, "collection-id": collection_id})
                if get_object_response.status_code == 422:
                    object_ids_confirmed_removed.append(object_id)

            for obj in object_ids_confirmed_removed:
                object_ids.remove(obj)

            if not object_ids:
                break

            if i == self.object_loop_times - 1:
                raise Exception("Object not removed; status={0} "
                                "collectionid={1}".format(get_object_response.status_code, collection_id))
        logger.info("Successfully tested batch remove objects")


if __name__ == '__main__':
    unittest.main()
