import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, publish_sns_topic
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException, \
    NoChangeEventException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, changeset_status, event_handler_status
from lng_datalake_dal.changeset_collection_table import ChangesetCollectionTable
from lng_datalake_dal.changeset_table import ChangesetTable
from lng_datalake_dal.collection_blocker_table import CollectionBlockerTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.tracking_table import TrackingTable

from service_commons import object_event_handler

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module namep
error_handler.lambda_name = getmodulename(stack()[0][1])

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")

# schemas for the notifications to publish
notification_schemas = ['Schemas/Publish/v1/close-changeset.json']


class RetryState:
    INIT = 0
    GET_CHANGEST = 10
    CHANGESET_CLOSING = 20
    CHECK_INFLIGHT = 30
    CHANGESET_CLOSED = 40
    GET_COLLECTIONS = 50
    GET_CATALOGS = 60
    SNS_PUBLISH = 70
    DONE = 99


MESSAGE_RETRY_STATE_KEY = 'retry-state'
MESSAGE_CHANGESET_DATA_KEY = 'changeset-data'
MESSAGE_UPDATED_CHANGESET_DATA_KEY = 'updated-changeset-data'
MESSAGE_COLLECTION_LIST_DATA = 'collections'
MESSAGE_CATALOG_LIST_DATA = 'catalogs'


@session_decorator.lng_aws_session()
@object_event_handler.queue_wrapper
@error_handler.handle
@tracker.track_event_handler(event_names.CHANGESET_CLOSE)
def lambda_handler(event, context):
    logger.info("Event: {}".format(event))
    logger.info("Context: {}".format(vars(context)))
    return close_changeset_event_handler(event, context.invoked_function_arn)


def close_changeset_event_handler(event: dict, lambda_arn: str) -> str:
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    object_event_handler.validate_message(message, event_names.CHANGESET_CLOSE, event_handler_version, lambda_arn)

    additional_message_attributes = message.get('additional-attributes', {})
    # Unload error handlers additional_attributes so we don't write inaccurate retry messages
    error_handler.additional_attributes = additional_message_attributes

    # If retry state not in message default to state INIT
    retry_state = additional_message_attributes.get(MESSAGE_RETRY_STATE_KEY, RetryState.INIT)
    if retry_state != RetryState.INIT:
        retry_state_names = {value: name for name, value in vars(RetryState).items() if name.isupper()}
        logger.info("Retrying state={}".format(retry_state_names[retry_state]))

    if retry_state <= RetryState.GET_CHANGEST:
        existing_changeset_item = get_changeset_item(message['changeset-id'])
        error_handler.additional_attributes[MESSAGE_CHANGESET_DATA_KEY] = existing_changeset_item
        error_handler.additional_attributes[MESSAGE_RETRY_STATE_KEY] = RetryState.CHANGESET_CLOSING
    else:
        existing_changeset_item = additional_message_attributes[MESSAGE_CHANGESET_DATA_KEY]


    if retry_state <= RetryState.CHANGESET_CLOSING:
        changeset_item = generate_changeset_item(existing_changeset_item, message['event-id'])
        update_changeset_item(changeset_item, changeset_status.OPENED)
        error_handler.additional_attributes[MESSAGE_UPDATED_CHANGESET_DATA_KEY] = changeset_item
        error_handler.additional_attributes[MESSAGE_RETRY_STATE_KEY] = RetryState.CHECK_INFLIGHT
    else:
        changeset_item = additional_message_attributes[MESSAGE_UPDATED_CHANGESET_DATA_KEY]

    if retry_state <= RetryState.CHECK_INFLIGHT:
        check_collection_blocker(message['changeset-id'])
        check_tracking(message['changeset-id'])
        error_handler.additional_attributes[MESSAGE_RETRY_STATE_KEY] = RetryState.CHANGESET_CLOSED

    if retry_state <= RetryState.CHANGESET_CLOSED:
        changeset_item['changeset-state'] = changeset_status.CLOSED
        update_changeset_item(changeset_item, changeset_status.CLOSING)
        error_handler.additional_attributes[MESSAGE_UPDATED_CHANGESET_DATA_KEY] = changeset_item
        error_handler.additional_attributes[MESSAGE_RETRY_STATE_KEY] = RetryState.GET_COLLECTIONS

    if retry_state <= RetryState.GET_COLLECTIONS:
        collections = get_collections_from_changeset(message['changeset-id'])
        error_handler.additional_attributes[MESSAGE_COLLECTION_LIST_DATA] = collections
        error_handler.additional_attributes[MESSAGE_RETRY_STATE_KEY] = RetryState.GET_CATALOGS
    else:
        collections = additional_message_attributes[MESSAGE_COLLECTION_LIST_DATA]

    if retry_state <= RetryState.GET_CATALOGS:
        duplicated_catalogs = set()
        for coll in collections:
            duplicated_catalogs.update(publish_sns_topic.get_catalog_ids_by_collection_id(coll))
        # this allows us to deduplicate the catalogs that are shared among common collections
        catalogs = list(duplicated_catalogs)
        error_handler.additional_attributes[MESSAGE_CATALOG_LIST_DATA] = catalogs
        error_handler.additional_attributes[MESSAGE_RETRY_STATE_KEY] = RetryState.SNS_PUBLISH
    else:
        catalogs = additional_message_attributes[MESSAGE_CATALOG_LIST_DATA]

    if retry_state <= RetryState.SNS_PUBLISH:
        sns_publish_data = additional_message_attributes.get(object_event_handler.MESSAGE_SNS_PUBLISH_DATA_KEY, {})
        object_event_handler.send_changeset_notifications(sns_publish_data, changeset_item, notification_schemas,
                                                          message, target_arn, collections, catalogs)

    return event_handler_status.SUCCESS


def get_changeset_item(changeset_id: str) -> dict:
    try:
        changeset_item = ChangesetTable().get_item({'changeset-id': changeset_id}, ConsistentRead=True)
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to get item from Changeset Table. changeset-id: {}, error: {}".format(changeset_id, e))
    if changeset_item['changeset-state'] == changeset_status.EXPIRED:
        raise TerminalErrorException(
            "Changeset {} attempted to be closed in {} state".format(changeset_id, changeset_status.EXPIRED))
    if changeset_item['changeset-state'] != changeset_status.OPENED:
        logger.warning(
            "Duplicate event for changeset-id: {}, state: {}".format(changeset_item['changeset-id'],
                                                                     changeset_item['changeset-state']))
        raise NoChangeEventException(
            "Changeset {} in {} state. No change event".format(changeset_id, changeset_item['changeset-state']))

    return changeset_item


def generate_changeset_item(existing_changeset_item: dict, event_id: str) -> dict:
    changeset_item = {}
    prop = None
    try:

        for prop in ChangesetTable().get_required_schema_keys():
            if prop == 'changeset-state':
                changeset_item[prop] = changeset_status.CLOSING
            elif prop == "event-ids":
                changeset_item[prop] = ChangesetTable().build_event_ids_list(existing_changeset_item,
                                                                             event_id)
            else:
                changeset_item[prop] = existing_changeset_item[prop]
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in ChangesetTable().get_optional_schema_keys():
        if prop in existing_changeset_item:
            changeset_item[prop] = existing_changeset_item[prop]

    return changeset_item


def update_changeset_item(changeset_item: dict, existing_state: str) -> None:
    try:
        ChangesetTable().update_item(changeset_item,
                                     condition='attribute_exists(ChangesetID) and ChangesetState=:state',
                                     expression_values={':state': existing_state})
    except error_handler.retryable_exceptions as ce:
        logger.error(ce)
        raise ce
    except ConditionError as e:
        logger.error("Condition error raised. Changeset item state != to {}".format(existing_state))
        raise TerminalErrorException(
            "Failed to update item in Changeset Table because existing item in incorrect state. "
            "Expected = {0} | Item = {1} | Exception = {2}".format(
                existing_state, changeset_item, e))
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Failed to update item in Changeset Table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(changeset_item, ex))


def check_collection_blocker(changeset_id: str) -> None:
    try:
        collection_blocker = CollectionBlockerTable().query_items(index='changeset-id-index',
                                                                  KeyConditionExpression='ChangesetID=:changeset_id',
                                                                  ExpressionAttributeValues={":changeset_id": {
                                                                      "S": changeset_id}},
                                                                  max_items=1)
    except error_handler.retryable_exceptions as ce:
        logger.error(ce)
        raise ce
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Failed to query Collection Blocker Table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(changeset_id, ex))
    if collection_blocker:
        raise RetryEventException(
            "Changeset still has Collection Blockers. CollectionID: {0}".format(collection_blocker[0]['collection-id']))


def check_tracking(changeset_id: str) -> None:
    try:
        tracking_item = TrackingTable().query_items(index='changeset-id-index',
                                                    KeyConditionExpression='ChangesetID=:changeset_id',
                                                    ExpressionAttributeValues={":changeset_id": {
                                                        "S": changeset_id}},
                                                    max_items=1)
    except error_handler.retryable_exceptions as ce:
        logger.error(ce)
        raise ce
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Failed to query Tracking Table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(changeset_id, ex))
    if tracking_item:
        raise RetryEventException(
            "Changeset still has objects in the tracking table: TrackingID: {0}, RequestTime: {1}".format(
                tracking_item[0]['tracking-id'], tracking_item[0]['request-time']))


def get_collections_from_changeset(changeset_id: str) -> list:
    try:
        return [i['collection-id'] for i in
                ChangesetCollectionTable().query_items(KeyConditionExpression='ChangesetID=:changeset_id',
                                                       ExpressionAttributeValues={":changeset_id": {
                                                           "S": changeset_id}},
                                                       ProjectionExpression='CollectionID')]
    except error_handler.retryable_exceptions as ce:
        logger.error(ce)
        raise ce
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Failed to query Changeset Collection Table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(changeset_id, ex))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CLOSE CHANGESET EVENT HANDLER LAMBDA]")
    asset_group = 'feature-jek'

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/ChangesetEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ['CHANGESET_DYNAMODB_TABLE'] = '{}-DataLake-ChangesetTable'.format(asset_group)
    os.environ['CHANGESET_COLLECTION_DYNAMODB_TABLE'] = '{}-DataLake-ChangesetCollectionTable'.format(asset_group)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{}-DataLake-CollectionBlockerTable'.format(asset_group)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{}-DataLake-TrackingTable'.format(asset_group)
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = '{}-DataLake-CatalogCollectionMappingTable'.format(
        asset_group)
    target_arn = "arn:aws:sns:us-east-1:288044017584:{0}-DataLakeEvents".format(asset_group)

    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")
    publish_sns_topic.WORKING_DIRECTORY = os.path.dirname(__file__)

    msg = {
        "request-time": "2019-10-04T22:30:53.899Z",
        "event-name": event_names.CHANGESET_CLOSE,
        "changeset-id": "a7317516-35e5-43eb-b90b-70f1aa8f6dab",
        "event-id": "b4de6245-b64f-492d-8026-71baad879c16",
        "stage": "LATEST",
        "event-version": 1
    }
    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }


    # Below is used to create a Tracking item as if it came from command lambda
    @session_decorator.lng_aws_session()
    @tracker.track_command(event_names.CHANGESET_CLOSE)
    def track(event, context):
        return {"response": {}}


    track_dict = {
        "context": {
            "client-request-id": msg["event-id"],
            "client-request-time": 1539954955
        },
        "request": {
            "event-name": msg["event-name"],
            "changeset-id": msg["changeset-id"]
        }
    }
    track(track_dict, {})

    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - CLOSE CHANGESET EVENT HANDLER LAMBDA]")
