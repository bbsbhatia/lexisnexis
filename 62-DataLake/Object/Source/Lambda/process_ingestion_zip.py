import logging
import os
import re
import zipfile
from collections import Counter
from concurrent.futures import ThreadPoolExecutor, as_completed
from datetime import datetime, timedelta
from inspect import stack, getmodulename

import orjson
from lng_aws_clients import s3, sns, sqs
from lng_datalake_commons import session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status, ingestion_status
from lng_datalake_dal.collection_blocker_table import CollectionBlockerTable

from service_commons import ingestion, object_common

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
# if __name__ == '__main__':
#     from aws_xray_sdk import global_sdk_config
#     global_sdk_config.set_sdk_enabled(False)
# libraries = ('botocore',)
# patch(libraries)

FANOUT_TOPIC_ARN = os.getenv("INGESTION_FANOUT_NOTIFICATION_TOPIC_ARN")
MAX_FILES_PER_PART = int(os.getenv("MAX_FILES_PER_PART", 100))
MAX_UNCOMPRESSED_FILE_SIZE = int(os.getenv("MAX_UNCOMPRESSED_FILE_SIZE", 700 * 2 ** 20))
MAX_RETRY_COUNT = int(os.getenv("MAX_RETRY_COUNT", 1000))

MESSAGE_RETRY_INDEXES_KEY = 'retry-indexes'

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return process_ingestion_zip(event, context)


def process_ingestion_zip(event: dict, context: object) -> None:
    ingestion_data = None
    collection_blocker_seq = 0
    terminal_error_exception = None

    try:
        message = ingestion.get_message(event)

        try:
            message_received_count = int(event['Records'][0]['attributes']['ApproximateReceiveCount'])
            s3_object = message['Records'][0]['s3']
            bucket_name = s3_object['bucket']['name']
            zip_key = s3_object['object']['key']
            zip_length = s3_object['object']['size']
        except KeyError as e:
            raise TerminalErrorException("Error getting s3 attributes from message||Missing attribute:{}"
                                         .format(e.args[0]))

        additional_message_attributes = message.get('additional-attributes', {})
        # Unload error handlers additional_attributes so we don't write inaccurate retry messages
        error_handler.additional_attributes = additional_message_attributes

        # get stage and ingestion-id from zip key - ingestion/<stage>/<ingestion_id>
        # TODO: Look at refactoring code to handle getting this data from the metadata of the object
        m = re.search(r"ingestion/(.+)/(.+)", zip_key)
        if m:
            stage = m.group(1)
            ingestion_id = m.group(2)
        else:
            raise TerminalErrorException("Cannot determine stage and Ingestion ID from key: {}".format(zip_key))

        ingestion_data = ingestion.get_ingestion_data(ingestion_id, message_received_count <= 20)
        ingestion_state = ingestion_data['ingestion-state']

        allowed_ingestion_states = (ingestion_status.PENDING,
                                    ingestion_status.VALIDATING,
                                    ingestion_status.PROCESSING,
                                    ingestion_status.ERROR)
        if ingestion_state not in allowed_ingestion_states:
            raise TerminalErrorException("Ingestion ID {0} state is not in {1} (state={2})"
                                         .format(ingestion_id, allowed_ingestion_states, ingestion_state))

        # Check for oldest collection blocker; retry if blocker exists that is not for this ingestion-id
        collection_blocker_seq = check_for_collection_blocker(ingestion_data['collection-id'], ingestion_id)
        if collection_blocker_seq != 0:
            raise RetryEventException("Oldest collection blocker for Collection ID {0} is for another request: {1}||"
                                      "Retrying({2})...".format(ingestion_data['collection-id'], ingestion_id,
                                                                collection_blocker_seq))

        # Update ingestion-state to Validating and change pending-expiration-epoch to 30 days from now
        ingestion_data['pending-expiration-epoch'] = int((datetime.now() + timedelta(days=30)).timestamp())
        # Don't want to reset a "Processing" ingestion back to "Validating" because there could be
        # ProcessIngestionZipPart/Objects running for this ingestion and they would fail due to a bad ingestion state
        if ingestion_state != ingestion_status.PROCESSING:
            ingestion.update_ingestion_state(ingestion_data, ingestion_status.VALIDATING)

        # make sure we have something that at least looks like a zip file
        ingestion.validate_zip_magic_number(bucket_name, zip_key)

        zip_list, unread_length, zip_central_directory_length = ingestion.read_zip_central_directory(bucket_name,
                                                                                                     zip_key,
                                                                                                     zip_length)

        content_type, old_object_versions_to_keep, pending_expiration_epoch, collection_hash, changeset_id, \
        changeset_expiration_epoch = read_zip_metadata(bucket_name, zip_key)

        validate_files(zip_list, zip_length, unread_length, ingestion_id)

        # Don't want to reset counters if ingestion state was already set to "Processing"
        if ingestion_state != ingestion_status.PROCESSING:
            ingestion_data['object-count'] = len(zip_list)
            ingestion_data['object-tracked-count'] = 0
            ingestion_data['object-processed-count'] = 0
            ingestion_data['object-updated-count'] = 0
            ingestion_data['object-error-count'] = 0
        ingestion.update_ingestion_state(ingestion_data, ingestion_status.PROCESSING)

        sns_base_message = {
            "ingestion-id": ingestion_id,
            "request-time": ingestion_data['ingestion-timestamp'],
            "bucket-name": bucket_name,
            "zip-key": zip_key,
            "zip-length": zip_length,
            "zip-central-directory-length": zip_central_directory_length,
            "stage": stage,
            "content-type": content_type,
            "old-object-versions-to-keep": old_object_versions_to_keep,
            "pending-expiration-epoch": pending_expiration_epoch,
            "collection-hash": collection_hash,
            'changeset-id': changeset_id,
            'changeset-expiration-epoch': changeset_expiration_epoch
        }
        retry_indexes = additional_message_attributes.get(MESSAGE_RETRY_INDEXES_KEY, [])
        publish_sns_messages(ingestion_id, zip_list, sns_base_message, stage, retry_indexes)

        # Used to guarantee integration test pass and errors aren't ignored.
        return event_handler_status.SUCCESS

    except RetryEventException as e:
        try:
            process_retry_exception(e, event, collection_blocker_seq)
        # will only raise a TerminalErrorException if we run out of retries
        except TerminalErrorException as e2:
            terminal_error_exception = e2

    except (TerminalErrorException, Exception) as e:
        terminal_error_exception = e

    if terminal_error_exception:
        process_terminal_error(terminal_error_exception, event, context, ingestion_data)


def check_for_collection_blocker(collection_id: str, ingestion_id: str) -> int:
    # limit since list can be very large and we only use this to calculate SQS delay which has a max of 900s
    query_limit = 30
    try:
        collection_blocker_items = CollectionBlockerTable().query_items(
            max_items=query_limit,
            ScanIndexForward=True,
            ConsistentRead=True,
            KeyConditionExpression='CollectionID = :collection_id',
            ExpressionAttributeValues={":collection_id": {"S": collection_id}})
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to query items in CollectionBlocker Table||{}".format(e))
    except Exception as ex:
        raise TerminalErrorException("Failed to query items in CollectionBlocker Table||{}".format(ex))

    collection_blocker_index = -1
    for index, collection_blocker_item in enumerate(collection_blocker_items, 0):
        if collection_blocker_item['request-id'] == ingestion_id:
            collection_blocker_index = index
            break
    if collection_blocker_index == -1:
        if len(collection_blocker_items) < query_limit:
            raise TerminalErrorException("Collection blocker not found for Collection ID {}".format(collection_id))
        else:
            collection_blocker_index = query_limit  # just assume the max since this is used for SQS delay calculation
    return collection_blocker_index


def read_zip_metadata(bucket_name: str, zip_key: str) -> tuple:
    s3_client = s3.get_client()

    try:
        response = s3_client.head_object(Bucket=bucket_name, Key=zip_key)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to head S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))

    # required
    try:
        content_type = response['ContentType']
        old_object_versions_to_keep = int(response['Metadata']['old-object-versions-to-keep'])
        collection_hash = response['Metadata']['collection-hash']
    except KeyError as e:
        raise TerminalErrorException("Error getting S3 metadata from {0}||Missing key: {1}"
                                     .format(zip_key, e.args[0]))

    # optional
    pending_expiration_epoch = int(response['Metadata'].get('pending-expiration-epoch', 0))
    changeset_id = response['Metadata'].get('changeset-id', '')
    changeset_expiration_epoch = int(response['Metadata'].get('changeset-expiration-epoch', 0))

    return content_type, old_object_versions_to_keep, pending_expiration_epoch, collection_hash, changeset_id, \
           changeset_expiration_epoch


def validate_files(zip_list: list, zip_length: int, unread_length: int, ingestion_id: str) -> None:
    object_list = []
    for file_info in zip_list:
        if '/' in file_info.filename:
            raise TerminalErrorException("Validation error for Ingestion ID {0}||"
                                         "Archive cannot contain folders"
                                         .format(ingestion_id))
        object_id_error = object_common.validate_object_id(file_info.filename)
        if object_id_error:
            raise TerminalErrorException("Validation error for Ingestion ID {0}||"
                                         "Unsupported file name (Object ID) detected: {1}||"
                                         "{2}"
                                         .format(ingestion_id, file_info.filename, object_id_error))
        object_list.append(file_info.filename)
        if file_info.compress_type not in (zipfile.ZIP_DEFLATED, zipfile.ZIP_STORED):
            raise TerminalErrorException("Validation error for Ingestion ID {0}||"
                                         "Unsupported compression type {1} detected on file: {2}"
                                         .format(ingestion_id, file_info.compress_type, file_info.filename))
        if file_info.flag_bits & 0x01:
            raise TerminalErrorException("Validation error for Ingestion ID {0}||"
                                         "Encrypted file detected: {1}"
                                         .format(ingestion_id, file_info.filename))
        if file_info.header_offset + unread_length > zip_length:
            raise TerminalErrorException("Validation error for Ingestion ID {0}||"
                                         "Header offset out of range on file: {1}"
                                         .format(ingestion_id, file_info.filename))
        if file_info.file_size > MAX_UNCOMPRESSED_FILE_SIZE:
            raise TerminalErrorException("Validation error for Ingestion ID {0}||"
                                         "File size {1} > max allowed size {2}: {3}"
                                         .format(ingestion_id, file_info.file_size, MAX_UNCOMPRESSED_FILE_SIZE,
                                                 file_info.filename))
    # check for duplicate objects in object_list
    duplicate_objects = [k for k, v in Counter(object_list).items() if v > 1]
    if duplicate_objects:
        raise TerminalErrorException("Validation error for Ingestion ID {0}||"
                                     "Duplicate file names (Object IDs) detected: {1}"
                                     .format(ingestion_id, duplicate_objects))

    logger.info("Successfully validated all files in zip file for Ingestion ID {}".format(ingestion_id))


def publish_sns_messages(ingestion_id: str, zip_list: list, message: dict, stage: str, retry_indexes: list) -> None:
    total_file_count = len(zip_list)
    # Initialize client outside of threading
    sns.get_client()

    futures = []
    with ThreadPoolExecutor(max_workers=10) as executor:
        for file_index in range(1, total_file_count + 1, MAX_FILES_PER_PART):
            if retry_indexes and file_index not in retry_indexes:
                continue
            file_count = min(MAX_FILES_PER_PART, total_file_count - file_index + 1)

            message['file-index'] = file_index
            message['file-count'] = file_count

            future = executor.submit(publish_sns_message, orjson.dumps(message).decode(), stage, file_index)
            futures.append(future)

    indexes_to_retry = []
    success_count = 0
    for future in as_completed(futures):
        error_index = future.result()
        if error_index > -1:
            indexes_to_retry.append(error_index)
        else:
            success_count += 1

    if success_count:
        logger.info("Successfully published {0}/{1} messages to {2} for Ingestion ID {3}".format(success_count,
                                                                                                 len(futures),
                                                                                                 FANOUT_TOPIC_ARN,
                                                                                                 ingestion_id))

    if indexes_to_retry:
        indexes_to_retry.sort()
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_INDEXES_KEY, indexes_to_retry)
        raise RetryEventException("Error publishing {0} messages for indexes {1} for Ingestion ID {2}"
                                  .format(len(indexes_to_retry),
                                          indexes_to_retry,
                                          ingestion_id))


def publish_sns_message(message: str, stage: str, index: int) -> int:

    message_attributes = {
        "stage": {
            "DataType": "String",
            "StringValue": stage
        },
        "event": {
            "DataType": "String",
            "StringValue": "part"
        }
    }

    try:
        sns.get_client().publish(TopicArn=FANOUT_TOPIC_ARN,
                           Message=message,
                           MessageAttributes=message_attributes)
    except Exception as e:
        logger.error("Caught exception when publishing SNS message for index {0}||{1}".format(index, e))
        return index

    return -1


def process_retry_exception(e: RetryEventException, event: dict, collection_blocker_seq: int) -> None:
    logger.warning(e)
    sqs_url = ""
    try:
        arn = event['Records'][0]['eventSourceARN'].split(':')
        sqs_url = "https://sqs.{0}.amazonaws.com/{1}/{2}".format(arn[3], arn[4], arn[5])
        seen_count = 0
        if 'seen-count' in event['Records'][0]['messageAttributes']:
            seen_count = int(event['Records'][0]['messageAttributes']['seen-count'].get('stringValue', 0))
            if seen_count + 1 > MAX_RETRY_COUNT:
                raise TerminalErrorException("Event exceeded max retry count of {0}||{1}".format(MAX_RETRY_COUNT, e))

        # make sure we delay at least 5 seconds if collection_blocker_seq=0
        # back off 1 second for every 10 attempts so we don't reach the retry limit too soon
        # 900 seconds is the longest delay supported by SQS
        sqs_delay = max(5, min(collection_blocker_seq * 30 + int(seen_count/10), 900))
        event_body = event['Records'][0]['body']

        # If we have additional attributes, load the sqs body, extract the Message
        # add the additional attributes, and redump the data to overwrite the event_body to send
        if error_handler.additional_attributes:
            json_event_body = orjson.loads(event_body)
            event_message = orjson.loads(json_event_body['Message'])
            event_message['additional-attributes'] = error_handler.additional_attributes
            json_event_body['Message'] = orjson.dumps(event_message).decode()
            event_body = orjson.dumps(json_event_body).decode()

        sqs.get_client().send_message(QueueUrl=sqs_url,
                                      MessageBody=event_body,
                                      MessageAttributes={'seen-count': {
                                          'DataType': 'Number',
                                          'StringValue': str(seen_count + 1)}},
                                      DelaySeconds=sqs_delay)
    except TerminalErrorException as e2:
        raise e2
    # If we throw any exception here just throw the original Retry exception we received.
    except Exception as inner:
        logger.warning("Error sending SQS message back to queue: {0}||{1}".format(sqs_url, inner))
        raise e


def process_terminal_error(e, event, context, ingestion_data):
    if isinstance(e, TerminalErrorException):
        logger.error(e)
    else:
        logger.exception(e)

    uid = ingestion_data['ingestion-id'] if ingestion_data else None
    error_handler.terminal_error(event, is_event=False, uid=uid, error_message=str(e), context=context)

    # update ingestion state to Error
    if ingestion_data and ingestion_data['ingestion-state'] not in (ingestion_status.PROCESSING,
                                                                    ingestion_status.COMPLETED,
                                                                    ingestion_status.ERROR,
                                                                    ingestion_status.CANCELLED):
        ingestion.update_ingestion_state(ingestion_data, ingestion_status.ERROR, error_description=str(e))


if __name__ == '__main__':
    import json
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    ASSET_GROUP = 'feature-jek'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['INGESTION_DYNAMODB_TABLE'] = "{0}-DataLake-IngestionTable".format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = "{0}-DataLake-CollectionBlockerTable".format(ASSET_GROUP)
    os.environ[
        "INGESTION_FANOUT_NOTIFICATION_TOPIC_ARN"] = "arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ProcessIngestionZip-IngestionFanOutTopic-XQX0Y7OOP5QU"

    topic_arn = os.getenv("INGESTION_FANOUT_NOTIFICATION_TOPIC_ARN")

    error_handler.terminal_errors_bucket = "feature-jek-datalake-lambda-terminal-errors-288044017584"

    input_message_dltest = {
        "Records": [
            {
                "eventVersion": "2.1",
                "eventSource": "aws:s3",
                "awsRegion": "us-east-1",
                "eventTime": "2019-03-06T17:03:38.960Z",
                "eventName": "ObjectCreated:CompleteMultipartUpload",
                "userIdentity": {
                    "principalId": "Anonymous"
                },
                "requestParameters": {
                    "sourceIPAddress": "198.176.80.34"
                },
                "responseElements": {
                    "x-amz-request-id": "6DA4447B2898360C",
                    "x-amz-id-2": "2q285qKaysncVfhZzVk6+uPxbBQBGv4Xm8bDlfZ0zTlUJQFgMRyCuDaIIiE/BWKulzUFQIAGZNA="
                },
                "s3": {
                    "s3SchemaVersion": "1.0",
                    "configurationId": "2547c7c3-165d-491e-96b5-80b66d76263a",
                    "bucket": {
                        "name": "feature-jek-dl-object-staging-288044017584-use1",
                        "ownerIdentity": {
                            "principalId": "A390FFG3NM79BK"
                        },
                        "arn": "arn:aws:s3:::feature-jek-dl-object-staging-288044017584-use1"
                    },
                    "object": {
                        "key": "ingestion/LATEST/3b288226-475a-11e9-ab4a-a552a489cae1",
                        "size": 9153942,
                        "eTag": "22f670669d3153af64ef56f400584119-1",
                        "sequencer": "005C7FFD6A03BAA2AA"
                    }
                }
            }
        ]
    }
    input_message_1524439 = {
        "Records": [
            {
                "eventVersion": "2.1",
                "eventSource": "aws:s3",
                "awsRegion": "us-east-1",
                "eventTime": "2019-03-06T17:03:38.960Z",
                "eventName": "ObjectCreated:CompleteMultipartUpload",
                "userIdentity": {
                    "principalId": "Anonymous"
                },
                "requestParameters": {
                    "sourceIPAddress": "198.176.80.34"
                },
                "responseElements": {
                    "x-amz-request-id": "6DA4447B2898360C",
                    "x-amz-id-2": "2q285qKaysncVfhZzVk6+uPxbBQBGv4Xm8bDlfZ0zTlUJQFgMRyCuDaIIiE/BWKulzUFQIAGZNA="
                },
                "s3": {
                    "s3SchemaVersion": "1.0",
                    "configurationId": "2547c7c3-165d-491e-96b5-80b66d76263a",
                    "bucket": {
                        "name": "feature-jek-dl-object-staging-288044017584-use1",
                        "ownerIdentity": {
                            "principalId": "A390FFG3NM79BK"
                        },
                        "arn": "arn:aws:s3:::feature-jek-dl-object-staging-288044017584-use1"
                    },
                    "object": {
                        "key": "ingestion/LATEST/1524439.zip",
                        "size": 7583684882,
                        "eTag": "22f670669d3153af64ef56f400584119-1",
                        "sequencer": "005C7FFD6A03BAA2AA"
                    }
                }
            }
        ]
    }
    input_message_body = {
        "Type": "Notification",
        "MessageId": "b9fcada1-4858-5970-889b-f958b7f3c09f",
        "TopicArn": "arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ObjectStagingBucket-LatestIngestionNotificationTopic-4KMV22O4JQON",
        "Subject": "Amazon S3 Notification",
        "Message": json.dumps(input_message_1524439),
        "Timestamp": "2019-03-06T17:03:39.144Z",
        "SignatureVersion": "1",
        "Signature": "GktMioxYAOyBiJTSooFzUck4SbHNp/JYthypwQNPqb7NhlW7djlNQs9WXoF87EwPM/OJ5IENMH9g1VNH7mpwukbq6aPaGYoGpecaUCjyOPACI7sTn/mjdbnwVrlJENyLs+ZDdheal9yE7fIG5BsYNP75gEehvO8BtuKxt4Iq7tilZi/ia3RyNRM5VcXhkNGhCMGjueTXzLQ1rpnGF+KkChgjTkweOiCEVo/SMR+qPNDZ4gbQ5Wkk/eTjdxyBZLQq15DfnkGSTuXSEXBvZFiGQ0ZYdlymWez0UHGl2OJWh4ncdsqQ416x0FNuN/vc2ub8VzCo4PuHu86hZcfy6w/dmQ==",
        "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem",
        "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ObjectStagingBucket-LatestIngestionNotificationTopic-4KMV22O4JQON:83b9d7b1-9ea0-4ca4-8c42-9a735c53ad80"
    }
    input_event = {
        "Records": [
            {
                "messageId": "4ea95117-fea8-4073-99e3-b10f56f6ca91",
                "receiptHandle": "AQEBAoJDW52N+Lv4RTd4Mp5MCrivHEULAoVaLYzNeTBRwi62hTwQYEp9Ne0AllrS3JbArYkC3MlFebEG3SHhb0GnpyVJ0jwfSxp4F4FIoOCloVKNYw6DwSz8Q1+URrVGrZZg8QIf3xxGn/LQkBvLYtQq9+uI2D63U5TI2oFmC3RzZ5LoRx01dBPS8us2sRDmwa8rumihascg790msmJ9AAgm4vTadLKEvbtOxpx3jaCZU9DIAbvldJbB+T13oWGTowF4pOczFcYty/VJjl5r69oqKBfNjbehpIKol1PPEVDOqsbnrRf/sCBTLrAZi3WtEjL7xSwDSqXicR90EnYKiVaxyaSVk1ROgyWf/wI132+Uruo7lfIWhf/Xfja3l9ev4k+2zC3wy0ix6tJCPNp+1Kv0Cq41AMO8ZRJkytPrwZbo58hvAh200M9oSqkF8Mexxt66GGo4IcWL6eoXeQNVVuQ7sgEMDDOzzHh1YhPqUcvQNkE=",
                "body": json.dumps(input_message_body),
                "attributes": {
                    "ApproximateReceiveCount": "12",
                    "SentTimestamp": "1551753965006",
                    "SenderId": "AIDAIT2UOQQY3AUEKVGXU",
                    "ApproximateFirstReceiveTimestamp": "1551753965006"
                },
                "messageAttributes": {},
                "md5OfBody": "2adf1c719efaf508bbe720464ef53318",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn:aws:sqs:us-east-1:288044017584:feature-jek-DataLake-ProcessInges-LatestIngestionNotificationQueue-4Y3RN3FMJ014",
                "awsRegion": "us-east-1"
            }
        ]
    }

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for _ in range(n)])
    mock_context = MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    lambda_handler(input_event, mock_context)
    logger.debug("Done process_ingestion_zip.")
