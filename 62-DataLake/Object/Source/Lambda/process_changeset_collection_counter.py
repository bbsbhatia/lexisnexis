import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_aws_clients import sqs
from lng_datalake_commons import sqs_extractor, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal import dynamo_mapper
from lng_datalake_dal.changeset_collection_table import ChangesetCollectionTable

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

MAX_RETRY_COUNT = os.getenv("MAX_RETRY_COUNT", 100)
CHANGESET_COLLECTION_COUNTER_RETRY_SQS_URL = os.getenv("RETRY_QUEUE_URL")


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return process_changeset_collection_counter(event, context)


def process_changeset_collection_counter(event: dict, context: dict) -> str:
    records = event["Records"]

    try:
        records_to_process = build_records_to_process(records)

        if records_to_process:
            changeset_collection_counters = build_changeset_collection_counters(records_to_process)
            build_and_process_changeset_collection_items(changeset_collection_counters)

    except (TerminalErrorException, Exception) as e:
        if isinstance(e, TerminalErrorException):
            logger.error(e)
        else:
            logger.exception(e)

        error_handler.terminal_error(records,
                                     is_event=False, error_message=str(e), context=context)

    return event_handler_status.SUCCESS


def build_records_to_process(records: list) -> list:
    records_to_process = []
    for record in records:
        try:
            if record.get('dynamodb', False):
                if record.get('eventName', 'MISSING') == 'INSERT':
                    transformed_record = dynamo_mapper.transform_to_client_dict(record["dynamodb"]["NewImage"])
                    records_to_process.append(transformed_record)
            # check if its a message from retry queue which will have parameter 'body'
            elif record.get('body', False):
                process_retry_message(record)
            else:
                logger.error("Input message not in acceptable format.")
                raise Exception("Input message not in acceptable format.")
        except Exception as e:
            raise TerminalErrorException(
                "Unhandled exception occurred while building records to process. {0}".format(e))

    return records_to_process


def build_changeset_collection_counters(records_to_process: list) -> dict:
    changeset_collection_counters = {}
    for record in records_to_process:
        try:
            changeset_collection_key = "{0}|{1}|{2}".format(record['changeset-id'],
                                                            record['collection-id'],
                                                            record['pending-expiration-epoch'])
            updated_counter_value = changeset_collection_counters.get(changeset_collection_key, 0) + 1
            changeset_collection_counters[changeset_collection_key] = updated_counter_value
        except Exception as e:
            raise TerminalErrorException(
                "Unhandled exception occurred while building changeset collection counters. {0}".format(e))

    return changeset_collection_counters


def build_and_process_changeset_collection_items(changeset_collection_counters: dict) -> None:
    for key, counter in changeset_collection_counters.items():
        changeset_collection_item = {}
        key_parts = key.split('|')
        changeset_collection_item['changeset-id'] = key_parts[0]
        changeset_collection_item['collection-id'] = key_parts[1]
        changeset_collection_item['pending-expiration-epoch'] = key_parts[2]
        changeset_collection_item['objects-processed'] = counter

        try:
            update_changeset_collection_item(changeset_collection_item)
        except error_handler.retryable_exceptions as e:
            logger.error("Error occurred {0}, sending message to retry queue: {1}".format(e, changeset_collection_item))
            retry_message(changeset_collection_item)
        except Exception as e:
            raise TerminalErrorException(
                "Unhandled exception occurred while processing changeset collection items. {0}".format(e))


def process_retry_message(retry_msg: dict) -> None:
    retried_count = int(retry_msg["messageAttributes"]["retry-count"]["stringValue"])
    if retried_count > MAX_RETRY_COUNT:
        logger.error("Max retry limit reached for this message {0}".format(retry_msg))
        raise TerminalErrorException("Max retry limit reached for this message {0}".format(retry_msg))

    retry_count = retried_count + 1

    changeset_collection_item = sqs_extractor.extract_sqs_message(retry_msg)

    try:
        update_changeset_collection_item(changeset_collection_item)
    except error_handler.retryable_exceptions as e:
        logger.error("Error occurred {0}, sending message to retry queue: {1}".format(e, changeset_collection_item))
        retry_message(changeset_collection_item, retry_count)
    except Exception as e:
        raise TerminalErrorException(
            "Unhandled exception occurred while processing changeset collection items. {0}".format(e))


def update_changeset_collection_item(changeset_collection_item: dict) -> None:
    ChangesetCollectionTable().update_counters(changeset_id=changeset_collection_item['changeset-id'],
                                               collection_id=changeset_collection_item['collection-id'],
                                               pending_expiration_epoch=changeset_collection_item[
                                                   'pending-expiration-epoch'],
                                               counter_attributes={'ObjectsProcessed': changeset_collection_item[
                                                   'objects-processed']})


def retry_message(item: dict, retry_count: int = 0) -> None:
    try:
        sqs.get_client().send_message(QueueUrl=CHANGESET_COLLECTION_COUNTER_RETRY_SQS_URL,
                                      MessageBody=orjson.dumps(item).decode(),
                                      MessageAttributes={
                                          'retry-count': {'StringValue': str(retry_count), 'DataType': 'Number'}})
    except Exception as e:
        raise TerminalErrorException(
            "Failed to send retry message {0} to SQS {1} error: {2}".format(CHANGESET_COLLECTION_COUNTER_RETRY_SQS_URL,
                                                                            item, e))

    logger.info("Retry message {0} published to SQS {1}".format(CHANGESET_COLLECTION_COUNTER_RETRY_SQS_URL, item))


if __name__ == '__main__':
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    ASSET_GROUP = 'feature-kgg'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CHANGESET_COLLECTION_DYNAMODB_TABLE'] = "{0}-DataLake-ChangesetCollectionTable".format(ASSET_GROUP)
    CHANGESET_COLLECTION_COUNTER_RETRY_SQS_URL = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-kgg-DataLake-ProcessChang-ProcessChangesetCollectionCounte-98CO6Z2L0T7"

    error_handler.terminal_errors_bucket = "{0}-datalake-lambda-terminal-errors-288044017584".format(ASSET_GROUP)

    input_msg_stream = {
        "Records": [
            {
                "eventID": "8e1d33a5021eec61b87d5f5b9ecde50b",
                "eventName": "INSERT",
                "eventVersion": "1.1",
                "eventSource": "aws:dynamodb",
                "awsRegion": "us-east-1",
                "dynamodb": {
                    "ApproximateCreationDateTime": 1571770094,
                    "Keys": {
                        "ChangesetID": {
                            "S": "1"
                        },
                        "CompositeKey": {
                            "S": "49cf62814f629ba0a679147d8784b516cde30a17|274|2"
                        }
                    },
                    "NewImage": {
                        "ContentType": {
                            "S": "pdf"
                        },
                        "ObjectID": {
                            "S": "49cf62814f629ba0a679147d8784b516cde30a17"
                        },
                        "CollectionHash": {
                            "S": "10"
                        },
                        "ObjectKey": {
                            "S": "49cf62814f629ba0a679147d8784b516cde30a17.gz"
                        },
                        "RawContentLength": {
                            "N": "10"
                        },
                        "VersionNumber": {
                            "N": "2"
                        },
                        "ChangesetID": {
                            "S": "1"
                        },
                        "ObjectState": {
                            "S": "Created"
                        },
                        "BucketName": {
                            "S": "feature-maen-datalake-object-store-288044017584"
                        },
                        "CompositeKey": {
                            "S": "49cf62814f629ba0a679147d8784b516cde30a17|274|2"
                        },
                        "CollectionID": {
                            "S": "274"
                        },
                        "PendingExpirationEpoch": {
                            "N": "1"
                        },
                        "EventID": {
                            "S": "123"
                        },
                        "VersionTimestamp": {
                            "S": "2018-02-07T09:40:59.592Z"
                        }
                    },
                    "SequenceNumber": "33416100000000011233958410",
                    "SizeBytes": 467,
                    "StreamViewType": "NEW_AND_OLD_IMAGES"
                },
                "eventSourceARN": "arn:aws:dynamodb:us-east-1:288044017584:table/feature-kgg-DataLake-ChangesetObjectTable/stream/2019-10-15T16:28:53.091"
            },
            {
                "eventID": "8e1d33a5021eec61b87d5f5b9ecde50b",
                "eventName": "INSERT",
                "eventVersion": "1.1",
                "eventSource": "aws:dynamodb",
                "awsRegion": "us-east-1",
                "dynamodb": {
                    "ApproximateCreationDateTime": 1571770094,
                    "Keys": {
                        "ChangesetID": {
                            "S": "1"
                        },
                        "CompositeKey": {
                            "S": "49cf62814f629ba0a679147d8784b516cde30a17|274|3"
                        }
                    },
                    "NewImage": {
                        "ContentType": {
                            "S": "pdf"
                        },
                        "ObjectID": {
                            "S": "49cf62814f629ba0a679147d8784b516cde30a17"
                        },
                        "CollectionHash": {
                            "S": "10"
                        },
                        "ObjectKey": {
                            "S": "49cf62814f629ba0a679147d8784b516cde30a17.gz"
                        },
                        "RawContentLength": {
                            "N": "10"
                        },
                        "VersionNumber": {
                            "N": "3"
                        },
                        "ChangesetID": {
                            "S": "1"
                        },
                        "ObjectState": {
                            "S": "Created"
                        },
                        "BucketName": {
                            "S": "feature-maen-datalake-object-store-288044017584"
                        },
                        "CompositeKey": {
                            "S": "49cf62814f629ba0a679147d8784b516cde30a17|274|2"
                        },
                        "CollectionID": {
                            "S": "274"
                        },
                        "PendingExpirationEpoch": {
                            "N": "1"
                        },
                        "EventID": {
                            "S": "123"
                        },
                        "VersionTimestamp": {
                            "S": "2018-02-07T09:40:59.592Z"
                        }
                    },
                    "SequenceNumber": "33416100000000011233958410",
                    "SizeBytes": 467,
                    "StreamViewType": "NEW_AND_OLD_IMAGES"
                },
                "eventSourceARN": "arn:aws:dynamodb:us-east-1:288044017584:table/feature-kgg-DataLake-ChangesetObjectTable/stream/2019-10-15T16:28:53.091"
            }
        ]
    }

    input_msg_sqs = {
        "Records": [
            {'messageId': '1d324c62-2a49-48e6-bed6-bc8c4bb856ee',
             'receiptHandle': 'AQEB2p0nmLIP28s3n84aO7Jmpi8CUlU5voVchGnSBOW3OwIwy9KTLI4rBzCQUohFhu1H5QB4GImmDrRc9qajoXzb+C6eVg4m8BKqAOkxz+8JWMqOn25QAoIAcGN769wYuYxReKDXGUq81GaJtd672xq2a0f/PrBvkbLI8Rl3Rcy3kRUEY302pi03+HtDVHvEepdRDF8Un82xU/NKsiespfWBve1G80JiTFWh6FHKAWAi88bW4CXStmuRxiixNCv1vvQYzg+aC0oXWJCqxfxGzlx2cZMPihd3wJaeHSSJoDlndvlqc2mt94K5iStC92RXtu0UV5Gtkf7wewOpBHmGK8yER/JuQ8f71PqaqnSL2O8QIH0/L4NTT4mq89IqvkwQtHeOqvYIeYUIz02gGDEaPBKjT2RxlmWS9B6o7Ytt578c+J75gzHWVOcR8t5eIFJxnkWYZF0WOt0D7hDP6AgIhjnXC9Qz8gfVt5y1zPWosTjaAnE=',
             'body': '{"changeset-id": "2", "collection-id": "277", "objects-processed": 1, "pending-expiration-epoch": "1"}',
             'attributes': {
                 'ApproximateReceiveCount': '1',
                 'SentTimestamp': '1553288123647',
                 'SenderId': 'AROAIUGBALTVOVCLLYE3Y:SeitteMA@legal.regn.net',
                 'ApproximateFirstReceiveTimestamp': '1553288123924'
             },
             'messageAttributes': {"retry-count": {
                 "stringValue": "2",
                 "stringListValues": [],
                 "binaryListValues": [],
                 "dataType": "Number"
             }},
             'md5OfBody': 'fef6ff5cbff8234c7176af60c694c30a',
             'eventSource': 'aws:sqs',
             'eventSourceARN': 'arn',
             'awsRegion': 'us-east-1'
             },
            {'messageId': '1d324c62-2a49-48e6-bed6-bc8c4bb856ee',
             'receiptHandle': 'AQEB2p0nmLIP28s3n84aO7Jmpi8CUlU5voVchGnSBOW3OwIwy9KTLI4rBzCQUohFhu1H5QB4GImmDrRc9qajoXzb+C6eVg4m8BKqAOkxz+8JWMqOn25QAoIAcGN769wYuYxReKDXGUq81GaJtd672xq2a0f/PrBvkbLI8Rl3Rcy3kRUEY302pi03+HtDVHvEepdRDF8Un82xU/NKsiespfWBve1G80JiTFWh6FHKAWAi88bW4CXStmuRxiixNCv1vvQYzg+aC0oXWJCqxfxGzlx2cZMPihd3wJaeHSSJoDlndvlqc2mt94K5iStC92RXtu0UV5Gtkf7wewOpBHmGK8yER/JuQ8f71PqaqnSL2O8QIH0/L4NTT4mq89IqvkwQtHeOqvYIeYUIz02gGDEaPBKjT2RxlmWS9B6o7Ytt578c+J75gzHWVOcR8t5eIFJxnkWYZF0WOt0D7hDP6AgIhjnXC9Qz8gfVt5y1zPWosTjaAnE=',
             'body': '{"changeset-id": "2", "collection-id": "277", "objects-processed": 1, "pending-expiration-epoch": "1"}',
             'attributes': {
                 'ApproximateReceiveCount': '1',
                 'SentTimestamp': '1553288123647',
                 'SenderId': 'AROAIUGBALTVOVCLLYE3Y:SeitteMA@legal.regn.net',
                 'ApproximateFirstReceiveTimestamp': '1553288123924'
             },
             'messageAttributes': {"retry-count": {
                 "stringValue": "100",
                 "stringListValues": [],
                 "binaryListValues": [],
                 "dataType": "Number"
             }},
             'md5OfBody': 'fef6ff5cbff8234c7176af60c694c30a',
             'eventSource': 'aws:sqs',
             'eventSourceARN': 'arn',
             'awsRegion': 'us-east-1'
             }
        ]
    }

    logging.lastResort.setLevel(logging.DEBUG)

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for _ in range(n)])
    mock_context = MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    lambda_handler(input_msg_stream, mock_context)
    # lambda_handler(input_msg_sqs, mock_context)

    logger.debug("Done with process_changeset_collection_counter.")
