import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, changeset_status, event_handler_status
from lng_datalake_dal.changeset_table import ChangesetTable

from service_commons import object_event_handler

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module namep
error_handler.lambda_name = getmodulename(stack()[0][1])

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")

# schemas for the notifications to publish
notification_schemas = ['Schemas/Publish/v1/create-changeset.json']


class RetryState:
    INIT = 0
    INSERT_CHANGESET = 10
    SNS_PUBLISH = 20
    DONE = 99


MESSAGE_RETRY_STATE_KEY = 'retry-state'


@session_decorator.lng_aws_session()
@object_event_handler.queue_wrapper
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {}".format(event))
    logger.info("Context: {}".format(vars(context)))
    return create_changeset_event_handler(event, context.invoked_function_arn)


def create_changeset_event_handler(event: dict, lambda_arn: str) -> str:
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    object_event_handler.validate_message(message, event_names.CHANGESET_OPEN, event_handler_version, lambda_arn)

    additional_message_attributes = message.get('additional-attributes', {})
    # Unload error handlers additional_attributes so we don't write inaccurate retry messages
    error_handler.additional_attributes = additional_message_attributes

    # If retry state not in message default to state INIT
    retry_state = additional_message_attributes.get(MESSAGE_RETRY_STATE_KEY, RetryState.INIT)
    if retry_state != RetryState.INIT:
        retry_state_names = {value: name for name, value in vars(RetryState).items() if name.isupper()}
        logger.info("Retrying state={}".format(retry_state_names[retry_state]))

    changeset_item = generate_changeset_item(message)

    if retry_state <= RetryState.INSERT_CHANGESET:
        put_changeset_item(changeset_item)
        error_handler.additional_attributes[MESSAGE_RETRY_STATE_KEY] = RetryState.SNS_PUBLISH

    if retry_state <= RetryState.SNS_PUBLISH:
        sns_publish_data = additional_message_attributes.get(object_event_handler.MESSAGE_SNS_PUBLISH_DATA_KEY, {})
        object_event_handler.send_changeset_notifications(sns_publish_data, changeset_item, notification_schemas,
                                                          message, target_arn, [], [])

    return event_handler_status.SUCCESS


def generate_changeset_item(message: dict) -> dict:
    changeset_item = {}
    prop = None
    try:

        for prop in ChangesetTable().get_required_schema_keys():
            if prop == 'changeset-timestamp':
                changeset_item[prop] = message['request-time']
            elif prop == 'changeset-state':
                changeset_item[prop] = changeset_status.OPENED
            elif prop == "event-ids":
                changeset_item[prop] = ChangesetTable().build_event_ids_list({}, message['event-id'])
            else:
                changeset_item[prop] = message[prop]
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in ChangesetTable().get_optional_schema_keys():
        if prop in message:
            changeset_item[prop] = message[prop]

    return changeset_item


def put_changeset_item(changeset_item: dict) -> None:
    try:
        ChangesetTable().put_item(changeset_item)
    except error_handler.retryable_exceptions as ce:
        logger.error(ce)
        raise ce
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Failed to put item to Changeset Table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(changeset_item, ex))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE CHANGESET EVENT HANDLER LAMBDA]")

    os.environ["AWS_PROFILE"] = "product-datalake-dev-wormholedeveloper"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/ChangesetEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ['CHANGESET_DYNAMODB_TABLE'] = 'feature-maen-DataLake-ChangesetTable'

    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {
        "request-time": "2019-10-04T22:30:53.899Z",
        "event-name": event_names.CHANGESET_OPEN,
        "owner-id": 109,
        "changeset-id": "rnpVZKOKgHrnHvhD",
        "event-id": "rnpVZKOKgHrnHvhD",
        "stage": "LATEST",
        "pending-expiration-epoch": 1572823854,
        "event-version": 1
    }
    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - CREATE CHANGESET EVENT HANDLER LAMBDA]")
