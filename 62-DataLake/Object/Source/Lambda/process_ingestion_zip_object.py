import hashlib
import io
import logging
import os
import struct
import zlib
from inspect import stack, getmodulename

import orjson
from botocore.config import Config
from botocore.exceptions import ClientError, ReadTimeoutError
from lng_aws_clients import s3, sqs, dynamodb
from lng_datalake_commons import session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_handler_status, ingestion_status, object_status, event_names, ingestion_counter
from lng_datalake_dal.event_store_backend_table import EventStoreBackendTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.object_store_table import ObjectStoreTable
from lng_datalake_dal.tracking_table import TrackingTable
from urllib3.exceptions import ProtocolError

from service_commons import ingestion, object_common

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
# if __name__ == '__main__':
#     from aws_xray_sdk import global_sdk_config
#     global_sdk_config.set_sdk_enabled(False)
# libraries = ('botocore',)
# patch(libraries)

event_version = os.getenv("EVENT_VERSION", 1)
datalake_bucket_name = os.getenv("DATALAKE_BUCKET_NAME")
INGESTION_COUNTER_QUEUE_URL = os.getenv('INGESTION_COUNTER_QUEUE_URL')
MAX_RETRY_COUNT = int(os.getenv("MAX_RETRY_COUNT", 1000))
CONNECTION_TIMEOUT = int(os.getenv("CONNECTION_TIMEOUT", 2))

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

# Set the default connection timeout for dynamodb to 2 seconds rather than 60
client_config = Config(
    connect_timeout=CONNECTION_TIMEOUT,
)


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return process_ingestion_zip_object(event, context)


def process_ingestion_zip_object(event, context):
    ingestion_data = None
    event_id = None
    terminal_error_exception = None

    try:
        message = ingestion.get_message(event)
        dynamodb.get_client(config=client_config)
        sqs.get_client(config=client_config)
        s3.get_client(config=client_config)

        try:
            ingestion_id = message['ingestion-id']
            bucket_name = message['bucket-name']
            zip_key = message['zip-key']
            object_id = message['object-id']
            event_id = message['event-id']
            zip_file_info = message['zip-file-info']
            z_header_offset = zip_file_info['header-offset']
            z_file_compressed_size = zip_file_info['file-compressed-size']
            z_file_size = zip_file_info['file-size']
            z_compression_type = zip_file_info['compression-type']
            z_crc = zip_file_info['crc']
            content_type = message['content-type']
            old_object_versions_to_keep = message['old-object-versions-to-keep']
            pending_expiration_epoch = message['pending-expiration-epoch']
            collection_hash = message['collection-hash']
            stage = message['stage']
            changeset_id = message['changeset-id']
            changeset_expiration_epoch = message['changeset-expiration-epoch']
        except KeyError as e:
            raise TerminalErrorException("Error getting attributes from message||Missing attribute: {}"
                                         .format(e.args[0]))

        ingestion_data = ingestion.get_ingestion_data(ingestion_id)
        ingestion_state = ingestion_data['ingestion-state']

        # allow Completed state b/c we sometimes complete ingestion early due to over-counting of ObjectsProcessedCount
        allowed_ingestion_states = (ingestion_status.PROCESSING, ingestion_status.COMPLETED)
        if ingestion_state not in allowed_ingestion_states:
            raise TerminalErrorException("Ingestion ID {0} state is not in {1} (state={2})||"
                                         "Cannot process Object ID {3}"
                                         .format(ingestion_id,
                                                 allowed_ingestion_states,
                                                 ingestion_state,
                                                 object_id))

        collection_id = ingestion_data['collection-id']

        # make sure this request is for is the oldest Tracking item
        check_tracking_item(object_id, collection_id, event_id)

        file_offset = get_file_offset(bucket_name, zip_key, z_header_offset)

        object_properties = {
            "object-id": object_id,
            "content-type": content_type,
            "version-timestamp": ingestion_data['ingestion-timestamp'],
            "collection-hash": collection_hash,
            "collection-id": collection_id
        }
        object_key, content_sha1, object_hash, body_value = decompress_file(bucket_name, zip_key, object_properties,
                                                                            file_offset,
                                                                            z_file_compressed_size,
                                                                            z_compression_type,
                                                                            z_crc)

        object_store_item = get_object_store_item(object_id, collection_id)
        object_properties['version-number'] = object_store_item.get('version-number', 0) + 1

        duplicated_content = object_common.object_contents_match(object_key, object_hash, content_sha1,
                                                                 object_store_item)
        if not duplicated_content:
            copy_object_to_datalake(body_value, object_key, object_properties, content_sha1)

        if changeset_id:
            # Create the object content inside the changeset-id prefix. If this content is duplicated it must still be
            # placed in the changeset-id prefix even though it will not be placed in the usual collection hash prefix.
            # It will be stored with the object-key of the most recent non-duplicate content request, so on a duplicate
            # request take the object-key from the object_store_item, not the newly generated object-key.
            changeset_object_key = object_common.generate_changeset_object_key(changeset_id,
                                                                               object_key if not duplicated_content else
                                                                               object_store_item['object-key'])
            copy_object_to_datalake(body_value, changeset_object_key,
                                    object_properties if not duplicated_content else object_store_item, content_sha1)

        event_name = get_event_name(object_store_item)
        event_dict = {
            'event-id': event_id,
            'event-name': event_name,
            'request-time': ingestion_data['ingestion-timestamp'],
            'object-id': object_id,
            'collection-id': collection_id,
            'collection-hash': collection_hash,
            'object-key': object_key,
            'raw-content-length': z_file_size,
            'content-type': content_type,
            'content-sha1': content_sha1,
            'object-hash': object_hash,
            'old-object-versions-to-keep': old_object_versions_to_keep,
            'ingestion-id': ingestion_data['ingestion-id'],
            'event-version': event_version,
            'stage': stage
        }
        if pending_expiration_epoch > 0:
            event_dict['pending-expiration-epoch'] = pending_expiration_epoch
        if changeset_id:
            event_dict['changeset-id'] = changeset_id
            event_dict['changeset-expiration-epoch'] = changeset_expiration_epoch

        if create_event_store_item(event_dict):
            update_processed_counter(ingestion_id)
        else:
            remove_tracking_item_if_event_processed(object_id, collection_id,
                                                    ingestion_data['ingestion-timestamp'],
                                                    event_id)

    except RetryEventException as e:
        try:
            process_retry_exception(e, event)
        # will only raise a TerminalErrorException if we run out of retries
        except TerminalErrorException as e2:
            terminal_error_exception = e2

    except (TerminalErrorException, Exception) as e:
        terminal_error_exception = e

    if terminal_error_exception:
        process_terminal_error(terminal_error_exception, event, context, event_id, ingestion_data)

    # Used to guarantee integration test pass and errors aren't ignored.
    return event_handler_status.SUCCESS


def check_tracking_item(object_id: str, collection_id: str, event_id: str) -> None:
    """
    Ensures the incoming event is the oldest in the Tracking table to be processed.
    Warns if there are no tracking items (let EventHandler determine what to do).
    Throws a RetryEventException if not for later processing.
    Note: This logic is similar to the tracker.track_event_handler() decorator.
    :param object_id:
    :param collection_id:
    :param event_id:
    """
    tracking_id = tracker.generate_tracking_id("object", object_id, collection_id)
    try:
        oldest_tracking_item = tracker._get_oldest_item_by_tracking_id(tracking_id)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to query items in Tracking Table||{}".format(e))
    except Exception as e:  # should only be a TerminalErrorException
        raise e
    # log warning if no tracking item (let event handler determine if this is a duplicate event or terminal error)
    if not oldest_tracking_item:
        logger.warning("Tracking record not found for event {}".format(event_id))
    elif not tracker._is_oldest_event(oldest_tracking_item.get('event-id'), event_id):
        raise RetryEventException("Oldest Tracking item {0} is for another request: {1}||"
                                  "Retrying...".format(tracking_id, oldest_tracking_item.get('event-id')))


def get_file_offset(bucket_name, zip_key, header_offset):
    # The local file header is not always the same size as the central directory file header because the extra field
    # can store different information between the 2 headers (zip64 extensions, extended timestamp, etc.). Therefore
    # we must read the local file header to determine its size so we can determine the file offset.

    # read the minimum local file header size (30 bytes) to determine size of the local file header
    try:
        header_body = s3.get_client().get_object(Bucket=bucket_name, Key=zip_key, Range="bytes={0}-{1}"
                                           .format(header_offset, header_offset + 30 - 1))['Body']
    except ClientError as e:
        if e.response['Error']['Code'] == "NoSuchKey":
            raise TerminalErrorException("S3 object {0} not found in bucket {1}".format(zip_key, bucket_name))
        raise RetryEventException("Unable to read S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to read S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))

    header = header_body.read()
    file_name_length, extra_field_length = struct.unpack("<HH", header[26:30])
    local_header_length = 30 + file_name_length + extra_field_length

    return header_offset + local_header_length


def decompress_file(bucket_name, zip_key, object_properties, file_offset, file_compressed_size, file_compression_type,
                    file_crc):

    try:
        file_body = s3.get_client().get_object(Bucket=bucket_name, Key=zip_key, Range="bytes={0}-{1}"
                                         .format(file_offset,
                                                 file_offset + file_compressed_size - 1))['Body']
    except ClientError as e:
        if e.response['Error']['Code'] == "NoSuchKey":
            raise TerminalErrorException("S3 object {0} not found in bucket {1}".format(zip_key, bucket_name))
        raise RetryEventException("Unable to read S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to read S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))

    if file_compression_type == zlib.DEFLATED:
        decompressor = zlib.decompressobj(-15)

    object_id = object_properties['object-id']

    crc = 0
    sha1 = hashlib.sha1()
    f = io.BytesIO()
    # We store entire decompressed document in memory so we are limited to about 750MB for a 1024MB Lambda
    try:
        for chunk in file_body.iter_chunks(4096):
            if file_compression_type == zlib.DEFLATED:
                try:
                    data = decompressor.decompress(chunk)
                except Exception as e:
                    raise TerminalErrorException(
                        "Decompress error on file {0} in {1}||{2}".format(object_id, zip_key, e))
            else:
                data = chunk

            sha1.update(data)
            crc = zlib.crc32(data, crc)

            f.write(data)
    except (ReadTimeoutError, ProtocolError) as e:
        raise RetryEventException("Unable to read S3 object {0}||{1}".format(zip_key, e))

    crc = crc & 0xffffffff
    if crc != file_crc:
        raise TerminalErrorException("CRC error on file {0} in {1}".format(object_id, zip_key))

    # Must keep content_sha1, object_hash and object_key calculations in sync
    # with object_common.build_object_key_and_hashes()
    # content_sha1 = SHA1 of body
    content_sha1 = sha1.hexdigest()

    # object_hash = SHA1 of body + content_type (user_metadata not supported for ingestion)
    sha1.update(object_properties['content-type'].encode())
    object_hash = sha1.hexdigest()

    # object_key = collection_hash + "/" + SHA1 of body + content_type + object_id + version_timestamp
    # (user_metadata not supported for ingestion)
    sha1.update(object_id.encode())
    sha1.update(object_properties['version-timestamp'].encode())
    object_key = "{0}/{1}".format(object_properties['collection-hash'], sha1.hexdigest())

    logger.info("Object ID {0} extracted to {1} from zip file {2}".format(object_id, object_key, zip_key))

    return object_key, content_sha1, object_hash, f.getvalue()


def copy_object_to_datalake(object_body, object_key, object_properties, content_sha1):
    object_destination_buckets = object_common.generate_object_store_buckets(datalake_bucket_name)
    bucket = None
    metadata = object_common.build_object_metadata(object_properties['collection-id'],
                                                   object_properties['object-id'],
                                                   object_properties['version-timestamp'],
                                                   object_properties['version-number'],
                                                   content_sha1,
                                                   {})
    try:
        # TODO: We should multi thread this put object
        for bucket in object_destination_buckets.values():
            logger.info("Copying object into datalake bucket: {0} key: {1}".format(bucket, object_key))
            s3.get_client().put_object(Body=object_body, Bucket=bucket, Key=object_key,
                                       Metadata=metadata,
                                       ContentType=object_properties['content-type'])
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to create S3 object {0} in bucket {1}||{2}".format(object_key, bucket, e))
    except Exception as ex:
        raise TerminalErrorException("Failed to create S3 object {0} in bucket {1}||{2}".format(object_key, bucket, ex))


def get_event_name(object_store_item, large_object=False):
    if object_store_item and object_store_item['object-state'] == object_status.CREATED:
        event_name = event_names.OBJECT_UPDATE if not large_object else event_names.OBJECT_UPDATE_PENDING
    else:
        event_name = event_names.OBJECT_CREATE if not large_object else event_names.OBJECT_CREATE_PENDING

    return event_name


def get_object_store_item(object_id, collection_id):
    try:
        return ObjectStoreTable().get_item({"object-id": object_id,
                                            "collection-id": collection_id},
                                           ConsistentRead=True)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to get item from ObjectStore Table||{}".format(e))
    except Exception as ex:
        raise TerminalErrorException("Failed to get item from ObjectStore Table||{}".format(ex))


def create_event_store_item(event_dict):
    logger.info("Putting event to EventStoreBackend Table")
    logger.debug("EventDict: {0}".format(orjson.dumps(event_dict).decode()))

    try:
        EventStoreBackendTable().put_item(event_dict)
    except ConditionError:
        logger.info("Event already exists in EventStoreBackend Table")
        return False
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to insert row into EventStoreBackend Table||{}".format(e))
    except Exception as e:
        raise TerminalErrorException("Failed to insert row into EventStoreBackend Table||{}".format(e))

    return True


def update_processed_counter(ingestion_id):
    message = {ingestion_id: {ingestion_counter.PROCESSED: 1}}
    logger.debug("Sending ingestion counter to Queue")
    try:
        sqs.get_client().send_message(QueueUrl=INGESTION_COUNTER_QUEUE_URL, MessageBody=orjson.dumps(message).decode())
    except error_handler.retryable_exceptions as e:
        logger.warning("Unable to publish ObjectProcessedCount to SQS {}||{}".format(INGESTION_COUNTER_QUEUE_URL, e))
    except Exception as ex:
        logger.error("Failed to publish ObjectProcessedCount to SQS {}||{}".format(INGESTION_COUNTER_QUEUE_URL, ex))


def remove_tracking_item(tracking_id, request_time, event_id, reason):
    try:
        logger.debug("Removing tracking-id: {0} {1} due to {2}".format(tracking_id, request_time, reason))
        TrackingTable().delete_item({"tracking-id": tracking_id, "request-time": request_time})
    except Exception as e:
        logger.critical("Deleting from tracking table failed for Event ID {0}. Human Intervention is needed!||{1}"
                        .format(event_id, e))


def remove_tracking_item_if_event_processed(object_id, collection_id, version_timestamp, event_id):
    logger.debug("Attempting to remove processed object data")
    object_store_data = get_object_store_item(object_id, collection_id)
    if object_store_data and object_store_data['version-timestamp'] > version_timestamp:
        logger.debug("Removing tracking id for: {0} {1} Duplicate event found".format(object_id, collection_id))
        remove_tracking_item(tracker.generate_tracking_id("object", object_id, collection_id),
                             version_timestamp, event_id, "event already processed")


def process_retry_exception(e: RetryEventException, event: dict) -> None:
    logger.warning(e)
    sqs_url = ""
    try:
        arn = event['Records'][0]['eventSourceARN'].split(':')
        sqs_url = "https://sqs.{0}.amazonaws.com/{1}/{2}".format(arn[3], arn[4], arn[5])
        seen_count = 0
        if 'seen-count' in event['Records'][0]['messageAttributes']:
            seen_count = int(event['Records'][0]['messageAttributes']['seen-count'].get('stringValue', 0))
            if seen_count + 1 > MAX_RETRY_COUNT:
                raise TerminalErrorException("Event exceeded max retry count of {0}||{1}".format(MAX_RETRY_COUNT, e))
        # Back off 1 second for each retry
        sqs_delay = min(120 + seen_count, 900)  # 900 seconds is the longest delay supported by SQS

        sqs.get_client().send_message(QueueUrl=sqs_url,
                                      MessageBody=event['Records'][0]['body'],
                                      MessageAttributes={'seen-count': {
                                          'DataType': 'Number',
                                          'StringValue': str(seen_count + 1)}},
                                      DelaySeconds=sqs_delay)
    except TerminalErrorException as e2:
        raise e2
    # If we throw any exception here just throw the original Retry exception we received.
    except Exception as inner:
        logger.warning("Error sending SQS message back to queue: {0}||{1}".format(sqs_url, inner))
        raise e


def process_terminal_error(e, event, context, event_id, ingestion_data):
    if isinstance(e, TerminalErrorException):
        logger.error(e)
    else:
        logger.exception(e)

    error_handler.terminal_error(event, is_event=False, uid=event_id, error_message=str(e), context=context)

    # update IngestionTable object-error-counter if we have ingestion data
    if ingestion_data:
        ingestion.update_error_counter(ingestion_data['ingestion-id'])
        # Do not remove from Tracking for now since we want to force investigation of terminal errors
        # TODO: Change this function to be called within tracker and handle throttle errors appropriately
        # remove_tracking_item(tracker.generate_tracking_id("object", object_id, ingestion_data['collection-id']),
        #                      ingestion_data['ingestion-timestamp'], event_id, "terminal failure")


if __name__ == '__main__':
    import json
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['INGESTION_DYNAMODB_TABLE'] = "{0}-DataLake-IngestionTable".format(ASSET_GROUP)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = "{0}-DataLake-TrackingTable".format(ASSET_GROUP)
    os.environ['EVENT_STORE_BACKEND_DYNAMODB_TABLE'] = '{0}-DataLake-EventStoreBackendTable'.format(ASSET_GROUP)
    os.environ["DATALAKE_BUCKET_NAME"] = '{0}-dl-object-store-use1'.format(ASSET_GROUP)
    os.environ[
        "INGESTION_COUNTER_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-ajb-DataLake-ProcessIngestionZip-IngestionCounterQueue-FH3Y86NPNYJG"

    INGESTION_COUNTER_QUEUE_URL = os.getenv("INGESTION_COUNTER_QUEUE_URL")
    ingestion.INGESTION_COUNTER_QUEUE_URL = os.getenv("INGESTION_COUNTER_QUEUE_URL")
    datalake_bucket_name = os.getenv("DATALAKE_BUCKET_NAME")

    error_handler.terminal_errors_bucket = "{0}-datalake-lambda-terminal-errors-288044017584".format(ASSET_GROUP)

    input_message_dltest = {
        'ingestion-id': 'dltest.zip',
        'bucket-name': 'feature-jek-dl-object-staging-288044017584-use1',
        'zip-key': 'ingestion/LATEST/dltest.zip',
        'object-id': 'ID0000.3RRM-T610-003C-73HX-00000-00.xml',
        'event-id': 'dltest.zip-138',
        'zip-file-info': {
            'header-offset': 4629491,
            'header-length': 69,
            'file-compressed-size': 30146,
            'file-size': 326279,
            'compression-type': 8,
            'crc': 2863282549
        },
        'content-type': 'application/xml',
        'old-object-versions-to-keep': 5,
        'pending-expiration-epoch': 0,
        'collection-hash': 'abcdefg',
        'stage': 'LATEST'
    }
    input_message_1524439 = {
        "ingestion-id": "1524439.zip",
        "bucket-name": "feature-jek-dl-object-staging-288044017584-use1",
        "zip-key": "ingestion/LATEST/1524439.zip",
        "content-type": "application/xml",
        "old-object-versions-to-keep": 5,
        "pending-expiration-epoch": 0,
        "collection-hash": "abcdefg",
        "stage": "LATEST",
        "zip-file-info": {
            "header-offset": 92173911,
            "header-length": 58,
            "file-compressed-size": 3338,
            "file-size": 10802,
            "compression-type": 8,
            "crc": 2923103768
        },
        "object-id": "3SCX-5VP0-000M-Y4TC-00000-00",
        "event-id": "1524439.zip-11021"
    }
    input_message_aaron = {
        "ingestion-id": "1524439.zip",
        "bucket-name": "feature-jek-dl-object-staging-288044017584-use1",
        "zip-key": "ingestion/LATEST/1524439.zip",
        "content-type": "application/xml",
        "old-object-versions-to-keep": 5,
        "pending-expiration-epoch": 0,
        "collection-hash": "abcdefg",
        "stage": "LATEST",
        "zip-file-info": {
            "header-offset": 7503370029,
            "header-length": 70,
            # "header-length": 58,
            "file-compressed-size": 108041,
            "file-size": 554260,
            "compression-type": 8,
            "crc": 747553958
        },
        "object-id": "5C85-71K0-00H6-R3GS-00000-00",
        "event-id": "1524439.zip-1000000"
    }
    input_message_aaron2 = {
        "ingestion-id": "1524439.zip",
        "bucket-name": "feature-jek-dl-object-staging-288044017584-use1",
        "zip-key": "ingestion/LATEST/1524439.zip",
        "content-type": "application/xml",
        "old-object-versions-to-keep": 5,
        "pending-expiration-epoch": 0,
        "collection-hash": "abcdefg",
        "stage": "LATEST",
        "zip-file-info": {
            "header-offset": 4296985748,
            "header-length": 58,
            "file-compressed-size": 5449,
            "file-size": 19615,
            "compression-type": 8,
            "crc": 3786091320
        },
        "object-id": "4KR8-P9B0-006W-80HV-00000-00",
        "event-id": "1524439.zip-482987"
    }
    input_message_body = {
        "Type": "Notification",
        "MessageId": "2d20f54a-44e2-552c-ae8c-a04907debca2",
        "TopicArn": "arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ProcessIngestionZip-IngestionFanOutTopic-XQX0Y7OOP5QU",
        "Message": json.dumps(input_message_aaron),
        "Timestamp": "2019-03-13T17:42:20.953Z",
        "SignatureVersion": "1",
        "Signature": "V6aWacWCs5PP4iGdF15HEmEcudcFynrI9WnI2Ejf9LfLVp0Zi5CeBkVfcn57IrBlRPwanh7M6NmeBSPqW8IPsLj5M6o5dZ2xlvAU29m5t7p6hrDHgJlybIZ6IWvhp5LDBUss9yyqBS0l3Iyu525d/zQSoCDXxQVzSn1bDsrDmbXYMo3VkNaocYdzR1KdP8CKCzQRoAD1IFQAhr47Pkg2RfwixQvkPq9N1dieS+haXN/kBFpzxIEVNWRtUx7rEBXaJy2yx51VOZgycqcqjNSWpj87K3Vqgv5fklMpo0db/5PEvVbYxvEWQW8UruAMZCGgV7j7tCORKwB0Cejgnqm5Cg==",
        "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem",
        "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ProcessIngestionZip-IngestionFanOutTopic-XQX0Y7OOP5QU:a4cb0a1b-73cd-4d8d-a1b4-84222c04a7f5",
        "MessageAttributes": {
            "stage": {
                "Type": "String",
                "Value": "LATEST"
            },
            "event": {
                "Type": "String",
                "Value": "object"
            }
        }
    }
    input_event = {
        "Records": [
            {
                "messageId": "2fd36404-71fb-4225-8dd1-3b50c8f2949a",
                "receiptHandle": "AQEBm9ohn//guiILMhXlIiUhvWz2GNpa5YujcYrM3jCQGxBzHXdTbA2oE15Vhhxzl8NBbWXNho78/qtnIawYj4LDyF8guXypRBrPAr5cJw0f0rGDW/ZfbtSXHo66mLqRe6kap+RBru00uGRxZE8Fv7wnbtxrmaXQN4NoiTScuGkKX5537ylqlZmm7xS+Zdub67fjxQzttXL/MWlYfXxOhKLOw9A8Eq36YlVmujB/3fejo8gi3fa1SuWvIKngpeWQGPDmZwZCldyfdECVlGYadZLuXIOXTl9EBHyokwuxInl8ikF0Tj0mkb/fwTlr6F/8rp9CLttqTwiAgoA5pNeNFzNdhvieWnHXqUhoacJuy0mvOs4gjt7YS6lu0VDSsWFKgevVlhSUImHo8MZ0K6NJ5ZrdWk0Vtjn/79CzagfrwA15n7DuAyIDB7Vyv+yChA0MJFb/UORplRunporeOU3tcqb2/fCPnmWRDXMqzUcawIpG+YY=",
                "body": json.dumps(input_message_body),
                "attributes": {
                    "ApproximateReceiveCount": "1",
                    "SentTimestamp": "1552498941001",
                    "SenderId": "AIDAIT2UOQQY3AUEKVGXU",
                    "ApproximateFirstReceiveTimestamp": "1552498941002"
                },
                "messageAttributes": {},
                "md5OfBody": "eea7e083664e2d14786f6a8b5e82db04",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn:aws:sqs:us-east-1:288044017584:feature-jek-DataLake-ProcessIngestionZi-LatestIngestionObjectQueue-1NWVUAQ0TIS4P",
                "awsRegion": "us-east-1"
            }
        ]
    }

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for _ in range(n)])
    mock_context = MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    lambda_handler(input_event, mock_context)
    logger.debug("Done process_ingestion_zip_object.")
