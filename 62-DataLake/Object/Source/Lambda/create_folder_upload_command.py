import logging
import os

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, NotAllowedError
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_constants import event_names
from lng_datalake_constants import object_status

from service_commons import object_command, object_common

__author__ = "John Konderla, Jose Molinet, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")
staging_bucket_arn = os.getenv("DATALAKE_STAGING_BUCKET_ARN")
assume_role_arn = os.getenv("ROLE_ARN")
folder_prefix = os.getenv("FOLDER_PREFIX", 'folder-upload')
policy_duration = int(os.getenv('POLICY_DURATION', '3600'))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/create_folder_upload_command-RequestSchema.json',
                         'Schemas/create_folder_upload_command-ResponseSchema.json', event_names.OBJECT_CREATE_FOLDER)
def lambda_handler(event, context):
    return create_folder_upload_command(event['request'], event['context']['client-request-id'],
                                        event['context']['stage'],
                                        event['context']['api-key-id'])


def create_folder_upload_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    collection_id = request['collection-id']

    # validate object id
    object_id = request['object-id']
    object_command.validate_object_id(object_id)

    # validate collection exists and it's in proper state
    collection_response = object_command.get_collection_response(collection_id)
    owner_id = collection_response['owner-id']

    # owner authorization
    owner_authorization.authorize(owner_id, api_key_id)

    # extract changeset if it exists
    if 'changeset-id' in request:
        changeset = object_command.validate_and_get_changeset_id(request['changeset-id'], collection_response['owner-id'])
    else:
        changeset = {}

    # get object from the Object Store Table if exist
    object_item = object_command.get_object_item(object_id, collection_id)
    object_state = object_item.get('object-state', '')

    active_event_id, staging_prefix, credentials = process_event(request_id, object_state, object_id, collection_id)

    pending_expiration_epoch = object_common.generate_object_expiration_time(collection_response)
    transaction_id = object_common.build_folder_object_transaction_id(active_event_id, collection_id, owner_id, stage,
                                                                      changeset)

    response_dict = {
        "response-dict": {
            'folder-upload': generate_folder_response(transaction_id, staging_prefix, credentials),
            'object': generate_object_response(collection_response, object_id, pending_expiration_epoch, stage,
                                               changeset.get('changeset-id', ''))
        }
    }

    return response_dict


def process_event(request_id: str, object_state: str, object_id: str, collection_id: str) -> tuple:
    """
    Update or remove the event from the Tracking Table based on the previous pending event (if exist)
    or the object state (if object exist). Also, add an expiration epoch (TTL) to the event item
    :param request_id: Request ID
    :param object_state: Object State
    :param object_id: Object ID
    :param collection_id: Collection ID
    """
    previous_event, current_event = object_command.get_previous_and_current_events(request_id, object_id, collection_id)
    previous_event_name = previous_event.get('event-name', '')

    # if the previous event is CREATE FOLDER and has a TTL, extend it's TTL and remove current event
    if previous_event_name == event_names.OBJECT_CREATE_FOLDER and 'pending-expiration-epoch' in previous_event:
        extend_session = True
        event_id = previous_event['event-id']
    # don't extend the previous event TTL, instead add a TTL to the current event if: 1- The previous event is REMOVE.
    # 2- There is no previous event, but the object state is Removed or the object doesn't exist.
    elif previous_event_name == event_names.OBJECT_REMOVE or (
            not previous_event_name and object_state != object_status.CREATED):
        extend_session = False
        event_id = request_id
    else:
        raise NotAllowedError("Object ID {} in Collection ID {} already exists or there is a pending request".format(
            object_id, collection_id), "Duplicate requests not allowed")

    staging_prefix = object_common.build_staging_prefix(folder_prefix, collection_id, event_id, object_id)

    credentials = object_command.generate_sts_role_token(staging_bucket_arn, staging_prefix, assume_role_arn,
                                                         policy_duration, request_id)
    if extend_session:
        object_command.extend_event_ttl(previous_event, current_event)
    return event_id, staging_prefix, credentials


def generate_folder_response(transaction_id: str, staging_prefix: str, credentials: dict) -> dict:
    folder_dict = {}
    prop = None

    try:
        folder_default_properties = {
            "transaction-id": transaction_id,
            "S3": {
                "Bucket": staging_bucket_name,
                "Prefix": staging_prefix,
            },
            "Credentials": credentials
        }
        # required upload properties
        for prop in command_wrapper.get_required_response_schema_keys('folder-upload'):
            folder_dict[prop] = folder_default_properties[prop]

    # Currently an unreachable exception as we provide defaults for all the required properties but this could save us
    # in the future.
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    return folder_dict


def generate_object_response(input_dict: dict, object_id: str, pending_expiration_epoch: int, stage: str,
                             changeset_id: str) -> dict:
    object_dict = {}
    prop = None

    try:

        default_properties = {
            "object-id": object_id,
            "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, object_id, input_dict['collection-id']),
            "collection-url": "/collections/{0}/{1}".format(stage, input_dict['collection-id']),
            "object-state": object_status.PENDING
        }
        # required object properties
        for prop in command_wrapper.get_required_response_schema_keys('object-properties'):
            if prop in default_properties:
                object_dict[prop] = default_properties[prop]
            else:
                object_dict[prop] = input_dict[prop]

    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('object-properties'):
        if prop == 'object-expiration-date' and pending_expiration_epoch:
            object_dict[prop] = time_helper.format_time(pending_expiration_epoch)
        elif prop == 'changeset-id' and changeset_id:
            object_dict[prop] = changeset_id
        elif prop in input_dict:
            object_dict[prop] = input_dict[prop]

    return object_dict


if __name__ == '__main__':
    import random
    import string
    from datetime import datetime
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE UPLOAD COMMAND LAMBDA]")
    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['DATALAKE_STAGING_BUCKET_NAME'] = '{}-dl-object-staging-use1'.format(ASSET_GROUP)
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{}-DataLake-OwnerTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{}-DataLake-TrackingTable'.format(ASSET_GROUP)
    os.environ['CHANGESET_DYNAMODB_TABLE'] = '{}-DataLake-ChangesetTable'.format(ASSET_GROUP)
    os.environ['ROLE_ARN'] = 'arn:aws:iam::288044017584:role/ADFS-WormholeDeveloper'

    staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")
    assume_role_arn = os.getenv('ROLE_ARN')


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-type": "aws-apigateway",
                "stage": "LATEST",
                "client-id": "abcd-client-id-7890",
                "http-method": "POST",
                "resource-id": "RESOURCE ID",
                "resource-path": "RESOURCE PATH",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": 'AARON_2',
                "object-id": "1010101",
                #'changeset-id': "jek-changeset"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE UPLOAD COMMAND LAMBDA]")
