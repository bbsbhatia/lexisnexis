import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import sns_extractor, session_decorator, publish_sns_topic
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import object_status, event_names, event_handler_status
from lng_datalake_dal.object_store_table import ObjectStoreTable

from service_commons import object_event_handler, object_common

__author__ = "Shekhar Ralhan, Mark Seitter, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

# Set environment variables
target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
datalake_bucket_name = os.getenv("DATALAKE_BUCKET_NAME")
primary_object_store_region = os.getenv("PRIMARY_BUCKET_REGION", 'us-east-1')

# schemas for the notifications to publish
notification_schemas = ['Schemas/Publish/v0/update-object.json', 'Schemas/Publish/v1/update-object.json']

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


# Retry states - additional attribute saved to event message on retry so that processing can resume where it left off
class RetryState:
    INIT = 0
    OST_GET = 1
    S3_COPY = 2
    OST_UPDATE = 3
    OSVT_PUT = 4
    OSVT_DELETE = 5
    SNS_PUBLISH = 6
    INGESTION_UPDATE = 7
    CHANGESET_UPDATE = 8
    DONE = 99


MESSAGE_RETRY_STATE_KEY = 'retry-state'
MESSAGE_OBJECT_DATA_KEY = 'object-data'


@session_decorator.lng_aws_session()
@object_event_handler.queue_wrapper
@error_handler.handle
@tracker.track_event_handler(event_names.OBJECT_UPDATE)
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return update_object_event_handler(event, context.invoked_function_arn)


def update_object_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """

    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    object_event_handler.validate_message(message, event_names.OBJECT_UPDATE, event_handler_version, lambda_arn)

    additional_message_attributes = message.get('additional-attributes', {})
    # Unload error handlers additional_attributes so we don't write inaccurate retry messages
    error_handler.additional_attributes = additional_message_attributes

    # If retry state not in message default to state OST_INIT
    retry_state = additional_message_attributes.get(MESSAGE_RETRY_STATE_KEY, RetryState.INIT)
    if retry_state != RetryState.INIT:
        retry_state_names = {value: name for name, value in vars(RetryState).items() if name.isupper()}
        logger.info("Retrying state={}".format(retry_state_names[retry_state]))

    # Getting catalog ids before updating the dynamo db to keep it consistent across all event handlers.
    # If we get catalog ids after updating dynamo db and we get throttled then in some cases we will miss publishing
    # to those catalogs.
    catalog_ids = publish_sns_topic.get_catalog_ids_by_collection_id(message['collection-id'])

    object_destination_buckets = object_common.generate_object_store_buckets(datalake_bucket_name)

    if retry_state <= RetryState.OST_GET:
        existing_object_store_item = object_event_handler.get_consistent_object_item(message["object-id"],
                                                                                     message["collection-id"])
        # For large object if there is a Create and Update request in flight and user only completes Update
        # request without completing Create request then there won't be an item in OST.
        # In this case we reclassify the event name to Object::Create and retry
        if not existing_object_store_item:
            error_handler.reclassify_event_name(event_names.OBJECT_CREATE)
            raise RetryEventException("Object does not exist. "
                                      "Retry by re-classing the event to {}".format(event_names.OBJECT_CREATE))

        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.S3_COPY)
        error_handler.set_message_additional_attribute(MESSAGE_OBJECT_DATA_KEY, existing_object_store_item)
    else:
        existing_object_store_item = additional_message_attributes.get(MESSAGE_OBJECT_DATA_KEY, {})

    # Generate these before we do any DB calls to verify we have enough data to conform to both schemas
    object_store_item = generate_object_store_item(message, existing_object_store_item, object_destination_buckets)
    object_store_version_item = object_event_handler.generate_object_store_version_item(object_store_item,
                                                                                        message['event-id'])

    if retry_state <= RetryState.S3_COPY:
        # TODO: change to require object-hash and content-sha1 after initial release
        if object_common.object_contents_match(message['object-key'],
                                               message.get('object-hash'),
                                               message.get('content-sha1'),
                                               existing_object_store_item):
            logger.info("Updated object content matches the existing object")
            # Since the object content matched we need to use existing ObjectStore item for publishing of
            # Object:UpdateNoChange events and for information used in the ChangesetObject table
            object_store_item = existing_object_store_item
            # Change event-name to no change
            message['event-name'] = event_names.OBJECT_UPDATE_NO_CHANGE

            # If there is duplicated content we want to only move the object if there is a changeset-id
            # in this case we will copy the object into the changeset-id prefix using the existing object's object-key
            object_event_handler.process_staging_object(message, object_destination_buckets,
                                                        object_store_item['version-number'],
                                                        object_store_item['object-key'])

            # Skip to SNS_PUBLISH because we don't need to update the OST or put a new version in the OSVT
            retry_state = RetryState.SNS_PUBLISH
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.SNS_PUBLISH)
        else:
            # The object content does not match the existing object store record so we should move the object to the
            # collection-hash prefix
            object_event_handler.process_staging_object(message, object_destination_buckets,
                                                        object_store_item['version-number'])
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.OST_UPDATE)

    # Make dynamodb ost update, osvt insert, and osvt delete
    # sets retry state to ingestion update if we have no object changes such as a duplicate event
    retry_state = update_tables(retry_state, object_store_item, object_store_version_item, message)

    if retry_state <= RetryState.SNS_PUBLISH:
        sns_publish_data = additional_message_attributes.get(object_event_handler.MESSAGE_SNS_PUBLISH_DATA_KEY, {})
        object_event_handler.send_object_notifications(sns_publish_data, object_store_item, notification_schemas,
                                                       message, target_arn, catalog_ids,
                                                       message.get('changeset-id', ''))
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.INGESTION_UPDATE)

    if retry_state <= RetryState.INGESTION_UPDATE:
        object_event_handler.update_ingestion(message)
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.CHANGESET_UPDATE)

    if retry_state <= RetryState.CHANGESET_UPDATE:
        object_event_handler.changeset_update(object_store_item, message)
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.DONE)

    # Used to guarantee integration test pass and errors aren't ignored.
    return event_handler_status.SUCCESS


def generate_object_store_item(message: dict, existing_object: dict, destination_buckets: dict) -> dict:
    new_dict = {}
    prop = None
    try:
        default_properties = {
            "object-state": object_status.CREATED,
            "bucket-name": destination_buckets[primary_object_store_region],
            "version-number": existing_object['version-number'] + 1,
            "version-timestamp": message['request-time']
        }

        for prop in ObjectStoreTable().get_required_schema_keys():
            if prop in default_properties:
                new_dict[prop] = default_properties[prop]
            # TODO: remove next 2 lines after initial release
            elif prop == 'collection-hash' and 'collection-hash' not in message:
                # calculate collection-hash since it is not in incoming message
                new_dict[prop] = object_common.build_collection_id_hash(message['collection-id'])
            elif prop == "event-ids":
                new_dict[prop] = ObjectStoreTable().build_event_ids_list(existing_object, message['event-id'])
            else:
                new_dict[prop] = message[prop]
    except Exception:
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in ObjectStoreTable().get_optional_schema_keys():
        if prop == 'replicated-buckets':
            new_dict[prop] = [bucket for bucket in destination_buckets.values() if bucket != new_dict['bucket-name']]
        elif prop in message:
            new_dict[prop] = message[prop]

    return new_dict


def update_tables(retry_state: int, object_store_item: dict, object_store_version_item: dict, message: dict) -> int:
    # Must perform OST_UPDATE prior to OSVT_PUT/DELETE so we know if object changes
    # Note: safe because Get/ListObject read don't read the OSVT if current version
    if retry_state <= RetryState.OST_UPDATE:
        is_condition_failure = object_event_handler.update_object_store_table(object_store_item)
        # Skip to ingestion update if version number does not match expected (we don't want to go backwards in time)
        if is_condition_failure:
            retry_state = RetryState.INGESTION_UPDATE
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.INGESTION_UPDATE)
        else:
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.OSVT_PUT)

    if retry_state <= RetryState.OSVT_PUT:
        object_event_handler.insert_object_store_version_table(object_store_version_item)
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.OSVT_DELETE)

    if retry_state <= RetryState.OSVT_DELETE:
        object_event_handler.delete_object_store_version_table(object_store_version_item,
                                                               message['old-object-versions-to-keep'])
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.SNS_PUBLISH)

    return retry_state


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE OBJECT EVENT HANDLER LAMBDA]")

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_VERSION_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreVersionTable'.format(ASSET_GROUP)
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = '{0}-DataLake-CatalogCollectionMappingTable'.format(
        ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{0}-DataLake-TrackingTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)

    os.environ["DATALAKE_BUCKET_NAME"] = '{0}-dl-object-store'.format(ASSET_GROUP)
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = '{0}-datalake-lambda-terminal-errors-288044017584'.format(
        ASSET_GROUP)

    os.environ["SUBSCRIPTION_NOTIFICATION_TOPIC_ARN"] = "arn:aws:sns:us-east-1:288044017584:{0}-DataLakeEvents".format(
        ASSET_GROUP)
    os.environ[
        "RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-jek-DataLake-RetryQueues-RetryQueue-1K2VM44Y6M500"

    target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
    datalake_bucket_name = os.getenv("DATALAKE_BUCKET_NAME")
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")

    publish_sns_topic.WORKING_DIRECTORY = os.path.dirname(__file__)
    object_common.DATA_LAKE_URL = "https://datalake-{0}.content.aws.lexis.com".format(ASSET_GROUP)

    input_message = {"event-id": "DqxpEhKJGGUhuEqe", "request-time": "2019-08-20T18:18:00.156Z",
                     "event-name": "Object::Update", "object-id": "test1", "collection-id": "AARON_1",
                     "collection-hash": "cce1350b70a59bc4477fb44a80606776e618d87c",
                     "bucket-name": "feature-ajb-dl-object-staging-288044017584-use1",
                     "object-key": "cce1350b70a59bc4477fb44a80606776e618d87c/b678cd84a31a0321b63fb044f5188bb25c770e32",
                     "content-type": "text/plain", "raw-content-length": 22,
                     "object-hash": "465b74fc6f5ade6983c352171777eeb40f7d8478",
                     "content-sha1": "1ff52dd5f3f7f0949c1387e9338374cd8a051ff3", "event-version": 1,
                     "old-object-versions-to-keep": 5, "stage": "LATEST"}

    input_message1 = {
        "old-object-versions-to-keep": 1,
        "content-type": "text/plain",
        "object-id": "aaron",
        "collection-id": "AARON_3",
        "object-key": "943012227688fa7ebaa24761378119f9d50aac53",
        "event-id": "d5bbb20c-b201-11e9-8bb5-299d9aa7dde9",
        "stage": "LATEST",
        "request-time": "2019-07-29T13:07:36.207Z",
        "event-name": "Object::Update",
        "event-version": 1,
        "raw-content-length": 15,
        "seen-count": 0,
        "additional-attributes": {
            "retry-state": 4,
            "object-data": {
                "object-state": "Created",
                "collection-hash": "07d6987eb2e02e80b54e33e19c8f69863004a821",
                "collection-id": "AARON_3",
                "version-number": 1,
                "object-key": "943012227688fa7ebaa24761378119f9d50aac53",
                "version-timestamp": "2019-07-29T13:07:36.207Z",
                "object-id": "aaron",
                "content-type": "text/plain",
                "raw-content-length": 15,
                "bucket-name": "feature-jek-dl-object-store-288044017584-use1",
                "replicated-buckets": [
                    "feature-jek-dl-object-store-288044017584-usw2"
                ]
            }
        }
    }
    input_message2 = {
        "event-id": "ovawRprJtOUBTlHh",
        "request-time": "2019-09-20T13:58:24.972Z",
        "event-name": "Object::Update",
        "object-id": "test2",
        "collection-id": "AARON_1",
        "collection-hash": "cce1350b70a59bc4477fb44a80606776e618d87c",
        "bucket-name": "feature-ajb-dl-object-staging-288044017584-use1",
        "object-key": "cce1350b70a59bc4477fb44a80606776e618d87c/805e71d3e71b19c5022a0b2dcf65f10403d5a487",
        "content-type": "text/plain",
        "raw-content-length": 22,
        "object-hash": "0208942ebe4586c08bcb16e3b25774e078324bd6",
        "content-sha1": "1ff52dd5f3f7f0949c1387e9338374cd8a051ff3",
        "event-version": 1,
        "old-object-versions-to-keep": 5,
        "stage": "LATEST",
        "object-metadata": {"name1": "value1", "name2": "value2"}
    }

    input_message_body = {
        "Type": "Notification",
        "MessageId": "ad0fcf66-38b1-5728-99db-9aded5a905d9",
        "TopicArn": "arn:aws:sns:us-east-1:195052678233:release-DataLake-EventDispatcher-EventDispatcherTopic-C974JPFGTBR6",
        "Message": json.dumps(input_message2),
        "Timestamp": "2019-07-29T12:47:20.916Z",
        "SignatureVersion": "1",
        "Signature": "MBhYKLKscoZ3C764j42y7gzBvn9rHe2R/uZ8XLv/g5Qm1YZObe1Eecyt8lM++6m/BVU53MeHQhLc80LwBWkP4Deoq04JH09o/4i9SlYCUJcj0ybf012m66tb9WbPpU12DSN+PV8sG7/+2WQ6/aeb8hUK/GQKrimLC0xtOZaQ5uAz99VupeBGtbti0Q8Fj4BatjMdSeCGlZbqB9iFrrBgAMPxO/ToItGIK9hI1IHbRRPxce8xCvhxAShgSPiHe3+CKVhmUXz4s7X7viOqQ9XzvWfY1gFfAuuw0go6XtYyDNR2a3o3myMIZqt7Q5A9wDT6IOkO64ynm5FWANiqmWSVCA==",
        "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem",
        "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:195052678233:release-DataLake-EventDispatcher-EventDispatcherTopic-C974JPFGTBR6:dea96194-5501-4959-a4f4-cb892e05861d",
        "MessageAttributes": {
            "stage": {"Type": "String", "Value": "v1"},
            "event-name": {"Type": "String", "Value": "Object::Update"},
            "event-version": {"Type": "Number", "Value": "1"}
        }
    }
    input_event = {
        "Records": [
            {
                "messageId": "4ea95117-fea8-4073-99e3-b10f56f6ca91",
                "receiptHandle": "AQEBAoJDW52N+Lv4RTd4Mp5MCrivHEULAoVaLYzNeTBRwi62hTwQYEp9Ne0AllrS3JbArYkC3MlFebEG3SHhb0GnpyVJ0jwfSxp4F4FIoOCloVKNYw6DwSz8Q1+URrVGrZZg8QIf3xxGn/LQkBvLYtQq9+uI2D63U5TI2oFmC3RzZ5LoRx01dBPS8us2sRDmwa8rumihascg790msmJ9AAgm4vTadLKEvbtOxpx3jaCZU9DIAbvldJbB+T13oWGTowF4pOczFcYty/VJjl5r69oqKBfNjbehpIKol1PPEVDOqsbnrRf/sCBTLrAZi3WtEjL7xSwDSqXicR90EnYKiVaxyaSVk1ROgyWf/wI132+Uruo7lfIWhf/Xfja3l9ev4k+2zC3wy0ix6tJCPNp+1Kv0Cq41AMO8ZRJkytPrwZbo58hvAh200M9oSqkF8Mexxt66GGo4IcWL6eoXeQNVVuQ7sgEMDDOzzHh1YhPqUcvQNkE=",
                "body": json.dumps(input_message_body),
                "attributes": {
                    "ApproximateReceiveCount": "12",
                    "SentTimestamp": "1551753965006",
                    "SenderId": "AIDAIT2UOQQY3AUEKVGXU",
                    "ApproximateFirstReceiveTimestamp": "1551753965006"
                },
                "messageAttributes": {},
                "md5OfBody": "2adf1c719efaf508bbe720464ef53318",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn:aws:sqs:us-east-1:195052678233:release-DataLake-UpdateObjectEventHandler-V1EventHandlerQueue-JVD6432IPZ7Y",
                "awsRegion": "us-east-1"
            }
        ]
    }


    # Below is used to create a Tracking item as if it came from command lambda
    @session_decorator.lng_aws_session()
    @tracker.track_command(event_names.OBJECT_UPDATE)
    def track(event, context):
        return {"response": {}}


    track_dict = {
        "context": {
            "client-request-id": input_message["event-id"],
            "client-request-time": 1539954955
        },
        "request": {
            "event-name": input_message["event-name"],
            "object-id": input_message["object-id"],
            "collection-id": input_message["collection-id"]
        }
    }
    # track(track_dict, {})

    lambda_handler(input_event, MockLambdaContext())
    logger.debug("[LOCAL RUN END - UPDATE OBJECT EVENT HANDLER LAMBDA]")
