import logging
import os
from concurrent.futures import ThreadPoolExecutor, as_completed
from inspect import stack, getmodulename

import orjson
from lng_aws_clients import sns, sqs
from lng_datalake_commons import session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_handler_status, ingestion_status, event_names, ingestion_counter
from lng_datalake_dal.tracking_table import TrackingTable

from service_commons import ingestion

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
# if __name__ == '__main__':
#     from aws_xray_sdk import global_sdk_config
#     global_sdk_config.set_sdk_enabled(False)
# libraries = ('botocore',)
# patch(libraries)

FANOUT_TOPIC_ARN = os.getenv("INGESTION_FANOUT_NOTIFICATION_TOPIC_ARN")
INGESTION_COUNTER_QUEUE_URL = os.getenv('INGESTION_COUNTER_QUEUE_URL')
MAX_RETRY_COUNT = int(os.getenv("MAX_RETRY_COUNT", 1000))


# Retry states - additional attribute saved to event message on retry so that processing can resume where it left off
class RetryState:
    INIT = 0
    TT_PUT = 10
    SNS_PUBLISH = 20
    DONE = 99


MESSAGE_RETRY_STATE_KEY = 'retry-state'
MESSAGE_RETRY_INDEXES_KEY = 'retry-indexes'

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
def lambda_handler(event: dict, context) -> str:
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return process_ingestion_zip_part(event, context)


def process_ingestion_zip_part(event: dict, context: object) ->str:
    ingestion_data = None
    terminal_error_exception = None
    file_index = 0
    file_count = 0

    try:
        message = ingestion.get_message(event)

        try:
            ingestion_id = message['ingestion-id']
            request_time = message['request-time']
            bucket_name = message['bucket-name']
            zip_key = message['zip-key']
            zip_length = message['zip-length']
            zip_central_directory_length = message['zip-central-directory-length']
            file_index = message['file-index']
            file_count = message['file-count']
            content_type = message['content-type']
            old_object_versions_to_keep = message['old-object-versions-to-keep']
            pending_expiration_epoch = message['pending-expiration-epoch']
            collection_hash = message['collection-hash']
            stage = message['stage']
            changeset_id = message['changeset-id']
            changeset_expiration_epoch = message['changeset-expiration-epoch']
        except KeyError as e:
            raise TerminalErrorException("Error getting attributes from message||Missing attribute: {0}"
                                         .format(e.args[0]))

        additional_message_attributes = message.get('additional-attributes', {})
        # Unload error handlers additional_attributes so we don't write inaccurate retry messages
        error_handler.additional_attributes = additional_message_attributes

        # If retry state not in message default to state INIT
        retry_state = additional_message_attributes.get(MESSAGE_RETRY_STATE_KEY, RetryState.INIT)
        if retry_state != RetryState.INIT:
            retry_state_names = {value: name for name, value in vars(RetryState).items() if name.isupper()}
            logger.info("Retrying state={}".format(retry_state_names[retry_state]))

        ingestion_data = ingestion.get_ingestion_data(ingestion_id)
        ingestion_state = ingestion_data['ingestion-state']

        if ingestion_state != ingestion_status.PROCESSING:
            raise TerminalErrorException("Ingestion ID {0} state is not set to {1} (state={2})||"
                                         "Cannot process parts {3}-{4}"
                                         .format(ingestion_id,
                                                 ingestion_status.PROCESSING,
                                                 ingestion_state,
                                                 file_index,
                                                 file_index + file_count - 1))

        zip_list, unread_length, _ = ingestion.read_zip_central_directory(bucket_name, zip_key, zip_length,
                                                                          zip_central_directory_length)

        if retry_state <= RetryState.TT_PUT:
            create_tracking_records(zip_list, ingestion_id, request_time, file_index, file_count,
                                    ingestion_data['collection-id'], changeset_id)

            update_tracked_count(ingestion_id, file_count)
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.SNS_PUBLISH)

        if retry_state <= RetryState.SNS_PUBLISH:
            base_sns_message = {
                "ingestion-id": ingestion_id,
                "bucket-name": bucket_name,
                "zip-key": zip_key,
                "content-type": content_type,
                "old-object-versions-to-keep": old_object_versions_to_keep,
                "pending-expiration-epoch": pending_expiration_epoch,
                "collection-hash": collection_hash,
                "stage": stage,
                "changeset-id": changeset_id,
                "changeset-expiration-epoch": changeset_expiration_epoch
            }
            retry_indexes = additional_message_attributes.get(MESSAGE_RETRY_INDEXES_KEY, [])
            publish_sns_messages(ingestion_id, zip_list, file_index, file_count, unread_length, base_sns_message,
                                 retry_indexes)
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.DONE)

        # Used to guarantee integration test pass and errors aren't ignored.
        return event_handler_status.SUCCESS

    except RetryEventException as e:
        try:
            process_retry_exception(e, event)
        # will only raise a TerminalErrorException if we run out of retries
        except TerminalErrorException as e2:
            terminal_error_exception = e2

    except (TerminalErrorException, Exception) as e:
        terminal_error_exception = e

    if terminal_error_exception:
        process_terminal_error(terminal_error_exception, event, context, ingestion_data, file_index, file_count)


def create_tracking_records(zip_list: list, ingestion_id: str, request_time: str, file_index: int, file_count: int,
                            collection_id: str, changeset_id: str) -> None:
    t = TrackingTable()
    tracking_items = []

    futures = []
    with ThreadPoolExecutor(max_workers=10) as executor:
        for index, file_info in enumerate(zip_list[file_index - 1:file_index - 1 + file_count], file_index):
            tracking_item = {
                "tracking-id": tracker.generate_tracking_id("object", file_info.filename, collection_id),
                "request-time": request_time,
                "event-id": ingestion.create_object_backend_event_id(ingestion_id, index),
                "event-name": event_names.OBJECT_UPDATE
            }
            if changeset_id:
                tracking_item['changeset-id'] = changeset_id
            tracking_items.append(tracking_item)

            if len(tracking_items) == 25 or index == file_index - 1 + file_count:
                future = executor.submit(t.batch_write_items, items_to_put=tracking_items)
                futures.append(future)
                tracking_items = []

            index += 1

    for future in as_completed(futures):
        try:
            response = future.result()
            if response:
                raise RetryEventException("Unable to create all Tracking Table items")

        except RetryEventException:
            raise
        except error_handler.retryable_exceptions as e:
            raise RetryEventException("Unable to create items in Tracking Table||{}".format(e))
        except Exception as ex:
            raise TerminalErrorException("Failed to create items in Tracking Table||{}".format(ex))

    logger.info("Successfully created {0} Tracking Table items for Ingestion ID {1} "
                "(parts {2}-{3})".format(file_count,
                                         ingestion_id,
                                         file_index,
                                         file_index + file_count - 1))


def update_tracked_count(ingestion_id: str, file_count: int) -> None:
    try:
        message = {ingestion_id: {ingestion_counter.TRACKED: file_count}}
        sqs.get_client().send_message(QueueUrl=INGESTION_COUNTER_QUEUE_URL, MessageBody=orjson.dumps(message).decode())
    except error_handler.retryable_exceptions as e:
        raise RetryEventException(
            "Unable to publish ObjectTrackedCount to SQS {}||{}".format(INGESTION_COUNTER_QUEUE_URL, e))
    except Exception as ex:
        logger.error("Failed to publish ObjectTrackedCount to SQS {}||{}".format(INGESTION_COUNTER_QUEUE_URL, ex))


def publish_sns_messages(ingestion_id: str, zip_list: list, file_index: int, file_count: int, unread_length: int,
                         message: dict, retry_indexes: list) -> None:

    # Initialize client outside of threading
    sns.get_client()

    futures = []
    with ThreadPoolExecutor(max_workers=10) as executor:
        for index, file_info in enumerate(zip_list[file_index - 1:file_index - 1 + file_count], file_index):
            if retry_indexes and index not in retry_indexes:
                continue
            zip_file_info = {
                "header-offset": file_info.header_offset + unread_length,
                "file-compressed-size": file_info.compress_size,
                "file-size": file_info.file_size,
                "compression-type": file_info.compress_type,
                "crc": file_info.CRC
            }
            message['zip-file-info'] = zip_file_info
            message['object-id'] = file_info.filename
            message['event-id'] = ingestion.create_object_backend_event_id(ingestion_id, index)

            future = executor.submit(publish_sns_message, orjson.dumps(message).decode(), message['stage'], index)
            futures.append(future)

    indexes_to_retry = []
    success_count = 0
    for future in as_completed(futures):
        error_index = future.result()
        if error_index > -1:
            indexes_to_retry.append(error_index)
        else:
            success_count += 1

    if success_count:
        logger.info("Successfully published {0}/{1} messages to {2} for Ingestion ID {3} "
                    "(parts {4}-{5})".format(success_count,
                                             len(futures),
                                             FANOUT_TOPIC_ARN,
                                             ingestion_id,
                                             file_index,
                                             file_index + file_count - 1))

    if indexes_to_retry:
        indexes_to_retry.sort()
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_INDEXES_KEY, indexes_to_retry)
        raise RetryEventException("Error publishing {0} messages for indexes {1} for Ingestion ID {2} (parts {3}-{4})"
                                  .format(len(indexes_to_retry),
                                          indexes_to_retry,
                                          ingestion_id,
                                          file_index,
                                          file_index + file_count - 1))


def publish_sns_message(message: str, stage: str, index: int) -> int:

    message_attributes = {
        "stage": {
            "DataType": "String",
            "StringValue": stage
        },
        "event": {
            "DataType": "String",
            "StringValue": "object"
        }
    }

    try:
        sns.get_client().publish(TopicArn=FANOUT_TOPIC_ARN,
                           Message=message,
                           MessageAttributes=message_attributes)
    except Exception as e:
        logger.error("Caught exception when publishing SNS message for index {0}||{1}".format(index, e))
        return index

    return -1


def process_retry_exception(e: RetryEventException, event: dict) -> None:
    logger.warning(e)
    sqs_url = ""
    try:
        arn = event['Records'][0]['eventSourceARN'].split(':')
        sqs_url = "https://sqs.{0}.amazonaws.com/{1}/{2}".format(arn[3], arn[4], arn[5])
        seen_count = 0
        if 'seen-count' in event['Records'][0]['messageAttributes']:
            seen_count = int(event['Records'][0]['messageAttributes']['seen-count'].get('stringValue', 0))
            if seen_count + 1 > MAX_RETRY_COUNT:
                raise TerminalErrorException("Event exceeded max retry count of {0}||{1}".format(MAX_RETRY_COUNT, e))
        # Back off 1 second for each retry
        sqs_delay = min(120 + seen_count, 900)  # 900 seconds is the longest delay supported by SQS
        event_body = event['Records'][0]['body']

        # If we have additional attributes, load the sqs body, extract the Message
        # add the additional attributes, and redump the data to overwrite the event_body to send
        if error_handler.additional_attributes:
            json_event_body = orjson.loads(event_body)
            event_message = orjson.loads(json_event_body['Message'])
            event_message['additional-attributes'] = error_handler.additional_attributes
            json_event_body['Message'] = orjson.dumps(event_message).decode()
            event_body = orjson.dumps(json_event_body).decode()

        sqs.get_client().send_message(QueueUrl=sqs_url,
                                      MessageBody=event_body,
                                      MessageAttributes={'seen-count': {
                                          'DataType': 'Number',
                                          'StringValue': str(seen_count + 1)}},
                                      DelaySeconds=sqs_delay)
    except TerminalErrorException as e2:
        raise e2
    # If we throw any exception here just throw the original Retry exception we received.
    except Exception as inner:
        logger.warning("Error sending SQS message back to queue: {0}||{1}".format(sqs_url, inner))
        raise e


def process_terminal_error(e: Exception, event: dict, context: object, ingestion_data: dict, file_index: int,
                           file_count: int) -> None:
    if isinstance(e, TerminalErrorException):
        logger.error(e)
    else:
        logger.exception(e)

    uid = "{0}-{1}:{2}".format(ingestion_data['ingestion-id'], file_index,
                               file_index + file_count - 1) if ingestion_data else None
    error_handler.terminal_error(event, is_event=False, uid=uid, error_message=str(e), context=context)

    # update IngestionTable object-error-counter if we have ingestion data
    if ingestion_data:
        ingestion.update_error_counter(ingestion_data['ingestion-id'], file_count)


if __name__ == '__main__':
    import json
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    ASSET_GROUP = 'feature-jek'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['INGESTION_DYNAMODB_TABLE'] = "{0}-DataLake-IngestionTable".format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = "{0}-DataLake-CollectionBlockerTable".format(ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = "{0}-DataLake-TrackingTable".format(ASSET_GROUP)
    os.environ[
        "INGESTION_FANOUT_NOTIFICATION_TOPIC_ARN"] = "arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ProcessIngestionZip-IngestionFanOutTopic-XQX0Y7OOP5QU"
    os.environ[
        "INGESTION_COUNTER_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-jek-DataLake-ProcessIngestionZip-IngestionCounterQueue-TXFXZDIDIZGO"

    FANOUT_TOPIC_ARN = os.getenv("INGESTION_FANOUT_NOTIFICATION_TOPIC_ARN")
    INGESTION_COUNTER_QUEUE_URL = os.getenv("INGESTION_COUNTER_QUEUE_URL")
    ingestion.INGESTION_COUNTER_QUEUE_URL = os.getenv("INGESTION_COUNTER_QUEUE_URL")

    error_handler.terminal_errors_bucket = "feature-jek-datalake-lambda-terminal-errors-288044017584"

    input_message_1524439 = {
        "ingestion-id": "1524439.zip",
        "request-time": "2019-03-06T18:01:49.119Z",
        "bucket-name": "feature-jek-dl-object-staging-288044017584-use1",
        "zip-key": "ingestion/LATEST/1524439.zip",
        "zip-length": 7583684882,
        "zip-central-directory-length": 80206738,
        # "file-index": 999001,
        "file-index": 482001,  # this is the 4GB boundary condition
        # "file-index": 1,
        "file-count": 1000,
        "stage": "LATEST",
        "content-type": "application/xml",
        "old-object-versions-to-keep": 5,
        "pending-expiration-epoch": 0,
        "collection-hash": "abcdefg"
    }
    input_message_dltest = {
        "ingestion-id": "dltest.zip",
        "request-time": "2019-03-06T18:01:49.119Z",
        "bucket-name": "feature-jek-dl-object-staging-288044017584-use1",
        "zip-key": "ingestion/LATEST/dltest.zip",
        "zip-length": 9153942,
        "zip-central-directory-length": 85022,
        "file-index": 1,
        "file-count": 1000,
        "stage": "LATEST",
        "content-type": "application/xml",
        "old-object-versions-to-keep": 5,
        "pending-expiration-epoch": 0,
        "collection-hash": "abcdefg"
    }
    input_message_body = {
        "Type": "Notification",
        "MessageId": "27b946dc-678c-5ea4-9390-13de9b02142a",
        "TopicArn": "arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ProcessIngestionZip-IngestionFanOutTopic-XQX0Y7OOP5QU",
        "Message": json.dumps(input_message_dltest),
        "Timestamp": "2019-03-13T12:40:46.618Z",
        "SignatureVersion": "1",
        "Signature": "b5jg7yBMosuaLm3vgEWe06iD7jvpbbl4mJke5vGo2NUTKXVlZeofhyeG9VWGTDkKmMMA3gqd6CWw+juXKWFGLw4nOlB/3cIuC57FDWzlj/0ybnrc0CLWKvVuaQAbTKCE9nQdKSbrbJPvtLJoOXM8BFGNDIH69nwhA+7glB+9N9qWVpb53xIOnzwuCEMezZZ6APdzFXpXDJurfS9wQ0AA3zllz4vGxuyf44AIT1MhFRezaUOZCNpBBN6HrOHyXV5S/MD5VMNNaGgPmhkxURyOLPfHClhCTBU/osvKyYVuUbrm0XSXnVhVk3o2UN4zopAYHzw0Mb+yBXH8HZflau0hmg==",
        "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem",
        "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ProcessIngestionZip-IngestionFanOutTopic-XQX0Y7OOP5QU:1002ac73-3a4e-485c-8f4d-c39ccb352840",
        "MessageAttributes": {
            "stage": {
                "Type": "String",
                "Value": "LATEST"
            },
            "event": {
                "Type": "String",
                "Value": "part"
            }
        }
    }
    input_event = {
        "Records": [
            {
                "messageId": "832f802e-79cf-4b05-8bf9-dec0f9887d44",
                "receiptHandle": "AQEBflOO1VfhgdfgI15d/ud+rTk/apSsCMHYvhNQxBD8d2J13D4jPGFE/JEz76emIHxsppOgYr6WzL3NLbTpwR98QrmmZvqkqA6UeojMT0I719CTtnRuiP7mgpsTCHlitde7VeFCf9nceduhk3dwFrVcGZH6LsPxCeQQW8gw7n/hX4L4iRaktiRBZqpMXIW30BXNQv/TduzQr/tHZYn9zbGKwYTC1j51UEgReF2nqV8P15mmYk6kr+44r/jvD4psFt4XySR+09z/K4Q4nKJPTcYQzJN1JTkakH68D2dJbxvuR9MO2RGKVrzwPv3UFCu+YRlbx420Y+b0AcLY4ioZBgubYWA9+VoG1YZ2l+wWkuQKLMkX1t84iN1FRRmhfuFTtnNzZFVAk1V/3y6/zuE+Wav74nueyGDmqqxrqhNUoaDrs3rMZpZytGfl3y4RX2CI33SBzlWjysOzrvj+JzhHgzln3Dl7NqVdYYpBmiOQoYebrIw=",
                "body": json.dumps(input_message_body),
                "attributes": {
                    "ApproximateReceiveCount": "2",
                    "SentTimestamp": "1552480846664",
                    "SenderId": "AIDAIT2UOQQY3AUEKVGXU",
                    "ApproximateFirstReceiveTimestamp": "1552480861230"
                },
                "messageAttributes": {},
                "md5OfBody": "5e5f5e4335bf66c7acbfda22cbd2b9ff",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn:aws:sqs:us-east-1:288044017584:feature-jek-DataLake-ProcessIngestionZip-LatestIngestionPartQueue-1W00L2FIYO5B6",
                "awsRegion": "us-east-1"
            }
        ]
    }
    print(json.dumps(input_event))
    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for _ in range(n)])
    mock_context = MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    lambda_handler(input_event, mock_context)

    logger.debug("Done process_ingestion_zip_part.")
