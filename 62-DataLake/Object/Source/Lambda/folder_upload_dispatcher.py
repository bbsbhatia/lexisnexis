import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_aws_clients import glue
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status, event_names

from service_commons import object_common

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event handler version environment variable
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])
GLUE_JOB_NAME = os.getenv("GLUE_FOLDER_PROCESSOR_JOB_NAME")
DATALAKE_BUCKET_NAME = os.getenv("DATALAKE_BUCKET_NAME")
COMPLETION_TOPIC_ARN = os.getenv("GLUE_DISPATCHER_TOPIC_ARN")


@session_decorator.lng_aws_session()
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return folder_upload_dispatcher(event, context.invoked_function_arn)


def folder_upload_dispatcher(event: dict, lambda_arn: str) -> str:
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.OBJECT_CREATE_FOLDER, event_names.OBJECT_UPDATE_FOLDER):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {} or {}".format(event,
                                                                                     event_names.OBJECT_CREATE_FOLDER,
                                                                                     event_names.OBJECT_UPDATE_FOLDER))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    if message['event-name'] == event_names.OBJECT_CREATE_FOLDER:
        event_name = event_names.OBJECT_CREATE_PENDING

    else:
        event_name = event_names.OBJECT_UPDATE_PENDING

    object_buckets = object_common.generate_object_store_buckets(DATALAKE_BUCKET_NAME)
    bucket_list = [object_buckets[bucket] for bucket in object_buckets]
    glue_args = build_glue_argument(message, event_name, bucket_list)
    logging.debug("Starting Glue Job: {0} with arguments: {1}".format(GLUE_JOB_NAME, glue_args))
    try:
        resp = glue.get_client().start_job_run(
            JobName=GLUE_JOB_NAME,
            Arguments={
                '--event_message': orjson.dumps(glue_args).decode()
            }
        )
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Exception while starting Folder Upload Glue Job {}; Exception {}".format(GLUE_JOB_NAME, e))

    logger.info("Glue JobRunId: {0}".format(resp['JobRunId']))

    # Used to guarantee integration test pass and errors aren't ignored.
    return event_handler_status.SUCCESS


def build_glue_argument(event_message: dict, event_name: str, destination_buckets: list) -> dict:
    glue_arg = {
        'completion-topic-arn': COMPLETION_TOPIC_ARN,
        'object-store-buckets': destination_buckets
    }
    for item in event_message:
        if item == 'event-name':
            glue_arg['event-name'] = event_name
        elif item == 'bucket-name':
            glue_arg['staging-bucket'] = event_message['bucket-name']
        elif item == 'object-key':
            glue_arg['staging-prefix'] = event_message['object-key']
        else:
            glue_arg[item] = event_message[item]

    return glue_arg


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - FOLDER UPLOAD DISPATCHER]")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["DATALAKE_BUCKET_NAME"] = 'feature-datalake-object-store-288044017584'
    os.environ[
        "GLUE_DISPATCHER_TOPIC_ARN"] = 'arn:aws:sns:us-east-1:288044017584:feature-DataLake-GlueFolderProcessor-GlueDispatcherNotificationTopic-QX810C2K4BU3'
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    sample_event = {
        "Records": [
            {
                "EventSource": "aws:sns",
                "EventVersion": "1.0",
                "EventSubscriptionArn": "arn:aws:sns:us-east-1:288044017584:feature-DataLake-EventDispatcher-EventDispatcherTopic-SBYWV36EXDXG:6869497a-ec0c-4996-900c-a5a7a54ca7a3",
                "Sns": {
                    "Type": "Notification",
                    "MessageId": "f9a94603-d15f-5e19-b0ea-15f04f4edb74",
                    "TopicArn": "arn:aws:sns:us-east-1:288044017584:feature-DataLake-EventDispatcher-EventDispatcherTopic-SBYWV36EXDXG",
                    "Subject": None,
                    "Message": "{\"version-id\": \"1\", \"bucket-name\": \"feature-datalake-object-staging-store-288044017584\", \"object-id\": \"John-Test-123123\", \"collection-id\": \"830\", \"object-key\": \"1_adc226d1-a57c-11e8-a52a-15395f060ef1\", \"event-id\": \"d4a93a5d-a57f-11e8-934b-2b23982bccda\", \"stage\": \"LATEST\", \"request-time\": \"2018-08-21T20:21:48.209Z\", \"event-name\": \"Object::FolderUploadCreate\", \"event-version\": 1, \"old-object-versions-to-keep\": -1}",
                    "Timestamp": "2018-08-19T16:33:21.994Z",
                    "SignatureVersion": "1",
                    "Signature": "jAXuz2jWLxEETC0YWUucdfvUTnOylPlBoSzH1TVCPNJ/tI+z4EQtISStjRJUM0wLPLwNkgNzX+Aidsi/oE8uPQe+grOVGi9hyvJmvVTyISxrAlRRiqsUGunSy6szcHfB5xLy7fOIKPpF5x6og4nra9YTFUhBaMR/1lgci7WtivR+1ESt7l3FUjxoRjk/kammABGW6iq5HDqpD9FvV0dnBNznavZK1NkUJQibAzwFoS+h4dGkwBI7QH63ygd6aoW1m4h2UE4LZJ/I9xDh+6CDvQfaa1jgbMTGz0Q04Ig5u+4S+Qx786lr8TwlQ4ywG0i+xiffjFcSpOxu2HSnPQ3cUA==",
                    "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-eaea6120e66ea12e88dcd8bcbddca752.pem",
                    "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:feature-DataLake-EventDispatcher-EventDispatcherTopic-SBYWV36EXDXG:6869497a-ec0c-4996-900c-a5a7a54ca7a3",
                    "MessageAttributes": {
                        "stage": {
                            "Type": "String",
                            "Value": "LATEST"
                        },
                        "event-name": {
                            "Type": "String",
                            "Value": "Object::FolderUpload"
                        },
                        "event-version": {
                            "Type": "String",
                            "Value": "1"
                        }
                    }
                }
            }
        ]
    }

    lambda_handler(sample_event, MockLambdaContext())
    logger.debug("[LOCAL RUN END - FOLDER UPLOAD DISPATCHER]")
