import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_aws_clients import s3
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, SemanticError, \
    NotAllowedError, UnhandledException, NoSuchCollection, InvalidRequestPropertyValue, NoSuchChangeset
from lng_datalake_commons import session_decorator
from lng_datalake_constants import collection_status, changeset_status
from lng_datalake_dal.changeset_table import ChangesetTable
from lng_datalake_dal.collection_table import CollectionTable

from service_commons import object_common, object_command

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Constant for encoding type
UTF_8_ENCODING = "UTF-8"
MAX_CONTENT_LENGTH = 5 * 1024 * 1024


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/get_object_command-RequestSchema.json',
                         'Schemas/get_object_command-ResponseSchema.json')
def lambda_handler(event, context):
    return get_object_command(event['request'], event['context']['stage'])


def get_object_command(request: dict, stage: str) -> dict:
    # required properties
    collection_id = request['collection-id']
    object_id = request['object-id']

    # optional properties
    version_number = request.get("version-number")
    changeset_id = request.get("changeset-id")

    if changeset_id:
        if not version_number:
            raise InvalidRequestPropertyValue("Version number does not exist in the request",
                                              "Version number is required to retrieve a changeset object")
        else:
            validate_changeset(changeset_id)

    # get collection data and validate
    collection_data = get_collection_data_and_validate(collection_id)

    # get object data and validate
    if changeset_id:
        object_data = object_command.get_changeset_object_response(object_id, collection_id, changeset_id,
                                                                   version_number)
    else:
        object_data = object_command.get_object_response(object_id, collection_id, version_number)

    # don't retrieve content if this is a multipart object
    content_type = object_data['content-type']
    if content_type.startswith("multipart"):
        # TODO: Ask Team on this message
        raise NotAllowedError(
            "Object ID {0} is a multipart object and must be retrieved through the object-key-url".format(object_id),
            "Multipart object can be accessed directly using object-key-url from "
            "GET /objects/{0}/describe/?collection-id={2}&object-id={1}".format(stage, object_id,
                                                                                collection_id))

    # get object version data if version number specified and not requesting current version
    object_version_data = {}
    if version_number and version_number != object_data['version-number'] and not changeset_id:
        object_version_data = object_command.get_object_version_response(object_id, collection_id, version_number)
        object_key = object_version_data['object-key']
        bucket_name = object_version_data['bucket-name']
    else:
        object_key = object_data['object-key']
        bucket_name = object_data['bucket-name']

    raw_content_length = object_version_data['raw-content-length'] if object_version_data \
        else object_data['raw-content-length']

    # don't retrieve content if raw content length too large or if binary content
    if raw_content_length > MAX_CONTENT_LENGTH or is_binary_content(content_type) or not is_utf8_content(content_type):
        object_content = None
        object_metadata = None

    # retrieve S3 content for object key
    else:
        # get metadata from S3 object (instead of OST/OSVT) to make sure we get DL metadata in addition to user
        # specified object metadata so we match headers returned from an S3 redirect
        object_content, object_metadata = read_object_content(bucket_name, object_key)

    return {"response-dict": generate_response_json(collection_data, object_data, object_version_data, object_key,
                                                    stage, object_content, object_metadata)}


def get_collection_data_and_validate(collection_id: str) -> dict:
    # validate collection exists and is not terminated
    try:
        collection_data = CollectionTable().get_item({'collection-id': collection_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Collection Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not collection_data:
        raise NoSuchCollection("Invalid Collection ID {0}".format(collection_id),
                               "Collection ID does not exist")

    if collection_data['collection-state'] in (collection_status.TERMINATING,
                                               collection_status.TERMINATED,
                                               collection_status.TERMINATING_FAILED):
        raise SemanticError("Collection ID {0} is terminated".format(collection_id),
                            "Objects cannot be retrieved from a terminated collection")

    return collection_data


def validate_changeset(changeset_id: str) -> None:
    try:
        changeset_data = ChangesetTable().get_item({'changeset-id': changeset_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Changeset Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not changeset_data or changeset_data['changeset-state'] == changeset_status.EXPIRED:
        raise NoSuchChangeset("Invalid Changeset ID {0}".format(changeset_id),
                              "Changeset ID does not exist")


def is_utf8_content(content_type: str) -> bool:
    content_split = content_type.split(';')
    if len(content_split) == 2:
        charset_type = content_split[1].replace('charset=', '').strip()
        if charset_type and charset_type.lower() != "utf-8":
            return False

    return True


def is_binary_content(content_type: str) -> bool:
    base_type = content_type.split(';')[0].strip()
    return base_type not in ["application/json",
                             "text/plain",
                             "text/csv",
                             "text/html",
                             "text/sgml",
                             "application/xml",
                             "application/xhtml+xml",
                             "application/atom+xml"]


def read_object_content(bucket_name: str, object_key: str) -> (str, dict):
    try:
        response = s3.get_client().get_object(Bucket=bucket_name, Key=object_key)
        return response['Body'].read().decode(UTF_8_ENCODING), response['Metadata']
    except ClientError as e:
        raise InternalError("Unable to read S3 object {0} from bucket {1}".format(object_key, bucket_name), e)
    except Exception as e:
        raise UnhandledException(e)


def generate_response_json(collection_data: dict, object_data: dict, object_version_data: dict, object_key: str,
                           stage: str, object_body: str, object_metadata: dict) -> dict:
    response = \
        {
            "collection-id": collection_data['collection-id'],
            "collection-url": "/collections/{0}/{1}".format(stage, collection_data['collection-id']),
            "object-id": object_data['object-id'],
            "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, object_data['object-id'],
                                                                      collection_data['collection-id']),
            "owner-id": collection_data['owner-id'],
            "asset-id": collection_data['asset-id'],
            "object-state": object_data['object-state'],
            "object-key-url": object_common.build_object_key_url(object_key, object_data['content-type']),
            "raw-content-length": object_data['raw-content-length'],
            "content-type": object_data['content-type']
        }

    if object_body is None:
        response['object-redirect'] = True
    else:
        response['object-body'] = object_body
        if object_metadata:
            response['object-metadata'] = object_metadata

    if object_version_data:
        response['raw-content-length'] = object_version_data['raw-content-length']
        response['version-number'] = object_version_data['version-number']
        response['version-timestamp'] = object_version_data['version-timestamp']
        if 'content-type' in object_version_data:
            response['content-type'] = object_version_data['content-type']
    else:  # Get version information from object if available
        if 'version-number' in object_data:
            response['version-number'] = object_data['version-number']
        if 'version-timestamp' in object_data:
            response['version-timestamp'] = object_data['version-timestamp']
        if 'changeset-id' in object_data:
            response['changeset-id'] = object_data['changeset-id']

    return response


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    from datetime import datetime
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - GET OBJECT COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    asset_group = 'feature-ajb'

    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(asset_group)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(asset_group)
    os.environ['OBJECT_STORE_VERSION_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreVersionTable'.format(asset_group)
    os.environ['CHANGESET_DYNAMODB_TABLE'] = '{0}-DataLake-ChangesetTable'.format(asset_group)
    os.environ['CHANGESET_OBJECT_DYNAMODB_TABLE'] = '{0}-DataLake-ChangesetObjectTable'.format(asset_group)

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "collection-id": "Agency",
                "object-id": "testFile1",
                "version-number": 3
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(lambda_event, MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - GET OBJECT COMMAND LAMBDA]")
