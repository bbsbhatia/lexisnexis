import logging
import os
from datetime import datetime, timedelta
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_aws_clients import sqs
from lng_datalake_commons import session_decorator, sqs_extractor
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status, changeset_status
from lng_datalake_dal import dynamo_mapper
from lng_datalake_dal.changeset_table import ChangesetTable

__author__ = "Daniel Wang"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

MAX_RETRY_COUNT = os.getenv("MAX_RETRY_COUNT", 10)
CHANGESET_EXPIRATION_RETRY_SQS_URL = os.getenv("CHANGESET_EXPIRATION_RETRY_QUEUE_URL")


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return process_changeset_expiration(event, context)


def process_changeset_expiration(event: dict, context: object) -> str:
    records = event['Records']
    event_id = context.aws_request_id
    try:
        records_to_process, messages_to_process = filter_records_to_process(records, event_id)

        process_changeset_items(records_to_process)
        process_retry_messages(messages_to_process)

    except (TerminalErrorException, Exception) as e:
        if isinstance(e, TerminalErrorException):
            logger.error(e)
        else:
            logger.exception(e)

        error_handler.terminal_error(records,
                                     is_event=False, error_message=str(e), context=context)

    return event_handler_status.SUCCESS


def filter_records_to_process(records: list, event_id: str) -> tuple:
    records_to_process = []
    messages_to_process = []
    for record in records:
        try:
            if record.get('dynamodb', False):
                if record.get('eventName', 'MISSING') == 'REMOVE' and not is_changeset_state_expired(record):
                    transformed_record = dynamo_mapper.transform_to_client_dict(record['dynamodb']['OldImage'])
                    # eventID from dynamo stream record has to be added to later build event ids list
                    transformed_record['event-id'] = event_id
                    records_to_process.append(transformed_record)
            # if message from a retry queue, then parameter will have 'body'
            elif record.get('body', False):
                messages_to_process.append(record)
            else:
                logger.error("Failed to process record {0}".format(record))
                raise Exception("Input must be in the form of stream record or queue message.")
        except Exception as e:
            raise TerminalErrorException(
                "Unhandled exception occurred while building records to process. {0}".format(e))

    return records_to_process, messages_to_process


def is_changeset_state_expired(record: dict) -> bool:
    return record['dynamodb']['OldImage']['ChangesetState']['S'] == changeset_status.EXPIRED


def process_changeset_items(records_to_process: list) -> None:
    for old_image in records_to_process:
        expired_changeset = expire_changeset_state(old_image)

        try:
            ChangesetTable().put_item(expired_changeset)
        except error_handler.retryable_exceptions as e:
            logger.error("Error occurred {0}, sending message to retry queue: {1}".format(e, expired_changeset))
            retry_message(expired_changeset)
        except Exception as e:
            raise TerminalErrorException(
                "Unhandled exception occurred while processing changeset item. {0}".format(e))


def expire_changeset_state(old_image: dict) -> dict:
    expired_changeset = old_image.copy()
    expired_changeset['changeset-state'] = changeset_status.EXPIRED
    expired_changeset['pending-expiration-epoch'] = int((datetime.fromtimestamp(old_image['pending-expiration-epoch']) +
                                                         timedelta(days=365)).timestamp())
    expired_changeset['event-ids'] = ChangesetTable().build_event_ids_list(
        expired_changeset, expired_changeset['event-id'])
    del expired_changeset['event-id']
    return expired_changeset


def process_retry_messages(messages_to_process: list) -> None:
    for retry_msg in messages_to_process:
        retried_count = int(retry_msg['messageAttributes']['retry-count']['stringValue'])
        if retried_count > MAX_RETRY_COUNT:
            logger.error("Max retry limit reached for this message {0}".format(retry_msg))
            raise TerminalErrorException("Max retry limit reached for this message {0}".format(retry_msg))

        retry_count = retried_count + 1

        changeset_item = sqs_extractor.extract_sqs_message(retry_msg)

        try:
            ChangesetTable().put_item(changeset_item)
        except error_handler.retryable_exceptions as e:
            logger.error("Error occurred {0}, sending message to retry queue: {1}".format(e, changeset_item))
            retry_message(changeset_item, retry_count)
        except Exception as e:
            raise TerminalErrorException(
                "Unhandled exception occurred while processing changeset collection items. {0}".format(e))


def retry_message(item: dict, retry_count: int = 0) -> None:
    try:
        sqs.get_client().send_message(QueueUrl=CHANGESET_EXPIRATION_RETRY_SQS_URL,
                                      MessageBody=orjson.dumps(item).decode(),
                                      MessageAttributes={
                                          'retry-count': {'StringValue': str(retry_count), 'DataType': 'Number'}})
    except Exception as e:
        raise TerminalErrorException(
            "Failed to send retry message {0} to SQS {1} error: {2}".format(item,
                                                                            CHANGESET_EXPIRATION_RETRY_SQS_URL, e))
    logger.info("Retry message {0} published to SQS {1}".format(item, CHANGESET_EXPIRATION_RETRY_SQS_URL))


if __name__ == '__main__':
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    ASSET_GROUP = 'feature-kgg'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CHANGESET_DYNAMODB_TABLE'] = "{0}-DataLake-ChangesetTable".format(ASSET_GROUP)
    CHANGESET_EXPIRATION_RETRY_SQS_URL = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-kgg-DataLake-ProcessChang-ProcessChangesetExpirationRetryQ-CXNZAUCNJ0HF"

    error_handler.terminal_errors_bucket = "{0}-datalake-lambda-terminal-errors-288044017584".format(ASSET_GROUP)

    input_msg_stream = {
        "Records": [
            {
                "eventID": "320594a976d374cf29b8b2d36f36da29",
                "eventName": "REMOVE",
                "eventVersion": "1.1",
                "eventSource": "aws:dynamodb",
                "awsRegion": "us-east-1",
                "dynamodb": {
                    "ApproximateCreationDateTime": 1573504426,
                    "Keys": {
                        "ChangesetID": {
                            "S": "test-changeset1"
                        }
                    },
                    "OldImage": {
                        "ChangesetTimestamp": {
                            "S": "2019-06-03T16:09:33.861Z"
                        },
                        "ChangesetID": {
                            "S": "test-changeset1"
                        },
                        "ChangesetState": {
                            "S": "Opened"
                        },
                        "aws:rep:updatetime": {
                            "N": "1573501662.691001"
                        },
                        "Description": {
                            "S": "test description"
                        },
                        "EventIds": {
                            "L": [
                                {
                                    "S": "14dc7e9e-2d81-41e0-bd81-303053250ec9"
                                }
                            ]
                        },
                        "OwnerID": {
                            "N": "1677"
                        },
                        "PendingExpirationEpoch": {
                            "N": "1573909785"
                        },
                        "aws:rep:deleting": {
                            "BOOL": False
                        },
                        "aws:rep:updateregion": {
                            "S": "us-east-1"
                        }
                    },
                    "SequenceNumber": "128918700000000012937941523",
                    "SizeBytes": 224,
                    "StreamViewType": "NEW_AND_OLD_IMAGES"
                },
                "eventSourceARN": "arn:aws:dynamodb:us-east-1:288044017584:table/feature-kgg-DataLake-ChangesetTable/stream/2019-10-15T16:22:41.540"
            },
            {
                "eventID": "7484b868a2a80db17052f1609e7ef0d9",
                "eventName": "INSERT",
                "eventVersion": "1.1",
                "eventSource": "aws:dynamodb",
                "awsRegion": "us-east-1",
                "dynamodb": {
                    "ApproximateCreationDateTime": 1573501934,
                    "Keys": {
                        "ChangesetID": {
                            "S": "test-changeset"
                        }
                    },
                    "NewImage": {
                        "ChangesetTimestamp": {
                            "S": "2019-06-03T16:09:33.861Z"
                        },
                        "ChangesetID": {
                            "S": "test-changeset2"
                        },
                        "ChangesetState": {
                            "S": "Opened"
                        },
                        "aws:rep:updatetime": {
                            "N": "1572879746.233001"
                        },
                        "Description": {
                            "S": "test description"
                        },
                        "EventIds": {
                            "L": [
                                {
                                    "S": "14dc7e9e-2d81-41e0-bd81-303053250ec8"
                                }
                            ]
                        },
                        "OwnerID": {
                            "N": "1677"
                        },
                        "PendingExpirationEpoch": {
                            "N": "1573909785"
                        },
                        "aws:rep:deleting": {
                            "BOOL": False
                        },
                        "aws:rep:updateregion": {
                            "S": "us-east-1"
                        }
                    },
                    "SequenceNumber": "128918500000000012936762352",
                    "SizeBytes": 250,
                    "StreamViewType": "NEW_AND_OLD_IMAGES"
                },
                "eventSourceARN": "arn:aws:dynamodb:us-east-1:288044017584:table/feature-kgg-DataLake-ChangesetTable/stream/2019-10-15T16:22:41.540"
            },
            {
                "eventID": "1c2b8501ce5b0f6fe8694ada9c2a7116",
                "eventName": "MODIFY",
                "eventVersion": "1.1",
                "eventSource": "aws:dynamodb",
                "awsRegion": "us-east-1",
                "dynamodb": {
                    "ApproximateCreationDateTime": 1573501934,
                    "Keys": {
                        "ChangesetID": {
                            "S": "test-changeset2"
                        }
                    },
                    "NewImage": {
                        "ChangesetTimestamp": {
                            "S": "2019-06-03T16:09:33.861Z"
                        },
                        "ChangesetID": {
                            "S": "test-changeset2"
                        },
                        "ChangesetState": {
                            "S": "Opened"
                        },
                        "aws:rep:updatetime": {
                            "N": "1573501934.860001"
                        },
                        "Description": {
                            "S": "test description"
                        },
                        "EventIds": {
                            "L": [
                                {
                                    "S": "14dc7e9e-2d81-41e0-bd81-303053250ec8"
                                }
                            ]
                        },
                        "OwnerID": {
                            "N": "1677"
                        },
                        "PendingExpirationEpoch": {
                            "N": "1573909785"
                        },
                        "aws:rep:deleting": {
                            "BOOL": False
                        },
                        "aws:rep:updateregion": {
                            "S": "us-east-1"
                        }
                    },
                    "OldImage": {
                        "ChangesetTimestamp": {
                            "S": "2019-06-03T16:09:33.861Z"
                        },
                        "ChangesetID": {
                            "S": "test-changeset2"
                        },
                        "ChangesetState": {
                            "S": "Opened"
                        },
                        "aws:rep:updatetime": {
                            "N": "1572879746.233001"
                        },
                        "Description": {
                            "S": "test description"
                        },
                        "OwnerID": {
                            "N": "1677"
                        },
                        "PendingExpirationEpoch": {
                            "N": "1573909785"
                        },
                        "aws:rep:deleting": {
                            "BOOL": False
                        },
                        "aws:rep:updateregion": {
                            "S": "us-east-1"
                        }
                    },
                    "SequenceNumber": "128918600000000012936762461",
                    "SizeBytes": 475,
                    "StreamViewType": "NEW_AND_OLD_IMAGES"
                },
                "eventSourceARN": "arn:aws:dynamodb:us-east-1:288044017584:table/feature-kgg-DataLake-ChangesetTable/stream/2019-10-15T16:22:41.540"
            }
        ]
    }

    input_msg_sqs = {
        "Records": [
            {
                "messageId": "1d324c62-2a49-48e6-bed6-bc8c4bb856ee",
                "receiptHandle": "AQEB2p0nmLIP28s3n84aO7Jmpi8CUlU5voVchGnSBOW3OwIwy9KTLI4rBzCQUohFhu1H5QB4GImmDrRc9qajoXzb+C6eVg4m8BKqAOkxz+8JWMqOn25QAoIAcGN769wYuYxReKDXGUq81GaJtd672xq2a0f/PrBvkbLI8Rl3Rcy3kRUEY302pi03+HtDVHvEepdRDF8Un82xU/NKsiespfWBve1G80JiTFWh6FHKAWAi88bW4CXStmuRxiixNCv1vvQYzg+aC0oXWJCqxfxGzlx2cZMPihd3wJaeHSSJoDlndvlqc2mt94K5iStC92RXtu0UV5Gtkf7wewOpBHmGK8yER/JuQ8f71PqaqnSL2O8QIH0/L4NTT4mq89IqvkwQtHeOqvYIeYUIz02gGDEaPBKjT2RxlmWS9B6o7Ytt578c+J75gzHWVOcR8t5eIFJxnkWYZF0WOt0D7hDP6AgIhjnXC9Qz8gfVt5y1zPWosTjaAnE=",
                "body": "{\"changeset-timestamp\":\"2019-10-28T05:11:21.047Z\",\"changeset-id\":\"test-changeset1\",\"changeset-state\":\"Opened\",\"description\":\"test description\",\"owner-id\":1677,\"pending-expiration-epoch\":1573909785,\"event-ids\":[\"14dc7e9e-2d81-41e0-bd81-303053250ec8\",\"320594a976d374cf29b8b2d36f36da29\"]}"
                ,
                "attributes": {
                    "ApproximateReceiveCount": "1",
                    "SentTimestamp": "1553288123647",
                    "SenderId": "AROAIUGBALTVOVCLLYE3Y:SeitteMA@legal.regn.net",
                    "ApproximateFirstReceiveTimestamp": "1553288123924"
                },
                "messageAttributes": {
                    "retry-count": {
                        "stringValue": "4",
                        "stringListValues": [],
                        "binaryListValues": [],
                        "dataType": "Number"
                    }
                },
                "md5OfBody": "fef6ff5cbff8234c7176af60c694c30a",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn",
                "awsRegion": "us-east-1"
            }
        ]
    }

    logging.lastResort.setLevel(logging.DEBUG)

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for _ in range(n)])
    mock_context = MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    lambda_handler(input_msg_stream, mock_context)
    # lambda_handler(input_msg_sqs, mock_context)

    logger.debug("Done with process_changeset_expiration")
