import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.command_wrapper import time_helper
from lng_datalake_commands.exceptions import InternalError, UnhandledException, NoSuchChangeset
from lng_datalake_commons import session_decorator
from lng_datalake_dal.changeset_table import ChangesetTable
from lng_datalake_constants import changeset_status

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/get_changeset_command-RequestSchema.json',
                         'Schemas/get_changeset_command-ResponseSchema.json')
def lambda_handler(event, context):
    return get_changeset_command(event['request'], event['context']['stage'])


def get_changeset_command(request: dict, stage: str) -> dict:
    changeset_id = request['changeset-id']

    try:
        changeset_response = ChangesetTable().get_item({"changeset-id": changeset_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Changeset Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not changeset_response or changeset_response['changeset-state'] == changeset_status.EXPIRED:
        raise NoSuchChangeset("Invalid Changeset ID {0}".format(changeset_id), "Changeset ID does not exist")

    return {"response-dict": generate_response_json(changeset_response, stage)}


def generate_response_json(changeset_data: dict, stage: str) -> dict:
    response_dict = {}
    prop = None

    try:
        default_properties = \
            {
                "changeset-url": "/objects/{0}/changeset/{1}".format(stage, changeset_data['changeset-id']),
                "changeset-expiration-date": time_helper.format_time(
                    changeset_data['pending-expiration-epoch'])
            }

        for prop in command_wrapper.get_required_response_schema_keys('changeset-properties'):
            if prop in default_properties:
                response_dict[prop] = default_properties[prop]
            else:
                response_dict[prop] = changeset_data[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('changeset-properties'):
        if prop in changeset_data:
            response_dict[prop] = changeset_data[prop]

    return response_dict


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    from datetime import datetime
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - GET CHANGESET COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'
    os.environ['CHANGESET_DYNAMODB_TABLE'] = 'feature-maen-DataLake-ChangesetTable'

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "changeset-id": "007ab2c8-c687-48a7-ace3-5428605a9345"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(lambda_event, MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - GET CHANGESET COMMAND LAMBDA]")
