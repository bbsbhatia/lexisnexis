import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, UnhandledException, NoSuchIngestion
from lng_datalake_commons import session_decorator
from lng_datalake_dal.ingestion_table import IngestionTable

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/get_ingestion_command-RequestSchema.json',
                         'Schemas/get_ingestion_command-ResponseSchema.json')
def lambda_handler(event, context):
    return get_ingestion_command(event['request'], event['context']['stage'])


def get_ingestion_command(request, stage):
    ingestion_id = request['ingestion-id']

    try:
        ingestion_response = IngestionTable().get_item({"ingestion-id": ingestion_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Ingestion Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not ingestion_response:
        raise NoSuchIngestion("Invalid Ingestion ID {0}".format(ingestion_id), "Ingestion ID does not exist")

    return {"response-dict": generate_response_json(ingestion_response, stage)}


def generate_response_json(ingestion_data: dict, stage: str) -> dict:
    response_dict = {}
    prop = None

    try:
        default_properties = \
            {
                "collection-url": "/collections/{0}/{1}".format(stage, ingestion_data['collection-id']),
                "ingestion-url": "/objects/{0}/ingestion/{1}".format(stage, ingestion_data['ingestion-id']),
                "ingestion-expiration-date": command_wrapper.time_helper.format_time(
                    ingestion_data['pending-expiration-epoch'])
            }

        for prop in command_wrapper.get_required_response_schema_keys('ingestion-properties'):
            if prop in default_properties:
                response_dict[prop] = default_properties[prop]
            else:
                response_dict[prop] = ingestion_data[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('ingestion-properties'):
        if prop in ingestion_data:
            response_dict[prop] = ingestion_data[prop]

    return response_dict


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    from datetime import datetime
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - GET INGESTION COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['INGESTION_DYNAMODB_TABLE'] = 'feature-jek-DataLake-IngestionTable'

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "ingestion-id": "0083e8d8-54d6-11e9-89a1-33281b9325bb"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(lambda_event, MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - GET INGESTION COMMAND LAMBDA]")
