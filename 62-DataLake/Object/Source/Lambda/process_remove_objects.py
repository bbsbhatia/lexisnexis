import logging
import os
from concurrent.futures import ThreadPoolExecutor, as_completed
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch, xray_recorder
from botocore.exceptions import ClientError
from lng_aws_clients import s3, sqs
from lng_datalake_commons import session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException, \
    IgnoreEventException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.collection_blocker_table import CollectionBlockerTable
from lng_datalake_dal.event_store_backend_table import EventStoreBackendTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.tracking_table import TrackingTable

from service_commons import object_event_handler, ingestion

__author__ = "Shekhar Ralhan"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

CHUNK_SIZE = int(os.getenv("CHUNK_SIZE", 1000))
MAX_WORKERS = int(os.getenv("MAX_WORKERS", 2))

MESSAGE_RETRY_INDEXES_KEY = 'retry-indexes'
MESSAGE_START_CHUNK_KEY = 'start-chunk'
MAX_RETRY_COUNT = int(os.getenv("MAX_RETRY_COUNT", 1000))
TRACKING_QUERY_SIZE = int(os.getenv("TRACKING_QUERY_SIZE", 25))

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])
RETRY_EXCEPTIONS = ["ProvisionedThroughputExceededException", "InvalidClientTokenId", "ThrottlingException",
                    "InternalServerError", "500", "Throttling", "InternalError", "ServiceUnavailable"]


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return process_remove_objects(event, context)


def process_remove_objects(event: dict, context) -> str:
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param context: Lambda context
    """
    cb_resp = {}
    terminal_error_exception = None
    response_status = None
    message = {}
    try:
        lambda_arn = context.invoked_function_arn
        message = ingestion.get_message(event)
        collection_id = message['collection-id']
        event_id = message['event-id']
        request_time = message['request-time']

        object_event_handler.validate_message(message, event_names.OBJECT_BATCH_REMOVE, event_handler_version,
                                              lambda_arn)

        additional_message_attributes = message.get('additional-attributes', {})
        # Unload error handlers additional_attributes so we don't write inaccurate retry messages
        error_handler.additional_attributes = additional_message_attributes

        # make sure this request is for is the oldest Tracking item and is able to process (ie not locked)
        cb_resp = check_collection_blocker_item(collection_id, event_id, request_time)

        staging_bucket_name = message['bucket-name']
        batch_key = message['object-key']

        batch_properties = get_batch(staging_bucket_name, batch_key)
        # Get the object ids to remove
        object_ids = batch_properties['object-ids']
        logger.debug('Found total of {0} items to remove'.format(len(object_ids)))

        # Get start chunk of object ids to remove from additional attributes, otherwise start at 0
        start_chunk = message.get('additional-attributes', {}).get('start-chunk', 0)

        # Split the object ids list into CHUNK_SIZE chunks
        chunks = [object_ids[i:i + CHUNK_SIZE] for i in range(0, len(object_ids), CHUNK_SIZE)]

        # Get and retry any object ids that may have previously failed on tracking or backend event store insert
        retry_indexes = message.get('additional-attributes', {}).get(MESSAGE_RETRY_INDEXES_KEY, [])
        if retry_indexes:
            process_object_ids(chunks[start_chunk], start_chunk, message, retry_indexes)
            start_chunk = start_chunk + 1

        # Process object ids by inserting tracking and backend event store records
        for idx, chunk in enumerate(chunks[start_chunk:]):
            chunk_num = idx + start_chunk
            process_object_ids(chunk, chunk_num, message)

    except RetryEventException as e:
        try:
            unlock_collection_blocker_row(cb_resp)
            process_retry_exception(e, event)
        # will only raise a TerminalErrorException if we run out of retries
        except Exception as e2:
            terminal_error_exception = e2
    except IgnoreEventException as e:
        logger.debug("IgnoreEventException raised for event {0} throwing away : {1}".format(event, e))
    except (TerminalErrorException, Exception) as e:
        terminal_error_exception = e
    else:
        delete_collection_blocker_by_key(collection_id, request_time, event_id, message['stage'])
        response_status = event_handler_status.SUCCESS

    if terminal_error_exception:
        logger.error(terminal_error_exception)
        try:
            unlock_collection_blocker_row(cb_resp)
        except Exception as exc:
            # Only log the exception to the logger because we are already on our way to terminal
            logger.error(exc)
        error_handler.terminal_error(event, is_event=False, uid=message.get('event-id'),
                                     error_message=str(terminal_error_exception), context=context)

    # Used to guarantee integration test pass and errors aren't ignored.
    return response_status


def check_collection_blocker_item(collection_id: str, event_id: str, request_time: str) -> dict:
    """
    Get the oldest item from Collection Blocker Table by collection-id and verify it corresponds to the event_id
    This method is specific to the event handlers. The received event from event parameter is retried
    in case of a DDB throttle or is piped to the terminal errors bucket in case of a non throttle error.
    :param collection_id: Collection ID
    :param event_id
    :param request_time
    :return: Collection blocker item with lock status if successful, otherwise raise a RetryException
    """
    try:
        items = CollectionBlockerTable().query_items(max_items=TRACKING_QUERY_SIZE,
                                                     KeyConditionExpression='CollectionID = :collection_id',
                                                     ExpressionAttributeValues={":collection_id": {"S": collection_id}},
                                                     ConsistentRead=True,
                                                     ScanIndexForward=True)
    except error_handler.retryable_exceptions as e:
        logger.debug("Retry exception thrown while query collection blocker table: {0}".format(e))
        raise RetryEventException(
            "Retry exception thrown while querying Collection Blocker for {0} {1}".format(collection_id, e))
    except Exception as ex:
        raise TerminalErrorException(
            "Failed to query items in CollectionBlocker Table by collection-id: {} for event-id: {}||{}".format(
                collection_id, event_id, ex))

    if not items:
        logger.error("Collection blocker not found for Collection ID {}. Event-id: {}".format(collection_id, event_id))
        raise TerminalErrorException(
            "Collection blocker not found for Collection ID {}. Event-id: {}".format(collection_id, event_id))

    # Items are in order from oldest to latest order from the query
    for item in items:
        if item['request-id'] == event_id:
            return lock_collection_blocker_row(item, event_id)
        elif item['request-time'] > request_time:
            # If our event's request time is less than the oldest blocker it will never be latest so send to terminal
            raise TerminalErrorException(
                "Current collection blocker timestamp is greater than requests OldestTime: {0} EventsTime: {1}".format(
                    item['request-time'], request_time))
        elif item['event-name'] != event_names.OBJECT_BATCH_REMOVE:
            # if we haven't found our event-id and we hit an ingestion
            # create item, then we can't go yet and break out of loop
            break

    # Default if we don't find anything is retry
    raise RetryEventException('Collection {0} not ready to be processed, Retrying'.format(collection_id))


def lock_collection_blocker_row(item: dict, event_id: str) -> dict:
    # If collection blocker is locked, it's a duplicate event
    if item.get('event-status') == 'Locked':
        logger.error("Unable to lock CollectionBlocker item {0} for event={1}".
                     format(item, event_id))
        raise IgnoreEventException("CollectionBlocker is locked. Duplicate event")

    try:
        # Attempt to lock the collection blocker for the first time
        logger.debug("Attempting to lock {0}".format(item))
        item['event-status'] = 'Locked'
        CollectionBlockerTable().update_item(item,
                                             condition='attribute_exists(CollectionID) and '
                                                       'attribute_not_exists(EventStatus)')

    except ConditionError:
        logger.debug("Unable to lock CollectionBlocker for Event: {0}".format(event_id))
        raise IgnoreEventException("Locking CollectionBlocker failed due to conditional exception.")
    except error_handler.retryable_exceptions as e:
        logger.debug("Retry exception thrown while attempting to lock the CollectionBlocker table: {0}".format(e))
        raise RetryEventException(
            "Retry exception thrown while attempting to lock the CollectionBlocker table {0}".format(e))
    except Exception as e:
        raise TerminalErrorException(
            "Terminal Error thrown while locking the CollectionBlocker table: {0}".format(e))

    return item


def get_batch(staging_bucket_name: str, batch_key: str) -> dict:
    """
    Gets the remove objects batch file from s3. The Remove Objects Command Lambda creates the batch in s3.

    For example, if batch_key = 9d9cded7-6d52-11e9-a43a-1954b9953e01
        Location: feature-mas-dl-object-staging-288044017584-use1/batch/LATEST/9d9cded7-6d52-11e9-a43a-1954b9953e01
        Contents:  {"description": "Remove Batch Test", "object-ids": ["ObjID0", "ObjID1", "ObjID2", "ObjID3"}
    :param staging_bucket_name: Bucket Name to pull the data from
    :param batch_key: Batch ID key. AKA Request ID, Event ID
    """
    try:
        batch_properties = orjson.loads(s3.read_s3_object(staging_bucket_name, batch_key))
        logger.info("Read batch from s3://{0}/{1}".format(staging_bucket_name, batch_key))
        if 'description' in batch_properties:
            logger.info("Batch Remove Description: {0}".format(batch_properties['description']))
        return batch_properties
    except ClientError as ce:
        if ce.response['Error']['Code'] in ["NoSuchKey", "NoSuchBucket"]:
            raise TerminalErrorException("S3 object {} not found in bucket {}".format(batch_key, staging_bucket_name))
        raise RetryEventException("Unable to read batch s3://{}/{}||{}".format(staging_bucket_name, batch_key, ce))
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to read batch s3://{}/{}||{}".format(staging_bucket_name, batch_key, e))
    except Exception as ex:
        raise TerminalErrorException("Unhandled exception occurred||{}".format(ex))


def process_object_ids(object_ids: list, chunk_num: int, message: dict, index_list: list = None) -> None:
    """
    Multithreaded processing of a list of object IDs for removal.

    :param object_ids: List of object IDs for the chunk being processed
    :param chunk_num: Chunk number that is being processed
    :param message: Process Remove Objects event message
    :param index_list: Optional list of indexes to process within the object IDs list
    """
    indexes_to_retry = []

    # We can use a with statement to ensure threads are cleaned up promptly
    with ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
        index_list = index_list if index_list else [idx for idx in range(len(object_ids))]

        partial_batch_idx = CHUNK_SIZE * chunk_num

        futures_list = [executor.submit(do_work, object_ids[idx], idx, partial_batch_idx + idx, message) for idx in
                        index_list]

        for future in as_completed(futures_list):
            try:
                error_idx = future.result()
                if error_idx > -1:
                    indexes_to_retry.append(error_idx)
            except TerminalErrorException as te:
                raise te

    # Set additional attributes for this batch in case next batch has terminal error
    error_handler.set_message_additional_attribute(MESSAGE_RETRY_INDEXES_KEY, indexes_to_retry)
    error_handler.set_message_additional_attribute(MESSAGE_START_CHUNK_KEY, chunk_num)

    if indexes_to_retry:
        raise RetryEventException("Error processing indexes {} in chunk {}".format(indexes_to_retry, chunk_num))


def do_work(object_id: str, idx: int, batch_idx: int, message: dict) -> int:
    """
    Thread task that writes records to the Tracking and Event Store Backend tables.

    :param object_id: Object ID to process
    :param idx: Index within the chunk of the Object ID to process
    :param batch_idx: Index of the Object ID within the original batch list (need to generate a unique Event ID)
    :param message: Process Remove Objects event message
    """
    try:
        TrackingTable().put_item(generate_tracking_item(message, object_id, batch_idx))
        EventStoreBackendTable().put_item(generate_event_item(message, object_id, batch_idx))
    except ConditionError as ce:
        logger.error("ConditionError: object_id {0}||{1}".format(object_id, ce))
    except error_handler.retryable_exceptions as ex:
        logger.error("Error creating tracking and event store backend records for {0}||{1}".format(object_id, ex))
        return idx
    except Exception as e:
        raise TerminalErrorException("Unhandled exception occurred||{0}".format(e))

    return -1


def generate_tracking_item(message: dict, object_id: str, item_ndx: int) -> dict:
    """
    Gets a tracking item using information from the items list and event message.

    :param message: event message
    :param object_id: Object ID for tracking
    :param item_ndx: the index of the item to be extracted from the items list
    :returns dict: tracking table item dictionary
    """
    tracking_item = \
        {
            "tracking-id": tracker.generate_tracking_id("object", object_id, message['collection-id']),
            "request-time": message['request-time'],
            "event-id": ingestion.create_object_backend_event_id(message['event-id'], item_ndx),
            "event-name": event_names.OBJECT_REMOVE
        }
    if 'changeset-id' in message:
        tracking_item['changeset-id'] = message['changeset-id']
    return tracking_item


def generate_event_item(message: dict, object_id: str, item_ndx: int) -> dict:
    """
    Gets an event item using information from the items list and event message.

    Note that a put is used to "update" the object and/or object version tables so all the required and optional
    attributes normally provided by remov_object_command.py also need to be specified here.

    :param message: event message
    :param object_id: Object ID for remove event
    :param item_ndx: The index of the item to be extracted from the items list
    :returns dict: event dictionary
    """
    event_dict = {
        'event-id': ingestion.create_object_backend_event_id(message['event-id'], item_ndx),
        'request-time': message['request-time'],
        'event-name': event_names.OBJECT_REMOVE,
        'object-id': object_id,
        'collection-id': message['collection-id'],
        'event-version': message['event-version'],
        'stage': message['stage'],
        'old-object-versions-to-keep': message['old-object-versions-to-keep']
    }
    if 'pending-expiration-epoch' in message:
        event_dict['pending-expiration-epoch'] = message['pending-expiration-epoch']
    if 'changeset-id' in message:
        event_dict['changeset-id'] = message['changeset-id']
        event_dict['changeset-expiration-epoch'] = message['changeset-expiration-epoch']

    return event_dict


def unlock_collection_blocker_row(item: dict) -> None:
    event_status = item.pop('event-status', None)
    # only unlock if we have an event-status in the cb_resp to prevent writes to dynamo
    if event_status:
        try:
            # Set collection blocker event-status to Retry
            CollectionBlockerTable().update_item(item, condition='attribute_exists(CollectionID)')
        except Exception as e:
            # ToDo: Need to retry this event somehow
            raise TerminalErrorException(
                "Update collection blocker table failed | Retry Status: {} | Exception: {}".format(event_status, e))


# TODO: Possibly merge with ingestion.delete_collection_blocker
def delete_collection_blocker_by_key(collection_id: str, request_time: str, event_id: str, stage: str) -> None:
    """
    Remove an item from the CollectionBlocker Table by primary key
    :param collection_id: Collection ID
    :param request_time: Request Time
    :param event_id: Event ID
    :param stage: Stage
    """
    primary_key = {"collection-id": collection_id, "request-time": request_time}
    try:
        CollectionBlockerTable().delete_item(primary_key)
        logger.info("Successfully removed collection blocker item: {}".format(primary_key))
    except ClientError as e:
        if e.response["Error"]["Code"] in RETRY_EXCEPTIONS:
            logger.error("CollectionBlocker delete exception: {0} Sending to TrackingRemove Lambda".format(e))
            error_handler.throttle_error(
                {"collection-id": collection_id, "event-id": event_id, "stage": stage,
                 "request-time": request_time, "event-name": event_names.TRACKING_REMOVE},
                service="Tracking")
        else:
            raise TerminalErrorException("Failed to delete item from collection blocker table. Event ID = {0} |"
                                         " Exception = {1}".format(event_id, e))
    except error_handler.retryable_exceptions as e:
        logger.error("CollectionBlocker delete exception: {0} Sending to TrackingRemove Lambda".format(e))
        error_handler.throttle_error(
            {"collection-id": collection_id, "event-id": event_id, "stage": stage,
             "request-time": request_time, "event-name": event_names.TRACKING_REMOVE},
            service="ConnectionError")
    except Exception as e:
        raise TerminalErrorException(
            "Failed to delete item from collection blocker table. Event ID: {0} | Exception: {1}".format(event_id, e))


def process_retry_exception(e: RetryEventException, event: dict) -> None:
    logger.warning(e)
    sqs_url = ""
    try:
        arn = event['Records'][0]['eventSourceARN'].split(':')
        sqs_url = "https://sqs.{0}.amazonaws.com/{1}/{2}".format(arn[3], arn[4], arn[5])
        seen_count = 0
        if 'seen-count' in event['Records'][0]['messageAttributes']:
            seen_count = int(event['Records'][0]['messageAttributes']['seen-count'].get('stringValue', 0))
            if seen_count + 1 > MAX_RETRY_COUNT:
                raise TerminalErrorException("Event exceeded max retry count of {0}||{1}".format(MAX_RETRY_COUNT, e))
        # Back off 1 second for each retry
        sqs_delay = min(120 + seen_count, 900)  # 900 seconds is the longest delay supported by SQS
        event_body = event['Records'][0]['body']

        # If we have additional attributes, load the sqs body, extract the Message
        # add the additional attributes, and redump the data to overwrite the event_body to send
        if error_handler.additional_attributes:
            json_event_body = orjson.loads(event_body)
            event_message = orjson.loads(json_event_body['Message'])
            event_message['additional-attributes'] = error_handler.additional_attributes
            json_event_body['Message'] = orjson.dumps(event_message).decode()
            event_body = orjson.dumps(json_event_body).decode()

        sqs.get_client().send_message(QueueUrl=sqs_url,
                                      MessageBody=event_body,
                                      MessageAttributes={'seen-count': {
                                          'DataType': 'Number',
                                          'StringValue': str(seen_count + 1)}},
                                      DelaySeconds=sqs_delay)
    except TerminalErrorException as e2:
        raise e2
    # If we throw any exception here just throw the original Retry exception we received.
    except Exception as inner:
        logger.warning("Error sending SQS message back to queue: {0}||{1}".format(sqs_url, inner))
        raise e


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import random
    import string


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - PROCESS REMOVE OBJECT EVENT HANDLER LAMBDA]")

    os.environ["AWS_PROFILE"] = "c-sand"

    ASSET_GROUP = 'feature-mas'
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "{0}-datalake-lambda-terminal-errors-288044017584".format(
        ASSET_GROUP)

    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{0}-DataLake-OwnerTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_BACKEND_DYNAMODB_TABLE'] = '{0}-DataLake-EventStoreBackendTable'.format(ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = "{0}-DataLake-TrackingTable".format(ASSET_GROUP)

    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    # event-id = base file name from s3://feature-sxr-dl-object-staging-288044017584-use1/batch/LATEST

    msg = {
        "collection-id": 'mine',
        "bucket-name": "{0}-dl-object-staging-use1".format(ASSET_GROUP),
        "event-id": "CEwJTjDgckJVKjPQ",
        "object-key": "batch/LATEST/CEwJTjDgckJVKjPQ",
        "request-time": "2019-11-06T19:54:10.513Z",
        "event-name": event_names.OBJECT_BATCH_REMOVE,
        "description": "Batch Remove Collection",
        "event-version": 1,
        "stage": "LATEST",
        "old-object-versions-to-keep": 3
    }

    sample_event = {
        "Message": json.dumps(msg)
    }

    input_event = {
        "Records": [
            {
                "messageId": "4ea95117-fea8-4073-99e3-b10f56f6ca91",
                "receiptHandle": "AQEBAoJDW52N+Lv4RTd4Mp5MCrivHEULAoVaLYzNeTBRwi62hTwQYEp9Ne0AllrS3JbArYkC3MlFebEG3SHhb0GnpyVJ0jwfSxp4F4FIoOCloVKNYw6DwSz8Q1+URrVGrZZg8QIf3xxGn/LQkBvLYtQq9+uI2D63U5TI2oFmC3RzZ5LoRx01dBPS8us2sRDmwa8rumihascg790msmJ9AAgm4vTadLKEvbtOxpx3jaCZU9DIAbvldJbB+T13oWGTowF4pOczFcYty/VJjl5r69oqKBfNjbehpIKol1PPEVDOqsbnrRf/sCBTLrAZi3WtEjL7xSwDSqXicR90EnYKiVaxyaSVk1ROgyWf/wI132+Uruo7lfIWhf/Xfja3l9ev4k+2zC3wy0ix6tJCPNp+1Kv0Cq41AMO8ZRJkytPrwZbo58hvAh200M9oSqkF8Mexxt66GGo4IcWL6eoXeQNVVuQ7sgEMDDOzzHh1YhPqUcvQNkE=",
                "body": json.dumps(sample_event),
                "attributes": {
                    "ApproximateReceiveCount": "12",
                    "SentTimestamp": "1551753965006",
                    "SenderId": "AIDAIT2UOQQY3AUEKVGXU",
                    "ApproximateFirstReceiveTimestamp": "1551753965006"
                },
                "messageAttributes": {},
                "md5OfBody": "2adf1c719efaf508bbe720464ef53318",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn:aws:sqs:us-east-1:288044017584:{0}-LATEST-object-ProcessRemoveObjects-Queue".format(
                    ASSET_GROUP),
                "awsRegion": "us-east-1"
            }
        ]
    }

    xray_recorder.context._context_missing = "LOG_ERROR"
    print(lambda_handler(input_event, MockLambdaContext()))
    logger.debug("[LOCAL RUN END - PROCESS REMOVE OBJECTS EVENT HANDLER LAMBDA]")
