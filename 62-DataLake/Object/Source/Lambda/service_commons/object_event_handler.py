import logging
import os
from concurrent.futures import ThreadPoolExecutor, as_completed

import orjson
from botocore.exceptions import ClientError
from functools import wraps
from lng_aws_clients import s3, sqs
from lng_datalake_commons import sqs_extractor, publish_sns_topic, time_helper, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import ingestion_counter, event_names
from lng_datalake_dal.changeset_object_table import ChangesetObjectTable
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.object_store_table import ObjectStoreTable
from lng_datalake_dal.object_store_version_table import ObjectStoreVersionTable

from service_commons import object_common

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# initializing the thread pool with 2 threads as we have 2 buckets available
THREAD_POOL = ThreadPoolExecutor(2)

INGESTION_COUNTER_QUEUE_URL = os.getenv('INGESTION_COUNTER_QUEUE_URL')
MESSAGE_SNS_PUBLISH_DATA_KEY = 'sns-publish-data'


def queue_wrapper(func):
    @wraps(func)
    def func_wrapper(event, context):
        try:
            logger.debug("Sqs Event: {0}".format(event))
            logger.debug("Sqs Length: {0}".format(len(event['Records'])))

            resp = []
            for record in event['Records']:
                # Need to form event that looks like AWS SNS for downstream data
                base_sns_event = {'EventSource': 'aws::sns', 'EventVersion': '1.0', 'EventSubscriptionArn': 'SQS'}

                sns_event = sqs_extractor.extract_sqs_message(record)
                base_sns_event['Sns'] = sns_event
                new_event = {'Records': [base_sns_event]}
                resp.append(func(new_event, context))

            return resp

        except Exception as e:
            logger.warning("Error extracting queue message: {}".format(e))
            return func(event, context)

    return func_wrapper


def copy_object_to_datalake(source_bucket_name: str, datalake_buckets: list, object_key: str, object_meta: dict,
                            version_number: int, source_key=None) -> None:
    """
    Copies s3 object from temporary  bucket to datalake bucket. This method uses the boto3 s3 client directly

    :param source_bucket_name: Source s3 bucket to copy the data from
    :param datalake_buckets: destination s3 buckets to copy the data to
    :param object_key: Key of the s3 object to copy to
    :param object_meta: collection-id, content-type, and content-length
    :param version_number: version
    :param source_key: Key of the s3 object to copy from
    """
    try:
        if not source_key:
            source_key = object_key
        # created_timestamp = item['timestamp'] Maybe needed in the s3.copy() to pick Unmodified objects time.
        copy_source = {'Bucket': source_bucket_name, 'Key': source_key}
        move_object_to_datalake_bucket(copy_source, datalake_buckets, object_key, object_meta, version_number)

    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed copy_object_to_datalake due to unknown reasons. "
            "Source = {0}  Destinations = {1}  Key = {2}  Exception = {1}".format(source_bucket_name,
                                                                                  datalake_buckets, object_key,
                                                                                  e))


def move_object_to_datalake_bucket(copy_source, destination_buckets, object_store_key,
                                   object_meta, version_number):
    futures = []
    # Initialize the client just in case before threading
    s3.get_client()
    for bucket in destination_buckets:
        futures.append(THREAD_POOL.submit(copy_obj, copy_source, bucket, object_store_key, object_meta, version_number))

    for x in as_completed(futures):
        exception = x.exception()
        if exception:
            logger.error("Error while copying data from {0} to {1} {2}: {3}".format(copy_source, destination_buckets,
                                                                                    object_store_key, exception))
            # TODO: Remove once we figure out why we are getting AccessDenied errors
            if isinstance(exception, ClientError):
                logger.error(exception.response)
            if isinstance(exception, error_handler.retryable_exceptions):
                raise exception
            raise Exception("Error while copying data: {0}".format(exception))


def copy_obj(copy_source, dest_bucket, dest_key, object_meta, version_number):
    logger.info('Copying object into datalake from {0} to bucket: {1} key: {2}'.format(copy_source,
                                                                                       dest_bucket,
                                                                                       dest_key))
    metadata = object_common.build_object_metadata(object_meta['collection-id'],
                                                   object_meta['object-id'],
                                                   object_meta['request-time'],
                                                   version_number,
                                                   object_meta['content-sha1'],
                                                   object_meta.get('object-metadata', {}))

    s3.get_client().copy(copy_source, dest_bucket, dest_key,
                         ExtraArgs={
                             'ContentType': object_meta['content-type'],
                             'Metadata': metadata,
                             'MetadataDirective': 'REPLACE'
                         })


def is_object_in_datalake(datalake_bucket_name: str, object_key: str) -> bool:
    try:
        s3.get_client().head_object(Bucket=datalake_bucket_name, Key=object_key)
    except ClientError as e:
        if e.response['Error']['Message'] == "Not Found":
            return False
        raise e
    except error_handler.retryable_exceptions as e:
        raise e
    return True


def get_consistent_object_item(object_id: str, collection_id: str) -> dict:
    try:
        return ObjectStoreTable().get_item({'collection-id': collection_id,
                                            'object-id': object_id}, ConsistentRead=True)

    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to get item from ObjectStoreTable. Collection-id {0} Object-id {1} Exception={2}".format(
                collection_id, object_id, e))


def update_ingestion(message: dict) -> None:
    """
    If we have ingestion-id in input dict, then send a message to INGESTION_COUNTER_QUEUE to update ObjectUpdatedCount
    If this is a retry event and the ObjectUpdatedCount was already updated for this object then no update is necessary
    :param message: input message to event handler
    """

    if 'ingestion-id' in message:
        ingestion_id = message["ingestion-id"]
        message = {ingestion_id: {ingestion_counter.UPDATED: 1}}
        try:
            sqs.get_client().send_message(QueueUrl=INGESTION_COUNTER_QUEUE_URL,
                                          MessageBody=orjson.dumps(message).decode())
        except error_handler.retryable_exceptions as e:
            logger.warning("Unable to publish ObjectUpdatedCount to SQS {}||{}".format(INGESTION_COUNTER_QUEUE_URL, e))
        except Exception as ex:
            logger.error("Failed to publish ObjectUpdatedCount to SQS {}||{}".format(INGESTION_COUNTER_QUEUE_URL, ex))


def send_object_notifications(sns_publish_data: dict, object_store_item: dict, schemas: list, event: dict,
                              target_arn: str, catalog_ids: list, changeset_id: str) -> None:
    """
    Generate a notification message for each schema and, if two schemas are specified, a third message with the
    concatenated schemas is also published.
    :param sns_publish_data: In case of a retry event, this param contains the generated messages pending of publishing
    :param object_store_item: Item from the Object Store Table
    :param schemas: List of schemas to generate the notifications
    :param event: Object event message
    :param target_arn: Target ARN
    :param catalog_ids: List of catalog ids containing the collection
    :param changeset_id: Changeset that this Object is participating in
    """
    notification_attributes = {
        'event-name': event['event-name'],
        'collection-id': [object_store_item['collection-id']]}
    if catalog_ids:
        notification_attributes['catalog-id'] = catalog_ids

    if sns_publish_data:
        notifications = sns_publish_data["notifications"]
        return publish_notifications(notifications, notification_attributes, target_arn)

    notifications = []
    concat_notification = {}
    schema_versions = []

    # TODO: delete this when the v0 schema is removed
    # No-op events do not publish to v0 schema or the v0v1 concat schema
    if event['event-name'] == event_names.OBJECT_UPDATE_NO_CHANGE:
        publish_schemas = [schemas[-1]]
    else:
        publish_schemas = schemas

    logger.info("Publishing object notifications to: {0}".format(publish_schemas))
    for schema_rel_path in publish_schemas:
        schema_version = schema_rel_path.split('/')[2]
        notification = generate_object_notification(schema_rel_path, schema_version, object_store_item,
                                                    event, changeset_id)
        notifications.append(
            {"schema-rel-path": schema_rel_path, "schema-version": schema_version, "message": notification})
        if len(publish_schemas) > 1:
            schema_versions.append(schema_version)
            if schema_version == 'v0':
                concat_notification.update(notification)
            else:
                concat_notification[schema_version] = notification

    if concat_notification:
        notifications.append({"schema-version": ''.join(sorted(schema_versions)), "message": concat_notification})

    publish_notifications(notifications, notification_attributes, target_arn)


def publish_notifications(notifications: list, notification_attributes: dict, target_arn: str) -> None:
    for index, notification in enumerate(notifications):
        try:
            publish_sns_topic.publish_to_topic(notification, notification_attributes, target_arn)
        except error_handler.retryable_exceptions as e:
            logger.warning("{} exception when sending message {} to "
                           "SNS topic {}||{}".format(type(e).__name__, notification, target_arn, e))
            retry_info = {"notifications": notifications[index:]}
            error_handler.set_message_additional_attribute(MESSAGE_SNS_PUBLISH_DATA_KEY, retry_info)
            raise e


def generate_object_notification(schema_file: str, schema_version: str, object_store_item: dict,
                                 event: dict, changeset_id: str) -> dict:
    notification_schema = publish_sns_topic.load_json_schema(schema_file)

    # construct "object"
    required_props, optional_props = get_notification_schema_props(notification_schema, schema_version)
    object_msg = {}
    try:
        default_properties = {
            "event-name": event['event-name'],
            "item-state": object_store_item['object-state'],
            "S3": {"Key": object_store_item['object-key'], "Bucket": object_store_item['bucket-name']},
            "object-key-url": object_common.build_object_key_url(object_store_item['object-key'],
                                                                 object_store_item['content-type'])
        }
        for prop in required_props:
            if prop in default_properties:
                object_msg[prop] = default_properties[prop]
            else:
                object_msg[prop] = object_store_item[prop]
    except KeyError as e:
        raise TerminalErrorException("Failed to generate object notification. Missing required property: {}".format(e))

    for prop in optional_props:
        if prop == "object-expiration-date" and "pending-expiration-epoch" in object_store_item:
            object_msg[prop] = time_helper.format_time(object_store_item["pending-expiration-epoch"])
        elif prop in object_store_item:
            object_msg[prop] = object_store_item[prop]

    if schema_version == 'v0':
        return object_msg

    # for schema_version v1 and later
    context_msg = build_notification_context(notification_schema, event)

    notification = {"SchemaVersion": notification_schema['properties']['SchemaVersion']["enum"][0],
                    "Schema": notification_schema['properties']['Schema']["enum"][0],
                    "Type": "Publish",
                    "context": context_msg,
                    "object": object_msg}

    if changeset_id:
        notification['changeset-object'] = build_changeset_object_notification(changeset_id,
                                                                               notification_schema,
                                                                               object_store_item)
    return notification


def build_notification_context(notification_schema: dict, event: dict) -> dict:
    required_context_props, optional_context_props = get_notification_schema_context_props(notification_schema)
    context_msg = {}
    try:
        # In the case of ingestion/batch remove, we add to the event id so remove the last portion
        request_id = event['event-id']
        request_id_split = request_id.split('-')
        if len(request_id_split) == 6:
            request_id = '-'.join(request_id_split[:5])

        default_context_properties = {
            "request-id": request_id
        }
        for prop in required_context_props:
            if prop in default_context_properties:
                context_msg[prop] = default_context_properties[prop]
            else:
                context_msg[prop] = event[prop]

    except KeyError as e:
        raise TerminalErrorException(
            "Failed to generate object notification context. Missing required property: {}".format(e))

    # "context" currently has no optional props
    for prop in optional_context_props:
        if prop in event:
            context_msg[prop] = event[prop]

    return context_msg


def build_changeset_object_notification(changeset_id: str, notification_schema: dict,
                                        object_store_item: dict) -> dict:
    required_changeset_object_props, optional_changeset_object_props = get_notification_schema_changeset_object_props(
        notification_schema)
    changeset_object_msg = {}
    try:
        changeset_object_key = object_common.generate_changeset_object_key(changeset_id,
                                                                           object_store_item['object-key'])
        default_properties = {
            'S3': {
                'Key': changeset_object_key,
                'Bucket': object_store_item['bucket-name']
            },
            'object-key-url': object_common.build_object_key_url(changeset_object_key,
                                                                 object_store_item['content-type'])
        }
        for prop in required_changeset_object_props:
            if prop == "changeset-id":
                changeset_object_msg["changeset-id"] = changeset_id
            elif prop in object_store_item:
                changeset_object_msg[prop] = object_store_item[prop]
            else:
                changeset_object_msg[prop] = default_properties[prop]

    except KeyError as e:
        raise TerminalErrorException(
            "Failed to generate changeset object notification. Missing required property: {}".format(e))

    for prop in optional_changeset_object_props:
        if prop in object_store_item:
            changeset_object_msg[prop] = object_store_item[prop]

    return changeset_object_msg


def get_notification_schema_props(schema: dict, schema_version: str) -> tuple:
    # object props
    if schema_version == 'v0':
        required_props = schema['required']
        all_props = schema['properties'].keys()
    else:
        required_props = schema['properties']['object']['required']
        all_props = schema['properties']['object']['properties'].keys()
    optional_props = [p for p in all_props if p not in required_props]
    return required_props, optional_props


def get_notification_schema_context_props(schema: dict) -> tuple:
    # context props
    required_context_props = schema['properties']['context']['required']
    all_context_props = schema['properties']['context']['properties'].keys()
    optional_context_props = [p for p in all_context_props if p not in required_context_props]
    return required_context_props, optional_context_props


def get_notification_schema_changeset_object_props(schema: dict) -> tuple:
    # changeset-object props
    required_changeset_object_props = schema['properties']['changeset-object']['required']
    all_changeset_object_props = schema['properties']['changeset-object']['properties'].keys()
    optional_changeset_object_props = [p for p in all_changeset_object_props if
                                       p not in required_changeset_object_props]
    return required_changeset_object_props, optional_changeset_object_props


def generate_object_store_version_item(object_store_item: dict, event_id: str) -> dict:
    # Use {} vs. dict() for efficiency
    new_dict = {}
    key = None

    try:
        for key in ObjectStoreVersionTable().get_required_schema_keys():
            if key == 'composite-key':
                new_dict[key] = ObjectStoreVersionTable.generate_composite_key(object_store_item['object-id'],
                                                                               object_store_item['collection-id'])
            elif key == 'event-id':
                new_dict[key] = event_id
            # event item stores the staging bucket so we must use separate parameter for bucket name
            else:
                new_dict[key] = object_store_item[key]
    except Exception as e:
        logger.error(e)
        raise TerminalErrorException("Missing required key: {0}".format(key))
    for key in ObjectStoreVersionTable().get_optional_schema_keys():
        if key in object_store_item:
            new_dict[key] = object_store_item[key]

    return new_dict


def validate_message(message: dict, event_name: str, event_handler_version: int, lambda_arn: str) -> None:
    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_name):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name does not match {}".format(message, event_name))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(message, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage does not match {}".format(message, lambda_arn))


def send_changeset_notifications(sns_publish_data: dict, changeset_item: dict, schemas: list, event: dict,
                                 target_arn: str, collection_ids: list, catalog_ids: list) -> None:
    """
    Generate a notification message for each schema and, if two schemas are specified, a third message with the
    concatenated schemas is also published.
    :param sns_publish_data: In case of a retry event, this param contains the generated messages pending of publishing
    :param changeset_item: Item constructed of a changeset
    :param schemas: List of schemas to generate the notifications
    :param event: Changeset event message
    :param target_arn: Target ARN
    :param collection_ids: IDs of collections to publish a notification
    :param catalog_ids: List of catalog ids containing the collection
    """
    notification_attributes = {
        'event-name': event['event-name'],
        'changeset-id': changeset_item['changeset-id']
    }
    if collection_ids:
        notification_attributes['collection-id'] = collection_ids
    if catalog_ids:
        notification_attributes['catalog-id'] = catalog_ids
    if sns_publish_data:
        notifications = sns_publish_data["notifications"]
        return publish_notifications(notifications, notification_attributes, target_arn)

    notifications = []

    logger.info("Publishing changeset notifications to: {0}".format(schemas))
    for schema_rel_path in schemas:
        schema_version = schema_rel_path.split('/')[2]
        notification = generate_changeset_notification(schema_rel_path, changeset_item,
                                                       event, collection_ids, catalog_ids)
        notifications.append(
            {"schema-rel-path": schema_rel_path, "schema-version": schema_version, "message": notification})

    publish_notifications(notifications, notification_attributes, target_arn)


def generate_changeset_notification(schema_file: str, changeset_item: dict,
                                    event: dict, collections: list, catalogs: list) -> dict:
    notification_schema = publish_sns_topic.load_json_schema(schema_file)

    # construct "changeset"
    required_props, optional_props = get_changeset_notification_schema_props(notification_schema)
    changeset_msg = {}
    try:
        default_properties = {
            "event-name": event['event-name'],
            "changeset-expiration-date": time_helper.format_time(changeset_item["pending-expiration-epoch"])
        }
        for prop in required_props:
            if prop in default_properties:
                changeset_msg[prop] = default_properties[prop]
            else:
                changeset_msg[prop] = changeset_item[prop]
    except KeyError as e:
        raise TerminalErrorException(
            "Failed to generate changeset notification. Missing required property: {}".format(e))

    for prop in optional_props:
        if prop == 'collection-ids' and collections:
            changeset_msg[prop] = collections
        elif prop == 'catalog-ids' and catalogs:
            changeset_msg[prop] = catalogs
        elif prop in changeset_item:
            changeset_msg[prop] = changeset_item[prop]

    # for schema_version v1 and later
    context_msg = build_notification_context(notification_schema, event)

    return {"SchemaVersion": notification_schema['properties']['SchemaVersion']["enum"][0],
            "Schema": notification_schema['properties']['Schema']["enum"][0],
            "Type": "Publish",
            "context": context_msg,
            "changeset": changeset_msg}


def get_changeset_notification_schema_props(schema: dict) -> tuple:
    required_props = schema['properties']['changeset']['required']
    all_props = schema['properties']['changeset']['properties'].keys()
    optional_props = [p for p in all_props if p not in required_props]
    return required_props, optional_props


def process_staging_object(message: dict, object_destination_buckets: dict, version_number: int,
                           existing_key: str = None) -> None:
    """
    Copy object from staging if needed and add tags to staging object. If large object then put ACL permissions.

    :param message: Object Event message
    :param object_destination_buckets: buckets to put the objects
    :param version_number: version number of the new object
    :param existing_key: true when an object is being updated without changes, needed for changeset objects
    """
    object_key = message['object-key']

    # Don't attempt to copy the object into the lake if it's not a large object
    if not message.get('is-large-object-request', False) and 'ingestion-id' not in message:
        if not existing_key:
            copy_object_to_datalake(message['bucket-name'],
                                    [x for x in object_destination_buckets.values()],
                                    object_key,
                                    message,
                                    version_number)
        if 'changeset-id' in message:
            changeset_object_key = object_common.generate_changeset_object_key(message['changeset-id'],
                                                                               existing_key if existing_key else
                                                                               message[
                                                                                   'object-key'])
            copy_object_to_datalake(message['bucket-name'],
                                    [x for x in object_destination_buckets.values()],
                                    changeset_object_key,
                                    message,
                                    version_number,
                                    object_key)


def insert_object_store_table(object_store_item: dict) -> bool:
    """
    Inserts object into ObjectStore table.

    :param object_store_item:  item to be inserted
    :return bool: object successfully inserted and was not a duplicate
    """
    no_object_changes = False

    try:
        ObjectStoreTable().put_item(object_store_item)
        logger.info("Insert to Object Store table succeeded.")
    except (SchemaValidationError, SchemaError, SchemaLoadError) as e:
        raise TerminalErrorException("Failed to insert item into Object Store table. Item = {0} | Exception = {1}"
                                     .format(object_store_item, e))
    except ConditionError as ce:
        logger.debug('ConditionError: {}'.format(ce))
        # TODO: Look at how  to actually handle this logic given we get conditional errors occasionally on the first insert
        # no_object_changes = True
        raise TerminalErrorException('ConditionError inserting to OST: {0}'.format(object_store_item))

    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Failed to insert item into Object Store table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(object_store_item, e))

    return no_object_changes


def update_object_store_table(object_store_item: dict) -> bool:
    """
    Updates object in ObjectStore table.

    :param object_store_item:  item to be updated
    :return bool: object successfully updated and was not a duplicate
    """
    no_object_changes = False

    try:
        ObjectStoreTable().update_item(object_store_item)
        logger.info("Update to Object Store table succeeded.")
    except (SchemaValidationError, SchemaError, SchemaLoadError) as e:
        raise TerminalErrorException("Failed to update item in Object Store table. Item = {0} | Exception = {1}"
                                     .format(object_store_item, e))
    except ConditionError as ce:
        logger.debug('ConditionError: {}'.format(ce))
        # no_object_changes = True
        # TODO: Look at how  to actually handle this logic given we get conditional errors occasionally on the first insert
        # no_object_changes = True
        raise TerminalErrorException('ConditionError inserting to OST: {0}'.format(object_store_item))

    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Failed to update item in Object Store table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(object_store_item, e))

    return no_object_changes


def insert_object_store_version_table(object_store_version_item: dict) -> None:
    """
    Inserts object into ObjectStoreVersion table.

    :param object_store_version_item:  item to be inserted
    """
    try:
        ObjectStoreVersionTable().put_item(object_store_version_item)
        logger.info("Insert to Object Store Version table succeeded.")
    except (SchemaValidationError, SchemaError, SchemaLoadError) as e:
        raise TerminalErrorException("Failed to insert item into Object Store Version table. "
                                     "Item = {0} | Exception = {1}".format(object_store_version_item, e))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to insert item into Object Store Version table due to unknown reasons. "
            "Item = {0} | Exception = {1}".format(object_store_version_item, e))


def delete_object_store_version_table(object_store_version_item: dict, old_object_versions_to_keep: int) -> None:
    """
    Deletes old version of object from ObjectStoreVersion table.

    :param object_store_version_item:  current version of item for which old version will be deleted
    :param old_object_versions_to_keep: number of old versions to keep of item
    """
    if 0 <= old_object_versions_to_keep < (object_store_version_item['version-number'] - 1):
        last_kept_version = object_store_version_item['version-number'] - (old_object_versions_to_keep + 1)
        if last_kept_version > 0:
            try:
                ObjectStoreVersionTable().delete_item({'composite-key': object_store_version_item['composite-key'],
                                                       'version-number': last_kept_version})
            except error_handler.retryable_exceptions as e:
                raise e
            except Exception as e:
                raise TerminalErrorException(
                    "Failed to delete item from Object Store Version table due to unknown reasons. "
                    "Item = {0} | Exception = {1}".format(object_store_version_item, e))


def changeset_update(object_store_item: dict, message: dict) -> None:
    """
    places a new record in the Changeset Object Table if there is a changeset-id in the message
    we are not worried about putting the same object version multiple times because the sort key is the changeset-id
    if identical objects come through with the same changeset-id we will put over the existing record but that is okay
    :param object_store_item: the generated object store item
    :param message: the initial message received by the event handler
    :return: nothing, end immediately if no changeset-id in message
    """
    if 'changeset-id' not in message:
        return
    logger.debug('Found changeset-id {0} for event-id {1}'.format(message['changeset-id'], message['event-id']))
    changeset_object_item = {}
    prop = None
    try:

        for prop in ChangesetObjectTable().get_required_schema_keys():

            if prop == 'pending-expiration-epoch':
                changeset_object_item[prop] = message['changeset-expiration-epoch']
            elif prop == 'composite-key':
                changeset_object_item[prop] = ChangesetObjectTable.generate_composite_key(
                    object_store_item['object-id'],
                    object_store_item['collection-id'],
                    object_store_item['version-number'])
            elif prop == 'object-key':
                changeset_object_item[prop] = object_common.generate_changeset_object_key(message['changeset-id'],
                                                                                          object_store_item[
                                                                                              'object-key'])
            elif prop in object_store_item:
                changeset_object_item[prop] = object_store_item[prop]
            else:
                changeset_object_item[prop] = message[prop]
    except Exception:
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in ChangesetObjectTable().get_optional_schema_keys():
        if prop in object_store_item:
            changeset_object_item[prop] = object_store_item[prop]
        elif prop in message:
            changeset_object_item[prop] = message[prop]

    try:
        ChangesetObjectTable().put_item(dict_items=changeset_object_item)
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to put item to Changeset Object Version table due to unknown reasons. "
            "Item = {0} | Exception = {1}".format(changeset_object_item, e))
