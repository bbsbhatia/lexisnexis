import base64
import logging
import os

import orjson
from botocore.exceptions import ClientError
from lng_aws_clients import s3, sts
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, SemanticError, \
    UnhandledException, NoSuchCollection, NoSuchObject, NoSuchRelationship, NoSuchObjectVersion, NotAuthorizedError
from lng_datalake_commons import time_helper
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import collection_status, object_status, event_names, relationship_status, changeset_status
from lng_datalake_dal.changeset_table import ChangesetTable
from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.object_store_table import ObjectStoreTable
from lng_datalake_dal.object_store_version_table import ObjectStoreVersionTable
from lng_datalake_dal.relationship_owner_table import RelationshipOwnerTable
from lng_datalake_dal.tracking_table import TrackingTable
from lng_datalake_dal.changeset_object_table import ChangesetObjectTable

from service_commons import object_common

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Constants for encoding type
UTF_8_ENCODING = "utf-8"
BASE_64_ENCODING = "base64"


def get_object_content(request: dict) -> bytes:
    body = request['body']
    body_encoding = request.get('body-encoding', UTF_8_ENCODING).lower()

    if body_encoding == BASE_64_ENCODING:
        object_content = base64.b64decode(body)
    elif body_encoding == UTF_8_ENCODING:
        object_content = bytes(body, encoding=UTF_8_ENCODING)
    else:
        raise InternalError("Unhandled body-encoding value of {0}".format(body_encoding))

    return object_content


def unpack_folder_object_transaction_id(transaction_id: str) -> dict:
    try:
        parts = orjson.loads(base64.b64decode(transaction_id))
    except Exception:
        raise InvalidRequestPropertyValue("Invalid Transaction ID {0}".format(transaction_id))
    return parts


def create_multipart_upload(request_info: dict, collection_response: dict, pending_expiration_epoch,
                            bucket_name: str) -> dict:
    metadata_dict = {
        'event-id': request_info['request-id'],
        'collection-id': collection_response['collection-id'],
        'collection-hash': collection_response['collection-hash'],
        'old-object-versions-to-keep': str(collection_response['old-object-versions-to-keep']),
        'stage': request_info['stage']}

    if 'object-id' in request_info:
        metadata_dict['object-id'] = request_info['object-id']
    if 'event-name' in request_info:
        metadata_dict['event-name'] = request_info['event-name']
    if 'request-time' in request_info:
        metadata_dict['request-time'] = request_info['request-time']
    if 'object-metadata' in request_info:
        metadata_dict['object-metadata'] = orjson.dumps(request_info['object-metadata']).decode()
    if pending_expiration_epoch:
        metadata_dict["pending-expiration-epoch"] = str(pending_expiration_epoch)
    if 'changeset-id' in request_info:
        metadata_dict['changeset-id'] = request_info['changeset-id']
        metadata_dict['changeset-expiration-epoch'] = str(request_info['changeset-expiration-epoch'])

    resp = s3.get_client().create_multipart_upload(ACL='bucket-owner-full-control', Bucket=bucket_name,
                                                   ContentType=request_info['content-type'],
                                                   Key=request_info['object-key'],
                                                   Metadata=metadata_dict)
    return {'S3': {'Bucket': bucket_name, 'Key': request_info['object-key']}, 'UploadId': resp['UploadId']}


def get_collection_response(collection_id):
    try:
        collection_response = CollectionTable().get_item({'collection-id': collection_id})
    except ClientError as ce:
        logger.error(ce)
        raise InternalError("Unable to get item from Collection Table", ce)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)

    if not collection_response:
        raise NoSuchCollection("Invalid Collection ID {0}".format(collection_id),
                               "Collection ID does not exist")

    # TODO: below can be removed once collection-hash is populated in CollectionTable for all collections
    if 'collection-hash' not in collection_response:
        collection_response['collection-hash'] = object_common.build_collection_id_hash(
            collection_response['collection-id'])

    # These attributes are required in the Collection table
    collection_state = collection_response['collection-state']

    # The collection state must be CREATED to create/update/remove objects
    if collection_state != collection_status.CREATED:
        raise SemanticError(
            "Collection ID {0} is not set to {1} (state={2})".format(
                collection_id,
                collection_status.CREATED,
                collection_state))

    return collection_response


def get_object_response(object_id: str, collection_id: str, version_number: int = None) -> dict:
    object_response = get_object_item(object_id, collection_id)

    if not object_response:
        raise NoSuchObject("Invalid Object ID {0} in Collection ID {1}".format(object_id, collection_id),
                           "Object ID {0} does not exist".format(object_id))

    if not version_number or version_number == object_response['version-number']:
        if object_response['object-state'] == object_status.REMOVED:
            raise SemanticError("Object ID {0} in Collection ID {1} has been removed".format(object_id, collection_id),
                                "Removed objects cannot be retrieved")

        if object_common.is_expired_object(object_response, command_wrapper.get_request_time()):
            raise NoSuchObject("Invalid Object ID {0} in Collection ID {1}".format(object_id, collection_id),
                               "Object ID {0} does not exist".format(object_id))
    return object_response


def get_changeset_object_response(object_id: str, collection_id: str, changeset_id: str, version_number: int) -> dict:
    object_response = get_changeset_object_item(object_id, collection_id, changeset_id, version_number)

    if not object_response or object_common.is_expired_object(object_response, command_wrapper.get_request_time()):
        raise NoSuchObject("Invalid Object ID {0} in Changeset ID {1}".format(object_id, changeset_id),
                           "Object ID {0} does not exist.".format(object_id))

    if object_response['object-state'] == object_status.REMOVED:
        raise SemanticError("Object ID {0} in Changeset ID {1} has been removed".format(object_id, changeset_id),
                            "Removed objects cannot be retrieved")

    return object_response


def get_object_version_response(object_id: str, collection_id: str, version_number: int) -> dict:
    composite_key = ObjectStoreVersionTable().generate_composite_key(object_id,
                                                                     collection_id)
    try:
        object_version_response = ObjectStoreVersionTable().get_item({
            "composite-key": composite_key,
            "version-number": version_number
        })
    except ClientError as e:
        raise InternalError("Unable to get item from Object Store Version Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not object_version_response:
        raise NoSuchObjectVersion(
            "Invalid Version Number {0} for Object ID {1} in Collection ID {2}".format(version_number,
                                                                                       object_id, collection_id),
            "Object Version does not exist")

    return object_version_response


def get_relationship_response(relationship_id: str):
    try:
        relationship_response = RelationshipOwnerTable().get_item({'relationship-id': relationship_id})
    except ClientError as ce:
        raise InternalError("Unable to get item from Relationship Owner Table", ce)
    except Exception as ex:
        raise UnhandledException(ex)

    if not relationship_response:
        raise NoSuchRelationship("Invalid Relationship ID {0}".format(relationship_id),
                                 "Relationship ID does not exist")

    relationship_state = relationship_response['relationship-state']
    # The relationship state must be CREATED
    if relationship_state != relationship_status.CREATED:
        raise SemanticError(
            "Relationship ID {0} is not in {1} state".format(
                relationship_id,
                relationship_status.CREATED))

    return relationship_response


def validate_create_event(request_id: str, object_id: str, collection_id: str) -> None:
    """
    Determine if a create object or create object multipart upload event is valid based on the previous pending event
    (if exist) in the Tracking Table or the object state (if object exist).
    :param request_id: Request ID
    :param object_id: Object ID
    :param collection_id: Collection ID
    """
    previous_event, _ = get_previous_and_current_events(request_id, object_id, collection_id)

    blocking_event_names = [event_names.OBJECT_CREATE, event_names.OBJECT_UPDATE,
                            event_names.OBJECT_CREATE_FOLDER, event_names.OBJECT_UPDATE_FOLDER]

    if previous_event and previous_event["event-name"] in blocking_event_names:
        raise SemanticError("Object ID {} already exist in Collection ID {}".format(object_id, collection_id),
                            "Duplicated Object IDs within the same Collection ID are not allowed")

    if not previous_event:
        obj = get_object_item(object_id, collection_id)
        if obj and obj['object-state'] != object_status.REMOVED:
            raise SemanticError("Object ID {} already exist in Collection ID {}".format(object_id, collection_id),
                                "Duplicated Object IDs within the same Collection ID are not allowed")


def determine_upsert_event_name(previous_event_name: str, object_state: str, is_large_or_multipart: bool) -> str:
    """
    Determine the event name for event_id based on the previous pending event (if exist)
    or the object state (if object exist).
    :param previous_event_name: Name of the previous pending event
    :param object_state: Object State
    :param is_large_or_multipart: True if the object is a large or multipart, otherwise False
    :return: Event name
    """
    if previous_event_name in [event_names.OBJECT_CREATE, event_names.OBJECT_UPDATE] or (
            not previous_event_name and object_state == object_status.CREATED):
        event_name = event_names.OBJECT_UPDATE_PENDING if is_large_or_multipart else event_names.OBJECT_UPDATE
    else:
        event_name = event_names.OBJECT_CREATE_PENDING if is_large_or_multipart else event_names.OBJECT_CREATE

    return event_name


def get_previous_and_current_events(event_id: str, object_id: str, collection_id: str) -> tuple:
    """
    Get the pending event that was inserted into the Tracking Table immediately before event-id and
    the event item corresponding to event-id
    :param event_id: Event ID of the current event
    :param object_id: Object ID
    :param collection_id: Collection ID
    :return: Previous and Current events
    """
    tracking_id = tracker.generate_tracking_id('object', object_id, collection_id)
    try:
        tracking_items = TrackingTable().query_items(
            max_items=2,
            KeyConditionExpression="TrackingID = :tracking_id AND RequestTime <= :request_time",
            ExpressionAttributeValues={":tracking_id": {"S": tracking_id},
                                       ":request_time": {"S": command_wrapper.get_request_time()}},
            ScanIndexForward=False,
            ConsistentRead=True)
    except ClientError as e:
        raise InternalError("Failed to query items in Tracking Table by Tracking ID {}".format(tracking_id), e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not tracking_items or tracking_items[0]['event-id'] != event_id:
        raise InternalError(
            "Event ID {} doesn't exist in the Tracking Table for Tracking ID {}".format(event_id, tracking_id))

    current_event = tracking_items[0]
    previous_event = tracking_items[1] if len(tracking_items) > 1 else {}

    return previous_event, current_event


def get_object_item(object_id: str, collection_id: str) -> dict:
    """
    Get the object item from the Object Store Table based on the object-id and collection-id.
    :param object_id: Object ID
    :param collection_id: Collection ID
    :return: Object item
    """
    try:
        return ObjectStoreTable().get_item({'object-id': object_id, 'collection-id': collection_id})
    except ClientError as ce:
        logger.error(ce)
        raise InternalError("Unable to get item from Object Store Table", ce)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)


def get_changeset_object_item(object_id: str, collection_id: str, changeset_id: str, version_number: int) -> dict:
    """
    Get the changeset object item from the Changeset Object Table based on the composite key and changeset-id
    :param object_id: Object ID part of composite key
    :param collection_id: Collection ID part of composite key
    :param changeset_id: Changeset ID range key
    :param version_number: Version Number part of composite key
    :return: Changeset Object item
    """
    composite_key = ChangesetObjectTable().generate_composite_key(object_id, collection_id, version_number)

    try:
        return ChangesetObjectTable().get_item({
            'composite-key': composite_key,
            'changeset-id': changeset_id
        })
    except ClientError as ce:
        logger.error(ce)
        raise InternalError("Unable to get item from Changeset Object Table", ce)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)


def generate_sts_role_token(staging_bucket_arn, staging_prefix, assume_role_arn, policy_duration, request_id):
    s3_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:PutObject"
                ],
                "Resource": [
                    "{}/{}/*".format(staging_bucket_arn, staging_prefix)
                ]
            },
            {
                "Effect": "Deny",
                "Action": [
                    "*"
                ],
                "Resource": [
                    "{}/{}/*/*".format(staging_bucket_arn, staging_prefix)
                ]
            }
        ]
    }
    try:
        sts_response = sts.get_client().assume_role(RoleArn=assume_role_arn,
                                                    RoleSessionName='FolderUploadRoleAssume_{}'.format(request_id),
                                                    Policy='{}'.format(orjson.dumps(s3_policy).decode()),
                                                    DurationSeconds=policy_duration)
    except ClientError as ce:
        logger.error(ce)
        raise InternalError("Unable to generate role credentials", ce)

    # TODO: time format is incorrect compared to request-time property
    sts_response['Credentials']['Expiration'] = time_helper.format_time(sts_response['Credentials']['Expiration'])
    return sts_response["Credentials"]


def validate_object_id(object_id: str) -> None:
    corrective_action = object_common.validate_object_id(object_id)
    if corrective_action:
        raise InvalidRequestPropertyValue("Invalid Object ID {0}".format(object_id), corrective_action)


def encode_md5(md5_hash):
    try:
        return base64.b64encode(bytes.fromhex(md5_hash)).decode(UTF_8_ENCODING)
    except Exception as e:
        raise InternalError('Failed to create an encoded md5 hash: {0}'.format(md5_hash), e)


def extend_event_ttl(previous_event: dict, current_event: dict) -> None:
    """
    Extend the TTL of the previous and remove the current event.
    removed from the Tracking Table
    :param previous_event: Pending event that was inserted into the Tracking Table immediately before current event
    :param current_event: Current event
    """
    try:
        previous_event['pending-expiration-epoch'] = object_common.get_expiration_epoch_folder(hours=1.25)
        current_event_keys = {'tracking-id': current_event['tracking-id'],
                              'request-time': current_event['request-time']}
        TrackingTable().batch_write_items(items_to_put=[previous_event], items_to_delete=[current_event_keys])
    except (ClientError, ConditionError) as e:
        raise InternalError("Unable to put item in Tracking Table", e)
    except Exception as ex:
        raise UnhandledException(ex)


def put_object_to_staging_bucket(object_content, staging_bucket_name: str, object_key: str, content_type: str):
    try:
        s3.put_s3_object(object_content, staging_bucket_name, object_key, content_type=content_type)
        logger.debug("Wrote object content to s3://{}/{}".format(staging_bucket_name, object_key))
    except ClientError as ce:
        logger.error(ce)
        raise InternalError("Unable to create S3 object {0}".format(object_key), ce)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)


def validate_object_metadata(object_metadata: dict) -> None:
    object_metadata_len = 0
    for key, value in object_metadata.items():
        # Per AWS: each key-value pair must conform to US-ASCII when you are using REST
        # TODO: When we move to python 3.7+ change this to use the new .isascii
        try:
            key.encode('ascii')
            value.encode('ascii')
        except UnicodeEncodeError:
            raise InvalidRequestPropertyValue("Invalid object metadata",
                                              "Object metadata key/value must be US-ASCII")

        if key.lower().startswith("dl-"):
            raise InvalidRequestPropertyValue("Invalid object metadata",
                                              "Object metadata keys cannot begin with dl-")
        object_metadata_len += len(key) + len(value)

    if object_metadata_len > 1024:
        raise InvalidRequestPropertyValue("Invalid object metadata",
                                          "Object metadata length {0} exceeds max length of 1024".format(
                                              object_metadata_len))


def validate_and_get_changeset_id(changeset_id: str, owner_id: int) -> dict:
    try:
        changeset = ChangesetTable().get_item({'changeset-id': changeset_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Changeset Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not changeset:
        raise InvalidRequestPropertyValue("Changeset ID {0} does not exist".format(changeset_id),
                                          "Create a Changeset and use that ID")
    if changeset['owner-id'] != owner_id:
        raise NotAuthorizedError(
            "Owner ID {0} does not have permission to modify Objects in Changeset ID {1}".format(owner_id,
                                                                                                 changeset_id),
            "Use a Changeset ID created by Owner ID {0}".format(owner_id))

    if changeset['changeset-state'] != changeset_status.OPENED:
        raise InvalidRequestPropertyValue("Changeset ID {0} is in {1} not {2} state".format(changeset_id,
                                                                                            changeset[
                                                                                                'changeset-state'],
                                                                                            changeset_status.OPENED),
                                          "Create a new Changeset and use that ID")
    logger.debug(
        'Validated that Changeset ID {0} exists and is in {1} state'.format(changeset_id, changeset_status.OPENED))
    return changeset
