import base64
import hashlib
import os
import re
from datetime import datetime, timedelta

import orjson
from lng_datalake_commands import command_wrapper
from lng_datalake_commons import time_helper
from lng_datalake_constants import object_status

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

REGIONS = {'us-east-1': 'use1', 'us-west-2': 'usw2'}
DATA_LAKE_URL = os.getenv("DATA_LAKE_URL")


def validate_object_id(object_id: str) -> str:
    if 1 > len(object_id) or len(object_id) > 256:
        return "Object ID length be between 1 and 256 characters"
    elif not re.match("^[A-Za-z0-9-.:_,()]*$", object_id):
        return "Object ID contains invalid characters"
    else:
        return ""


def build_object_key_and_hashes(object_content: bytes, content_type: str, object_id: str, version_timestamp: str,
                                user_object_metadata: dict, collection_hash: str) -> tuple:
    # content_sha1 = SHA1 of body
    sha1 = hashlib.sha1()
    sha1.update(object_content)
    content_sha1 = sha1.hexdigest()

    # object_hash = SHA1 of body + content_type + user_metadata
    sha1.update(content_type.encode())
    if user_object_metadata:
        sha1.update(orjson.dumps(user_object_metadata))
    object_hash = sha1.hexdigest()

    # object_key = collection_hash + "/" + SHA1 of body + content_type + user_metadata + object_id + version_timestamp
    sha1.update(object_id.encode())
    sha1.update(version_timestamp.encode())
    object_key = "{0}/{1}".format(collection_hash, sha1.hexdigest())

    return content_sha1, object_hash, object_key


def build_object_metadata(collection_id: str, object_id: str, version_timestamp: str, version_number: int,
                          content_sha1: str, user_object_metadata: dict) -> dict:
    # datalake system metadata must be prefixed with "dl-"
    object_metadata = {
        "dl-collection-id": collection_id,
        "dl-object-id": object_id,
        "dl-version-timestamp": version_timestamp,
        "dl-version-number": str(version_number),
        "dl-content-sha1": content_sha1
    }
    object_metadata.update(user_object_metadata)
    return object_metadata


def build_large_object_key(request_id: str, stage: str) -> str:
    return '{0}/{1}/{2}'.format('large-object', stage, request_id)


def build_multipart_object_key(request_id: str, stage: str) -> str:
    return '{0}/{1}/{2}'.format('multipart-object', stage, request_id)


def build_multipart_ingestion_key(request_id: str, stage: str) -> str:
    return '{0}/{1}/{2}'.format('ingestion', stage, request_id)


def build_batch_key(request_id: str, stage: str) -> str:
    return '{0}/{1}/{2}'.format('batch', stage, request_id)


def build_staging_prefix(folder_prefix: str, collection_id: str, event_id: str, object_id: str) -> str:
    return "{}/{}_{}_{}".format(folder_prefix, collection_id, event_id, object_id)


def build_object_key_url(object_key: str, content_type: str) -> str:
    object_key_url = '{0}/objects/store/{1}'.format(DATA_LAKE_URL, object_key)
    if content_type == 'multipart/mixed':
        object_key_url += '/'
    return object_key_url


def build_temporary_object_key_url(object_key: str) -> str:
    object_key_url = '{0}/objects/temp/{1}'.format(DATA_LAKE_URL, object_key)
    return object_key_url


def build_temporary_multipart_object_key_url(request_id: str, stage: str) -> str:
    object_key_url = build_temporary_object_key_url(build_multipart_object_key(request_id, stage))
    return object_key_url


def build_folder_object_transaction_id(transaction_id: str, collection_id: str, owner_id: int, stage: str,
                                       changeset: dict) -> str:
    folder_object_transaction_id = {"transaction-id": transaction_id,
                                    "collection-id": collection_id,
                                    "owner-id": owner_id,
                                    "stage": stage}
    if changeset:
        folder_object_transaction_id['changeset'] = changeset
    return base64.b64encode(orjson.dumps(folder_object_transaction_id)).decode()


def generate_object_expiration_time(collection_response: dict) -> int:
    if 'object-expiration' in collection_response:
        # Popping off the last char of the collection's object-expiration
        # because it is a h, denoting hours
        hours = int(collection_response['object-expiration'][:-1])
        object_expiration = int(
            (datetime.strptime(command_wrapper.get_request_time() + '+0000', "%Y-%m-%dT%H:%M:%S.%fZ%z")
             + timedelta(hours=hours)).timestamp())
        return object_expiration
    return 0


def generate_object_store_buckets(base_object_store: str) -> dict:
    object_store_buckets = {}
    for region, abbreviation in REGIONS.items():
        object_store_buckets[region] = ('{0}-{1}'.format(base_object_store, abbreviation))
    return object_store_buckets


def build_collection_id_hash(collection_id: str) -> str:
    return hashlib.sha1(collection_id.encode()).hexdigest()


def get_expiration_epoch_folder(hours: float) -> int:
    return int((datetime.now() + timedelta(hours=hours)).timestamp())


def is_valid_content_type(content_type: str) -> bool:
    base_type = content_type.split(';')[0].strip()
    return base_type in __get_valid_content_types()


def __get_valid_content_types() -> list:
    return ["application/json",
            "text/plain",
            "text/csv",
            "text/html",
            "text/sgml",
            "application/xml",
            "application/xhtml+xml",
            "application/atom+xml",
            "application/octet-stream",
            "application/pdf",
            "application/rtf",
            "application/zip",
            "image/gif",
            "image/png",
            "image/jpeg",
            "image/jp2",
            "image/bmp",
            "image/svg+xml",
            "image/tiff",
            "image/vnd.microsoft.icon",
            "application/postscript",
            "application/vnd.ms-powerpoint",
            "application/vnd.ms-excel",
            "application/msword",
            "application/vnd.visio",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/x-hotdocs-auto",
            "audio/aac",
            "audio/basic"
            "audio/mpeg",
            "audio/vnd.rn-realaudio",
            "audio/wav",
            "video/x-msvideo",
            "video/mpeg"
            ]


def object_contents_match(object_key: str, object_hash: str, content_sha1: str,
                          existing_object_store_item: dict) -> bool:
    if not existing_object_store_item or existing_object_store_item['object-state'] == object_status.REMOVED:
        return False
    if 'object-hash' in existing_object_store_item:
        return existing_object_store_item['object-hash'] == object_hash
    else:
        # Return false to force content to move from the old bucket to the new bucket with the collection-hash as
        # a prefix
        return False


def is_expired_object(object_store_item: dict, request_date_time: str) -> bool:
    if 'pending-expiration-epoch' in object_store_item and \
            request_date_time > time_helper.format_time(object_store_item['pending-expiration-epoch']):
        return True
    else:
        return False


def generate_changeset_object_key(changeset_id: str, object_key: str) -> str:
    return '{}/{}'.format(changeset_id, object_key) if object_key != 'tombstone' else object_key
