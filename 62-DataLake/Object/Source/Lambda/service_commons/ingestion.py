import io
import logging
import os
import re
import zipfile

import orjson
from botocore.exceptions import ClientError
from botocore.response import StreamingBody
from lng_aws_clients import s3, sqs
from lng_datalake_commons import sqs_extractor
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import ingestion_counter
from lng_datalake_dal.collection_blocker_table import CollectionBlockerTable
from lng_datalake_dal.ingestion_table import IngestionTable

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

INGESTION_COUNTER_QUEUE_URL = os.getenv('INGESTION_COUNTER_QUEUE_URL')


def get_message(event):
    if len(event['Records']) > 1:
        raise TerminalErrorException("Ingestion Get Message only handles a single SQS message and not a batch")

    message_body = sqs_extractor.extract_sqs_message(event['Records'][0])

    try:
        message = orjson.loads(message_body['Message'])
    except KeyError as e:
        raise TerminalErrorException("Error getting message from record||{}".format(e))
    except (ValueError, TypeError) as json_error:
        raise TerminalErrorException("Cannot load message {0}||{1}".format(message_body, json_error))

    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))
    return message


def get_ingestion_data(ingestion_id, retry_on_missing_data=False):
    try:
        ingestion_data = IngestionTable().get_item({"ingestion-id": ingestion_id}, ConsistentRead=True)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to get item from Ingestion Table||{}".format(e))
    except Exception as ex:
        raise TerminalErrorException("Failed to get item from Ingestion Table||{}".format(ex))

    if not ingestion_data:
        if not retry_on_missing_data:
            raise TerminalErrorException("Invalid Ingestion ID {0}||Ingestion ID does not exist in Ingestion Table"
                                         .format(ingestion_id))
        else:
            raise RetryEventException("Invalid Ingestion ID {0}||Ingestion ID does not exist in Ingestion Table"
                                      .format(ingestion_id))
    return ingestion_data


def update_ingestion_state(ingestion_data, ingestion_state, error_description=None):
    ingestion_data['ingestion-state'] = ingestion_state
    if error_description:
        ingestion_data['error-description'] = error_description
    try:
        IngestionTable().update_item(ingestion_data)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to update item in Ingestion Table||{}".format(e))
    except Exception as ex:
        raise TerminalErrorException("Failed to update item in Ingestion Table||{}".format(ex))


def validate_zip_magic_number(bucket_name, zip_key):
    s3_client = s3.get_client()

    try:
        response = s3_client.get_object(Bucket=bucket_name, Key=zip_key, Range="bytes=0-3")
    except ClientError as e:
        if e.response['Error']['Code'] == "NoSuchKey":
            raise TerminalErrorException("S3 object {0} not found in bucket {1}".format(zip_key, bucket_name))
        raise RetryEventException("Unable to read S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to read S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))
    magic_number = response['Body'].read(4)
    if magic_number != b'PK\x03\x04':
        raise TerminalErrorException("Error reading zip file {}||Not a zip file".format(zip_key))


def read_zip_central_directory(bucket_name, zip_key, zip_length, initial_read_length=0):
    if initial_read_length == 0:
        # read a minimum of 16KB and a maximum of 100GB based on zip length
        initial_read_length = max(min(int(zip_length * 0.02), 100 * 2 ** 20), 16 * 2 ** 10)

    # just read the end of the file to get the central directory
    response, unread_length = _read_s3_object_portion(bucket_name, zip_key, initial_read_length, zip_length)
    try:
        z, buffer = _read_streaming_body(response["Body"], zip_key)

        # calculate how big the central directory actually was since we didn't get an exception
        central_directory_length = initial_read_length - buffer.find(b'PK\x01\x02')

    except ValueError as e:
        # read more if we get this exception (exception includes number of additional bytes to read)
        m = re.match(r'negative seek value -(\d+)', e.args[0])
        if not m:
            raise TerminalErrorException("Error reading zip file {0}||{1}".format(zip_key, e))
        extra_read_length = int(m.group(1))
        central_directory_length = initial_read_length + extra_read_length

        # 2nd attempt to read central directory with more bytes
        response, unread_length = _read_s3_object_portion(bucket_name, zip_key, initial_read_length + extra_read_length,
                                                          zip_length)
        unread_length = zip_length - (initial_read_length + extra_read_length)
        z, _ = _read_streaming_body(response['Body'], zip_key)

    logger.info("Successfully read central directory of zip file {0} which contained {1} files"
                .format(zip_key, len(z.infolist())))

    return z.infolist(), unread_length, central_directory_length


def _read_s3_object_portion(bucket_name, zip_key, read_length, zip_length):
    logger.debug("Attempting to read central directory of zip file {0} (bytes={1})"
                 .format(zip_key, read_length))
    try:
        s3_client = s3.get_client()
        response = s3_client.get_object(Bucket=bucket_name, Key=zip_key, Range="bytes=-{}"
                                        .format(read_length))

        if zip_length > read_length:
            unread_length = zip_length - read_length
        else:
            unread_length = 0

    except ClientError as e:
        if e.response['Error']['Code'] == "NoSuchKey":
            raise TerminalErrorException("S3 object {0} not found in bucket {1}".format(zip_key, bucket_name))
        raise RetryEventException("Unable to read S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to read S3 object {0} from bucket {1}||{2}".format(zip_key, bucket_name, e))

    return response, unread_length


def _read_streaming_body(body: StreamingBody, zip_key: str) -> (zipfile.ZipFile, bytes):
    try:
        # must use io.BytesIO b/c StreamingBody doesn't have seek() method
        with io.BytesIO(body.read()) as b:
            content = b.getvalue()
            z = zipfile.ZipFile(b)
    except zipfile.BadZipFile as e:
        raise TerminalErrorException("Error reading zip file {0}||{1}".format(zip_key, e))
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to read S3 object {0}||{1}".format(zip_key, e))

    return z, content


def update_error_counter(ingestion_id, count=1):
    message = {ingestion_id: {ingestion_counter.ERROR: count}}
    try:
        sqs.get_client().send_message(QueueUrl=INGESTION_COUNTER_QUEUE_URL, MessageBody=orjson.dumps(message).decode())
    except error_handler.retryable_exceptions as e:
        logger.warning("Unable to publish ObjectErrorCount to SQS {}||{}".format(INGESTION_COUNTER_QUEUE_URL, e))
    except Exception as ex:
        logger.error("Failed to publish ObjectErrorCount to SQS {}||{}".format(INGESTION_COUNTER_QUEUE_URL, ex))


def is_object_in_datalake(datalake_bucket_name: str, object_key: str) -> bool:
    try:
        s3.get_client().head_object(Bucket=datalake_bucket_name, Key=object_key)
    except ClientError as e:
        if e.response['Error']['Message'] == "Not Found":
            return False
        raise e
    except error_handler.retryable_exceptions as e:
        raise e
    return True


def delete_collection_blocker(collection_id, request_time, request_id):
    try:
        CollectionBlockerTable().delete_item({"collection-id": collection_id,
                                              "request-time": request_time})
        logger.info("Collection Blocker deleted for Request ID {}".format(request_id))
    except error_handler.retryable_exceptions:
        logger.error("Unable to delete item from CollectionBlocker Table")
        raise
    except Exception as ex:
        raise TerminalErrorException("Failed to delete item from CollectionBlocker Table||{}".format(ex))


def create_object_backend_event_id(event_id: str, item_index: int) -> str:
    """
    Creates an object backend event ID from the event-id and item index.

    :param event_id: event-id or request-id
    :param item_index: Index of the item
    :returns str: Objects event ID.
    """
    return "{0}-{1:07d}".format(event_id, item_index)

if __name__ == '__main__':
    from lng_aws_clients import session

    os.environ['AWS_PROFILE'] = 'c-sand'

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    session.set_session()

    _, _, cd_length = read_zip_central_directory("aaron-test-99", "1524439.zip", 10485760, 99999)
    logger.info("CD length: {}".format(cd_length))
    _, _, cd_length = read_zip_central_directory("aaron-test-99", "dltest.zip", 9153942, 99999)
    logger.info("CD length: {}".format(cd_length))
