import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, event_handler_status, ingestion_status
from lng_datalake_dal.ingestion_table import IngestionTable

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event handler version environment variable
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return create_ingestion_event_handler(event, context.invoked_function_arn)


def create_ingestion_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.OBJECT_CREATE_INGESTION):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event,
                                                                               event_names.OBJECT_CREATE_INGESTION))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    ingestion_item = generate_ingestion_item(message)
    put_ingestion_item(ingestion_item)

    return event_handler_status.SUCCESS


def generate_ingestion_item(message) -> dict:
    ingestion_item = {}
    prop = None
    try:

        for prop in IngestionTable().get_required_schema_keys():
            if prop == 'ingestion-timestamp':
                ingestion_item[prop] = message['request-time']
            elif prop == 'ingestion-state':
                ingestion_item[prop] = ingestion_status.PENDING
            elif prop == "event-ids":
                ingestion_item[prop] = IngestionTable().build_event_ids_list({}, message['event-id'])
            else:
                ingestion_item[prop] = message[prop]
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in IngestionTable().get_optional_schema_keys():
        if prop in message:
            ingestion_item[prop] = message[prop]

    return ingestion_item


def put_ingestion_item(ingestion_item) -> None:
    try:
        IngestionTable().put_item(ingestion_item)
    except error_handler.retryable_exceptions as ce:
        logger.error(ce)
        raise
    except Exception as ex:
        logger.error(ex)
        raise TerminalErrorException("Failed to put item to Ingestion Table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(ingestion_item, ex))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE INGESTION EVENT HANDLER LAMBDA]")

    os.environ["AWS_PROFILE"] = "datalake-wormhole"
    os.environ[
        "RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-jek-DataLake-RetryQueues-" \
                             "RetryQueue-135L28JKZ7FDJ"

    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "feature-jek-datalake-lambda-terminal-errors-288044017584"
    os.environ['INGESTION_DYNAMODB_TABLE'] = 'feature-jek-DataLake-IngestionTable'
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {
        "pending-expiration-epoch": 1552989630,
        "bucket-name": "feature-jek-dl-object-staging-288044017584-use1",
        "ingestion-id": "wqmoIscbDzQmZaTV",
        "collection-id": "JohnK",
        "event-id": "wqmoIscbDzQmZaTV",
        "stage": "LATEST",
        'upload-id': 'ii_CxlI9dt4355tBE86Q5xPvGaR1pKMJaxqcpPIpfLg5qJ9TjHii993OoGX7ThpkiNOw7RFyL9vupaFWg4FC7OcvDWY63Hffp2JV3OVlxWDoFFC45HXRVPfpcVFrM6ieSPUIZ6UsmZs0vL9DQxT5AA--',
        'archive-format': 'zip',
        "request-time": "2019-02-13T22:44:53.670Z",
        "event-name": "Object::IngestionCreate",
        "owner-id": 1,
        "event-version": 1,
        "seen-count": 0
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }
    logger.debug(sample_event)
    lambda_handler(sample_event, MockLambdaContext())
    logger.debug("[LOCAL RUN END - CREATE INGESTION EVENT HANDLER LAMBDA]")
    # retry_lambda_handler(sample_event, MockLambdaContext)
