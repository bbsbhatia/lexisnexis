import logging
import os
from datetime import datetime

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_aws_clients import s3
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, SemanticError, UnhandledException
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import object_status
from lng_datalake_dal.tracking_table import TrackingTable

from service_commons import object_command, object_common

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event version environment variable
event_version = os.getenv("EVENT_VERSION", 1)
staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")
folder_prefix = os.getenv("FOLDER_PREFIX", 'folder-upload')


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/finish_folder_upload_command-RequestSchema.json',
                         'Schemas/finish_folder_upload_command-ResponseSchema.json')
def lambda_handler(event, context):
    return finish_folder_upload_command(event['request'], event['context']['stage'],
                                        event['context']['api-key-id'])


def finish_folder_upload_command(request: dict, stage: str, api_key_id: str) -> dict:
    orig_transaction_id = request['transaction-id']
    parts = object_command.unpack_folder_object_transaction_id(orig_transaction_id)
    event_id = parts["transaction-id"]
    collection_id = parts["collection-id"]
    owner_id = parts["owner-id"]
    transaction_stage = parts["stage"]

    # extract changeset, if exists
    changeset = parts.get('changeset', {})
    object_id = request['object-id']

    if stage != transaction_stage:
        raise SemanticError(
            "API Stage: {0} does not match the stage of the transaction creation: {1}".format(stage, transaction_stage),
            "Request for creation and completion must be done in the same api stage")

    # owner authorization
    owner_authorization.authorize(owner_id, api_key_id)

    tracking_record = get_tracking_record(object_id, collection_id, event_id)

    staging_prefix = object_common.build_staging_prefix(folder_prefix, collection_id, event_id, object_id)
    check_upload_directory(staging_prefix, orig_transaction_id)

    collection_response = object_command.get_collection_response(collection_id)

    optional_object_props = {
        'pending-expiration-epoch': object_common.generate_object_expiration_time(collection_response),
        'changeset': changeset
    }

    event_dict = generate_event_dict(staging_prefix, collection_response, object_id, stage, event_id,
                                     tracking_record['event-name'], optional_object_props)

    response_dict = generate_response_dict(collection_response, object_id, stage, optional_object_props, staging_prefix)

    tracking_record.pop('pending-expiration-epoch')
    update_tracking_record(tracking_record)
    return {'response-dict': response_dict, 'event-dict': event_dict}


def get_tracking_record(object_id: str, collection_id: str, event_id: str) -> dict:
    """
    Get the pending event that was inserted into the Tracking Table
    :param object_id: Object ID
    :param collection_id: Collection ID
    :param event_id: Event ID
    :return: Tracking record with the pending expiration epoch
    """
    try:
        tracking_item = TrackingTable().query_items(KeyConditionExpression="TrackingID = :tracking_id",
                                                    FilterExpression="attribute_exists(PendingExpirationEpoch) and "
                                                                     "EventID =:event_id",
                                                    ScanIndexForward=True,
                                                    ConsistentRead=True,
                                                    PaginationConfig={'MaxItems': 1},
                                                    ExpressionAttributeValues={
                                                        ":tracking_id": {
                                                            "S": tracker.generate_tracking_id('object', object_id,
                                                                                              collection_id)},
                                                        ":event_id": {"S": event_id}})
    except ClientError as e:
        raise InternalError("Unable to query items in Tracking Table by Object ID {0}".format(object_id), e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not tracking_item:
        raise SemanticError("No open transactions for Object ID: {0} in Collection ID: {1}".format(object_id,
                                                                                                   collection_id),
                            'Create a new transaction')
    return tracking_item[0]


def check_upload_directory(staging_prefix: str, transaction_id: str) -> None:
    try:
        directory = s3.get_client().list_objects_v2(Bucket=staging_bucket_name,
                                                    Prefix=staging_prefix,
                                                    MaxKeys=1)
        contents = directory.get('Contents', [])
    except ClientError as ce:
        logger.error(ce)
        raise InternalError("Unable to read s3 location", ce)
    except Exception as ex:
        raise UnhandledException(ex)

    if not contents:
        raise SemanticError("No objects have been uploaded for transaction-id: {0}".format(transaction_id),
                            "Confirm objects were uploaded to the correct location")


def generate_event_dict(transaction_id: str, collection_response: dict, object_id: str, stage: str, request_id: str,
                        event_name: str, optional_props: dict) -> dict:
    event_dict = {
        'object-key': transaction_id,
        'bucket-name': staging_bucket_name,
        'stage': stage,
        'event-id': request_id,
        # use an asterisk after request-time on a pending event so primary key will be different when
        # ProcessLargeObject writes the real event
        'request-time': "{0}*".format(command_wrapper.get_request_time()),
        'event-version': event_version,
        "event-name": event_name,
        'object-id': object_id,
        'old-object-versions-to-keep': collection_response['old-object-versions-to-keep'],
        'collection-id': collection_response['collection-id'],
        'collection-hash': collection_response['collection-hash']
    }
    if optional_props.get('pending-expiration-epoch', 0):
        event_dict['pending-expiration-epoch'] = optional_props['pending-expiration-epoch']

    if optional_props.get('changeset', {}):
        event_dict['changeset-id'] = optional_props['changeset']['changeset-id']
        event_dict['changeset-expiration-epoch'] = optional_props['changeset']['pending-expiration-epoch']

    return event_dict


def generate_response_dict(collection_properties: dict, object_id: str, stage: str, optional_object_props: dict,
                           staging_prefix: str) -> dict:
    response_dict = {}

    try:
        default_properties = {
            "object-id": object_id,
            "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, object_id,
                                                                      collection_properties['collection-id']),
            "collection-url": "/collections/{0}/{1}".format(stage, collection_properties['collection-id']),
            "object-state": object_status.PENDING,
            "temporary-object-key-url": object_common.build_temporary_object_key_url(staging_prefix) + '/'
        }
        for prop in command_wrapper.get_required_response_schema_keys('object-properties'):
            response_dict[prop] = default_properties[prop] if prop in default_properties else collection_properties[
                prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(e))

    for prop in command_wrapper.get_optional_response_schema_keys('object-properties'):
        if prop == 'object-expiration-date' and optional_object_props.get('pending-expiration-epoch'):
            response_dict[prop] = time_helper.format_time(optional_object_props['pending-expiration-epoch'])
        elif prop in 'changeset-id' and optional_object_props.get('changeset', {}):
            response_dict[prop] = optional_object_props['changeset'][prop]
        elif prop in collection_properties:
            response_dict[prop] = collection_properties[prop]

    return response_dict


def update_tracking_record(tracking_record: dict) -> None:
    """
    Updates the tracking table record

    :param: tracking_record: request_id, tracking_id, request_time, event_name
    :return None
    """
    try:
        TrackingTable().update_item(tracking_record)
        logger.info("Successfully updated the record in tracking table!")
    except ClientError as e:
        raise InternalError("Unable to update item in Tracking Table: {0}".format(tracking_record), e)
    except Exception as e:
        raise UnhandledException(e)


if __name__ == '__main__':
    import random
    import string
    import json
    from importlib import reload
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE OBJECT COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'

    ASSET_GROUP = 'feature-ajb'

    os.environ['DATALAKE_STAGING_BUCKET_NAME'] = '{0}-dl-object-staging-use1'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{}-DataLake-EventStoreTable'.format(ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{}-DataLake-TrackingTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{}-DataLake-OwnerTable'.format(ASSET_GROUP)

    os.environ['DATA_LAKE_URL'] = 'https://datalake-{0}.content.aws.lexis.com'.format(ASSET_GROUP)
    reload(object_common)

    staging_bucket_name = os.getenv('DATALAKE_STAGING_BUCKET_NAME')


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": "17449f9d-1e63-4b5d-a876-ee87729f7262",
                "client-request-time": "1570583814476",
                "client-type": "aws-apigateway",
                "stage": "LATEST",
                "client-id": "2io862anp3",
                "http-method": "POST",
                "resource-id": "l92zpv",
                "resource-path": "/{objectId}/folder/{transactionId}/complete",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "object-id": "aaronFOLDER3",
                "transaction-id": "eyJ0cmFuc2FjdGlvbi1pZCI6ImViNWRhMzY1LWVjZDQtNGQ4ZS04YjE4LWU2ZDhlMWU3YjAyOSIsImNvbGxlY3Rpb24taWQiOiJBQVJPTl8yIiwib3duZXItaWQiOjgsInN0YWdlIjoiTEFURVNUIn0="
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE OBJECT COMMAND LAMBDA]")
