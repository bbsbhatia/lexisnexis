import logging
import os
from datetime import datetime

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, UNSUPPORTED_MEDIA_TYPE
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_commons.tracking.tracker import get_collection_blocker_expiration
from lng_datalake_constants import ingestion_status, event_names

from service_commons import object_command, object_common

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event version environment variable
event_version = os.getenv("EVENT_VERSION", 1)
staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/create_ingestion_command-RequestSchema.json',
                         'Schemas/create_ingestion_command-ResponseSchema.json',
                         event_names.OBJECT_CREATE_INGESTION)
def lambda_handler(event, context):
    return create_ingestion_command(event['request'], event['context']['client-request-id'],
                                    event['context']['stage'],
                                    event['context']['api-key-id'])


def create_ingestion_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    collection_id = request['collection-id']
    # TODO: currently we have an optional 'stored-content-type' property, if not provided we use the header.content-type
    #   'stored-content-type' needs to become required.
    content_type = request['content-type']
    archive_format = request['archive-format']
    description = request.get('description')

    if not object_common.is_valid_content_type(content_type):
        raise UNSUPPORTED_MEDIA_TYPE

    if description and len(description) > 100:
        raise InvalidRequestPropertyValue("Invalid Description", "Description must be less than 100 characters")

    collection_response = object_command.get_collection_response(collection_id)

    # owner authorization
    owner_authorization.authorize(collection_response['owner-id'], api_key_id)

    # extract changeset if it exists
    if 'changeset-id' in request:
        changeset = object_command.validate_and_get_changeset_id(request['changeset-id'], collection_response['owner-id'])
    else:
        changeset = {}

    object_key = object_common.build_multipart_ingestion_key(request_id, stage)

    object_expiration_epoch = object_common.generate_object_expiration_time(collection_response)

    multipart_request_info = {'content-type': content_type,
                              'request-id': request_id, 'stage': stage,
                              'object-key': object_key}
    if changeset:
        multipart_request_info['changeset-id'] = changeset['changeset-id']
        multipart_request_info['changeset-expiration-epoch'] = changeset['pending-expiration-epoch']

    logger.info("Creating s3 multipart upload request")
    multipart_resp = object_command.create_multipart_upload(multipart_request_info, collection_response,
                                                            object_expiration_epoch,
                                                            staging_bucket_name)
    time_stamp = command_wrapper.get_request_time()
    ingestion_expiration_epoch = get_collection_blocker_expiration(time_stamp)

    ingestion_properties = {
        'ingestion-id': request_id,
        'archive-format': archive_format,
        'ingestion-timestamp': time_stamp,
        'ingestion-expiration-date': command_wrapper.time_helper.format_time(ingestion_expiration_epoch)
    }
    if changeset:
        ingestion_properties['changeset-id'] = changeset['changeset-id']

    if description:
        ingestion_properties['description'] = description

    ingestion_dict = generate_ingestion_response_json(collection_response, stage, ingestion_properties)

    response_dict = generate_response_json(multipart_resp, ingestion_dict)

    event_dict = generate_event_dict(response_dict, collection_response['collection-hash'], ingestion_expiration_epoch,
                                     request_id, stage, changeset)

    return {'response-dict': response_dict, 'event-dict': event_dict}


def generate_ingestion_response_json(collection_response: dict, stage: str, ingestion_properties: dict) -> dict:
    ingestion_dict = {}
    prop = None

    try:
        # required ingestion properties
        for prop in command_wrapper.get_required_response_schema_keys('ingestion-properties'):
            if prop == "collection-url":
                ingestion_dict[prop] = "/collections/{0}/{1}".format(stage, collection_response['collection-id'])
            elif prop == "ingestion-url":
                ingestion_dict[prop] = "/objects/{0}/ingestion/{1}".format(stage, ingestion_properties['ingestion-id'])
            elif prop == 'ingestion-state':
                ingestion_dict[prop] = ingestion_status.PENDING
            elif prop in ingestion_properties:
                ingestion_dict[prop] = ingestion_properties[prop]
            else:
                ingestion_dict[prop] = collection_response[prop]

    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('ingestion-properties'):
        if prop == 'object-expiration-date' and 'object-expiration' in collection_response:
            ingestion_dict[prop] = time_helper.format_time(
                object_common.generate_object_expiration_time(collection_response))
        elif prop in ingestion_properties:
            ingestion_dict[prop] = ingestion_properties[prop]
        elif prop in collection_response:
            ingestion_dict[prop] = collection_response[prop]

    return ingestion_dict


def generate_response_json(multipart_resp: dict, ingestion_dict: dict) -> dict:
    response_dict = {}
    multipart_dict = {}
    prop = None

    try:
        # required multipart-upload properties
        for prop in command_wrapper.get_required_response_schema_keys('multipart-upload'):
            multipart_dict[prop] = multipart_resp[prop]

    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    response_dict['ingestion'] = ingestion_dict
    response_dict['multipart-upload'] = multipart_dict

    return response_dict


def generate_event_dict(json_response: dict, collection_hash: str, ingestion_expiration_epoch: int, request_id: str,
                        stage: str, changeset: dict) -> dict:
    event_entry = {
        'upload-id': json_response['multipart-upload']['UploadId'],
        'bucket-name': json_response['multipart-upload']['S3']['Bucket'],
        'ingestion-id': json_response['ingestion']['ingestion-id'],
        'collection-id': json_response['ingestion']['collection-id'],
        'collection-hash': collection_hash,
        'request-time': json_response['ingestion']['ingestion-timestamp'],
        'archive-format': json_response['ingestion']['archive-format'],
        'owner-id': json_response['ingestion']['owner-id'],
        'pending-expiration-epoch': ingestion_expiration_epoch,
        'event-id': request_id,
        'event-version': event_version,
        'stage': stage,
        'event-name': event_names.OBJECT_CREATE_INGESTION
    }
    if "description" in json_response['ingestion']:
        event_entry['description'] = json_response['ingestion']['description']
    if changeset:
        event_entry['changeset-id'] = changeset['changeset-id']
        event_entry['changeset-expiration-epoch'] = changeset['pending-expiration-epoch']

    return event_entry


if __name__ == '__main__':
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE INGESTION UPLOAD COMMAND LAMBDA]")

    ASSET_GROUP = 'feature-jek'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['DATALAKE_STAGING_BUCKET_NAME'] = '{0}-dl-object-staging-use1'.format(ASSET_GROUP)
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{0}-DataLake-OwnerTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-EventStoreTable'.format(ASSET_GROUP)

    staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-type": "aws-apigateway",
                "stage": "LATEST",
                "client-id": "abcd-client-id-7890",
                "http-method": "POST",
                "resource-id": "RESOURCE ID",
                "resource-path": "RESOURCE PATH",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": 'JohnK',
                "content-type": "application/zips",
                'archive-format': 'zip'

            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    lambda_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE INGESTION UPLOAD COMMAND LAMBDA]")
