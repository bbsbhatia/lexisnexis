import json
import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, UnhandledException, NoSuchObjectRelationship, \
    NoSuchObjectRelationshipVersion
from lng_datalake_commons import session_decorator
from lng_datalake_dal.object_relationship_table import ObjectRelationshipTable

from service_commons import object_command

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/get_object_relationship_command-RequestSchema.json',
                         'Schemas/get_object_relationship_command-ResponseSchema.json')
def lambda_handler(event, context):
    return get_object_relationship_command(event['request'])


def get_object_relationship_command(request: dict) -> dict:
    # required properties
    target_object = request["target-object"]
    relationship_id = request["relationship-id"]

    # optional properties
    relationship_version = request.get('relationship-version')

    # validate collection id exists and it's in proper state
    object_command.get_collection_response(target_object['collection-id'])

    # validate object id exists and it's in proper state
    object_response = object_command.get_object_response(target_object['object-id'],
                                                         target_object['collection-id'])

    if object_response['version-number'] != target_object['version-number']:
        # validate object version exists
        object_command.get_object_version_response(target_object['object-id'],
                                                   target_object['collection-id'],
                                                   target_object['version-number'])

    # validate relationship id exists and it's in proper state
    object_command.get_relationship_response(relationship_id)

    object_relationship = {}
    target_id = ObjectRelationshipTable().generate_object_composite_id(target_object['object-id'],
                                                                       target_object['collection-id'],
                                                                       target_object['version-number'])
    if relationship_version:
        object_relationship = get_object_relationship(target_id, relationship_id, relationship_version)
    else:
        object_relationship = get_latest_object_relationship(target_id, relationship_id)
        _, relationship_version = ObjectRelationshipTable().unpack_relationship_key(
            object_relationship['relationship-key'])

    return {'response-dict': generate_response_json(request, object_relationship, relationship_version)}


def get_object_relationship(target_id: str, relationship_id: str, relationship_version: int) -> dict:
    relationship_key = ObjectRelationshipTable().generate_relationship_key(relationship_id, relationship_version)
    try:
        obj_relationship_item = ObjectRelationshipTable().get_item({'target-id': target_id,
                                                                    'relationship-key': relationship_key})
    except ClientError as ce:
        logger.error(ce)
        raise InternalError("Unable to get item from Object Relationship Table", ce)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)

    if not obj_relationship_item:
        unpacked_obj = unpack_composite_object_id(target_id)
        raise NoSuchObjectRelationshipVersion(
            "Invalid Object Relationship Version {0} for Object Relationship {1}".format(relationship_version,
                                                                                         relationship_id),
            "Object Relationship {0} Version {1} "
            "does not exist for Object ID {2}, Collection ID {3}, "
            "Version Number {4}".format(relationship_id, relationship_version,
                                        unpacked_obj["object-id"],
                                        unpacked_obj["collection-id"],
                                        unpacked_obj["version-number"]))
    return obj_relationship_item


def get_latest_object_relationship(target_id: str, relationship_id: str) -> dict:
    try:
        obj_relationship_items = ObjectRelationshipTable().query_items(
            max_items=1,
            KeyConditionExpression='TargetID = :target_id',
            ExpressionAttributeValues={":target_id": {"S": target_id}},
            ScanIndexForward=False)
    except ClientError as e:
        raise InternalError("Unable to query items from Object Relationship Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not obj_relationship_items:
        unpacked_obj = unpack_composite_object_id(target_id)
        raise NoSuchObjectRelationship("Invalid Object Relationship {0}".format(relationship_id),
                                       "Object Relationship {0} does not exist for Object ID {1}, Collection ID {2}, "
                                       "Version Number {3}".format(relationship_id,
                                                                   unpacked_obj["object-id"],
                                                                   unpacked_obj["collection-id"],
                                                                   unpacked_obj["version-number"]))

    return obj_relationship_items[0]


def generate_response_json(input_dict: dict, object_relationship: dict, relationship_version: int) -> dict:
    response_dict = {}
    prop = None

    try:
        default_properties = \
            {
                "related-object": unpack_composite_object_id(object_relationship["related-id"]),
                "object-relationship-state": object_relationship["object-relationship-state"],
                "object-relationship-timestamp": object_relationship["object-relationship-timestamp"],
                "relationship-version": relationship_version
            }

        # required relationship properties
        for prop in command_wrapper.get_required_response_schema_keys('object-relationship'):
            if prop in default_properties:
                response_dict[prop] = default_properties[prop]
            else:
                response_dict[prop] = input_dict[prop]

    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    # optional relationship properties
    for prop in command_wrapper.get_optional_response_schema_keys('object-relationship'):
        if prop in input_dict:
            response_dict[prop] = input_dict[prop]

    return response_dict


def unpack_composite_object_id(composite_object_id: str) -> dict:
    unpacked_obj = {}
    obj_parts = composite_object_id.split('|')
    unpacked_obj['object-id'] = obj_parts[0]
    unpacked_obj['collection-id'] = obj_parts[1]
    unpacked_obj['version-number'] = int(obj_parts[2])
    return unpacked_obj


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    from datetime import datetime

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - LIST OBJECTS COMMAND LAMBDA]")

    ASSET_GROUP = 'feature-kgg'

    os.environ['AWS_PROFILE'] = 'c-sand'

    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_VERSION_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreVersionTable'.format(ASSET_GROUP)
    os.environ['RELATIONSHIP_OWNER_DYNAMODB_TABLE'] = '{0}-DataLake-RelationshipOwnerTable'.format(ASSET_GROUP)
    os.environ['OBJECT_RELATIONSHIP_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectRelationshipTable'.format(ASSET_GROUP)

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "target-object": {
                    "object-id": "targetObjectB",
                    "collection-id": "Caselaw",
                    "version-number": 1
                },
                "relationship-id": "judges",
                # "relationship-version": 4

            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(lambda_event, MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - GET OBJECT RELATIONSHIP COMMAND LAMBDA]")
