import logging
import os
from datetime import datetime

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, ForbiddenStateTransitionError, NoSuchIngestion, \
    UnhandledException
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_constants import ingestion_status, event_names
from lng_datalake_dal.ingestion_table import IngestionTable

__author__ = "John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event version environment variable
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/cancel_ingestion_command-RequestSchema.json',
                         'Schemas/cancel_ingestion_command-ResponseSchema.json')
def lambda_handler(event, context):
    return cancel_ingestion_command(event['request'],
                                    event['context']['client-request-id'],
                                    event['context']['stage'],
                                    event['context']['api-key-id'])


def cancel_ingestion_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # Note that IngestionID is set to the same value as the Request ID used to create the ingestion.
    ingestion_id = request['ingestion-id']

    ingestion_item = get_ingestion_item(ingestion_id)
    if not ingestion_item:
        raise NoSuchIngestion("Invalid Ingestion ID {0}".format(ingestion_id),
                              "Ingestion ID does not exist")

    # owner authorization
    owner_authorization.authorize(ingestion_item['owner-id'], api_key_id)

    if ingestion_item['ingestion-state'] != ingestion_status.PENDING:
        raise ForbiddenStateTransitionError(
            'Cannot cancel ingestion in {0} state'.format(ingestion_item['ingestion-state']),
            'Only a {} ingestion can be cancelled'.format(ingestion_status.PENDING))

    # Set ingestion state to Cancelled for response
    ingestion_item['ingestion-state'] = ingestion_status.CANCELLED

    response_dict = generate_response_json(ingestion_item, stage)

    event_dict = generate_event_dict(request_id, ingestion_id, stage)

    return {'response-dict': response_dict, 'event-dict': event_dict}


def get_ingestion_item(ingestion_id: str) -> dict:
    try:
        ingestion_item = IngestionTable().get_item({'ingestion-id': ingestion_id}, ConsistentRead=True)
    except ClientError as e:
        raise InternalError("Unable to get item from Ingestion Table", e)
    except Exception as e:
        raise UnhandledException(e)

    return ingestion_item


def generate_response_json(ingestion_item: dict, stage: str) -> dict:
    default_properties = {
        "collection-url": "/collections/{0}/{1}".format(stage, ingestion_item['collection-id']),
        "ingestion-url": "/objects/{0}/ingestion/{1}".format(stage, ingestion_item['ingestion-id']),
        "ingestion-expiration-date": time_helper.format_time(ingestion_item['pending-expiration-epoch'])
    }

    response_dict = {}
    prop = None

    try:
        for prop in command_wrapper.get_required_response_schema_keys('ingestion-properties'):
            if prop in default_properties:
                response_dict[prop] = default_properties[prop]
            else:
                response_dict[prop] = ingestion_item[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('ingestion-properties'):
        if prop in ingestion_item:
            response_dict[prop] = ingestion_item[prop]

    return response_dict


def generate_event_dict(request_id: str, ingestion_id: str, stage: str) -> dict:
    event_dict = {
        'event-id': request_id,
        'event-name': event_names.OBJECT_CANCEL_INGESTION,
        'ingestion-id': ingestion_id,
        'request-time': command_wrapper.get_request_time(),
        'event-version': event_version,
        'stage': stage
    }

    return event_dict


if __name__ == '__main__':
    import random
    import string
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CANCEL INGESTION COMMAND LAMBDA]")

    ASSET_GROUP = 'feature'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OWNER_DYNAMODB_TABLE'] = '{0}-DataLake-OwnerTable'.format(ASSET_GROUP)
    os.environ['INGESTION_DYNAMODB_TABLE'] = '{0}-DataLake-IngestionTable'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-EventStoreTable'.format(ASSET_GROUP)


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-type": "aws-apigateway",
                "stage": "v1",
                "client-id": "abcd-client-id-7890",
                "http-method": "POST",
                "resource-id": "RESOURCE ID",
                "resource-path": "RESOURCE PATH",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "ingestion-id": 'a69d8274-436c-11e9-9701-055af6e7f46b'
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - CANCEL INGESTION COMMAND LAMBDA]")
