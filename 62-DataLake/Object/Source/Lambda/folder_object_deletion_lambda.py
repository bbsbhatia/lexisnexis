import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_aws_clients import s3, sqs
from lng_datalake_commons import sqs_extractor, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_constants import event_handler_status

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

FOLDER_DELETE_SQS_URL = os.getenv('FOLDER_DELETE_SQS_URL')


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return folder_object_deletion(event, context)


def folder_object_deletion(event: dict, context: object) -> str:
    sqs_client = sqs.get_client()

    for record in event["Records"]:
        message = sqs_extractor.extract_sqs_message(record)
        if not message.get('bucket-name', '') or not message.get('object-key', ''):
            error_message = 'Error getting values from message. Missing bucket-name or object-key: {}'.format(message)
            error_handler.terminal_error(message, is_event=False, error_message=error_message, context=context)
        elif message.get('retry-count', 0) > 10:
            error_message = 'Too many retries for this message'
            error_handler.terminal_error(message, is_event=False, error_message=error_message, context=context)
        else:
            try:
                delete_object_list(message['bucket-name'], message['object-key'])
                replicated_buckets = message.get('replicated-buckets', [])
                for bucket in replicated_buckets:
                    delete_object_list(bucket, message['object-key'])
            except ClientError:
                message['retry-count'] = message.get('retry-count', 0) + 1
                sqs_client.send_message(QueueUrl=FOLDER_DELETE_SQS_URL, MessageBody=orjson.dumps(message).decode())
            except Exception as e:
                error_handler.terminal_error(message, is_event=False, error_message=str(e), context=context)

    # Used to guarantee integration test pass and errors aren't ignored.
    return event_handler_status.SUCCESS


def delete_object_list(bucket_name: str, object_key: str) -> None:
    try:
        s3_client = s3.get_client()
        paginator = s3_client.get_paginator('list_objects_v2')
        for page in paginator.paginate(Bucket=bucket_name, Prefix=object_key):
            if page['KeyCount'] <= 0:
                logger.error('Error: No objects found with prefix "{}"'.format(object_key))
                return
            key_list = []
            count = 0
            for source in page['Contents']:
                key_list.append({'Key': source['Key']})
                count = count + 1
                if count == 1000:
                    s3_client.delete_objects(Bucket=bucket_name, Delete={'Objects': key_list})
                    logger.info("deleted list of {} objects with prefix {}".format(count, object_key))
                    key_list = []
                    count = 0
            # If any left over
            if key_list:
                s3_client.delete_objects(Bucket=bucket_name, Delete={'Objects': key_list})
            logger.info("deleted list of {} objects with prefix {}".format(len(key_list), object_key))
    except Exception:
        raise


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("** Local Test Run **")

    os.environ["AWS_PROFILE"] = "c-sand"
    FOLDER_DELETE_SQS_URL = 'https://sqs.us-east-1.amazonaws.com/288044017584/feature-DataLake-FolderObjectDeleti-FolderObjectDeletionLambdaDeadLett-AM0S43I2RPI7'

    error_handler.terminal_errors_bucket = "ccs-sandbox-lambda-terminal-errors"

    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-ObjectStoreTable'
    os.environ['OBJECT_STORE_VERSION_DYNAMODB_TABLE'] = 'feature-DataLake-ObjectStoreVersionTable'

    pay_load_1 = {
        "object-key": "test_prefix_1/",
        "object-id": "321",
        "bucket-name": 'feature-datalake-object-store-288044017584'
    }
    pay_load_2 = {
        "object-key": "test_prefix_2/",
        "object-id": "321",
        "bucket-name": 'feature-datalake-object-store-288044017584'
    }
    pay_load_3 = {
        "object-key": "test_prefix_3/",
        "object-id": "321",
        "bucket-name": 'feature-datalake-object-store-288044017584'
    }
    pay_load_4 = {
        "object-key": "test_prefix_4/",
        "object-id": "321",
        "bucket-name": 'feature-datalake-object-store-288044017584'
    }
    sample_record = {
        "Records": [
            {
                "body": json.dumps(pay_load_1)
            },
            {
                "body": json.dumps(pay_load_2)
            },
            {
                "body": json.dumps(pay_load_3)
            },
            {
                "body": json.dumps(pay_load_4)
            }
        ]
    }

    lambda_handler(sample_record, MockLambdaContext)
