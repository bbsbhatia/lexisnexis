import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import publish_sns_topic, session_decorator
from lng_datalake_commons import sns_extractor
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import object_status, event_names, event_handler_status
from lng_datalake_dal.object_store_table import ObjectStoreTable

from service_commons import object_event_handler, object_common

__author__ = "John Morelock, Prashant Srivastava, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

# Set environment variables
target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")

# schemas for the notifications to publish
notification_schemas = ['Schemas/Publish/v0/remove-object.json', 'Schemas/Publish/v1/remove-object.json']

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


# Retry states - additional attribute saved to event message on retry so that processing can resume where it left off
class RetryState:
    INIT = 0
    OST_GET = 1
    OST_UPDATE = 2
    OSVT_PUT = 3
    OSVT_DELETE = 4
    SNS_PUBLISH = 5
    CHANGESET_UPDATE = 6
    DONE = 99


MESSAGE_RETRY_STATE_KEY = 'retry-state'
MESSAGE_OBJECT_DATA_KEY = 'object-data'


@session_decorator.lng_aws_session()
@object_event_handler.queue_wrapper
@error_handler.handle
@tracker.track_event_handler(event_names.OBJECT_REMOVE)
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return remove_object_event_handler(event, context.invoked_function_arn)


def remove_object_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """

    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    object_event_handler.validate_message(message, event_names.OBJECT_REMOVE, event_handler_version, lambda_arn)

    additional_message_attributes = message.get('additional-attributes', {})
    # Unload error handlers additional_attributes so we don't write inaccurate retry messages
    error_handler.additional_attributes = additional_message_attributes

    # If retry state not in message default to state OST_INIT
    retry_state = additional_message_attributes.get(MESSAGE_RETRY_STATE_KEY, RetryState.INIT)
    if retry_state != RetryState.INIT:
        retry_state_names = {value: name for name, value in vars(RetryState).items() if name.isupper()}
        logger.info("Retrying state={}".format(retry_state_names[retry_state]))

    # Getting catalog ids before updating the dynamo db to keep it consistent across all event handlers.
    # If we get catalog ids after updating dynamo db and we get throttled then in some cases we will miss publishing
    # to those catalogs.
    catalog_ids = publish_sns_topic.get_catalog_ids_by_collection_id(message['collection-id'])

    if retry_state <= RetryState.OST_GET:
        # should only exist if current object state is Removed
        existing_object_store_item = object_event_handler.get_consistent_object_item(message["object-id"],
                                                                                     message["collection-id"])
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.OST_UPDATE)
        error_handler.set_message_additional_attribute(MESSAGE_OBJECT_DATA_KEY, existing_object_store_item)
    else:
        existing_object_store_item = additional_message_attributes[MESSAGE_OBJECT_DATA_KEY]

    if not existing_object_store_item:
        logger.info("Object not found in Object Store table; ObjectID: {0} CollectionID: {1}".format(
            message["object-id"], message["collection-id"]))
        retry_state = RetryState.DONE
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.DONE)
    else:
        # Generate these before we do any DB calls to verify we have enough data to conform to both schemas
        object_store_item = generate_object_store_item(message, existing_object_store_item)
        object_store_version_item = object_event_handler.generate_object_store_version_item(object_store_item,
                                                                                            message['event-id'])
        if existing_object_store_item["object-state"] == object_status.REMOVED:
            logger.info("Object already in removed state; ObjectID: {0} CollectionID: {1}".format(
                message["object-id"], message["collection-id"]))
            object_store_item['version-number'] = existing_object_store_item['version-number']
            retry_state = RetryState.CHANGESET_UPDATE
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.CHANGESET_UPDATE)

    # Must perform OST_UPDATE prior to OSVT_PUT/DELETE so we know if object changes
    # Note: safe because Get/ListObject read don't read the OSVT if current version
    if retry_state <= RetryState.OST_UPDATE:
        no_object_changes = object_event_handler.update_object_store_table(object_store_item)
        # Nothing left to do if no object changes
        if no_object_changes:
            retry_state = RetryState.CHANGESET_UPDATE
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.CHANGESET_UPDATE)
        else:
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.OSVT_PUT)

    if retry_state <= RetryState.OSVT_PUT:
        object_event_handler.insert_object_store_version_table(object_store_version_item)
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.OSVT_DELETE)

    if retry_state <= RetryState.OSVT_DELETE:
        object_event_handler.delete_object_store_version_table(object_store_version_item,
                                                               message['old-object-versions-to-keep'])
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.SNS_PUBLISH)

    if retry_state <= RetryState.SNS_PUBLISH:
        sns_publish_data = additional_message_attributes.get(object_event_handler.MESSAGE_SNS_PUBLISH_DATA_KEY, {})
        object_event_handler.send_object_notifications(sns_publish_data, object_store_item, notification_schemas,
                                                       message, target_arn, catalog_ids,
                                                       message.get('changeset-id', ''))
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.CHANGESET_UPDATE)

    if retry_state <= RetryState.CHANGESET_UPDATE:
        object_event_handler.changeset_update(object_store_item, message)
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.DONE)

    # Used to guarantee integration test pass and errors aren't ignored.
    return event_handler_status.SUCCESS


def generate_object_store_item(message, object_data):
    new_dict = {}
    prop = None
    try:
        default_properties = {
            "object-state": object_status.REMOVED,
            "bucket-name": "tombstone",
            "object-key": "tombstone",
            "content-type": "tombstone",
            "raw-content-length": -1,
            "version-number": object_data.get('version-number', 0) + 1,
            "version-timestamp": message['request-time'],
            "collection-hash": object_common.build_collection_id_hash(message['collection-id'])
        }

        for prop in ObjectStoreTable().get_required_schema_keys():
            if prop in default_properties:
                new_dict[prop] = default_properties[prop]
            elif prop == "event-ids":
                new_dict[prop] = ObjectStoreTable().build_event_ids_list(object_data, message['event-id'])
            else:
                new_dict[prop] = message[prop]
    except Exception:
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in ObjectStoreTable().get_optional_schema_keys():
        if prop == 'replicated-buckets' and 'replicated-buckets' in object_data:
            new_dict[prop] = ['tombstone']
        elif prop in message:
            new_dict[prop] = message[prop]

    return new_dict


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE OBJECT EVENT HANDLER LAMBDA]")

    ASSET_GROUP = 'feature-jek'

    os.environ['AWS_PROFILE'] = 'c-sand'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_VERSION_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreVersionTable'.format(ASSET_GROUP)
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = '{0}-DataLake-CatalogCollectionMappingTable'.format(
        ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{0}-DataLake-TrackingTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)

    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = '{0}-datalake-lambda-terminal-errors-288044017584'.format(
        ASSET_GROUP)

    os.environ["SUBSCRIPTION_NOTIFICATION_TOPIC_ARN"] = "arn:aws:sns:us-east-1:288044017584:{0}-DataLakeEvents".format(
        ASSET_GROUP)
    os.environ[
        "RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-jek-DataLake-RetryQueues-RetryQueue-1K2VM44Y6M500"

    target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")

    input_message = {
        "old-object-versions-to-keep": 1,
        "object-id": "aaron",
        "collection-id": "AARON_3",
        "event-id": "d5bbb20c-b201-11e9-8bb5-299d9aa7dde9",
        "stage": "LATEST",
        "request-time": "2019-07-30T13:07:36.207Z",
        "event-name": "Object::Remove",
        "event-version": 1,
        "seen-count": 0,
        "additional-attributes": {
            "retry-state": 0,
            "object-data": {
                "object-state": "Created",
                "collection-hash": "07d6987eb2e02e80b54e33e19c8f69863004a821",
                "collection-id": "AARON_3",
                "version-number": 4,
                "object-key": "943012227688fa7ebaa24761378119f9d50aac53",
                "version-timestamp": "2019-07-29T13:07:36.207Z",
                "object-id": "aaron",
                "content-type": "text/plain",
                "raw-content-length": 15,
                "bucket-name": "feature-jek-dl-object-store-288044017584-use1",
                "replicated-buckets": [
                    "feature-jek-dl-object-store-288044017584-usw2"
                ]
            }
        }
    }
    input_message_body = {
        "Type": "Notification",
        "MessageId": "ad0fcf66-38b1-5728-99db-9aded5a905d9",
        "TopicArn": "arn:aws:sns:us-east-1:195052678233:release-DataLake-EventDispatcher-EventDispatcherTopic-C974JPFGTBR6",
        "Message": json.dumps(input_message),
        "Timestamp": "2019-07-29T12:47:20.916Z",
        "SignatureVersion": "1",
        "Signature": "MBhYKLKscoZ3C764j42y7gzBvn9rHe2R/uZ8XLv/g5Qm1YZObe1Eecyt8lM++6m/BVU53MeHQhLc80LwBWkP4Deoq04JH09o/4i9SlYCUJcj0ybf012m66tb9WbPpU12DSN+PV8sG7/+2WQ6/aeb8hUK/GQKrimLC0xtOZaQ5uAz99VupeBGtbti0Q8Fj4BatjMdSeCGlZbqB9iFrrBgAMPxO/ToItGIK9hI1IHbRRPxce8xCvhxAShgSPiHe3+CKVhmUXz4s7X7viOqQ9XzvWfY1gFfAuuw0go6XtYyDNR2a3o3myMIZqt7Q5A9wDT6IOkO64ynm5FWANiqmWSVCA==",
        "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem",
        "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:195052678233:release-DataLake-EventDispatcher-EventDispatcherTopic-C974JPFGTBR6:dea96194-5501-4959-a4f4-cb892e05861d",
        "MessageAttributes": {
            "stage": {"Type": "String", "Value": "v1"},
            "event-name": {"Type": "String", "Value": "Object::Remove"},
            "event-version": {"Type": "Number", "Value": "1"}
        }
    }
    input_event = {
        "Records": [
            {
                "messageId": "4ea95117-fea8-4073-99e3-b10f56f6ca91",
                "receiptHandle": "AQEBAoJDW52N+Lv4RTd4Mp5MCrivHEULAoVaLYzNeTBRwi62hTwQYEp9Ne0AllrS3JbArYkC3MlFebEG3SHhb0GnpyVJ0jwfSxp4F4FIoOCloVKNYw6DwSz8Q1+URrVGrZZg8QIf3xxGn/LQkBvLYtQq9+uI2D63U5TI2oFmC3RzZ5LoRx01dBPS8us2sRDmwa8rumihascg790msmJ9AAgm4vTadLKEvbtOxpx3jaCZU9DIAbvldJbB+T13oWGTowF4pOczFcYty/VJjl5r69oqKBfNjbehpIKol1PPEVDOqsbnrRf/sCBTLrAZi3WtEjL7xSwDSqXicR90EnYKiVaxyaSVk1ROgyWf/wI132+Uruo7lfIWhf/Xfja3l9ev4k+2zC3wy0ix6tJCPNp+1Kv0Cq41AMO8ZRJkytPrwZbo58hvAh200M9oSqkF8Mexxt66GGo4IcWL6eoXeQNVVuQ7sgEMDDOzzHh1YhPqUcvQNkE=",
                "body": json.dumps(input_message_body),
                "attributes": {
                    "ApproximateReceiveCount": "12",
                    "SentTimestamp": "1551753965006",
                    "SenderId": "AIDAIT2UOQQY3AUEKVGXU",
                    "ApproximateFirstReceiveTimestamp": "1551753965006"
                },
                "messageAttributes": {},
                "md5OfBody": "2adf1c719efaf508bbe720464ef53318",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn:aws:sqs:us-east-1:195052678233:release-DataLake-RemoveObjectEventHandler-V1EventHandlerQueue-JVD6432IPZ7Y",
                "awsRegion": "us-east-1"
            }
        ]
    }


    # Below is used to create a Tracking item as if it came from command lambda
    @session_decorator.lng_aws_session()
    @tracker.track_command(event_names.OBJECT_REMOVE)
    def track(event, context):
        return {"response": {}}


    track_dict = {
        "context": {
            "client-request-id": input_message["event-id"],
            "client-request-time": 1539954955
        },
        "request": {
            "event-name": input_message["event-name"],
            "object-id": input_message["object-id"],
            "collection-id": input_message["collection-id"]
        }
    }
    track(track_dict, {})

    lambda_handler(input_event, MockLambdaContext())
    logger.debug("[LOCAL RUN END - REMOVE OBJECT EVENT HANDLER LAMBDA]")
