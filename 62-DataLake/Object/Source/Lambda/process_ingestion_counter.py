import logging
import os
from inspect import stack, getmodulename

import orjson
from lng_aws_clients import sqs
from lng_datalake_commons import sqs_extractor, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status, ingestion_status, ingestion_counter
from lng_datalake_dal.ingestion_table import IngestionTable

from service_commons import ingestion

__author__ = "Mark Seitter, Prashant Srivastava"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
# if __name__ == '__main__':
#     from aws_xray_sdk import global_sdk_config
#     global_sdk_config.set_sdk_enabled(False)
# libraries = ('botocore',)
# patch(libraries)

INGESTION_COUNTER_QUEUE_URL = os.getenv('INGESTION_COUNTER_QUEUE_URL')
INGESTION_COUNTER_REDRIVE_QUEUE_URL = os.getenv('INGESTION_COUNTER_REDRIVE_QUEUE_URL')

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return process_ingestion_counter(event, context)


def process_ingestion_counter(event: dict, context: object) -> str:
    records = build_update_record(event)
    for ingestion_id, counters in records.items():
        try:
            ingestion_data = update_ingestion_counters(ingestion_id, counters)

            if not ingestion_data:
                continue

            # try to delete collection blocker only if we are updating ObjectTrackedCount
            if ingestion_counter.TRACKED in counters and \
                    ingestion_data['object-tracked-count'] >= ingestion_data['object-count']:
                try:
                    ingestion.delete_collection_blocker(ingestion_data['collection-id'],
                                                        ingestion_data['ingestion-timestamp'],
                                                        ingestion_id)
                except error_handler.retryable_exceptions:
                    retry_message(ingestion_id, counters, reset_counters=True)

            update_ingestion_status_to_completed(ingestion_data, counters)

        except (TerminalErrorException, Exception) as e:
            if isinstance(e, TerminalErrorException):
                logger.error(e)
            else:
                logger.exception(e)

            error_handler.terminal_error({ingestion_id: counters},
                                         is_event=False, error_message=str(e), context=context)

    # Used to guarantee integration test pass and errors aren't ignored.
    return event_handler_status.SUCCESS


def update_ingestion_counters(ingestion_id: str, counters: dict) -> dict:
    try:
        return IngestionTable().update_counters(ingestion_id, counters, counter_only=False)
    except error_handler.retryable_exceptions as e:
        logger.error(e)
        retry_message(ingestion_id, counters, reset_counters=False)


def update_ingestion_status_to_completed(ingestion_data: dict, counters: dict) -> None:
    # IngestionState is updated to "Completed" after the last object in the ingestion is processed.
    # If that update fail, the retry event will increment ObjectUpdatedCount again and its value
    # will be greater than ObjectCount, that's the reason why ">=" is used in this condition.
    # Need to also check ObjectProcessedCount to make sure all items have been processed to protect against
    # over-counting
    ingestion_id = ingestion_data['ingestion-id']
    if ingestion_data['object-updated-count'] >= ingestion_data['object-count'] \
            and ingestion_data['object-processed-count'] >= ingestion_data['object-count'] \
            and ingestion_data['object-tracked-count'] >= ingestion_data['object-count']:
        try:
            ingestion_data['ingestion-state'] = ingestion_status.COMPLETED
            IngestionTable().update_item(ingestion_data)
        except error_handler.retryable_exceptions:
            retry_message(ingestion_id, counters, reset_counters=True)
        except Exception as e:
            raise TerminalErrorException(
                "Unhandled exception occurred while updating ingestion record. {}".format(e))


def build_update_record(event: dict) -> dict:
    update_records = {}
    for record in event['Records']:
        message = sqs_extractor.extract_sqs_message(record)
        ingestion_id, counters = next(iter(message.items()))

        # If we don't have an ingestion ID default to an empty dict if the counter doesnt exist start at 0
        current_value = update_records.get(ingestion_id, {})
        for counter, value in counters.items():
            current_value[counter] = current_value.get(counter, 0) + value
            update_records[ingestion_id] = current_value

    return update_records


def retry_message(ingestion_id: str, counters: dict, reset_counters: bool) -> None:
    # TODO: add a retry count so that we stop after n retries
    if reset_counters:
        # set the counter values to 0 and retry the message
        for counter_type in counters.keys():
            counters[counter_type] = 0
    message = {ingestion_id: counters}
    try:
        sqs.get_client().send_message(QueueUrl=INGESTION_COUNTER_QUEUE_URL, MessageBody=orjson.dumps(message).decode())
    except Exception as e:
        raise TerminalErrorException(
            "Failed to retry ingestion: {0} counter: {1} error: {2}".format(ingestion_id, counters, e))

    logger.info("Retry message published to SQS {0} for ingestion: {1} counter {2}".format(INGESTION_COUNTER_QUEUE_URL,
                                                                                           ingestion_id,
                                                                                           counters))


if __name__ == '__main__':
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    ASSET_GROUP = 'feature-mas'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['INGESTION_DYNAMODB_TABLE'] = "{0}-DataLake-IngestionTable".format(ASSET_GROUP)

    error_handler.terminal_errors_bucket = "{0}-datalake-lambda-terminal-errors-288044017584".format(ASSET_GROUP)

    messages = [{'messageId': '1d324c62-2a49-48e6-bed6-bc8c4bb856ee',
                 'receiptHandle': 'AQEB2p0nmLIP28s3n84aO7Jmpi8CUlU5voVchGnSBOW3OwIwy9KTLI4rBzCQUohFhu1H5QB4GImmDrRc9qajoXzb+C6eVg4m8BKqAOkxz+8JWMqOn25QAoIAcGN769wYuYxReKDXGUq81GaJtd672xq2a0f/PrBvkbLI8Rl3Rcy3kRUEY302pi03+HtDVHvEepdRDF8Un82xU/NKsiespfWBve1G80JiTFWh6FHKAWAi88bW4CXStmuRxiixNCv1vvQYzg+aC0oXWJCqxfxGzlx2cZMPihd3wJaeHSSJoDlndvlqc2mt94K5iStC92RXtu0UV5Gtkf7wewOpBHmGK8yER/JuQ8f71PqaqnSL2O8QIH0/L4NTT4mq89IqvkwQtHeOqvYIeYUIz02gGDEaPBKjT2RxlmWS9B6o7Ytt578c+J75gzHWVOcR8t5eIFJxnkWYZF0WOt0D7hDP6AgIhjnXC9Qz8gfVt5y1zPWosTjaAnE=',
                 'body': '{"567": {"ObjectUpdatedCount": 0}}',
                 'attributes': {
                     'ApproximateReceiveCount': '1',
                     'SentTimestamp': '1553288123647',
                     'SenderId': 'AROAIUGBALTVOVCLLYE3Y:SeitteMA@legal.regn.net',
                     'ApproximateFirstReceiveTimestamp': '1553288123924'
                 },
                 'messageAttributes': {},
                 'md5OfBody': 'fef6ff5cbff8234c7176af60c694c30a',
                 'eventSource': 'aws:sqs',
                 'eventSourceARN': 'arn:aws:sqs:us-east-1:288044017584:feature-mas-DataLake-CreateObjectEventHand-LatestEventHandlerQueue-1GQYFE6A2VR7H',
                 'awsRegion': 'us-east-1'
                 },
                {'messageId': '1d324c62-2a49-48e6-bed6-bc8c4bb856ee',
                 'receiptHandle': 'AQEB2p0nmLIP28s3n84aO7Jmpi8CUlU5voVchGnSBOW3OwIwy9KTLI4rBzCQUohFhu1H5QB4GImmDrRc9qajoXzb+C6eVg4m8BKqAOkxz+8JWMqOn25QAoIAcGN769wYuYxReKDXGUq81GaJtd672xq2a0f/PrBvkbLI8Rl3Rcy3kRUEY302pi03+HtDVHvEepdRDF8Un82xU/NKsiespfWBve1G80JiTFWh6FHKAWAi88bW4CXStmuRxiixNCv1vvQYzg+aC0oXWJCqxfxGzlx2cZMPihd3wJaeHSSJoDlndvlqc2mt94K5iStC92RXtu0UV5Gtkf7wewOpBHmGK8yER/JuQ8f71PqaqnSL2O8QIH0/L4NTT4mq89IqvkwQtHeOqvYIeYUIz02gGDEaPBKjT2RxlmWS9B6o7Ytt578c+J75gzHWVOcR8t5eIFJxnkWYZF0WOt0D7hDP6AgIhjnXC9Qz8gfVt5y1zPWosTjaAnE=',
                 'body': '{"1234": {"ObjectUpdatedCount": 5}}',
                 'attributes': {
                     'ApproximateReceiveCount': '1',
                     'SentTimestamp': '1553288123647',
                     'SenderId': 'AROAIUGBALTVOVCLLYE3Y:SeitteMA@legal.regn.net',
                     'ApproximateFirstReceiveTimestamp': '1553288123924'
                 },
                 'messageAttributes': {},
                 'md5OfBody': 'fef6ff5cbff8234c7176af60c694c30a',
                 'eventSource': 'aws:sqs',
                 'eventSourceARN': 'arn:aws:sqs:us-east-1:288044017584:feature-mas-DataLake-CreateObjectEventHand-LatestEventHandlerQueue-1GQYFE6A2VR7H',
                 'awsRegion': 'us-east-1'
                 },
                {'messageId': '526b5ed3-5a18-4711-bd4e-d82a065269de',
                 'receiptHandle': 'AQEB4iBwLhNT07Iu6uBnz/kthOeDrlL26IwnEE+mRUcgjRiQxwQ0kVPPW0L6XVYZZ+ipbjGrW3EnmhjKV+mq9u+gFZph/gRnKkVOIcj9eGX52AJ/OjldWbxPS6O80ZNMm9T/lK9IkrDdDtQxhZqfBt63+0FNPg4se9ucc2cS7BvWIGycVWofgsEXPfhapqS8ix3zI7vHC0rqiprP4soliDOTGPjGoplY5y2KxfO2h+dJZ0Dy5VUmoKoz5yQimVN7xwpSIfIIHfvYX2pjY31N8qJiwutgCJW6bxT7eNIrYMqunXRmu+re8sDZ5aWcQSZzZPHs0adLi2TUdR5nmno29vJ2UO9Osr+1DGrbHKArLraHgl8r7tV2CKk6geyp5EbDdd9OZc+G67bnjM8jNwQWDo6j8O29XXHGUGhzWIVVkQOM3LGEnFPlFlGyXTZwckHuA4RRUvzh4L2slf+72vV11O4LyrpfVp1BrAvhykqYWMf5dAU=',
                 'body': '{"1234": {"ObjectFailedCount": 6}}',
                 'attributes': {
                     'ApproximateReceiveCount': '1',
                     'SentTimestamp': '1553288123673',
                     'SenderId': 'AROAIUGBALTVOVCLLYE3Y:SeitteMA@legal.regn.net',
                     'ApproximateFirstReceiveTimestamp': '1553288123924'
                 },
                 'messageAttributes': {},
                 'md5OfBody': 'b2af35ba6d1afec1d7447d56f2b73e39',
                 'eventSource': 'aws:sqs',
                 'eventSourceARN': 'arn:aws:sqs:us-east-1:288044017584:feature-mas-DataLake-CreateObjectEventHand-LatestEventHandlerQueue-1GQYFE6A2VR7H',
                 'awsRegion': 'us-east-1'},

                {'messageId': '526b5ed3-5a18-4711-bd4e-d82a065269de',
                 'receiptHandle': 'AQEB4iBwLhNT07Iu6uBnz/kthOeDrlL26IwnEE+mRUcgjRiQxwQ0kVPPW0L6XVYZZ+ipbjGrW3EnmhjKV+mq9u+gFZph/gRnKkVOIcj9eGX52AJ/OjldWbxPS6O80ZNMm9T/lK9IkrDdDtQxhZqfBt63+0FNPg4se9ucc2cS7BvWIGycVWofgsEXPfhapqS8ix3zI7vHC0rqiprP4soliDOTGPjGoplY5y2KxfO2h+dJZ0Dy5VUmoKoz5yQimVN7xwpSIfIIHfvYX2pjY31N8qJiwutgCJW6bxT7eNIrYMqunXRmu+re8sDZ5aWcQSZzZPHs0adLi2TUdR5nmno29vJ2UO9Osr+1DGrbHKArLraHgl8r7tV2CKk6geyp5EbDdd9OZc+G67bnjM8jNwQWDo6j8O29XXHGUGhzWIVVkQOM3LGEnFPlFlGyXTZwckHuA4RRUvzh4L2slf+72vV11O4LyrpfVp1BrAvhykqYWMf5dAU=',
                 'body': '{"1234": {"ObjectCount": 3}}',
                 'attributes': {
                     'ApproximateReceiveCount': '1',
                     'SentTimestamp': '1553288123673',
                     'SenderId': 'AROAIUGBALTVOVCLLYE3Y:SeitteMA@legal.regn.net',
                     'ApproximateFirstReceiveTimestamp': '1553288123924'
                 },
                 'messageAttributes': {},
                 'md5OfBody': 'b2af35ba6d1afec1d7447d56f2b73e39',
                 'eventSource': 'aws:sqs',
                 'eventSourceARN': 'arn:aws:sqs:us-east-1:288044017584:feature-mas-DataLake-CreateObjectEventHand-LatestEventHandlerQueue-1GQYFE6A2VR7H',
                 'awsRegion': 'us-east-1'}
                ]

    input_event = {"Records": messages}

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for _ in range(n)])
    mock_context = MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    lambda_handler(input_event, mock_context)

    logger.debug("Done process_ingestion_counter.")
