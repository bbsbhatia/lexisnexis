import logging
import os

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_constants import event_names
from lng_datalake_constants import object_status

from service_commons import object_command, object_common

__author__ = "John Konderla, Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")
staging_bucket_arn = os.getenv("DATALAKE_STAGING_BUCKET_ARN")
assume_role_arn = os.getenv("ROLE_ARN")
folder_prefix = os.getenv("FOLDER_PREFIX", 'folder-upload')
policy_duration = int(os.getenv('POLICY_DURATION', '3600'))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/update_folder_upload_command-RequestSchema.json',
                         'Schemas/update_folder_upload_command-ResponseSchema.json', event_names.OBJECT_UPDATE_FOLDER)
def lambda_handler(event, context):
    return update_folder_upload_command(event['request'], event['context']['client-request-id'],
                                        event['context']['stage'],
                                        event['context']['api-key-id'])


def update_folder_upload_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    collection_id = request['collection-id']

    # validate object id
    object_id = request['object-id']
    object_command.validate_object_id(object_id)

    # validate collection exists and it's in proper state
    collection_response = object_command.get_collection_response(collection_id)
    owner_id = collection_response['owner-id']

    # owner authorization
    owner_authorization.authorize(owner_id, api_key_id)

    # extract changeset if it exists
    if 'changeset-id' in request:
        changeset = object_command.validate_and_get_changeset_id(request['changeset-id'], collection_response['owner-id'])
    else:
        changeset = {}

    # get object from the Object Store Table if exist
    object_item = object_command.get_object_item(object_id, collection_id)
    object_state = object_item.get('object-state', '')

    active_event_id, staging_prefix, credentials = process_event(request_id, object_state, object_id,
                                                                 collection_id)

    pending_expiration_epoch = object_common.generate_object_expiration_time(collection_response)
    transaction_id = object_common.build_folder_object_transaction_id(active_event_id, collection_id, owner_id, stage,
                                                                      changeset)

    response_dict = {
        "response-dict": {
            'folder-upload': generate_folder_response(transaction_id, staging_prefix, credentials),
            'object': generate_object_response(collection_response, object_id, pending_expiration_epoch, stage,
                                               changeset.get('changeset-id', ''))
        }
    }
    return response_dict


def process_event(event_id: str, object_state: str, object_id: str, collection_id: str) -> tuple:
    """
    Update or remove the event from the Tracking Table based on the previous pending event (if exist)
    or the object state (if object exist). Also, add an expiration epoch (TTL) to the event item
    :param event_id: Event ID which is same as Request ID
    :param object_state: Object State
    :param object_id: Object ID
    :param collection_id: Collection ID
    """
    previous_event, current_event = object_command.get_previous_and_current_events(event_id, object_id, collection_id)
    previous_event_name = previous_event.get('event-name', '')

    # if a previous event exist (CREATE or UPDATE) and has a TTL, then the TTL of that previous event will be extended
    # and the current event will be removed
    if previous_event_name in [event_names.OBJECT_CREATE_FOLDER,
                               event_names.OBJECT_UPDATE_FOLDER] and 'pending-expiration-epoch' in previous_event:
        extend_session = True
        event_id = previous_event['event-id']
    else:
        # change the current event name to CREATE in the following cases: 1- The previous event is REMOVE.
        # 2- There is no previous event, but the object state is Removed or the object doesn't exist.
        if previous_event_name == event_names.OBJECT_REMOVE or (
                not previous_event_name and object_state in [object_status.REMOVED, '']):
            current_event['event-name'] = event_names.OBJECT_CREATE_FOLDER
        extend_session = False

    staging_prefix = object_common.build_staging_prefix(folder_prefix, collection_id, event_id, object_id)

    credentials = object_command.generate_sts_role_token(staging_bucket_arn, staging_prefix, assume_role_arn,
                                                         policy_duration, event_id)

    if extend_session:
        object_command.extend_event_ttl(previous_event, current_event)
    return event_id, staging_prefix, credentials


def generate_folder_response(transaction_id: str, staging_prefix: str, credentials: dict) -> dict:
    folder_dict = {}
    prop = None

    try:
        folder_default_properties = {
            "transaction-id": transaction_id,
            "S3": {
                "Bucket": staging_bucket_name,
                "Prefix": staging_prefix,
            },
            "Credentials": credentials
        }
        # required upload properties
        for prop in command_wrapper.get_required_response_schema_keys('folder-upload'):
            folder_dict[prop] = folder_default_properties[prop]

    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    return folder_dict


def generate_object_response(input_dict: dict, object_id: str, pending_expiration_epoch: int, stage: str,
                             changeset_id: str) -> dict:
    object_dict = {}
    prop = None

    try:

        default_properties = {
            "object-id": object_id,
            "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, object_id, input_dict['collection-id']),
            "collection-url": "/collections/{0}/{1}".format(stage, input_dict['collection-id']),
            "object-state": object_status.PENDING
        }
        # required object properties
        for prop in command_wrapper.get_required_response_schema_keys('object-properties'):
            if prop in default_properties:
                object_dict[prop] = default_properties[prop]
            else:
                object_dict[prop] = input_dict[prop]

    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('object-properties'):
        if prop == 'object-expiration-date' and pending_expiration_epoch:
            object_dict[prop] = time_helper.format_time(pending_expiration_epoch)
        elif prop == 'changeset-id' and changeset_id:
            object_dict[prop] = changeset_id
        elif prop in input_dict:
            object_dict[prop] = input_dict[prop]

    return object_dict


if __name__ == '__main__':
    import random
    import string
    from datetime import datetime
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE UPLOAD COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['DATALAKE_STAGING_BUCKET_NAME'] = 'feature-jek-dl-object-staging-use1'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-jek-DataLake-CollectionTable'
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = 'feature-jek-DataLake-CollectionBlockerTable'
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = 'feature-jek-DataLake-ObjectStoreTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-jek-DataLake-EventStoreTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-jek-DataLake-OwnerTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-jek-DataLake-TrackingTable'

    staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-type": "aws-apigateway",
                "stage": "STAGE",
                "client-id": "abcd-client-id-7890",
                "http-method": "PUT",
                "resource-id": "RESOURCE ID",
                "resource-path": "RESOURCE PATH",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": 'JohnK',
                "object-id": "1010101",
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - UPDATE UPLOAD COMMAND LAMBDA]")
