import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, paginator_token, command_validator
from lng_datalake_commands.exceptions import InternalError, UnhandledException, SemanticError
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_constants import changeset_status
from lng_datalake_constants import collection_status, object_status
from lng_datalake_dal.changeset_object_table import ChangesetObjectTable
from lng_datalake_dal.changeset_table import ChangesetTable
from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.object_store_table import ObjectStoreTable
from lng_datalake_dal.object_store_version_table import ObjectStoreVersionTable

from service_commons import object_common

__author__ = "Aaron Belvo, Kiran G, John Konderla, Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 10))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/describe_objects_command-RequestSchema.json',
                         'Schemas/describe_objects_command-ResponseSchema.json')
def lambda_handler(event, context):
    return describe_objects_command(event['request'], event['context']['stage'])


def describe_objects_command(request: dict, stage: str) -> dict:
    # optional properties
    collection_id = request.get('collection-id')
    object_id = request.get('object-id')
    version_number = request.get('version-number')
    max_items = request.get('max-items', 10)
    next_token = request.get('next-token')
    changeset_id = request.get('changeset-id')

    pagination_token = None

    # If we have a next-token validate the max-results matches the original request
    # Dyanmo doesn't handle pagination if the max-items changes
    if next_token:
        tokens = paginator_token.unpack_validate_pagination_token(next_token, max_items)
        pagination_token = tokens['pagination-token']

    # sanity check for max-items (request schema should prevent)
    command_validator.validate_max_results(max_items, DEFAULT_MAX_ITEMS)

    # if we have a changeset_id, then only search the ChangesetObjectTable
    if changeset_id:
        object_list = process_changeset_request(changeset_id, object_id, version_number, collection_id, max_items,
                                                pagination_token)
    else:
        # If we have both an object and collection id
        if object_id and collection_id:
            object_list = get_object_collection_filter(object_id, collection_id, version_number)
        elif object_id:
            object_list = get_object_filter(object_id, max_items, pagination_token)
        elif collection_id:
            object_list = get_collection_filter(collection_id, max_items, pagination_token)
        else:
            object_list = query_table(max_items, pagination_token)

    return {"response-dict": generate_response_json(object_list, ObjectStoreTable().get_pagination_token(), stage,
                                                    max_items, changeset_id)}


def process_changeset_request(changeset_id: str, object_id: str, version_number: int, collection_id: str,
                              max_items: int, pagination_token: str) -> list:
    object_list = []
    if is_valid_changeset_id(changeset_id):
        validate_changeset_parameters(object_id, version_number, collection_id)
        if object_id and collection_id and version_number:
            object_list = get_changeset_id_object_filter(changeset_id, object_id, version_number, collection_id)
        elif collection_id:
            object_list = get_changeset_id_collection_filter(changeset_id, collection_id, max_items,
                                                             pagination_token)
        else:
            object_list = get_changeset_id_filter(changeset_id, max_items, pagination_token)

    return object_list


def is_valid_changeset_id(changeset_id: str) -> bool:
    try:
        changeset_item = ChangesetTable().get_item({'changeset-id': changeset_id})
        if changeset_item:
            return changeset_item['changeset-state'] != changeset_status.EXPIRED
        else:
            return False
    except ClientError as error:
        raise InternalError("Unable to get item from Changeset Table", error)
    except Exception as e:
        raise UnhandledException(e)


def validate_changeset_parameters(object_id: str, version_number: int, collection_id: str):
    if object_id and not (version_number and collection_id):
        raise SemanticError("Invalid request, missing version-number and/or collection-id",
                            "Requests with changeset-id and object-id must also include version-number and collection-id")
    elif version_number and not (object_id and collection_id):
        raise SemanticError("Invalid request, missing object-id and/or collection-id",
                            "Requests with changeset-id and version-number must also include object-id and collection-id")


def get_changeset_id_object_filter(changeset_id: str, object_id: str, version_number: int, collection_id: str) -> list:
    object_list = []
    try:
        composite_key = ChangesetObjectTable().generate_composite_key(object_id, collection_id, version_number)
        request_dict = \
            {
                "composite-key": composite_key,
                "changeset-id": changeset_id
            }
        object_item = ChangesetObjectTable().get_item(request_dict)
        if object_item:
            object_list.append(object_item)
    except ClientError as error:
        raise InternalError("Unable to get item from Changeset Object Store Table", error)
    except Exception as e:
        raise UnhandledException(e)

    return object_list


def get_changeset_id_collection_filter(changeset_id: str, collection_id: str, max_items: int,
                                       pagination_token: str) -> list:
    try:
        object_list = ChangesetObjectTable().query_items(index='changeset-collection-hash-index',
                                                         KeyConditionExpression='ChangesetID=:changeset_id and CollectionHash=:collection_hash',
                                                         ExpressionAttributeValues={
                                                             ":changeset_id": {"S": changeset_id},
                                                             ":collection_hash": {
                                                                 "S": object_common.build_collection_id_hash(
                                                                     collection_id)}},
                                                         max_items=max_items,
                                                         pagination_token=pagination_token)
    except ClientError as error:
        raise InternalError("Unable to query items from Changeset Object Store Table", error)
    except Exception as e:
        raise UnhandledException(e)

    return object_list


def get_changeset_id_filter(changeset_id: str, max_items: int, pagination_token: str) -> list:
    try:
        object_list = ChangesetObjectTable().query_items(index='changeset-collection-hash-index',
                                                         KeyConditionExpression='ChangesetID=:changeset_id',
                                                         ExpressionAttributeValues={
                                                             ":changeset_id": {"S": changeset_id}, },
                                                         max_items=max_items,
                                                         pagination_token=pagination_token)
    except ClientError as error:
        raise InternalError("Unable to query items from Changeset Object Store Table", error)
    except Exception as e:
        raise UnhandledException(e)

    return object_list


def get_object_collection_filter(object_id: str, collection_id: str, version_number: int) -> list:
    try:
        object_list = ObjectStoreTable().query_items(
            KeyConditionExpression='ObjectID=:object_id AND CollectionID=:collection_id',
            ExpressionAttributeValues={":object_id": {"S": object_id}, ":collection_id": {"S": collection_id}})
        # If we have the version_number pull the data from the OSVT
        if version_number and object_list:
            composite_key = ObjectStoreVersionTable().generate_composite_key(object_id, collection_id)
            osvt_resp = ObjectStoreVersionTable().query_items(
                KeyConditionExpression='CompositeKey=:composite_key and VersionNumber=:version_number',
                ExpressionAttributeValues={":composite_key": {"S": composite_key},
                                           ":version_number": {"N": str(version_number)}})
            # We know it's only the first in the list because we are hitting the exact index for both queries
            # If we dont find the version, we set the list to empty since it's missing
            if osvt_resp:
                object_list[0].update(osvt_resp[0])
            else:
                object_list = []

        return object_list
    except ClientError as error:
        raise InternalError("Unable to query items from Object Store Table/Object Store Version Table", error)
    except Exception as e:
        raise UnhandledException(e)


def get_object_filter(object_id: str, max_items: int, pagination_token: str) -> list:
    try:

        object_list = ObjectStoreTable().query_items(KeyConditionExpression='ObjectID=:object_id',
                                                     ExpressionAttributeValues={":object_id": {"S": object_id}},
                                                     max_items=max_items,
                                                     pagination_token=pagination_token)
        return object_list
    except ClientError as error:
        raise InternalError("Unable to query items from Object Store Table", error)
    except Exception as e:
        raise UnhandledException(e)


def get_collection_filter(collection_id: str, max_items: int, pagination_token: str) -> list:
    try:
        object_list = ObjectStoreTable().query_items(index='objectstore-collection-hash-and-state-index',
                                                     KeyConditionExpression='CollectionHash=:collection_hash',
                                                     ExpressionAttributeValues={":collection_hash": {
                                                         "S": object_common.build_collection_id_hash(
                                                             collection_id)}},
                                                     max_items=max_items,
                                                     pagination_token=pagination_token)
        return object_list
    except ClientError as error:
        raise InternalError("Unable to query items from Object Store Table", error)
    except Exception as e:
        raise UnhandledException(e)


def query_table(max_items: int, pagination_token: str) -> list:
    try:
        object_list = ObjectStoreTable().get_all_items(max_items=max_items,
                                                       pagination_token=pagination_token)
        return object_list
    except ClientError as error:
        raise InternalError("Unable to query items from Object Store Table", error)
    except Exception as e:
        raise UnhandledException(e)


def generate_response_json(object_list: list, pagination_token: str, stage: str, max_items: int,
                           changeset_id: str) -> dict:
    objects_response = []
    collection_data = {}
    collection_state = ''
    expired_object_count = 0
    for object_data in object_list:
        object_state = object_data['object-state']
        if collection_data.get('collection-id') != object_data['collection-id']:
            collection_data = get_collection(object_data['collection-id'])
            collection_state = collection_data['collection-state']

        # Set object state to removed if the collection has been terminated
        if collection_state in (collection_status.TERMINATING,
                                collection_status.TERMINATED,
                                collection_status.TERMINATING_FAILED):
            object_state = object_status.REMOVED

        # if object has expired, then continue so it is not included in the response
        if object_common.is_expired_object(object_data, command_wrapper.get_request_time()):
            expired_object_count += 1
            continue

        response_item = \
            {
                "collection-id": object_data['collection-id'],
                "collection-url": "/collections/{0}/{1}".format(stage, object_data['collection-id']),
                "object-id": object_data['object-id'],
                "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, object_data['object-id'],
                                                                          object_data['collection-id']),
                "object-state": object_state,
                "asset-id": collection_data['asset-id'],
                "owner-id": collection_data['owner-id']
            }

        if object_state == object_status.CREATED:
            add_create_only_keys(response_item, object_data)

        if object_state == object_status.REMOVED:
            response_item['version-timestamp'] = object_data['version-timestamp']
            response_item['version-number'] = object_data['version-number']
        if changeset_id:
            response_item['changeset-id'] = object_data['changeset-id']

        objects_response.append(response_item)

    response = {"objects": objects_response}
    if pagination_token:
        response['next-token'] = paginator_token.generate_pagination_token(max_items, pagination_token)

    response['item-count'] = len(object_list) - expired_object_count

    return response


def get_collection(collection_id: str) -> dict:
    # get collection
    try:
        return CollectionTable().get_item({'collection-id': collection_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Collection Table")
    except Exception as e:
        raise UnhandledException(e)


def add_create_only_keys(response_item: dict, object_data: dict) -> None:
    response_item["content-type"] = object_data['content-type']
    response_item["object-key-url"] = object_common.build_object_key_url(object_data['object-key'],
                                                                         object_data['content-type'])
    response_item['version-number'] = object_data['version-number']
    response_item['version-timestamp'] = object_data['version-timestamp']

    response_item["S3"] = {
        "Key": object_data['object-key'],
        "Bucket": object_data['bucket-name']
    }
    if object_data['raw-content-length'] > 0:
        response_item["raw-content-length"] = object_data['raw-content-length']

    if 'pending-expiration-epoch' in object_data:
        response_item['object-expiration-date'] = time_helper.format_time(
            object_data.pop('pending-expiration-epoch'))
    if 'object-metadata' in object_data:
        response_item['object-metadata'] = object_data['object-metadata']
    if 'replicated-buckets' in object_data:
        response_item['replicated-buckets'] = object_data['replicated-buckets']


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    from datetime import datetime
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - DESCRIBE OBJECTS COMMAND LAMBDA]")

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'

    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_VERSION_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreVersionTable'.format(ASSET_GROUP)

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "collection-id": 'Agency',
                "object-id": "testFile1"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(lambda_event, MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - DESCRIBE OBJECTS COMMAND LAMBDA]")
