import logging
import os
from datetime import datetime

from aws_xray_sdk.core import patch
from lng_aws_clients import s3
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, UNSUPPORTED_MEDIA_TYPE
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_constants import event_names, object_status

from service_commons import object_command, object_common

__author__ = "Shekhar Ralhan, Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event version environment variable
event_version = os.getenv("EVENT_VERSION", 1)
staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")

UTF_8_ENCODING = "utf-8"


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/create_object_command-RequestSchema.json',
                         'Schemas/create_object_command-ResponseSchema.json', event_names.OBJECT_CREATE)
def lambda_handler(event, context):
    return create_object_command(event['request'],
                                 event['context']['client-request-id'],
                                 event['context']['stage'],
                                 event['context']['api-key-id'])


def create_object_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required parameters
    collection_id = request['collection-id']
    content_type = request['content-type']

    # Just to verify we check content-types consistently across APIs incase the vm strips out data (like the + symbol)
    if not object_common.is_valid_content_type(content_type):
        raise UNSUPPORTED_MEDIA_TYPE

    # optional parameters
    is_large_object = request.get('is-large-object', False)
    # we use request id for the object id if it's not supplied
    object_id = request.get('object-id', request_id)
    object_metadata = request.get('object-metadata', {})

    object_command.validate_object_id(object_id)

    if object_metadata:
        object_command.validate_object_metadata(object_metadata)

    # validate collection exists and it's in proper state
    collection_response = object_command.get_collection_response(collection_id)

    # owner authorization
    owner_authorization.authorize(collection_response['owner-id'], api_key_id)

    # extract changeset if it exists
    if 'changeset-id' in request:
        changeset = object_command.validate_and_get_changeset_id(request['changeset-id'],
                                                                 collection_response['owner-id'])
    else:
        changeset = {}

    object_command.validate_create_event(request_id, object_id, collection_id)
    pending_expiration_epoch = object_common.generate_object_expiration_time(collection_response)

    if is_large_object:
        object_key = object_common.build_large_object_key(request_id, stage)
        content_length = 0
        content_sha1 = ""
        object_hash = ""
        response_dict = process_large_object_request(request, object_id, collection_response, object_key,
                                                     request_id, stage, pending_expiration_epoch)
    else:
        object_content = object_command.get_object_content(request)

        content_sha1, object_hash, object_key = \
            object_common.build_object_key_and_hashes(object_content,
                                                      content_type,
                                                      object_id,
                                                      command_wrapper.get_request_time(),
                                                      object_metadata,
                                                      collection_response['collection-hash'])

        content_length = len(object_content)
        object_command.put_object_to_staging_bucket(object_content, staging_bucket_name, object_key, content_type)
        object_response_props = {
            'pending-expiration-epoch': pending_expiration_epoch,
            'changeset-id': changeset.get('changeset-id', ''),
            'object-metadata': object_metadata,
            'object-state': object_status.CREATED
        }

        # response json to be returned
        response_dict = generate_response_json(collection_response, object_id, object_key,
                                               stage, object_response_props)

    object_properties = {
        'object-id': object_id,
        'object-key': object_key,
        'content-type': content_type,
        'raw-content-length': content_length,
        'object-hash': object_hash,
        'content-sha1': content_sha1,
        'is-large-object': is_large_object
    }
    event_dict = generate_event_dict(request_id, object_properties, collection_response,
                                     pending_expiration_epoch, object_metadata, stage, changeset)

    return {"response-dict": response_dict,
            'event-dict': event_dict}


def process_large_object_request(request: dict, object_id: str, collection_response: dict, object_key: str,
                                 request_id: str, stage: str,
                                 pending_expiration_epoch: int) -> dict:
    content_md5 = request.get('content-md5')
    if not content_md5:
        raise InvalidRequestPropertyValue('Content-MD5 does not exist in the request',
                                          'Content-MD5 is required for large objects')
    encoded_md5_hash = object_command.encode_md5(content_md5)
    metadata_dict = {
        'event-id': request_id,
        'request-time': command_wrapper.get_request_time(),
        'event-name': event_names.OBJECT_CREATE_PENDING,
        'object-id': object_id,
        'collection-id': collection_response['collection-id'],
        'collection-hash': collection_response['collection-hash'],
        'old-object-versions-to-keep': str(collection_response['old-object-versions-to-keep']),
        'stage': stage
    }
    if pending_expiration_epoch:
        metadata_dict["pending-expiration-epoch"] = str(pending_expiration_epoch)
    signed_url = s3.get_client().generate_presigned_url('put_object',
                                                        Params={'Bucket': staging_bucket_name,
                                                                'Key': object_key,
                                                                'ContentType': request['content-type'],
                                                                'ContentMD5': encoded_md5_hash,
                                                                'Metadata': metadata_dict
                                                                },
                                                        HttpMethod='PUT')

    large_upload_props = {'url': signed_url,
                          'headers': {'Content-Type': request['content-type'],
                                      'Content-MD5': encoded_md5_hash}}
    object_props = {
        'changeset-id': '',
        'object-metadata': {},
        'object-state': object_status.PENDING,
        'pending-expiration-epoch': pending_expiration_epoch
    }
    response_dict = generate_response_json(collection_response, object_id, "", stage, object_props,
                                           large_upload_props)
    return response_dict


def generate_response_json(collection_properties: dict, object_id: str, object_key: str, stage: str, object_props: dict,
                           large_upload_props: dict = None) -> dict:
    response_dict = {}
    object_dict = {}
    large_dict = {}
    prop = None

    try:
        default_properties = {
            "object-id": object_id,
            "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, object_id,
                                                                      collection_properties['collection-id']),
            "collection-url": "/collections/{0}/{1}".format(stage, collection_properties['collection-id']),
            "object-state": object_props['object-state'],
            # TODO: remove if/else below once support for large object is removed
            "temporary-object-key-url": object_common.build_temporary_object_key_url(object_key) if object_key else ""
        }
        for prop in command_wrapper.get_required_response_schema_keys('object-properties'):
            object_dict[prop] = default_properties[prop] if prop in default_properties else collection_properties[prop]

        if large_upload_props:
            # required large-upload properties
            for prop in command_wrapper.get_required_response_schema_keys('large-upload'):
                large_dict[prop] = large_upload_props[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(e))

    object_dict = generate_optional_object_properties(object_dict, collection_properties, object_props)

    response_dict['object'] = object_dict
    if large_dict:
        response_dict['large-upload'] = large_dict

    return response_dict


def generate_optional_object_properties(object_dict: dict, collection_properties: dict, object_props: dict) -> dict:
    """
    inserts the optional properties of an object to the client
    :param collection_properties: attributes of the collection
    :param object_dict: the object properties already generated that will be returned to the client
    :param object_props: properties of the object being created
    :return: the object response dictionary
    """
    for prop in command_wrapper.get_optional_response_schema_keys('object-properties'):
        if 'object-expiration-date' == prop and object_props.get('pending-expiration-epoch'):
            object_dict[prop] = time_helper.format_time(object_props['pending-expiration-epoch'])
        elif prop in ('changeset-id', 'object-metadata') and object_props.get(prop):
            object_dict[prop] = object_props[prop]
        elif prop in collection_properties:
            object_dict[prop] = collection_properties[prop]

    return object_dict


def generate_event_dict(request_id: str, object_properties: dict, collection_data: dict, pending_expiration_epoch: int,
                        object_metadata: dict, stage: str, changeset: dict):
    event_dict = {
        'event-id': request_id,
        'request-time': command_wrapper.get_request_time(),
        'object-id': object_properties['object-id'],
        'collection-id': collection_data['collection-id'],
        'collection-hash': collection_data['collection-hash'],
        'bucket-name': staging_bucket_name,
        'object-key': object_properties['object-key'],
        'content-type': object_properties['content-type'],
        'raw-content-length': object_properties['raw-content-length'],
        'old-object-versions-to-keep': collection_data['old-object-versions-to-keep'],
        'event-version': event_version,
        'stage': stage
    }
    if pending_expiration_epoch:
        event_dict['pending-expiration-epoch'] = pending_expiration_epoch
    if object_metadata:
        event_dict['object-metadata'] = object_metadata
    if object_properties['is-large-object']:
        # use an asterisk after request-time on a pending event so primary key will be different when
        # ProcessLargeObject writes the real event
        event_dict['request-time'] = "{0}*".format(command_wrapper.get_request_time())
        event_dict['event-name'] = event_names.OBJECT_CREATE_PENDING
    else:
        event_dict['event-name'] = event_names.OBJECT_CREATE
        event_dict['object-hash'] = object_properties['object-hash']
        event_dict['content-sha1'] = object_properties['content-sha1']

    if changeset:
        event_dict['changeset-id'] = changeset['changeset-id']
        event_dict['changeset-expiration-epoch'] = changeset['pending-expiration-epoch']

    return event_dict


if __name__ == '__main__':
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json
    from importlib import reload

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE OBJECT COMMAND LAMBDA]")

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['DATALAKE_STAGING_BUCKET_NAME'] = '{0}-dl-object-staging-use1'.format(ASSET_GROUP)
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-EventStoreTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{0}-DataLake-OwnerTable'.format(ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{0}-DataLake-TrackingTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)
    os.environ['CHANGESET_DYNAMODB_TABLE'] = '{0}-DataLake-ChangesetTable'.format(ASSET_GROUP)

    os.environ['DATA_LAKE_URL'] = 'https://datalake-{0}.content.aws.lexis.com'.format(ASSET_GROUP)
    reload(object_common)

    staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-type": "aws-apigateway",
                "stage": "LATEST",
                "client-id": "abcd-client-id-7890",
                "http-method": "POST",
                "resource-id": "RESOURCE ID",
                "resource-path": "RESOURCE PATH",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": "AARON_2",
                "content-type": "text/plain",
                # "is-large-object": True,
                # "content-md5": "f315202b28422ed5c2af4f843b8c2764"
                "object-id": "test7",
                # "body-encoding": "base64",
                "object-metadata": {
                    "name1": "value1",
                    "name2": "value2"
                },
                "body": "This is a test"
                # "changeset-id": 'jek-changeset'
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE OBJECT COMMAND LAMBDA]")
