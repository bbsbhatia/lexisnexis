import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_aws_clients import s3
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_constants import ingestion_status
from lng_datalake_dal.collection_blocker_table import CollectionBlockerTable
from lng_datalake_dal.ingestion_table import IngestionTable

from service_commons import object_common

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event handler version environment variable
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


# Retry states - additional attribute saved to event message on retry so that processing can resume where it left off
class RetryState:
    MULTIPART = 1
    COLLECTION_BLOCKER = 2
    CANCEL_INGESTION = 3


MESSAGE_RETRY_STATE_KEY = 'retry-state'


@session_decorator.lng_aws_session()
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return cancel_ingestion_event_handler(event, context.invoked_function_arn)


def cancel_ingestion_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    # If retry state not in message default to state MULTIPART
    retry_state = message.get('additional-attributes', {}).get(MESSAGE_RETRY_STATE_KEY, RetryState.MULTIPART)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.OBJECT_CANCEL_INGESTION):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event,
                                                                               event_names.OBJECT_CANCEL_INGESTION))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    ingestion_id = message['ingestion-id']
    ingestion_item = get_ingestion_item(ingestion_id)
    if not ingestion_item:
        raise TerminalErrorException("Ingestion item not found {0}".format(ingestion_id))

    ingestion_state = ingestion_item['ingestion-state']
    if ingestion_state == ingestion_status.PENDING:

        # Note that the Object Key in the staging bucket is based on the Request ID used to create the ingestion
        object_key = object_common.build_multipart_ingestion_key(ingestion_id, message['stage'])

        # Skip MULTIPART if it has already been processed
        if retry_state <= RetryState.MULTIPART:
            abort_multipart_upload(ingestion_item['bucket-name'], object_key, ingestion_item['upload-id'])
            # Save retry-state so we do not abort multipart upload again if a retry occurs after this
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.COLLECTION_BLOCKER)

        # Skip COLLECTION_BLOCKER if it has already been processed
        if retry_state <= RetryState.COLLECTION_BLOCKER:
            remove_collection_blocker(ingestion_id)
            # Save retry-state so we do not run remove collection blocker again if a retry occurs after this
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.CANCEL_INGESTION)

        cancel_ingestion(ingestion_item, message['event-id'])

    elif ingestion_state == ingestion_status.CANCELLED:
        logging.info("Duplicate ingestion event ignored for ingestion-id {}".format(ingestion_id))
    else:
        raise TerminalErrorException(
            "Failed to process event {} ingestion cannot be cancelled while in state {}".format(event, ingestion_state))

    # Used to guarantee integration test pass and errors aren't ignored.
    return event_handler_status.SUCCESS


def get_ingestion_item(ingestion_id: str) -> dict:
    try:
        ingestion_item = IngestionTable().get_item({'ingestion-id': ingestion_id})
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Unhandled exception occurred: {}".format(e))

    return ingestion_item


def abort_multipart_upload(bucket_name: str, object_key: str, upload_id: str):
    try:
        s3.get_client().abort_multipart_upload(Bucket=bucket_name, Key=object_key, UploadId=upload_id)
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Unhandled exception occurred: {}".format(e))


def remove_collection_blocker(ingestion_id):
    cb_items = get_collection_blocker_item(ingestion_id)

    if len(cb_items) == 1:
        delete_collection_blocker_item(cb_items[0])
    elif len(cb_items) > 1:
        raise TerminalErrorException(
            "Duplicate items found in Collection Blocker Table for ingestion ID = {}".format(ingestion_id))
    else:
        raise TerminalErrorException("Collection Blocker item not found for ingestion id {}".format(ingestion_id))


def get_collection_blocker_item(ingestion_id: str) -> list:
    param_dict = {
        'FilterExpression': 'RequestID=:ingestion_id',
        'ExpressionAttributeValues': {":ingestion_id": {"S": ingestion_id}}
    }

    try:
        item_list = CollectionBlockerTable().get_all_items(**param_dict)
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Unhandled exception deleting collection blocker for ingestion ID = {};"
                                     " Exception = {}".format(ingestion_id, e))

    return item_list


def delete_collection_blocker_item(item: dict):
    try:
        dict_key = {
            "collection-id": item['collection-id'],
            "request-time": item['request-time']
        }
        CollectionBlockerTable().delete_item(dict_key)
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Unhandled exception deleting collection blocker = {} {};"
                                     " Exception = {}".format(item['collection-id'], item['request-time'], e))


def cancel_ingestion(ingestion_item: dict, event_id: str) -> dict:
    ingestion_item['ingestion-state'] = ingestion_status.CANCELLED
    ingestion_item["event-ids"] = IngestionTable().build_event_ids_list(ingestion_item, event_id)

    try:
        IngestionTable().update_item(ingestion_item)
        logger.debug("Updated ingestion state to {0} for ingestion id {1}".format(ingestion_status.CANCELLED,
                                                                                  ingestion_item['ingestion-id']))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Unhandled exception updating ingestion ID {}; Exception = {}" \
                                     .format(ingestion_item['ingestion-id'], e))
    return ingestion_item


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CANCEL INGESTION EVENT HANDLER LAMBDA]")

    ASSET_GROUP = 'feature'

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/" \
                                    "{0}-DataLake-RetryQueues-RetryQueue-135L28JKZ7FDJ".format(ASSET_GROUP)
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "{0}-datalake-lambda-terminal-errors-288044017584".format(
        ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)
    os.environ['INGESTION_DYNAMODB_TABLE'] = '{0}-DataLake-IngestionTable'.format(ASSET_GROUP)
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {
        "event-id": "YoZSKZJKwRIcpawv",
        "event-name": "Object::IngestionCancel",
        "ingestion-id": "a69d8274-436c-11e9-9701-055af6e7f46b",
        "request-time": "2019-03-08T20:13:25.926Z",
        "event-version": 1,
        "stage": "LATEST",
        "seen-count": 0,
        'additional-attributes': {'retry-state': 2}
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext())
    logger.debug("[LOCAL RUN END - CANCEL INGESTION EVENT HANDLER LAMBDA]")
    # retry_lambda_handler(sample_event, MockLambdaContext)
