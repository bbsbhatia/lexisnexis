import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands import owner_authorization
from lng_datalake_commands.exceptions import InternalError, NoSuchChangeset, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_commons import time_helper
from lng_datalake_constants import changeset_status, event_names
from lng_datalake_dal.changeset_table import ChangesetTable

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/close_changeset_command-RequestSchema.json',
                         'Schemas/close_changeset_command-ResponseSchema.json', event_names.CHANGESET_CLOSE)
def lambda_handler(event, context):
    return close_changeset_command(event['request'],
                                   event['context']['client-request-id'],
                                   event['context']['stage'],
                                   event['context']['api-key-id'])


def close_changeset_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    changeset_id = request['changeset-id']

    changeset = validate_and_get_changeset_id(changeset_id)

    # Get OwnerID from authorization key
    owner_authorization.authorize(changeset['owner-id'], api_key_id)

    response_dict = generate_response_json(changeset, stage)

    event_dict = generate_event_store_item(changeset, request_id, stage)

    return {'response-dict': response_dict, 'event-dict': event_dict}


def validate_and_get_changeset_id(changeset_id: str) -> dict:
    try:
        changeset = ChangesetTable().get_item({'changeset-id': changeset_id}, ConsistentRead=True)
    except ClientError as e:
        raise InternalError("Unable to get item from Changeset Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not changeset or changeset['changeset-state'] == changeset_status.EXPIRED:
        raise NoSuchChangeset("Invalid Changeset ID {0}".format(changeset_id),
                              "Changeset ID does not exist")

    logger.debug(
        'Validated that Changeset ID {0} exists'.format(changeset_id))
    return changeset


def generate_response_json(changeset: dict, stage: str) -> dict:
    response_dict = {}

    required_props = command_wrapper.get_required_response_schema_keys('changeset-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('changeset-properties')

    try:
        required_props_defaults = \
            {
                "owner-id": changeset['owner-id'],
                "changeset-url": "/objects/{0}/changeset/{1}".format(stage, changeset['changeset-id']),
                "changeset-state": changeset_status.CLOSING,
                "changeset-expiration-date": time_helper.format_time(changeset['pending-expiration-epoch'])
            }
        for prop in required_props:
            if prop in required_props_defaults:
                response_dict[prop] = required_props_defaults[prop]
            else:
                response_dict[prop] = changeset[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(e))

    for prop in optional_props:
        if prop in changeset:
            response_dict[prop] = changeset[prop]

    return response_dict


def generate_event_store_item(changeset_item: dict, request_id: str, stage: str) -> dict:
    event_dict = {
        'changeset-id': changeset_item['changeset-id'],
        'event-id': request_id,
        'request-time': command_wrapper.get_request_time(),
        'event-name': event_names.CHANGESET_CLOSE,
        'event-version': event_version,
        'stage': stage
    }

    return event_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    from datetime import datetime

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE CHANGESET COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-maen-DataLake-OwnerTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-maen-DataLake-EventStoreTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "POST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "description": "my first changeset"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE CHANGESET COMMAND LAMBDA]")
