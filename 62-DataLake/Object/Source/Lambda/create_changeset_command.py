import logging
import os
from datetime import timedelta, datetime

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper
from lng_datalake_commands import owner_authorization
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue
from lng_datalake_commons import session_decorator
from lng_datalake_commons import time_helper
from lng_datalake_constants import changeset_status, event_names

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/create_changeset_command-RequestSchema.json',
                         'Schemas/create_changeset_command-ResponseSchema.json')
def lambda_handler(event, context):
    return create_changeset_command(event['request'],
                                    event['context']['client-request-id'],
                                    event['context']['stage'],
                                    event['context']['api-key-id'])


def create_changeset_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    description = request.get('description')

    if description and len(description) > 100:
        raise InvalidRequestPropertyValue("Invalid Description", "Description must be less than 100 characters")

    # Get OwnerID from authorization key
    owner_response = owner_authorization.get_owner_by_api_key_id(api_key_id)
    owner_id = owner_response['owner-id']

    changeset_expiration_date = generate_changeset_expiration_epoch()

    response_dict = generate_response_json(request, request_id, owner_id, changeset_expiration_date, stage)

    event_dict = generate_event_store_item(response_dict, request_id, changeset_expiration_date, stage)

    return {'response-dict': response_dict, 'event-dict': event_dict}


def generate_changeset_expiration_epoch() -> int:
    return int(
        (datetime.strptime(command_wrapper.get_request_time() + '+0000', "%Y-%m-%dT%H:%M:%S.%fZ%z")
         + timedelta(days=30)).timestamp())


def generate_response_json(request: dict, changeset_id: str, owner_id: int, changeset_expiration: int,
                           stage: str) -> dict:
    response_dict = {}
    prop = None

    required_props_defaults = \
        {
            "changeset-id": changeset_id,
            "owner-id": owner_id,
            "changeset-url": "/objects/{0}/changeset/{1}".format(stage, changeset_id),
            "changeset-state": changeset_status.OPENED,
            "changeset-timestamp": command_wrapper.get_request_time(),
            "changeset-expiration-date": time_helper.format_time(changeset_expiration)
        }

    required_props = command_wrapper.get_required_response_schema_keys('changeset-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('changeset-properties')

    try:
        for prop in required_props:
            if prop in required_props_defaults:
                response_dict[prop] = required_props_defaults[prop]
            else:
                response_dict[prop] = request[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in request:
            response_dict[prop] = request[prop]

    return response_dict


def generate_event_store_item(changeset_response: dict, request_id: str, changeset_expiration_date: int,
                              stage: str) -> dict:
    event_dict = {
        'changeset-id': request_id,
        'owner-id': changeset_response['owner-id'],
        'event-id': request_id,
        'request-time': command_wrapper.get_request_time(),
        'pending-expiration-epoch': changeset_expiration_date,
        'event-name': event_names.CHANGESET_OPEN,
        'event-version': event_version,
        'stage': stage
    }

    if 'description' in changeset_response:
        event_dict['description'] = changeset_response['description']

    return event_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    from datetime import datetime

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE CHANGESET COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-maen-DataLake-OwnerTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-maen-DataLake-EventStoreTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "POST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "description": "my first changeset"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE CHANGESET COMMAND LAMBDA]")
