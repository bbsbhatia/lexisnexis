import logging
import os

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, UNSUPPORTED_MEDIA_TYPE
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_constants import event_names, object_status

from service_commons import object_command, object_common

__author__ = "Shekhar Ralhan, Mark Seitter, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event version environment variable
event_version = os.getenv("EVENT_VERSION", 1)

staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")
UTF_8_ENCODING = "utf-8"


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/update_object_multipart_upload_command-RequestSchema.json',
                         'Schemas/update_object_multipart_upload_command-ResponseSchema.json',
                         event_names.OBJECT_UPDATE)
def lambda_handler(event, context):
    return update_object_multipart_upload_command(event['request'], event['context']['client-request-id'],
                                                  event['context']['stage'],
                                                  event['context']['api-key-id'])


def update_object_multipart_upload_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required parameters
    collection_id = request['collection-id']
    content_type = request['content-type']

    if not object_common.is_valid_content_type(content_type):
        raise UNSUPPORTED_MEDIA_TYPE

    # optional parameters
    object_id = request['object-id']
    object_metadata = request.get('object-metadata', {})

    object_command.validate_object_id(object_id)

    if object_metadata:
        object_command.validate_object_metadata(object_metadata)

    # validate collection exists and it's in proper state
    collection_response = object_command.get_collection_response(collection_id)

    # owner authorization
    owner_authorization.authorize(collection_response['owner-id'], api_key_id)

    # extract changeset if it exists
    if 'changeset-id' in request:
        changeset = object_command.validate_and_get_changeset_id(request['changeset-id'], collection_response['owner-id'])
    else:
        changeset = {}

    object_store_response = object_command.get_object_item(object_id, collection_id)

    previous_event, _ = object_command.get_previous_and_current_events(request_id, object_id, collection_id)

    event_name = object_command.determine_upsert_event_name(previous_event.get('event-name', ''),
                                                            object_store_response.get('object-state', ''),
                                                            True)
    event_prop = {'event-id': request_id,
                  'event-name': event_name}

    object_key = object_common.build_multipart_object_key(request_id, stage)
    pending_expiration_epoch = object_common.generate_object_expiration_time(collection_response)

    request_info = {'object-id': object_id,
                    'content-type': content_type,
                    'event-name': event_name,
                    'request-id': request_id,
                    'request-time': command_wrapper.get_request_time(),
                    'stage': stage,
                    'object-key': object_key}
    if object_metadata:
        request_info['object-metadata'] = object_metadata
    if changeset:
        request_info['changeset-id'] = changeset['changeset-id']
        request_info['changeset-expiration-epoch'] = changeset['pending-expiration-epoch']

    multipart_resp = object_command.create_multipart_upload(request_info, collection_response, pending_expiration_epoch,
                                                            staging_bucket_name)
    optional_object_props = {
        'changeset-id': changeset.get('changeset-id'),
        'pending-expiration-epoch': pending_expiration_epoch,
        'object-metadata': object_metadata
    }

    response_dict = generate_response_json(collection_response, object_id, request_id, collection_id, multipart_resp,
                                           stage, optional_object_props)

    object_properties = {
        'object-id': object_id,
        'object-key': object_key,
        'content-type': content_type
    }
    event_dict = generate_event_dict(event_prop, object_properties, collection_response, stage,
                                     pending_expiration_epoch, object_metadata, changeset)

    return {'response-dict': response_dict, 'event-dict': event_dict}


def generate_response_json(input_dict: dict, object_id: str, request_id: str, collection_id: str, multipart_resp: dict,
                           stage: str, optional_object_props: dict) -> dict:
    response_dict = {}
    object_dict = {}
    multipart_dict = {}
    prop = None

    default_properties = {
        "object-id": object_id,
        "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, object_id, collection_id),
        "collection-url": "/collections/{0}/{1}".format(stage, collection_id),
        "object-state": object_status.PENDING,
        "temporary-object-key-url": object_common.build_temporary_multipart_object_key_url(request_id, stage)
    }
    try:
        for prop in command_wrapper.get_required_response_schema_keys('object-properties'):
            if prop in default_properties:
                object_dict[prop] = default_properties[prop]
            else:
                object_dict[prop] = input_dict[prop]

        # required multipart-upload properties
        for prop in command_wrapper.get_required_response_schema_keys('multipart-upload'):
            multipart_dict[prop] = multipart_resp[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('object-properties'):
        if 'object-expiration-date' == prop and optional_object_props.get('pending-expiration-epoch'):
            object_dict[prop] = time_helper.format_time(optional_object_props['pending-expiration-epoch'])
        elif prop in ('changeset-id', 'object-metadata') and optional_object_props.get(prop):
            object_dict[prop] = optional_object_props[prop]
        elif prop in input_dict:
            object_dict[prop] = input_dict[prop]

    response_dict['object'] = object_dict
    response_dict['multipart-upload'] = multipart_dict

    return response_dict


def generate_event_dict(event_prop: dict, object_properties: dict, collection_response: dict, stage: str,
                        pending_expiration_epoch: int, object_metadata: dict, changeset: dict):
    event_dict = {
        'event-id': event_prop['event-id'],
        # use an asterisk after request-time on a pending event so primary key will be different when
        # ProcessLargeObject writes the real event
        'request-time': "{0}*".format(command_wrapper.get_request_time()),
        'event-name': event_prop['event-name'],
        'object-id': object_properties['object-id'],
        'collection-id': collection_response['collection-id'],
        'collection-hash': collection_response['collection-hash'],
        'bucket-name': staging_bucket_name,
        'object-key': object_properties['object-key'],
        'content-type': object_properties['content-type'],
        'raw-content-length': 0,
        'old-object-versions-to-keep': collection_response['old-object-versions-to-keep'],
        'event-version': event_version,
        'stage': stage
    }
    if pending_expiration_epoch:
        event_dict['pending-expiration-epoch'] = pending_expiration_epoch
    if object_metadata:
        event_dict['object-metadata'] = object_metadata

    if changeset:
        event_dict['changeset-id'] = changeset['changeset-id']
        event_dict['changeset-expiration-epoch'] = changeset['pending-expiration-epoch']

    return event_dict


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import random
    import string
    from datetime import datetime
    import json
    from importlib import reload
    from lng_datalake_commons.error_handling import error_handler

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE MULTIPART OBJECT COMMAND LAMBDA]")

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{0}-DataLake-OwnerTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-EventStoreTable'.format(ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{0}-DataLake-TrackingTable'.format(ASSET_GROUP)

    os.environ['DATALAKE_STAGING_BUCKET_NAME'] = '{0}-dl-object-staging-use1'.format(ASSET_GROUP)
    staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")

    os.environ['DATA_LAKE_URL'] = 'https://datalake-{0}.content.aws.lexis.com'.format(ASSET_GROUP)
    reload(object_common)

    os.environ[
        'RETRY_QUEUE_URL'] = 'https://sqs.us-east-1.amazonaws.com/288044017584/feature-jek-DataLake-RetryQueues-RetryQueue-KHWM7JSCNCHO'
    error_handler.retry_queue_url = os.getenv('RETRY_QUEUE_URL')


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-type": "aws-apigateway",
                "stage": "LATEST",
                "client-id": "abcd-client-id-7890",
                "http-method": "PUT",
                "resource-id": "RESOURCE ID",
                "resource-path": "RESOURCE PATH",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": "AARON_2",
                "content-type": "text/plain",
                "object-id": "aaron-multi"
            }
        }

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - UPDATE MULTIPART OBJECT COMMAND LAMBDA]")
