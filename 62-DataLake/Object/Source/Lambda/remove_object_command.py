import logging
import os

import orjson
from aws_xray_sdk.core import patch, xray_recorder
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_constants import event_names, object_status

from service_commons import object_command, object_common

__author__ = "John Morelock, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get event version environment variable
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/remove_object_command-RequestSchema.json',
                         'Schemas/remove_object_command-ResponseSchema.json', event_names.OBJECT_REMOVE)
def lambda_handler(event, context):
    return remove_object_command(event['request'],
                                 event['context']['client-request-id'],
                                 event['context']['stage'],
                                 event['context']['api-key-id'])


def remove_object_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required properties
    collection_id = request['collection-id']
    object_id = request['object-id']

    # validate collection exists and is in proper state
    # TODO: Ask team if this should return a NoSuchCollection or should it return an Unauthorized
    # Given that they are technically not allowed to remove from said collection (even if it doesnt exist)
    collection_data = object_command.get_collection_response(collection_id)

    # owner authorization
    owner_authorization.authorize(collection_data['owner-id'], api_key_id)

    # extract changeset if it exists
    if 'changeset-id' in request:
        changeset = object_command.validate_and_get_changeset_id(request['changeset-id'], collection_data['owner-id'])
    else:
        changeset = {}

    pending_expiration_epoch = object_common.generate_object_expiration_time(collection_data)
    # Create an event for object remove
    event_dict = \
        {
            'event-id': request_id,
            'request-time': command_wrapper.get_request_time(),
            'event-name': event_names.OBJECT_REMOVE,
            'object-id': object_id,
            'collection-id': collection_id,
            'event-version': event_version,
            'stage': stage,
            'old-object-versions-to-keep': collection_data['old-object-versions-to-keep']
        }

    response_dict = generate_response_json(collection_data, object_id, stage, pending_expiration_epoch,
                                           changeset.get('changeset-id', ''))

    if pending_expiration_epoch:
        event_dict['pending-expiration-epoch'] = pending_expiration_epoch

    if changeset:
        event_dict['changeset-id'] = changeset['changeset-id']
        event_dict['changeset-expiration-epoch'] = changeset['pending-expiration-epoch']

    logger.debug("Event Store JSON: {0}".format(orjson.dumps(event_dict).decode()))

    return {'response-dict': response_dict, 'event-dict': event_dict}


def generate_response_json(collection_data: dict, object_id: str, stage: str, pending_expiration_epoch: int,
                           changeset_id: str) -> dict:
    response_dict = {}
    prop = None

    default_properties = {
        "object-id": object_id,
        "object-url": "/objects/{0}/{1}?collection-id={2}".format(stage, object_id, collection_data['collection-id']),
        "collection-url": "/collections/{0}/{1}".format(stage, collection_data['collection-id']),
        "object-state": object_status.REMOVED
    }

    try:
        for prop in command_wrapper.get_required_response_schema_keys('object-properties'):
            if prop in default_properties:
                response_dict[prop] = default_properties[prop]
            else:
                response_dict[prop] = collection_data[prop]

    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('object-properties'):
        if prop == 'object-expiration-date' and pending_expiration_epoch:
            response_dict[prop] = time_helper.format_time(pending_expiration_epoch)
        elif 'changeset-id' == prop and changeset_id:
            response_dict[prop] = changeset_id
        elif prop in collection_data:
            response_dict[prop] = collection_data[prop]

    return response_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import random
    import string
    from datetime import datetime

    xray_recorder.context._context_missing = "LOG_ERROR"

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE OBJECT COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-DataLake-CollectionTable'
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-ObjectStoreTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-OwnerTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-DataLake-TrackingTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "DELETE",
                "stage": "LATEST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": 'zero-versions-id-regression-1552407285',
                "object-id": "090cebb8-44e2-11e9-971f-f52b1118d130"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - REMOVE OBJECT COMMAND LAMBDA]")
