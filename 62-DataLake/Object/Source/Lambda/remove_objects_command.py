import logging
import os

import orjson
from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_aws_clients import s3
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names

from service_commons import object_command, object_common

__author__ = "John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)
staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")
# Max batch size is limited to seven digits in the event handler.
max_remove_object_batch_size = int(os.getenv("MAX_OBJECTS_REMOVE_BATCH_SIZE", 20000))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/remove_objects_command-RequestSchema.json',
                         'Schemas/remove_objects_command-ResponseSchema.json', event_names.OBJECT_BATCH_REMOVE)
def lambda_handler(event, context):
    return remove_objects_command(event['request'],
                                  event['context']['client-request-id'],
                                  event['context']['stage'],
                                  event['context']['api-key-id'])


def remove_objects_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required properties
    collection_id = request['collection-id']
    object_ids = request['object-ids']

    # Optional properties
    description = request.get('description')

    if description and len(description) > 100:
        raise InvalidRequestPropertyValue("Invalid Description", "Description must be less than 100 characters")

    # validate collection exists and is in proper state
    collection_resp = object_command.get_collection_response(collection_id)

    # owner authorization
    owner_authorization.authorize(collection_resp['owner-id'], api_key_id)

    # extract changeset if it exists
    if 'changeset-id' in request:
        changeset = object_command.validate_and_get_changeset_id(request['changeset-id'], collection_resp['owner-id'])
    else:
        changeset = {}

    validate_batch(object_ids)

    object_ids = remove_duplicates(object_ids)

    batch_key = object_common.build_batch_key(request_id, stage)

    write_batch_to_staging(batch_key, object_ids, description)

    response_dict = generate_response_json(request, collection_resp, stage, len(object_ids))

    event_dict = generate_event_dict(response_dict, collection_resp, request_id, stage, changeset, batch_key)

    logger.debug("Event Store JSON: {0}".format(orjson.dumps(event_dict).decode()))

    return {'response-dict': response_dict, 'event-dict': event_dict}


def validate_batch(object_ids: list) -> None:
    object_cnt = len(object_ids)
    if object_cnt < 1 or object_cnt > max_remove_object_batch_size:
        raise InvalidRequestPropertyValue("Invalid batch size {0}".format(object_cnt),
                                          "Batch size must be between 1 and {0}".format(max_remove_object_batch_size))

    for index, object_id in enumerate(object_ids):
        corrective_action = object_common.validate_object_id(object_id)
        if corrective_action:
            raise InvalidRequestPropertyValue("Invalid object-id in batch at index {0}".format(index),
                                              corrective_action)


def remove_duplicates(object_ids: list) -> list:
    object_set = set(object_ids)
    if len(object_ids) != len(object_set):
        logger.info("Removed {} duplicates from request".format(len(object_ids) - len(object_set)))
        object_ids = list(object_set)

    return object_ids


def write_batch_to_staging(batch_key: str, object_ids: list, description: str = None):

    batch_properties = {}
    try:
        metadata = {'object-count': len(object_ids)}
        if description:
            batch_properties['description'] = description
            metadata['description'] = description
        batch_properties['object-ids'] = object_ids
        batch_file_content = orjson.dumps(batch_properties).decode()

        s3.put_s3_object(batch_file_content, staging_bucket_name, batch_key,
                         content_type='application/json',
                         meta_dict=metadata)
        logger.debug("Wrote remove objects batch to s3://{0}/{1}".format(staging_bucket_name, batch_key))
    except ClientError as ce:
        logger.error(ce)
        raise InternalError("Unable to create S3 object {0}".format(batch_key), ce)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)


def generate_response_json(input_dict: dict, collection_response: dict, stage: str, object_ids_count: int) -> dict:
    response_dict = {}
    prop = None

    try:
        default_properties = {
            "object-count": object_ids_count,
            "remove-timestamp": command_wrapper.get_request_time(),
            "collection-url": "/collections/{0}/{1}".format(stage, collection_response['collection-id'])
        }

        # required ingestion properties
        for prop in command_wrapper.get_required_response_schema_keys('batch-properties'):
            if prop in default_properties:
                response_dict[prop] = default_properties[prop]
            else:
                response_dict[prop] = collection_response[prop]

    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in command_wrapper.get_optional_response_schema_keys('batch-properties'):
        if prop in input_dict:
            response_dict[prop] = input_dict[prop]
        elif prop in collection_response:
            response_dict[prop] = collection_response[prop]

    return response_dict


def generate_event_dict(json_resp: dict, collection_resp: dict, request_id: str, stage: str, changeset: dict, batch_key:str) -> dict:
    """
    Generate event dict.

    Note that a put is used to "update" the object and/or object version tables so all the required and optional
    attributes normally provided by remove_object_command.py also need to be specified here.

    """
    event_dict = {
        'event-id': request_id,
        'request-time': json_resp['remove-timestamp'],
        'event-name': event_names.OBJECT_BATCH_REMOVE,
        'collection-id': json_resp['collection-id'],
        'event-version': event_version,
        'stage': stage,
        'old-object-versions-to-keep': collection_resp['old-object-versions-to-keep'],
        'bucket-name': staging_bucket_name,
        'object-key': batch_key
    }

    pending_expiration_epoch = object_common.generate_object_expiration_time(collection_resp)
    if pending_expiration_epoch:
        event_dict['pending-expiration-epoch'] = pending_expiration_epoch

    if 'description' in json_resp:
        event_dict['description'] = json_resp['description']
    if changeset:
        event_dict['changeset-id'] = changeset['changeset-id']
        event_dict['changeset-expiration-epoch'] = changeset['pending-expiration-epoch']

    return event_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import random
    import string
    from datetime import datetime

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE OBJECT COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    ASSET_GROUP = 'feature-mas'
    os.environ['DATALAKE_STAGING_BUCKET_NAME'] = '{0}-dl-object-staging-use1'.format(ASSET_GROUP)
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{0}-DataLake-OwnerTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-EventStoreTable'.format(ASSET_GROUP)

    staging_bucket_name = os.getenv("DATALAKE_STAGING_BUCKET_NAME")


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "DELETE",
                "stage": "LATEST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-id": '63',
                "object-ids": ["Object1", "Object2"],
                "description": 'PCT-123456'
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - REMOVE OBJECTS COMMAND LAMBDA]")
