import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import sns_extractor, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.event_store_table import EventStoreTable

__author__ = "Mark Seitter, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

my_location = os.path.dirname(__file__)

# Get event version environment variable
event_version = os.getenv("EVENT_VERSION", 1)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return process_large_object(event)


def process_large_object(event: dict) -> str:
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    event_name = message['event-name']

    if event_name == event_names.OBJECT_CREATE_PENDING:
        event_name = event_names.OBJECT_CREATE
    elif event_name == event_names.OBJECT_UPDATE_PENDING:
        event_name = event_names.OBJECT_UPDATE
    else:
        raise TerminalErrorException("Unknown event {0} in message: {1}".format(event_name, message))

    event_store_dict = generate_event_dict(message, event_name)

    logger.info('event_dict: {}'.format(event_store_dict))
    write_to_event_store(event_store_dict)

    return event_handler_status.SUCCESS


def generate_event_dict(sns_message: dict, event_name: str) -> dict:
    event_dict = {
        'event-id': sns_message['event-id'],
        'object-id': sns_message['object-id'],
        'collection-id': sns_message['collection-id'],
        'collection-hash': sns_message['collection-hash'],
        'request-time': sns_message['request-time'],
        'object-key': sns_message['object-key'],
        'content-type': sns_message['content-type'],
        'raw-content-length': sns_message['raw-content-length'],
        'is-new-object': sns_message['is-new-object'],
        'is-large-object-request': True,
        'old-object-versions-to-keep': sns_message['old-object-versions-to-keep'],
        'event-version': event_version,
        'stage': sns_message['stage'],
        'event-name': event_name
    }
    if 'pending-expiration-epoch' in sns_message:
        event_dict['pending-expiration-epoch'] = sns_message['pending-expiration-epoch']
    if 'object-metadata' in sns_message:
        event_dict['object-metadata'] = sns_message['object-metadata']
    # object-hash and content-sha1 will only be in SNS message for large objects (not folder objects)
    if 'object-hash' in sns_message:
        event_dict['object-hash'] = sns_message['object-hash']
    if 'content-sha1' in sns_message:
        event_dict['content-sha1'] = sns_message['content-sha1']
    if 'changeset-id' in sns_message:
        event_dict['changeset-id'] = sns_message['changeset-id']
        event_dict['changeset-expiration-epoch'] = sns_message['changeset-expiration-epoch']
    return event_dict


def write_to_event_store(event_dict):
    try:
        EventStoreTable().put_item(event_dict)
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        logging.exception(e)
        raise TerminalErrorException("Unhandled exception occurred: {0}".format(e))


if __name__ == '__main__':
    import json
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-EventStoreTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)

    create_event = {"Records": [{
        "EventSource": "aws:sns", "EventVersion": "1.0",
        "EventSubscriptionArn": "arn:aws:sns:us-east-1:288044017584:feature-DataLake-GlueDispatcherLambda-GlueDispatcherNotificationTopic-16DWXT40I23IG:07b004f1-0100-4d65-913a-b8feeb90c7c2",
        "Sns": {
            "Type": "Notification",
            "MessageId": "87ff85b6-c59b-57fc-ac82-d6189d794306",
            "TopicArn": "arn:aws:sns:us-east-1:288044017584:feature-DataLake-GlueDispatcherLambda-GlueDispatcherNotificationTopic-16DWXT40I23IG",
            "Subject": "empty",
            "Message": "{\"object-id\": \"objects\", \"content-type\": \"text/plain\", \"raw-content-length\": 9, \"collection-id\": \"1\", \"old-object-versions-to-keep\": 2, \"content-type\": \"text/plain\", \"object-key\": \"99792da1-7a83-11e8-b1f0-d7e3d7549474.gz\", \"event-id\": \"99792da1-7a83-11e8-b1f0-d7e3d7549474\", \"event-name\": \"Object::CreatePending\", \"is-new-object\": true, \"stage\": \"LATEST\"}",
            "Timestamp": "2018-06-28T03:31:46.943Z", "SignatureVersion": "1",
            "Signature": "HA0eIUZubHu+hSVhlvx5HyRR7wBA90uph699/duVQYbPCRfjMOiYirb9Ft0+07/IXkWEo9ObnLuF8zwuiM6pWsMl8uhVFZRk1M7IWRxT0yeeWBHiEp6itWzu58WXV+GtLASHb03QRlsosC5IyRNX0RY2a24/3vDDXYdl9kf3/OGZmqXf4HQmqoFA8Hxzo6LRj585FMecZbwB3MVh/+YrBEIsyUp6jvVvMqoVAQjr0oXv0Doz/U7dUfQAt22K/azWUNf4xxVS3vWiHft0af+b2y3bDsaICkrnJshMTST+I8fDaY7FffKXw7qV824K7dFy4qm1ensQNOEeJaWXLsoc/g==",
            "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-eaea6120e66ea12e88dcd8bcbddca752.pem",
            "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:feature-DataLake-GlueDispatcherLambda-GlueDispatcherNotificationTopic-16DWXT40I23IG:07b004f1-0100-4d65-913a-b8feeb90c7c2",
            "MessageAttributes": {}}}]}

    # update_event = {
    #     "Records": [{
    #         "EventSource": "aws:sns",
    #         "EventVersion": "1.0",
    #         "EventSubscriptionArn": "arn:aws:sns:us-east-1:288044017584:AWSTEST-DataLakeObjectStagingBucket-LargeObjectNotificationTopic-GKOB926S01ZE:fa7f4a33-902f-4feb-be6f-ada8d295b453",
    #         "Sns": {
    #             "Type": "Notification",
    #             "MessageId": "f92567da-0839-5a02-8c7e-345d5dee1942",
    #             "TopicArn": "arn:aws:sns:us-east-1:288044017584:AWSTEST-DataLakeObjectStagingBucket-LargeObjectNotificationTopic-GKOB926S01ZE",
    #             "Subject": "Amazon S3 Notification",
    #             "Message": "{\"event-id\": \"jYUWitNvFMsoybgQ\", \"object-id\": \"48cf81db-762b-11e8-becb-7f9fb9037eee\", \"content-type\": \"application/json\", \"raw-content-length\": \"3\", \"collection-id\": \"1\", \"object-key\": \"jYUWitNvFMsoybgQ\", \"event-name\": \"Object::UpdatePending\", \"is-new-object\": true}",
    #             "Timestamp": "2018-05-29T13:20:23.754Z",
    #             "SignatureVersion": "1",
    #             "Signature": "VNJ/6WalmOmJTskmG6LhE8LZKgZlQ9/z1OOTNsspZMMRCCM3CV8zDUzu+vSPcZMa7MRGEL6mnrIZntorDPx0swo+vMvtwskTNvDshA9H+hlr/PgHDtKTbbcG3JAHty81aby5hmBx9IIO9apQq8Qv6IUazmepH9axe8XR3u0mcFYzMhGoYM754SXA/Kxi4v8d9XGwky8zyTJFJGiNfsB720f7Jv4Dd0krkOntmJA3PRp3m3i+JDlLisFkSFh7Qsf2rmoEp1kxwTaWWQH2cydthQLNfXkmlWZkOndjhBdBt+YjRGaT9BI3OCS8N1rogqzFdHgTEtK0AoH9kcqai6u0JA==",
    #             "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-eaea6120e66ea12e88dcd8bcbddca752.pem",
    #             "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:AWSTEST-DataLakeObjectStagingBucket-LargeObjectNotificationTopic-GKOB926S01ZE:fa7f4a33-902f-4feb-be6f-ada8d295b453",
    #             "MessageAttributes": {
    #
    #             }
    #         }
    #     }]
    # }
    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for _ in range(n)])

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    json_response = lambda_handler(create_event, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("Done process_large_object.")
