import logging
import os
from datetime import datetime

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, paginator_token, command_validator
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_constants import collection_status, object_status
from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.object_store_table import ObjectStoreTable

from service_commons import object_common

__author__ = "Aaron Belvo, Kiran G, John Konderla, Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)
collection_hash_and_state_idx = 'objectstore-collection-hash-and-state-index'
accepted_object_states = ['CREATED', 'REMOVED', 'FAILED']
DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 1000))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/list_objects_command-RequestSchema.json',
                         'Schemas/list_objects_command-ResponseSchema.json')
def lambda_handler(event, context):
    return list_objects_command(event['request'])


def list_objects_command(request: dict) -> dict:
    # optional properties
    collection_id = request.get('collection-id')
    object_state = request.get('state')
    max_items = request.get('max-items', 1000)
    next_token = request.get('next-token')

    state_filter = None
    pagination_token = None
    object_list = []

    # If we have a next-token validate the max-results matches the original request
    # Dyanmo doesn't handle pagination if the max-items changes
    if next_token:
        tokens = paginator_token.unpack_validate_pagination_token(next_token, max_items)
        pagination_token = tokens['pagination-token']

    # sanity check for max-items (request schema should prevent)
    command_validator.validate_max_results(max_items, DEFAULT_MAX_ITEMS)

    # sanity check for object-state
    if object_state:
        state_filter = object_state.strip().upper()
        if state_filter not in accepted_object_states:
            raise InvalidRequestPropertyValue("Invalid state value: {0}".format(object_state),
                                              "Accepted values {}".format(accepted_object_states))

    ost_page_token = None
    # collection id provided
    is_valid_collection = True
    if collection_id:
        collection_data = get_collection(collection_id)
        if collection_data:
            if collection_data['collection-state'] in (collection_status.TERMINATING,
                                                       collection_status.TERMINATED,
                                                       collection_status.TERMINATING_FAILED):
                is_valid_collection = False
            object_list = query_with_collection_filter(collection_id, state_filter, max_items, pagination_token)
            ost_page_token = ObjectStoreTable().get_pagination_token()

    else:
        object_list = query_table(state_filter, max_items, pagination_token)
        ost_page_token = ObjectStoreTable().get_pagination_token()

    # Filter out any expired objects
    object_list = [object_data for object_data in object_list
                   if not object_common.is_expired_object(object_data, command_wrapper.get_request_time())]

    return {"response-dict": generate_response_json(object_list, ost_page_token, max_items, is_valid_collection)}


def get_collection(collection_id: str) -> dict:
    # validate collection exists and is not terminated
    try:
        return CollectionTable().get_item({'collection-id': collection_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Collection Table", e)
    except Exception as e:
        raise UnhandledException(e)


def query_with_collection_filter(collection_id: str, state_filter: str, max_items: int, pagination_token: str) -> list:
    param_dict = {}
    collection_hash = object_common.build_collection_id_hash(collection_id)
    if state_filter:
        param_dict['KeyConditionExpression'] = 'CollectionHash=:collection_hash and ObjectState=:object_state'
        param_dict['ExpressionAttributeValues'] = {":collection_hash": {"S": collection_hash},
                                                   ":object_state": {"S": getattr(object_status, state_filter)}}
    else:
        param_dict['KeyConditionExpression'] = 'CollectionHash=:collection_hash'
        param_dict['ExpressionAttributeValues'] = {":collection_hash": {"S": collection_hash}}

    try:
        return ObjectStoreTable().query_items(index=collection_hash_and_state_idx,
                                              max_items=max_items,
                                              pagination_token=pagination_token,
                                              **param_dict)
    except ClientError as e:
        raise InternalError("Unable to query items from Object Store Table"
                            " index {0}".format(collection_hash_and_state_idx), e)
    except Exception as e:
        raise UnhandledException(e)


def query_table(state_filter: str, max_items: int, pagination_token: str) -> list:
    param_dict = {}
    if state_filter:
        param_dict['FilterExpression'] = 'ObjectState=:object_state'
        param_dict['ExpressionAttributeValues'] = {":object_state": {"S": getattr(object_status, state_filter)}}

    try:
        object_list = ObjectStoreTable().get_all_items(max_items=max_items,
                                                       pagination_token=pagination_token,
                                                       **param_dict)
        return object_list
    except ClientError as e:
        raise InternalError("Unable to get item from Object Store Table", e)
    except Exception as e:
        raise UnhandledException(e)


def generate_response_json(object_list: list, pagination_token: str, max_items: int, active_collection: bool) -> dict:
    objects_response = []
    default_object_state = {}
    if not active_collection:
        default_object_state = {'object-state': object_status.REMOVED}
    for object_data in object_list:
        object_state = default_object_state.get('object-state', object_data['object-state'])
        response_item = \
            {
                "collection-id": object_data['collection-id'],
                "object-id": object_data['object-id'],
                "object-state": object_state
            }
        if object_state == object_status.CREATED:
            response_item["object-key-url"] = object_common.build_object_key_url(object_data['object-key'],
                                                                                 object_data['content-type'])
            response_item["raw-content-length"] = object_data['raw-content-length']
            response_item["S3"] = {
                "Key": object_data['object-key'],
                "Bucket": object_data['bucket-name']
            }

        objects_response.append(response_item)

    response = {"objects": objects_response}
    if pagination_token:
        response['next-token'] = paginator_token.generate_pagination_token(max_items, pagination_token)

    response['item-count'] = len(object_list)

    return response


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - LIST OBJECTS COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    ASSET_GROUP = 'feature-ajb'

    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_VERSION_DYNAMODB_TABLE'] = '{}-DataLake-ObjectStoreVersionTable'.format(ASSET_GROUP)

    collection_hash_and_state_idx = 'objectstore-collection-hash-and-state-index'

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "dummy": 0,
                "collection-id": 'LATax',
                # "state": 'all',
                # "max-items": 3
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(lambda_event, MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - LIST OBJECTS COMMAND LAMBDA]")
