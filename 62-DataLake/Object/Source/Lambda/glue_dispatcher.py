import hashlib
import logging
import os
import random
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from boto3.s3.transfer import TransferConfig
from botocore.config import Config
from botocore.exceptions import ClientError
from lng_aws_clients import glue, s3, sqs, sns
from lng_datalake_commons import sns_extractor, session_decorator, publish_sns_topic
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_dal.object_store_table import ObjectStoreTable
from lng_datalake_dal.tracking_table import TrackingTable

from service_commons import object_common, object_event_handler

__author__ = "Mark Seitter, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

my_location = os.path.dirname(__file__)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

# Get event version environment variable
event_version = os.getenv("EVENT_VERSION", 1)

COMPLETION_TOPIC_ARN = os.getenv("GLUE_DISPATCHER_TOPIC_ARN")
DATALAKE_BUCKET_NAME = os.getenv("DATALAKE_BUCKET_NAME")
GLUE_JOB_NAME = os.getenv("GLUE_LARGE_OBJECT_PROCESSOR_JOB_NAME")
MESSAGE_RETRY_STATE_KEY = 'retry-state'
OBJECT_METADATA_KEY = 'metadata'
CONTENT_LENGTH_KEY = 'content-length'
TRACKING_RECORD_KEY = 'tracking-data'
OBJECT_DATA_KEY = 'object-data'
SEEN_COUNT = 'seen-count'
COMPLETED_COPY_METADATA_KEY = 'copied-object-metadata'
MAX_OBJECT_SIZE = int(os.getenv('MAX_OBJECT_SIZE', 750 * 1024 * 1024))
MAX_RETRY_COUNT = int(os.getenv("MAX_RETRY_COUNT", 1000))
LATEST_MULTIPART_QUEUE_URL = os.getenv('LATEST_MULTIPART_QUEUE_URL')
V1_MULTIPART_QUEUE_URL = os.getenv('V1_MULTIPART_QUEUE_URL')

transfer_config = TransferConfig(multipart_threshold=8388608,
                                 max_concurrency=5,
                                 multipart_chunksize=8388608,
                                 num_download_attempts=5,
                                 max_io_queue=100,
                                 io_chunksize=262144,
                                 use_threads=True)

# Needs to equal to thread pool * transferConfig max_concurrency
client_config = Config(
    max_pool_connections=10,
)


# Retry states - additional attribute saved to event message on retry so that processing can resume where it left off
class RetryState:
    INIT = 0
    GET_OBJECT_METADATA = 10
    GET_TRACKING_RECORD = 20
    UPDATE_TRACKING_RECORD = 30
    GET_OBJECT_STORE_RECORD = 40
    DISPATCH_UPLOAD = 50
    SEND_COMPLETED_COPY_NOTIFICATION = 60
    DONE = 99


@session_decorator.lng_aws_session()
@object_event_handler.queue_wrapper
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))

    return glue_dispatcher(event, context)


def glue_dispatcher(event: dict, context: object) -> str:
    stage = context.invoked_function_arn.split(":")[7]
    try:
        sns_message = sns_extractor.extract_sns_message(event)
        logger.info("Event Message: {0}".format(orjson.dumps(sns_message).decode()))
        # Instantiate s3 client using the config, further calls to s3.get_client will get the existing client
        s3.get_client(config=client_config)

        s3_message = sns_message['Records'][0]
        s3_object = s3_message['s3']
        staging_bucket_name = s3_object['bucket']['name']
        staging_object_key = s3_object['object']['key']
        content_length = s3_object['object']['size']

        additional_message_attributes = s3_message.get('additional-attributes', {})
        # Unload error handlers additional_attributes so we don't write inaccurate retry messages
        error_handler.additional_attributes = additional_message_attributes

        # If retry state not in message default to state OST_INIT
        retry_state = additional_message_attributes.get(MESSAGE_RETRY_STATE_KEY, RetryState.INIT)
        if retry_state != RetryState.INIT:
            retry_state_names = {value: name for name, value in vars(RetryState).items() if name.isupper()}
            logger.info("Retrying state={}".format(retry_state_names[retry_state]))

        if retry_state <= RetryState.GET_OBJECT_METADATA:
            # Get the metadata so we can update the tracking record to remove the pending expiration epoch
            metadata = get_s3_object_metadata(staging_bucket_name, staging_object_key)
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.GET_TRACKING_RECORD)
            error_handler.set_message_additional_attribute(OBJECT_METADATA_KEY, metadata)

        else:
            metadata = additional_message_attributes[OBJECT_METADATA_KEY]

        if retry_state <= RetryState.UPDATE_TRACKING_RECORD:
            modify_tracking_item(retry_state, metadata, additional_message_attributes)

        # make sure this request is for is the oldest Tracking item, we should always do this
        is_oldest_tracking_item(metadata['object-id'], metadata['collection-id'], metadata['event-id'])

        if retry_state <= RetryState.GET_OBJECT_STORE_RECORD:
            # get object store item so we can determine version number and object hash to send in glue_args
            object_store_item = get_object_store_item(metadata['object-id'], metadata['collection-id'])

            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.DISPATCH_UPLOAD)
            error_handler.set_message_additional_attribute(OBJECT_DATA_KEY, object_store_item)
        else:
            object_store_item = additional_message_attributes[OBJECT_DATA_KEY]

        if retry_state <= RetryState.DISPATCH_UPLOAD:
            retry_state, completed_object_metadata = dispatch_upload(retry_state, metadata, staging_bucket_name,
                                                                     staging_object_key,
                                                                     object_store_item,
                                                                     content_length)
        else:
            completed_object_metadata = additional_message_attributes[COMPLETED_COPY_METADATA_KEY]

        if retry_state <= RetryState.SEND_COMPLETED_COPY_NOTIFICATION:
            transform_message_and_publish_to_topic(completed_object_metadata,
                                                   COMPLETION_TOPIC_ARN, stage)
            error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.DONE)
    except Exception as e:
        handle_exceptions(e, event, stage, context)

    # Used to guarantee integration test pass and errors aren't ignored.
    return event_handler_status.SUCCESS


def get_s3_object_metadata(bucket_name: str, object_key: str) -> dict:
    try:
        return s3.get_client().head_object(Bucket=bucket_name, Key=object_key)['Metadata']
    except ClientError as e:
        if e.response['Error']['Message'] == "Not Found":
            raise TerminalErrorException("Object {0} not found in bucket {1}".format(object_key,
                                                                                     bucket_name))
        raise RetryEventException("Unable to head S3 object {0} from bucket {1}||{2}".format(object_key,
                                                                                             bucket_name, e))
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to head S3 object {0} from bucket {1}||{2}".format(object_key,
                                                                                             bucket_name, e))
    except Exception as e:
        raise TerminalErrorException(
            "Failed to do a head object on Bucket: {0} Key: {1} | Exception: {2}".format(bucket_name, object_key, e))


def modify_tracking_item(retry_state: int, metadata: dict, additional_message_attributes: dict) -> None:
    """
    Because we are not promised that a call into our system from a client will produce an object we have our large and
    multipart object tracking records created with a ttl. The Glue Dispatcher is only fired once we have a successful
    upload, which means we need to remove the ttl from the tracking record.
    :param retry_state: The current state of the event being processed
    :param metadata: Metadata associated with the object upload including event-id, object-id, request-time,
    collection-id, and other pieces of important data.
    :param additional_message_attributes: Attributes used to help with retries that store the state of things such as
    the tracking item, if it has been successfully queried
    :return: None
    """
    if retry_state <= RetryState.GET_TRACKING_RECORD:
        tracking_record = get_tracking_item(metadata['object-id'], metadata['collection-id'],
                                            metadata['request-time'])
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.UPDATE_TRACKING_RECORD)
        error_handler.set_message_additional_attribute(TRACKING_RECORD_KEY, tracking_record)
    else:
        tracking_record = additional_message_attributes[TRACKING_RECORD_KEY]

    if retry_state <= RetryState.UPDATE_TRACKING_RECORD:
        update_tracking_item(tracking_record)
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.GET_OBJECT_STORE_RECORD)


def get_tracking_item(object_id: str, collection_id: str, request_time: str) -> dict:
    tracking_id = tracker.generate_tracking_id("object", object_id, collection_id)
    try:
        tracking_record = TrackingTable().get_item({'tracking-id': tracking_id, 'request-time': request_time},
                                                   ConsistentRead=True)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to get item from the Tracking Table||{}".format(e))
    except Exception as e:
        raise TerminalErrorException(
            "Failed to get item from Tracking table due to unknown reasons. "
            "Tracking ID = {0}, Request Time = {1} | Exception = {2}".format(tracking_id, request_time, e))
    if not tracking_record:
        raise TerminalErrorException(
            "No tracking record found for tracking-id {0} request-time: {1}".format(tracking_id, request_time))
    if 'pending-expiration-epoch' not in tracking_record:
        raise TerminalErrorException(
            "Tracking record with tracking-id {0} request-time: {1} has no pending-expiration-epoch".format(tracking_id,
                                                                                                            request_time))

    return tracking_record


def update_tracking_item(tracking_record: dict) -> None:
    tracking_record.pop('pending-expiration-epoch')
    try:
        TrackingTable().update_item(tracking_record,
                                    condition='attribute_exists(PendingExpirationEpoch)')
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to update the Tracking Table||{}".format(e))
    except ConditionError as ce:
        logger.error('ConditionError: {}||Possible duplicate event'.format(ce))
        raise TerminalErrorException(
            "Condition Error while trying to remove PendingExpirationEpoch from Tracking Record: {0}||{1}".format(
                tracking_record, ce))
    except Exception as e:
        raise TerminalErrorException("Failed to update item in the Tracking Table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(tracking_record, e))


def is_oldest_tracking_item(object_id: str, collection_id: str, event_id: str) -> None:
    """
    Ensures the incoming event is the oldest in the Tracking table to be processed.
    Warns if there are no tracking items (let EventHandler determine what to do).
    Throws a RetryEventException if not for later processing.
    Note: This logic is similar to the tracker.track_event_handler() decorator.
    :param object_id:
    :param collection_id:
    :param event_id:
    """
    tracking_id = tracker.generate_tracking_id("object", object_id, collection_id)
    try:
        oldest_tracking_item = tracker._get_oldest_item_by_tracking_id(tracking_id)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to query items in Tracking Table||{}".format(e))
    except Exception as e:  # should only be a TerminalErrorException
        raise e

    if not oldest_tracking_item:
        logger.error("Tracking record not found for event {}".format(event_id))
        raise TerminalErrorException("No tracking record found for {0}".format(tracking_id))
    elif not tracker._is_oldest_event(oldest_tracking_item['event-id'], event_id):
        raise RetryEventException("Oldest Tracking item {0} is for another request: {1}||"
                                  "Retrying...".format(tracking_id, oldest_tracking_item['event-id']))


def get_object_store_item(object_id: str, collection_id: str) -> dict:
    try:
        return ObjectStoreTable().get_item({"object-id": object_id,
                                            "collection-id": collection_id},
                                           ConsistentRead=True)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to get item from ObjectStore Table||{}".format(e))
    except Exception as ex:
        raise TerminalErrorException("Failed to get item from ObjectStore Table||{}".format(ex))


def dispatch_upload(retry_state: int, metadata: dict, staging_bucket_name: str, staging_object_key: str,
                    object_store_item: dict, content_length: int) -> tuple:
    object_buckets = object_common.generate_object_store_buckets(DATALAKE_BUCKET_NAME)
    bucket_list = [object_buckets[bucket] for bucket in object_buckets]
    completed_object_metadata = {}
    if content_length <= MAX_OBJECT_SIZE:
        logger.debug('content-length {0} below max size {1}'.format(content_length, MAX_OBJECT_SIZE))
        completed_object_metadata = move_object_in_lambda(metadata, staging_bucket_name, staging_object_key,
                                                          object_store_item, bucket_list)
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY,
                                                       RetryState.SEND_COMPLETED_COPY_NOTIFICATION)
        error_handler.set_message_additional_attribute(COMPLETED_COPY_METADATA_KEY, completed_object_metadata)

    else:
        logger.debug('content-length {0} above max size {1}'.format(content_length, MAX_OBJECT_SIZE))
        start_glue_job(bucket_list, staging_bucket_name, staging_object_key, object_store_item)
        error_handler.set_message_additional_attribute(MESSAGE_RETRY_STATE_KEY, RetryState.DONE)
        retry_state = RetryState.DONE

    return retry_state, completed_object_metadata


def move_object_in_lambda(metadata: dict, staging_bucket_name: str, staging_object_key: str, object_store_item: dict,
                          destination_buckets: list) -> dict:
    try:
        resp = s3.get_client().get_object(Bucket=staging_bucket_name, Key=staging_object_key)

    except ClientError as e:
        if e.response['Error']['Message'] == "Not Found":
            raise TerminalErrorException("Object {0} not found in bucket {1}".format(staging_object_key,
                                                                                     staging_bucket_name))
        raise RetryEventException("Unable to get S3 object {0} from bucket {1}||{2}".format(staging_object_key,
                                                                                            staging_bucket_name, e))

    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to get S3 object {0} from bucket {1}||{2}".format(staging_object_key,
                                                                                            staging_bucket_name, e))
    except Exception as ex:
        raise TerminalErrorException('Error getting staging object: Bucket={}, Key={}, Exception={}'
                                     .format(staging_bucket_name, staging_object_key, ex))
    body = resp['Body']  # body is a StreamingBody object
    metadata['content-type'] = resp['ContentType']
    metadata['raw-content-length'] = resp['ContentLength']
    metadata['staging-key'] = staging_object_key
    metadata['staging-bucket'] = staging_bucket_name
    metadata['old-object-versions-to-keep'] = int(metadata['old-object-versions-to-keep'])
    if 'pending-expiration-epoch' in metadata:
        metadata['pending-expiration-epoch'] = int(metadata['pending-expiration-epoch'])
    if 'changeset-expiration-epoch' in metadata:
        metadata['changeset-expiration-epoch'] = int(metadata['changeset-expiration-epoch'])

    metadata['version-number'] = object_store_item.get('version-number', 0) + 1

    # Must keep content_sha1, object_hash and object_key calculations in sync
    # with object_common.build_object_key_and_hashes()
    # content_sha1 = SHA1 of body
    sha1 = hashlib.sha1()
    block_size = 4096
    try:
        logger.debug('Beginning to build sha1 hash for object')
        for chunk in body.iter_chunks(block_size):
            sha1.update(chunk)
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Unable to read S3 object {0}||{1}".format(staging_object_key, e))

    logger.debug('Completed building sha1 hash for object')

    metadata['content-sha1'] = sha1.hexdigest()

    # object_hash = SHA1 of body + content_type + metadata (user)
    sha1.update(metadata['content-type'].encode())
    if 'object-metadata' in metadata:
        # object-metadata is a json formatted string (see object_command.create_multipart_upload())
        sha1.update(metadata['object-metadata'].encode())
    metadata['object-hash'] = sha1.hexdigest()

    # object_key = collection_hash + "/" + SHA1 of body + content_type + user_metadata + object_id + version_timestamp
    sha1.update(metadata['object-id'].encode())
    sha1.update(metadata['request-time'].encode())
    metadata['object-key'] = "{0}/{1}".format(metadata['collection-hash'], sha1.hexdigest())

    metadata['is-new-object'] = not object_common.object_contents_match(metadata['object-key'],
                                                                        metadata['object-hash'],
                                                                        metadata['content-sha1'],
                                                                        object_store_item)

    copy_source = {'Bucket': staging_bucket_name, 'Key': staging_object_key}

    # convert object metadata from a json formatted string to a dict since this is what is required going forward
    if 'object-metadata' in metadata:
        metadata['object-metadata'] = orjson.loads(metadata['object-metadata'])

    if metadata['is-new-object']:
        try:
            object_event_handler.move_object_to_datalake_bucket(copy_source, destination_buckets,
                                                                metadata['object-key'],
                                                                metadata, metadata['version-number'])
        except error_handler.retryable_exceptions as e:
            logger.info(e)
            raise RetryEventException(
                "Unable to copy S3 object from {0} to {1} {2}".format(copy_source, destination_buckets,
                                                                      metadata['object-key']))
        logger.debug('Successfully moved object to DataLake')
    else:
        logger.debug('Content matches existing record. Did not copy object.')

    move_changeset_object(metadata, object_store_item, copy_source, destination_buckets)

    return metadata


def move_changeset_object(metadata: dict, object_store_item: dict, copy_source: dict,
                          destination_buckets: list) -> None:
    if 'changeset-id' in metadata:
        try:
            changeset_object_key = object_common.generate_changeset_object_key(metadata['changeset-id'],
                                                                               metadata['object-key'] if metadata[
                                                                                   'is-new-object'] else
                                                                               object_store_item['object-key'])

            changeset_object_properties = {
                'collection-id': metadata['collection-id'],
                'object-id': metadata['object-id'],
                'request-time': metadata['request-time'] if metadata[
                    'is-new-object'] else
                object_store_item['version-timestamp'],
                'content-sha1': metadata['content-sha1'] if metadata[
                    'is-new-object'] else
                object_store_item['content-sha1'],
                'content-type': metadata['content-type'],
                'object-metadata': metadata.get('object-metadata', {})
            }
            object_event_handler.move_object_to_datalake_bucket(copy_source, destination_buckets,
                                                                changeset_object_key,
                                                                changeset_object_properties,
                                                                metadata['version-number'] if metadata[
                                                                    'is-new-object'] else
                                                                object_store_item['version-number'])
        except error_handler.retryable_exceptions as e:
            logger.info(e)
            raise RetryEventException(
                "Unable to copy S3 object from {0} to {1} {2}".format(copy_source, destination_buckets,
                                                                      metadata['object-key']))
        logger.debug(
            'Successfully moved object to changeset-id prefix {} in the DataLake'.format(metadata['changeset-id']))


def transform_message_and_publish_to_topic(message: dict, target_arn: str, stage: str) -> None:
    """
    Publishes message to Glue Dispatcher topic
    :param message: publishable string
    :param target_arn: arn for target sns
    :param stage: stage name
    :return: Response dict
    """
    publishable_message = publish_sns_topic.message_to_publishable_json(message)
    try:
        # Add stage to filter so ProcessLargeObject can filter
        filter_dict = {'stage': {"DataType": "String",
                                 "StringValue": stage}}
        response = sns.get_client().publish(TargetArn=target_arn, Message=publishable_message, MessageStructure="json",
                                            MessageAttributes=filter_dict)
        logging.debug(
            "Message {0} with attributes {1} published to {2}".format(publishable_message, filter_dict, target_arn))
        return response
    except error_handler.retryable_exceptions as e:
        raise RetryEventException("Exception thrown while attempting to publish. Retrying event. {0}".format(e))
    except Exception as e:
        raise TerminalErrorException(
            "Failed to publish message: {} to subscription  topic: {} due to unknown reasons. {}".format(
                publishable_message, target_arn, e))


def start_glue_job(bucket_list: list, staging_bucket_name: str, staging_object_key: str,
                   object_store_item: dict) -> None:
    glue_args = {
        'completion-topic-arn': COMPLETION_TOPIC_ARN,
        'object-store-buckets': bucket_list,
        'staging-bucket': staging_bucket_name,
        'staging-object-key': staging_object_key,
        'version-number': object_store_item.get('version-number', 0) + 1
    }
    if object_store_item:
        glue_args['object-state'] = object_store_item['object-state']
        glue_args['object-key'] = object_store_item['object-key']
        glue_args['object-hash'] = object_store_item.get('object-hash', '')
        glue_args['version-timestamp'] = object_store_item['version-timestamp']

    try:
        resp = glue.get_client().start_job_run(
            JobName=GLUE_JOB_NAME,
            Arguments={
                '--event_message': orjson.dumps(glue_args).decode()
            }
        )
    except error_handler.retryable_exceptions as e:
        raise RetryEventException(
            "Exception while starting Large Object Glue Job {}; Exception {}".format(GLUE_JOB_NAME, e))
    except Exception as e:
        raise TerminalErrorException(
            "Exception while starting Large Object Glue Job {}; Exception {}".format(GLUE_JOB_NAME, e))

    logger.info("Glue JobRunId: {0}".format(resp['JobRunId']))


def handle_exceptions(e: Exception, event: dict, stage: str, context: object) -> None:
    if isinstance(e, RetryEventException):
        try:
            # will only raise a TerminalErrorException if we run out of retries
            # Otherwise will raise an unhandled exception leading to the event becoming available again
            process_retry_exception(e, event, stage)
            return
        except TerminalErrorException as e2:
            terminal_error_exception = e2

    else:
        terminal_error_exception = e

    if not isinstance(terminal_error_exception, TerminalErrorException):
        logger.exception(terminal_error_exception)
    else:
        logger.error(terminal_error_exception)

    error_handler.terminal_error(event, is_event=False, error_message=str(terminal_error_exception),
                                 context=context)


def process_retry_exception(e: RetryEventException, event: dict, stage: str) -> None:
    logger.warning(e)
    sqs_url = V1_MULTIPART_QUEUE_URL if stage == 'v1' else LATEST_MULTIPART_QUEUE_URL
    sns_message = sns_extractor.extract_sns_message(event)
    seen_count = error_handler.additional_attributes.get(SEEN_COUNT, 0) + 1
    error_handler.set_message_additional_attribute(SEEN_COUNT, seen_count)
    s3_message = sns_message['Records'][0]
    s3_message['additional-attributes'] = error_handler.additional_attributes
    retry_event = event['Records'][0]['Sns']
    retry_event['Message'] = orjson.dumps(sns_message).decode()
    if seen_count > MAX_RETRY_COUNT:
        raise TerminalErrorException("Event exceeded max retry count of {0}||{1}".format(MAX_RETRY_COUNT, e))
    try:

        # Back off 1 second for each retry
        sqs_delay = min(120 + seen_count, 900)  # 900 seconds is the longest delay supported by SQS
        sqs.get_client().send_message(QueueUrl=sqs_url,
                                      MessageBody=orjson.dumps(retry_event).decode(),
                                      DelaySeconds=sqs_delay)
        logger.info('Successfully placed event on queue {} for retry with seen-count = {}'.format(sqs_url,
                                                                                                  seen_count))
    # If we throw any exception here just throw the original Retry exception we received.
    except Exception as inner:
        logger.warning("Error sending SQS message back to queue: {0}||{1}".format(sqs_url, inner))
        raise e


if __name__ == '__main__':
    import json
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    ASSET_GROUP = 'feature-ajb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['DATALAKE_BUCKET_NAME'] = '{}-datalake-object-store-2880440175841'.format(ASSET_GROUP)
    os.environ[
        'GLUE_DISPATCHER_TOPIC_ARN'] = 'arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-GlueLargeObjectProcessor-GlueDispatcherNotificationTopic-1MK4A4S2JL5TE'
    os.environ['GLUE_LARGE_OBJECT_PROCESSOR_JOB_NAME'] = '{}-GlueLargeObjectProcessor'.format(ASSET_GROUP)

    os.environ['OBJECT_STORE_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreTable'.format(ASSET_GROUP)
    os.environ['OBJECT_STORE_VERSION_DYNAMODB_TABLE'] = '{0}-DataLake-ObjectStoreVersionTable'.format(ASSET_GROUP)
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = '{0}-DataLake-CatalogCollectionMappingTable'.format(
        ASSET_GROUP)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{0}-DataLake-TrackingTable'.format(ASSET_GROUP)
    DATALAKE_BUCKET_NAME = '{0}-dl-object-store'.format(ASSET_GROUP)

    COMPLETION_TOPIC_ARN = os.getenv("GLUE_DISPATCHER_TOPIC_ARN")
    datalake_bucket_name = os.getenv("DATALAKE_BUCKET_NAME")
    LATEST_MULTIPART_QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/288044017584/feature-jek-DataLake-GlueDispatcherLambda-LatestMultipartQueue-1BF75UD2CY99X'
    os.environ['S3_TERMINAL_ERRORS_BUCKET_NAME'] = '{}-datalake-lambda-terminal-errors-288044017584'.format(ASSET_GROUP)


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {'Records': [{'messageId': 'f6e1b124-bec1-4295-93f1-c6dccbc1eebc',
                      'receiptHandle': 'AQEBgAsQjOYjND3yX3HGMEHcXf9/TIkpv5BiKYP0fCRz9U03ya6ice7ia7ohJTY+1YVzYutkYc6k3yscMs+8kXdPewfU8IhjJ3Hw7wjNZdyLbWq0ZXlD0+JY+whDjFRNC2BG4XI5dOiz3el+gmLpS3lICvCFIMREsVylcRXOG/3Vt3dbf1oRymoi5mr8XJxixEK4opiQZrq76U+Iuczt9s4WqkatImYOgxJ1VI0alFlo/U3xxX06t9xyrLWJTnx+I+KPPj7LEmvSDNsy0jbzpAfFVUP9A1JqGxLteWji941hW9MvnAPGQTeOil5tmkZB6I3P0AMW4V6qzUNG4w6H0Y/U+ND85eSGzDXeJ1oop3ELXReDCY4jJyklc9hV6RL8j1U2cbCCT581oI/CW3X81cQnn8JDwHQqZGF8gsseQ1SAUtv1OQYru5hUdEIvWgd4VyKW7LfX5/jlrhnUQhLUAQrsvw==',
                      'body': '{"Type":"Notification","MessageId":"eefbe100-0819-5e07-98d3-b4ff892280da","TopicArn":"arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ObjectStagingBucket-LatestLargeObjectNotificationTopic-155HGGW1NQI2V","Subject":"Amazon S3 Notification","Message":"{\\"Records\\":[{\\"eventVersion\\":\\"2.1\\",\\"eventSource\\":\\"aws:s3\\",\\"awsRegion\\":\\"us-east-1\\",\\"eventTime\\":\\"2019-09-21T01:38:03.572Z\\",\\"eventName\\":\\"ObjectCreated:CompleteMultipartUpload\\",\\"userIdentity\\":{\\"principalId\\":\\"Anonymous\\"},\\"requestParameters\\":{\\"sourceIPAddress\\":\\"198.176.81.34\\"},\\"responseElements\\":{\\"x-amz-request-id\\":\\"5A8D2602131BD216\\",\\"x-amz-id-2\\":\\"jVSpM92d8sUy4BrEvJG6TdKALUUMwHT4HA/NcPVf0ttUnbeCSbnzqDd6XjjS7kzSXLSzGhgEtaU=\\"},\\"s3\\":{\\"s3SchemaVersion\\":\\"1.0\\",\\"configurationId\\":\\"7867a25e-bb74-438b-96ff-a58faf57ab03\\",\\"bucket\\":{\\"name\\":\\"feature-ajb-dl-object-staging-288044017584-use1\\",\\"ownerIdentity\\":{\\"principalId\\":\\"A390FFG3NM79BK\\"},\\"arn\\":\\"arn:aws:s3:::feature-jek-dl-object-staging-288044017584-use1\\"},\\"object\\":{\\"key\\":\\"multipart-object/LATEST/a333d1c7-df6a-448b-b934-73de9102f9fe\\",\\"size\\":58861,\\"eTag\\":\\"22a739385949a76837e0908aa25fb99f-1\\",\\"sequencer\\":\\"005D857EE8E3A2DF99\\"},\\"additional-attributes\\":{\\"retry-state\\":40,\\"metadata\\":{\\"object-id\\":\\"smallObject-3\\",\\"event-name\\":\\"Object::UpdatePending\\",\\"collection-hash\\":\\"ee9acd7d5549b8e0a628da3eddcb5fd9815c74ad\\",\\"event-id\\":\\"1708ab33-d00b-431e-893f-52441fe01b7c\\",\\"stage\\":\\"LATEST\\",\\"old-object-versions-to-keep\\":\\"0\\",\\"collection-id\\":\\"JohnK\\",\\"request-time\\":\\"2019-09-21T01:37:38.844Z\\"},\\"tracking-data\\":{\\"tracking-id\\":\\"smallObject-3|JohnK|object\\",\\"request-time\\":\\"2019-09-21T01:37:38.844Z\\",\\"event-id\\":\\"1708ab33-d00b-431e-893f-52441fe01b7c\\",\\"event-name\\":\\"Object::Update\\"},\\"seen-count\\":1}}}]}","Timestamp":"2019-09-21T01:38:03.801Z","SignatureVersion":"1","Signature":"VYf+VoVyF/ihZc5BPAIjlsU40w0XCOSYlC8p6QUZ787xVl9lwKSqbxhiE8/LlUdBXFjhNtnO6D/uXwNIryX3dz6tCpVdlYguJNMW73WUkPebdZFRbD1VzcrIItBdNLW0mt7nX9e3wyA6jF7+fIkZdM15mAgTpoWGpLGeamSvC6vlNf8qvjfVzjubG+ODnpCFBmxUwSpC+1MWsMYt4s4DfNCBggV8ODPsrTRAv1IBKBRNuwY2c6cyNh+gWINEjxqp+AtNykAjmREu5EA+A2mdpVlFjGGarvciEt/rlGam8FrbPRLUkwmUj3i0CsMdC/tOvAQPtacSOI9Dwyo737iZdQ==","SigningCertURL":"https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem","UnsubscribeURL":"https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:feature-jek-DataLake-ObjectStagingBucket-LatestLargeObjectNotificationTopic-155HGGW1NQI2V:9f1ddc1f-c0d2-4daa-84af-a2b645cfc9ad"}',
                      'attributes': {'ApproximateReceiveCount': '1',
                                     'AWSTraceHeader': 'Root=1-5d857eff-c992f17d16fbaaa0f9024447;Parent=32f21e3cba829f27;Sampled=0',
                                     'SentTimestamp': '1569029888801',
                                     'SenderId': 'AROAUGEGC46YFRXBTTRC2:feature-jek-DataLake-GlueDispatcher-LambdaFunction-7AB91FBVJMHL',
                                     'ApproximateFirstReceiveTimestamp': '1569030009801'}, 'messageAttributes': {},
                      'md5OfBody': '9bea7aafa8e4e2d29d93718b4a1c1a94', 'eventSource': 'aws:sqs',
                      'eventSourceARN': 'arn:aws:sqs:us-east-1:288044017584:feature-jek-DataLake-GlueDispatcherLambda-LatestMultipartQueue-1BF75UD2CY99X',
                      'awsRegion': 'us-east-1'}]}

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    mock_context = MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    json_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - GLUE DISPATCHER LAMBDA]")
