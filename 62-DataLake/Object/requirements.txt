aws-xray-sdk==2.4.2
boto3==1.10.32
botocore==1.13.32
certifi==2019.11.28
chardet==3.0.4
docutils==0.15.2
fastjsonschema==2.14.1
future==0.18.2
idna==2.8
jmespath==0.9.4
jsonpickle==1.2
lng-aws-clients==1.1.65
lng-datalake-client==1.1.24
lng-datalake-commands==1.2.10
lng-datalake-commons==1.2.34
lng-datalake-constants==1.2.4
lng-datalake-dal==1.2.36
lng-datalake-testhelper==1.1.15
lng-orjson==2.0.6
python-dateutil==2.8.0
requests==2.22.0
s3transfer==0.2.1
six==1.13.0
urllib3==1.25.7
wrapt==1.11.2
