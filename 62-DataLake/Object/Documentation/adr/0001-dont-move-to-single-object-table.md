# Don't Move to a Single Object Table

Date: 2019-07-17

##Status: Accepted

* Deciders: Mark Seitter, Aaron Belvo


## Context
Decision <b>not</b> to replace the existing ObjectStore and ObjectStoreVersion table with a single Object table. There is no performance/cost gain and population of RDS in the future can still occur using existing tables.


## Decision

Decided to <b>not</b> move to a single ObjectStore table because of the following:

* No performance advantage because we would still need to perform 2 writes to a new Object table in order to populate a sparse index for the latest version of an object.
* Sparse index for latest version of an object would be a GSI which is eventually consistent (could be problematic).
* Can still investigate having multiple ObjectStore tables if collection index remains an issue. Changing List/DescribeObjects and future enhancements to global tables may address this issue.
* Can still populate future RDS using DynamoDB stream from the ObjectStore table (for inserts/deletes) and ObjectStoreVersion table (for deletes).
* Look at eliminating transactions to reduce costs and increase throughput to the ObjectStore and ObjectStoreVersion tables.


## Consequences

Keep current solution of using ObjectStore and ObjectStoreVersion tables.
* Keeps us from moving data from existing ObjectStore and ObjectStoreVersion tables to a new Object table.
* Removes complexity in having to keep track of what table to locate object data during a migration to a new Object table. 
* Allows us to focus our efforts on populating RDS (Aurora) with object data.
* Must remove ObjectTable1 table from all accounts.