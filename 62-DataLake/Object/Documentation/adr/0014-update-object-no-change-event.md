# Emit Object::UpdateNoChange events when duplicate objects are stored in the DataLake; Object::RemoveNoChange events when objects were already delated.

Date: 2019-10-25

## Status:  Accepted

* Deciders: Wormhole Team

## Context
SNS events are emitted when a new object is created or an existing object was modified.  Object::Update events are only produced when the object was actually changed, and not when a duplicate object was put into the DataLake.  This behavior is great for most subscribers, where the update object events have no impact on the state of the system.  

The behavior of suppressing Object Update events on duplicate objects is not desirable if the subscriber is the publisher of the object and the publisher is using the data in the message to keep track of the objects they own in their own database.  This is a common behavior since the object messages have more information in them than the response of the initial Object Update request.  When the subscribing publisher does not get events, the state of the object is ambiguous.  Either the object was a duplicate or the DataLake accepted the object put request but the DataLake either hasn't gotten to the request yet or an error occurred while processing it.

## Decision
Object::UpdateNoChange events will be emitted from the system when an object update was requested, but no action was taken by the DataLake because the object was identical to the previous version.

Object::RemoveNoChange will be emitted from the system when an object delete was requested, but the object was already removed.  This event completes the correction for the object event transparency/ambiguity issue.

Below is an example of a Create Subscription request for a subscriber that listens for unnecessary object updates on the DataLake.  Only schema-versions "v1" and greater will have these events.
```json5
{
  "filter": {
    "event-name": [
      "Object::UpdateNoChange"
    ],
  },
  "endpoint": "https://sqs.us-east-1.amazonaws.com/638746469603/deja-vu-SubscriptionQueue-ABCEASY123",
  "protocol": "sqs",
  "changesets-only": false,
  "subscription-name": "dejavusubscription",
  "description": "Good story, Bro.  Tell it again.",
  "schema-version": [
    "v1"
  ]
}
```

## Consequences
* This feature adds additional eventing, which may increase run costs.
* This feature may agitate existing SNS throttling limitations within AWS as this change will increase SNS traffic. 


## Links

* [Version One Story S-79617](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story%3A890309&RoomContext=TeamRoom%3A26489)