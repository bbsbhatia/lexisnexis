# List Object Relationships API Endpoint
Date: 2019-08-20

## Status:  Accepted
* Deciders: Planet X

## Context
Technical Story: [S-75459](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:837118)
* Datalake clients need to list Object Relationships with following options:
  1) All Object Relationships.
  2) Object Relationships for a given Target Object Version. This will return latest relationship version for each Target Object version.
  3) Object Relationships for a Relationship ID. 
  4) Object Relationships for a Target Object Collection ID. 
  5) Object Relationships for a Relationship ID and Target Object Collection ID.
  
* To be able to query on Relationship ID and Collection ID we need to store these as separate attributes and build 
 indices of those.
* To be consistent we need to store all parts of composite values as seperate attributes.
    

## Decision
* Object Relationship Table needs to be updated so that all parts of composite values are stored as seperate attributes.
* New GSIs on Relationship ID and Collection ID will be added to Object Relationship Table.
* Entity Relationship Diagram for Object Relationship Table will added at path: Core/Documentation/uml/erd/ObjectRelationshipTable.puml
* A List Object Relationships API endpoint will be added to list Object Relationships. URL path will be:
GET /objects/v1/relationship
* This API will have following query parameters
  1) object-id (optional): object id for target object. Is required if collection-id is provided.
  2) collection-id (optional/required): collection id for target object. Is required if object-id is provided.
  2) version-number (optional): version of target object. If object-id and collection-id are provided and there 
     is no object-version input then we will query on the latest version of that object.
  3) relationship-id (optional): relationship id


* Example lambda request and response:  

Lambda Request JSON:
```json
{
	"target-object": {
		"object-id": "target-object-id",
		"collection-id": "target-collection-id",
		"version-number": 1
	},	
	"relationship-id": "relationship-id"	
}
```
Lambda Response JSON:
```json
[{
     "target-object": {
       "object-id": "target-object-id",
       "collection-id": "target-collection-id",
       "version-number": 1
     },
     "related-objects": [{
       "object-id": "related-object-id",
       "collection-id": "related-collection-id",
       "version-number": 1
     }],
     "relationship-id": "relationship-id",
     "relationship-version": 1,
     "object-relationship-state": "state",
     "object-relationship-timestamp": "timestamp"
 },
 {
     "target-object": {
       "object-id": "target-object-id",
       "collection-id": "target-collection-id",
       "version-number": 1
     },
     "related-objects": [{
       "object-id": "related-object-id",
       "collection-id": "related-collection-id",
       "version-number": 2
     }],
     "relationship-id": "relationship-id",
     "relationship-version": 2,
     "object-relationship-state": "state",
     "object-relationship-timestamp": "timestamp"
 },
 {
      "target-object": {
        "object-id": "target-object-id",
        "collection-id": "target-collection-id",
        "version-number": 1
      },
      "related-objects": [{
        "object-id": "related-object-id",
        "collection-id": "related-collection-id",
        "version-number": 3
      }],
      "relationship-id": "relationship-id",
      "relationship-version": 3,
      "object-relationship-state": "state",
      "object-relationship-timestamp": "timestamp"
  }]

```
## Consequences
* New API for Listing Object Relationships will be created.

## Links
* [Version One Story S-75459](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:837118)
* [ERD for Object Relationship Table - TBA]()
