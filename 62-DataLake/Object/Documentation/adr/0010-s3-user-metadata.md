# Support for S3 User Metadata

Date: 2019-09-18

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Add support for user object metadata when creating or updating an individual object (not objects part of an 
ingestion). User metadata will part of the create/update object subscription messages, stored as user defined S3
metadata on the S3 object and will be returned from the DescribeObject API. User object metadata will also be used in 
the object hash calculation which is used to determine object uniqueness.

## Decision
1. A limited amount of user metadata can be optionally passed to the the following APIs:
   * CreateObject (normal objects only)
   * UpdateObject (normal objects only)
   * CreateMultipartObject
   * UpdateMultipartObject

2. The following APIs will not be supported initially:
   * Create/UpdateFolderUpload

     Folder objects will not be supported at this time because folders are just S3 prefixes and don't have S3 metadata.
    We plan on having a future story to support storing object metadata for folder objects in the 
    ObjectStore/ObjectStoreVersion tables.
    
   * CreateIngestion

     Ingestion will not be supported due to the additional complexities of storing metadata within the zip file.

3. The CreateObject and UpdateObject APIs will not support large objects (PUT/POST /objects/{objectId}/large) since
these APIs are deprecated and will be removed in the near future.

4. New optional headers will be used for specifying object metadata to the above 4 APIs that support metadata. These
headers must start with "x-dl-meta-". Below is an example:
   ```text
   x-dl-meta-name1: value1
   x-dl-meta-name2: value2
   ```
   
    Restrictions on "x-dl-meta-" headers:
    * Values can only be strings (not integers, booleans, lists or objects/dictionaries) since headers can only be
    strings.
    * Cannot use "dl-" prefix for any metadata key since this is reserved for DataLake metadata. For example,
    x-dl-meta-dl-name would be invalid.
    * Limited to 1024 bytes which includes keys and values. Amazon restricts total user defined S3 metadata to 2048
      bytes but we are reserving half of this for DataLake metadata.

    The VM templates for the supported APIs will parse the headers that begin with "x-dl-meta-", strip the prefix and
    pass the metadata to the command lambdas using a new optional "object-metadata" property. The command lambda request
    schemas will have a new optional "object-metadata" property which will require metadata properties to only have
    string values:
    ```json5
    {
       "object-metadata": {
        "type": "object",
        "additionalProperties": { "type": "string" }
      }
    } 
    ```
   
    Below is an example:
    ```json5
    {
      "object-metadata": {
         "name1": "1",
         "name2": "text"
      }
    }
    ```

5. Object metadata will be available to clients in the following ways:
   * Subscription Publish and Republish messages for Object::Create and Object::Update (v1 schema only). Filtering on
   object metadata will not be supported at this time. We plan on having a future story to support filtering on object
   metadata.
   * DescribeObjects API.
   * Accessing user defined metadata on the S3 objects directly.

## Consequences

Object metadata will be used in the object hash calculation performed by the Create/UpdateObjectCommand lambdas and the
GlueLargeObjectProcessor.

Object metadata will be added to the EventStoreTable item by Create/UpdateObjectCommand and ProcessLargeObject lambdas.
 
The Create/UpdateObjectEventHandler lambdas will read object metadata from the EventStoreTable and add to the
ObjectStore/ObjectStoreVersion tables and set S3 metadata on normal objects copied to the S3 object store bucket.

Object metadata must be available to the DescribeObjects lambda and RepublishEventsProcessor glue script which will
require object metadata to be stored in the ObjectStore/ObjectStoreVersion tables in addition to the S3 object. 

New "object-metadata" header will be used to pass a base64 encoded string for object metadata on a
Create/UpdateMultipartUpload request. This header will be read by the GlueLargeObjectProcessor to set the user
defined metadata on the S3 objects as they are copied to the object store bucket.

The below 2 publish schemas will need to be modified to include object metadata:
* v1/create-object.json
* v1/update-object.json

## Links

* [Version One Story S-76842](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story%3A852412)