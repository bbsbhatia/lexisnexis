
# Add ChangesetObjectTable


Date: 2019-09-24


## Status:  accepted 

* Deciders: Wormhole-PlanetX

## Context
Technical Story: [Changeset: Design ChangesetObjectTable](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:894834) <!-- optional -->

To prevent the complexity of minor versioning of Objects within the ObjectStoreVersionTable as
would be a requirement for Objects with identical content whom participate in multiple Changesets we have moved to use 
a separate table called the ChangesetObjectTable. This allows us to offload all Objects participating in a 
Changeset and removes the concept of minor versioning in the ObjectStoreVersionTable.


Concerns with minor versions

* Complexity of Event Handlers
* Republishing Collections or Catalogs (not Changesets) would require skipping over minor versions
* Adds complexity for Object deletion
* Describing Objects and presenting versions (future requirement) would require similar filtering logic

Options
* Deal with it, manage complexity in Event Handlers and Republish.
* Offload Changeset Objects to a separate table.

## Decision
Chosen Option: 2

## Consequences

* Good, because allows the logic in the Event Handlers to only interact with Changesets when they're present in the current message, leading to simplified old-object-versions-to-keep interactions.
* Good, because allows Republish of Collections and Catalogs to be simplified
* Good, because the change does not negatively effect Republish of Changesets
* Good, reduce the amount of throughput on the ObjectStoreVersionTable
* Good, allows us to listen to inserts to the ChangesetObjectTable to add Collections to the ChangesetObjectTable
* Bad, increases the number of streams the ObjectDeletionListenerLambda must listen to
* Bad, increases the complexity of the ObjectDeletionListenerLambda
* Bad, a new table and cost to keep it provisioned

### Changeset Object Table
* Table Key
    * Hash: composite-key: str
    * Range: changeset-id: str
* ChangesetIDAndCollectionHashIndex
    * Hash: changeset-id: str
    * Range: collection-hash: str
* Attributes
	* composite-key: str -- object-id|collection-id|version-number
	* object-id: str
	* collection-id: str
	* bucket-name: str
	* object-key: str
	* content-type: str
	* version-number: int
	* version-timestamp: str
	* raw-content-length: int
	* event-id: str
	* pending-expiration-epoch: int
	* replicated-buckets: list
	* object-hash: str
	* content-sha1: str
	* object-metadata: dict
	* changeset-id: str
	* collection-hash: str

* DAL: new method to generate composite-key: -- object-id|collection-id|version-number

## Links
* Supersedes [0007-change-set](0007-change-set.md)
* V1 Story: [Changeset: Design ChangesetObjectTable](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:894834)