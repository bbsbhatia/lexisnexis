
# Split EventStore table for backend/internal processes


Date: 2019-07-23

## Status:  Accepted

* Deciders: Mark Seitter, Aaron Belvo

## Context
Currently all front-end commands and backend processes (process ingestion zip object/delete objects batch) use the 
same EventStore table for all requests.  The backend processes are causing performance issues on our front-end customers 
because of the high write to the same partitions.



## Decision

Decision is to create a new EventStoreBackendTable that mimics the existing EventStore table and point the process 
ingestion zip object and delete objects batch to this new table.  Existing EventDispatcher Lambda will be off both tables
streams.

## Consequences

We now have two EventStoreTables to look at when debugging issues depending on where the problem arose.