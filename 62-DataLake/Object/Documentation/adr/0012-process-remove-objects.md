
# Modify Process Remove Objects Event Handler


Date: 2019-10-04


## Status:  accepted 

* Deciders: Wormhole-PlanetX

## Context
Technical Story: [Batch Remove Objects -Refactor](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:858252) <!-- optional -->

The previous Remove Objects Event Handler was implemented by utilizing batch_write for writing records to the Tracking table and Event Store Backend table.

To prevent overwriting records in the Event Store Backend table due to lack of conditional batch_write, the original implementation needed to be modified. 

## Decision
Decision was made to utilize put_item in a multi-threaded manner to write to the Tracking table and Event Store Backend table.

Based on several test invocations of the Process Remove Objects Event Handler (with 20,000 object IDs as input), it was determined that a MAX_WORKERS value of 2 was ideal (based on the table below). 

| Number of Threads    | Billed Time (ms)    | Memory Used (MB)|
| :------------------- | :------------------ |:--------------- |
| **2 Threads**        | **256,000**         | **108**         |
| 5 Threads            | 272,500             | 111             |
| 10 Threads           | 279,100             | 114             |

A lambda size of 1024 MB was used for the above test runs.
Smaller (832 MB) and larger (2048 & 3008 MB) lambdas were also tested, but did not prove to be cost effective.  This effort was done to "right size" the required lambda. 
## Consequences
 
* Good, because this prevents the overwriting of records to the Event Store Backend table
* Good, because previously, on Retries, an overwrite to the Event Store Backend table would invoke a duplicate event to Event Dispatacher
* Bad, because put_item is slower than batch_write, but necessary for conditional writes to dynamodb
## Links

* V1 Story: [Batch Remove Objects -Refactor](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:858252)