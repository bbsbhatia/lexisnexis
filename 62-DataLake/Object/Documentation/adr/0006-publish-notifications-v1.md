
# Publish messages with schema version v1


Date: 2019-08-09

## Status:  Accepted

* Deciders:  Wormhole Team

## Context
Technical Story: [S-71080](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:780243)

[ADR lng-datalake-commons - Notification schema validation](../../../lng_datalake_commons/Documentation/adr/001-validate-notification-schema.md)
* Include more information in the published messages for Object events, as well as rename some properties and remove duplicated information.

## Decision

* Create the new schemas (v1) for the events: Object::Create, Object::Update and Object::Remove.
* Start publishing messages with schemas version v1, but keep sending notifications with schemas v0 and a message including both v0 and v1.
* Once all the clients migrate to use the new schema v1, stop sending messages with version v0.

## Consequences

* A 200% cost increase in the SNS Requests to DataLakeEvents SNS topic, because two additional messages are published until all clients migrate to the 
new schema version. 

## Links
* [Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:780243)

[ADR lng-datalake-commons - Notification schema validation](../../../lng_datalake_commons/Documentation/adr/001-validate-notification-schema.md)