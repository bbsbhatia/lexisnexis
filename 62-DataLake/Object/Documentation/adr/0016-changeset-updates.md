
# Changeset Refinement


Date: 2019-11-18

## Status:  Accepted 

* Deciders: Planet-X

## Context
Subscribers will need to be able to get objects participating in a changeset even if the collection attributes that an 
object exists in would normally expire or remove the version.

Required updates:

1. Changesets will have their own prefix (changeset-id) in the object bucket. 
    * Instead of trying to manage the expiration of the longest living object TTL (whether it is the normal object or 
    the changeset tied object) we will place all objects participating in a changeset into its own prefix
    * This allows us to expire or remove object versions from the OSVT and the collection-hash prefix in s3 without
    interfering with the object that participated in the changeset
    
1. Get and Describe Object Commands will Accept changeset-id as a Query Parameter
    * Normal semantics apply to a non-existent object when the changeset-id is not included
    * If the changeset-id is included we will do the look for the object in the ChangesetObjectTable
    * GET:
        * If changeset-id is included version-number and collection-id must also be included
    * DESCRIBE
        * If changeset-id and object-id are included the caller must also provide collection-id and version-number
        * If changeset-id and version-number are included the caller must also provide collection-id and object-id
        * If changeset-id and collection-id are included the request may be fulfilled without other parameters
        * If changeset-id is included the request may be fulfilled without other parameters    
1. Update the Publish Schemas:

    * Existing semantics for "Object::UpdateNoChange" and "Object::RemoveNoChange" will continue however if an object 
        participates in a changeset the new changeset-id, s3 location, and object-key-url will be included
    * Objects participating in a changeset will now have a changeset-object in their publish message:
    ```json5
    {
      "v1": {
        "SchemaVersion": "v1",
        "Schema": "https://datalake.content.aws.lexis.com/schemas/v1/update-object.json",
        "Type": "Publish",
        "context": {
          "request-time": "2018-04-09T08:51:10.910488",
          "request-id": "GYtjQjLwAqWKtWem"
        },
        "object": {
          "object-id": "f4f76a67a918839d4576072376fb7ef6644210d3",
          "collection-id": "274",
          "object-state": "Created",
          "content-type": "text/plain",
          "raw-content-length": 62,
          "version-number": 2,
          "version-timestamp": "2018-04-09T08:51:10.910488",
          "event-name": "Object::Update",
          "S3": {
            "Key": "<collection-hash>/f4f76a67a918839d4576072376fb7ef6644210d3",
            "Bucket": "mock_datalake_bucket-use1"
          },
          "object-key-url": "https://datalake_url.com/objects/store/<collection-hash>/f4f76a67a918839d4576072376fb7ef6644210d3",
          "replicated-buckets": [
            "mock_datalake_bucket-usw2"
          ]
        },
        "changeset-object": {
          "changeset-id": "foobar",
          "S3": {
            "Key": "<changeset-id>/<collection-hash>/f4f76a67a918839d4576072376fb7ef6644210d3",
            "Bucket": "mock_datalake_bucket-use1"
          },
          "object-key-url": "https://datalake_url.com/objects/store/<changeset-id>/<collection-hash>/f4f76a67a918839d4576072376fb7ef6644210d3",
          "replicated-buckets": [
            "mock_datalake_bucket-usw2"
          ]
        }
      }
    }
    ```
    
1. Object Expiration Listener Must Listen to the Object Store Version Table and the Changeset Object Table
    * 2 streams but a single lambda to delete from these locations, there is no overlap so they can be handled 
    independently 

## Consequences

* Good, this keeps our objects in separate locations so we can handle their TTL independently
* Good, allows the client to have a single prefix to point towards when ingesting a changeset through methods that 
allow you to specify prefix
* Bad, we have to keep 4 copies of an object, one in the collection-hash prefix in us-east-1, one in us-west-2, 
and a second set in the changeset-id prefix in both regions
