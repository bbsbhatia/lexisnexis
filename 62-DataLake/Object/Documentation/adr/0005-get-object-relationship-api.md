# Get Object Relationship API Endpoint
Date: 2019-08-01

## Status:  Accepted
* Deciders: Planet X

## Context
Technical Story: [S-73148](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:805957)
* Datalake clients need to get Object Relationship info based on a Target Object and Relationship ID
* Get Object Relationship is also needed to perform regression tests on Object Relationship APIs

## Decision
* A GET Object Relationship API endpoint will be added to get Object Relationship info:
* URL path will:
GET /object/v1/{objectId}/relationship/{relationshipId}
* This API will have 2 path parameters
  1) objectId: object id for target object
  2) relationshipId: the relationship ID
* This API will have 3 query parameters
  1) collection-id (required): collection id for target object
  2) object-version (required): object version for target object
  3) relationship-version (optional): relationship version. If this is not provided then info on 
  latest relationship version will be returned

* Example lambda request and API response:  

Lambda Request JSON:
```json
{
	"target-object": {
		"object-id": "target-object-id",
		"collection-id": "target-collection-id",
		"version-number": 1
	},	
	"relationship-id": "relationship-id",
	"relationship-version": 1
}
```
200 API Response:
```json
{
    "target-object": {
      "object-id": "target-object-id",
      "collection-id": "target-collection-id",
      "version-number": 1
    },
    "related-object": {
      "object-id": "related-object-id",
      "collection-id": "related-collection-id",
      "version-number": 1
    },
    "relationship-id": "relationship-id",
    "relationship-version": 1,
    "object-relationship-state": "state",
    "object-relationship-timestamp": "timestamp"
}
```

## Consequences
* New GET API for Object Relationships will be created.

## Links
* [Version One Story S-73148](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:805957)
