# Eliminate Transactions on Object Tables

Date: 2019-07-19

## Status: Accepted


* Deciders: Mark Seitter, Aaron Belvo

## Context
Eliminate DynamoDB transactions to the ObjectStore/ObjectStoreVersion tables in the CreateObject, UpdateObject and 
RemoveObject EventHandler lambdas.

* Reduce costs (DynamoDB transactions use 2x read/write capacity).
* Increase performance (less throttling when writing to the ObjectStore table and indexes).

## Decision

Decided to eliminate DynamoDB transactions when inserting and deleting from the ObjectStore and ObjectStoreVersion tables.
We will use additional attributes on the SQS message that is written to the RetryQueue to keep track of state to prevent
duplicate DynamoDB operations in the case of failures that are retried.

## Consequences

Transactions will be removed from CreateObject, UpdateObject and RemoveObject EventHandlers.

Retry failures on CreateObject, UpdateObject and RemoveObject EventHandlers must be managed properly with the use of the
"additional-attributes" attribute that has been used previously by other EventHandlers..

Example SQS retry message:
```
{
    "ingestion-id": "3840a796-aa69-11e9-b3d0-fb501f1f9e4b",
    "old-object-versions-to-keep": 2,
    "content-type": "application/xml",
    "object-id": "8MSP-Y7P2-D6PH-W54T-00000-00",
    "collection-id": "FPD_182980",
    "object-key": "ee43c12351da780b2ca6a29f41d9cd34e9590c37",
    "event-id": "3840a796-aa69-11e9-b3d0-fb501f1f9e4b-0004047",
    "stage": "v1",
    "request-time": "2019-07-19T21:07:30.356Z",
    "event-name": "Object::Update",
    "event-version": 1,
    "raw-content-length": 51313,
    "seen-count": 1,
    "additional-attributes": "osvt-created"
}
```
