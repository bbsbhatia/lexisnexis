
# Change Sets


Date: 2019-09-04

## Status:  Superseded by [0011-change-set](0011-changeset-update.md)

* Deciders: Wormhole Team

## Context
DataLake publishers and subscribers need a way to identify sets of related documents. Typically,
documents in a set reference each other so in order to maintain referential integrity a way is needed
for publishers to identify documents in a set as they are added/updated/deleted in the DataLake. 
Subscribers need to be informed when a set of data has been published and a way to retrieve the
set as a whole. 

Currently, there are two types of document sets:

* TOC - Table of Contents
    * A set of documents that are referenced by a Table of Contents.
    * Documents in the set can also refer back to the TOC 
* Data Set
    * Documents that are related by some other relationship:
        * Year or month of publication
        * Published together as a bound volume or CD/DVD

There are two options available for supporting document sets in the DataLake:

1. Do nothing - Let the publishers and subscribers work out a mechanism to publish sets of data.
FPD currently publishes sets into the PSH using a "sync" partition. For each set of data FPD
creates a manifest of all the documents in the set. This manifest contains the specific location
of each document in the PSH. Once the set of data is complete FPD writes the manifest to the sync
partition. Subscribers consume manifests from the sync partition as they are published. This same
methodology can be used in the DataLake using a collection to hold sync manifests. Subscribers
then subscribe to the sync collection in order to receive create events for any new manifests.
Subscribers then process the manifest retrieving version specific objects from the DataLake that
make up the set of data.
 
2. Add Change Sets to the DataLake - Change Sets will allow a publisher to open or create a
Change Set before publication starts and receive a Change Set ID (CSID). This CSID must then be
included in any document changes (create/update/delete) made to the set of data in the DataLake.
Once complete, the publisher closes the Change Set. This CSID will be included in any document level
events sent out to subscribers. In addition, a Change Set Closed event will be sent so that
subscribers will know when a change set has completed. Subscribers can process these events in
order to consume the change set. Subscribers will also have the option to list and republish objects
using the CSID. Information about a given Change Set can be retrieved for up to 30 days after it is
created.

## Decision

1. Chosen option: "2", since a standardized mechanism is needed across all publishers and subscribers 
to support Change Sets in the DataLake.

2. Change Set Event Ordering
    * It is the publisher's responsibility to ensure change sets are opened/closed and objects are
    published in the proper order especially if two or more open change sets touch the same
    object.

3. New Change Set Events
    * New events for Close Change Set and Open Change Set
    * The close Change Set event will include a list of all the collections that took place in the
     Change Set as a message attribute.
    * The Collection ID message attribute will need to be changed from a string to an array of strings.
    * SNS message size limit of 256K will limit the number of collections/catalogs to 
    about 6,000. collection and catalog ID are limited to 40 characters making 6,000 IDs ~234K.
    * Options
        * Multiple change set closed events
            * Upside: All subscribers will get a Object::ChangeSetClosed event that meets
            their filter policy
            * Downside: Some subscribers may get duplicate events
        * Single change set closed event: Chosen
            * Upside: Single notification, less complexity
            * Upside: It is very unlikely there will be more than 6k collections/catalogs participating in an change set
             could easily have this posted in documentation for Subscribers/Publishers
        
4. Change Set resources will be added to the Object API
    * Open - POST: /objects/changeset
    * Close - POST: /objects/changeset/{changeseId}
    * Get - GET: /objects/changeset/{changesetId}
    * List - Get: /objects/changeset (owner-id)
    * Describe - Get: /changeset/describe (changeset-id, owner-id, state)
        * May be needed to list all the collection-ids that make up a change set
        * Do we list collections if change set is still open?
 
5. Change set closed event and object eventual consistency

    Because we must have all object events related to a change set processed before we can
    confidently close the change set special care must be taken to track objects 
    related to the change set while in-flight.
    * When a publisher closes a change set DataLake must wait for all object operations in the change
    set to complete before sending the change set closed event
    * DataLake will delay sending the change set closed event until all objects in the change set
    have been processed 
    
    Options:
    * Change Set Tracking Table
        * Use CSID as Hash, EventID as Sort
        * Upside: Can use consistent read to determine if objects are in flight for a CSID
        * Downside: We have have a third tracking table
        * Opt - Command would add, EH would remove using sqs with lambda listening to remove from CSTT 
        and add to Change Set Collection Table
        * Opt - Use streams off of the Tracking Table and Collection Blocker Table to add and delete entries of the 
        Change Set Tracking Table.
    * Fifo SQS
        * Downside: Would need one for each CS
    * SQS Que 
    	* Downside: one for each CS
    * Kinesis
        * Upside: Can query
    	* Downside: Still have to store the data somewhere for it to be useful
    * GSI on Tracking Table: Chosen
    	* Upside: Using existing table
    	* Downside: Eventually consistent, could lead to ChangeSetClosed event 
    	firing before all objects in the change set have been processed. This is unlikely based on [AWS description of propagation of data to the GSI](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GSI.html#GSI.Writes).
    * Counter on Change Set Collection Table
        * Increment for each object created in the change set on command side
        * Decrement for each object processed in the change set
        * If counter > 0 when Object::ChangeSetClosed event fired retry.
            * If counter still > 0 query the ChangeSetID GSI on Tracking and do a count
        * Downside: Counters in DynamoDB are inconsistent and can cause issues
        * Upside: Not another table, uses retry logic to try and account for eventually consistent GSI
    
6. Existing Object API Modifications
    * All object operations will need to accept CSID as a query parameter
        * Create
        * Update
        * Delete
        * Multipart
        * Folder
        * Ingestion
        * Batch Delete
    * Change Set ID will need to be included in all object events where the object is part of a change
    set.
    * Any objects that are a part of a change set must have a TTL greater than 30 days
    so we can honor the change set TTL.
    * Objects with old-object-versions-to-keep will have to be given a TTL instead of deleted
    from the version table so we can honor the change set TTL.
    
7. Existing Subscription API Modifications
    * Clients will need a way to subscribe to CS only events
    * Clients will need a way to identify Change Sets in event messages
    * Sample Create Subscription Request, with change-sets-only. Default value = false
    ```json
     {
      "protocol": "sqs",
      "schema-version": ["v0", "v1"],
      "endpoint": "some-sqs-arn",
      "change-sets-only": true,
      "filter": {
       "event-name": ["Object::Create", "Object::Remove", "Object::ChangeSetClosed"],
       "catalog-id": ["Cat1"]
      } 
     }
    ```
    * Sample filter policy for change sets only:
    ```json
    {
     "notification-type": ["{SubscriptionID}", "Event"],
     "event-name": ["Object::Create", "Object::Remove",
                    "Object::ChangeSetClosed"],
     "catalog-id": ["Cat1"],
     "schema-version": "v0v1",
     "change-set-id": [{"exists": true}]
    }
    ```
    * Sample filter attributes event: Object::ChangeSetClosed
    ```json
    {
     "notification-type": "Event",
     "event-name": "Object::ChangeSetClosed",
     "catalog-id": ["Cat1"],
     "collection-id": ["Coll1", "Coll2", "Coll3"],
     "schema-version": "v0v1",
     "change-set-id": "123456789"
    }
    ``` 
    * Sample filter attributes event: Object::Create
    ```json
    {
     "notification-type": "Event",
     "event-name": "Object::Create",
     "catalog-id": ["Cat1"],
     "schema-version": "v0v1",
     "collection-id": "Coll1"
    }
    ``` 
    * Sample filter attributes republish: Object::Create
    ```json
    {
     "notification-type": "{SubscriptionID}",
     "event-name": "Object::Create",
     "catalog-id": ["Cat1"],
     "collection-id": "Coll1",
     "change-set-id": "123456789"
    }
    ``` 
    * Messages with the filter attribute change-set-id will still be delivered to subscribers 
        who do not have a change-set-id property in their filter policy.

8. OSVT Changes
    * ChangeSetIDAndCollectionHashIndex
        * Hash = CSID, Range = CollectionHash
        * This allows us to split change set Republishes into multiple collections 
        to distribute across workers by querying with these two values.
    * Minor-versions: 1.0, 1.1, 1.2
        * Identical events with different CSIDs
    * Major versions: 1.0, 2.0, 3.0...
        * Objects with different content
    * Republish:
        * All versions associated with the CISD will be republished
        * Only major version will be exposed to client
    * Example multiple updates: same object ID
        * Same content and CSID
            * No Op
        * Different content and same CSID
            * New major version
        * Same content and new CSID
            * New minor version
        * Different content and different CSID
            * New major version
            
9. Change Set Table
    * Hash = CSID
    * A single row for each change set
    * Should be open for no longer than 30 days and available for historical use for 1 year
    * Enforcing 30 day open change set options:
        * Listener for the change set table: Chosen 
            * After TTL of 30 days listener puts back into table with state expired
            * Sets TTL for 365 days
            * Downside: Very small exposure while listener processed TTL event
        * Expiration Date - set to 30 days - command lambda checks
            * TTL always set to 365
            * What sets state to expired? Command lambda could do this - not ideal
    
10. Change Set Collection Table
    * Hash = CSID, Range = CollectionID
    * Allows the us to store the collections that participated in a change set
    * Allows us to distribute collections within a change set across multiple nodes during republish
    * Allows us to publish Object::ChangeSetClosed event without relying on GSI
    

## Consequences

1. Change Sets
    * Good, because not all publishers are as sophisticated or capable as FPD
    * Good, because publishers and subscribers will not have to process manifest files
    * Good, because a "sync" collection will not have to be identified and maintained for a set of data 
    * Bad, because this will require significant changes to DataLake to support his feature
2. Change Set Event Ordering
    * Good, because DataLake does not have to check for overlap between change sets when they close
    * Good, because publishers will have more flexibility in how they manage change sets
    * Bad, because publishers will need to manage this complexity themselves if needed.
3. New Change Set Events
    * Good, because subscribers need to know when a change set is opened and closed
    * Bad, because collection IDs will inflating the close event size
    * **Bad, because the message size limit of 256K will limit the number of collections to about 6,000**
    * Only 1 owner (FPD) has more than 500 collections and their largest source contains 575.
    A change set spanning all of that content will not be anywhere close the limit.
4. Change Set resources will be added to the Object API
    * Good, because change sets are strongly tied to object interactions
    * Good, because change sets are not owner specific and should not be part of the Admin API
    * Good, because change sets are not passed collections or catalogs and should not be part of
    Collection API.
    * Good, because adding to the object API reduces development and maintenance time
    * Bad, because it's a higher level operation than objects an could be in a ChangeSet API
    * Bad, because "changeset" can no longer be used as an object ID.
5. Change set closed event and object eventual consistency
    * Good, because change set closed events should only be fired after all the object events have
    completed
    * Bad, because SNS does not guarantee the order that messages are sent so it is still possible for
    a subscriber to receive an object event after the change set closed event 
6. Existing Object API Modifications
    * Good, because this is needed to support change sets
    * Bad, because this is a large change
    * Bad, more complexity in event handlers for TTL
7. Existing Subscription API modifications
    * Good, because this is needed to support change sets
    * Bad, because this is a large change
8. Do Not Use State Machine for Close Change Set
    * Good, because state machines are less expensive than lambda especially when waiting for processes
    to complete.
    * Good, because state machines can run up to a year. Potentially unneeded because change sets are only
    valid for 30 days.
    * Good, because state machines are easy to monitor
    * Good, because it will allow GSIs to update before they are accessed by the state machine
    * Bad, because it takes slightly longer for a state machine to start
    * Bad, because you cannot query a dynamodb table so we would have to use a lambda
9. Object Store Version Table Modifications
    * Good, can support minor versions of objects
    * Good, supports change set republishes
    * Bad, more entries
    * Bad, a costly index
10. New Change Set Table
    * Good, we have data to do what we need to do for change sets
    * Good, time boxing when a change set exists and can be used
11. New Change Set Collection Tables
    * Good, puts all collections relevant to a change set in a single table without having to query another table
    * Good, allows for us to split collections participating in a change set across multiple workers for Republish 

## Links
* [Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:838624)
* [Fabrication Product Deleivery System (FPD)](https://lngit.lexisnexis.com/sites/ProdSysOps/Documentation/Turnover%20Docs/New_Lexis_Fabrication_Product_Delivery_Turnover.doc)
* [Publication Subscription Hub (Pub Sub Hub or PSH)](https://lngdevelopment.lexisnexis.com/sites/SharedServices/InfrastructureServices/Shared%20Documents/Publish%20Subscribe%20Hub/UserDocs/Chuck%20Carter's%20Starter%20Guide%20for%20EBPS%20team%20PubSubHubStarter.docx)