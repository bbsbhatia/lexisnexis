# New SQS Retry Logic for ProcessIngestionZip and ProcessIngestionZipObject

Date: 2019-09-12

## Status:  Accepted

* Deciders: Aaron Belvo & Mark Seitter

## Context
ProcessIngestionZip has been raising RetryEventExceptions to cause SQS to retry the lambda after the visibility timeout
when the ingestion is not the current one to be processed (not current in CollectionBlockerTable). These retries work 
fine as long as the lambda error rate is not too high but there is a point where SQS will start to exponentially backoff
lambda executions due to high error rates. These reties can be more that 12 hours even though the visibility timeout
on the SQS queue is set to 120 seconds (this issue was reported to AWS where they finally concluded this is expected
behaviour even though it is not documents). A similar issue now exists in ProcessIngestionZipObject because it is now
using the TrackingTable for sequencing so it was also raising RetryEventExceptions when the tracking record was not
the current one to be processed.

## Decision
Decided to process exceptions inside the ProcessIngestionZip and ProcessIngestionZipObject lambdas by sending the
message back to the SQS queue and exiting without raising an exception so lambda will remove the original message from
the SQS queue.

Retries will be limited to 1000 by using the "seen-count" attribute in the SQS MessageAttributes. This attribute will
be read from the input event, incremented and sent with new SQS message for each lambda retry. If the seen-count
reaches 1000 then a TerminalErrorException will be thrown.

#####SQS DelaySeconds
Message delay time will be calculated as follows:
* ProcessIngestionZip

  The position of the ingestion in the CollectionBlocker table is used to calculate the message delay. Each position
  will add a 30 second delay in hopes that this will minimize retries.
  
* ProcessIngestionZipObject

  The seen-count will be used to calculate the message delay. A base delay of 120 seconds + the seen-count (1-1000)
  will be used for the message delay. This will hopefully give us ample time to address issues with a Tracking record
  that is blocking the ProcessIngestionZipObject lambda from processing the object.
  
## Consequences
Must monitor for ProcessIngestionZipObject terminal errors since these will happen sooner than they do today due to 
the exponential backoff SQS was performing for lambda for a queue with high error rates.