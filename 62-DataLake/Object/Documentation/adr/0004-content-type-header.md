
# Updates to ContentType Header


Date: 2019-07-26

## Status:  Accepted

* Deciders: Wormhole Team

## Context
When generating the Java SDK Client from the Swagger file, if the Content-Type header is not required for Put/Post requests
that pass a Content-Type (ie Create/Update Object), there is no way for the end user to pass a Content-Type with how swagger
currently generates the client.
Also for the calls that have no body (ingestion, multipart objects), because content-type is not required header, API Gateway
will default to appliation/json and pass the mapping template validation even if passthrough is Never.  



## Decision
For any method that can accept different body types as an input, and has a body the Content-Type header <b>must</b> be required
on the API Method.  For any method that needs a content-type to determine how to story an object later but does not have a body,

## Consequences

In the swagger UI, users will have to type in the content-type rather than pick from the dropdown.