# Return temporary S3 location on Object Put and Object Post calls

Date: 2019-10-25

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Some publishers require immediate access to objects stored in the DataLake.  These publishers do not have the luxury of waiting for the DataLake's native, eventual consistency architecture to process the Object Create or Update request.

## Decision
That staging bucket S3 location will be supplied on the create and update object responses as the "temporary-object-key-url".  The temporary location will be available immediately and will be available for a minimum of four hours after the initial request.  The temporary location will not meet High Availability requirements.  No changes will be made to enforce that the staging location is destroyed in four hours.  Four hours was chosen to allow for a multi-part object to be uploaded (a maximum of three hours) and processed (95% chance of taking less than one hour).  

Below is an example of an updated Create Object response with the new "temporary-object-key-url":
```json5
{
  "context": {
    "request-id": "446d33a0-bc88-408e-92fc-733a3b44bb07",
    "request-time-epoch": "1572025691237",
    "api-id": "01oew1oab0",
    "resource-id": "29xkj0",
    "stage": "v1"
  },
  "object": {
    "object-state": "Created",
    "collection-id": "dataindexheitkadjtest",
    "collection-url": "/collections/v1/dataindexheitkadjtest",
    "object-id": "alterego",
    "object-url": "/objects/v1/alterego?collection-id=dataindexheitkadjtest",
    "temporary-object-key-url": "https://datalake.content.aws.lexis.com/objects/temp/76e8b564ee14d2ff08d622ed6aa32f36d905924e/da0f8a0c8574bf967e6bc1df1ccb270b0a68a6a9",
    "owner-id": 2405,
    "asset-id": 62
  }
}
```

## Consequences
* Publishers could have stored the objects temporarily themselves.
    * This solution would have required ALL calling applications to replicate this behavior.
    * If the calling application had to produce a solution to store the objects themselves, it wouldn't need the DataLake.
* The DataLake could have improved the latency between the initial object request and the publication of the SNS topic for that object.
    * This solution does not alleviate the race condition, it only improves the odds that the object event will arrive before the request for the object times out.
    * This solution will still be much slower than providing the temporary location.
* The Publisher to the DataLake could change its approach to also be eventually consistent.
    * Some applications already have a well-defined semantic, and this is not practical resolution.
* The Publisher to the DataLake could have blocked and/or delayed its "done" state.
    * Delaying the done state does not alleviate the race condition, it only improves the odds that the object event will arrive before the request for the object times out.
    * Blocking the done state until the SNS message is received is problematic in that the SNS message delay time from the initial object creation is currently erratic.  This delay could last for several minutes to an hour or more.
* The DataLake could provide Object PUT/POST interfaces that block until the object is processed.  (This could actually be implemented as a layer on top of all DataLake calls for those clients that require the transactional semantic.)
    * This solution would only need to be implemented once for all RELX applications.
    * This would change the DataLake to a synchronous pattern.
        * Architectural Change
        * Forces a lot of re-work
        * Removes event-sourcing
* The DataLake's Get Object call could be enhanced to pull from the staging area instead of the permanent object location while the object is being processed.
    * This would require clients to use the Get Object interface, which is currently not a desirable Architectural approach.
    * This approach may still needed at a future date.
* Cloudfront 404 intercept could be used to return the temporary file
    * DataLake only sometimes knows where the object will be.
        * Does not know multipart-upload location.

## Links

* [Version One Story S-84647](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story%3A949409&RoomContext=TeamRoom%3A26489)