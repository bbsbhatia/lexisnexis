# Object/Collection Modifications for new S3 Object Store Buckets

Date: 2019-08-14

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Object and Collection changes are required to utilize the new S3 object store buckets that were created
in S-75018 (see link below). These changes involve the following:
* Using the collection hash value for the object key prefix.
* Add new object metadata when creating objects.
* Eliminate de-duping of objects in S3 object store bucket (we will still not create a new version of a document if
the new ObjectHash changes from the current version on an update).
* Return new collection-key-url attribute in ListCollections and GetCollection APIs for collections that have
been fully migrated to the new object store buckets.
* Re-enable the object deletion process for objects that expire so we can keep the new S3 object store bucket clean.

## Decision
Objects will be stored in the new object store bucket with the following naming convention for the <b>ObjectKey</b> to
insure uniqueness across all versions of an ObjectID:

    <collection-hash>/<hash-calculation>

* <collection-hash>: SHA1 hash of collection id (same value that is currently used in the ObjectStore table)
* <hash-calculation>: SHA1 hash of the following:
  * Body   
  * ContentType
  * S3 User Metadata (future)
  * ObjectID
  * VersionTimestamp 

New <b>S3 DataLake Metadata</b> will be added to each object (prefixed with "dl-" so it does not conflict with user
metadata that will be added in the future):
* dl-collection-id = CollectionID (we currently add originalcollectionid which will be eliminated)
* dl-object-id = ObjectID
* dl-content-sha1 = SHA1 hash of body
* dl-version-timestamp = VersionTimestamp
* dl-version-number = VersionNumber (?)

New <b>ObjectHash</b> will be computed and stored in the ObjectStore and ObjectStoreVersion tables. This will be used by
the UpdateObjectEventHandler for determining if a new version of the object needs to be created (we can't use the
object key since this includes the version timestamp in the metadata). The ObjectHash will be a SHA1 hash of the 
following:
* Body
* ContentType
* S3 User Metadata (future)

New <b>ContentSha1</b> will be computed and stored in the ObjectStore and ObjectStoreVersion tables. This will ba a SHA1
of the body only which is the same as the old ObjectKey value. This will be used by the UpdateObjectEventHandler for 
determining if a new version of the object needs to be created for an object that exists in the old object store
buckets by comparing the ContentSha1 to the ObjectKey. Additionally, it may have future benefit by adding this
value to the published events.

## Consequences
Applications that store objects in the object store bucket will need to be modified to use new object store bucket.

New attributes will need to be added to the Collection, ObjectStore, ObjectStoreVersion and EventStore table schemas.

Applications that currently calculate the ObjectKey will need to be modified to calculate ObjectKey using new algorithm
and additionally compute the new ObjectHash and ContentSha1 values.

Modifications will be required to background applications that copy objects directly to the S3 object store so
that we don't leave orphaned S3 objects that are discarded by the UpdateObjectEventHandler when the ObjectHash values
match. The ProcessIngestionZipObject and GlueLargeObjectProcessor applications currently check to see if the ObjectKey
already exists prior to creating the S3 object but will need to be changed to check the ObjectHash value since the new 
ObjectKey will be unique. Below are the 2 primary options that were discussed:
1. Copy the S3 object to the object store but tag it so a new S3 lifecycle rule can delete the object if the tag is not
removed. The UpdateObjectEventHandler would remove the tag if the event was processed as a new version.
   * Many unnecessary S3 writes to object store table.
   * S3 tags can be expensive.
2. Use Tracking to sequence the running of ProcessIngestionZipObject and GlueDispatcher/GlueLargeObjectProcessor so 
these applications will only process events if they are the current event for the object (similar to what is done in 
the Create/Update/RemoveObjectEventHandlers). This allows these applications to know the ObjectHash value of the current 
version of the document (from the ObjectStore table) so that they can compare ObjectHash values and not write the object
to the S3 object store if they are identical.
   * Requires modifications to ProcessIngestionZipObject for Tracking sequencing.
   * Requires an SQS queue to be added in front of the GlueDispatcher and modifications to GlueDispatcher for 
   Tracking sequencing.
   * Will result in some SQS retries on the IngestionObjectQueue which have caused issues in the past on the 
   IngestionNotificationQueue (these retries should be small in comparison but it needs to be monitored).
   * Potentially allows us to write version number to S3 metadata since it will be one more than the current version.
   * Simplifies ProcessIngestionZipObject so that it no longer needs to read Tracking to determine event type.
   * Sequencing could be removed from Create/UpdateObject for ingestion and large objects since it will be done already.

We decided to select option #2 above due the extra costs associated with option #1 and other listed benefits for 
option #2.

The ObjectDeleteListener lambda can be enabled again to process S3 deletes on the new bucket only.

The object-key-index on the ObjectStore table will no longer be required.

## Links

* [Version One Story S-75018](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story%3A832020)
* [ADR Core - New S3 Object Store Buckets](../../../Core/Documentation/adr/0001-new-object-store-buckets.md)