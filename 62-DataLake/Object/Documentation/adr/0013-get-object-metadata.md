# Return Object Metadata in Headers for GetObject

Date: 2019-10-07

## Status:  Proposed

* Deciders: Wormhole Team

## Context
Add object metadata to headers of GetObject API for non-binary objects <= 5MB to make responses more consistent with the
response headers that are returned from an S3 redirect (binary objects and objects > 5MB). Objects that are redirected 
to S3 currently contain these headers since object metadata is stored as headers on the S3 object.

## Decision
Discussed 2 options below:
1) Modify the GetObject API to return all metadata (user defined object metadata plus DataLake metadata) in response 
headers with the "x-amz-meta-" prefix. This matches the headers that are returned from an S3 redirect.
2) Modify the GetObject API to return all metadata (user defined object metadata plus DataLake metadata) in response
headers with the "x-dl-meta-" prefix. This prefix will be consistent with the object metadata headers passed to the 
Create/UpdateObject API request. Will required additional modifications to the LambdaEdgeRewriteUrl lambda to modify the 
prefix on "x-amz-meta-" headers to "x-dl-meta-" for consistency.

Option #2 was selected because the request headers for the Create/UpdateObject APIs will be consistent with the headers
returned from the GetObject API. The only negative is that users that access S3 objects using the AWS REST APIs (either
directly or using an SDK) will see metadata headers with the "x-amz-meta-" prefix.

#### GetObject API Modifications 
*  Modify GetObjectCommand lambda to return all object metadata (user defined and DataLake). This metadata will be 
returned in the lambda response using the new "object-metadata" property.
*  Modify the ObjectIdGetResponse.vm Velocity template to read "object-metadata" property and convert to headers with
the "x-dl-meta-" prefix (to be consistent with headers returned from S3 redirect).

Below is an example of object metadata that will be returned from the GetObjectCommand lambda:
```json5
{
        "object-metadata": {
            "dl-version-timestamp": "2019-10-02T18:52:51.752Z",
            "dl-content-sha1": "943012227688fa7ebaa24761378119f9d50aac53",
            "dl-object-id": "aaron88",
            "dl-version-number": "1",
            "dl-collection-id": "AARON_2",
            "name1": "value1",
            "name2": "value2"
        },
}
```

Below are the headers that will be returned from the GetObject API:
```text
        x-dl-meta-dl-version-timestamp: 2019-10-02T18:52:51.752Z
        x-dl-meta-dl-content-sha1: 943012227688fa7ebaa24761378119f9d50aac53
        x-dl-meta-dl-object-id: aaron88
        x-dl-meta-dl-version-number: 1
        x-dl-meta-dl-collection-id: AARON_2
        x-dl-meta-name1: value1
        x-dl-meta-name2: value2
```

#### LambdaEdgeRewriteUrl Modifications
Modify response headers for objects/store/* requests that have the "x-amz-meta-" prefix. The "x-amz-meta-" prefix
will be replaced with "x-dl-meta-" to be consistent with headers in GetObject API responses that are't redirected to S3.

## Consequences
We are changing the current behavior of the headers returned in a GetObject with a redirect.???? 

## Links

* [ADR - Support for S3 User Metadata](0010-s3-user-metadata.md)
* [Version One Story S-81403](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story%3A913579)