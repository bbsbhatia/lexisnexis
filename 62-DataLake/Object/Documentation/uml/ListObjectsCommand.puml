@startuml
skinparam BoxPadding 10
skinparam SequenceTitleFontSize 25
skinparam SequenceGroupHeaderFontSize 15
skinparam SequenceGroupFontSize 15
skinparam SequenceGroupBodyBackgroundColor transparent
skinparam Padding 4


title: List Objects Command

participant Cloudfront
box API Gateway #goldenrod
	participant "DataLakeApi-Object" as Api
end box
box Lambda #darkorange
	participant "ListObjectsCommand" as Command
end box
box DynamoDB #royalblue
	participant CollectionTable
	participant ObjectStoreTable
end box


Cloudfront ++

    Cloudfront -> Api ++: Request GET /objects
        note right Cloudfront
            Required parameters:
                x-api-key (header)
            Optional parameters:
                collection-id (query)
                state (query)
                max-items (query)
                next-token (query)
        endnote

        Api -> Api ++: validate Request: none
            Api --
        Api -> Api ++: Request integration
            Api --

        Api -> Command ++: invoke
            Command -> Command ++: [Commons.SessionDecorator.puml] Decorator
                Command --
            Command -> Command ++: [Commands.CommandWrapper.puml] Decorator
                Command --

            opt if Request contains pagination-token
                opt if Request max-items does not match the original request
                    Api <<-- Command :raise InvalidRequestPropertyValue (400)
                end
            end

            opt if Request max-items is less than 1 or greater than 10
                Api <<-- Command :raise InvalidRequestPropertyValue (400)
            end

            opt if Request has state that is not CREATED, REMOVED, or FAILED
                Api <<-- Command :raise InvalidRequestPropertyValue (400)
            end

            alt if Request contains collection-id
                Command -> CollectionTable ++: get_item() by collection-id
                Command <<-- CollectionTable --: Collection

                opt if Collection returned
                    Command -> Command ++: build collection_hash from collection-id (sha1 hash)
                        Command --
                    alt if Request contains state
                        Command -> ObjectStoreTable ++: query_items() by collection-hash and state
                        Command <<-- ObjectStoreTable --: Objects
                    else
                        Command -> ObjectStoreTable ++: query_items() by collection-hash
                        Command <<-- ObjectStoreTable --: Objects
                    end
                end
            else
                Command -> ObjectStoreTable ++: get_all_items()
                Command <<-- ObjectStoreTable --: Objects
            end

            Command -> Command ++: generate Response dict
                Command --

            Command -> Command ++: [Commands.CommandWrapper.puml] Decorator
                Command --
            Command -> Command ++: [Commons.SessionDecorator.puml] Decorator
                Command --

        Api <<-- Command --: Response (Success)

        Api -> Api ++: Response integration
            note right Api
                Response Integration Status Codes
                    Success: 200 (OK)
                    Lambda Error Regex:
                        ^400.* -> 400 (Bad Request)
                        ^500.* -> 500 (Internal Server Error)
            endnote
        Api --

        Cloudfront <<-- Api: Response <Status Code>

    Api --
Cloudfront --
@enduml