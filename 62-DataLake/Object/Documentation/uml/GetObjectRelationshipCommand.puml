@startuml
skinparam BoxPadding 10
skinparam SequenceTitleFontSize 25
skinparam SequenceGroupHeaderFontSize 15
skinparam SequenceGroupFontSize 15
skinparam SequenceGroupBodyBackgroundColor transparent
skinparam Padding 4


title: Get Object Relationship Command

participant Cloudfront
box "API Gateway" #goldenrod
	participant "DataLakeApi-Object" as Api
end box
box "Lambda" #darkorange
	participant " GetObjectRelationshipLambda (Command) " as Command
end box
box "DynamoDB" #royalblue
    participant CollectionTable
    participant ObjectStoreTable
    participant ObjectStoreVersionTable
    participant RelationshipOwnerTable
    participant ObjectRelationshipTable

end box


Cloudfront ++

    Cloudfront -> Api ++: Request GET /object/{object-id}/relationship/{relationship-id}
        note right Cloudfront
            Required parameters:
                collection-id (query)
                version-number(query)
                objectId (path)
                relationshipId (path)
                x-api-key (header)
            Optional parameters:
                relationship-version (query)
        endnote
        Api -> Api ++: validate Request: \n query params, headers
            Api --
        Api -> Api ++: Request integration
            Api --

        Api -> Command ++: invoke
            Command -> Command ++: [Commons.SessionDecorator.puml] Decorator
                Command --
            Command -> Command ++: [Commands.CommandWrapper.puml] Decorator
                Command --

            Command -> Command ++: object_command.get_collection_response()

                Command -> CollectionTable ++: get_item() Collection by collection-id
                    Command <<-- CollectionTable: Collection
                CollectionTable --

                opt if Collection not found
                    Api <<-- Command :raise Invalid Collection ID (400)
                end

            Command --

            Command -> Command ++: object_command.get_object_response()

                Command -> ObjectStoreTable ++: get_item() Object by object-id and collection-id
                    Command <<-- ObjectStoreTable: Object
                ObjectStoreTable --

                opt if Object not found
                    Api <<-- Command :raise Invalid Object ID (400)
                end
            Command --

            opt if Object version-number not equal to input version-number
                 Command -> Command ++: object_command.get_object_version_response()

                      Command -> ObjectStoreVersionTable ++: get_item() Object Version by object-id, collection-id and version-number
                           Command <<-- ObjectStoreVersionTable: Object Version
                      ObjectStoreVersionTable --

                      opt if Object Version not found
                          Api <<-- Command :raise Invalid Object Version (400)
                      end
               Command --
            end

            Command -> Command ++: object_command.get_relationship_response()

                Command -> RelationshipOwnerTable ++: get_item() Relationship by relationship-id
                    Command <<-- RelationshipOwnerTable: Relationship
                RelationshipOwnerTable --

                opt if Relationship not found
                    Api <<-- Command :raise Invalid Relationship ID (400)
                end

            Command --

            Command -> Command ++: ObjectRelationshipTable().generate_object_composite_id("targetObject")
                 Command --

            alt if relationship-version in request
                Command -> Command ++: get_object_relationship()
                    Command -> Command ++: ObjectRelationshipTable().generate_relationship_key()
                         Command --
                    Command -> ObjectRelationshipTable ++: get_item() Object Relationship by target-id and relationship-key
                          Command <<-- ObjectRelationshipTable: ObjectRelationship
                    ObjectRelationshipTable --
                Command --

                opt if Object Relationship not found
                    Api <<-- Command :raise Invalid Object Relationship (400)
                end
            else if relationship-version not in request
                Command -> Command ++: get_latest_object_relationship()
                    Command -> ObjectRelationshipTable ++: query_items() Object Relationships by target-id, ScanIndexForward=False, max_items=1
                        Command <<-- ObjectRelationshipTable: [ObjectRelationship]
                    ObjectRelationshipTable --
                Command --
                opt if Object Relationships not found
                    Api <<-- Command :raise Invalid Object Relationship (400)
                end
            end

            Command -> Command ++: generate_response_dict()
                Command --

            Command -> Command ++: [Commands.CommandWrapper.puml] Decorator
                Command --
            Command -> Command ++: [Commons.SessionDecorator.puml] Decorator
                Command --

            Api <<-- Command: Response (Success)
        Command --

        Api -> Api ++: Response integration
            Api --
        note right Api
            Response Integration Status Codes
                Success: 200 (Accepted)
                Lambda Error Regex:
                    ^400.* -> 400 (Bad Request)
                    ^500.* -> 500 (Internal Server Error)
        endnote
        Cloudfront <<-- Api: Response <Status Code>
    Api --

Cloudfront --
@enduml