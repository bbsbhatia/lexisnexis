import unittest
from unittest.mock import patch

from importlib import reload

from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
from lng_datalake_testhelper.io_utils import IOUtils

import waf_serverless_whitelist

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'WafServerlessWhitelist')


class TestWafServerlessWhitelist(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        class MockUrllib3Response:
            status = 200
            data = '{"prefixes":[{"ip_prefix": "18.208.0.0/13", "region": "us-east-1", "service": "EC2"}, {"ip_prefix": "52.95.245.0/24", "region": "us-east-1", "service": "EC2"}, {"ip_prefix": "99.77.142.0/24", "region": "ap-east-1", "service": "EC2"}]}'.encode()

            def request(self, method, endpoint):
                return self

        cls.mock_urllib3 = patch('urllib3.PoolManager').start()
        cls.mock_urllib3.return_value = MockUrllib3Response()
        cls.session_patch = patch("lng_aws_clients.session.set_session").start()
        reload(waf_serverless_whitelist)

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.mock_urllib3.stop()
        cls.session_patch.stop()

    # + waf_serverless_whitelist
    @patch('lng_aws_clients.waf_regional.get_client')
    @patch('lng_aws_clients.waf.get_client')
    def test_update_waf_rules_valid(self, mock_waf, mock_waf_regional):
        mock_waf.return_value.get_ip_set.return_value = io_utils.load_data_json('waf.get_ip_set.valid.json')
        mock_waf_regional.return_value.get_ip_set.return_value = io_utils.load_data_json(
            'waf.get_ip_set.regional_valid.json')

        mock_waf.return_value.get_change_token.return_value = io_utils.load_data_json('waf.get_change_token.valid.json')
        mock_waf_regional.return_value.get_change_token.return_value = io_utils.load_data_json(
            'waf.get_change_token.regional_valid.json')

        mock_waf.return_value.update_ip_set.return_value = io_utils.load_data_json('waf.update_ip_set.valid.json')
        mock_waf_regional.return_value.update_ip_set.return_value = io_utils.load_data_json('waf.update_ip_set.valid.json')
        valid_event = io_utils.load_data_json('sns.valid_event.json')
        self.assertIsNone(waf_serverless_whitelist.lambda_handler(valid_event, MockLambdaContext()))


if __name__ == '__main__':
    print(unittest.main())
