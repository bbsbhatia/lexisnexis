import unittest
from unittest.mock import patch

from importlib import reload

from lng_datalake_testhelper.io_utils import IOUtils

import waf_serverless_whitelist

from waf_serverless_whitelist import format_waf_cidr, create_16_ranges, generate_waf_insert, get_existing_cidr, \
    update_waf_rules

__author__ = "Mark Seitter, John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__)


class TestWafServerlessWhitelist(unittest.TestCase):

    # + update_waf_rules - valid ip get
    @patch('urllib3.PoolManager')
    @patch('waf_serverless_whitelist.get_existing_cidr')
    @patch('lng_aws_clients.waf_regional.get_client')
    @patch('lng_aws_clients.waf.get_client')
    def test_update_waf_rules_valid(self, mock_waf, mock_waf_regional, mock_existing_cidrs, mock_urllib3):
        reload(waf_serverless_whitelist)
        mock_urllib3.return_value.request.return_value = MockUrllib3Response(200)
        mock_waf.return_value.get_change_token.return_value = {'ChangeToken': "123"}
        mock_waf_regional.return_value.get_change_token.return_value = {'ChangeToken': "123"}
        mock_existing_cidrs.return_value = ['13.250.0.0/16', '13.251.0.0/16']

        mock_waf.return_value.update_ip_set.return_value = {'ChangeToken': "123"}
        mock_waf_regional.return_value.update_ip_set.return_value = {'ChangeToken': "123"}
        self.assertIsNone(update_waf_rules())

    # - update_waf_rules - error getting IPs
    @patch('urllib3.PoolManager')
    def test_update_waf_rules_invalid(self, mock_urllib3):
        reload(waf_serverless_whitelist)
        mock_request = MockUrllib3Response(400)
        mock_urllib3.return_value.request.return_value = mock_request

        with self.assertRaisesRegex(Exception, 'Non 200 response returned: 400'):
            update_waf_rules()

    # + get_existing_cidr - valid ip get
    def test_get_existing_cidr_valid(self):
        get_ip_set_response = {'IPSet': {'IPSetDescriptors': [
            {'Type': 'IPV4', 'Value': '13.250.0.0/16'},
            {'Type': 'IPV4', 'Value': '13.251.0.0/16'}]}}

        class MockWafClient:

            def get_ip_set(self, IPSetId):
                return get_ip_set_response

        mock_client = MockWafClient()

        expected_response = ['13.250.0.0/16', '13.251.0.0/16']
        self.assertEqual(get_existing_cidr(mock_client, '123'), expected_response)

    # + generate_waf_insert - valid no existing
    def test_generate_waf_insert_valid(self):
        expected = [{'Action': 'INSERT',
                     'IPSetDescriptor': {'Type': 'IPV4', 'Value': '13.250.0.0/16'}},
                    {'Action': 'INSERT',
                     'IPSetDescriptor': {'Type': 'IPV4', 'Value': '13.251.0.0/16'}}]
        self.assertEqual(generate_waf_insert('13.250.0.0/15', []), expected)

    # + generate_waf_insert - valid multiple existing
    def test_generate_waf_insert_valid_multiple_existing(self):
        expected = []
        self.assertEqual(generate_waf_insert('13.250.0.0/15', ['13.250.0.0/16', '13.251.0.0/16']), expected)

    # + generate_waf_insert - valid one existing
    def test_generate_waf_insert_valid_one_existing(self):
        expected = [{'Action': 'INSERT',
                     'IPSetDescriptor': {'Type': 'IPV4', 'Value': '13.250.0.0/16'}}]
        self.assertEqual(generate_waf_insert('13.250.0.0/15', ['13.251.0.0/16']), expected)

    # + generate_waf_insert - valid no existing single block
    def test_generate_waf_insert_valid_single_block(self):
        expected = [{'Action': 'INSERT',
                     'IPSetDescriptor': {'Type': 'IPV4', 'Value': '10.0.0.0/8'}}]
        self.assertEqual(generate_waf_insert('10.0.0.0/8', []), expected)

    # + generate_waf_insert - valid no matches
    def test_generate_waf_insert_valid_no_matches(self):
        expected = [{'Action': 'INSERT',
                     'IPSetDescriptor': {'Type': 'IPV4', 'Value': '10.0.0.0/8'}}]
        self.assertEqual(generate_waf_insert('10.0.0.0/8', ['13.250.0.0/16']), expected)

        # + generate_waf_insert - valid one value

    def test_generate_waf_insert_valid_one_value(self):
        expected = [{'Action': 'INSERT',
                     'IPSetDescriptor': {'Type': 'IPV4', 'Value': '10.0.0.0/32'}}]
        self.assertEqual(generate_waf_insert('10.0.0.0/32', []), expected)

    # +create_16_ranges - valid response
    def test_create_16_ranges_valid_2_block(self):
        expected = ['13.250.0.0/16', '13.251.0.0/16']
        self.assertEqual(create_16_ranges('13.250.0.0', 2), expected)

    # +create_16_ranges - valid response
    def test_create_16_ranges_valid_11_block(self):
        expected = ['44.224.0.0/16', '44.225.0.0/16', '44.226.0.0/16', '44.227.0.0/16', '44.228.0.0/16',
                    '44.229.0.0/16', '44.230.0.0/16', '44.231.0.0/16', '44.232.0.0/16', '44.233.0.0/16',
                    '44.234.0.0/16', '44.235.0.0/16', '44.236.0.0/16', '44.237.0.0/16', '44.238.0.0/16',
                    '44.239.0.0/16', '44.240.0.0/16', '44.241.0.0/16', '44.242.0.0/16', '44.243.0.0/16',
                    '44.244.0.0/16', '44.245.0.0/16', '44.246.0.0/16', '44.247.0.0/16', '44.248.0.0/16',
                    '44.249.0.0/16', '44.250.0.0/16', '44.251.0.0/16', '44.252.0.0/16', '44.253.0.0/16',
                    '44.254.0.0/16', '44.255.0.0/16']
        self.assertEqual(create_16_ranges('44.224.0.0', 32), expected)

    # +format_waf_cidr - valid response
    def test_format_waf_cidr_valid(self):
        expected = {'Action': 'INSERT',
                    'IPSetDescriptor': {
                        'Type': 'IPV4',
                        'Value': '10.0.0.0/8'
                    }}
        self.assertEqual(format_waf_cidr("10.0.0.0/8"), expected)


class MockUrllib3Response:

    data = '{"prefixes":[{"ip_prefix": "18.208.0.0/13", "region": "us-east-1", "service": "EC2"}, {"ip_prefix": "52.95.245.0/24", "region": "us-east-1", "service": "EC2"}, {"ip_prefix": "99.77.142.0/24", "region": "ap-east-1", "service": "EC2"}]}'.encode()

    def __init__(self, status_code):
        self.status = status_code

    def request(self, method, endpoint):
        return self


if __name__ == '__main__':
    print(unittest.main())
