import json
import logging
import os

import urllib3
from lng_aws_clients import session, waf, waf_regional

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

URL_ENDPOINT = os.getenv('URL_ENDPOINT', 'https://ip-ranges.amazonaws.com/ip-ranges.json')
WAF_IPSET_ID = os.getenv('WAF_IPSET_ID')
WAF_REGIONAL_IPSET_ID = os.getenv('WAF_REGIONAL_IPSET_ID')

http = urllib3.PoolManager()
allowed_service = 'EC2'
allowed_regions = ['us-east-1', 'us-east-2', 'us-west-2', 'eu-west-2']
cidr_format = "{0}.{1}.{2}.{3}/{4}"


@session.lng_aws_session()
def lambda_handler(event, context) -> None:
    logger.info("Event: {0}".format(json.dumps(event)))
    logger.info("Context: {0}".format(vars(context)))
    return update_waf_rules()


def update_waf_rules():
    aws_ips_response = http.request('GET', URL_ENDPOINT)
    if aws_ips_response.status != 200:
        raise Exception('Non 200 response returned: {0}'.format(aws_ips_response.status))
    ip_resp_json = json.loads(aws_ips_response.data.decode('utf-8'))

    for client, endpoint in [(waf.get_client(), WAF_IPSET_ID), (waf_regional.get_client(), WAF_REGIONAL_IPSET_ID)]:
        existing_cidr = get_existing_cidr(client, endpoint)

        allow_cidr = []
        for prefix in ip_resp_json['prefixes']:
            if prefix['service'] == allowed_service and prefix['region'] in allowed_regions:
                allow_cidr.extend(generate_waf_insert(prefix['ip_prefix'], existing_cidr))

        if allow_cidr:
            change_token = client.get_change_token()['ChangeToken']

            logger.debug("Adding ips: {0} to endpoint: {1}".format(allow_cidr, endpoint))
            update_ip_set_response = client.update_ip_set(IPSetId=endpoint,
                                                          ChangeToken=change_token,
                                                          Updates=allow_cidr)

            logger.debug(update_ip_set_response)


def get_existing_cidr(client, endpoint) -> list:
    existing = []
    for ipset in client.get_ip_set(IPSetId=endpoint)['IPSet']['IPSetDescriptors']:
        existing.append(ipset['Value'])
    return existing


def generate_waf_insert(ip_cidr: str, existing_cidr: list) -> list:
    address, cidr_range = ip_cidr.split('/')
    cidr_range = int(cidr_range)
    waf_update = []

    # AWS WAF only supports ranges of /8 and any range between /16 through /32
    if cidr_range < 16 and cidr_range != 8:
        iterations = 2 ** (16 - cidr_range)
        cidr_to_add = create_16_ranges(address, iterations)
    else:
        cidr_to_add = [ip_cidr]

    for cidr in cidr_to_add:
        if cidr not in existing_cidr:
            waf_update.append(format_waf_cidr(cidr))
    return waf_update


def create_16_ranges(ipv4_base: str, iterations: int) -> list:
    cidrs = []
    digits = ipv4_base.split('.')
    # Add the first
    cidrs.append(cidr_format.format(*digits, 16))
    # always do N-1 iterations since we do the first one above
    for x in range(iterations - 1):
        digits[1] = int(digits[1]) + 1
        cidrs.append(cidr_format.format(*digits, 16))

    return cidrs


def format_waf_cidr(cidr_address: str) -> dict:
    return {'Action': 'INSERT',
            'IPSetDescriptor': {
                'Type': 'IPV4',
                'Value': cidr_address
            }}


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL TEST RUN - BEGIN]")

    os.environ['AWS_PROFILE'] = 'c-sand'

    WAF_IPSET_ID = ''
    WAF_REGIONAL_IPSET_ID = ''

    session.set_session()
    event = {
        "Records": [{
            "Sns": {
                "Type": "Notification",
                "MessageId": "10070a22-f9fc-5be0-bcf3-5fe04a298cee",
                "TopicArn": "arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged",
                "Subject": "IP Space Changed",
                "Message": "{\"create-time\":\"2019-08-05-22-03-05\",\"synctoken\":\"1565042585\",\"md5\":\"28e5576ff7042572179f551e0cad3675\",\"url\":\"https://ip-ranges.amazonaws.com/ip-ranges.json\"}",
                "Timestamp": "2019-08-05T22:45:41.589Z",
                "SignatureVersion": "1",
                "Signature": "QZUj8bW2Llhdm3S3uZrE7256bJPCO+JEdvczVjhZIPigD29Ia2kxUgXrDtBiQvVPghSoYz72qOca1DMcqIfqnWkx3o63MqPxbmwpPr5s01CCZb5/lpXiQaakpl2+duFMR4xXr1xZyTK/uJw6q75x7fNKbsgItiHB2s6OWYwclUNo1ih/LMRMlAvlMp/YwcMGwfy/NIuwkVmLlYBm4Cm40SbRoeUOIFKVZz8fl0mH9jTLfbg5HSu/ADhceYs4Xxw5QjbPGe08513CcWRUn1m4ZfAfUs+ERxb/skHL1/AbAIV3OLABkKdtt4dVkUtWZ8dc6kUFDgEHNijEqrBo0c5D/w==",
                "SigningCertURL": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem",
                "UnsubscribeURL": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged:719b87a6-cdb4-430e-a72f-7802b0f94707"
            }

        }]
    }
    lambda_handler(event, MockLambdaContext)
    logger.debug("[LOCAL TEST RUN - END]")
