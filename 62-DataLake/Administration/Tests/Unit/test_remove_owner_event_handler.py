import json
import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commands.exceptions import InternalError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_constants.event_names import OWNER_REMOVE
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import remove_owner_event_handler
from service_commons import owner_event_handler

io_util = IOUtils(__file__, 'RemoveOwnerEventHandler')


class TestRemoveOwnerEventHandler(unittest.TestCase):

    # + remove_owner_event_handler - removes owner from the owner table
    @patch('remove_owner_event_handler.remove_owner_api_key')
    @patch("service_commons.owner_event_handler.get_owner_collection_status")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    @patch("remove_owner_event_handler.remove_owner_from_table")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_remove_owner_event_handler_removes_from_table_when_message_is_valid(self,
                                                                                 mock_is_valid_event_message,
                                                                                 mock_remove_owner_from_table,
                                                                                 mock_extract_sns_message,
                                                                                 mock_get_collection_status,
                                                                                 mock_remove_owner_api_key):
        # Given
        msg = {
            "owner-id": 25,
            "owner-name": "WormholeUK",
            "email-distribution": [
                "wormholeuk@lexisnexis.com",
                "wormholeuk@legal.regn.net"
            ],
            "timestamp": "2017-11-13T10:16:08.829755",
            "event-id": "xyz",
            "event-version": 1,
            "stage": "LATEST"
        }
        event = {
            "Records": [{
                "Sns": {
                    "Message": json.dumps(msg)
                }
            }]
        }
        expected_message = {
            "owner-id": 25,
            "owner-name": "WormholeUK",
            "email-distribution": [
                "wormholeuk@lexisnexis.com",
                "wormholeuk@legal.regn.net"
            ],
            "timestamp": "2017-11-13T10:16:08.829755",
            "event-id": "xyz",
            "event-version": 1,
            "stage": "LATEST"
        }
        mock_extract_sns_message.return_value = expected_message
        mock_is_valid_event_message.return_value = True
        mock_get_collection_status.return_value = owner_event_handler.COLLECTION_NONE
        mock_remove_owner_api_key.return_value = None

        # When
        result = remove_owner_event_handler.remove_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        mock_is_valid_event_message.assert_called_once_with(expected_message, OWNER_REMOVE)
        mock_extract_sns_message.assert_called_once_with(event)
        mock_remove_owner_from_table.assert_called_once_with(expected_message["owner-id"])
        self.assertEqual(result, event_handler_status.SUCCESS)

    # - remove_owner_event_handler raises a TerminalErrorException when it can't get owner_id from message
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_remove_owner_event_handler_removes_from_table_when_message_is_valid_1(self,
                                                                                   mock_is_valid_event_message,
                                                                                   mock_extract_sns_message):
        # Given
        msg = {
            "owner-id": 25,
            "owner-name": "WormholeUK",
            "email-distribution": [
                "wormholeuk@lexisnexis.com",
                "wormholeuk@legal.regn.net"
            ],
            "timestamp": "2017-11-13T10:16:08.829755",
            "event-id": "xyz",
            "event-version": 1,
            "stage": "LATEST"
        }
        event = {
            "Records": [{
                "Sns": {
                    "Message": json.dumps(msg)
                }
            }]
        }
        invalid_message = {
            "owner-name": "WormholeUK",
            "email-distribution": [
                "wormholeuk@lexisnexis.com",
                "wormholeuk@legal.regn.net"
            ],
            "timestamp": "2017-11-13T10:16:08.829755",
            "event-id": "xyz",
            "event-version": 1,
            "stage": "LATEST"
        }
        mock_extract_sns_message.return_value = invalid_message
        mock_is_valid_event_message.return_value = True

        # When
        with self.assertRaises(TerminalErrorException) as context:
            remove_owner_event_handler.remove_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        mock_is_valid_event_message.assert_called_once_with(invalid_message, OWNER_REMOVE)
        mock_extract_sns_message.assert_called_once_with(event)
        key_error = KeyError("owner-id")
        expected_error_message = "Key = {0} does not exist. Message = {1}".format(key_error, invalid_message)
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    # - remove_owner_event_handler raises a TerminalErrorException when owner collection status is ACTIVE
    @patch("service_commons.owner_event_handler.get_owner_collection_status")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_remove_owner_event_handler_removes_from_table_when_message_is_valid_2(self,
                                                                                   mock_is_valid_event_message,
                                                                                   mock_extract_sns_message,
                                                                                   mock_get_collection_status):
        # Given
        msg = {
            "owner-id": 25,
            "owner-name": "WormholeUK",
            "email-distribution": [
                "wormholeuk@lexisnexis.com",
                "wormholeuk@legal.regn.net"
            ],
            "timestamp": "2017-11-13T10:16:08.829755",
            "event-id": "xyz",
            "event-version": 1,
            "stage": "LATEST"
        }
        event = {
            "Records": [{
                "Sns": {
                    "Message": json.dumps(msg)
                }
            }]
        }
        expected_message = {
            "owner-id": 25,
            "owner-name": "WormholeUK",
            "email-distribution": [
                "wormholeuk@lexisnexis.com",
                "wormholeuk@legal.regn.net"
            ],
            "timestamp": "2017-11-13T10:16:08.829755",
            "event-id": "xyz",
            "event-version": 1,
            "stage": "LATEST"
        }
        mock_extract_sns_message.return_value = expected_message
        mock_is_valid_event_message.return_value = True
        mock_get_collection_status.return_value = owner_event_handler.COLLECTION_ACTIVE

        # When
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to process event*') as _:
            remove_owner_event_handler.remove_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        mock_is_valid_event_message.assert_called_once_with(expected_message, OWNER_REMOVE)

    # - remove_owner_event_handler - raises a TerminalErrorException when is_valid_event_message is false
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_remove_owner_event_handler_raises_exception_when_message_is_invalid(self,
                                                                                 mock_is_valid_event_message,
                                                                                 mock_extract_sns_message,
                                                                                 mock_set_session):
        # Given
        event = {"event-name": "Owner::Create"}
        mock_extract_sns_message.return_value = event
        mock_is_valid_event_message.return_value = False

        # When
        with self.assertRaises(TerminalErrorException) as context:
            remove_owner_event_handler.remove_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        mock_is_valid_event_message.assert_called_once_with(event, OWNER_REMOVE)
        expected_error_message = "Failed to extract event: {} or event-name doesnt match {}".format(event, OWNER_REMOVE)
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    # + remove_owner_from_table - sucessfully called delete_item on OwnerTable
    @patch("lng_datalake_dal.owner_table.OwnerTable.delete_item")
    def test_remove_owner_from_table_successfully_calls_delete_item(self,
                                                                    mock_delete_item):
        # Given
        owner_id = 1

        # When
        remove_owner_event_handler.remove_owner_from_table(owner_id)

        # Then
        mock_delete_item.assert_called_once_with({"owner-id": owner_id})

    # - remove_owner_event_handler - raises a TerminalErrorException when incorrect event version
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    def test_remove_owner_event_handler_raises_exception_when_message_is_invalid_version(self, mock_extract_sns_message,
                                                                                         mock_set_session,
                                                                                         mock_valid_event_message):
        # Given
        mock_extract_sns_message.return_value = None
        mock_valid_event_message.return_value = True
        event = {}

        # When
        with self.assertRaises(TerminalErrorException) as context:
            remove_owner_event_handler.remove_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        expected_error_message = "Failed to process event: {} event-version does not match {}".format(event, "1")
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    # - remove_owner_event_handler - raises a TerminalErrorException when incorrect stage
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    def test_remove_owner_event_handler_raises_exception_when_stage_is_invalid(self, mock_extract_sns_message,
                                                                               mock_set_session,
                                                                               mock_valid_event_message,
                                                                               mock_valid_event_version):
        # Given
        mock_extract_sns_message.return_value = None
        mock_valid_event_message.return_value = True
        mock_valid_event_version.return_value = True
        event = {}

        # When
        with self.assertRaises(TerminalErrorException) as context:
            remove_owner_event_handler.remove_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        expected_error_message = "Failed to process event: {} stage not match {}".format(event,
                                                                                         MockLambdaContext().
                                                                                         invoked_function_arn)
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    # - remove_owner_from_table - raises internal error when dal raises EndpointConnectionError
    @patch("lng_datalake_dal.owner_table.OwnerTable.delete_item")
    def test_remove_owner_from_table_raises_error_when_endpoint_connection_error_exception_thrown(self,
                                                                                                  mock_delete_item):
        # Given
        owner = 1
        endpoint_connection_error = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')

        mock_delete_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')

        # When
        with self.assertRaises(EndpointConnectionError) as context:
            remove_owner_event_handler.remove_owner_from_table(owner)

        # Then
        expected_exception = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        self.assertEqual(str(context.exception), str(expected_exception))

    # - remove_owner_from_table - raises internal error when dal raises client error
    @patch("lng_datalake_dal.owner_table.OwnerTable.delete_item")
    def test_remove_owner_from_table_raises_endpoint_connection_error_exception(self,
                                                                                mock_delete_item):
        # Given
        owner = 1
        endpoint_connection_error = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')

        mock_delete_item.side_effect = endpoint_connection_error

        # When
        with self.assertRaises(EndpointConnectionError) as context:
            remove_owner_event_handler.remove_owner_from_table(owner)

        # Then
        expected_exception = endpoint_connection_error
        self.assertEqual(str(context.exception), str(expected_exception))

    # - remove_owner_from_table - raises client error
    @patch("lng_datalake_dal.owner_table.OwnerTable.delete_item")
    def test_remove_owner_from_table_raises_error_when_client_error_thrown(self,
                                                                           mock_delete_item):
        # Given
        owner = 1
        client_error = ClientError({'ResponseMetadata': {},
                                    'Error': {'Code': 'ProvisionedThroughputExceededException',
                                              'Message': 'This is a mock'}}, "FAKE")

        mock_delete_item.side_effect = client_error

        # When
        with self.assertRaises(ClientError) as context:
            remove_owner_event_handler.remove_owner_from_table(owner)

        # Then
        expected_exception = client_error
        self.assertEqual(str(context.exception), str(expected_exception))

    # - remove_owner_from_table - raises internal error when dal raises client error
    @patch("lng_datalake_dal.owner_table.OwnerTable.delete_item")
    def test_remove_owner_from_table_raises_error_when_exception_thrown(self,
                                                                        mock_delete_item):
        # Given
        owner = 1
        exception = Exception("Error occurred")
        mock_delete_item.side_effect = exception

        # When
        with self.assertRaises(TerminalErrorException) as context:
            remove_owner_event_handler.remove_owner_from_table(owner)

        # Then
        expected_exception = TerminalErrorException("General exception occurred when removing owner id 1 from table. Error: Error occurred")
        self.assertEqual(str(context.exception), str(expected_exception))

    # + remove_owner_api_key removes api key and updates item
    @patch('remove_owner_event_handler.delete_api_key')
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_remove_owner_api_key(self,
                                  mock_owner_table_update_item,
                                  mock_get_item_from_owner_table,
                                  mock_delete_api_key):
        # Given
        mock_delete_api_key.return_value = None
        owner_resp = io_util.load_data_json('dynamodb.get_owner.json')
        mock_get_item_from_owner_table.return_value = owner_resp
        owner_resp_no_api_key = dict(owner_resp)
        owner_resp_no_api_key['api-key'] = 'REMOVED'
        owner_resp_no_api_key['api-key-id'] = 'REMOVED'
        owner_resp_no_api_key['event-ids'] = ['test_event']


        # When
        result = remove_owner_event_handler.remove_owner_api_key(owner_resp, "test_event")

        # Then
        mock_owner_table_update_item.assert_called_once_with(owner_resp_no_api_key)
        self.assertEqual(result, None)

    # - remove_owner_api_key throws EndpointConnectionError on update item
    @patch('remove_owner_event_handler.delete_api_key')
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_remove_owner_api_key_endpoint_connection_error_exception(self,
                                                                      mock_owner_table_update_item,
                                                                      mock_get_item_from_owner_table,
                                                                      mock_delete_api_key):
        # Given
        mock_delete_api_key.return_value = None
        owner_resp = io_util.load_data_json('dynamodb.get_owner.json')
        mock_get_item_from_owner_table.return_value = owner_resp
        mock_owner_table_update_item.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')

        # When
        with self.assertRaisesRegex(InternalError, "Unable to remove owner's API Key"):
            remove_owner_event_handler.remove_owner_api_key(owner_resp)

    # - remove_owner_api_key throws EndpointConnectionError on update item
    @patch('remove_owner_event_handler.delete_api_key')
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_remove_owner_api_key_endpoint_connection_error_exception(self,
                                                                      mock_owner_table_update_item,
                                                                      mock_get_item_from_owner_table,
                                                                      mock_delete_api_key):
        # Given
        mock_delete_api_key.return_value = None
        owner_resp = io_util.load_data_json('dynamodb.get_owner.json')
        mock_get_item_from_owner_table.return_value = owner_resp
        mock_owner_table_update_item.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')

        # When
        with self.assertRaises(EndpointConnectionError):
            remove_owner_event_handler.remove_owner_api_key(owner_resp, "test_event")

    # - remove_owner_api_key throws ClientError on update item
    @patch('remove_owner_event_handler.delete_api_key')
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_remove_owner_api_key_client_error(self,
                                               mock_owner_table_update_item,
                                               mock_get_item_from_owner_table,
                                               mock_delete_api_key):
        # Given
        mock_delete_api_key.return_value = None
        owner_resp = io_util.load_data_json('dynamodb.get_owner.json')
        mock_get_item_from_owner_table.return_value = owner_resp
        mock_owner_table_update_item.side_effect = ClientError({"ResponseMetadata": {}},
                                                               "OperationName")

        # When
        with self.assertRaises(ClientError):
            remove_owner_event_handler.remove_owner_api_key(owner_resp, "test_event")

    # - remove_owner_api_key throws InternalError on get item
    @patch('remove_owner_event_handler.delete_api_key')
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_remove_owner_api_key_internal_error_update_item(self,
                                                             mock_owner_table_update_item,
                                                             mock_get_item_from_owner_table,
                                                             mock_delete_api_key):
        # Given
        mock_delete_api_key.return_value = None
        owner_resp = io_util.load_data_json('dynamodb.get_owner.json')
        mock_get_item_from_owner_table.return_value = owner_resp
        mock_owner_table_update_item.side_effect = Exception("Some error")

        # When
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to remove owner apiKey from Owner table due to unknown reasons."):
            remove_owner_event_handler.remove_owner_api_key(owner_resp, "test_event")

    # - delete_api_key_error throws InternalError
    @patch('lng_aws_clients.apigateway.get_client')
    def test_delete_api_key_error(self, mock_apigateway_get_client):
        mock_apigateway_get_client.return_value.delete_api_key.side_effect = ClientError({"ResponseMetadata": {}},
                                                                                         "OperationName")
        with self.assertRaises(ClientError):
            remove_owner_event_handler.delete_api_key("123")

        mock_apigateway_get_client.return_value.delete_api_key.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            remove_owner_event_handler.delete_api_key("123")

        mock_apigateway_get_client.return_value.delete_api_key.side_effect = Exception("some error")
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to delete api key from ApiGateway due to unknown reasons. "):
            remove_owner_event_handler.delete_api_key("123")


if __name__ == '__main__':
    unittest.main()
