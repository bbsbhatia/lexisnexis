import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, NotAuthorizedError, ForbiddenStateTransitionError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from remove_owner_command import remove_owner_command, validate_relationships_removed
from service_commons import owner_command

io_util = IOUtils(__file__, 'RemoveOwner')


class TestRemoveOwner(unittest.TestCase):

    # + remove_owner_command adds remove event to event_store collections_status = None
    @patch('lng_aws_clients.apigateway.get_client')
    @patch("service_commons.owner_command.get_owner_collection_status")
    @patch("service_commons.owner_command.create_owner_event_store_item")
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch("service_commons.owner_command.generate_json_response")
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.query_items')
    def test_remove_owner_command_adds_remove_event_to_event_store_none(self, mock_query_items,
                                                                        mock_generate_json_response,
                                                                        mock_owner_authorization,
                                                                        mock_create_owner_event_store_item,
                                                                        mock_get_collection_status,
                                                                        mock_apigateway_client):
        # Given
        event = {"owner-id": 123}
        event_version = 1
        aws_request_id = MockLambdaContext.aws_request_id
        owner_resp = io_util.load_data_json('dynamodb.get_owner.json')
        mock_owner_authorization.return_value = owner_resp
        mock_query_items.return_value = None
        mock_create_owner_event_store_item.return_value = io_util.load_data_json('expected_event.json')
        mock_generate_json_response.return_value = io_util.load_data_json('expected_response.json')
        mock_get_collection_status.return_value = owner_command.COLLECTION_NONE
        mock_apigateway_client.return_value.delete_api_key.return_value = None

        # When
        result = remove_owner_command(event, aws_request_id, "LATEST", "testApiKeyId")

        # Then
        mock_owner_authorization.assert_called_once_with(event["owner-id"], "testApiKeyId")
        mock_get_collection_status.assert_called_once_with(event["owner-id"])
        mock_create_owner_event_store_item.assert_called_once_with(mock_owner_authorization.return_value,
                                                                   "Owner::Remove",
                                                                   aws_request_id,
                                                                   event_version,
                                                                   "LATEST")
        mock_generate_json_response.assert_called_once()
        expected_response = {
            'response-dict': io_util.load_data_json('expected_response.json'),
            'event-dict': io_util.load_data_json('expected_event.json')
        }

        self.assertDictEqual(result, expected_response)

    # + remove_owner_command adds remove event to event_store collection = TERMINATED
    @patch('lng_aws_clients.apigateway.get_client')
    @patch("service_commons.owner_command.get_owner_collection_status")
    @patch("service_commons.owner_command.create_owner_event_store_item")
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch("service_commons.owner_command.generate_json_response")
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.query_items')
    def test_remove_owner_command_adds_remove_event_to_event_store_terminated(self, mock_query_items,
                                                                              mock_generate_json_response,
                                                                              mock_owner_authorization,
                                                                              mock_create_owner_event_store_item,
                                                                              mock_get_collection_status,
                                                                              mock_apigateway_client):
        # Given
        event = {"owner-id": 123}
        aws_request_id = MockLambdaContext.aws_request_id
        owner_resp = io_util.load_data_json('dynamodb.get_owner.json')
        mock_owner_authorization.return_value = owner_resp
        mock_query_items.return_value = None
        mock_create_owner_event_store_item.return_value = io_util.load_data_json('expected_event.json')
        mock_generate_json_response.return_value = io_util.load_data_json('expected_response.json')
        mock_get_collection_status.return_value = owner_command.COLLECTION_TERMINATED
        mock_apigateway_client.return_value.delete_api_key.return_value = None

        # When
        result = remove_owner_command(event, aws_request_id, "LATEST", "testApiKeyId")

        # Then
        mock_owner_authorization.assert_called_once_with(event["owner-id"], "testApiKeyId")
        mock_get_collection_status.assert_called_once_with(event["owner-id"])
        mock_create_owner_event_store_item.assert_called_once()
        mock_generate_json_response.assert_called_once()
        expected_response = {
            'response-dict': io_util.load_data_json('expected_response.json'),
            'event-dict': io_util.load_data_json('expected_event.json')
        }

        self.assertDictEqual(result, expected_response)

    # - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_owner_command_failed_owner_auth(self, mock_set_session, mock_owner_authorization):
        # Given
        request = {"owner-id": 123}
        aws_request_id = MockLambdaContext.aws_request_id
        mock_set_session.return_value = None

        # When
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")

        # Then
        with self.assertRaisesRegex(NotAuthorizedError,
                                    "NotAuthorizedError||API Key provided is not valid for Owner ID.*"):
            remove_owner_command(request, aws_request_id, "LATEST", "testApiKeyId-bad")

    # - remove_owner_command throws when owner has at least one active collection
    @patch("lng_aws_clients.session.set_session")
    @patch("service_commons.owner_command.get_owner_collection_status")
    @patch("service_commons.owner_command.create_owner_event_store_item")
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.query_items')
    def test_remove_owner_command_throws_when_get_collection_status(self, mock_query_items,
                                                                    mock_owner_authorization,
                                                                    mock_create_owner_event_store_item,
                                                                    mock_get_collection_status,
                                                                    mock_set_session):
        # Given
        event = {"owner-id": 123}
        api_gateway_request_id = MockLambdaContext.aws_request_id
        mock_set_session.return_value = None
        mock_query_items.return_value = None
        mock_owner_authorization.return_value = io_util.load_data_json('dynamodb.get_owner.json')
        mock_get_collection_status.return_value = owner_command.COLLECTION_ACTIVE

        # When
        with self.assertRaisesRegex(ForbiddenStateTransitionError,
                                    "Owner has at least one non terminated collection") as context:
            remove_owner_command(event, api_gateway_request_id, "LATEST", "testApiKeyId")

        # Then
        mock_owner_authorization.assert_called_once_with(event["owner-id"], "testApiKeyId")
        mock_get_collection_status.assert_called_once_with(event["owner-id"])
        mock_create_owner_event_store_item.assert_not_called()

    # - remove_owner_command throws internal error when collection status is not ACTIVE, NONE or TERMINATED
    @patch('lng_aws_clients.apigateway.get_client')
    @patch("service_commons.owner_command.get_owner_collection_status")
    @patch("lng_aws_clients.session.set_session")
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch("service_commons.owner_command.generate_json_response")
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.query_items')
    def test_remove_owner_command_internal_error(self, mock_query_items,
                                                 mock_generate_json_response,
                                                 mock_owner_authorization,
                                                 mock_set_session,
                                                 mock_get_collection_status,
                                                 mock_apigateway_client):
        # Given
        event = {"owner-id": 123}
        aws_request_id = MockLambdaContext.aws_request_id
        owner_resp = io_util.load_data_json('dynamodb.get_owner.json')
        mock_owner_authorization.return_value = owner_resp
        mock_query_items.return_value = None
        mock_generate_json_response.return_value = io_util.load_data_json('expected_response.json')
        mock_get_collection_status.return_value = "INVALID_STATUS"
        mock_apigateway_client.return_value.delete_api_key.return_value = None
        mock_set_session.return_value = None

        # Then
        with self.assertRaisesRegex(InternalError,
                                    "InternalError||Unknown collection status of INVALID_STATUS for owner *"):
            remove_owner_command(event, aws_request_id, "LATEST", "testApiKeyId")

    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.query_items')
    def test_get_relationship_from_relationship_owner_table_client_error(self, mock_query_items):
        mock_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'Unit Test',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        with self.assertRaisesRegex(InternalError,
                                    "InternalError||Unable to get item from Owner Table||An error occurred"
                                    "(Unit Test) when calling the FAKE operation: This is a mock"):
            validate_relationships_removed(2)

    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.query_items')
    def test_get_relationship_from_relationship_owner_table(self, mock_query_items):
        mock_query_items.return_value = None
        self.assertIsNone(validate_relationships_removed(2))

    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.query_items')
    def test_get_relationship_from_relationship_owner_table_unhandled_exception(self, mock_query_items):
        mock_query_items.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            validate_relationships_removed(1)

    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.query_items')
    def test_get_relationship_from_relationship_owner_table_client_error_1(self, mock_query_items):
        mock_query_items.return_value = [{"relationship-id": "notRemoved", "relationship-state": "Created"}]
        with self.assertRaisesRegex(ForbiddenStateTransitionError,
                                    "RelationshipID notRemoved must be in a Removed state to be removed"):
            validate_relationships_removed(1)


if __name__ == "__main__":
    unittest.main()
