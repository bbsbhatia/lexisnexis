import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, CollectionPrefixAlreadyExists, \
    ConflictingRequestError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_owner_command import command_wrapper, create_owner_command, validate_collection_prefix, validate_email_list, \
    validate_owner_name, validate_unique_collection_prefix, validate_unique_owner, lambda_handler, get_next_owner_id, \
    get_usage_plan, create_api_key, generate_event_dict, generate_json_response

asset_group = 'AWSTEST'

io_util = IOUtils(__file__, 'CreateOwner')


class TestCreateOwner(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # - command_wrapper Bad Decorator
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_aws_clients.session.set_session')
    def test_command_decorator_fail(self, session_mock, mock_track):
        session_mock.return_value = None
        mock_track.return_value = None

        request_input = io_util.load_data_json('apigateway.request.failure.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyValue):
                lambda_handler(request_input, MockLambdaContext())

    # + lambda_handler valid response
    @patch('create_owner_command.create_owner_command')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self, mock_session, mock_event_store_put_item, mock_create_owner_command_response):
        mock_session.return_value = None
        mock_event_store_put_item.return_value = None
        mock_create_owner_command_response.return_value = io_util.load_data_json(
            'expected_create_owner_response_valid.json')

        request_input = io_util.load_data_json('apigateway.request.json')
        expected_response = io_util.load_data_json('apigateway.response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response, lambda_handler(request_input, MockLambdaContext()))

    # + create_owner_command success
    @patch("lng_datalake_commands.command_wrapper.get_request_time")
    @patch("lng_datalake_commands.command_wrapper.get_optional_response_schema_keys")
    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    @patch('create_owner_command.create_api_key')
    @patch('create_owner_command.get_next_owner_id')
    @patch('create_owner_command.validate_unique_collection_prefix')
    @patch('create_owner_command.validate_unique_owner')
    def test_create_owner_success(self, mock_validate_unique_owner, mock_validate_unique_collection_prefix,
                                  mock_get_next_owner_id, mock_create_api_key, mock_command_wrapper_get_required,
                                  mock_command_wrapper_get_optional, mock_command_wrapper_get_request_time):
        mock_validate_unique_owner.return_value = None
        mock_validate_unique_collection_prefix.return_value = None
        mock_get_next_owner_id.return_value = 31
        mock_create_api_key.return_value = ("keykeykeykeykeykeykeykeykeykeykeykeykey", "123456789")
        mock_command_wrapper_get_required.return_value = io_util.load_data_json("schema.required_response_schema_keys.json")
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json("schema.optional_response_schema_keys.json")
        mock_command_wrapper_get_request_time.return_value = "2018-05-18T19:31:17.813Z"

        request = io_util.load_data_json("owner_request_valid.json")
        request_id = "abcd"
        stage = "LATEST"
        expected_response = io_util.load_data_json("expected_create_owner_response_valid.json")
        self.assertDictEqual(expected_response, create_owner_command(request, request_id, stage))

    # - create_owner_command Invalid Request Property Value
    @patch('create_owner_command.create_api_key')
    @patch('create_owner_command.get_next_owner_id')
    @patch('create_owner_command.validate_unique_collection_prefix')
    @patch('create_owner_command.validate_unique_owner')
    def test_create_owner_multiple_collection_prefixes(self, mock_validate_unique_owner,
                                                       mock_validate_unique_collection_prefix, mock_get_next_owner_id,
                                                       mock_create_api_key):
        mock_validate_unique_owner.return_value = None
        mock_validate_unique_collection_prefix.return_value = None
        mock_get_next_owner_id.return_value = 99
        mock_create_api_key.return_value = ("fake-api-key", "fake-api-key-id")

        formatted_request = io_util.load_data_json('owner_request_too_many_prefixes.json')
        request_id = "abcd"
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid number of collection Prefixes 2"):
            create_owner_command(formatted_request, request_id, "LATEST")

    # + get_next_owner_id calls update_counter and returns the response
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    def test_get_next_owner_id_gets_result_from_counter_table(self, mock_counter_table_update_counter):
        expected_table_name = "owner"
        expected_owner_id = 123
        mock_counter_table_update_counter.return_value = expected_owner_id

        actual = get_next_owner_id()
        self.assertEqual(expected_owner_id, actual)

        mock_counter_table_update_counter.assert_called_once_with(expected_table_name)

    # - get_next_owner_id client error
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    def test_get_next_owner_id_raises_internal_error_when_client_error_occurs(self, mock_counter_table_update_counter):
        mock_counter_table_update_counter.side_effect = ClientError({"ResponseMetadata": {},
                                                                     "Error": {"Code": "mock error code",
                                                                               "Message": "mock error message"}},
                                                                    "client-error-mock")

        with self.assertRaisesRegex(InternalError, "Unable to update Counter Table"):
            get_next_owner_id()

    # - get_next_owner_id internal error
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    def test_get_next_owner_id_raises_internal_error_when_exception_occurs(self, mock_counter_table_update_counter):
        mock_counter_table_update_counter.side_effect = Exception(
            {'Error': {'Code': '500', 'Message': 'Generic Error'}}, 'update_counter')

        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_next_owner_id()

    # + create_api_key success
    @patch("lng_aws_clients.apigateway.get_client")
    def test_create_api_key_success(self, mock_apigateway):
        mock_apigateway.return_value.create_api_key.return_value = io_util.load_data_json(
            'apiclient.create_api_key_response.json')
        mock_apigateway.return_value.get_paginator.return_value.paginate.return_value = io_util.load_data_json(
            'apigateway.get_usage_plans_response_valid.json')

        request = io_util.load_data_json('owner_request_valid.json')
        expected = 'keykeykeykeykeykeykeykeykeykeykeykeykey', '123456789'

        with patch('create_owner_command.asset_group', asset_group):
            self.assertEqual(expected, create_api_key(request))

    # - create_api_key internal error with bad environment variable
    @patch("create_owner_command.get_usage_plan")
    def test_create_api_key_error_raises_internal_error_when_environ_key_error_occurs(self, mock_get_usage_plan):
        mock_get_usage_plan.return_value = None

        request = io_util.load_data_json('owner_request_valid.json')

        with self.assertRaisesRegex(InternalError, 'Required environment variable '
                                                   'ASSET_GROUP not defined or empty'):
            with patch('create_owner_command.asset_group', None):
                create_api_key(request)

    # - create_api_key Client Error
    @patch("create_owner_command.get_usage_plan")
    @patch("lng_aws_clients.apigateway.get_client")
    def test_create_api_key_error_raises_internal_error_when_client_error_occurs(self, mock_apigateway,
                                                                                 mock_get_usage_plan):
        mock_apigateway.return_value.create_api_key.side_effect = ClientError(
            {"ResponseMetadata": {}, "Error": {"Code": "500", "Message": "Error Creating Owner"}}, "update_counter")
        mock_get_usage_plan.return_value = None

        request = {'owner-id': 31,
                   'owner-name': 'Wormhole API Test',
                   'email-distribution': ['wormholeapi@reedelsevier.com']}

        with self.assertRaisesRegex(InternalError, "Unable to create API Key"):
            with patch('create_owner_command.asset_group', asset_group):
                create_api_key(request)

    # - create_api_key, KeyError
    @patch("create_owner_command.get_usage_plan")
    @patch("lng_aws_clients.apigateway.get_client")
    def test_create_api_key_error_raises_internal_error_when_key_error_occurs(self, mock_apigateway,
                                                                              mock_get_usage_plan):
        mock_apigateway.return_value.create_api_key.side_effect = KeyError(
            {'Error': {'Code': '500', 'Message': 'Error Creating Owner'}}, 'API Key not returned')
        mock_get_usage_plan.return_value = None

        request = {'owner-id': 31,
                   'owner-name': 'Wormhole API Test',
                   'email-distribution': ['wormholeapi@reedelsevier.com']}

        with self.assertRaisesRegex(InternalError, "API Key not returned"):
            with patch('create_owner_command.asset_group', asset_group):
                create_api_key(request)

    # - create_api_key internal error is raised
    @patch("create_owner_command.get_usage_plan")
    @patch("lng_aws_clients.apigateway.get_client")
    def test_create_api_key_error_raises_internal_error_when_exception_error_occurs(self, mock_apigateway,
                                                                                    mock_get_usage_plan):
        mock_apigateway.return_value.create_api_key.side_effect = Exception(
            {'Error': {'Code': '500', 'Message': 'Error Creating Owner'}}, 'General Exception Occurred')
        mock_get_usage_plan.return_value = None

        request = {'owner-id': 31,
                   'owner-name': 'Wormhole API Test',
                   'email-distribution': ['wormholeapi@reedelsevier.com']}

        with self.assertRaisesRegex(InternalError, 'Unhandled exception occurred'):
            with patch('create_owner_command.asset_group', asset_group):
                create_api_key(request)

    # - get_usage_plan InternalError
    @patch("lng_aws_clients.apigateway.get_client")
    def test_get_usage_plan_error_raises_internal_error_when_undefined_usage_plan(self, mock_apigateway):
        api_get_usage_plans_resp = io_util.load_data_json('apigateway.get_usage_plans_response_valid.json')
        mock_apigateway.return_value.get_usage_plans.return_value = api_get_usage_plans_resp

        undefined_usage_plan_name = 'CANT_FIND_ME'

        with self.assertRaisesRegex(InternalError,
                                    "Could not find usage plan CANT_FIND_ME".format(undefined_usage_plan_name)):
            get_usage_plan(undefined_usage_plan_name)

    # - get_usage_plan ClientError
    @patch("lng_aws_clients.apigateway.get_client")
    def test_get_usage_plan_error_raises_internal_error_when_client_error_occurs(self, mock_apigateway):
        mock_apigateway.return_value.get_paginator.side_effect = ClientError(
            {'Error': {'Code': '500', 'Message': 'Error Getting Usage Plan'}}, 'get_usage_plans')

        with self.assertRaisesRegex(InternalError, "Unable get usage plans for current account"):
            get_usage_plan('DUMMY_USAGE_PLAN')

    # - get_usage_plan KeyError
    @patch("lng_aws_clients.apigateway.get_client")
    def test_get_usage_plan_error_raises_internal_error_when_key_error_occurs(self, mock_apigateway):
        mock_apigateway.return_value.get_paginator.side_effect = KeyError(
            {'Error': {'Code': '500', 'Message': 'Key Error'}}, 'get_usage_plans')

        with self.assertRaisesRegex(InternalError, 'Key error while searching for usage plan'):
            get_usage_plan('DUMMY_USAGE_PLAN')

    # - get_usage_plan GeneralException
    @patch("lng_aws_clients.apigateway.get_client")
    def test_get_usage_plan_error_raises_internal_error_when_exception_error_occurs(self, mock_apigateway):
        mock_apigateway.return_value.get_paginator.side_effect = Exception(
            {'Error': {'Code': '500', 'Message': 'Error Getting Usage Plan'}}, 'General Exception Occurred')

        with self.assertRaisesRegex(InternalError, 'Unhandled exception occurred'):
            get_usage_plan('DUMMY_USAGE_PLAN')

    # + validate_owner_name
    def test_validate_owner_name(self):
        self.assertIsNone(validate_owner_name("dummy_owner"))

    # - validate_owner_name Only Digits
    def test_validate_owner_Invalid_name_digits(self):
        owner_name = "1234"
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid Owner Name {0}".format(owner_name)):
            validate_owner_name(owner_name)

    # - validate_owner_name Name too long
    def test_validate_owner_too_long_name_length(self):
        owner_name = "z"*61
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid Owner Name {0}".format(owner_name)):
            validate_owner_name(owner_name)

    # - validate_owner_name Name too short
    def test_validate_owner_too_short_name_length(self):
        owner_name = ""
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid Owner Name {0}".format(owner_name)):
            validate_owner_name(owner_name)

    # + validate_email_list
    def test_validate_email_list(self):
        test_emails = [
            "prettyandsimple@example.com",
            "very.common@example.com",
            "disposable.style.email.with+symbol@example.com",
            "other.email-with-dash@example.com",
            "fully-qualified-domain@example.com",
            "user.name+tag+sorting@example.com",
            "x@example.com (one-letter local-part)",
            "example-indeed@strange-example.com",
            "admin@mailserver1",
            "#!$%&'*+-/=?^_`{}|~@example.org",
            "example@s.solutions",
            "user@localserver",
            "user@[2001:DB8::1]"  # NOSONAR
        ]
        self.assertIsNone(validate_email_list(test_emails))

    # - validate_email_list raises InvalidPropertyRequestValue
    def test_validate_email_list_raises_exception_when_email_list_is_invalid(self):
        test_emails = ["emailexample.com", "example@email.com", "emailexample2.com"]

        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "The following emails are invalid \['emailexample.com', 'emailexample2.com'\]"):
            validate_email_list(test_emails)

    # + validate_collection_prefix
    def test_validate_collection_prefix(self):
        test_prefix1 = "A"
        test_prefix2 = "1A"
        test_prefix3 = "ABCDED"
        test_prefix4 = "123A"
        test_prefix5 = "A123S"
        test_prefix6 = "AA123"

        self.assertIsNone(validate_collection_prefix(test_prefix1))
        self.assertIsNone(validate_collection_prefix(test_prefix2))
        self.assertIsNone(validate_collection_prefix(test_prefix3))
        self.assertIsNone(validate_collection_prefix(test_prefix4))
        self.assertIsNone(validate_collection_prefix(test_prefix5))
        self.assertIsNone(validate_collection_prefix(test_prefix6))

    # - validate_collection_prefix raises InvalidPropertyRequestValue bad characters
    def test_validate_collection_prefix_raises_exception_regex(self):
        test_prefix1 = "123"
        test_prefix2 = "a-123"
        test_prefix3 = "a-b"

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Collection prefix {0}".format(test_prefix1)):
            validate_collection_prefix(test_prefix1)

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Collection prefix {0}".format(test_prefix2)):
            validate_collection_prefix(test_prefix2)

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Collection prefix {0}".format(test_prefix3)):
            validate_collection_prefix(test_prefix3)

    # - validate_collection_prefix raises InvalidPropertyRequestValue too long
    def test_validate_collection_prefix_raises_exception_length(self):
        test_prefix4 = "A12345678910"

        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid Collection prefix {0}".format(test_prefix4)):
            validate_collection_prefix(test_prefix4)

    # + validate_unique_collection_prefix
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_collection_prefix_success(self, mock_owner_query_items):
        mock_owner_query_items.return_value = []

        self.assertIsNone(validate_unique_collection_prefix("FPD"))

    # - validate_unique_collection_prefix : duplicate collection prefix
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_collection_prefix_duplicate_error_1(self, mock_owner_query_items):
        collection_prefix = "FPD"

        mock_owner_query_items.return_value = [
            {'owner-name': 'dummy_owner_name',
             'email-distribution': ['email1@lexisnexis.com', 'email2@lexisnexis.com'],
             'api-key': 'someKey',
             'owner-id': 39,
             'collection-prefix': collection_prefix}]

        with self.assertRaisesRegex(CollectionPrefixAlreadyExists,
                                    "Collection prefix already exists*"):
            validate_unique_collection_prefix(collection_prefix)

    # - validate_unique_collection_prefix client error
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_collection_prefix_client_error(self, mock_owner_query_items):
        mock_owner_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                          'Error': {
                                                              'Code': 'OTHER',
                                                              'Message': 'Unable to access OwnerTable"'}},
                                                         "FAKE")

        with self.assertRaisesRegex(InternalError, "Unable to query items from Owner Table"):
            validate_unique_collection_prefix("FPD")

    # - validate unique collection prefix  : raises exception
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_collection_prefix_exception(self, mock_owner_query_items):
        mock_owner_query_items.side_effect = Exception

        collection_prefix = "FPD"
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            validate_unique_collection_prefix(collection_prefix)

    # + validate_unique_owner
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_success(self, mock_owner_query_items):
        mock_owner_query_items.return_value = []
        self.assertIsNone(validate_unique_owner("dummy_owner_name", ["abc_123@lexisnexis.com"]))

    # + validate_unique_owner removed owner
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_success_removed(self, mock_owner_query_items):
        mock_owner_query_items.return_value = [{'owner-name': 'dummy_owner_name',
                                                'email-distribution': ['abc_123@lexisnexis.com'],
                                                'api-key-id': 'REMOVED'}]
        self.assertIsNone(validate_unique_owner("dummy_owner_name", ["abc_123@lexisnexis.com"]))

    # - validate_unique_owner : duplicate owner
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_duplicate_error(self, mock_owner_query_items):
        mock_owner_query_items.return_value = [
            {'owner-name': 'dummy_owner_name',
             'email-distribution': ['abc_123@lexisnexis.com', 'cde_123@lexisnexis.com'],
             'api-key-id': 'keykeykeykeykeykey',
             'owner-id': 39}]

        with self.assertRaisesRegex(ConflictingRequestError, "Owner already exists*"):
            validate_unique_owner("dummy_owner_name", ['abc_123@lexisnexis.com', 'cde_123@lexisnexis.com'])

    # - validate_unique_owner : raises client error
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_client_error(self, mock_owner_query_items):
        mock_owner_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                          'Error': {
                                                              'Code': 'OTHER',
                                                              'Message': 'Unable to access OwnerTable"'}},
                                                         "FAKE")

        with self.assertRaisesRegex(InternalError, "Unable to query items from Owner Table"):
            validate_unique_owner("dummy_owner_name", ['abc_123@lexisnexis.com', 'cde_123@lexisnexis.com'])

    # - validate_unique_owner : Unhandled exception
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_exception(self, mock_owner_query_items):
        mock_owner_query_items.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            validate_unique_owner("dummy_owner_name", ['abc_123@lexisnexis.com', 'cde_123@lexisnexis.com'])

    # - generate_json_response Missing required property
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_missing_required_property(self, mock_command_wrapper_get_optional,
                                                              mock_command_wrapper_get_required):
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json(
            "schema.optional_response_schema_keys.json")
        mock_command_wrapper_get_required.return_value = io_util.load_data_json(
            "schema.required_response_schema_keys.json")

        request = {
            "owner-id": 1006,
            "email-distribution": [
                "who@what.com"
            ]
        }

        with self.assertRaisesRegex(InternalError, "Missing required property:"):
            generate_json_response(request)

    # + generate_event_dict
    @patch('create_owner_command.command_wrapper.get_request_time')
    def test_generate_event_dict_with_collection_prefix(self, mock_get_time):
        mock_get_time.return_value = "2018-05-18T19:31:17.813Z"

        request = {
            "owner-id": 31,
            "owner-name": "Wormhole API Test",
            "email-distribution": ["wormholeapi@reedelsevier.com"],
            "collection-prefixes": ["FPD"],
            "api-key": "keykeykeykeykeykeykeykeykeykeykeykeykey",
            "api-key-id": "123456789"
        }
        expect_result = io_util.load_data_json(
            "event_with_prefix_valid.json")

        self.assertDictEqual(expect_result, generate_event_dict(request, "abcd", "LATEST"))


if __name__ == '__main__':
    unittest.main()
