import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_owner_event_handler import lambda_handler, create_owner_event_handler, get_api_key, insert_to_owner_table, \
    generate_owner_item

io_util = IOUtils(__file__, 'CreateOwnerEvent')


class TestCreateOwnerEventHandler(unittest.TestCase):

    # + lambda_handler - valid request
    @patch("create_owner_event_handler.create_owner_event_handler")
    def test_lambda_handler(self, handler_mock):
        handler_mock.return_value = event_handler_status.SUCCESS

        event = io_util.load_data_json("valid_event.json")
        self.assertEqual(lambda_handler(event, MockLambdaContext), event_handler_status.SUCCESS)

    # + create_owner_event_handler - inserts a valid object into the owner table
    @patch("create_owner_event_handler.insert_to_owner_table")
    @patch("create_owner_event_handler.generate_owner_item")
    @patch("create_owner_event_handler.get_api_key")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    def test_create_owner_event_handler_inserts_into_table_when_message_is_valid(self,
                                                                                 mock_extract_sns_message,
                                                                                 mock_get_api_key,
                                                                                 mock_owner_item_transform,
                                                                                 mock_insert_into_owner_table):
        mock_extract_sns_message.return_value = {
            "key": "event",
            "event-name": "Owner::Create",
            "event-version": 1,
            "stage": "LATEST",
            "api-key-id": "apiKeyId123"
        }
        mock_get_api_key.return_value = "testApiKey"
        mock_owner_item_transform.return_value = {
            "key": "owner"
        }

        event = {
            "key": "event",
            "event-name": "Owner::Create",
            "event-version": 1,
            "stage": "LATEST",
            "api-key-id": "apiKeyId123"
        }

        self.assertEqual(event_handler_status.SUCCESS,
                         create_owner_event_handler(event, MockLambdaContext().invoked_function_arn))

        mock_owner_item_transform.assert_called_with({
            "key": "event",
            "event-name": "Owner::Create",
            "event-version": 1,
            "stage": "LATEST",
            "api-key-id": "apiKeyId123",
            "api-key": "testApiKey"
        })

    # - create_owner_event_handler - raises a TerminalErrorException when sns_extractor doesn't return anything
    @patch("create_owner_event_handler.insert_to_owner_table")
    @patch("create_owner_event_handler.generate_owner_item")
    @patch("lng_aws_clients.apigateway.get_client")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    def test_create_owner_event_handler_raises_exception_when_message_is_invalid(self,
                                                                                 mock_extract_sns_message,
                                                                                 mock_get_api_key,
                                                                                 mock_owner_item_transform,
                                                                                 mock_insert_to_owner_table):
        mock_extract_sns_message.return_value = None
        event = {}

        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to extract event: {} or event-name doesnt match Owner::Create"):
            create_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        mock_get_api_key.assert_not_called()
        mock_owner_item_transform.assert_not_called()
        mock_insert_to_owner_table.assert_not_called()

    # - create_owner_event_handler - raises a TerminalErrorException when incorrect event version
    @patch("create_owner_event_handler.insert_to_owner_table")
    @patch("create_owner_event_handler.generate_owner_item")
    @patch("lng_aws_clients.apigateway.get_client")
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    @patch("lng_aws_clients.session.set_session")
    def test_create_owner_event_handler_raises_exception_when_message_is_invalid_version(self,
                                                                                         mock_set_session,
                                                                                         mock_extract_sns_message,
                                                                                         mock_valid_event_message,
                                                                                         mock_get_api_key,
                                                                                         mock_owner_item_transform,
                                                                                         mock_insert_to_owner_table):
        mock_extract_sns_message.return_value = None
        mock_valid_event_message.return_value = True

        event = {}
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to process event: {} event-version does not match 1"):
            create_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        mock_get_api_key.assert_not_called()
        mock_owner_item_transform.assert_not_called()
        mock_insert_to_owner_table.assert_not_called()

    # - create_owner_event_handler - raises a TerminalErrorException when incorrect stage
    @patch("create_owner_event_handler.insert_to_owner_table")
    @patch("create_owner_event_handler.generate_owner_item")
    @patch("lng_aws_clients.apigateway.get_client")
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    @patch("lng_aws_clients.session.set_session")
    def test_create_owner_event_handler_raises_exception_when_stage_is_invalid(self,
                                                                               mock_set_session,
                                                                               mock_extract_sns_message,
                                                                               mock_valid_event_message,
                                                                               mock_valid_event_version,
                                                                               mock_get_api_key,
                                                                               mock_owner_item_transform,
                                                                               mock_insert_to_owner_table):
        mock_extract_sns_message.return_value = None
        mock_set_session.return_value = None
        mock_valid_event_message.return_value = True
        mock_valid_event_version.return_value = True

        event = {}
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to process event: {} stage not match arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST"):
            create_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        mock_get_api_key.assert_not_called()
        mock_owner_item_transform.assert_not_called()
        mock_insert_to_owner_table.assert_not_called()

    # + get_api_key - Success
    @patch("lng_aws_clients.apigateway.get_client")
    def test_get_api_key_success(self, mock_apigateway):
        mock_apigateway.return_value.get_api_key.return_value = io_util.load_data_json(
            "apiclient.get_api_key_response.json")
        self.assertEqual("keykeykeykeykeykeykeykeykeykeykeykeykey", get_api_key("testApiKeyId"))

    # - get_api_key - ClientError
    @patch("lng_aws_clients.apigateway.get_client")
    def test_get_api_key_client_error(self, mock_apigateway):
        mock_apigateway.return_value.get_api_key.side_effect = ClientError(
            {"ResponseMetadata": {}, "Error": {"Code": "500", "Message": "Mock ClientError"}}, "get_api_key")
        with self.assertRaisesRegex(ClientError, "Mock ClientError"):
            get_api_key("testApiKeyId")

    # - get_api_key - Exception
    @patch("lng_aws_clients.apigateway.get_client")
    def test_get_api_key_exception(self, mock_apigateway):
        mock_apigateway.return_value.get_api_key.side_effect = Exception(
            {'Error': {'Code': '500', 'Message': 'Error Getting Api Key'}}, 'General Exception Occurred')
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Unhandled exception occurred getting API Key for api-key-id: testApiKeyId"):
            get_api_key("testApiKeyId")

    # + insert_to_owner_table - inserts valid object into OwnerTable
    @patch("lng_datalake_dal.owner_table.OwnerTable.put_item")
    @patch("create_owner_event_handler.logger")
    def test_insert_to_owner_table_calls_put_item(self, mock_logger, mock_put_item):
        owner = {
            "owner-id": 5,
            "event-version": 1,
            "owner-name": "My Owner Name",
            "email-distribution": [
                "email@address.com"
            ]
        }

        self.assertIsNone(insert_to_owner_table(owner))

        mock_logger.info.assert_called_once_with("Insertion to Owner table succeeded.")
        mock_put_item.assert_called_once_with(owner)

    # - insert_to_owner_table - when there is a SchemaValidationError throw a TerminalErrorException
    @patch("lng_datalake_dal.owner_table.OwnerTable.put_item")
    def test_insert_to_owner_table_calling_put_item_throws_schema_validation_exception(self, mock_put_item):
        mock_put_item.side_effect = SchemaValidationError({"'Error": {"Code": "500", "Message": "Error"}},
                                                          "Schema Validation")

        owner = {
            "owner-id": 5,
            "event-version": 1,
            "owner-name": "My Owner Name",
            "email-distribution": [
                "email@address.com"
            ]
        }
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into Owner table."):
            insert_to_owner_table(owner)

        mock_put_item.assert_called_once_with(owner)

    # - insert_to_owner_table - when there is a ConditionError throw a TerminalErrorException
    @patch("lng_datalake_dal.owner_table.OwnerTable.put_item")
    def test_insert_to_owner_table_calling_put_item_throws_condition_error(self, mock_put_item):
        mock_put_item.side_effect = ConditionError({"Error": {"Code": "500", "Message": "Error"}}, "Condition Error")

        owner = {
            "owner-id": 5,
            "event-version": 1,
            "owner-name": "My Owner Name",
            "email-distribution": [
                "email@address.com"
            ]
        }
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into Owner table."):
            insert_to_owner_table(owner)

        mock_put_item.assert_called_once_with(owner)

    # - insert_to_owner_table - when there is a SchemaError throw a TerminalErrorException
    @patch("lng_datalake_dal.owner_table.OwnerTable.put_item")
    def test_insert_to_owner_table_calling_put_item_throws_schema_error(self, mock_put_item):
        mock_put_item.side_effect = SchemaError({"Error": {"Code": "500", "Message": "Error"}}, "Schema Error")

        owner = {
            "owner-id": 5,
            "event-version": 1,
            "owner-name": "My Owner Name",
            "email-distribution": [
                "email@address.com"
            ]
        }
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into Owner table."):
            insert_to_owner_table(owner)

        mock_put_item.assert_called_once_with(owner)

    # - insert_to_owner_table - when there is a SchemaLoadError throw a TerminalErrorException
    @patch("lng_datalake_dal.owner_table.OwnerTable.put_item")
    def test_insert_to_owner_table_calling_put_item_throws_schema_load_error(self, mock_put_item):
        mock_put_item.side_effect = SchemaLoadError({"Error": {"Code": "500", "Message": "Error"}}, "Schema Load Error")

        owner = {
            "owner-id": 5,
            "event-version": 1,
            "owner-name": "My Owner Name",
            "email-distribution": [
                "email@address.com"
            ]
        }
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into Owner table."):
            insert_to_owner_table(owner)

        mock_put_item.assert_called_once_with(owner)

    # - insert_to_owner_table - when there is a EndpointConnectionError throw a RetryException
    @patch("lng_datalake_dal.owner_table.OwnerTable.put_item")
    def test_insert_to_owner_table_calling_put_item_throws_endpoint_connection_error_exception(self, mock_put_item):
        mock_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')

        owner = {
            "owner-id": 5,
            "event-version": 1,
            "owner-name": "My Owner Name",
            "email-distribution": [
                "email@address.com"
            ]
        }
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            insert_to_owner_table(owner)

        mock_put_item.assert_called_once_with(owner)

    # - insert_to_owner_table - when there is a ClientError throw a ClientError
    @patch("lng_datalake_dal.owner_table.OwnerTable.put_item")
    def test_insert_to_owner_table_calling_put_item_throws_client_error(self, mock_put_item):
        mock_put_item.side_effect = ClientError({"Error": {"Code": "500", "Message": "Error"}}, "Client Error")

        owner = {
            "owner-id": 5,
            "event-version": 1,
            "owner-name": "My Owner Name",
            "email-distribution": [
                "email@address.com"
            ]
        }
        with self.assertRaisesRegex(Exception,
                                    "An error occurred \(500\) when calling the Client Error operation: Error"):
            insert_to_owner_table(owner)

        mock_put_item.assert_called_once_with(owner)

    # - insert_to_owner_table - when there is any other exception throw Exception
    @patch("lng_datalake_dal.owner_table.OwnerTable.put_item")
    def test_insert_to_owner_table_calling_put_item_throws_exception(self, mock_put_item):
        mock_put_item.side_effect = Exception({"Error": {"Code": "500", "Message": "Error"}}, "put_item")

        owner = {
            "owner-id": 5,
            "event-version": 1,
            "owner-name": "My Owner Name",
            "email-distribution": [
                "email@address.com"
            ]
        }
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to insert item into Owner table due to unknown reasons."):
            insert_to_owner_table(owner)

        mock_put_item.assert_called_once_with(owner)

    # + generate_owner_item
    def test_generate_owner_item(self):
        message = io_util.load_data_json('create_owner_sns_message.json')
        expected_response = io_util.load_data_json('transformed_owner_valid.json')

        actual_response = generate_owner_item(message)

        self.assertDictEqual(expected_response, actual_response)

    # - generate_owner_item - KeyError
    def test_generate_owner_item_key_error(self):
        message = io_util.load_data_json('create_owner_sns_message.json')
        message.pop('owner-name')
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Missing required property: owner-name"):
            generate_owner_item(message)


if __name__ == '__main__':
    unittest.main()
