import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue
from lng_datalake_testhelper.io_utils import IOUtils

import list_assets_command

__author__ = "Prashant Srivastava, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListAssets')


class TestListAssets(unittest.TestCase):

    # + list_assets_command - successfully calls get_all_items on AssetTable, and gets next pagination token on
    # instance of the AssetTable
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.asset_table.AssetTable.get_all_items")
    @patch("list_assets_command.generate_json_response")
    def test_list_assets_command_success(self,
                                         mock_generate_json_response,
                                         mock_asset_table_get_all_items,
                                         mock_set_session):
        mock_set_session.return_value = None
        request = io_util.load_data_json("apigateway.request.valid.json")["request"]
        expected_table_data = io_util.load_data_json("dynamodb.response.transformed.valid.json")
        dummy_next_pagination_token = "eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiT3duZXJJRCI6IHsiTiI6ICIzIn19fQ=="
        mock_asset_table_get_all_items.return_value = expected_table_data
        expected_generated_response = {"assets": expected_table_data,
                                       "pagination-token": dummy_next_pagination_token}
        mock_generate_json_response.return_value = expected_generated_response
        response = list_assets_command.list_assets_command(request)
        self.assertDictEqual(expected_generated_response, response["response-dict"])

    # - list_assets_command - InternalError - Exception
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.asset_table.AssetTable.get_all_items")
    def test_list_owner_command_internal_error_1(self,
                                                 mock_asset_table_get_all_items,
                                                 mock_set_session):
        mock_set_session.return_value = None
        request = io_util.load_data_json("apigateway.request.valid.json")["request"]
        error_response = {"Error": {"Code": "500", "Message": "Unable to get data from Asset Table"}}
        exception = Exception(error_response, "get_all_items")
        mock_asset_table_get_all_items.side_effect = exception

        with self.assertRaises(InternalError) as context:
            list_assets_command.list_assets_command(request)

        expected_error = InternalError("Unhandled exception occurred", exception)
        self.assertTrue(isinstance(context.exception, InternalError))
        self.assertEqual(str(expected_error), str(context.exception))

    # - list_assets_command - InternalError - ClientError
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.asset_table.AssetTable.get_all_items")
    def test_list_owner_command_internal_error_2(self,
                                                 mock_asset_table_get_all_items,
                                                 mock_set_session):
        mock_set_session.return_value = None
        request = io_util.load_data_json("apigateway.request.valid.json")["request"]
        error_response = {"Error": {"Code": "500", "Message": "Unable to get data from Asset Table"}}
        exception = ClientError(error_response, "get_all_items")
        mock_asset_table_get_all_items.side_effect = exception

        with self.assertRaises(InternalError) as context:
            list_assets_command.list_assets_command(request)

        expected_error = InternalError("Unable to get items from Asset Table", exception)
        self.assertTrue(isinstance(context.exception, InternalError))
        self.assertEqual(str(expected_error), str(context.exception))

    # + generate_json_response - returns owners and pagination token
    def test_generate_json_response_success(self):
        list_of_assets = io_util.load_data_json("dynamodb.response.transformed.valid.json")
        pagination_token = "eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQXNzZXRJRCI6IHsiTiI6ICIyNyJ9fX0="
        expected_response = {
            "assets": list_of_assets,
            "item-count": 4,
            "next-token": "eyJtYXgtaXRlbXMiOjQsInBhZ2luYXRpb24tdG9rZW4iOiJleUpGZUdOc2RYTnBkbVZUZEdGeWRFdGxlU0k2SUhzaVFYTnpaWFJKUkNJNklIc2lUaUk2SUNJeU55SjlmWDA9In0="
        }
        response = list_assets_command.generate_json_response(list_of_assets, pagination_token, 4)
        self.assertEqual(expected_response, response)

    # + generate_json_response - returns owners only
    def test_generate_json_response_success_2(self):
        list_of_assets = io_util.load_data_json("dynamodb.response.transformed.valid.json")
        pagination_token = None
        expected_response = {
            "assets": list_of_assets,
            "item-count": 4
        }
        response = list_assets_command.generate_json_response(list_of_assets, pagination_token, 1000)
        self.assertEqual(expected_response, response)

    # -Failure test of Lambda for invalid max items
    @patch('lng_aws_clients.session.set_session')
    def test_list_owners_command_fail_invalid_max_results_2(self, session_mock):
        session_mock.return_value = None
        request_input = \
            {
                "collection-id": '274',
                "max-items": 1,
                "next-token": "eyJtYXgtaXRlbXMiOiAzLCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkIn0=",
            }
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Continuation request must pass the same max-items as initial request*"):
            list_assets_command.list_assets_command(request_input)


if __name__ == '__main__':
    unittest.main()
