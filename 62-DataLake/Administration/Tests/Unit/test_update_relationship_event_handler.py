import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import update_relationship_event_handler as update_module
from update_relationship_event_handler import update_to_relationship_owner_table, update_relationship_event_handler, \
    update_relationship_commitments, get_existing_relationship

io_utils = IOUtils(__file__, "UpdateRelationship")


class TestUpdateRelationshipEventHandler(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'TestArn'})
    def setUpClass(cls):  # NOSONAR
        reload(update_module)
        cls.arn = 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +Successful test: valid event with updated description only
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    @patch("update_relationship_event_handler.update_to_relationship_owner_table")
    @patch("update_relationship_event_handler.get_existing_relationship")
    def test_update_relationship_event_handler_valid_event(self, mock_get_rel, mocked_update_table,
                                                           mocked_set_session,
                                                           mocked_validation, mocked_sns_extractor):
        event = io_utils.load_data_json("valid_event.json")
        mocked_sns_extractor.return_value = io_utils.load_data_json("valid_deserialized_sns_message_1.json")
        mocked_validation.is_valid_event_message.return_value = True
        mocked_validation.is_valid_event_version.return_value = True
        mocked_validation.is_valid_event_stage.return_value = True
        mocked_set_session.return_value = None
        mocked_update_table.return_value = None
        mock_get_rel.return_value = {
            "relationship-id": "rel-01",
            "relationship-type": "Metadata",
            "owner-id": 2,
            "description": "test description"
        }

        self.assertEqual(
            update_relationship_event_handler(event, self.arn, MockLambdaContext.get_remaining_time_in_millis),
            event_handler_status.SUCCESS)

    #  - Retry Event
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    @patch("update_relationship_event_handler.update_relationship_commitments")
    @patch("lng_datalake_commons.error_handling.error_handler.set_message_additional_attribute")
    @patch("update_relationship_event_handler.get_existing_relationship")
    def test_create_relationship_event_handler_valid_event_with_commitments(self, mocked_get_rel, mocked_set_attribute,
                                                                            mocked_update,
                                                                            mocked_set_session, mocked_validation,
                                                                            mocked_sns_extractor):
        event = io_utils.load_data_json("valid_event_with_commitments.json")
        mocked_sns_extractor.return_value = io_utils.load_data_json("valid_deserialized_sns_message_2.json")
        mocked_validation.is_valid_event_message.return_value = True
        mocked_validation.is_valid_event_version.return_value = True
        mocked_validation.is_valid_event_stage.return_value = True
        mocked_set_session.return_value = None
        mocked_set_attribute.return_value = None
        index_coll_add = 1
        index_coll_del = 0
        mocked_update.return_value = index_coll_add, index_coll_del
        mocked_get_rel.return_value = {
            "relationship-id": "judges",
            "relationship-type": "Metadata",
            "owner-id": 2,
            "description": "test description"
        }
        with self.assertRaisesRegex(RetryEventException,
                                    "Retry event to finish the update with the unprocessed collection ids"):
            update_relationship_event_handler(event, self.arn, MockLambdaContext.get_remaining_time_in_millis)

    # -Failed test: invalid event name
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    def test_update_relationship_event_handler_invalid_event_name(self, mocked_set_session, mocked_validation,
                                                                  mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_name.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_validation.is_valid_event_message.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "event-name doesn't match"):
            update_relationship_event_handler(event, self.arn, MockLambdaContext.get_remaining_time_in_millis)

    # -Failed test: invalid event version
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    def test_update_relationship_event_handler_invalid_event_version(self, mocked_set_session, mocked_validation,
                                                                     mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_version.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_validation.is_valid_event_message.return_value = True
        mocked_validation.is_valid_event_version.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "event-version does not match"):
            update_relationship_event_handler(event, self.arn, MockLambdaContext.get_remaining_time_in_millis)

    # -Failed test: invalid event stage
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    def test_update_relationship_event_handler_invalid_event_stage(self, mocked_set_session, mocked_validation,
                                                                   mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_stage.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_validation.is_valid_event_message.return_value = True
        mocked_validation.is_valid_event_version.return_value = True
        mocked_validation.is_valid_event_stage.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "stage not match"):
            update_relationship_event_handler(event, self.arn, MockLambdaContext.get_remaining_time_in_millis)

    # + update relationship owner table
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.update_item")
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item")
    @patch("update_relationship_event_handler.logger")
    def test_update_to_relationship_owner_table_calling_update_item(self, mock_logger, mock_get_item, mock_update_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            'relationship-state': 'Created',
            "owner-id": 151,
            "description": "blah"
        }
        message = {
            "relationship-id": "newjudges",
            "description": "blah"
        }
        mock_get_item.return_value = relationship

        update_to_relationship_owner_table(message)

        mock_logger.info.assert_called_once_with(
            "Update for RelationshipId: {} was successful".format(message['relationship-id']))
        mock_update_item.assert_called_once_with(relationship)

    # - update relationship owner table schema validation error
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.update_item")
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item")
    def test_update_to_relationship_owner_table_calling_update_item_schema_validation_error(self, mock_get_item,
                                                                                            mock_update_item):
        message = {
            "relationship-id": "newjudges",
            "description": "blah"
        }
        error_response = {"'Error": {"Code": "500", "Message": "Error"}}
        exception = SchemaValidationError(error_response, "Schema Validation")
        mock_update_item.side_effect = exception
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151,
            "description": "blah"
        }
        mock_get_item.return_value = relationship

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Schema Validation") as context:
            update_to_relationship_owner_table(message)

    # - update relationship owner table terminal error
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.update_item")
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item")
    def test_update_to_relationship_owner_table_calling_update_item_terminal_error(self, mock_get_item,
                                                                                   mock_update_item):
        message = {
            "relationship-id": "newjudges",
            "description": "blah"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = ConditionError(error_response, "Condition Error")
        mock_update_item.side_effect = exception
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151,
            "description": "blah"
        }
        mock_get_item.return_value = relationship

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Condition Error") as context:
            update_to_relationship_owner_table(message)

    # - update relationship owner table schema error
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.update_item")
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item")
    def test_update_to_relationship_owner_table_calling_update_item_schema_error(self, mock_get_item, mock_update_item):
        message = {
            "relationship-id": "newjudges",
            "description": "blah"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = SchemaError(error_response, "Schema Error")
        mock_update_item.side_effect = exception
        relationship = {
            "relationship-id": "newjudges",
            "owner-id": 151,
            "relationship-type": "Metadata",
            'relationship-state': 'Created',
            "description": "blah"
        }
        mock_get_item.return_value = relationship

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Schema Error") as context:
            update_to_relationship_owner_table(message)

        mock_update_item.assert_called_once_with(relationship)
        expected_error_message = "TerminalErrorException||Failed to update item in RelationshipOwner table. " \
                                 "Item = {0} | Exception = {1}".format(relationship, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    # - update relationship owner table EndpointConnectionError
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.update_item")
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item")
    def test_update_to_relationship_owner_table_calling_update_item_endpoint_connection_error_exception(self,
                                                                                                        mock_get_item,
                                                                                                        mock_update_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151,
            "description": "blah"
        }
        mock_update_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')

        relationship = {
            "relationship-id": "newjudges",
            "owner-id": 151,
            "relationship-type": "Metadata",
            'relationship-state': 'Created',
            "description": "blah"
        }
        mock_get_item.return_value = relationship
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            update_to_relationship_owner_table(relationship)

    # - update relationship owner table client error
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.update_item")
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item")
    def test_update_to_relationship_owner_table_calling_update_item_client_error(self, mock_get_item, mock_update_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151,
            "description": "blah"
        }
        mock_update_item.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "client-error-mock")
        relationship = {
            "relationship-id": "newjudges",
            "owner-id": 151,
            "relationship-type": "Metadata",
            'relationship-state': 'Created',
            "description": "blah"
        }
        mock_get_item.return_value = relationship
        with self.assertRaisesRegex(ClientError, "client-error-mock"):
            update_to_relationship_owner_table(relationship)

    # - update relationship owner table exception
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.update_item")
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item")
    def test_update_to_relationship_owner_table_calling_update_item_exception(self, mock_get_item, mock_update_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }
        mock_update_item.side_effect = Exception
        relationship = {
            "relationship-id": "newjudges",
            "owner-id": 151,
            "relationship-type": "Metadata",
            'relationship-state': 'Created',
            "description": "blah"
        }
        mock_get_item.return_value = relationship

        # When
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to update item in RelationshipOwner table due to unknown reasons"):
            update_to_relationship_owner_table(relationship)

    # +Successful test: update CatalogCollectionMappingTable
    @patch('lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.batch_write_items')
    def test_update_catalog_collections_mapping(self, mock_batch_write_items):
        relationship_id = "rel101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        catalog_ids_to_add = ["1", "2", "3"]
        catalog_ids_to_delete = ["4", "5"]
        mock_batch_write_items.return_value = {}
        index_add, index_del = update_relationship_commitments(relationship_id, collection_ids_to_add,
                                                               collection_ids_to_delete, catalog_ids_to_add,
                                                               catalog_ids_to_delete, index_add=0, index_del=0)
        self.assertGreaterEqual(index_add, len(collection_ids_to_add))
        self.assertGreaterEqual(index_del, len(collection_ids_to_delete))

    # -Failed test: unprocessed items when updating the RelationshipCommitmentTable
    @patch('lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.batch_write_items')
    def test_update_relationship_collections_mapping_unprocessed_items(self, mock_batch_write_items):
        relationship_id = "rel101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        catalog_ids_to_add = ["1", "2", "3"]
        catalog_ids_to_delete = ["4", "5"]
        mock_batch_write_items.return_value = {'PutRequest': {'relationship-id': 'rel101', 'collection-id': '3'}}
        index_add, index_del = update_relationship_commitments(relationship_id, collection_ids_to_add,
                                                               collection_ids_to_delete, catalog_ids_to_add,
                                                               catalog_ids_to_delete, index_add=0, index_del=0)
        self.assertGreaterEqual(index_add, 0)
        self.assertGreaterEqual(index_del, 0)

    # -Failed test: EndpointConnectionError when updating RelationshipCommitmentTable with batch_write_items
    @patch('lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.batch_write_items')
    def test_update_relationship_collections_mapping_endpoint_connection_error_exception(self, mock_batch_write_items):
        relationship_id = "101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        catalog_ids_to_add = ["1", "2", "3"]
        catalog_ids_to_delete = ["4", "5"]
        mock_batch_write_items.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        index_add, index_del = update_relationship_commitments(relationship_id, collection_ids_to_add,
                                                               collection_ids_to_delete, catalog_ids_to_add,
                                                               catalog_ids_to_delete, index_add=0, index_del=0)
        self.assertGreaterEqual(index_add, 0)
        self.assertGreaterEqual(index_del, 0)

    # -Failed test: ClientError when updating RelationshipCommitmentTable with batch_write_items
    @patch('lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.batch_write_items')
    def test_update_relationship_collections_mapping_client_error(self, mock_batch_write_items):
        relationship_id = "101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        catalog_ids_to_add = ["1", "2", "3"]
        catalog_ids_to_delete = ["4", "5"]
        mock_batch_write_items.side_effect = ClientError({'ResponseMetadata': {},
                                                          'Error': {
                                                              'Code': 'Mock ClientError',
                                                              'Message': 'This is a mock'}},
                                                         "FAKE")
        index_add, index_del = update_relationship_commitments(relationship_id, collection_ids_to_add,
                                                               collection_ids_to_delete, catalog_ids_to_add,
                                                               catalog_ids_to_delete, index_add=0, index_del=0)
        self.assertGreaterEqual(index_add, 0)
        self.assertGreaterEqual(index_del, 0)

    # -Failed test: General Exception when updating RelationshipCommitmentTable with batch_write_items
    @patch('lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.batch_write_items')
    def test_update_relationship_collections_mapping_general_exception(self, mock_batch_write_items):
        relationship_id = "101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        catalog_ids_to_add = ["1", "2", "3"]
        catalog_ids_to_delete = ["4", "5"]
        mock_batch_write_items.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "General Exception Occurred"):
            update_relationship_commitments(relationship_id, collection_ids_to_add, collection_ids_to_delete,
                                            catalog_ids_to_add, catalog_ids_to_delete, index_add=0,
                                            index_del=0)

    # - relationship doesn't exist terminal error
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_existing_relationship_terminal(self, mock_get_item):
        mock_get_item.return_value = None
        rel = {"relationship-id": "blah"}
        with self.assertRaisesRegex(TerminalErrorException, "Can not update a non existent relationship"):
            get_existing_relationship(rel)

    # - relationship owner table EndpointConnectionError
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_existing_relationship_endpoint_connection_error_exception(self, mock_get_item):
        mock_get_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        rel = {"relationship-id": "blah"}
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            get_existing_relationship(rel)

    # - relationship owner table client error
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_existing_relationship_client(self, mock_get_item):
        mock_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                 'Error': {
                                                     'Code': 'Mock ClientError',
                                                     'Message': 'This is a mock'}},
                                                "FAKE")
        rel = {"relationship-id": "blah"}
        with self.assertRaisesRegex(ClientError, "ClientError"):
            get_existing_relationship(rel)

    # - relationship owner table terminal error
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_existing_relationship_bad_terminal(self, mock_get_item):
        mock_get_item.side_effect = Exception
        rel = {"relationship-id": "blah"}
        with self.assertRaisesRegex(TerminalErrorException, "Can not update a non existent"):
            get_existing_relationship(rel)


if __name__ == '__main__':
    unittest.main()
