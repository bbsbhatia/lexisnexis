import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, InvalidRequestPropertyName, \
    SemanticError, NoSuchRelationship
from lng_datalake_constants import event_names
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from update_relationship_command import get_relationship_by_id, validate_commitment_limit, transform_event_actions, \
    transform_patch_operations, generate_json_response, generate_event_dict, command_wrapper, lambda_handler, \
    transform_catalog_commitments, transform_collection_commitments

io_util = IOUtils(__file__, 'UpdateRelationship')


class TestUpdateRelationship(unittest.TestCase):

    # - Bad Decorator
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_aws_clients.session.set_session')
    def test_command_decorator_fail(self, session_mock, mock_track):
        session_mock.return_value = None
        mock_track.return_value = None
        request_input = io_util.load_data_json('apigateway.request.failure.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    # + valid lambda handler
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('update_relationship_command.update_relationship_command')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self, mock_session, mock_event_store_put_item, mock_update_relationship_command_response,
                            mock_tracking):
        mock_session.return_value = None
        mock_event_store_put_item.return_value = None
        update_relationship_command_response = io_util.load_data_json(
            'expected_create_relationship_response_valid.json')
        mock_update_relationship_command_response.return_value = update_relationship_command_response
        mock_tracking.return_value = None
        request_input = io_util.load_data_json('apigateway.request.json')
        expected_response = io_util.load_data_json('apigateway.response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response, lambda_handler(request_input, MockLambdaContext()))

    # + valid get relationship by id
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_relationship_by_id_valid(self, mock_session, mock_get):
        mock_session.return_value = None
        mock_get.return_value = {"relationship-id": "Ab1", "relationship-state": "Created"}
        self.assertEqual({"relationship-id": "Ab1", "relationship-state": "Created"}, get_relationship_by_id("Ab1"))

    # - invalid relationship id state
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_relationship_by_id_removed(self, mock_session, mock_get):
        mock_session.return_value = None
        mock_get.return_value = {"relationship-id": "Ab1", "relationship-state": "Removed"}
        with self.assertRaisesRegex(SemanticError, "Invalid Relationship ID  Ab1||"
                                                   "Relationship cannot be updated in Removed state"):
            get_relationship_by_id("Ab1")

    # - invalid relationship id existence
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_relationship_by_id_invalid(self, mock_session, mock_get):
        mock_session.return_value = None
        mock_get.return_value = None
        with self.assertRaisesRegex(NoSuchRelationship, "Invalid Relationship ID  Ab1||"
                                                        "Relationship ID does not exist"):
            get_relationship_by_id("Ab1")

    # - Unhandled error
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_relationship_by_id_internal_error(self, mock_session, mock_get):
        mock_session.return_value = None
        mock_get.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_relationship_by_id("Ab1")

    # - Client Error
    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_relationship_by_id_client_error(self, mock_session, mock_get):
        mock_session.return_value = None
        mock_get.side_effect = ClientError({'ResponseMetadata': {},
                                            'Error': {
                                                'Code': 'Unit Test',
                                                'Message': 'This is a mock'}},
                                           "FAKE")
        with self.assertRaisesRegex(InternalError,
                                    "InternalError||Unable to get item from Owner Table||An error occurred"
                                    "(Unit Test) when calling the FAKE operation: This is a mock"):
            get_relationship_by_id("Ab1")

    # - Too many commitments
    def test_invalid_commitment_limit(self):
        commitment_count = 501
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid number of Commitment IDs 501||"
                                                                 "Number of Commitment IDs should not exceed 500"):
            validate_commitment_limit(commitment_count)

    # + right at the limit
    def test_valid_commitment_limit(self):
        commitment_count = 500
        self.assertIsNone(validate_commitment_limit(commitment_count))

    # + Nothing to do
    def test_transform_event_actions(self):
        actions = {}
        add_catalog_ids = set()
        remove_catalog_ids = set()
        add_collection_ids = set()
        remove_collection_ids = set()

        self.assertIsNone(transform_event_actions(actions, add_catalog_ids, remove_catalog_ids, add_collection_ids,
                                                  remove_collection_ids))

    # + build the patch actions with data
    def test_build_actions_with_data(self):
        actions = {}
        add_catalog_ids = set("1")
        remove_catalog_ids = set("2")
        add_collection_ids = set("3")
        remove_collection_ids = set("4")
        done_actions = {'catalog-id-actions': {'add': ['1'], 'remove': ['2']},
                        'collection-id-actions': {'add': ['3'], 'remove': ['4']}}
        transform_event_actions(actions, add_catalog_ids, remove_catalog_ids, add_collection_ids,
                                remove_collection_ids)

        self.assertDictEqual(actions, done_actions)

    # + build actions with catalogs
    def test_build_actions_with_data_catalogs(self):
        actions = {}
        add_catalog_ids = set("1")
        remove_catalog_ids = set("2")
        add_collection_ids = set()
        remove_collection_ids = set()
        done_actions = {'catalog-id-actions': {'add': ['1'], 'remove': ['2']}}
        transform_event_actions(actions, add_catalog_ids, remove_catalog_ids, add_collection_ids,
                                remove_collection_ids)

        self.assertDictEqual(actions, done_actions)

    # + build actions with collections
    def test_build_actions_with_data_collections(self):
        actions = {}
        add_catalog_ids = set()
        remove_catalog_ids = set()
        add_collection_ids = set("3")
        remove_collection_ids = set("4")
        done_actions = {'collection-id-actions': {'add': ['3'], 'remove': ['4']}}
        transform_event_actions(actions, add_catalog_ids, remove_catalog_ids, add_collection_ids,
                                remove_collection_ids)

        self.assertDictEqual(actions, done_actions)

    # + test transform of actions
    def test_transform_patch_operations(self):
        patch_operations = [{"op": "remove", "path": "/catalog-commitments", "value": ["hello"]},
                            {"op": "remove", "path": "/collection-commitments", "value": ["hello"]},
                            {"op": "replace", "path": "/description", "value": "hello"}]

        actions = {'collection-id-actions': {'remove': ['hello']},
                   'catalog-id-actions': {'remove': ['hello']},
                   'description': 'hello'}
        self.assertDictEqual(actions, transform_patch_operations(patch_operations, {}))

    # + Test transform of actions with add
    # + No op for description that doesn't exist
    @patch('service_commons.relationship_command.validate_catalog_commitments')
    @patch('service_commons.relationship_command.validate_collection_commitments')
    @patch('lng_aws_clients.session.set_session')
    def test_transform_patch_operations_add(self, mock_session, mock_collection, mock_catalog):
        mock_session.return_value = None
        mock_collection.return_value = None
        mock_catalog.return_value = None
        patch_operations = [{"op": "add", "path": "/catalog-commitments", "value": ["hello"]},
                            {"op": "add", "path": "/collection-commitments", "value": ["hello"]},
                            {"op": "replace", "path": "/description", "value": "hello"}]

        actions = {'collection-id-actions': {'add': ['hello']},
                   'catalog-id-actions': {'add': ['hello']},
                   'description': 'hello'}
        no_description_actions = {'collection-id-actions': {'add': ['hello']},
                                  'catalog-id-actions': {'add': ['hello']}}
        self.assertDictEqual(actions, transform_patch_operations(patch_operations, {}))
        self.assertDictEqual(no_description_actions,
                             transform_patch_operations(patch_operations, {'description': 'hello'}))

    # + valid json response
    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    @patch("lng_datalake_commands.command_wrapper.get_optional_response_schema_keys")
    def test_generate_json_response_success(self,
                                            mock_command_wrapper_get_optional,
                                            mock_command_wrapper_get_required):
        rel_owner_resp = io_util.load_data_json("dynamodb.relationship_owner_response.transformed.valid.json")
        mock_command_wrapper_get_required.return_value = io_util.load_data_json(
            "schema.required_response_schema_keys.json")
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json(
            "schema.optional_response_schema_keys.json")
        expected_response = {'relationship-id': 'judges', 'owner-id': 151, 'relationship-type': 'Metadata',
                             "relationship-state": "Created"}
        response = generate_json_response(rel_owner_resp)
        self.assertDictEqual(expected_response, response)

    # - bad json properties
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_missing_required_property(self,
                                                              mock_command_wrapper_get_optional,
                                                              mock_command_wrapper_get_required):
        mock_command_wrapper_get_required.return_value = io_util.load_data_json(
            "schema.required_response_schema_keys.json")
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json(
            "schema.optional_response_schema_keys.json")

        request = {
            "relationship-id": "bad"
        }

        with self.assertRaisesRegex(InternalError, "Missing required property:"):
            generate_json_response(request)

    # + generate good event dict
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_event_dict(self, mock_request_time):
        mock_request_time.return_value = "2019-07-29T22:14:02.304Z"
        response_json = {'relationship-id': 'judges',
                         'owner-id': 2,
                         'relationship-type': 'Metadata',
                         'relationship-state': 'Created'}
        rel_id_actions = {'description': 'new description',
                          'catalog-id-actions': {'add': ["3"]},
                          'collection-id-actions': {'remove': ["4"]}}
        expected_event_dict = {'relationship-id': 'judges',
                               'request-time': "2019-07-29T22:14:02.304Z",
                               'event-name': event_names.RELATIONSHIP_UPDATE,
                               'event-id': 'request_id',
                               'event-version': 1,
                               'stage': 'stage',
                               'description': 'new description',
                               'catalog-id-actions': {'add': ["3"]},
                               'collection-id-actions': {'remove': ["4"]}}

        self.assertDictEqual(expected_event_dict, generate_event_dict(response_json,
                                                                      rel_id_actions,
                                                                      "request_id",
                                                                      "stage"))

    # + good event dictionary
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_event_dict_description(self, mock_request_time):
        mock_request_time.return_value = "2019-07-29T22:14:02.304Z"
        response_json = {'relationship-id': 'judges',
                         'owner-id': 2,
                         'description': 'old description',
                         'relationship-type': 'Metadata',
                         'relationship-state': 'Created'}
        rel_id_actions = {'catalog-id-actions': {'add': ["3"]},
                          'collection-id-actions': {'remove': ["4"]}}
        expected_event_dict = {'relationship-id': 'judges',
                               'request-time': "2019-07-29T22:14:02.304Z",
                               'event-name': event_names.RELATIONSHIP_UPDATE,
                               'event-id': 'request_id',
                               'event-version': 1,
                               'stage': 'stage',
                               'description': 'old description',
                               'catalog-id-actions': {'add': ["3"]},
                               'collection-id-actions': {'remove': ["4"]}}

        self.assertDictEqual(expected_event_dict, generate_event_dict(response_json,
                                                                      rel_id_actions,
                                                                      "request_id",
                                                                      "stage"))

    # + valid catalogs to remove
    def test_transform_catalog_commitments(self):
        patch_operation = {"op": "remove", "path": "/catalog-commitments", "value": ["hello", "hello1"]}
        empty_add_set = set()
        empty_remove_set = set()
        expected_add_set = set()
        expected_remove_set = {"hello", "hello1"}
        total_commitments = 0
        expected_total_commitments = 2
        self.assertEqual(expected_total_commitments,
                         transform_catalog_commitments(total_commitments, patch_operation, empty_add_set,
                                                       empty_remove_set))
        self.assertEqual(empty_remove_set, expected_remove_set)
        self.assertEqual(empty_add_set, expected_add_set)

    # + valid catalogs to add
    @patch('service_commons.relationship_command.validate_catalog_commitments')
    @patch('lng_aws_clients.session.set_session')
    def test_transform_catalog_commitments_add(self, mock_session, mock_catalogs):
        mock_session.return_value = None
        mock_catalogs.return_value = None
        patch_operation = {"op": "add", "path": "/catalog-commitments", "value": ["hello", "hello1"]}
        empty_add_set = set()
        empty_remove_set = set()
        expected_add_set = {"hello", "hello1"}
        expected_remove_set = set()
        total_commitments = 0
        expected_total_commitments = 2
        self.assertEqual(expected_total_commitments,
                         transform_catalog_commitments(total_commitments, patch_operation, empty_add_set,
                                                       empty_remove_set))
        self.assertEqual(empty_remove_set, expected_remove_set)
        self.assertEqual(empty_add_set, expected_add_set)

    # + valid collections to remove
    def test_transform_collection_commitments(self):
        patch_operation = {"op": "remove", "path": "/collection-commitments", "value": ["hello", "hello1"]}
        empty_add_set = set()
        empty_remove_set = set()
        expected_add_set = set()
        expected_remove_set = {"hello", "hello1"}
        total_commitments = 0
        expected_total_commitments = 2
        self.assertEqual(expected_total_commitments,
                         transform_collection_commitments(total_commitments, patch_operation, empty_add_set,
                                                          empty_remove_set))
        self.assertEqual(empty_remove_set, expected_remove_set)
        self.assertEqual(empty_add_set, expected_add_set)

    # + valid collections to add
    @patch('service_commons.relationship_command.validate_collection_commitments')
    @patch('lng_aws_clients.session.set_session')
    def test_transform_collection_commitments_add(self, mock_session, mock_collections):
        mock_session.return_value = None
        mock_collections.return_value = None
        patch_operation = {"op": "add", "path": "/collection-commitments", "value": ["hello", "hello1"]}
        empty_add_set = set()
        empty_remove_set = set()
        expected_add_set = {"hello", "hello1"}
        expected_remove_set = set()
        total_commitments = 0
        expected_total_commitments = 2
        self.assertEqual(expected_total_commitments,
                         transform_collection_commitments(total_commitments, patch_operation, empty_add_set,
                                                          empty_remove_set))
        self.assertEqual(empty_remove_set, expected_remove_set)
        self.assertEqual(empty_add_set, expected_add_set)


if __name__ == '__main__':
    unittest.main()
