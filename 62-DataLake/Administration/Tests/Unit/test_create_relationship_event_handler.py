import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_relationship_event_handler import insert_to_relationship_commitment_table, \
    insert_to_relationship_owner_table, create_relationship_event_handler, \
    create_catalog_commitments, create_collection_commitments

io_utils = IOUtils(__file__, "CreateRelationship")


class TestCreateRelationshipEventHandler(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_all_create_relationship_event_handler(self, mock_commit_put_item, mocked_rel_put_item, mocked_set_session,
                                                   mock_validate,
                                                   ):
        event = io_utils.load_data_json("valid_event.json")
        mock_commit_put_item.return_value = None
        mocked_rel_put_item.return_value = None
        mocked_set_session.return_value = None
        mock_validate.return_value = True

        self.assertEqual(
            create_relationship_event_handler(event,
                                              'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'),
            event_handler_status.SUCCESS)

        event = io_utils.load_data_json("invalid_event.json")
        self.assertRaisesRegex(TerminalErrorException, "No message exists in SNS Notification",
                               create_relationship_event_handler, event,
                               'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST')

    # - create_relationship_event_handler - raises a TerminalErrorException when incorrect message
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    def test_create_relationship_event_handler_raises_exception_when_message_is_invalid(self,
                                                                                        mock_extract_sns_message,
                                                                                        mock_set_session):
        # Given
        mock_extract_sns_message.return_value = None
        mock_set_session.return_value = None
        event = {}

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Failed to extract event") as context:
            create_relationship_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        expected_error_message = "Failed to extract event: {} or event-name doesnt match {}".format(event,
                                                                                                    "Relationship::Create")
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    # - create_relationship_event_handler - raises a TerminalErrorException when incorrect event version
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    def test_create_relationship_event_handler_raises_exception_when_message_is_invalid_version(self,
                                                                                                mock_extract_sns_message,
                                                                                                mock_set_session,
                                                                                                mock_valid_event_message):
        # Given
        mock_extract_sns_message.return_value = None
        mock_set_session.return_value = None
        mock_valid_event_message.return_value = True
        event = {}

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Failed to process event") as context:
            create_relationship_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        expected_error_message = "Failed to process event: {} event-version does not match {}".format(event, "1")
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    def test_create_relationship_event_handler_raises_exception_when_event_is_invalid(self, mock_extract_sns_message,
                                                                                      mock_set_session,
                                                                                      mock_valid_event_message,
                                                                                      mock_valid_event_version):
        mock_extract_sns_message.return_value = None
        mock_set_session.return_value = None
        mock_valid_event_message.return_value = True
        mock_valid_event_version.return_value = True
        event = {}

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Failed to process event:") as context:
            create_relationship_event_handler(event,
                                              MockLambdaContext().invoked_function_arn)

        # Then
        expected_error_message = "Failed to process event: {} stage not match {}".format(event,
                                                                                         MockLambdaContext().
                                                                                         invoked_function_arn)
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    @patch("create_relationship_event_handler.logger")
    def test_insert_to_rel_owner_table_calling_put_item(self, mock_logger, mock_put_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }

        self.assertIsNone(insert_to_relationship_owner_table(relationship))

        mock_logger.info.assert_called_once_with("Insertion to RelationshipOwner table succeeded.")
        mock_put_item.assert_called_once_with(relationship)

    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    def test_insert_to_rel_owner_table_calling_put_item_throws_schema_validation_exception(self, mock_put_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }
        error_response = {"'Error": {"Code": "500", "Message": "Error"}}
        exception = SchemaValidationError(error_response, "Schema Validation")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Schema Validation") as context:
            insert_to_relationship_owner_table(relationship)

        # Then
        mock_put_item.assert_called_once_with(relationship)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipOwner table. " \
                                 "Item = {0} | Exception = {1}".format(relationship, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    def test_insert_to_rel_owner_table_calling_put_item_throws_condition_error(self, mock_put_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = ConditionError(error_response, "Condition Error")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Condition Error") as context:
            insert_to_relationship_owner_table(relationship)

        # Then
        mock_put_item.assert_called_once_with(relationship)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipOwner table. " \
                                 "Item = {0} | Exception = {1}".format(relationship, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    def test_insert_to_rel_owner_table_calling_put_item_throws_schema_error(self, mock_put_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = SchemaError(error_response, "Schema Error")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Schema Error") as context:
            insert_to_relationship_owner_table(relationship)

        # Then
        mock_put_item.assert_called_once_with(relationship)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipOwner table. " \
                                 "Item = {0} | Exception = {1}".format(relationship, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    def test_insert_to_rel_owner_table_calling_put_item_throws_schema_load_error(self, mock_put_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = SchemaLoadError(error_response, "Schema Load Error")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Schema Load Error") as context:
            insert_to_relationship_owner_table(relationship)

        # Then
        mock_put_item.assert_called_once_with(relationship)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipOwner table. " \
                                 "Item = {0} | Exception = {1}".format(relationship, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    def test_insert_to_rel_owner_table_calling_put_item_throws_endpoint_connection_error_exception(self, mock_put_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"') as context:
            insert_to_relationship_owner_table(relationship)

        # Then
        mock_put_item.assert_called_once_with(relationship)
        expected_error_message = "{0}".format(exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    def test_insert_to_rel_owner_table_calling_put_item_throws_client_error(self, mock_put_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = ClientError(error_response, "Client Error")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(Exception, "Client Error") as context:
            insert_to_relationship_owner_table(relationship)

        # Then
        mock_put_item.assert_called_once_with(relationship)
        expected_error_message = "{0}".format(exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.put_item")
    def test_insert_to_rel_owner_table_calling_put_item_throws_exception(self, mock_put_item):
        relationship = {
            "relationship-id": "newjudges",
            "relationship-type": "Metadata",
            "owner-id": 151
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = Exception(error_response, "put_item")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into RelationshipOwner") as context:
            insert_to_relationship_owner_table(relationship)

        # Then
        mock_put_item.assert_called_once_with(relationship)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipOwner " \
                                 "table due to unknown reasons. Item = {0} | Exception = {1}".format(relationship,
                                                                                                     exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    @patch("create_relationship_event_handler.logger")
    def test_insert_to_rel_comm_table_calling_put_item(self, mock_logger, mock_put_item):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|collectionID",
            "target-type": "Collection"
        }

        self.assertIsNone(insert_to_relationship_commitment_table(commitment))
        mock_put_item.assert_called_once_with(commitment)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_insert_to_rel_comm_table_calling_put_item_throws_schema_validation_exception(self, mock_put_item):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|collectionID",
            "target-type": "Collection"
        }
        error_response = {"'Error": {"Code": "500", "Message": "Error"}}
        exception = SchemaValidationError(error_response, "Schema Validation")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Schema Validation") as context:
            insert_to_relationship_commitment_table(commitment)

        # Then
        mock_put_item.assert_called_once_with(commitment)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipCommitmentTable table. " \
                                 "Item = {0} | Exception = {1}".format(commitment, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_insert_to_rel_comm_table_calling_put_item_throws_condition_error(self, mock_put_item):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|collectionID",
            "target-type": "Collection"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = ConditionError(error_response, "Condition Error")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Condition Error") as context:
            insert_to_relationship_commitment_table(commitment)

        # Then
        mock_put_item.assert_called_once_with(commitment)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipCommitmentTable table. " \
                                 "Item = {0} | Exception = {1}".format(commitment, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_insert_to_rel_comm_table_calling_put_item_throws_schema_error(self, mock_put_item):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|collectionID",
            "target-type": "Collection"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = SchemaError(error_response, "Schema Error")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Schema Error") as context:
            insert_to_relationship_commitment_table(commitment)

        # Then
        mock_put_item.assert_called_once_with(commitment)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipCommitmentTable table. " \
                                 "Item = {0} | Exception = {1}".format(commitment, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_insert_to_rel_comm_table_calling_put_item_throws_schema_load_error(self, mock_put_item):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|collectionID",
            "target-type": "Collection"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = SchemaLoadError(error_response, "Schema Load Error")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException, "Schema Load Error") as context:
            insert_to_relationship_commitment_table(commitment)

        # Then
        mock_put_item.assert_called_once_with(commitment)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipCommitmentTable table. " \
                                 "Item = {0} | Exception = {1}".format(commitment, exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_insert_to_rel_comm_table_calling_put_item_throws_endpoint_connection_error_exception(self, mock_put_item):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|collectionID",
            "target-type": "Collection"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"') as context:
            insert_to_relationship_commitment_table(commitment)

        # Then
        mock_put_item.assert_called_once_with(commitment)
        expected_error_message = "{0}".format(exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_insert_to_rel_comm_table_calling_put_item_throws_client_error(self, mock_put_item):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|collectionID",
            "target-type": "Collection"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = ClientError(error_response, "Client Error")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(Exception, "Client Error") as context:
            insert_to_relationship_commitment_table(commitment)

        # Then
        mock_put_item.assert_called_once_with(commitment)
        expected_error_message = "{0}".format(exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_insert_to_rel_comm_table_calling_put_item_throws_exception(self, mock_put_item):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|collectionID",
            "target-type": "Collection"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = Exception(error_response, "put_item")
        mock_put_item.side_effect = exception

        # When
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to insert item into RelationshipCommitmentTable") as context:
            insert_to_relationship_commitment_table(commitment)

        # Then
        mock_put_item.assert_called_once_with(commitment)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipCommitmentTable " \
                                 "table due to unknown reasons. Item = {0} | Exception = {1}".format(commitment,
                                                                                                     exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_create_catalog_commitments_terminal(self, mock_put):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "catalog|blah",
            "target-type": "Catalog"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = Exception(error_response, "put_item")
        mock_put.side_effect = exception
        message = {'relationship-id': 'newjudges', 'catalog-commitments': ['blah']}
        # When
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to insert item into RelationshipCommitmentTable") as context:
            create_catalog_commitments(message)

        # Then
        mock_put.assert_called_once_with(commitment)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipCommitmentTable " \
                                 "table due to unknown reasons. Item = {0} | Exception = {1}".format(commitment,
                                                                                                     exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_create_collection_commitments_terminal(self, mock_put):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|blah",
            "target-type": "Collection"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = Exception(error_response, "put_item")
        mock_put.side_effect = exception
        message = {'relationship-id': 'newjudges', 'collection-commitments': ['blah']}
        # When
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Failed to insert item into RelationshipCommitmentTable") as context:
            create_collection_commitments(message)

        # Then
        mock_put.assert_called_once_with(commitment)
        expected_error_message = "TerminalErrorException||Failed to insert item into RelationshipCommitmentTable " \
                                 "table due to unknown reasons. Item = {0} | Exception = {1}".format(commitment,
                                                                                                     exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_create_collection_commitments_endpoint_connection_error_exception(self, mock_put):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|blah",
            "target-type": "Collection"
        }

        exception = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        mock_put.side_effect = exception
        message = {'relationship-id': 'newjudges', 'collection-commitments': ['blah']}
        # When
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"') as context:
            create_collection_commitments(message)

        # Then
        mock_put.assert_called_once_with(commitment)
        expected_error_message = "{0}".format(exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_create_collection_commitments_client(self, mock_put):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "collection|blah",
            "target-type": "Collection"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = ClientError(error_response, "Client Error")
        mock_put.side_effect = exception
        message = {'relationship-id': 'newjudges', 'collection-commitments': ['blah']}
        # When
        with self.assertRaisesRegex(ClientError, "Client Error") as context:
            create_collection_commitments(message)

        # Then
        mock_put.assert_called_once_with(commitment)
        expected_error_message = "{0}".format(exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_create_catalog_commitments_endpoint_connection_error_exception(self, mock_put):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "catalog|blah",
            "target-type": "Catalog"
        }

        exception = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        mock_put.side_effect = exception
        message = {'relationship-id': 'newjudges', 'catalog-commitments': ['blah']}
        # When
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"') as context:
            create_catalog_commitments(message)

        # Then
        mock_put.assert_called_once_with(commitment)
        expected_error_message = "{0}".format(exception)
        self.assertEqual(str(context.exception), expected_error_message)

    @patch("lng_datalake_dal.relationship_commitment_table.RelationshipCommitmentTable.put_item")
    def test_create_catalog_commitments_client(self, mock_put):
        commitment = {
            "relationship-id": "newjudges",
            "target-composite-key": "catalog|blah",
            "target-type": "Catalog"
        }
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = ClientError(error_response, "Client Error")
        mock_put.side_effect = exception
        message = {'relationship-id': 'newjudges', 'catalog-commitments': ['blah']}
        # When
        with self.assertRaisesRegex(ClientError, "Client Error") as context:
            create_catalog_commitments(message)

        # Then
        mock_put.assert_called_once_with(commitment)
        expected_error_message = "{0}".format(exception)
        self.assertEqual(str(context.exception), expected_error_message)


if __name__ == '__main__':
    unittest.main()
