import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import NoSuchOwner
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_owner_command import get_owner_command, lambda_handler

__author__ = "Chuck Nelson"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetOwner')


class TestGetOwner(unittest.TestCase):

    # + get_owner_command - validates an owner is returned
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    @patch("lng_aws_clients.session.set_session")
    def test_get_owner_valid(self, session_mock, owner_mock):
        session_mock.return_value = None
        owner_mock.return_value = \
            {
                "owner-id": 1,
                "owner-name": "partyTime",
                "email-distribution": "mitchaja@legal.regn.net"
            }
        request_input = \
            {
                "owner-id": 1
            }
        expected_response = \
            {'response-dict':
                {
                    'owner-id': 1,
                    'owner-name': 'partyTime',
                    'email-distribution': "mitchaja@legal.regn.net"
                }
            }
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetOwnerCommand-ResponseSchema.json')):
            self.assertDictEqual(expected_response, get_owner_command(request_input))

    # - get_owner_command - no owner returned
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    @patch("lng_aws_clients.session.set_session")
    def test_get_owner_invalid(self, session_mock, owner_mock):
        session_mock.return_value = None
        owner_mock.return_value = {}
        request_input = \
            {
                "owner-id": 1
            }
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetOwnerCommand-ResponseSchema.json')):
            with self.assertRaisesRegex(NoSuchOwner, 'Invalid Owner ID'):
                get_owner_command(request_input)

    # + lambda_handler - cover lambda_handler lines of code
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch("get_owner_command.get_owner_command")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_valid(self, session_mock,
                                  get_owner_command_mock):
        session_mock.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path

        request_input = io_util.load_data_json('apigateway.request_valid.json')
        get_owner_command_mock.return_value = io_util.load_data_json('get_owner_command-response.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()), expected_response)


if __name__ == '__main__':
    unittest.main()
