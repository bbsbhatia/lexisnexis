import json
import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants.event_handler_status import SUCCESS
from lng_datalake_constants.event_names import OWNER_UPDATE
from lng_datalake_dal.exceptions import ConditionError, SchemaError, SchemaLoadError, SchemaValidationError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import update_owner_event_handler

io_utils = IOUtils(__file__, 'UpdateOwnerEventHandler')


class TestUpdateOwnerEventHandler(unittest.TestCase):

    # + test that update_owner_event_handler creates an owner object and updates owner table
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    @patch("update_owner_event_handler.update_owner_table")
    def test_update_owner_event_handler_calls_update_owner_table(self, mock_update_owner_table,
                                                                 mock_get_owner):
        # Given
        msg = io_utils.load_data_json('valid_deserialized_sns_message.json')
        event = {
            "Records": [{
                "Sns": {
                    "Message": json.dumps(msg)
                }
            }]
        }

        context = MockLambdaContext()
        mock_get_owner.return_value = io_utils.load_data_json('valid_owner_mock_return.json')

        # When
        result = update_owner_event_handler.update_owner_event_handler(event, context.invoked_function_arn)

        # Then
        self.assertEqual(result, SUCCESS)
        mock_update_owner_table.assert_called_once_with(mock_get_owner.return_value)

    # - update_owner_event_handler - raises a TerminalErrorException when is_valid_event_message is false
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_update_owner_event_handler_raises_exception_when_message_is_invalid(self,
                                                                                 mock_is_valid_event_message,
                                                                                 mock_extract_sns_message,
                                                                                 mock_set_session):
        # Given
        event = {"event-name": "Owner::Create"}
        mock_extract_sns_message.return_value = event
        mock_is_valid_event_message.return_value = False

        # When
        with self.assertRaises(TerminalErrorException) as context:
            update_owner_event_handler.update_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        mock_is_valid_event_message.assert_called_once_with(event, OWNER_UPDATE)
        expected_error_message = "Failed to extract event: {} or event-name doesnt match {}".format(event, OWNER_UPDATE)
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    # - test update_owner_event_handler throws TerminalErrorException for invalid event version
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    def test_update_owner_table_throws_terminal_error_exception_for_invalid_event_error(self, mock_validate,
                                                                                        mock_set_session):
        # Given
        msg = io_utils.load_data_json('valid_deserialized_sns_message.json')
        event = {
            "Records": [{
                "Sns": {
                    "Message": json.dumps(msg)
                }
            }]
        }

        context = MockLambdaContext()
        mock_validate.return_value = False

        # When
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to process event:.* event-version does not match'):
            update_owner_event_handler.update_owner_event_handler(event, context.invoked_function_arn)

    # - update_owner_event_handler - raises a TerminalErrorException when incorrect stage
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch("lng_aws_clients.session.set_session")
    @patch("lng_datalake_commons.sns_extractor.extract_sns_message")
    def test_update_owner_event_handler_raises_exception_when_stage_is_invalid(self, mock_extract_sns_message,
                                                                               mock_set_session,
                                                                               mock_valid_event_message,
                                                                               mock_valid_event_version):
        # Given
        mock_extract_sns_message.return_value = None
        mock_valid_event_message.return_value = True
        mock_valid_event_version.return_value = True
        event = {}

        # When
        with self.assertRaises(TerminalErrorException) as context:
            update_owner_event_handler.update_owner_event_handler(event, MockLambdaContext().invoked_function_arn)

        # Then
        expected_error_message = "Failed to process event: {} stage not match {}".format(event,
                                                                                         MockLambdaContext().
                                                                                         invoked_function_arn)
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(context.exception), str(expected_error))

    # - test update_owner_event_handler throws TerminalErrorException for failed to update
    @patch("service_commons.owner_event_handler.initialize_dict_with_keys")
    @patch("service_commons.owner_event_handler.get_owner_from_owner_table")
    @patch("lng_aws_clients.session.set_session")
    def test_update_owner_table_throws_terminal_error_for_map_empty(self, mock_set_session, mock_get_owner,
                                                                    mock_dict_with_keys):
        mock_set_session.return_value = None
        # Given
        msg = io_utils.load_data_json('valid_deserialized_sns_message.json')
        event = {
            "Records": [{
                "Sns": {
                    "Message": json.dumps(msg)
                }
            }]
        }
        context = MockLambdaContext()
        mock_get_owner.return_value = io_utils.load_data_json('valid_owner_mock_return.json')
        mock_dict_with_keys.return_value = {}

        # When
        with self.assertRaisesRegex(TerminalErrorException, "TerminalErrorException||Failed to update *"):
            update_owner_event_handler.update_owner_event_handler(event, context.invoked_function_arn)

    # + test update_owner_table calls OwnerTable with put_item
    @patch("update_owner_event_handler.logger")
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_update_owner_table_calls_update_item(self, mock_owner_table_update_item, mock_logger):
        # Given
        owner = io_utils.load_data_json('valid_owner_mock_return.json')

        # When
        result = update_owner_event_handler.update_owner_table(owner)

        # Then
        expected_success_message = "Update for OwnerId: {} was successful".format(owner["owner-id"])
        mock_logger.info.assert_called_once_with(expected_success_message)
        mock_owner_table_update_item.assert_called_once_with(owner)
        self.assertIsNone(result)

    # - test update_owner_table throws TerminalErrorException for given exceptions
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_update_owner_table_throws_terminal_error_exception_for_condition_error(self, mock_update_item):
        # Given
        owner = io_utils.load_data_json('valid_owner_mock_return.json')
        for side_effect in [ConditionError(), SchemaError(), SchemaLoadError(), SchemaValidationError()]:
            mock_update_item.side_effect = side_effect
            # When
            with self.assertRaises(TerminalErrorException) as context:
                update_owner_event_handler.update_owner_table(owner)

            # Then
            expected_error_message = "Failed to update item in Owner table. Item = {0} | Exception = {1}" \
                .format(owner, mock_update_item.side_effect)
            expected_error = TerminalErrorException(expected_error_message)
            self.assertEqual(str(expected_error), str(context.exception))

    # - test update_owner_table throws TerminalErrorException for general exception
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_update_owner_table_throws_terminal_error_exception_for_general_exception(self, mock_update_item):
        # Given
        owner = io_utils.load_data_json('valid_owner_mock_return.json')
        mock_update_item.side_effect = Exception()

        # When
        with self.assertRaises(TerminalErrorException) as context:
            update_owner_event_handler.update_owner_table(owner)

        # Then
        expected_error_message = "Failed to update item in Owner table due to unknown reasons. Item = {0} | " \
                                 "Exception = {1}".format(owner, mock_update_item.side_effect)
        expected_error = TerminalErrorException(expected_error_message)
        self.assertEqual(str(expected_error), str(context.exception))

    # - update_owner_table - when there is a EndpointConnectionError
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_update_owner_table_throws_endpoint_connection_error_exception(self,
                                                                           mock_update_item):
        # Given
        owner = io_utils.load_data_json('valid_owner_mock_return.json')
        exception = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        mock_update_item.side_effect = exception

        # When
        with self.assertRaises(EndpointConnectionError) as context:
            update_owner_event_handler.update_owner_table(owner)

        # Then
        expected_error_message = 'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'
        self.assertEqual(expected_error_message, str(context.exception))

    # - update_owner_table - when there is a ClientError
    @patch("lng_datalake_dal.owner_table.OwnerTable.update_item")
    def test_update_owner_table_throws_client_error_exception(self,
                                                              mock_update_item):
        # Given
        owner = io_utils.load_data_json('valid_owner_mock_return.json')
        error_response = {"Error": {"Code": "500", "Message": "Error"}}
        exception = ClientError(error_response, "Client Error")
        mock_update_item.side_effect = exception

        # When
        with self.assertRaisesRegex(ClientError, "Client Error") as context:
            update_owner_event_handler.update_owner_table(owner)

        # Then
        expected_error_message = "{0}".format(exception)
        self.assertEqual(expected_error_message, str(context.exception))


if __name__ == '__main__':
    unittest.main()
