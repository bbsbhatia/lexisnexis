import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import NoSuchAsset, InternalError
from lng_datalake_testhelper.io_utils import IOUtils

from get_asset_command import get_asset_command, generate_response_json

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetAsset')


class TestGetAsset(unittest.TestCase):

    # + get_asset_command - validates an asset is returned
    @patch("lng_datalake_dal.asset_table.AssetTable.get_item")
    @patch("lng_aws_clients.session.set_session")
    def test_get_asset_valid(self, session_mock, asset_mock):
        session_mock.return_value = None
        asset_mock.return_value = \
            {
                "asset-id": 1,
                "asset-name": "partyTime"
            }
        request_input = \
            {
                "asset-id": 1
            }
        expected_response = \
            {'response-dict': {'asset-id': 1, 'asset-name': 'partyTime'}}
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetAssetCommand-ResponseSchema.json')):
            self.assertDictEqual(expected_response, get_asset_command(request_input))

    # - get_asset_command - no asset returned
    @patch("lng_datalake_dal.asset_table.AssetTable.get_item")
    @patch("lng_aws_clients.session.set_session")
    def test_get_asset_invalid(self, session_mock, asset_mock):
        session_mock.return_value = None
        asset_mock.return_value = {}
        request_input = \
            {
                "asset-id": 1
            }
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetAssetCommand-ResponseSchema.json')):
            with self.assertRaisesRegex(NoSuchAsset, 'Invalid Asset ID'):
                get_asset_command(request_input)

    # - get_asset_command - client error
    @patch("lng_datalake_dal.asset_table.AssetTable.get_item")
    @patch("lng_aws_clients.session.set_session")
    def test_get_asset_client_error(self, session_mock, asset_mock):
        session_mock.return_value = None
        asset_mock.side_effect = ClientError({}, 'dummy')
        request_input = \
            {
                "asset-id": 1
            }
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetAssetCommand-ResponseSchema.json')):
            with self.assertRaisesRegex(InternalError, 'Unable to get item from Asset Table'):
                get_asset_command(request_input)

    # - get_asset_command - general error
    @patch("lng_datalake_dal.asset_table.AssetTable.get_item")
    @patch("lng_aws_clients.session.set_session")
    def test_get_asset_error(self, session_mock, asset_mock):
        session_mock.return_value = None
        asset_mock.side_effect = Exception({}, 'dummy')
        request_input = \
            {
                "asset-id": 1
            }
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetAssetCommand-ResponseSchema.json')):
            with self.assertRaisesRegex(Exception, "Unhandled exception occurred"):
                get_asset_command(request_input)

    # -generate_response_json - Missing required property Exception->InternalError
    @patch("lng_aws_clients.session.set_session")
    def test_generate_response_json(self, session_mock):
        session_mock.return_value = None
        request_input = \
            {
                "asset-id": 1
            }
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetAssetCommand-ResponseSchema.json')):
            with self.assertRaisesRegex(InternalError, "Missing required property:"):
                generate_response_json(request_input)

if __name__ == '__main__':
    unittest.main()
