import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue
from lng_datalake_testhelper.io_utils import IOUtils

import list_owners_command

io_util = IOUtils(__file__)


class TestListOwners(unittest.TestCase):

    # + list_owners_command - successfully calls get_all_items on OwnerTable, and gets next pagination token on
    # instance of the OwnerTable
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_all_items")
    @patch("list_owners_command.get_optional_properties_from_request")
    @patch("list_owners_command.generate_json_response")
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_pagination_token")
    def test_list_owners_command_calls_get_all_items_on_owner_table(self, mock_get_pagination_token,
                                                                    mock_generate_json_response,
                                                                    mock_get_optional_properties_from_request,
                                                                    mock_owner_table_get_all_items):
        # Given
        request = {"max-items": 25, "next-token": "abc123"}
        expected_table_data = [
            {"owner-id": 1, "owner-name": "Wormhole", "email-distribution": ["wormhole@email.com"]}
        ]

        dummy_next_pagination_token = "xyz321"
        mock_get_pagination_token.return_value = dummy_next_pagination_token

        mock_get_optional_properties_from_request.return_value = {"max-items": 25, "pagination-token": "abc123"}

        mock_owner_table_get_all_items.return_value = expected_table_data

        # When
        list_owners_command.list_owners_command(request)

        # Then
        mock_owner_table_get_all_items.assert_called_once_with(request["max-items"],
                                                               'abc123')
        mock_get_pagination_token.assert_called_once()
        mock_generate_json_response.assert_called_once_with(expected_table_data, dummy_next_pagination_token, 25)

    # - list_owner_command - throws an internal error when there is an exception
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_all_items")
    @patch("list_owners_command.get_optional_properties_from_request")
    def test_list_owner_command_catches_client_error_raises_exception(self, mock_get_optional_properties_from_request,
                                                                      mock_owner_table_get_all_items,):
        # Given
        request = {"max-items": 25, "next-token": "abc123"}
        error_response = {"Error": {"Code": "500", "Message": "Error Uploading"}}
        exception = Exception(error_response, "get_all_items")
        mock_owner_table_get_all_items.side_effect = exception
        mock_get_optional_properties_from_request.return_value = {"max-items": 25, "pagination-token": "abc123"}

        # When
        with self.assertRaises(InternalError) as context:
            list_owners_command.list_owners_command(request)

        # Then
        expected_error = InternalError("Unhandled exception occurred", exception)
        mock_owner_table_get_all_items.assert_called_once_with(request["max-items"], 'abc123')
        self.assertTrue(isinstance(context.exception, InternalError))
        self.assertEqual(str(context.exception), str(expected_error))

    # # - list_owner_command - throws an internal error when call to dal fails with client error
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_all_items")
    @patch("list_owners_command.get_optional_properties_from_request")
    def test_list_owner_command_catches_client_error_raises_internal_error(self,
                                                                           mock_get_optional_properties_from_request,
                                                                           mock_owner_table_get_all_items):
        # Given
        request = {"max-items": 25, "next-token": "abc123"}
        error_response = {"Error": {"Code": "500", "Message": "Error Uploading"}}
        client_error = ClientError(error_response, "get_all_items")
        mock_owner_table_get_all_items.side_effect = client_error
        mock_get_optional_properties_from_request.return_value = {"max-items": 25, "pagination-token": "abc123"}

        # When
        with self.assertRaisesRegex(InternalError, "Unable to get items from Owner Table"):
            list_owners_command.list_owners_command(request)

        # Then
        mock_owner_table_get_all_items.assert_called_once_with(request["max-items"], 'abc123')

    # -Failure test of Lambda for invalid max items
    @patch('lng_aws_clients.session.set_session')
    def test_list_owners_command_fail_invalid_max_results_2(self, session_mock):
        session_mock.return_value = None
        request_input = \
            {
                "collection-id": '274',
                "max-items": 1,
                "next-token": "eyJtYXgtaXRlbXMiOiAyNSwgInBhZ2luYXRpb24tdG9rZW4iOiAiYWJjMTIzIn0=",
            }
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Continuation request must pass the same max-items as initial request"):
            list_owners_command.list_owners_command(request_input)

    # + get_optional_properties_from_request - calls validate_max_results and returns optional properties dictionary
    def test_get_optional_properties_from_request_calls_validate_max_results(self):
        # Given
        valid_optional_properties = {"max-items": 25, "pagination-token": None}

        # When
        actual_response = list_owners_command.get_optional_properties_from_request(valid_optional_properties)

        # Then
        self.assertEqual(valid_optional_properties, actual_response)

    # - get_optional_properties_from_request -
    #  throws InvalidRequestPropertyValue when calling validate_max_results with bad request
    def test_get_optional_properties_from_request_throws_invalid_request_property_value(self):
        # Given
        invalid_request = {"max-items": 5000}
        error_message = InvalidRequestPropertyValue("Invalid max-items value: {0}".format(
            invalid_request["max-items"]),
            "Value must be between 1 and 1000")

        # When
        with self.assertRaises(InvalidRequestPropertyValue) as context:
            list_owners_command.get_optional_properties_from_request(invalid_request)

        # Then
        self.assertEqual(str(error_message), str(context.exception))
        self.assertTrue(isinstance(context.exception, InvalidRequestPropertyValue))

    # + generate_json_response - generates response to be returned
    def test_generate_json_response_assembles_response_with_pagination_token(self):
        # Given
        list_of_owners = [{"owner-id": 1, "owner-name": "Agata", "email-distribution": ["agata2@example.com"]}]
        pagination_token = "abc123"

        expected_response = {
            "owners": [{"owner-id": 1, "owner-name": "Agata", "email-distribution": ["agata2@example.com"]}],
            'item-count': 1,
            "next-token": "eyJtYXgtaXRlbXMiOjEsInBhZ2luYXRpb24tdG9rZW4iOiJhYmMxMjMifQ=="
        }

        # When
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('ListOwnersCommand-ResponseSchema.json')):
            result = list_owners_command.generate_json_response(list_of_owners, pagination_token, 1)

        # Then
        self.assertEqual(expected_response, result)

    # + generate_json_response - generates response to be returned
    def test_generate_json_response_assembles_response_without_pagination_token(self):
        # Given
        list_of_owners = [{"owner-id": 1, "owner-name": "Agata", "email-distribution": ["agata2@example.com"],
                           "collection-prefix": "FPD"}]
        pagination_token = None

        expected_response = {
            "owners": [{"owner-id": 1, "owner-name": "Agata", "email-distribution": ["agata2@example.com"],
                        "collection-prefixes": ["FPD"]}],
            'item-count': 1
        }

        # When
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('ListOwnersCommand-ResponseSchema.json')):
            result = list_owners_command.generate_json_response(list_of_owners, pagination_token, 1)

        # Then
        self.assertEqual(expected_response, result)

    # - generate_json_response - missing required
    def test_generate_json_response_fail(self):
        # Given
        list_of_owners = [{"test": "fail"}]

        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('ListOwnersCommand-ResponseSchema.json')):
            with self.assertRaisesRegex(InternalError, "Missing required property"):
                list_owners_command.generate_json_response(list_of_owners, None, 1)


if __name__ == '__main__':
    unittest.main()
