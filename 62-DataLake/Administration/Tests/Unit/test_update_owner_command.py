import copy
import unittest
from unittest.mock import patch

from lng_datalake_commands.exceptions import SemanticError, NotAuthorizedError, InvalidRequestPropertyValue
from lng_datalake_testhelper.io_utils import IOUtils

from update_owner_command import update_owner_command, has_owner_changes, replace_op_commands

io_util = IOUtils(__file__, 'UpdateOwner')


class TestUpdateOwnerCommand(unittest.TestCase):

    # + update_owner_command - validates a response is returned
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch("service_commons.owner_command.generate_json_response")
    @patch("update_owner_command.has_owner_changes")
    @patch("service_commons.owner_command.validate_owner")
    @patch("service_commons.owner_command.create_owner_event_store_item")
    @patch("lng_aws_clients.session.set_session")
    def test_update_owner_command_returns_response(self, mock_set_session,
                                                   mock_create_owner_event_store_item,
                                                   mock_validate_owner,
                                                   mock_has_owner_changes,
                                                   mock_generate_json_response,
                                                   mock_owner_authorization):
        mock_set_session.return_value = None
        # Given
        request = io_util.load_data_json('request_data_valid.json')
        get_owner_from_table_response = io_util.load_data_json('dynamodb.get_owner_valid.json')
        mock_owner_authorization.return_value = get_owner_from_table_response
        get_updated_owner_response = io_util.load_data_json('expected_response_dict.json')
        mock_create_owner_event_store_item.return_value = io_util.load_data_json('create_owner_event_store_item.json')
        generate_json_response = io_util.load_data_json('generate_json_response.json')

        mock_generate_json_response.return_value = generate_json_response

        # When
        response = update_owner_command(request, "123", "LATEST", "testApiKeyId")

        # Then
        mock_owner_authorization.assert_called_once_with(request["owner-id"], "testApiKeyId")
        mock_has_owner_changes.assert_called_once_with(get_owner_from_table_response, generate_json_response)
        mock_validate_owner.assert_called_once_with(generate_json_response)
        self.assertEqual(get_updated_owner_response, response)

    # - updated_owner_command - throws SemanticError if has_owner_changes returns false
    @patch("update_owner_command.has_owner_changes")
    @patch("lng_aws_clients.session.set_session")
    @patch('lng_datalake_commands.owner_authorization.authorize')
    def test_update_owner_command_throws_semantic_error_if_there_are_no_changes(self,
                                                                                mock_owner_authorization,
                                                                                mock_set_session,
                                                                                mock_has_owner_changes):
        # Given
        request = io_util.load_data_json('request_data_valid.json')
        mock_has_owner_changes.return_value = False
        mock_owner_authorization.return_value = {'owner-id': 5, 'owner-name': 'Bob',
                                                 'email-distribution': ['test@ok.com']}
        mock_set_session.return_value = None

        # When
        with self.assertRaises(SemanticError) as context:
            with patch('lng_datalake_commands.command_wrapper._response_schema',
                       io_util.load_schema_json('UpdateOwnerCommand-ResponseSchema.json')):
                update_owner_command(request, "123", "LATEST", "testApiKeyId")

        # Then
        expected_exception = SemanticError("No data changes")
        self.assertEqual(str(context.exception), str(expected_exception))

    # - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_aws_clients.session.set_session')
    def test_update_owner_command_failed_owner_auth(self, mock_set_session, mock_owner_authorization):
        # Given
        request = io_util.load_data_json('request_data_valid.json')
        mock_set_session.return_value = None

        # When
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")

        # Then
        with self.assertRaisesRegex(NotAuthorizedError,
                                    "NotAuthorizedError||API Key provided is not valid for Owner ID.*"):
            update_owner_command(request, "123", "LATEST", "testApiKeyId-bad")

    # + has_owner_changes returns true if there are differences between existing and updated owner-name
    def test_has_owner_changes_returns_true_if_has_changes_in_owner_name(self):
        # Given
        existing_owner = io_util.load_data_json('dynamodb.get_owner_valid.json')
        updated_owner = copy.deepcopy(existing_owner)
        updated_owner['owner-name'] = 'updated owner'

        # When
        has_changes = has_owner_changes(existing_owner, updated_owner)

        # Then
        self.assertTrue(has_changes)

    # + has_owner_changes returns true if there are differences between existing and updated email-distribution
    def test_has_owner_changes_returns_true_if_has_changes_in_email_distribution_list(self):
        # Given
        existing_owner = io_util.load_data_json('dynamodb.get_owner_valid.json')
        updated_owner = copy.deepcopy(existing_owner)
        updated_owner['email-distribution'] = ['test@email.com']

        # When
        has_changes = has_owner_changes(existing_owner, updated_owner)

        # Then
        self.assertTrue(has_changes)

    # - has_owner_changes should return false if there aren't any differences between existing and updated owner
    def test_has_owner_changes_returns_false_if_has_no_changes(self):
        # Given
        existing_owner = io_util.load_data_json('dynamodb.get_owner_valid.json')

        # When
        has_no_changes = has_owner_changes(existing_owner, existing_owner)

        # Then
        self.assertFalse(has_no_changes)

    # + replace_op_commands - Valid request
    def test_replace_op_commands_valid(self):
        input = io_util.load_data_json('patch_request_data_valid.json')
        expected_resp = {
            "owner-id": 12345,
            "owner-name": "Bob",
            "email-distribution": ["test@ok.com"]
        }
        self.assertDictEqual(replace_op_commands(input), expected_resp)

    # - replace_op_commands - Invalid request
    def test_replace_op_commands_fail(self):
        input = io_util.load_data_json('patch_request_data_invalid.json')
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid operation verb supplied"):
            replace_op_commands(input)


if __name__ == '__main__':
    unittest.main()
