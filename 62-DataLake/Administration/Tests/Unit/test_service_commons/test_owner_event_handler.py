import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_testhelper.io_utils import IOUtils

from service_commons import owner_event_handler
from service_commons.owner_event_handler import get_owner_from_owner_table, get_owner_collection_status, \
    initialize_dict_with_keys

io_util = IOUtils(__file__, 'OwnerEventHandler')


class TestAdministrationOwnerEventHandler(unittest.TestCase):

    # + get_owner_from_owner_table - get a valid owner from db
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    def test_get_owner_from_owner_table_returns_owner(self, mock_get_item_from_owner_table):
        # Given
        owner_id = 1
        expected_response = io_util.load_data_json('transformed_owner_valid.json')
        mock_get_item_from_owner_table.return_value = expected_response

        # When
        response = get_owner_from_owner_table(owner_id)

        # Then
        mock_get_item_from_owner_table.assert_called_once_with({"owner-id": owner_id})
        self.assertEqual(expected_response, response)

    # - get_owner_from_owner_table - raises TerminalException
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    def test_get_owner_from_owner_table_raises_error_when_owner_does_not_exist(self,
                                                                               mock_get_item_from_owner_table):
        # Given
        owner_id = 1
        expected_response = {}
        mock_get_item_from_owner_table.return_value = expected_response

        with self.assertRaisesRegex(TerminalErrorException, 'does not exist'):
            get_owner_from_owner_table(owner_id)

    # - get_owner_from_owner_table - raises client error
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    def test_get_owner_from_owner_table_raises_client_error(self, mock_get_item_from_owner_table):
        # Given
        owner_id = 1
        client_error = ClientError({'ResponseMetadata': {},
                                    'Error': {'Code': 'ProvisionedThroughputExceededException',
                                              'Message': 'This is a mock'}}, "FAKE")

        mock_get_item_from_owner_table.side_effect = client_error

        with self.assertRaisesRegex(ClientError, 'This is a mock'):
            get_owner_from_owner_table(owner_id)

    # - get_owner_from_owner_table - raises terminal exception
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    def test_get_owner_from_owner_table_raises_exception(self, mock_get_item_from_owner_table):
        # Given
        owner_id = 1
        exception = Exception("Error occurred")
        mock_get_item_from_owner_table.side_effect = exception

        # When
        with self.assertRaises(TerminalErrorException) as context:
            get_owner_from_owner_table(owner_id)

    # + get_collection_status returns ACTIVE when owner has a collection
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_throws_when_get_collection_status(self, mock_collection_table_query_items):
        # Given
        owner_id = 123
        mock_collection_table_query_items.return_value = io_util.load_data_json('dynamodb.get_collection_valid.json')

        # When
        result = get_owner_collection_status(owner_id)

        # Then
        expected_expression = {":owner_id": {"N": str(owner_id)}}
        mock_collection_table_query_items.assert_called_once_with(index='collection-by-owner-index',
                                                                  max_items=1000,
                                                                  pagination_token=None,
                                                                  KeyConditionExpression='OwnerID = :owner_id ',
                                                                  ExpressionAttributeValues=expected_expression)
        self.assertEqual(result, owner_event_handler.COLLECTION_ACTIVE)

    # + get_colleciton_status returns NONE when an owner has no collection
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_returns_none(self, mock_collection_table_query_items):
        # Given
        owner_id = 123
        mock_collection_table_query_items.return_value = io_util.load_data_json(
            'dynamodb.get_collection_valid_empty.json')

        # When
        result = get_owner_collection_status(owner_id)

        # Then
        expected_expression = {":owner_id": {"N": str(owner_id)}}
        mock_collection_table_query_items.assert_called_once_with(index='collection-by-owner-index',
                                                                  max_items=1000,
                                                                  pagination_token=None,
                                                                  KeyConditionExpression='OwnerID = :owner_id ',
                                                                  ExpressionAttributeValues=expected_expression)
        self.assertEqual(result, owner_event_handler.COLLECTION_NONE)

    # + get_collection_status returns TERMINATED when an owner has all terminated collection
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_returns_terminated(self,
                                                      mock_collection_table_query_items):
        # Given
        owner_id = 123
        mock_collection_table_query_items.return_value = io_util.load_data_json(
            'dynamodb.get_collection_terminated.json')

        # When
        result = get_owner_collection_status(owner_id)

        # Then
        expected_expression = {":owner_id": {"N": str(owner_id)}}
        mock_collection_table_query_items.assert_called_once_with(index='collection-by-owner-index',
                                                                  max_items=1000,
                                                                  pagination_token=None,
                                                                  KeyConditionExpression='OwnerID = :owner_id ',
                                                                  ExpressionAttributeValues=expected_expression)
        self.assertEqual(result, owner_event_handler.COLLECTION_TERMINATED)

    # - get_collection_status throws a ClientError
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_throws_client_error(self, mock_collection_table_query_items):
        # Given
        owner_id = 123
        mock_collection_table_query_items.side_effect = ClientError({"ResponseMetadata": {}}, "OperationName")

        with self.assertRaisesRegex(ClientError, "An error occurred"):
            get_owner_collection_status(owner_id)

    # - get_collection_status throws TerminalException when a GeneralException is thrown
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_throws_internal_error(self, mock_collection_table_query_items):
        # Given
        owner_id = 123
        exception_to_throw = Exception("Some error")
        mock_collection_table_query_items.side_effect = exception_to_throw

        with self.assertRaisesRegex(TerminalErrorException, "Unable to get status"):
            get_owner_collection_status(owner_id)

    # + initialize_dict_with_keys
    def test_initialize_dict_with_keys_success_1(self):
        updated_owner = {
            'owner-id': 12345,
            'owner-name': 'Test Owner',
            'event-id': 'test_event'
        }
        existing_owner = io_util.load_data_json('transformed_owner_valid.json')
        expected_response = io_util.load_data_json('transformed_owner_valid.json')
        expected_response['owner-name'] = updated_owner['owner-name']

        expected_response['event-ids'] = ["e6f3bf6b-3e45-11e9-9d5b-a5f0b99ab302", "test_event"]

        actual_response = initialize_dict_with_keys(updated_owner, existing_owner)

        self.assertDictEqual(expected_response, actual_response)

    # + initialize_dict_with_keys
    def test_initialize_dict_with_keys_success_2(self):
        message = io_util.load_data_json('create_owner_sns_message.json')
        expected_response = io_util.load_data_json('transformed_owner_valid.json')

        actual_response = initialize_dict_with_keys(message)

        self.assertDictEqual(expected_response, actual_response)

    # - initialize_dict_with_keys - KeyError
    def test_initialize_dict_with_keys_key_error(self):
        updated_owner = {
            'owner-id': 12345
        }
        existing_owner = io_util.load_data_json('transformed_owner_valid.json')
        existing_owner.pop('owner-name')
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Key = owner-name does not exist in owner {}".format(updated_owner['owner-id'])):
            initialize_dict_with_keys(updated_owner, existing_owner)


if __name__ == "__main__":
    unittest.main()
