import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, NoSuchCollection, \
    NoSuchCatalog, SemanticError
from lng_datalake_testhelper.io_utils import IOUtils

from service_commons.relationship_command import validate_catalog_commitments, validate_collection_commitments

io_util = IOUtils(__file__, 'RelationshipCommand')


class TestAdministrationRelationshipCommand(unittest.TestCase):

    # + valid catalog commitments
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_validate_catalog_commitments(self, mock_get_item):
        mock_get_item.return_value = [{"catalog-id": "fake_catalog_id", "catalog-state": "Created"}]
        self.assertEqual(None, validate_catalog_commitments(["fake_catalog_id"]))

    # - invalid catalog id doesn't exist
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_validate_catalog_commitments_invalid_catalog(self, mock_get_item):
        mock_get_item.return_value = None
        catalog_id = "fake-catalog-id"
        with self.assertRaisesRegex(NoSuchCatalog, "Invalid Catalog ID(s) {0}||"
                                                   "Catalog ID(s) does not exist".format(catalog_id)):
            validate_catalog_commitments([catalog_id])

    # - invalid catalog id doesn't exist
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_validate_catalog_commitments_invalid_catalog_count(self, mock_get_item):
        mock_get_item.return_value = [{"catalog-id": "fake_catalog_id", "catalog-state": "Created"}]
        catalog_ids = ["fake_catalog_id", "missing_catalog_id", "2nd_missing_catalog_id"]
        with self.assertRaisesRegex(NoSuchCatalog, "Invalid Catalog ID(s) {0}||"
                                                   "Catalog ID(s) does not exist".format(
            "{'missing_catalog_id', '2nd_missing_catalog_id'}")):
            validate_catalog_commitments(catalog_ids)

    # - Unhandled exception
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_validate_catalog_commitments_exception(self, mock_get_item):
        mock_get_item.side_effect = Exception
        catalog_id = "fake-catalog-id"
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            validate_catalog_commitments([catalog_id])

    # - Client error exception
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_validate_catalog_commitments_client_exception(self, mock_get_item):
        mock_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                 'Error': {
                                                     'Code': 'Unit Test',
                                                     'Message': 'This is a mock'}},
                                                "FAKE")
        catalog_id = "fake-catalog-id"
        with self.assertRaisesRegex(InternalError,
                                    "InternalError||Unable to get item from Catalog Table||An error occurred"
                                    "(Unit Test) when calling the FAKE operation: This is a mock"):
            validate_catalog_commitments([catalog_id])

    # + validation of valid collecitons
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collection_commitments(self, mock_get_item):
        mock_get_item.return_value = [{"collection-id": "fake_collection_id", "collection-state": "Created"}]
        self.assertEqual(None, validate_collection_commitments(["fake_collection_id"]))

    # - Invalid collection ID
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collection_commitments_invalid_collection(self, mock_get_item):
        mock_get_item.return_value = None
        collection_id = "fake-collection-id"
        with self.assertRaisesRegex(NoSuchCollection, "Invalid Collection ID(s) {0}||"
                                                      "Collection ID(s) does not exist".format(
            collection_id)):
            validate_collection_commitments([collection_id])

    # - Invalid collection id
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collection_commitments_invalid_collection_count(self, mock_get_item):
        mock_get_item.return_value = [{"collection-id": "fake_collection_id", "collection-state": "Created"}]
        collection_ids = ["fake_collection_id", "missing_collection_id", "2nd_missing_collection_id"]
        with self.assertRaisesRegex(NoSuchCollection, "Invalid Collection ID(s) {0}||"
                                                      "Collection ID(s) does not exist".format(
            "{'missing_collection_id', '2nd_missing_collection_id'}")):
            validate_collection_commitments(collection_ids)

    # - Invalid collection id state
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collection_commitments_invalid_collection_state(self, mock_get_item):
        mock_get_item.return_value = [{"collection-id": "fake_collection_id", "collection-state": "Terminated"}]
        collection_ids = ["fake_collection_id"]
        with self.assertRaisesRegex(SemanticError, "Invalid Collection ID(s) {0}||"
                                                   "Collection ID(s) not in valid state".format(
            "{'fake_collection_id'}")):
            validate_collection_commitments(collection_ids)

    # - Unhandled exception
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collection_commitments_exception(self, mock_get_item):
        mock_get_item.side_effect = Exception
        collection_id = "fake-collection-id"
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            validate_collection_commitments([collection_id])

    # - ClientError exception
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collection_commitments_client_exception(self, mock_get_item):
        mock_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                 'Error': {
                                                     'Code': 'Unit Test',
                                                     'Message': 'This is a mock'}},
                                                "FAKE")
        collection_id = "fake-collection-id"
        with self.assertRaisesRegex(InternalError,
                                    "InternalError||Unable to get item from Collection Table||An error occurred"
                                    "(Unit Test) when calling the FAKE operation: This is a mock"):
            validate_collection_commitments([collection_id])


if __name__ == "__main__":
    unittest.main()
