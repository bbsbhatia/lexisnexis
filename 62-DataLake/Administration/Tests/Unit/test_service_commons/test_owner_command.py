import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, ConflictingRequestError, \
    NoSuchOwner, CollectionPrefixAlreadyExists
from lng_datalake_constants.event_names import OWNER_CREATE
from lng_datalake_testhelper.io_utils import IOUtils

from service_commons import owner_command
from service_commons.owner_command import validate_owner, validate_owner_name, validate_email_list, \
    validate_collection_prefix, validate_unique_collection_prefix, validate_unique_owner, get_owner_from_owner_table, \
    get_owner_collection_status, create_owner_event_store_item, generate_json_response

io_util = IOUtils(__file__, 'OwnerCommand')


class TestAdministrationOwnerCommand(unittest.TestCase):

    # + validate owner
    @patch('service_commons.owner_command.validate_owner_name')
    @patch('service_commons.owner_command.validate_email_list')
    @patch('service_commons.owner_command.validate_unique_owner')
    def test_validate_owner(self,
                            mock_validate_owner_name,
                            mock_validate_email_list,
                            mock_validate_unique_owner):
        owner_input = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ["abc_123@lexisnexis.com"]
        }

        mock_validate_owner_name.return_value = None
        mock_validate_email_list.return_value = None
        mock_validate_unique_owner.return_value = None
        self.assertIsNone(validate_owner(owner_input))

    # + validate owner with collection prefix
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    @patch('service_commons.owner_command.validate_owner_name')
    @patch('service_commons.owner_command.validate_email_list')
    @patch('service_commons.owner_command.validate_unique_owner')
    def test_validate_owner_1(self,
                              mock_validate_owner_name,
                              mock_validate_email_list,
                              mock_validate_unique_owner,
                              mock_owner_query_items):
        owner_input = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ["abc_123@lexisnexis.com"]
        }

        mock_owner_query_items.return_value = []
        mock_validate_owner_name.return_value = None
        mock_validate_email_list.return_value = None
        mock_validate_unique_owner.return_value = None
        self.assertIsNone(validate_owner(owner_input))

    # + validate owner
    @patch('service_commons.owner_command.validate_owner_name')
    @patch('service_commons.owner_command.validate_email_list')
    @patch('service_commons.owner_command.validate_unique_owner')
    @patch('service_commons.owner_command.validate_unique_collection_prefix')
    @patch('service_commons.owner_command.validate_collection_prefix')
    def test_validate_owner_with_collection_prefix(self,
                                                   mock_validate_unique_collection_prefix,
                                                   mock_validate_collection_prefix,
                                                   mock_validate_owner_name,
                                                   mock_validate_email_list,
                                                   mock_validate_unique_owner
                                                   ):
        owner = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ["abc_123@lexisnexis.com"],
            "collection-prefixes": ["PFD"]
        }

        mock_validate_unique_collection_prefix.return_value = None
        mock_validate_collection_prefix.return_value = None
        mock_validate_owner_name.return_value = None
        mock_validate_email_list.return_value = None
        mock_validate_unique_owner.return_value = None

        self.assertIsNone(validate_owner(owner, validate_prefix=True))

    @patch('service_commons.owner_command.validate_owner_name')
    @patch('service_commons.owner_command.validate_email_list')
    @patch('service_commons.owner_command.validate_unique_owner')
    @patch('service_commons.owner_command.validate_unique_collection_prefix')
    @patch('service_commons.owner_command.validate_collection_prefix')
    def test_validate_owner_Invalid_number_of_prefixes(self,
                                                       mock_validate_unique_collection_prefix,
                                                       mock_validate_collection_prefix,
                                                       mock_validate_owner_name,
                                                       mock_validate_email_list,
                                                       mock_validate_unique_owner
                                                       ):
        owner = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ["abc_123@lexisnexis.com"],
            "collection-prefixes": ["PFD1", "PFD2"]
        }

        mock_validate_unique_collection_prefix.return_value = None
        mock_validate_collection_prefix.return_value = None
        mock_validate_owner_name.return_value = None
        mock_validate_email_list.return_value = None
        mock_validate_unique_owner.return_value = None
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "InvalidRequestPropertyValue||Invalid number of collection Prefixes"):
            self.assertIsNone(validate_owner(owner, validate_prefix=True))

    # + validate owner name
    def test_validate_owner_name(self):
        self.assertIsNone(validate_owner_name("dummy_owner"))

    # - validate owner name - error
    def test_validate_owner_name_error(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "InvalidRequestPropertyValue||Owner name is not valid"):
            validate_owner_name('')

    # + validate email list
    def test_validate_email_list(self):
        test_emails = [
            "prettyandsimple@example.com",
            "very.common@example.com",
            "disposable.style.email.with+symbol@example.com",
            "other.email-with-dash@example.com",
            "fully-qualified-domain@example.com",
            "user.name+tag+sorting@example.com",
            "x@example.com (one-letter local-part)",
            "example-indeed@strange-example.com",
            "admin@mailserver1",
            "#!$%&'*+-/=?^_`{}|~@example.org",
            "example@s.solutions",
            "user@localserver",
            "user@[2001:DB8::1]"  # NOSONAR
        ]
        self.assertIsNone(validate_email_list(test_emails))

    # - Test that validate_email_list raises InvalidPropertyRequestValue when is_valid_email returns false
    def test_validate_email_list_raises_exception_when_email_list_is_invalid(self):
        test_emails = ["emailexample.com", "example@email.com", "emailexample2.com"]

        # When
        with self.assertRaises(InvalidRequestPropertyValue) as context:
            validate_email_list(test_emails)

        # Then
        test_emails.remove("example@email.com")
        expected_exception = InvalidRequestPropertyValue("The following emails are invalid {}".format(test_emails))
        self.assertEqual(str(context.exception), str(expected_exception))

    # + validate collection-prefix
    def test_validate_collection_prefix(self):
        test_prefix1 = "A"
        test_prefix2 = "1A"
        test_prefix3 = "ABCDED"
        test_prefix4 = "123A"
        test_prefix5 = "A123S"
        test_prefix6 = "AA123"

        self.assertIsNone(validate_collection_prefix(test_prefix1))
        self.assertIsNone(validate_collection_prefix(test_prefix2))
        self.assertIsNone(validate_collection_prefix(test_prefix3))
        self.assertIsNone(validate_collection_prefix(test_prefix4))
        self.assertIsNone(validate_collection_prefix(test_prefix5))
        self.assertIsNone(validate_collection_prefix(test_prefix6))

    # - Test that validate_collection_prefix raises InvalidPropertyRequestValue
    def test_validate_collection_prefix_raises_exception(self):
        test_prefix1 = "123"
        test_prefix2 = "a-123"
        test_prefix3 = "a-b"
        test_prefix4 = "A12345678910"

        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Collection prefix is invalid {0}".format(test_prefix1)):
            validate_collection_prefix(test_prefix1)

        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Collection prefix is invalid {0}".format(test_prefix2)):
            validate_collection_prefix(test_prefix2)

        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Collection prefix is invalid {0}".format(test_prefix3)):
            validate_collection_prefix(test_prefix3)

        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid Collection prefix {0}".format(test_prefix4)):
            validate_collection_prefix(test_prefix4)

    # + validate unique collection prefix
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_collection_prefix_success(self, mock_owner_query_items):
        mock_owner_query_items.return_value = []

        self.assertIsNone(validate_unique_collection_prefix("FPD"))

    # - validate unique collection prefix : duplicate collection prefix
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_collection_prefix_duplicate_error_1(self, mock_owner_query_items):
        collection_prefix = "FPD"

        mock_owner_query_items.return_value = [
            {'owner-name': 'dummy_owner_name',
             'email-distribution': ['email1@lexisnexis.com', 'email2@lexisnexis.com'],
             'api-key': 'someKey',
             'owner-id': 39,
             'collection-prefix': collection_prefix}]

        with self.assertRaisesRegex(CollectionPrefixAlreadyExists,
                                    "Collection prefix already exists*"):
            validate_unique_collection_prefix(collection_prefix)

    # - validate unique collection  prefix : raises client error
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_collection_prefix_client_error(self, mock_owner_query_items):
        collection_prefix = "FPD"

        mock_owner_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                          'Error': {
                                                              'Code': 'OTHER',
                                                              'Message': 'Unable to access OwnerTable"'}},
                                                         "FAKE")
        self.assertRaises(InternalError, validate_unique_collection_prefix, collection_prefix)

    # - validate unique collection prefix  : raises exception
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_collection_prefix_exception(self, mock_owner_query_items):
        collection_prefix = "FPD"
        mock_owner_query_items.side_effect = Exception
        self.assertRaises(InternalError, validate_unique_collection_prefix, collection_prefix)

    # tests for validate unique owner -----------------

    # + validate unique owner
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_success(self, mock_owner_query_items):
        owner_input = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ["abc_123@lexisnexis.com"]
        }

        mock_owner_query_items.return_value = []

        self.assertIsNone(validate_unique_owner(owner_input))

    # - validate unique owner : duplicate owner same order of email list
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_duplicate_error_1(self, mock_owner_query_items):
        owner_input = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ['abc_123@lexisnexis.com', 'cde_123@lexisnexis.com']
        }

        mock_owner_query_items.return_value = [
            {'owner-name': 'dummy_owner_name',
             'email-distribution': ['abc_123@lexisnexis.com', 'cde_123@lexisnexis.com'],
             'api-key': 'keykeykeykeykeykey',
             'owner-id': 39}]

        with self.assertRaisesRegex(ConflictingRequestError,
                                    "Owner already exists*"):
            validate_unique_owner(owner_input)

    # - validate unique owner : duplicate owner different order of email list
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_duplicate_error_2(self, mock_owner_query_items):
        owner_input = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ['cde_123@lexisnexis.com', 'abc_123@lexisnexis.com']
        }

        mock_owner_query_items.return_value = [
            {'owner-name': 'dummy_owner_name',
             'email-distribution': ['abc_123@lexisnexis.com', 'cde_123@lexisnexis.com'],
             'api-key': 'keykeykeykeykeykey',
             'owner-id': 39}]

        with self.assertRaisesRegex(ConflictingRequestError,
                                    "Owner already exists*"):
            validate_unique_owner(owner_input)

    # - validate unique owner : raises client error
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_client_error(self, mock_owner_query_items):
        owner_input = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ["abc_123@lexisnexis.com"]
        }

        mock_owner_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                          'Error': {
                                                              'Code': 'OTHER',
                                                              'Message': 'Unable to access OwnerTable"'}},
                                                         "FAKE")
        self.assertRaises(InternalError, validate_unique_owner, owner_input)

    # - validate unique owner : raises exception
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_validate_unique_owner_exception(self, mock_owner_query_items):
        owner_input = {
            "owner-name": "dummy_owner_name",
            "email-distribution": ["abc_123@lexisnexis.com"]
        }
        mock_owner_query_items.side_effect = Exception
        self.assertRaises(InternalError, validate_unique_owner, owner_input)

    # + get_owner_from_owner_table - get a valid owner from db
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    def test_get_owner_from_owner_table_returns_owner(self, mock_get_item_from_owner_table):
        # Given
        owner_id = 1
        expected_response = io_util.load_data_json('transformed_owner_valid.json')
        mock_get_item_from_owner_table.return_value = expected_response

        # When
        response = get_owner_from_owner_table(owner_id)

        # Then
        mock_get_item_from_owner_table.assert_called_once_with({"owner-id": owner_id})
        self.assertEqual(expected_response, response)

    # - get_owner_from_owner_table - raises InvalidRequestPropertyValue if owner does not exist
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    def test_get_owner_from_owner_table_raises_error_when_owner_does_not_exist(self,
                                                                               mock_get_item_from_owner_table):
        # Given
        owner_id = 1
        expected_response = {}
        mock_get_item_from_owner_table.return_value = expected_response

        # When
        with self.assertRaises(NoSuchOwner) as context:
            get_owner_from_owner_table(owner_id)

        # Then
        expected_exception = NoSuchOwner("Invalid Owner ID 1", "Owner ID does not exist")
        self.assertEqual(str(expected_exception), str(context.exception))

    # - get_owner_from_owner_table - raises internal error when dal raises client error
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    def test_get_owner_from_owner_table_raises_internal_error(self, mock_get_item_from_owner_table):
        # Given
        owner_id = 1
        client_error = ClientError({'ResponseMetadata': {},
                                    'Error': {'Code': 'ProvisionedThroughputExceededException',
                                              'Message': 'This is a mock'}}, "FAKE")

        mock_get_item_from_owner_table.side_effect = client_error

        # When
        with self.assertRaises(InternalError) as context:
            get_owner_from_owner_table(owner_id)

        # Then
        expected_exception = InternalError("Unable to get item from Owner Table", client_error)
        self.assertEqual(str(context.exception), str(expected_exception))

    # - get_owner_from_owner_table - raises internal error when dal raises exception
    @patch("lng_datalake_dal.owner_table.OwnerTable.get_item")
    def test_get_owner_from_owner_table_raises_exception(self, mock_get_item_from_owner_table):
        # Given
        owner_id = 1
        exception = Exception("Error occurred")
        mock_get_item_from_owner_table.side_effect = exception

        # When
        with self.assertRaises(Exception) as context:
            get_owner_from_owner_table(owner_id)

        # Then
        expected_exception = InternalError("Unhandled exception occurred", exception)
        self.assertEqual(str(context.exception), str(expected_exception))

    # + get_collection_status returns ACTIVE when owner has a collection
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_throws_when_get_collection_status(self, mock_collection_table_query_items):
        # Given
        owner_id = 123
        mock_collection_table_query_items.return_value = io_util.load_data_json('dynamodb.get_collection_valid.json')

        # When
        result = get_owner_collection_status(owner_id)

        # Then
        expected_expression = {":owner_id": {"N": str(owner_id)}}
        mock_collection_table_query_items.assert_called_once_with(index='collection-by-owner-index',
                                                                  max_items=1000,
                                                                  pagination_token=None,
                                                                  KeyConditionExpression='OwnerID = :owner_id ',
                                                                  ExpressionAttributeValues=expected_expression)
        self.assertEqual(result, owner_command.COLLECTION_ACTIVE)

    # + get_colleciton_status returns NONE when an owner has no collection
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_returns_none(self, mock_collection_table_query_items):
        # Given
        owner_id = 123
        mock_collection_table_query_items.return_value = io_util.load_data_json(
            'dynamodb.get_collection_valid_empty.json')

        # When
        result = get_owner_collection_status(owner_id)

        # Then
        expected_expression = {":owner_id": {"N": str(owner_id)}}
        mock_collection_table_query_items.assert_called_once_with(index='collection-by-owner-index',
                                                                  max_items=1000,
                                                                  pagination_token=None,
                                                                  KeyConditionExpression='OwnerID = :owner_id ',
                                                                  ExpressionAttributeValues=expected_expression)
        self.assertEqual(result, owner_command.COLLECTION_NONE)

    # + get_collection_status returns TERMINATED when an owner has all terminated collection
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_returns_terminated(self,
                                                      mock_collection_table_query_items):
        # Given
        owner_id = 123
        mock_collection_table_query_items.return_value = io_util.load_data_json(
            'dynamodb.get_collection_terminated.json')

        # When
        result = get_owner_collection_status(owner_id)

        # Then
        expected_expression = {":owner_id": {"N": str(owner_id)}}
        mock_collection_table_query_items.assert_called_once_with(index='collection-by-owner-index',
                                                                  max_items=1000,
                                                                  pagination_token=None,
                                                                  KeyConditionExpression='OwnerID = :owner_id ',
                                                                  ExpressionAttributeValues=expected_expression)
        self.assertEqual(result, owner_command.COLLECTION_TERMINATED)

    # - get_collection_status throws InternalError when a ClientError is thrown
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_throws_client_error(self, mock_collection_table_query_items):
        # Given
        owner_id = 123
        mock_collection_table_query_items.side_effect = ClientError({"ResponseMetadata": {}}, "OperationName")

        # When
        with self.assertRaisesRegex(InternalError, "Unable to query Collection Table"):
            get_owner_collection_status(owner_id)

    # - get_collection_status throws InternalError when a GeneralException is thrown
    @patch("lng_datalake_dal.collection_table.CollectionTable.query_items")
    def test_get_collection_status_throws_internal_error(self, mock_collection_table_query_items):
        # Given
        owner_id = 123
        exception_to_throw = Exception("Some error")
        mock_collection_table_query_items.side_effect = exception_to_throw

        # When
        with self.assertRaises(InternalError) as context:
            get_owner_collection_status(owner_id)

        # Then
        expected_exception = InternalError("Unhandled exception occurred", exception_to_throw)
        self.assertEqual(str(context.exception), str(expected_exception))

    # + create_event_store_item returns dictionary with added properties to original request
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_create_owner_event_store_item_returns_modified_dictionary(self, mock_command_wrapper):
        # Given
        request = io_util.load_data_json('transformed_owner_valid.json')
        expected_event_store_item = io_util.load_data_json('event_store_response_valid.json')

        mock_command_wrapper.return_value = expected_event_store_item['request-time']
        request_id = expected_event_store_item['event-id']
        event_version = expected_event_store_item['event-version']
        stage = expected_event_store_item['stage']

        # When
        actual_event_store_item = create_owner_event_store_item(request, OWNER_CREATE, request_id, event_version, stage)

        # Then
        self.assertDictEqual(actual_event_store_item, expected_event_store_item)

    # + generate_json_response - returns owners and pagination token
    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    @patch("lng_datalake_commands.command_wrapper.get_optional_response_schema_keys")
    def test_generate_json_response_success_1(self,
                                              mock_command_wrapper_get_optional,
                                              mock_command_wrapper_get_required):
        owner_resp = io_util.load_data_json("dynamodb.owner_response.transformed.valid.json")
        mock_command_wrapper_get_required.return_value = io_util.load_data_json(
            "schema.required_response_schema_keys_valid.json")
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json(
            "schema.optional_response_schema_keys_valid.json")
        expected_response = {
            "owner-id": 1006,
            "owner-name": "RemoveOwnerTest",
            "email-distribution": [
                "who@what.com"
            ]
        }
        response = generate_json_response(owner_resp)
        self.assertDictEqual(expected_response, response)

    # + generate_json_response - returns owners and pagination token
    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    @patch("lng_datalake_commands.command_wrapper.get_optional_response_schema_keys")
    def test_generate_json_response_success_2(self,
                                              mock_command_wrapper_get_optional,
                                              mock_command_wrapper_get_required):
        owner_resp = io_util.load_data_json("dynamodb.owner_response.transformed.valid.json")
        mock_command_wrapper_get_required.return_value = io_util.load_data_json(
            "schema.required_response_schema_keys_valid.json")
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json(
            "schema.optional_response_schema_keys_valid.json")
        expected_response = {
            "owner-id": 1006,
            "owner-name": "NewOwnerTest",
            "email-distribution": [
                "who@what.com"
            ],
            "collection-prefixes": ["PDF"]

        }

        update_owner = {
            "owner-name": "NewOwnerTest"
        }
        # Update owner command uses new and existing owner parameters
        response = generate_json_response(update_owner, owner_resp)
        self.assertEqual(expected_response, response)

    # -Failed test: InternalError exception due to missing required property
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_missing_required_property(self,
                                                              mock_command_wrapper_get_optional,
                                                              mock_command_wrapper_get_required):
        mock_command_wrapper_get_required.return_value = io_util.load_data_json(
            "schema.required_response_schema_keys_valid.json")
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json(
            "schema.optional_response_schema_keys_valid.json")

        request = {
            "owner-id": 1006,
            "email-distribution": [
                "who@what.com"
            ]

        }

        with self.assertRaisesRegex(InternalError, "Missing required property:"):
            generate_json_response(request)


if __name__ == "__main__":
    unittest.main()
