import unittest

from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_testhelper.io_utils import IOUtils

from service_commons.relationship_event_handler import generate_relationship_item

io_util = IOUtils(__file__, 'RelationshipEventHandler')


class TestRelationshipOwnerEventHandler(unittest.TestCase):

    def test_initialize_dict_with_keys_success(self):
        new_rel = {
            "relationship-id": "judges",
            "owner-id": 1234,
            "relationship-type": "Metadata",
            "relationship-state": "Created",
            "description": "new description"

        }
        rel = {
            "relationship-id": "judges",
            "owner-id": 1234,
            "relationship-type": "Metadata",
            "description": "new description"
        }
        self.assertEqual(new_rel, generate_relationship_item(rel))

    def test_initialize_dict_with_keys_key_error(self):
        rel = {
            "relationship-id": "judges",
            "relationship-state": "Created",
            "description": "new description"
        }
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Missing required property: owner-id"):
            generate_relationship_item(rel)


if __name__ == '__main__':
    unittest.main()
