import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, InvalidRequestPropertyName, \
    RelationshipAlreadyExists
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_relationship_command import lambda_handler, validate_relationship_id, generate_json_response, \
    get_relationship_from_relationship_owner_table, validate_commitments

io_util = IOUtils(__file__, 'CreateRelationship')


class TestCreateRelationship(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    @patch('lng_aws_clients.session.set_session')
    def test_command_decorator_fail(self, session_mock):
        session_mock.return_value = None

        request_input = io_util.load_data_json('apigateway.request.failure.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    @patch('create_relationship_command.create_relationship_command')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self, mock_session, mock_event_store_put_item, mock_create_relationship_command_response):
        mock_session.return_value = None
        mock_event_store_put_item.return_value = None
        create_relationship_command_response = io_util.load_data_json(
            'expected_create_relationship_response_valid.json')
        mock_create_relationship_command_response.return_value = create_relationship_command_response
        request_input = io_util.load_data_json('apigateway.request.json')
        expected_response = io_util.load_data_json('apigateway.response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response, lambda_handler(request_input, MockLambdaContext()))

    @patch('create_relationship_command.get_relationship_from_relationship_owner_table')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_relationship_id_to_short(self, mock_session, mock_response):
        mock_session.return_value = None
        mock_response.return_value = None
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Relationship ID  ||"
                                                                 "Relationship ID should not exceed characters"):
            validate_relationship_id("")

    @patch('create_relationship_command.get_relationship_from_relationship_owner_table')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_relationship_id_to_long(self, mock_session, mock_response):
        mock_session.return_value = None
        mock_response.return_value = None
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Relationship ID  ||"
                                                                 "Relationship ID should not exceed 40 characters"):
            validate_relationship_id("{:41}".format("relationshipId"))

    @patch('create_relationship_command.get_relationship_from_relationship_owner_table')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_relationship_id_invalid_chars(self, mock_session, mock_response):
        mock_session.return_value = None
        mock_response.return_value = None
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Relationship ID  Ab1-#||"
                                                                 "Relationship ID contains invalid characters"):
            validate_relationship_id("Ab1-#")

    @patch('create_relationship_command.get_relationship_from_relationship_owner_table')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_relationship_id_only_digits(self, mock_session, mock_response):
        mock_session.return_value = None
        mock_response.return_value = None
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Relationship ID  161||"
                                                                 "Relationship ID cannot contain only digits"):
            validate_relationship_id("161")

    @patch('create_relationship_command.get_relationship_from_relationship_owner_table')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_relationship_id_already_exists(self, mock_session, mock_response):
        mock_session.return_value = None
        mock_response.return_value = {'relationship-id': 'notNone'}
        with self.assertRaisesRegex(RelationshipAlreadyExists, "Invalid Relationship ID  161||"
                                                               "Relationship ID already exists"):
            validate_relationship_id("asdf161")

    @patch('create_relationship_command.get_relationship_from_relationship_owner_table')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_relationship_id_is_valid(self, mock_session, mock_response):
        mock_session.return_value = None
        mock_response.return_value = None
        self.assertEqual(None, validate_relationship_id("judges"))

    @patch("lng_datalake_commands.command_wrapper.get_required_response_schema_keys")
    @patch("lng_datalake_commands.command_wrapper.get_optional_response_schema_keys")
    def test_generate_json_response_success(self,
                                            mock_command_wrapper_get_optional,
                                            mock_command_wrapper_get_required):
        owner_resp = io_util.load_data_json("dynamodb.relationship_owner_response.transformed.valid.json")
        mock_command_wrapper_get_required.return_value = io_util.load_data_json(
            "schema.required_response_schema_keys.json")
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json(
            "schema.optional_response_schema_keys.json")
        expected_response = {'relationship-id': 'newjudges', 'owner-id': 151, 'relationship-type': 'Metadata',
                             'description': '',
                             'collection-commitments': ['two-version-id-regression-1560443998'],
                             'catalog-commitments': ['102']}
        response = generate_json_response(owner_resp)
        self.assertDictEqual(expected_response, response)

    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_missing_required_property(self,
                                                              mock_command_wrapper_get_optional,
                                                              mock_command_wrapper_get_required):
        mock_command_wrapper_get_required.return_value = io_util.load_data_json(
            "schema.required_response_schema_keys.json")
        mock_command_wrapper_get_optional.return_value = io_util.load_data_json(
            "schema.optional_response_schema_keys.json")

        request = {
            "relationship-id": "bad"
        }

        with self.assertRaisesRegex(InternalError, "Missing required property:"):
            generate_json_response(request)

    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_relationship_from_relationship_owner_table_client_error(self, mock_get_item):
        mock_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                 'Error': {
                                                     'Code': 'Unit Test',
                                                     'Message': 'This is a mock'}},
                                                "FAKE")
        with self.assertRaisesRegex(InternalError,
                                    "InternalError||Unable to get item from Owner Table||An error occurred"
                                    "(Unit Test) when calling the FAKE operation: This is a mock"):
            get_relationship_from_relationship_owner_table("some-key")

    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_relationship_from_relationship_owner_table_unhandled_exception(self, mock_get_item):
        mock_get_item.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_relationship_from_relationship_owner_table("some-key")

    @patch('lng_datalake_dal.relationship_owner_table.RelationshipOwnerTable.get_item')
    def test_get_relationship_from_relationship_owner_table(self, mock_get_item):
        mock_get_item.return_value = None
        self.assertEqual(None, get_relationship_from_relationship_owner_table("some-key"))

    def test_validate_empty_commitments(self):
        self.assertEqual(None, validate_commitments(None, None))

    @patch('service_commons.relationship_command.validate_collection_commitments')
    @patch('service_commons.relationship_command.validate_catalog_commitments')
    def test_validate_commitments_objects(self, mock_cats, mock_colls):
        mock_colls.return_value = None
        mock_cats.return_value = None
        validate_commitments(['Notempty'], ['Notempty'])


if __name__ == '__main__':
    unittest.main()
