import os
import unittest
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_owner_event_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateOwnerEventHandler')

table_dict = {
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_store_table',
}


class TestCreateOwnerEventHandler(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda handler
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch("lng_aws_clients.apigateway.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self,
                            mock_session,
                            mock_apigateway,
                            mock_dynamodb_get_client,
                            mock_timestamp,
                            mock_sns_get_client
                            ):
        mock_session.return_value = None
        mock_apigateway.return_value.get_api_key.return_value = io_util.load_data_json(
            "apiclient.get_api_key_response.json")
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_timestamp.return_value = "2019-04-10T12:00:00.000Z"

        self.assertEqual(
            create_owner_event_handler.lambda_handler(io_util.load_data_json("valid_event.json"),
                                                      MockLambdaContext()), event_handler_status.SUCCESS)

        mock_apigateway.return_value.get_api_key.assert_called_once()
        mock_dynamodb_get_client.return_value.put_item.assert_called_with(
            ConditionExpression='attribute_not_exists(OwnerID)',
            Item=io_util.load_data_json('expected_owner_put.json'),
            TableName='fake_owner_table'
        )


if __name__ == '__main__':
    unittest.main()
