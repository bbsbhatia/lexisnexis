import unittest
from unittest.mock import patch
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
import os
from get_owner_command import lambda_handler

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetOwner')


class TestGetOwner(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda_handler - validates an owner is returned
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_get_owner_valid(self, session_mock, mocked_get_client):
        session_mock.return_value = None
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mocked_get_client.return_value.get_item.return_value = io_util.load_data_json('lng.dynamodb.response_valid.json')

        request_input = io_util.load_data_json('apigateway.request_valid.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()), expected_response)


if __name__ == '__main__':
    unittest.main()
