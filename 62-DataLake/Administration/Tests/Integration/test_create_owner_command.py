import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_owner_command import lambda_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateOwner')

table_dict = {
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_store_table',
}

asset_group = 'AWSTEST'


class TestCreateOwner(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda handler
    @patch.dict(os.environ, table_dict)
    @patch("lng_aws_clients.apigateway.get_client")
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_create_owner_success(self,
                                  session_mock,
                                  mock_dynamodb_get_client,
                                  mock_apigateway, path=io_util.lambda_path):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = {}
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_dynamodb_get_client.return_value.update_item.return_value = \
            {
                "Attributes": {
                    "AtomicCounter": {
                        "N": "1"
                    }
                }
            }
        apiclient_api_key_response = io_util.load_data_json('apiclient.create_api_key_response.json')
        mock_apigateway.return_value.create_api_key.return_value = apiclient_api_key_response

        api_get_usage_plans_resp = io_util.load_data_json('apigateway.get_usage_plans_response.json')
        mock_apigateway.return_value.get_paginator.return_value.paginate.return_value = api_get_usage_plans_resp[
            'pageWrap']

        command_wrapper.WORKING_DIRECTORY = path
        request_input = io_util.load_data_json('apigateway.request_input.json')
        expected_response_output = io_util.load_data_json('apigateway.expected_response.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with patch('create_owner_command.asset_group', asset_group):
                actual_response = lambda_handler(request_input, MockLambdaContext())
                self.assertDictEqual(actual_response, expected_response_output)

    # + lambda handler with prefix
    @patch.dict(os.environ, table_dict)
    @patch("lng_aws_clients.apigateway.get_client")
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_create_owner_success_with_prefix(self,
                                              session_mock,
                                              mock_dynamodb_get_client,
                                              mock_apigateway, path=io_util.lambda_path):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = {}
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_dynamodb_get_client.return_value.update_item.return_value = \
            {
                "Attributes": {
                    "AtomicCounter": {
                        "N": "1"
                    }
                }
            }
        apiclient_api_key_response = io_util.load_data_json('apiclient.create_api_key_response.json')
        mock_apigateway.return_value.create_api_key.return_value = apiclient_api_key_response

        api_get_usage_plans_resp = io_util.load_data_json('apigateway.get_usage_plans_response.json')
        mock_apigateway.return_value.get_paginator.return_value.paginate.return_value = api_get_usage_plans_resp[
            'pageWrap']

        command_wrapper.WORKING_DIRECTORY = path
        request_input = io_util.load_data_json('apigateway.request_input1.json')
        expected_response_output = io_util.load_data_json('apigateway.expected_response2.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with patch('create_owner_command.asset_group', asset_group):
                actual_response = lambda_handler(request_input, MockLambdaContext())
                self.assertDictEqual(actual_response, expected_response_output)


if __name__ == '__main__':
    unittest.main()
