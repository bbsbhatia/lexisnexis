import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from list_owners_command import lambda_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListOwners')


class TestListOwners(unittest.TestCase):
    # setup and teardown
    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + list_owners_command with max-results and pagination - valid
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_list_owners_lambda_handler_success(self, mock_session,
                                                mock_dynamodb_client):
        mock_session.return_value = None
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('dynamodb_response_with_pagination.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway_input_with_optional_props.json')
        expected_response = io_util.load_data_json('apigateway_response_with_pagination.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(expected_response, lambda_handler(input_data, MockLambdaContext))

    # + list_owners_command the optional parameters - valid
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_list_owners_lambda_handler_success_all_results(self, mock_session,
                                                            mock_dynamodb_client):
        mock_session.return_value = None
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('dynamodb_response_with_all_results.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway_input_without_optional_props.json')
        expected_response = io_util.load_data_json('apigateway_response_with_all_results.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response, lambda_handler(input_data, MockLambdaContext))


if __name__ == "__main__":
    unittest.main()
