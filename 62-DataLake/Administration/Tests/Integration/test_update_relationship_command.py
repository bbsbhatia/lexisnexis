import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper, command_validator
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from update_relationship_command import lambda_handler

asset_group = 'AWSTEST'

io_util = IOUtils(__file__, 'UpdateRelationship')

table_dict = {
    'RELATIONSHIP_OWNER_DYNAMODB_TABLE': 'fake_relationship_owner_table',
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table',
    'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'
}


class TestUpdateRelationship(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + valid update_relationship_command request
    @patch.dict(os.environ, table_dict)
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_update_relationship_success(self,
                                         session_mock,
                                         mock_dynamodb_get_client, path=io_util.lambda_path):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.GetItem_2.json')
            if TableName == 'fake_relationship_owner_table':
                return io_util.load_data_json('dynamodb.GetItem_1.json')
            return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.batch_get_item.return_value = io_util.load_data_json(
            'dynamodb.BatchGetItem.json')

        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.PutItem_1.json')

        command_wrapper.WORKING_DIRECTORY = path
        command_validator.WORKING_DIRECTORY = path
        request_input = io_util.load_data_json('apigateway_request.json')
        expected_response_output = io_util.load_data_json('apigateway_response.json')

        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            actual_response = lambda_handler(request_input, MockLambdaContext)
            self.assertDictEqual(actual_response, expected_response_output)


if __name__ == '__main__':
    unittest.main()
