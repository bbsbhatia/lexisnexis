import os
import unittest
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_relationship_event_handler import lambda_handler

io_util = IOUtils(__file__, 'CreateRelationshipEventHandler')

table_dict = {
    'RELATIONSHIP_OWNER_DYNAMODB_TABLE': 'fake_relationship_owner_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_store_table',
}


class TestCreateOwnerEventHandler(unittest.TestCase):
    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda handler
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self,
                            mock_session,
                            mock_dynamodb_get_client,
                            mock_sns_get_client
                            ):
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        mock_sns_get_client.return_value.get_topic_attributes.return_value = io_util.load_data_json(
            "sns.GetTopicAttributes.json")
        self.assertEqual(
            lambda_handler(io_util.load_data_json("valid_event.json"),
                                                      MockLambdaContext()), event_handler_status.SUCCESS)


if __name__ == '__main__':
    unittest.main()
