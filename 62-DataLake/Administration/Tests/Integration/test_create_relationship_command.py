import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_relationship_command import lambda_handler

io_util = IOUtils(__file__, 'CreateRelationship')

table_dict = {
    'RELATIONSHIP_OWNER_DYNAMODB_TABLE': 'fake_relationship_owner_table',
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_store_table',
    'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table',
    'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'
}


class TestCreateRelationship(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + create relationship success
    @patch.dict(os.environ, table_dict)
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_create_relationship_success(self,
                                         session_mock,
                                         mock_dynamodb_get_client, path=io_util.lambda_path):

        def mock_query_items(TableName, PaginationConfig=None, IndexName=None, **kwargs):  # NOSONAR
            data = None

            class paginate_object():
                @staticmethod
                def build_full_result():
                    return data

            if TableName == 'fake_owner_table':
                data = {"Items": [{"OwnerID": {"N": "103"}}]}
                return paginate_object
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate = mock_query_items
        mock_dynamodb_get_client.return_value.get_item.return_value = {}
        mock_dynamodb_get_client.return_value.batch_get_item.return_value = io_util.load_data_json(
            'dynamo.batch_get_item_response.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        command_wrapper.WORKING_DIRECTORY = path
        request_input = io_util.load_data_json('apigateway.request_input.json')
        expected_response_output = io_util.load_data_json('apigateway.expected_response.json')

        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            actual_response = lambda_handler(request_input, MockLambdaContext)
            self.assertDictEqual(actual_response, expected_response_output)

    # + create relationship success without optional params
    @patch.dict(os.environ, table_dict)
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_create_relationship_success_no_optional(self,
                                                     session_mock,
                                                     mock_dynamodb_get_client, path=io_util.lambda_path):

        def mock_query_items(TableName, PaginationConfig=None, IndexName=None, **kwargs):  # NOSONAR
            data = None

            class paginate_object():
                @staticmethod
                def build_full_result():
                    return data

            if TableName == 'fake_owner_table':
                data = {"Items": [{"OwnerID": {"N": "103"}}]}
                return paginate_object
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate = mock_query_items
        mock_dynamodb_get_client.return_value.get_item.return_value = {}
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        command_wrapper.WORKING_DIRECTORY = path
        request_input = io_util.load_data_json('apigateway.request_input_no_optional.json')
        expected_response_output = io_util.load_data_json('apigateway.expected_response_no_optional.json')

        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            actual_response = lambda_handler(request_input, MockLambdaContext)
            self.assertDictEqual(actual_response, expected_response_output)


if __name__ == '__main__':
    unittest.main()
