import os
import unittest
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import update_owner_event_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateOwnerEventHandler')

table_dict = {
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_store_table',
}


class TestUpdateOwnerEventHandler(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, table_dict)
    def setUpClass(self):  # NOSONAR
        pass

    @classmethod
    def tearDownClass(self):  # NOSONAR
        TableCache.clear()

    # + lambda handler - name and email
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_dal.owner_table.OwnerTable.put_item')
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self,
                            mock_session,
                            mock_get_owner,
                            mock_put_owner,
                            mock_dynamodb_get_client,
                            mock_sns_get_client
                            ):
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        mock_sns_get_client.return_value.get_topic_attributes.return_value = io_util.load_data_json(
            "sns.GetTopicAttributes.json")

        mock_get_owner.return_value = io_util.load_data_json('dynamo.owner_get_owner.json')

        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = io_util.load_data_json('tracking.query.json')

        self.assertEqual(
            update_owner_event_handler.lambda_handler(io_util.load_data_json("valid_event.json"),
                                                      MockLambdaContext()), event_handler_status.SUCCESS)

        expected_put_owner = io_util.load_data_json('dynamo.owner_put_owner.json')

        mock_put_owner.called_once_with(expected_put_owner, None)

    # + lambda handler - name only
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_dal.owner_table.OwnerTable.put_item')
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_name_only(self,
                                      mock_session,
                                      mock_get_owner,
                                      mock_put_owner,
                                      mock_dynamodb_get_client,
                                      mock_sns_get_client
                                      ):
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = io_util.load_data_json('tracking.query.json')

        mock_sns_get_client.return_value.get_topic_attributes.return_value = io_util.load_data_json(
            "sns.GetTopicAttributes.json")

        mock_get_owner.return_value = io_util.load_data_json('dynamo.owner_get_owner.json')

        mock_put_owner.return_value = None

        self.assertEqual(
            update_owner_event_handler.lambda_handler(io_util.load_data_json("valid_event_name_only.json"),
                                                      MockLambdaContext()), event_handler_status.SUCCESS)

        expected_put_owner = io_util.load_data_json('dynamo.owner_put_owner.json')

        mock_put_owner.called_once_with(expected_put_owner, None)

    # + lambda handler - email only
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_datalake_dal.owner_table.OwnerTable.put_item')
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_email_only(self,
                                       mock_session,
                                       mock_get_owner,
                                       mock_put_owner,
                                       mock_dynamodb_get_client,
                                       mock_sns_get_client
                                       ):
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        mock_sns_get_client.return_value.get_topic_attributes.return_value = io_util.load_data_json(
            "sns.GetTopicAttributes.json")

        mock_get_owner.return_value = io_util.load_data_json('dynamo.owner_get_owner.json')

        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = io_util.load_data_json('tracking.query.json')

        mock_put_owner.return_value = None

        self.assertEqual(
            update_owner_event_handler.lambda_handler(io_util.load_data_json("valid_event_email_only.json"),
                                                      MockLambdaContext()), event_handler_status.SUCCESS)

        expected_put_owner = io_util.load_data_json('dynamo.owner_put_owner.json')

        mock_put_owner.called_once_with(expected_put_owner, None)


if __name__ == '__main__':
    unittest.main()
