import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from remove_owner_command import lambda_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveOwner')

table_dict = {
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_store_table',
    'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
    'TRACKING_DYNAMODB_TABLE': 'fake_tracking_table',
    'COLLECTION_DYNAMODB_OWNER_ID_AND_NAME_INDEX': 'collection-by-owner-and-name-index',
    'RELATIONSHIP_OWNER_DYNAMODB_TABLE': 'fake_relationship_owner_table'
}


class TestRemoveOwner(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda handler
    @patch.dict(os.environ, table_dict)
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_remove_owner_success(self, session_mock, mock_dynamodb_get_client):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [
            {},
            {},
            io_util.load_data_json('tracking.query.json')]
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamo.event_store_get_response.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request_input = io_util.load_data_json('apigateway.request_input.json')
        expected_response_output = io_util.load_data_json('apigateway.expected_response.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(request_input, MockLambdaContext()), expected_response_output)


if __name__ == '__main__':
    unittest.main()
