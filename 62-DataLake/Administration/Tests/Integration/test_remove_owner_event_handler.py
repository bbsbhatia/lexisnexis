import os
import unittest
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import remove_owner_event_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveOwnerEventHandler')

table_dict = {
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_store_table',
    'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
    'TRACKING_DYNAMODB_TABLE': 'fake_tracking_table',
    'COLLECTION_DYNAMODB_OWNER_ID_AND_NAME_INDEX': 'collection-by-owner-and-name-index'
}


class TestRemoveOwnerEventHandler(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda handler
    @patch("lng_aws_clients.apigateway.get_client")
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self,
                            mock_session,
                            mock_dynamodb_get_client,
                            mock_sns_get_client,
                            mock_apigateway_get_client
                            ):
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamo.owner_query_item_response.json')
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.side_effect = [
            io_util.load_data_json('tracking.query.json'),
            io_util.load_data_json('dynamo.collection_query_item_response.json')]
        mock_sns_get_client.return_value.get_topic_attributes.return_value = io_util.load_data_json(
            "sns.GetTopicAttributes.json")
        mock_apigateway_get_client.return_value.delete_api_key.return_value = \
            io_util.load_data_json('apigateway.delete_api_key.json')
        self.assertEqual(
            remove_owner_event_handler.lambda_handler(io_util.load_data_json("valid_event.json"),
                                                      MockLambdaContext()), event_handler_status.SUCCESS)


if __name__ == '__main__':
    unittest.main()
