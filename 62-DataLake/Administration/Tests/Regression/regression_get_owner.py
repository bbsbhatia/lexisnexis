import json
import os
import unittest

from lng_datalake_client.Admin.get_owner import get_owner

__author__ = "Chuck Nelson"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.1"


class TestGetOwner(unittest.TestCase):
    # make changes here for custom owner
    default_owner_id = int(os.getenv("OWNER_ID", 1))
    default_owner_name = os.getenv("OWNER_NAME", "Mark.")

    default_email_list = os.getenv("EMAIL_DISTRIBUTION_LIST", ["bogus@lexisnexis.com"])
    if isinstance(default_email_list, str):
        default_email_list = json.loads(default_email_list)

    # + Test the 200 status code and owner properties for GET_OWNER
    def test_get_owner(self, owner_data=None):
        if not owner_data:
            owner_data = {}
        expected_response = \
            {
                "owner-id": int(owner_data.get("owner-id", self.default_owner_id)),
                "owner-name": owner_data.get('owner-name', self.default_owner_name),
                "email-distribution": owner_data.get('email-distribution',
                                                     self.default_email_list)
            }
        if owner_data.get("collection-prefixes"):
            expected_response["collection-prefixes"] = owner_data["collection-prefixes"]

        request_dict = {"owner-id": int(owner_data.get("owner-id", self.default_owner_id))}
        response = get_owner(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        # test the response body
        response_dict = response.json()
        self.assertIn('context', response_dict)
        self.assertTrue(isinstance(response_dict['context'].get('request-id'), str))
        self.assertTrue(isinstance(response_dict['context'].get("api-id"), str))
        self.assertTrue(isinstance(response_dict['context'].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'],
                         response_dict['context'].get('stage'))
        self.assertIn('owner', response_dict)
        self.assertDictEqual(expected_response, response_dict["owner"])
        return response_dict["owner"]

    # - get_owner with invalid owner id
    def test_get_owner_invalid_owner_id(self, owner_data=None):
        if not owner_data:
            owner_data = {}

        invalid_owner_id = int(owner_data.get("owner-id", "-100"))

        request_dict = {'owner-id': invalid_owner_id}
        response = get_owner(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchOwner")
        self.assertEqual(json_error_resp['message'], "Invalid Owner ID {}".format(invalid_owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Owner ID does not exist")

    # - get_owner with invalid x-api-key
    def test_get_owner_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "owner-id": self.default_owner_id
        }

        response = get_owner(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - get_owner with invalid content-type
    def test_get_owner_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "owner-id": self.default_owner_id
        }

        response = get_owner(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
