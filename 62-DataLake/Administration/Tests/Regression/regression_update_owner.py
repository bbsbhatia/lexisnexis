import os
import random
import unittest
from datetime import datetime

from lng_datalake_client.Admin.get_owner import get_owner
from lng_datalake_client.Admin.update_owner import update_owner

__author__ = "Jason Feng"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestUpdateOwner(unittest.TestCase):
    # make changes here for custom owner
    default_owner_id = int(os.getenv("OWNER_ID", 195))

    # valid - update owner 202
    def test_update_owner(self, owner_data=None):
        if not owner_data:
            owner_data = {}

        input_owner_name = owner_data.get("owner-name", "updated_owner_name_{}".format(datetime.now().isoformat()))
        input_email_list = owner_data.get("email-distribution", ["ABC_123@lexisnexis.com",
                                                                 "abc_{}@lexisnexis.com".format(
                                                                     random.randrange(1, 100))])
        request_dict = \
            {
                "body": {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/owner-name",
                        "value": input_owner_name
                    }, {
                        "op": "replace",
                        "path": "/email-distribution",
                        "value": input_email_list
                    }]}
                ,
                "owner-id": owner_data.get("owner-id", self.default_owner_id)
            }
        response = update_owner(request_dict)
        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        expected_response = \
            {
                "owner-id": request_dict["owner-id"],
                "owner-name": input_owner_name,
                "email-distribution": input_email_list
            }
        if owner_data.get("collection-prefixes"):
            expected_response["collection-prefixes"] = owner_data["collection-prefixes"]

        self.assertEqual(202, response.status_code)
        self.assertIn('context', response_dict)
        self.assertTrue(isinstance(response_dict['context'].get('request-id'), str))
        self.assertTrue(isinstance(response_dict['context'].get("api-id"), str))
        self.assertTrue(isinstance(response_dict['context'].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'],
                         response_dict['context'].get('stage'))
        self.assertIn("owner", response_dict)
        response_body_dict = response_dict["owner"]
        self.assertDictEqual(expected_response, response_body_dict)
        return response_body_dict

    # invalid - owner id
    def test_update_owner_invalid_owner_id(self):
        request_dict = \
            {
                'body': {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/owner-name",
                        "value": "update_owner_name_{}".format(datetime.now().isoformat())
                    }, {
                        "op": "replace",
                        "path": "/email-distribution",
                        "value": ["ABC_123@lexisnexis.com",
                                  "abc_{}@lexisnexis.com".format(random.randrange(1, 100))]
                    }]
                },
                "owner-id": -1
            }

        response = update_owner(request_dict)
        # test the status code 404
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchOwner")
        self.assertEqual(json_error_resp['message'], "Invalid Owner ID {}".format(request_dict['owner-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Owner ID does not exist")

    # invalid - missing key, auth error
    def test_update_owner_invalid_x_api_key(self, owner_data=None):
        if not owner_data:
            owner_data = {}

        request_dict = \
            {
                'headers': {
                    'Content-type': 'application/json'
                },
                'body': {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/owner-name",
                        "value": "update_owner_name_{}".format(datetime.now().isoformat())
                    }, {
                        "op": "replace",
                        "path": "/email-distribution",
                        "value": ["ABC_123@lexisnexis.com",
                                  "abc_{}@lexisnexis.com".format(random.randrange(1, 100))]
                    }]
                },
                "owner-id": owner_data.get("owner-id", self.default_owner_id)
            }
        response = update_owner(request_dict)
        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # invalid - wrong key, auth error
    def test_update_owner_incorrect_x_api_key(self, owner_data=None, api_key=None):
        if not owner_data:
            owner_data = {}

        owner_id = owner_data.get("owner-id", self.default_owner_id)
        request_dict = \
            {
                'headers': {
                    'Content-type': 'application/json',
                    'x-api-key': api_key
                },
                'body': {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/owner-name",
                        "value": "update_owner_name_{}".format(datetime.now().isoformat())
                    }, {
                        "op": "replace",
                        "path": "/email-distribution",
                        "value": ["ABC_123@lexisnexis.com",
                                  "abc_{}@lexisnexis.com".format(random.randrange(1, 100))]
                    }]
                },
                "owner-id": owner_id
            }

        response = update_owner(request_dict)
        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")

    # invalid - wrong content type
    def test_update_owner_invalid_content_type(self, owner_data=None):
        if not owner_data:
            owner_data = {}

        key = os.environ['X-API-KEY']
        request_dict = \
            {
                "headers": {
                    "Content-type": "application/atom+xml",
                    "x-api-key": key
                },
                "body": {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/owner-name",
                        "value": "update_owner_name_{}".format(datetime.now().isoformat())
                    }, {
                        "op": "replace",
                        "path": "/email-distribution",
                        "value": ["ABC_123@lexisnexis.com",
                                  "abc_{}@lexisnexis.com".format(random.randrange(1, 100))]
                    }]
                },
                "owner-id": owner_data.get("owner-id", self.default_owner_id)
            }
        response = update_owner(request_dict)
        # test the status code 415
        self.assertEqual(415, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # invalid - update same data
    def test_update_owner_same_data(self, owner_data=None):

        if not owner_data:
            owner_data = {}

        get_owner_response_dict = get_owner({'owner-id': owner_data.get("owner-id", self.default_owner_id)}).json()[
            "owner"]
        request_dict = \
            {
                'body': {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/owner-name",
                        "value": get_owner_response_dict["owner-name"]
                    }, {
                        "op": "replace",
                        "path": "/email-distribution",
                        "value": get_owner_response_dict["email-distribution"]
                    }]
                },
                "owner-id": owner_data.get("owner-id", self.default_owner_id)
            }
        response = update_owner(request_dict)
        # test the status code 422
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "No data changes")

    # invalid - wrong email format
    def test_update_owner_invalid_email_format(self, owner_data=None):
        if not owner_data:
            owner_data = {}

        input_email_list = ["ABC_123_53:37@lexisnexis.com"]
        request_dict = \
            {
                'body': {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/owner-name",
                        "value": "update_owner_name_{}".format(datetime.now().isoformat())
                    }, {
                        "op": "replace",
                        "path": "/email-distribution",
                        "value": input_email_list
                    }]
                },
                'owner-id': owner_data.get("owner-id", self.default_owner_id)
            }

        response = update_owner(request_dict)
        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "The following emails are invalid {}".format(input_email_list))


if __name__ == '__main__':
    unittest.main()
