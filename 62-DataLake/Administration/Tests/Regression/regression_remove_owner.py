import datetime
import json
import os
import unittest

from lng_datalake_client.Admin.remove_owner import remove_owner
from lng_datalake_constants import relationship_status

__author__ = "Chuck Nelson"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestRemoveOwner(unittest.TestCase):
    # set the default variables here
    default_owner_id = int(os.getenv("OWNER_ID", 195))
    default_owner_name = os.getenv("OWNER_NAME", "updated_owner_name_2018-09-24T16:08:30.014272")
    default_relationship_state = os.getenv("RELATIONSHIP_STATE", relationship_status.REMOVED)
    default_relationship_id = os.getenv("RELATIONSHIP_ID",
                                        "createrelationshipid{}".format(
                                            datetime.datetime.now().strftime("%y%m%d%H%M%S%f")))

    default_email_list = os.getenv("EMAIL_DISTRIBUTION_LIST", ["bogus@lexisnexis.com"])
    if isinstance(default_email_list, str):
        default_email_list = json.loads(default_email_list)

    # + Test the 202 status code and owner properties for REMOVE_OWNER
    def test_remove_owner(self, owner_data=None):
        if not owner_data:
            owner_data = {}
        expected_response = \
            {
                "owner-id": owner_data.get("owner-id", self.default_owner_id),
                "owner-name": owner_data.get("owner-name", self.default_owner_name),
                "email-distribution": owner_data.get("email-distribution",
                                                     self.default_email_list)
            }

        if owner_data.get("collection-prefixes"):
            expected_response["collection-prefixes"] = owner_data["collection-prefixes"]

        request = {
            "owner-id": owner_data.get("owner-id", self.default_owner_id)
        }
        response = remove_owner(request)

        response_dict = response.json()

        # test the status code 202
        self.assertEqual(202, response.status_code)
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertIn("stage", response_dict["context"])
        self.assertEqual(os.environ["STAGE"],
                         response_dict["context"].get("stage"))

        # test the response body
        self.assertIn("owner", response_dict)
        self.assertDictEqual(expected_response, response_dict["owner"])
        return response.json()["owner"]

    # - remove_owner with invalid owner id
    def test_remove_owner_invalid_owner_id(self, owner_data=None):
        if not owner_data:
            owner_data = {}

        response = remove_owner({'owner-id': owner_data.get('owner-id', -100)})

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchOwner")
        self.assertEqual(json_error_resp['message'], "Invalid Owner ID {}".format(owner_data.get('owner-id', -100)))
        self.assertEqual(json_error_resp['corrective-action'], "Owner ID does not exist")

    # - remove_owner with invalid x-api-key
    def test_remove_owner_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "owner-id": 1
        }

        response = remove_owner(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # invalid - wrong key, auth error
    def test_remove_owner_incorrect_x_api_key(self, owner_data=None, api_key=None):
        if not owner_data:
            owner_data = {}

        owner_id = owner_data.get("owner-id", self.default_owner_id)
        request_dict = \
            {
                'headers': {
                    'Content-type': 'application/json',
                    'x-api-key': api_key
                },
                "owner-id": owner_id
            }

        response = remove_owner(request_dict)
        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")

    # - remove_owner with invalid content-type
    def test_remove_owner_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "owner-id": 1
        }

        response = remove_owner(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - remove owner with existing collection
    def test_remove_owner_with_collection(self, owner_data=None):
        if not owner_data:
            owner_data = {}

        response = remove_owner({'owner-id': owner_data.get('owner-id', self.default_owner_id)})

        # test the status code 409 - ForbiddenStateTransitionError
        self.assertEqual(409, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "ForbiddenStateTransitionError")
        self.assertEqual(json_error_resp['message'], "Owner has at least one non terminated collection")
        self.assertEqual(json_error_resp['corrective-action'], "Remove all collections for this owner")

    # - remove owner with existing relationship
    def test_remove_owner_with_relationship(self, owner_data=None, relationship_data=None):
        if not owner_data:
            owner_data = {}
        if not relationship_data:
            relationship_data = {
                'relationship-id': self.default_relationship_id,
                'relationship-state': self.default_relationship_state
            }

        response = remove_owner({'owner-id': owner_data.get('owner-id', self.default_owner_id)})

        # test the status code 409 - ForbiddenStateTransitionError
        self.assertEqual(409, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "ForbiddenStateTransitionError")
        self.assertEqual(json_error_resp['message'], "RelationshipID {0} must be in a {1} state to be removed".format(
            relationship_data['relationship-id'],
            relationship_data['relationship-state']))
        self.assertEqual(json_error_resp['corrective-action'], "Remove all relationships before removing owner")

        if __name__ == '__main__':
            unittest.main()
