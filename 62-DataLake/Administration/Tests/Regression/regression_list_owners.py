import os
import unittest

from lng_datalake_client.Admin.list_owners import list_owners

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestListOwners(unittest.TestCase):
    default_invalid_max_results = 1001

    # + Test the 200 status code and response for all results
    def test_list_owners_all_results(self):
        request_input = {}
        response = list_owners(request_input)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        # test the response body
        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_dict["context"]["stage"])
        self.assertIn("owners", response_dict)
        self.assertGreaterEqual(len(response_dict["owners"]), 0)
        self.assertTrue(isinstance(response_dict["item-count"], int))

    # + Test the 200 status code and response for pagination
    def test_list_owners_pagination(self):
        input_dict_1 = {"max-items": 1}
        response_1 = list_owners(input_dict_1)

        # test the status code 200
        self.assertEqual(200, response_1.status_code)

        # test the response body
        response_1 = response_1.json()
        self.assertIn("context", response_1)
        self.assertTrue(isinstance(response_1["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_1["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_1["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_1["context"]["stage"])
        self.assertIn("owners", response_1)
        self.assertEqual(len(response_1["owners"]), 1)
        self.assertIn("next-token", response_1)
        self.assertTrue(isinstance(response_1["item-count"], int))

        # second request to test pagination
        input_dict_2 = {"max-items": 1,
                        "next-token": response_1["next-token"]}
        response_2 = list_owners(input_dict_2)

        # test the status code 200
        self.assertEqual(200, response_2.status_code)

        # test the response body
        response_2 = response_2.json()
        self.assertIn("context", response_2)
        self.assertTrue(isinstance(response_2["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_2["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_2["context"].get("resource-id"), str))
        self.assertEqual(os.environ["STAGE"], response_2["context"]["stage"])
        self.assertIn("owners", response_2)
        self.assertLessEqual(1, len(response_2["owners"]))
        self.assertTrue(isinstance(response_2["item-count"], int))

        # test that the responses are different on pagination
        self.assertNotEqual(response_1["owners"], response_2["owners"])

        # - list assets with invalid max-results value

    def test_list_owners_pagination_invalid(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        invalid_max_items = input_dict.get("max-items", self.default_invalid_max_results)
        request_input = {
            "max-items": invalid_max_items
        }

        response = list_owners(request_input)

        # test the status code 400
        self.assertEqual(400, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid max-items value: {0}".format(invalid_max_items))
        self.assertEqual(json_error_resp['corrective-action'], "Value must be between 1 and 1000")

    # - list_owners with invalid x-api-key
    def test_list_owners_invalid_x_api_key(self):
        input_dict = {
            "headers": {
                "Content-type": "application/json"
            }
        }

        response = list_owners(input_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - list_owners with invalid content-type
    def test_list_owners_invalid_content_type(self):
        key = os.environ["X-API-KEY"]
        input_dict = {
            "headers": {
                "Content-type": "application/atom+xml",
                "x-api-key": key
            }
        }

        response = list_owners(input_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - list owners with invalid max-results value
    def test_list_invalid_owners_pagination_diff_max_items(self):
        input_dict = {
            "max-items": 1,
            "next-token": "eyJtYXgtaXRlbXMiOiA0LCAicGFnaW5hdGlvbi10b2tlbiI6ICJhYmNkPSJ9"
        }
        response = list_owners(input_dict)
        self.assertEqual(400, response.status_code)
