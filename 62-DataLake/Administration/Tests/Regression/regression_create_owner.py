import datetime
import json
import os
import unittest

from lng_datalake_client.Admin.create_owner import create_owner

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestCreateOwner(unittest.TestCase):
    # set the default variables here
    default_owner_name = os.getenv("OWNER_NAME", "regression_owner_{}".format(datetime.datetime.now().isoformat()))
    default_duplicate_owner_id = int(os.getenv("DUP_OWNER_ID", "202"))

    default_email_list = os.getenv("EMAIL_DISTRIBUTION_LIST", ["abc_123@lexisnexis.com"])
    if isinstance(default_email_list, str):
        default_email_list = json.loads(default_email_list)

    def __init__(self, *args, **kwargs):
        super(TestCreateOwner, self).__init__(*args, **kwargs)

    # + test create owner
    def test_create_owner(self, input_data=None):
        if not input_data:
            input_data = {}

        request_dict = {
            "body": {
                "owner-name": input_data.get("owner-name", self.default_owner_name),
                "email-distribution": input_data.get("email-distribution", self.default_email_list)
            }
        }
        # Add optional attributes
        if input_data.get("collection-prefixes"):
            request_dict["body"]["collection-prefixes"] = input_data["collection-prefixes"]

        response = create_owner(request_dict)

        # test the status code - 202
        self.assertEqual(202, response.status_code)

        # test the response body
        response_dict = response.json()
        self.assertIn('context', response_dict)
        self.assertTrue(isinstance(response_dict['context'].get('request-id'), str))
        self.assertTrue(isinstance(response_dict['context'].get("api-id"), str))
        self.assertTrue(isinstance(response_dict['context'].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'],
                         response_dict['context'].get('stage'))
        self.assertIn('owner', response_dict)
        self.assertTrue(isinstance(response_dict['owner'].get('owner-id'), int))
        self.assertEqual(request_dict["body"]["owner-name"], response_dict['owner'].get('owner-name'))
        self.assertListEqual(request_dict["body"]["email-distribution"],
                             response_dict['owner'].get("email-distribution"))
        return response_dict["owner"]

    # - test create owner - invalid owner name - 400
    def test_create_owner_invalid_owner_name(self):
        request_dict = {
            "body": {
                "owner-name": "",
                "email-distribution": ["dummy_email@lexisnexis.com"]
            }
        }
        response = create_owner(request_dict)
        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Owner Name {0}".format(request_dict['body']['owner-name']))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Owner Name should not be empty and length should not exceed 60 characters")

    # - test create owner - invalid email - 400
    def test_create_owner_invalid_email(self, input_data=None):
        if not input_data:
            input_data = {}

        request_dict = {
            "body": {
                "owner-name": input_data.get("owner-name", self.default_owner_name),
                "email-distribution": ["dummy_email"]
            }
        }
        response = create_owner(request_dict)
        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "The following emails are invalid {}".format(request_dict['body']['email-distribution']))

    # - test create owner - empty email list - 400
    def test_create_owner_empty_email_list(self, input_data=None):
        if not input_data:
            input_data = {}

        request_dict = {
            "body": {
                "owner-name": input_data.get("owner-name", self.default_owner_name),
                "email-distribution": []
            }
        }
        response = create_owner(request_dict)
        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['message'], "Request did not match JSON schema")

    # - test create owner - invalid content-type - 415
    def test_create_owner_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                "Content-type": "application/atom+xml",
                "x-api-key": key
            },
            "body": {
                "owner-name": "dummy_owner_name",
                "email-distribution": ["abc_123@lexisnexis.com"]
            }
        }
        response = create_owner(request_dict)
        self.assertEqual(415, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - test create owner - duplicate owner - 409
    def test_create_owner_duplicate(self, owner_data=None):
        if not owner_data:
            owner_data = {}
        request_dict = {
            "body": {
                "owner-name": owner_data.get("owner-name", self.default_owner_name),
                "email-distribution": owner_data.get("email-distribution", self.default_email_list)
            }
        }
        duplicate_owner_id = owner_data.get("owner-id", self.default_duplicate_owner_id)
        response = create_owner(request_dict)
        self.assertEqual(409, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "ConflictingRequestError")
        self.assertEqual(json_error_resp['message'],
                         "Owner already exists with owner-id {} for given owner-name and email-distribution".format(
                             duplicate_owner_id))

    # - test create owner - prefix exceeded length limit - 400
    def test_create_owner_with_invalid_prefix_exceeded_limit_length(self, input_data=None):
        if not input_data:
            input_data = {}

        request_dict = {
            "body": {
                "owner-name": input_data.get("owner-name", self.default_owner_name),
                "email-distribution": input_data.get("email-distribution", self.default_email_list),
                "collection-prefixes": input_data.get("collection-prefixes", ["FPD12345678910"]),

            }
        }

        response = create_owner(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['message'], "Invalid Collection prefix FPD12345678910")
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['corrective-action'],
                         "Collection prefix length should not exceed 10 characters")

    # - test create owner - prefix_registered_by_other_owner - 409
    def test_create_owner_prefix_registered_by_other_owner(self, owner_data=None):
        if not owner_data:
            owner_data = {}
        request_dict = {
            "body": {
                "owner-name": owner_data.get("owner-name", self.default_owner_name),
                "email-distribution": owner_data.get("email-distribution", self.default_email_list),
                "collection-prefixes": owner_data.get("collection-prefixes", ["FPD"]),
            }
        }
        duplicate_owner_id = owner_data.get("owner-id", self.default_duplicate_owner_id)
        response = create_owner(request_dict)
        self.assertEqual(409, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "CollectionPrefixAlreadyExists")
        self.assertEqual(json_error_resp['message'], "Collection prefix already exists")
        self.assertEqual(json_error_resp['corrective-action'],
                         "Owner ID {} owns this prefix".format(duplicate_owner_id))

    # - test create owner - prefix is not valid
    def test_create_owner_with_invalid_prefix(self, input_data=None):
        if not input_data:
            input_data = {}

        request_dict = {
            "body": {
                "owner-name": input_data.get("owner-name", self.default_owner_name),
                "email-distribution": input_data.get("email-distribution", self.default_email_list),
                "collection-prefixes": input_data.get("collection-prefixes", ["A-123"]),

            }
        }

        response = create_owner(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['message'], "Invalid Collection prefix A-123")
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['corrective-action'],
                         "Collection prefix can only contain capital letters and digits, "
                         "Collection prefix cannot contain only digits")


if __name__ == '__main__':
    unittest.main()
