import datetime
import os
import unittest

from lng_datalake_client.Admin.create_relationship import create_relationship

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestCreateRelationship(unittest.TestCase):
    default_relationship_id = os.getenv("RELATIONSHIP_ID",
                                        "createrelationshipid{}".format(
                                            datetime.datetime.now().strftime("%y%m%d%H%M%S%f")))
    default_relationship_type = os.getenv("RELATIONSHIP_TYPE", "Metadata")

    def test_create_relationship(self, input_data=None):
        if not input_data:
            input_data = {}
        stage = os.environ['STAGE']

        request_dict = {
            "relationshipId": input_data.get("relationshipId", self.default_relationship_id),
            "body": {
                "relationship-type": input_data.get("relationship-type", self.default_relationship_type)
            }
        }
        # Add optional attributes
        if input_data.get("collection-commitments"):
            request_dict["body"]["collection-commitments"] = input_data["collection-commitments"]
        if input_data.get("catalog-commitments"):
            request_dict["body"]["catalog-commitments"] = input_data["catalog-commitments"]

        response = create_relationship(request_dict)

        # test the status code - 202
        self.assertEqual(202, response.status_code)
        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("relationship", response_dict)
        self.assertEqual(request_dict['relationshipId'], response_dict['relationship'].get('relationship-id'))
        self.assertEqual(request_dict['body']['relationship-type'],
                         response_dict['relationship'].get('relationship-type'))

    # - invalid relationship-id length
    def test_create_relationship_invalid_relationship_id_length(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        rel_id = input_dict.get("relationshipId", self.default_relationship_id.ljust(41, "x"))

        request_dict = {
            "relationshipId": rel_id,
            "body": {
                "relationship-type": input_dict.get("relationship-type", self.default_relationship_type)
            }
        }
        response = create_relationship(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Relationship ID {0}".format(rel_id))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Relationship ID length should not exceed 40 characters")

    # - invalid classification-type
    def test_create_relationship_invalid_type(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        rel_id = input_dict.get("relationshipId", self.default_relationship_id)
        request_dict = {
            "relationshipId": rel_id,
            "body": {
                "relationship-type": "badtype"
            }
        }
        response = create_relationship(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyName")
        self.assertEqual(json_error_resp['message'], "Request did not match JSON schema")
        self.assertEqual(json_error_resp['corrective-action'],
                         "data must be one of ['Metadata', 'Transformation']")

    def test_create_relationship_error_invalid_relationship_id_bad_chars(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        rel_id = input_dict.get("relationshipId", "{0})".format(self.default_relationship_id))
        request_dict = {
            "relationshipId": rel_id,
            "body": {
                "relationship-type": "Metadata"
            }
        }

        response = create_relationship(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Relationship ID {}".format(request_dict['relationshipId']))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Relationship ID contains invalid characters")

    def test_create_relationship_invalid_x_api_key(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            },
            "relationshipId": "blah",
            "body": {
                "relationship-type": "Metadata"
            }
        }

        response = create_relationship(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    def test_create_relationship_unsupported_media(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            'headers': {
                'Content-type': 'application/xml',
                'x-api-key': key
            },
            "relationshipId": "blah",
            "body": {
                "relationship-type": "Metadata"
            }
        }

        response = create_relationship(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    def test_create_relationship_invalid_relationship_id_duplicate(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "relationshipId": input_dict.get("relationshipId", self.default_relationship_id),
            "body": {
                "relationship-type": input_dict.get("relationship-type", self.default_relationship_type)
            }
        }

        response = create_relationship(request_dict)

        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "RelationshipAlreadyExists")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Relationship ID {}".format(request_dict['relationshipId']))
        self.assertEqual(json_error_resp['corrective-action'], "Relationship ID already exists")


if __name__ == '__main__':
    unittest.main()
