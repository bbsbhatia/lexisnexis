@Library('DevOpsShared@v1') _
pipeline {
    agent any
    parameters {
        string(name: 'assetGroup', description: '[Optional] The name of the asset group to deploy into')
    }
    environment {
        def ASSET_ID = "62"
        def ASSET_NAME = "DataLake"
        def ASSET_AREA_NAME = "Administration"
        def ASSET_GROUP = "${params.assetGroup ?: commons.getBranchName()}"
        def TARGET_ACCOUNT = "288044017584"
    }
    stages {
        stage('Teardown Administration Documentation') {
            steps {
                deleteStack((String) TARGET_ACCOUNT, 'AdministrationApiDocumentation')
            }
        }
        stage('Teardown Administration ApiGateway Stage') {
            steps {
                deleteStack((String) TARGET_ACCOUNT, 'AdministrationApiStage')
            }
        }
        stage('Teardown Administration ApiGateway') {
            steps {
                deleteStack((String) TARGET_ACCOUNT, 'AdministrationApi')
            }
        }
        stage('Teardown Lambdas') {
            steps {
                script {
                    allLambdas = ['GetOwnerCommand',
                                  'ListOwnersCommand',
                                  'CreateOwnerCommand',
                                  'CreateOwnerEventHandler',
                                  'UpdateOwnerCommand',
                                  'UpdateOwnerEventHandler',
                                  'RemoveOwnerCommand',
                                  'RemoveOwnerEventHandler',
                                  'GetAssetCommand',
                                  'ListAssetsCommand',
                                  'CreateRelationshipCommand',
                                  'CreateRelationshipEventHandler',
                                  'UpdateRelationshipCommand',
                                  'UpdateRelationshipEventHandler']

                    def parallelDeploySteps = allLambdas.collectEntries {
                        ["${it}": { deleteStack((String) TARGET_ACCOUNT, it) }]
                    }
                    parallel parallelDeploySteps
                }
            }
        }
    }
}

def deleteStack(String targetAccount, String templateName, String region = 'us-east-1') {
    withAwsAccount(account: targetAccount, region: region) {
        String stackName = cloudformation.generateStackName(templateName)
        echo "deleting stack ${stackName}"
        sh "aws cloudformation delete-stack --stack-name ${stackName}"
        sh "aws cloudformation wait stack-delete-complete --stack-name ${stackName}"
    }
}