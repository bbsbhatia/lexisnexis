
# Update Relationship Command Lambda for updating a already created relationship-id


Date: 2019-07-31

## Status:  Accepted

* Deciders: Planet X

## Context
Technical Story: Update Relationship Command lambda to enable updating a relationship

Users need a way to add commitments to an already created relationship-id is needed for users of 
DataLake

## Decision

Command lambda will be completed first

500 commitment operations can be done in one PATCH request

Catalogs and Collections committed to must exist and be in a valid state

PATCH api is created under DataLakeURL/administration/stage/relationships to update an 
already created relationship-id

## Consequences
Users will now be able to update an already created relationship-id resource
## Links

* [Update Relationship-id Admin Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:722681)
* [ADR for create-relationship-api](0001-create-relationship-api.md)