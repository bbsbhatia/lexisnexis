# Owner APIs to be refactored to match patters


Date: 2019-08-27

## Status:  Accepted

* Deciders: Planet X

## Context
Technical Story: Some of the Owner APIs for admin
 are not built using established patterns that should be followed.
 
 The Owner APIs that don't follow current patterns include:
 
 * Create Owner
 * Remove Owner
 * Update Owner

## Decision

* Create Owner will be modeled after Create Relationship
* Update Owner will be modeled after Update Relationship
* Remove Owner will be modeled after Remove Object

* List Owners and Get Owner will be done as part of a separate story that 
will establish a new pattern that goes directly against the Dynamo Table 
without a Lambda

* Create Owner will be changed to reference the Usage Plan
for all DataLake APIs of an environment, this means
use will be able to access and use LATEST, but determining
Owner api key access wil no longer be required

## Links

* [Rewrite Administration to match our patterns](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:852427)
