
# Event handler is needed to complete update relationship api so that a relationship-id can be updated


Date: 2019-08-06

## Status: Approved

* Deciders: Planet X<!-- optional -->

## Context
Technical Story: Event handler needs to be completed for the update relationship api to be completed and fully
functioning. 


## Decision

The update relationship event handler will be completed and coded to deal with throttling.
Subscription notifications for catalogs, collections, and relationships-ids will not be 
handled at this time.

## Consequences
Users will be able to add or remove commitments to Catalogs or Collections for a given
relationship-id
Users will not be able to subscribe to or receive notifications for commitment or 
relationship actions.


## Links

* [Update Relationship-id Admin Event Handler Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:819725)
* [ADR for create-relationship-api](0001-create-relationship-api.md)
* [ADR for update-relationship-command-lambda](0001-update-relationship-command-lambda.md)