
# Create Relationship API to help enable creation of object relationships

Date: 2019-07-19

## Status: Proposed

* Deciders: Planet X

## Context
Technical Story: Relationship IDs are needed to define the type and owner of a relationship between two objects in 
the DataLake. Relationship IDs are also needed to define the commitments (contract to provide said relationship) to 
objects residing in collections or catalogs.

* What resources can commitments be made on?
* How many commitments can be made when a relationship ID is created?
* What types of Relationships can be created?


## Decision

Commitments can only be made to Catalogs and Collections due to complexity of 
creating commitments to objects

100 Catalog commitments and 100 Collection commitments can be made in one create
relationship request due to performance reasons

Relationship Types will be Aggregate, Metadata, and Transform at this time.

## Consequences
* Users will not be able to create commitments to a specific object in the DL
* Users will need to make a patch call to have more than 100 commitments for a given relationship ID

## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:722680)