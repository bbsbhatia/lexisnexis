import logging
import os
from inspect import stack, getmodulename

from aws_xray_sdk.core import patch
from lng_aws_clients import apigateway
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.owner_table import OwnerTable

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)

libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

# Set environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {}".format(event))
    logger.info("Context: {}".format(vars(context)))
    return create_owner_event_handler(event, context.invoked_function_arn)


def create_owner_event_handler(event: dict, lambda_arn: str) -> str:
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.OWNER_CREATE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.OWNER_CREATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    message['api-key'] = get_api_key(message['api-key-id'])

    owner_dict = generate_owner_item(message)

    insert_to_owner_table(owner_dict)

    return event_handler_status.SUCCESS


def get_api_key(api_key_id: str) -> str:
    try:
        return apigateway.get_client().get_api_key(apiKey=api_key_id, includeValue=True)['value']
    except error_handler.retryable_exceptions as exception:
        raise exception
    except Exception as exception:
        raise TerminalErrorException(
            "Unhandled exception occurred getting API Key for api-key-id: {0} | Exception = {1}".format(api_key_id,
                                                                                                        exception))


def generate_owner_item(message: dict) -> dict:
    owner_item = {}
    prop = None
    try:
        for prop in OwnerTable().get_required_schema_keys():
            if prop == "event-ids":
                existing_owner = {}
                owner_item[prop] = OwnerTable().build_event_ids_list(existing_owner, message['event-id'])
            else:
                owner_item[prop] = message[prop]
    except KeyError:
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in OwnerTable().get_optional_schema_keys():
        if prop in message:
            owner_item[prop] = message[prop]

    return owner_item


def insert_to_owner_table(owner: dict) -> None:
    try:
        OwnerTable().put_item(owner)
        logger.info("Insertion to Owner table succeeded.")
    except (ConditionError, SchemaError, SchemaLoadError, SchemaValidationError) as exception:
        raise TerminalErrorException("Failed to insert item into Owner table. Item = {0} | Exception = {1}"
                                     .format(owner, exception))
    except error_handler.retryable_exceptions as exception:
        raise exception
    except Exception as exception:
        raise TerminalErrorException("Failed to insert item into Owner table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(owner, exception))


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("** Local Test Run **")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-DataLakeDynamoTables-OwnerTable-5KEYPM5GQY7N'
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/" \
                                    "create-owner-event-handler-RetryQueue-1UXMVBY5ID6JO"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"

    msg = {
        "owner-id": 82,  # to test ensure this id does not exist in dynamo
        "owner-name": "Wormhole UK",
        "email-distribution": [
            "wormholeuk@lexisnexis.com",
            "wormholeuk@legal.regn.net"
        ],
        "request-time": "2017-11-13T10:16:08.829Z",
        "event-name": event_names.OWNER_CREATE,
        "event-id": "xyz",
        "event-version": event_handler_version,
        "stage": "LATEST",
        "api-key-id": "<SET ME>"  # use valid api-key-id so lambda can look up api-key
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - CREATE OWNER EVENT HANDLER LAMBDA]")
