import logging
import os
from inspect import stack, getmodulename

import lng_datalake_commons.error_handling.error_handler as error_handler
from aws_xray_sdk.core import patch
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.exceptions import ConditionError, SchemaError, SchemaLoadError, SchemaValidationError
from lng_datalake_dal.owner_table import OwnerTable

from service_commons import owner_event_handler

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

# Set environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@error_handler.handle
@tracker.track_event_handler(event_names.OWNER_UPDATE)
def lambda_handler(event, context):
    logger.info("Event: {}".format(event))
    logger.info("Context: {}".format(vars(context)))
    return update_owner_event_handler(event, context.invoked_function_arn)


def update_owner_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.OWNER_UPDATE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.OWNER_UPDATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    existing_owner = owner_event_handler.get_owner_from_owner_table(message['owner-id'])
    updated_item = owner_event_handler.initialize_dict_with_keys(message, existing_owner)
    if updated_item:
        update_owner_table(updated_item)
    else:
        raise TerminalErrorException('Failed to update message:{0}'.format(message))

    return event_handler_status.SUCCESS


def update_owner_table(owner_dict: dict):
    try:
        OwnerTable().update_item(owner_dict)
        logger.info("Update for OwnerId: {} was successful".format(owner_dict["owner-id"]))
    except (ConditionError,
            SchemaError,
            SchemaLoadError,
            SchemaValidationError) as exception:
        raise TerminalErrorException("Failed to update item in Owner table. Item = {0} | Exception = {1}"
                                     .format(owner_dict, exception))

    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as exception:
        raise TerminalErrorException("Failed to update item in Owner table due to unknown reasons. Item = {0} | "
                                     "Exception = {1}".format(owner_dict, exception))


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-DataLake-" \
                                    "UpdateOwnerEventHandlerLambda-RetryQueue-XLRVQRO0CZDH"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "feature-datalake-lambda-terminal-errors-288044017584"
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-DataLakeDynamoTables-OwnerTable-5KEYPM5GQY7N'

    msg = {
        "owner-id": 5,
        "owner-name": "Jess",
        "email-distribution": [
            "jess@lexisnexis.com"
        ],
        "request-time": "2017-11-13T10:16:08.829Z",
        "event-id": "xyz",
        "event-name": event_names.OWNER_UPDATE,
        "event-version": event_handler_version,
        "stage": "LATEST"
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext())
    logger.debug("[LOCAL RUN END - UPDATE OWNER EVENT HANDLER LAMBDA]")
