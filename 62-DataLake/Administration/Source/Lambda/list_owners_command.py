import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, paginator_token, command_validator
from lng_datalake_commands.exceptions import InternalError, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_dal.owner_table import OwnerTable

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

DEFAULT_MAX_ITEMS = 1000


@session_decorator.lng_aws_session()
@command_wrapper.command("Schemas/ListOwnersCommand-RequestSchema.json",
                         "Schemas/ListOwnersCommand-ResponseSchema.json")
def lambda_handler(event, context):
    return list_owners_command(event["request"])


def list_owners_command(request):
    optional_properties = get_optional_properties_from_request(request)

    try:
        list_of_owners = OwnerTable().get_all_items(optional_properties["max-items"],
                                                    optional_properties["pagination-token"])
    except ClientError as error:
        raise InternalError("Unable to get items from Owner Table", error)
    except Exception as error:
        raise UnhandledException(error)

    pagination_token = OwnerTable().get_pagination_token()

    return {
        "response-dict": generate_json_response(list_of_owners, pagination_token, optional_properties["max-items"])}


def get_optional_properties_from_request(request):
    max_items = request.get("max-items", DEFAULT_MAX_ITEMS)
    command_validator.validate_max_results(max_items, DEFAULT_MAX_ITEMS)
    optional_properties = {}
    optional_properties["max-items"] = max_items
    optional_properties["pagination-token"] = None
    next_token = request.get("next-token")
    # If we have a next-token validate the max-items matches the original request
    # Dyanmo doesn't handle pagination if the max-items changes
    if next_token:
        tokens = paginator_token.unpack_validate_pagination_token(next_token, max_items)
        optional_properties["pagination-token"] = tokens['pagination-token']
    return optional_properties


def generate_json_response(list_of_owners, pagination_token, max_items):
    owners_response = []
    req_keys = command_wrapper.get_required_response_schema_keys("owners-pagination", "properties", "owners", "items")
    opt_keys = command_wrapper.get_optional_response_schema_keys("owners-pagination", "properties", "owners", "items")
    key = None
    for owner_data in list_of_owners:
        response_item = {}
        try:
            for key in req_keys:
                response_item[key] = owner_data[key]
        except Exception as e:
            logger.error(e)
            raise InternalError("Missing required property: {0}".format(key))

        for key in opt_keys:
            if key in owner_data:
                response_item[key] = owner_data[key]
            # TODO We accept only one prefix per owner for now,
            #  but we have the type as list to support more than one prefix in the future
            if key == 'collection-prefixes' and owner_data.get('collection-prefix'):
                response_item[key] = [owner_data.get('collection-prefix')]

        owners_response.append(response_item)

    response = {"owners": owners_response}

    if pagination_token:
        response["next-token"] = paginator_token.generate_pagination_token(max_items, pagination_token)

    response['item-count'] = len(list_of_owners)

    return response


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    from datetime import datetime

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["OWNER_DYNAMODB_TABLE"] = "feature-man-DataLake-OwnerTable"

    apig_json = \
        {
            "context": {
                "client-request-id": '"1234-request-id-wxyz"',
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "max-items": 4,
                "next-token": "4|eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiT3duZXJJRCI6IHsiTiI6ICIyNTEifX19"
            }
        }

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(apig_json, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - LIST OWNERS COMMAND LAMBDA]")
