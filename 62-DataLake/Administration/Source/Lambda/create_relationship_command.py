import logging
import os
import re

from aws_xray_sdk.core import patch, xray_recorder
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, UnhandledException, InvalidRequestPropertyValue, \
    RelationshipAlreadyExists
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names
from lng_datalake_dal.relationship_owner_table import RelationshipOwnerTable

from service_commons import relationship_command

__author__ = "Doug Heitkamp, Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

event_version = os.getenv("EVENT_VERSION", 1)

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command("Schemas/CreateRelationshipCommand-RequestSchema.json",
                         "Schemas/CreateRelationshipCommand-ResponseSchema.json", event_names.RELATIONSHIP_CREATE)
def lambda_handler(event, context):
    return create_relationship_command(event['request'],
                                       event['context']['client-request-id'],
                                       event['context']['stage'],
                                       event['context']['api-key-id'])


def create_relationship_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required parameters
    relationship_id = request['relationship-id']

    validate_relationship_id(relationship_id)

    validate_commitments(request.get('collection-commitments'), request.get('catalog-commitments'))

    owner_id = owner_authorization.get_owner_by_api_key_id(api_key_id)['owner-id']

    request['owner-id'] = owner_id

    response_json = generate_json_response(request)

    event_dict = generate_event_dict(response_json, request_id, stage)

    return {'response-dict': response_json, 'event-dict': event_dict}


def validate_relationship_id(relationship_id: str) -> None:
    if not re.match("^[A-Za-z0-9-.:]*$", relationship_id):
        raise InvalidRequestPropertyValue("Invalid Relationship ID {0}".format(relationship_id),
                                          "Relationship ID contains invalid characters")
    elif relationship_id.isdigit():
        raise InvalidRequestPropertyValue("Invalid Relationship ID {0}".format(relationship_id),
                                          "Relationship ID cannot contain only digits")
    elif len(relationship_id) > 40 or len(relationship_id) < 1:
        raise InvalidRequestPropertyValue("Invalid Relationship ID {0}".format(relationship_id),
                                          "Relationship ID length should not exceed 40 characters")
    elif get_relationship_from_relationship_owner_table(relationship_id):
        raise RelationshipAlreadyExists("Invalid Relationship ID {0}".format(relationship_id),
                                        "Relationship ID already exists")


def get_relationship_from_relationship_owner_table(relationship_id: str) -> dict:
    try:
        return RelationshipOwnerTable().get_item({"relationship-id": relationship_id})
    except ClientError as error:
        raise InternalError("Unable to get item from Relationship Owner Table", error)
    except Exception as error:
        raise UnhandledException(error)


def validate_commitments(collection_commitments: list, catalog_commitments: list) -> None:
    if collection_commitments:
        relationship_command.validate_collection_commitments(collection_commitments)
    if catalog_commitments:
        relationship_command.validate_catalog_commitments(catalog_commitments)


def generate_json_response(new_relationship_values: dict) -> dict:
    req_keys = command_wrapper.get_required_response_schema_keys('relationship')
    optional_keys = command_wrapper.get_optional_response_schema_keys('relationship')

    return_dict = {}

    for prop in req_keys:
        if prop in new_relationship_values:
            return_dict[prop] = new_relationship_values[prop]
        else:
            raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_keys:
        if prop in new_relationship_values:
            return_dict[prop] = new_relationship_values[prop]
    return return_dict


def generate_event_dict(json_response: dict, request_id: str, stage: str) -> dict:
    event_entry = {'relationship-id': json_response['relationship-id'],
                   'relationship-type': json_response['relationship-type'],
                   'owner-id': json_response['owner-id'],
                   'request-time': command_wrapper.get_request_time(),
                   'event-name': event_names.RELATIONSHIP_CREATE,
                   'event-id': request_id,
                   'event-version': event_version,
                   'stage': stage}

    if "description" in json_response:
        event_entry['description'] = json_response['description']
    if "collection-commitments" in json_response:
        event_entry['collection-commitments'] = json_response['collection-commitments']
    if "catalog-commitments" in json_response:
        event_entry['catalog-commitments'] = json_response['catalog-commitments']

    return event_entry


if __name__ == '__main__':
    import json
    import random
    import string
    from datetime import datetime
    from lng_datalake_testhelper import mock_lambda_context

    os.environ["AWS_PROFILE"] = 'c-sand'
    os.environ["RELATIONSHIP_OWNER_DYNAMODB_TABLE"] = "feature-djh-DataLake-RelationshipOwnerTable"
    os.environ["OWNER_DYNAMODB_TABLE"] = "feature-djh-DataLake-OwnerTable"
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-djh-DataLake-EventStoreTable'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-djh-DataLake-CatalogTable'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-djh-DataLake-CollectionTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - DESCRIBE CATALOGS COMMAND LAMBDA]")

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "http-method": "POST",
                "stage": "LATEST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "relationship-id": "newjudges",
                "relationship-type": "Metadata",
                "collection-commitments": ["2"]
            }

        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)
    xray_recorder.context._context_missing = "LOG_ERROR"

    lambda_response = lambda_handler(lambda_event, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN E0128ND - DESCRIBE CATALOGS COMMAND LAMBDA]")
