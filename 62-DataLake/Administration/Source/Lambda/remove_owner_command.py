import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, ForbiddenStateTransitionError, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names, relationship_status
from lng_datalake_dal.relationship_owner_table import RelationshipOwnerTable

from service_commons import owner_command

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/RemoveOwnerCommand-RequestSchema.json',
                         'Schemas/RemoveOwnerCommand-ResponseSchema.json', event_names.OWNER_REMOVE)
def lambda_handler(event, context):
    return remove_owner_command(event["request"],
                                event['context']['client-request-id'],
                                event['context']['stage'],
                                event['context']['api-key-id'])


def remove_owner_command(request, request_id, stage, api_key_id):
    # owner authorization
    owner_id = request['owner-id']
    owner_response = owner_authorization.authorize(owner_id, api_key_id)

    validate_relationships_removed(owner_id)

    collection_status = owner_command.get_owner_collection_status(owner_id)

    if collection_status == owner_command.COLLECTION_ACTIVE:
        raise ForbiddenStateTransitionError("Owner has at least one non terminated collection",
                                            "Remove all collections for this owner")

    logger.info("Removing owner {0} with collections {1}".format(owner_id, collection_status))

    # Only update owner if collection status is NONE or TERMINATED
    if collection_status == owner_command.COLLECTION_NONE or collection_status == owner_command.COLLECTION_TERMINATED:
        event_dict = owner_command.create_owner_event_store_item(owner_response, event_names.OWNER_REMOVE, request_id,
                                                                 event_version, stage)
    else:
        raise InternalError("Unknown collection status of {0} for owner {1}".format(collection_status, owner_id))

    return {'response-dict': owner_command.generate_json_response({}, owner_response), 'event-dict': event_dict}


def validate_relationships_removed(owner_id: int) -> None:
    try:
        relationship_response = RelationshipOwnerTable().query_items(index="relationship-owner-by-owner-id-index",
                                                                     KeyConditionExpression='OwnerID = :owner_id',
                                                                     ExpressionAttributeValues={
                                                                         ":owner_id": {"N": str(owner_id)}})
    except ClientError as error:
        raise InternalError("Unable to get item from Relationship Owner Table", error)
    except Exception as error:
        raise UnhandledException(error)
    if relationship_response:
        for relationship in relationship_response:
            if relationship['relationship-state'] != relationship_status.REMOVED:
                raise ForbiddenStateTransitionError(
                    "RelationshipID {0} must be in a {1} state to be removed".format(relationship['relationship-id'],
                                                                                     relationship_status.REMOVED),
                    "Remove all relationships before removing owner")


if __name__ == '__main__':
    import json
    import random
    import string
    from datetime import datetime
    from lng_datalake_testhelper import mock_lambda_context

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE OWNER COMMAND LAMBDA]")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["OWNER_DYNAMODB_TABLE"] = "feature-djh-DataLake-OwnerTable"
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-djh-DataLake-CollectionTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-djh-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-djh-DataLake-TrackingTable'
    os.environ['RELATIONSHIP_OWNER_DYNAMODB_TABLE'] = 'feature-djh-DataLake-RelationshipOwnerTable'
    os.environ['COLLECTION_DYNAMODB_OWNER_ID_AND_NAME_INDEX'] = 'collection-by-owner-and-name-index'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    apig_json = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "DELETE",
                "api-key-id": "<SET ME>"
            },
            "request": {
                'owner-name': 'updated_owner_name_2019-07-19T12:34:13.895516',
                'email-distribution': ['updated_owner_138@lexisnexis.com'], 'owner-id': 255,
                'collection-prefixes': ['5D31B854']
            }
        }

    rand_str = lambda n: "".join([random.choice(string.ascii_letters) for _ in range(n)])

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)
    owner_command.collection_table_index = os.environ['COLLECTION_DYNAMODB_OWNER_ID_AND_NAME_INDEX']

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    json_response = lambda_handler(apig_json, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - REMOVE OWNER COMMAND LAMBDA]")
