import logging
import os
from inspect import stack, getmodulename

import lng_datalake_commons.error_handling.error_handler as error_handler
import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.exceptions import ConditionError, SchemaError, SchemaLoadError, SchemaValidationError
from lng_datalake_dal.relationship_commitment_table import RelationshipCommitmentTable
from lng_datalake_dal.relationship_owner_table import RelationshipOwnerTable

from service_commons import relationship_event_handler

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

# Set environment variables
timeout_delay = int(os.getenv('MAX_REMAINING_MILLIS', '30000'))
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

get_remaining_time_in_millis = None


@session_decorator.lng_aws_session()
@error_handler.handle
@tracker.track_event_handler(event_names.RELATIONSHIP_UPDATE)
def lambda_handler(event, context):
    logger.info("Event: {}".format(orjson.dumps(event).decode()))
    logger.info("Context: {}".format(vars(context)))
    return update_relationship_event_handler(event, context.invoked_function_arn, context.get_remaining_time_in_millis)


def update_relationship_event_handler(event: dict, lambda_arn: str, _get_remaining_time_in_millis: callable):
    """
    Encapsulates all the Lambda handler logic

    :param _get_remaining_time_in_millis: remaining execution time for lambda in milliseconds
    :param event: sns event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.RELATIONSHIP_UPDATE):
        raise TerminalErrorException(
            "Failed to extract event: {0} or event-name doesn't "
            "match {1}".format(event, event_names.RELATIONSHIP_UPDATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    global get_remaining_time_in_millis
    get_remaining_time_in_millis = _get_remaining_time_in_millis

    additional_attributes = message.get('additional-attributes', {})

    if additional_attributes.get('relationship-id-not-updated', True):
        update_to_relationship_owner_table(message)

    collection_ids_to_add = message.get("collection-id-actions", {}).get('add', [])
    collection_ids_to_del = message.get("collection-id-actions", {}).get('remove', [])

    catalog_ids_to_add = message.get("catalog-id-actions", {}).get('add', [])
    catalog_ids_to_del = message.get("catalog-id-actions", {}).get('remove', [])

    if collection_ids_to_add or collection_ids_to_del or catalog_ids_to_add or catalog_ids_to_del:
        # get the index of the first unprocessed collection id in the add and remove list. If this is a retry event,
        # the index indicates the collection id from which to continue the mapping update.
        index_add = message.get('additional-attributes', {}).get('commitment-index', {}).get('add', 0)
        index_del = message.get('additional-attributes', {}).get('commitment-index', {}).get('remove', 0)

        new_index_add, new_index_del = update_relationship_commitments(message['relationship-id'],
                                                                       collection_ids_to_add, collection_ids_to_del,
                                                                       catalog_ids_to_add, catalog_ids_to_del,
                                                                       index_add, index_del)

        if new_index_add < len(collection_ids_to_add + catalog_ids_to_add) or \
                new_index_del < len(collection_ids_to_del + catalog_ids_to_del):
            error_handler.set_message_additional_attribute('commitment-index',
                                                           {'add': new_index_add, 'remove': new_index_del})
            raise RetryEventException("Retry event to finish the update with the unprocessed collection ids")

    return event_handler_status.SUCCESS


def get_existing_relationship(message: dict) -> dict:
    """
    Map the incoming event store message relationship information to a
    relationship in the relationship owner table
    :param message: event store message relationship information
    :return: the existing relationship from the RelationshipOwnerTable
    """
    relationship_id = message["relationship-id"]
    try:
        existing_relationship = RelationshipOwnerTable().get_item({'relationship-id': message["relationship-id"]})
        if not existing_relationship:
            raise TerminalErrorException('Can not update a non existent relationship {0} '.format(message))
    except error_handler.retryable_exceptions as e:
        raise e
    except TerminalErrorException as e:
        raise e
    except Exception as eError:
        raise TerminalErrorException('Can not update a non existent {} due to {}'.format(relationship_id, eError))

    return existing_relationship


def update_to_relationship_owner_table(message: dict) -> None:
    """
    Update a relationship_item in the Relationship Owner Table
    relationship-id-not-updated will be set to false since it is now updated
    if throttle occurs later on, this function won't be called.
    :param message: relationship item to be updated
    """
    if 'description' in message:
        relationship = get_existing_relationship(message)
        relationship_dict = relationship_event_handler.generate_relationship_item(message, relationship)

        try:
            RelationshipOwnerTable().update_item(relationship_dict)
            logger.info("Update for RelationshipId: {} was successful".format(relationship_dict["relationship-id"]))
        except (ConditionError, SchemaError, SchemaLoadError, SchemaValidationError) as e:
            raise TerminalErrorException(
                "Failed to update item in RelationshipOwner table. Item = {0} | Exception = {1}".format(
                    relationship_dict,
                    e))
        except error_handler.retryable_exceptions as e:
            raise e
        except Exception as e:
            raise TerminalErrorException("Failed to update item in RelationshipOwner table due to unknown reasons. "
                                         "Item = {0} | Exception = {1}".format(relationship_dict, e))

    error_handler.set_message_additional_attribute("relationship-id-not-updated", False)


def update_relationship_commitments(relationship_id: str, collection_ids_to_add: list, collection_ids_to_del: list,
                                    catalog_ids_to_add: list, catalog_ids_to_del: list,
                                    index_add: int, index_del: int) -> tuple:
    """
    Batch writes to the RelationshipCommitment table
    Will return the indexes it last tried to attempt, due to the nature of batch_write
    The indexes will deal with throttle errors
    Batch write for
    :param relationship_id: the relationship-id to make commitments for
    :param collection_ids_to_add: collections to make new commitments to
    :param collection_ids_to_del: collections to remove commitments from
    :param catalog_ids_to_add: catalogs to make new commitments to
    :param catalog_ids_to_del: collections to remove new commitments from
    :param index_add: start index for adding
    :param index_del: start index for removing
    :return: index_add, index_del
    """
    global get_remaining_time_in_millis

    # DynamoDB BatchWriteItem has limit of 25 items per batch
    max_batch_size = 25
    while index_add < len(collection_ids_to_add + catalog_ids_to_add) or \
            index_del < len(collection_ids_to_del + catalog_ids_to_del) and \
            get_remaining_time_in_millis() > timeout_delay:
        add_batch_size = max_batch_size
        joined_add = [{'relationship-id': relationship_id,
                       'target-composite-key': RelationshipCommitmentTable().generate_target_composite_key(
                           'collection', coll),
                       'target-type': 'Collection'} for coll in collection_ids_to_add] + \
                     [{'relationship-id': relationship_id,
                       'target-composite-key': RelationshipCommitmentTable().generate_target_composite_key(
                           'catalog', cat),
                       'target-type': 'Catalog'} for cat in catalog_ids_to_add]
        items_to_add = joined_add[index_add:index_add + add_batch_size]
        add_batch_size = len(items_to_add)

        del_batch_size = max_batch_size - add_batch_size
        joined_del = [{'relationship-id': relationship_id,
                       'target-composite-key': RelationshipCommitmentTable().generate_target_composite_key(
                           'collection', coll)} for coll in collection_ids_to_del] + \
                     [{'relationship-id': relationship_id,
                       'target-composite-key': RelationshipCommitmentTable().generate_target_composite_key(
                           'catalog', cat)} for cat in catalog_ids_to_del]
        items_to_del = joined_del[index_del:index_del + del_batch_size]
        del_batch_size = len(items_to_del)

        try:
            unprocessed_items = RelationshipCommitmentTable().batch_write_items(items_to_add, items_to_del)
            if unprocessed_items:
                break
            index_add += add_batch_size
            index_del += del_batch_size
        except error_handler.retryable_exceptions as e:
            logger.error(e)
            break
        except Exception as e:
            logger.error(e)
            raise TerminalErrorException("General Exception Occurred: {}".format(e))

    return index_add, index_del


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("** Local Test Run **")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RELATIONSHIP_OWNER_DYNAMODB_TABLE"] = "feature-djh-DataLake-RelationshipOwnerTable"
    os.environ["RELATIONSHIP_COMMITMENT_DYNAMODB_TABLE"] = "feature-djh-DataLake-RelationshipCommitmentTable"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "feature-djh-datalake-lambda-terminal-errors"

    msg = {
        "relationship-id": "judges",
        "relationship-type": "Metadata",
        "owner-id": 2,
        "request-time": "2019-08-06T19:52:58.552Z",
        "event-name": "Relationship::Update",
        "event-id": "\"1234-request-id-wxyz\"",
        "event-version": 1,
        "stage": "LATEST",
        "description": "hello",
        "collection-id-actions": {
            "add": [
                "62",
                "2"
            ],
            "remove": [
                "4",
                "10"
            ]
        },
        "catalog-id-actions": {
            "add": [
                "22",
                "23"
            ],
            "remove": [
                "4",
                "10"
            ]
        }
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - UPDATE CATALOG EVENT HANDLER LAMBDA]")
