import logging
import os

from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import collection_status
from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.owner_table import OwnerTable

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 1000))

# Status of all owner's collections
COLLECTION_NONE = 'NONE'
COLLECTION_TERMINATED = 'TERMINATED'
COLLECTION_ACTIVE = 'ACTIVE'


def get_owner_from_owner_table(owner_id):
    try:
        owner_response = OwnerTable().get_item({"owner-id": owner_id})
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException('Unable to get Owner ID {} from Owner Table. Error = {}'.format(owner_id, error))
    if not owner_response:
        raise TerminalErrorException('Owner ID {} does not exist in Owner Table.'.format(owner_id))
    return owner_response


def get_owner_collection_status(owner_id):
    """Get the status of all the owner's collections"""
    try:
        collection_table = CollectionTable()
        total_collections = 0
        done = False
        next_token = None
        while not done:
            collection_items = collection_table.query_items(index='collection-by-owner-index',
                                                            max_items=DEFAULT_MAX_ITEMS,
                                                            pagination_token=next_token,
                                                            KeyConditionExpression='OwnerID = :owner_id ',
                                                            ExpressionAttributeValues={
                                                                ":owner_id": {"N": str(owner_id)}})

            total_collections += len(collection_items)

            for collection_item in collection_items:
                if collection_item['collection-state'].split("::")[-1] != collection_status.TERMINATED:
                    return COLLECTION_ACTIVE

            next_token = collection_table.get_pagination_token()
            if not next_token:
                done = True

    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException(
            'Unable to get status of Collection for Owner ID {}. Error = {}'.format(owner_id, error))

    if total_collections == 0:
        return COLLECTION_NONE

    # Status ACTIVE is returned in loop above as soon as it is found so TERMINATED is only other possibility
    return COLLECTION_TERMINATED


def initialize_dict_with_keys(message, existing_owner=None):
    if not existing_owner:
        existing_owner = {}
    return_dict = {}
    key = None
    try:
        for key in OwnerTable().get_required_schema_keys():
            if key == "event-ids":
                return_dict[key] = OwnerTable().build_event_ids_list(existing_owner, message['event-id'])
            else:
                return_dict[key] = message.get(key, existing_owner.get(key, False))
            if not return_dict[key]:
                raise KeyError

        # Because these keys are Optional it is still necessary to verify they existed before
        for key in OwnerTable().get_optional_schema_keys():
            if key in message:
                return_dict[key] = message[key]
            elif key in existing_owner:
                return_dict[key] = existing_owner[key]
    except KeyError as e:
        logger.error(e)
        raise TerminalErrorException("Key = {0} does not exist in owner {1}"
                                     .format(key, message['owner-id']))
    return return_dict
