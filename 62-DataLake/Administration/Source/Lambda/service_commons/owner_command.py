# TODO
#  Code for administration API need to be refactored to follow the same standards and flow we have with the other APIs
import logging
import os

import re
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, NoSuchOwner, \
    ConflictingRequestError, CollectionPrefixAlreadyExists, UnhandledException
from lng_datalake_commons import email_helper
from lng_datalake_constants import collection_status
from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.owner_table import OwnerTable

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 1000))

# Status of all owner's collections
COLLECTION_NONE = 'NONE'
COLLECTION_TERMINATED = 'TERMINATED'
COLLECTION_ACTIVE = 'ACTIVE'


def validate_owner(owner, validate_prefix=False) -> None:
    validate_owner_name(owner["owner-name"])
    validate_email_list(owner["email-distribution"])

    collection_prefixes = owner.get("collection-prefixes", [])

    # TODO We accept only one prefix per owner for now,
    #  but we have the type as list to support more than one prefix in the future
    if len(collection_prefixes) > 1:
        raise InvalidRequestPropertyValue("Invalid number of collection Prefixes {}".format(len(collection_prefixes)),
                                          "Number of Collection Prefixes should not exceed 1")

    collection_prefix = None
    if len(collection_prefixes) == 1:
        collection_prefix = collection_prefixes[0]

    if collection_prefix and validate_prefix:
        validate_collection_prefix(collection_prefix)
        validate_unique_collection_prefix(collection_prefix)

    validate_unique_owner(owner)


def validate_owner_name(owner_name: str) -> None:
    if not owner_name:
        raise InvalidRequestPropertyValue("Invalid Owner Name", "Owner Name must not be an empty string")


def validate_email_list(email_list: list) -> None:
    invalid_list = []
    for email in email_list:
        if not email_helper.is_valid_email_format(email):
            invalid_list.append(email)

    if invalid_list:
        raise InvalidRequestPropertyValue(
            "The following emails are invalid {0}".format(invalid_list))


def validate_collection_prefix(collection_prefix: str) -> None:
    # capital alphanumeric, not all digits
    match_results = re.match(r"^\d*[A-Z][A-Z0-9]*$", collection_prefix)

    if not match_results:
        raise InvalidRequestPropertyValue(
            "Collection prefix is invalid {0}".format(collection_prefix),
            "Collection prefix can only contain capital letters and digits, "
            "Collection prefix cannot contain only digits")

    if len(collection_prefix) > 10:
        raise InvalidRequestPropertyValue("Invalid Collection prefix {0}".format(collection_prefix),
                                          "Collection prefix length should not exceed 10 characters")


# check if the collection prefix is already exists
def validate_unique_collection_prefix(collection_prefix: str) -> None:
    try:
        response = OwnerTable().query_items(index="owner-by-collection-prefix-index",
                                            KeyConditionExpression='CollectionPrefix=:collection_prefix',
                                            ExpressionAttributeValues={
                                                ":collection_prefix": {"S": collection_prefix}})

    except ClientError as e:
        raise InternalError("Unable to query items from Owner Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if response:
        raise CollectionPrefixAlreadyExists("Collection prefix already exists",
                                            "Owner ID {0} owns this prefix".format(response[0]["owner-id"]))


# check if the owner name and email distribution list already exists
def validate_unique_owner(input_owner: dict) -> None:
    try:
        response = OwnerTable().query_items(index="owner-by-name-index",
                                            KeyConditionExpression='OwnerName=:owner_name',
                                            ExpressionAttributeValues={
                                                ":owner_name": {"S": input_owner["owner-name"]}})
    except ClientError as e:
        raise InternalError("Unable to query items from Owner Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    # below code checks for same email list for a given owner name
    input_email_list = input_owner["email-distribution"]

    for owner in response:
        dynamo_email_list = owner["email-distribution"]
        if len(set(input_email_list).intersection(dynamo_email_list)) == len(dynamo_email_list):
            raise ConflictingRequestError(
                "Owner already exists with owner-id {0} for given owner-name and email-distribution".format(
                    owner["owner-id"]))


def get_owner_from_owner_table(owner_id: int) -> dict:
    try:
        owner_response = OwnerTable().get_item({"owner-id": owner_id})
    except ClientError as error:
        raise InternalError("Unable to get item from Owner Table", error)
    except Exception as error:
        raise UnhandledException(error)

    if not owner_response:
        raise NoSuchOwner("Invalid Owner ID {}".format(owner_id), "Owner ID does not exist")

    return owner_response


def get_owner_collection_status(owner_id: int) -> str:
    """Get the status of all the owner's collections"""
    try:
        total_collections = 0
        done = False
        next_token = None
        while not done:
            collection_items = CollectionTable().query_items(index='collection-by-owner-index',
                                                             max_items=DEFAULT_MAX_ITEMS,
                                                             pagination_token=next_token,
                                                             KeyConditionExpression='OwnerID = :owner_id ',
                                                             ExpressionAttributeValues={
                                                                 ":owner_id": {"N": str(owner_id)}})

            total_collections += len(collection_items)

            for collection_item in collection_items:
                if collection_item['collection-state'] != collection_status.TERMINATED:
                    return COLLECTION_ACTIVE

            next_token = CollectionTable().get_pagination_token()
            if not next_token:
                done = True

    except ClientError as error:
        raise InternalError("Unable to query Collection Table", error)
    except Exception as error:
        raise UnhandledException(error)

    if total_collections == 0:
        return COLLECTION_NONE

    # Status ACTIVE is returned in loop above as soon as it is found so TERMINATED is only other possibility
    return COLLECTION_TERMINATED


def create_owner_event_store_item(owner_request: dict, event_name: str, request_id: str, event_version: int,
                                  stage: str) -> dict:
    event_item = {
        "owner-id": owner_request["owner-id"],
        "owner-name": owner_request["owner-name"],
        "email-distribution": owner_request["email-distribution"],
        "event-id": request_id,
        "request-time": command_wrapper.get_request_time(),
        "event-name": event_name,
        "event-version": event_version,
        "stage": stage
    }

    # Add api-key, api-key-id, collection-prefix if they exist in owner request
    if "api-key" in owner_request:
        event_item["api-key"] = owner_request["api-key"]
    if "api-key-id" in owner_request:
        event_item['api-key-id'] = owner_request['api-key-id']
    if "collection-prefix" in owner_request:
        event_item["collection-prefix"] = owner_request["collection-prefix"]

    return event_item


def generate_json_response(new_owner_values: dict, existing_owner: dict = None) -> dict:
    if not existing_owner:
        existing_owner = {}
    req_keys = command_wrapper.get_required_response_schema_keys('owner-properties')
    optional_keys = command_wrapper.get_optional_response_schema_keys('owner-properties')
    return_dict = {}
    prop = None

    try:
        for prop in req_keys:
            if prop in new_owner_values:
                return_dict[prop] = new_owner_values[prop]
            else:
                return_dict[prop] = existing_owner[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_keys:
        # TODO We accept only one prefix per owner for now,
        #  but we have the type as list to support more than one prefix in the future
        if prop == 'collection-prefixes':
            optional_prop = new_owner_values.get(prop, [existing_owner.get('collection-prefix')])
        else:
            optional_prop = new_owner_values.get(prop, existing_owner.get(prop))

        if optional_prop and optional_prop != [None]:
            return_dict[prop] = optional_prop
    return return_dict
