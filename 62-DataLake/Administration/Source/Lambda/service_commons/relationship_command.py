import logging
import os

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, UnhandledException, NoSuchCollection, SemanticError, \
    NoSuchCatalog
from lng_datalake_constants import collection_status
from lng_datalake_dal.catalog_table import CatalogTable
from lng_datalake_dal.collection_table import CollectionTable

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))


def validate_catalog_commitments(catalog_commitments: list) -> None:
    try:
        catalog_response = CatalogTable().batch_get_items('catalog-id', list(set(catalog_commitments)))
    except ClientError as e:
        logger.error(e)
        raise InternalError("Unable to batch get item from Catalog Table", e)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)

    if not catalog_response:
        raise NoSuchCatalog("Invalid Catalog ID(s) {0}".format(catalog_commitments),
                            "Catalog ID(s) does not exist")

    missing_catalogs = set(catalog_commitments) - set(cat['catalog-id'] for cat in catalog_response)
    if missing_catalogs:
        raise NoSuchCatalog("Invalid Catalog ID(s) {0}".format(missing_catalogs),
                            "Catalog ID(s) does not exist")


def validate_collection_commitments(collection_commitments: list) -> None:
    try:
        collection_response = CollectionTable().batch_get_items('collection-id', list(set(collection_commitments)))
    except ClientError as e:
        raise InternalError("Unable to batch get item from Collection Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not collection_response:
        raise NoSuchCollection("Invalid Collection ID(s) {0}".format(collection_commitments),
                               "Collection ID(s) does not exist")

    missing_collections = set(collection_commitments) - set(coll['collection-id'] for coll in collection_response)
    if missing_collections:
        raise NoSuchCollection("Invalid Collection ID(s) {0}".format(missing_collections),
                               "Collection ID(s) does not exist")

    bad_state = [coll for coll in collection_response if coll['collection-state'] in [
        collection_status.TERMINATED or collection_status.TERMINATING or collection_status.TERMINATING_FAILED]]
    if bad_state:
        raise SemanticError("Invalid Collection ID(s) {0}".format(bad_state),
                            "Collection ID(s) not in valid state")
