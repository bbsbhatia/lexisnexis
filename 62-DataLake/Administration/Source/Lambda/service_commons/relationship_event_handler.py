import logging
import os
from inspect import stack, getmodulename

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, event_handler_status, relationship_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.relationship_commitment_table import RelationshipCommitmentTable
from lng_datalake_dal.relationship_owner_table import RelationshipOwnerTable

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


def generate_relationship_item(message: dict, existing_relationship: dict = None) -> dict:
    if not existing_relationship:
        existing_relationship = {}
    relationship_item = {}
    prop = None
    try:

        for prop in RelationshipOwnerTable().get_required_schema_keys():
            if prop == 'relationship-state':
                relationship_item[prop] = relationship_status.CREATED
            elif prop in message:
                relationship_item[prop] = message[prop]
            else:
                relationship_item[prop] = existing_relationship[prop]
    except Exception as ex:
        raise TerminalErrorException("Missing required property: {0}".format(prop))

    for prop in RelationshipOwnerTable().get_optional_schema_keys():
        if prop in message:
            relationship_item[prop] = message[prop]
        elif prop in existing_relationship:
            relationship_item[prop] = existing_relationship[prop]

    return relationship_item
