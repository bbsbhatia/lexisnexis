import logging
import os
from inspect import stack, getmodulename

from aws_xray_sdk.core import patch
from lng_aws_clients import apigateway
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.owner_table import OwnerTable

from service_commons import owner_event_handler

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

# Set environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@error_handler.handle
@tracker.track_event_handler(event_names.OWNER_REMOVE)
def lambda_handler(event, context):
    logger.info("Event: {}".format(event))
    logger.info("Context: {}".format(vars(context)))
    return remove_owner_event_handler(event, context.invoked_function_arn)


def remove_owner_event_handler(event, lambda_arn):
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.OWNER_REMOVE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.OWNER_REMOVE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    try:
        owner_id = message["owner-id"]
    except KeyError as exception:
        raise TerminalErrorException("Key = {0} does not exist. Message = {1}".format(exception, message))

    collection_status = owner_event_handler.get_owner_collection_status(owner_id)
    if collection_status == owner_event_handler.COLLECTION_ACTIVE:
        raise TerminalErrorException(
            "Failed to process event: {} cannot remove owner with collection status {}".format(event,
                                                                                               collection_status))

    # Remove owner API Key from gateway if collection status is NONE or TERMINATED
    remove_owner_api_key(owner_id, message['event-id'])

    if collection_status == owner_event_handler.COLLECTION_NONE:
        remove_owner_from_table(owner_id)

    return event_handler_status.SUCCESS


def remove_owner_from_table(owner_id: int) -> None:
    try:
        OwnerTable().delete_item({"owner-id": owner_id})
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException(
            "General exception occurred when removing owner id {0} from table. Error: {1}".format(owner_id, error))


def remove_owner_api_key(owner_id, event_id):
    existing_owner = owner_event_handler.get_owner_from_owner_table(owner_id)
    try:
        # api-key-id is optional in the schema but required for API key removal
        if 'api-key-id' in existing_owner:
            delete_api_key(existing_owner['api-key-id'])
            existing_owner['api-key-id'] = 'REMOVED'
        existing_owner['api-key'] = 'REMOVED'
        existing_owner['event-ids'] = OwnerTable().build_event_ids_list(existing_owner, event_id)
        OwnerTable().update_item(existing_owner)
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as e:
        raise TerminalErrorException("Failed to remove owner apiKey from Owner table due to unknown reasons. "
                                     "OwnerId = {0} | Exception = {1}".format(owner_id, e))


def delete_api_key(api_key_id):
    try:
        apigateway.get_client().delete_api_key(apiKey=api_key_id)
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as e:
        raise TerminalErrorException(
            "Failed to delete api key from ApiGateway due to unknown reasons. "
            "apiKey = {0} | Exception = {1}".format(api_key_id, e))


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("** Local Test Run **")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-OwnerTable'
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/" \
                                    "create-owner-event-handler-RetryQueue-1UXMVBY5ID6JO"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-DataLake-CollectionTable'
    os.environ['COLLECTION_DYNAMODB_OWNER_ID_AND_NAME_INDEX'] = 'collection-by-owner-and-name-index'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-DataLake-TrackingTable'

    owner_event_handler.collection_table_index = os.environ['COLLECTION_DYNAMODB_OWNER_ID_AND_NAME_INDEX']

    msg = {
        "owner-id": 1251,
        "owner-name": "RemoveOwnerTest",
        "email-distribution": [
            "wormholeuk@lexisnexis.com",
            "wormholeuk@legal.regn.net"
        ],
        "request-time": "2017-11-13T10:16:08.829Z",
        "event-name": event_names.OWNER_REMOVE,
        "event-id": "xyz",
        "event-version": 1,
        "stage": "LATEST"
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - REMOVE OWNER EVENT HANDLER LAMBDA]")
