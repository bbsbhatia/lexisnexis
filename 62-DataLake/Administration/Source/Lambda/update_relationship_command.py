import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization, command_validator
from lng_datalake_commands.exceptions import InvalidRequestPropertyValue, InternalError, NoSuchRelationship, \
    SemanticError
from lng_datalake_commons import session_decorator
from lng_datalake_constants import relationship_status, event_names
from lng_datalake_dal.relationship_owner_table import RelationshipOwnerTable

from service_commons import relationship_command

__author__ = "Doug Heitkamp, Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

event_version = os.getenv("EVENT_VERSION", 1)

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)

libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command("Schemas/UpdateRelationshipCommand-RequestSchema.json",
                         "Schemas/UpdateRelationshipCommand-ResponseSchema.json", event_names.RELATIONSHIP_UPDATE)
def lambda_handler(event, context):
    return update_relationship_command(event['request'],
                                       event['context']['client-request-id'],
                                       event['context']['stage'],
                                       event['context']['api-key-id'])


def update_relationship_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    relationship_id = request['relationship-id']
    patch_operations = request['patch-operations']

    command_validator.validate_patch_operations("Schemas/Patch/Relationships", patch_operations)

    current_relationship = get_relationship_by_id(relationship_id)

    # owner authorization
    owner_id = current_relationship['owner-id']
    owner_authorization.authorize(owner_id, api_key_id)

    relationship_id_event_actions = transform_patch_operations(patch_operations, current_relationship)

    response_json = generate_json_response(current_relationship)

    event_dict = generate_event_dict(response_json, relationship_id_event_actions, request_id, stage)

    return {'response-dict': response_json, 'event-dict': event_dict}


def get_relationship_by_id(relationship_id: str) -> dict:
    try:
        relationship_response = RelationshipOwnerTable().get_item({'relationship-id': relationship_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Relationship Table", e)
    except Exception as ex:
        raise InternalError("Unhandled exception occurred", ex)

    if not relationship_response:
        raise NoSuchRelationship("Invalid Relationship ID {}".format(relationship_id),
                                 "Relationship ID does not exist")

    if relationship_response['relationship-state'] == relationship_status.REMOVED:
        raise SemanticError(
            'Invalid Relationship ID {0}'.format(relationship_response['relationship-id']),
            'Relationship cannot be updated in {} state'.format(
                relationship_response['relationship-state']))
    return relationship_response


def transform_patch_operations(patches: list, current_relationship: dict) -> dict:
    event_actions = {}
    add_collection_ids = set()
    remove_collection_ids = set()
    add_catalog_ids = set()
    remove_catalog_ids = set()
    total_commitment_ids = 0

    for action in patches:
        if action["path"] == "/description" and action["value"] != current_relationship.get('description', None):
            event_actions['description'] = action["value"]
        elif action["path"] == "/catalog-commitments":
            total_commitment_ids = transform_catalog_commitments(total_commitment_ids, action, add_catalog_ids,
                                                                 remove_catalog_ids)
        elif action["path"] == "/collection-commitments":
            total_commitment_ids = transform_collection_commitments(total_commitment_ids, action, add_collection_ids,
                                                                    remove_collection_ids)

    transform_event_actions(event_actions, add_catalog_ids, remove_catalog_ids, add_collection_ids,
                            remove_collection_ids)

    return event_actions


def transform_catalog_commitments(total_commitment_ids: int, event_action: dict, add_catalog_ids: set,
                                  remove_catalog_ids: set) -> int:
    total_commitment_ids += len(event_action["value"])
    validate_commitment_limit(total_commitment_ids)

    if event_action["op"] == "remove":
        remove_catalog_ids.update(event_action["value"])
        add_catalog_ids.difference_update(event_action["value"])
    else:
        relationship_command.validate_catalog_commitments(event_action["value"])
        add_catalog_ids.update(event_action["value"])
        remove_catalog_ids.difference_update(event_action["value"])

    return total_commitment_ids


def transform_collection_commitments(total_commitment_ids: int, event_action: dict, add_collection_ids: set,
                                     remove_collection_ids: set) -> int:
    total_commitment_ids += len(event_action["value"])
    validate_commitment_limit(total_commitment_ids)

    if event_action["op"] == "remove":
        remove_collection_ids.update(event_action["value"])
        add_collection_ids.difference_update(event_action["value"])
    else:
        relationship_command.validate_collection_commitments(event_action["value"])
        add_collection_ids.update(event_action["value"])
        remove_collection_ids.difference_update(event_action["value"])

    return total_commitment_ids


def validate_commitment_limit(total_commitment_ids: int) -> None:
    if total_commitment_ids > 500:
        raise InvalidRequestPropertyValue("Invalid number of Commitment IDs {}".format(total_commitment_ids),
                                          "Number of Commitment IDs should not exceed 500")


def transform_event_actions(event_actions: dict, add_catalog_ids: set, remove_catalog_ids: set, add_collection_ids: set,
                            remove_collection_ids: set) -> None:
    catalog_id_actions = {}
    collection_id_actions = {}
    if add_catalog_ids:
        catalog_id_actions['add'] = list(add_catalog_ids)

    if remove_catalog_ids:
        catalog_id_actions['remove'] = list(remove_catalog_ids)

    if add_collection_ids:
        collection_id_actions['add'] = list(add_collection_ids)

    if remove_collection_ids:
        collection_id_actions['remove'] = list(remove_collection_ids)

    if catalog_id_actions:
        event_actions['catalog-id-actions'] = catalog_id_actions

    if collection_id_actions:
        event_actions['collection-id-actions'] = collection_id_actions


def generate_json_response(current_relationship: dict) -> dict:
    req_keys = command_wrapper.get_required_response_schema_keys('update-relationship-properties')
    optional_keys = command_wrapper.get_optional_response_schema_keys('update-relationship-properties')

    return_dict = {}

    for prop in req_keys:
        if prop in current_relationship:
            return_dict[prop] = current_relationship[prop]
        else:
            raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_keys:
        if prop in current_relationship:
            return_dict[prop] = current_relationship[prop]

    return return_dict


def generate_event_dict(response_json: dict, relationship_id_actions: dict, request_id: str, stage: str) -> dict:
    event_entry = {'relationship-id': response_json['relationship-id'],
                   'request-time': command_wrapper.get_request_time(),
                   'event-name': event_names.RELATIONSHIP_UPDATE,
                   'event-id': request_id,
                   'event-version': event_version,
                   'stage': stage}

    if "description" in response_json and "description" not in relationship_id_actions:
        event_entry['description'] = response_json['description']
    elif "description" in relationship_id_actions:
        event_entry['description'] = relationship_id_actions['description']

    if "collection-id-actions" in relationship_id_actions:
        event_entry['collection-id-actions'] = relationship_id_actions['collection-id-actions']

    if "catalog-id-actions" in relationship_id_actions:
        event_entry['catalog-id-actions'] = relationship_id_actions['catalog-id-actions']

    return event_entry


if __name__ == '__main__':
    import random
    import string
    from datetime import datetime
    from lng_datalake_testhelper import mock_lambda_context
    import json

    os.environ["AWS_PROFILE"] = 'c-sand'
    os.environ["RELATIONSHIP_OWNER_DYNAMODB_TABLE"] = "feature-djh-DataLake-RelationshipOwnerTable"
    os.environ["OWNER_DYNAMODB_TABLE"] = "feature-djh-DataLake-OwnerTable"
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-djh-DataLake-EventStoreTable'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-djh-DataLake-CatalogTable'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-djh-DataLake-CollectionTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-djh-DataLake-TrackingTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN END - UPDATE RELATIONSHIPS COMMAND LAMBDA]")

    apig_json = \
        {
            "context": {
                "client-request-id": '"1234-request-id-wxyz"',
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "PATCH",
                "stage": "blah",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "relationship-id": "judges",
                "patch-operations": [
                    {
                        "op": "replace",
                        "path": "/description",
                        "value": "hello"
                    },
                    {
                        "op": "add",
                        "path": "/catalog-commitments",
                        "value": ["22", "23"]
                    },
                    {
                        "op": "add",
                        "path": "/collection-commitments",
                        "value": ["2", "62"]
                    }
                ]
            }
        }

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)
    command_validator.WORKING_DIRECTORY = os.path.dirname(__file__)

    lambda_response = lambda_handler(apig_json, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - UPDATE RELATIONSHIPS COMMAND LAMBDA]")
