import logging
import os

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import SemanticError, InvalidRequestPropertyValue
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names

from service_commons import owner_command

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/UpdateOwnerCommand-RequestSchema.json',
                         'Schemas/UpdateOwnerCommand-ResponseSchema.json', event_names.OWNER_UPDATE)
def lambda_handler(event, context):
    return update_owner_command(event['request'],
                                event['context']['client-request-id'],
                                event['context']['stage'],
                                event['context']['api-key-id'])


def update_owner_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # owner authorization
    owner_id = request['owner-id']
    owner_response = owner_authorization.authorize(owner_id, api_key_id)

    new_request = replace_op_commands(request)

    response_dict = owner_command.generate_json_response(new_request, owner_response)
    if not has_owner_changes(owner_response, response_dict):
        raise SemanticError("No data changes")

    owner_command.validate_owner(response_dict)

    # TODO We accept only one prefix per owner for now,
    #  but we have the type as list to support more than one prefix in the future
    if "collection-prefixes" in response_dict and len(response_dict["collection-prefixes"]) > 1:
        collection_prefix = response_dict["collection-prefixes"]
        response_dict['collection-prefix'] = collection_prefix

    event_dict = owner_command.create_owner_event_store_item(response_dict, event_names.OWNER_UPDATE, request_id,
                                                             event_version, stage)
    response_dict.pop('collection-prefix', '')
    return {'response-dict': response_dict, 'event-dict': event_dict}


# TODO: Add ability for smart patch merging (ie add) today we only support full replace
def replace_op_commands(request: dict) -> dict:
    for item in request['patch-operations']:
        if item['op'] != "replace":
            raise InvalidRequestPropertyValue("Invalid operation verb supplied", "Only replace is currently supported")
        request[item['path'].split('/')[1]] = item['value']
    del request['patch-operations']
    return request


def has_owner_changes(existing_owner, updated_owner):
    if existing_owner["owner-name"] != updated_owner["owner-name"]:
        return True
    if existing_owner["email-distribution"] != updated_owner["email-distribution"]:
        return True
    return False


if __name__ == '__main__':
    import json
    import random
    import string
    from datetime import datetime
    from lng_datalake_testhelper import mock_lambda_context

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE OWNER COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-man-DataLake-OwnerTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-man-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-man-DataLake-TrackingTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    apig_json = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "PATCH",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "owner-id": 417,
                "patch-operations": [{
                    "op": "replace",
                    "path": "/owner-name",
                    "value": "Wormhole API Test 5"
                }, {
                    "op": "replace",
                    "path": "/email-distribution",
                    "value": ["WormholeApiTest5@ReedElsevier.com"]
                }]
            }
        }

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for i in range(n)])

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    json_response = lambda_handler(apig_json, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - UPDATE OWNER COMMAND LAMBDA]")
