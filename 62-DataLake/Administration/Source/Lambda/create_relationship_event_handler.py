import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.relationship_commitment_table import RelationshipCommitmentTable
from lng_datalake_dal.relationship_owner_table import RelationshipOwnerTable

from service_commons import relationship_event_handler

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)

libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

# Set environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {}".format(orjson.dumps(event).decode()))
    logger.info("Context: {}".format(vars(context)))
    return create_relationship_event_handler(event, context.invoked_function_arn)


def create_relationship_event_handler(event, lambda_arn):
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.RELATIONSHIP_CREATE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.RELATIONSHIP_CREATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    relationship_dict = relationship_event_handler.generate_relationship_item(message)

    if message.get('additional-attributes', {}).get('relationship-id-not-created', True):
        insert_to_relationship_owner_table(relationship_dict)

    create_collection_commitments(message)

    create_catalog_commitments(message)

    return event_handler_status.SUCCESS


def insert_to_relationship_owner_table(rel: dict) -> None:
    try:
        RelationshipOwnerTable().put_item(rel)
        logger.info("Insertion to RelationshipOwner table succeeded.")
    except (ConditionError,
            SchemaError,
            SchemaLoadError,
            SchemaValidationError) as exception:
        raise TerminalErrorException("Failed to insert item into RelationshipOwner table. Item = {0} | Exception = {1}"
                                     .format(rel, exception))
    except error_handler.retryable_exceptions as exception:
        raise exception
    except Exception as exception:
        raise TerminalErrorException("Failed to insert item into RelationshipOwner table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(rel, exception))
    error_handler.set_message_additional_attribute("relationship-id-not-created", False)


def create_collection_commitments(message: dict) -> None:
    if 'collection-commitments' in message and message.get('additional-attributes', {}).get(
            'collection-commitments-not-completed', True):
        current_index = message.get('additional-attributes', {}).get('collection-id-index', 0)
        for index, value in enumerate(message['collection-commitments'], start=current_index):
            try:
                insert_to_relationship_commitment_table({'relationship-id': message['relationship-id'],
                                                         'target-composite-key': '{0}|{1}'.format('collection',
                                                                                                  value),
                                                         'target-type': 'Collection'})
            except TerminalErrorException as e:
                raise e
            except error_handler.retryable_exceptions as e:
                error_handler.set_message_additional_attribute('collection-id-index', index)
                raise e
        error_handler.set_message_additional_attribute('collection-commitments-not-completed', False)
        logger.info("Insertion of collections to RelationshipCommitmentTable table succeeded.")


def insert_to_relationship_commitment_table(commitment: dict) -> None:
    try:
        RelationshipCommitmentTable().put_item(commitment)
    except (ConditionError,
            SchemaError,
            SchemaLoadError,
            SchemaValidationError) as exception:
        raise TerminalErrorException(
            "Failed to insert item into RelationshipCommitmentTable table. Item = {0} | Exception = {1}"
                .format(commitment, exception))
    except error_handler.retryable_exceptions as exception:
        raise exception
    except Exception as exception:
        raise TerminalErrorException(
            "Failed to insert item into RelationshipCommitmentTable table due to unknown reasons. "
            "Item = {0} | Exception = {1}".format(commitment, exception))


def create_catalog_commitments(message: dict) -> None:
    if 'catalog-commitments' in message and message.get('additional-attributes', {}).get(
            'catalog-commitments-not-completed', True):
        current_index = message.get('additional-attributes', {}).get('catalog-id-index', 0)
        for index, value in enumerate(message['catalog-commitments'], start=current_index):
            try:
                insert_to_relationship_commitment_table({'relationship-id': message['relationship-id'],
                                                         'target-composite-key': '{0}|{1}'.format('catalog', value),
                                                         'target-type': 'Catalog'})
            except TerminalErrorException as e:
                raise e
            except error_handler.retryable_exceptions as e:
                error_handler.set_message_additional_attribute('catalog-id-index', index)
                raise e
        error_handler.set_message_additional_attribute('catalog-commitments-not-completed', False)
        logger.info("Insertion of catalogs to RelationshipCommitmentTable table succeeded.")


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("** Local Test Run **")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RELATIONSHIP_OWNER_DYNAMODB_TABLE"] = "feature-djh-DataLake-RelationshipOwnerTable"
    os.environ["RELATIONSHIP_COMMITMENT_DYNAMODB_TABLE"] = "feature-djh-DataLake-RelationshipCommitmentTable"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "feature-djh-datalake-lambda-terminal-errors"

    msg = {'catalog-commitments': ['48',
                                   '49',
                                   '50',
                                   '54'],
           'owner-id': 1,
           'relationship-id': 'judges1',
           'relationship-type': 'Metadata',
           'event-id': '863f9c2d-a964-11e9-be25-a94e02f89ef9',
           'stage': 'LATEST',
           'request-time': '2019-07-18T14:01:22.565Z',
           'event-name': 'Relationship::Create',
           'collection-commitments': ['2',
                                      '60',
                                      '62',
                                      '64'],
           'event-version': 1,
           'seen-count': 1,
           'additional-attributes': {'collection-commitments-not-completed': False,
                                     'catalog-id-index': 3}}

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - CREATE OWNER EVENT HANDLER LAMBDA]")
