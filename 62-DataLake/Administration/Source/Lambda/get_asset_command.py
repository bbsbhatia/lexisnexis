import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NoSuchAsset, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_dal.asset_table import AssetTable

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command("Schemas/GetAssetCommand-RequestSchema.json",
                         "Schemas/GetAssetCommand-ResponseSchema.json")
def lambda_handler(event, context):
    return get_asset_command(event['request'])


def get_asset_command(request: dict) -> dict:
    asset_id = request['asset-id']

    try:
        asset_response = AssetTable().get_item({"asset-id": asset_id})
    except ClientError as error:
        raise InternalError("Unable to get item from Asset Table", error)
    except Exception as error:
        raise UnhandledException(error)

    if not asset_response:
        raise NoSuchAsset("Invalid Asset ID {0}".format(asset_id), "Asset ID does not exist")

    return {"response-dict": generate_response_json(asset_response)}


def generate_response_json(input_dict):
    response_dict = {}

    required_props = command_wrapper.get_required_response_schema_keys('asset-properties')
    prop = None
    try:
        for prop in required_props:
            response_dict[prop] = input_dict[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError("Missing required property: {0}".format(prop))

    return response_dict


if __name__ == '__main__':
    import json
    import random
    import string
    from lng_datalake_testhelper import mock_lambda_context

    os.environ["AWS_PROFILE"] = "product-datalake-dev-devopsadmin"
    os.environ['ASSET_DYNAMODB_TABLE'] = 'DataLakeDynamo-AssetTable-12S1V8YP519H8'

    apig_json = \
        {
            "context": {
                "client-request-id": "test-invoke-request",
                "client-request-time": "2018-05-07T16:02:29.607741",
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "asset-id": 62
            }
        }

    rand_str = lambda n: "".join([random.choice(string.ascii_letters) for _ in range(n)])

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    json_response = lambda_handler(apig_json, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - GET ASSET COMMAND LAMBDA]")
