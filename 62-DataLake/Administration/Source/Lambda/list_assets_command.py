import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, paginator_token, command_validator
from lng_datalake_commands.exceptions import InternalError, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_dal.asset_table import AssetTable

__author__ = "Prashant Srivastava, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 1000))


@session_decorator.lng_aws_session()
@command_wrapper.command("Schemas/ListAssetsCommand-RequestSchema.json",
                         "Schemas/ListAssetsCommand-ResponseSchema.json")
def lambda_handler(event, context):
    return list_assets_command(event["request"])


def list_assets_command(request):
    # optional properties
    max_items = request.get('max-items', DEFAULT_MAX_ITEMS)
    next_token = request.get('next-token')
    command_validator.validate_max_results(max_items, DEFAULT_MAX_ITEMS)
    pagination_token = None

    # If we have a next-token validate the max-items matches the original request
    # Dyanmo doesn't handle pagination if the max-items changes
    if next_token:
        tokens = paginator_token.unpack_validate_pagination_token(next_token, max_items)
        pagination_token = tokens['pagination-token']

    try:
        list_of_assets = AssetTable().get_all_items(max_items=max_items,
                                                    pagination_token=pagination_token)
    except ClientError as error:
        raise InternalError("Unable to get items from Asset Table", error)
    except Exception as error:
        raise UnhandledException(error)

    return {"response-dict": generate_json_response(list_of_assets, AssetTable().get_pagination_token(), max_items)}


def generate_json_response(list_of_assets, pagination_token, max_items):
    asset_response = []
    for asset in list_of_assets:
        response_item = {
            'asset-id': asset['asset-id'],
            'asset-name': asset['asset-name']
        }
        asset_response.append(response_item)
    response = {"assets": asset_response}

    if pagination_token:
        response["next-token"] = paginator_token.generate_pagination_token(max_items, pagination_token)

    response['item-count'] = len(list_of_assets)

    return response


if __name__ == '__main__':
    import json
    from datetime import datetime
    from lng_datalake_testhelper import mock_lambda_context

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["ASSET_DYNAMODB_TABLE"] = "feature-mas-DataLake-AssetTable"

    apig_json = \
        {
            "context": {
                "client-request-id": '"1234-request-id-wxyz"',
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "max-items": 1,
                "next-token": "1|eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQXNzZXRJRCI6IHsiTiI6ICIxIn19fQ=="
            }
        }

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(apig_json, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - LIST ASSETS COMMAND LAMBDA]")
