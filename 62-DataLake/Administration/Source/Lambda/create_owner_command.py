import logging
import os
import re
from datetime import datetime

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_aws_clients import apigateway
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, UnhandledException
from lng_datalake_commands.exceptions import InvalidRequestPropertyValue, ConflictingRequestError, \
    CollectionPrefixAlreadyExists
from lng_datalake_commons import email_helper
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names
from lng_datalake_dal.counter_table import CounterTable
from lng_datalake_dal.owner_table import OwnerTable

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)
asset_group = os.getenv("ASSET_GROUP")


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/CreateOwnerCommand-RequestSchema.json',
                         'Schemas/CreateOwnerCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return create_owner_command(event['request'],
                                event['context']['client-request-id'],
                                event['context']['stage'])


def create_owner_command(request: dict, request_id: str, stage: str) -> dict:
    # required
    owner_name = request['owner-name'].strip()
    request['email-distribution'] = list(set([email.lower().strip() for email in request['email-distribution']]))
    email_distribution = request['email-distribution']
    # optional
    collection_prefixes = request.get("collection-prefixes")

    validate_owner_name(owner_name)
    validate_email_list(email_distribution)
    validate_unique_owner(owner_name, email_distribution)

    if collection_prefixes:
        # TODO We accept only one prefix right now, this is restricted at gateway model
        if len(collection_prefixes) > 1:
            raise InvalidRequestPropertyValue(
                "Invalid number of collection Prefixes {0}".format(len(collection_prefixes)),
                "Number of Collection Prefixes should not exceed 1")

        validate_collection_prefix(collection_prefixes[0])
        validate_unique_collection_prefix(collection_prefixes[0])

    request['owner-id'] = get_next_owner_id()

    # Create an API Key with usage plan and add to response
    request['api-key'], request['api-key-id'] = create_api_key(request)

    response_json = generate_json_response(request)

    event_dict = generate_event_dict(request,
                                     request_id,
                                     stage)

    return {'response-dict': response_json, 'event-dict': event_dict}


def validate_owner_name(owner_name: str) -> None:
    if owner_name.isdigit():
        raise InvalidRequestPropertyValue("Invalid Owner Name {0}".format(owner_name),
                                          "Owner Name cannot contain only digits")

    if not 1 <= len(owner_name) <= 60:
        raise InvalidRequestPropertyValue("Invalid Owner Name {0}".format(owner_name),
                                          "Owner Name should not be empty and length should not exceed 60 characters")


def validate_email_list(email_list: list) -> None:
    invalid_list = []
    for email in email_list:
        if not email_helper.is_valid_email_format(email):
            invalid_list.append(email)

    if invalid_list:
        raise InvalidRequestPropertyValue(
            "The following emails are invalid {0}".format(invalid_list))


def validate_unique_owner(owner_name: str, email_distribution: list) -> None:
    try:
        response = OwnerTable().query_items(index="owner-by-name-index",
                                            KeyConditionExpression='OwnerName=:owner_name',
                                            ExpressionAttributeValues={
                                                ":owner_name": {"S": owner_name}})
    except ClientError as e:
        raise InternalError("Unable to query items from Owner Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    for owner in response:
        if owner["api-key-id"] != "REMOVED" and set(email_distribution) == set(owner["email-distribution"]):
            raise ConflictingRequestError(
                "Owner already exists with owner-id {0} for given owner-name and email-distribution".format(
                    owner["owner-id"]))


def validate_collection_prefix(collection_prefix: str) -> None:
    # capital alphanumeric, not all digits
    match_results = re.match(r"^\d*[A-Z][A-Z0-9]*$", collection_prefix)

    if not match_results:
        raise InvalidRequestPropertyValue(
            "Invalid Collection prefix {0}".format(collection_prefix),
            "Collection prefix can only contain capital letters and digits, "
            "Collection prefix cannot contain only digits")

    if len(collection_prefix) > 10:
        raise InvalidRequestPropertyValue("Invalid Collection prefix {0}".format(collection_prefix),
                                          "Collection prefix length should not exceed 10 characters")


def validate_unique_collection_prefix(collection_prefix: str) -> None:
    try:
        response = OwnerTable().query_items(index="owner-by-collection-prefix-index",
                                            KeyConditionExpression='CollectionPrefix=:collection_prefix',
                                            ExpressionAttributeValues={
                                                ":collection_prefix": {"S": collection_prefix}})

    except ClientError as e:
        raise InternalError("Unable to query items from Owner Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if response:
        raise CollectionPrefixAlreadyExists("Collection prefix already exists",
                                            "Owner ID {0} owns this prefix".format(response[0]["owner-id"]))


def get_next_owner_id() -> int:
    try:
        # "owner" represents the Namespace to be atomically increased by one
        return CounterTable().update_counter('owner')
    except ClientError as error:
        raise InternalError("Unable to update Counter Table", error)
    except Exception as error:
        raise UnhandledException(error)


def create_api_key(request: dict) -> tuple:
    if not asset_group:
        raise InternalError("Required environment variable ASSET_GROUP not defined or empty")

    # Get usage plan first to avoid creating unused API Keys if usage plan not found
    usage_plan_name = '{}-All-DataLake'.format(asset_group)
    usage_plan = get_usage_plan(usage_plan_name)

    try:
        api_key_name = "{}:{}".format(asset_group, request['owner-id'])
        api_key_resp = apigateway.get_client().create_api_key(name=api_key_name,
                                                              enabled=True,
                                                              description="DataLake API Key {}".format(api_key_name),
                                                              generateDistinctId=True)

        api_key = api_key_resp['value']
        api_key_id = api_key_resp['id']

        # Add API Key to usage plan
        apigateway.get_client().create_usage_plan_key(
            usagePlanId=usage_plan['id'],
            keyId=api_key_resp['id'],
            keyType='API_KEY'
        )

    except ClientError as error:
        raise InternalError("Unable to create API Key", error)
    except KeyError as error:
        raise InternalError("API Key not returned", error)
    except Exception as error:
        raise UnhandledException(error)

    return api_key, api_key_id


def get_usage_plan(usage_plan_name) -> dict:
    try:
        paginator = apigateway.get_client().get_paginator('get_usage_plans')
        for page in paginator.paginate():
            for item in page['items']:
                if item['name'] == usage_plan_name:
                    return item

        raise InternalError("Could not find usage plan {}".format(usage_plan_name))
    except ClientError as error:
        raise InternalError("Unable get usage plans for current account", error)
    except KeyError as error:
        raise InternalError("Key error while searching for usage plan", error)
    except InternalError:
        raise
    except Exception as error:
        raise UnhandledException(error)


def generate_json_response(new_owner_values: dict) -> dict:
    req_keys = command_wrapper.get_required_response_schema_keys('owner-properties')
    optional_keys = command_wrapper.get_optional_response_schema_keys('owner-properties')

    return_dict = {}

    for prop in req_keys:
        if prop in new_owner_values:
            return_dict[prop] = new_owner_values[prop]
        else:
            raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_keys:
        if prop in new_owner_values:
            return_dict[prop] = new_owner_values[prop]
    return return_dict


def generate_event_dict(owner_request: dict, request_id: str, stage: str) -> dict:
    event_item = {
        "owner-id": owner_request["owner-id"],
        "owner-name": owner_request["owner-name"],
        "email-distribution": owner_request["email-distribution"],
        "event-id": request_id,
        "api-key-id": owner_request['api-key-id'],
        "request-time": command_wrapper.get_request_time(),
        "event-name": event_names.OWNER_CREATE,
        "event-version": event_version,
        "stage": stage
    }

    # Add collection-prefix if it exists in owner request
    if "collection-prefixes" in owner_request:
        event_item["collection-prefix"] = owner_request["collection-prefixes"][0]

    return event_item


if __name__ == '__main__':
    import json
    import random
    import string
    from lng_datalake_testhelper import mock_lambda_context

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['ASSET_GROUP'] = 'feature-mas'
    os.environ['COUNTER_DYNAMODB_TABLE'] = 'feature-mas-DataLake-CounterTable'
    os.environ["OWNER_DYNAMODB_TABLE"] = "feature-mas-DataLake-OwnerTable"
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-mas-DataLake-EventStoreTable'
    asset_group = os.getenv("ASSET_GROUP")

    main_owner_name = "Sammmm"
    distribution_list = ["sam@LexisNexias.com"]


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    apig_json = \
        {
            "context": {
                "client-request-id": "1234",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "POST",
                "stage": 'LATEST'
            },
            "request": {
                "owner-name": main_owner_name,
                "email-distribution": distribution_list
            }
        }

    rand_str = lambda n: ''.join([random.choice(string.ascii_letters) for _ in range(n)])

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    json_response = lambda_handler(apig_json, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE OWNER COMMAND LAMBDA]")
