import logging
import os

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper
from lng_datalake_commons import session_decorator

from service_commons import owner_command

__author__ = "Chuck Nelson"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command("Schemas/GetOwnerCommand-RequestSchema.json",
                         "Schemas/GetOwnerCommand-ResponseSchema.json")
def lambda_handler(event, context):
    return get_owner_command(event['request'])


def get_owner_command(request: dict) -> dict:
    owner_id = request['owner-id']
    owner_response = owner_command.get_owner_from_owner_table(owner_id)

    return {"response-dict": owner_command.generate_json_response({}, owner_response)}


if __name__ == '__main__':
    import json
    import random
    import string
    from lng_datalake_testhelper import mock_lambda_context

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["OWNER_DYNAMODB_TABLE"] = "feature-man-DataLake-OwnerTable"


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    apig_json = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": "1526671877813",
                "http-method": "GET",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "owner-id": 461
            }
        }

    rand_str = lambda n: "".join([random.choice(string.ascii_letters) for _ in range(n)])

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = rand_str(16)

    json_response = lambda_handler(apig_json, mock_context)

    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - GET OWNER COMMAND LAMBDA]")
