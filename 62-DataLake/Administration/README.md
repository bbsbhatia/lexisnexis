# Introduction 
Administration is one of the micro services within the DataLake project. Its purpose is to manage Owners and Assets within the DataLake.

##Getting Started

####Installation process

To get this code up and running, you will need Python installed on your machine. When running the code, we recommend installing [IntelliJ](https://www.jetbrains.com/idea/) with the following plugins:

* Python Community Edition


* AWS CloudFormation

You should ensure you have updated the pypi endpoint within IntelliJ following [these instructions](https://wiki.regn.net/wiki/Project_Setup#Update_pypi_endpoint:).

##### Virtual Environments

You should create the following directory `c:/venv` this will be for any Python packages that require a virtual environment. You can also install [this plugin](https://wiki.regn.net/wiki/Project_Wormhole_Utilities#Create_Virtual_Environments), which will configure the virtual environments within IntelliJ.

##### AWS Console Access

To run the Lambdas locally within this project, you will need to have access to the [AWS sandbox](https://wiki.regn.net/wiki/Newlexis_AWS_console_and_API_based_access_information#Console_Access) and have set up and run the [CloudKey Powershell script](https://wiki.regn.net/wiki/Newlexis_AWS_console_and_API_based_access_information#CLI_.26_API_access_via_SAML_Authorization).

#### Software dependencies

You can see the Python packages that are required for the project within `62-DataLake-Administration\requirements.txt`

##### Adding new packages

Should you need to add any more packages, you can do so in IntelliJ under File > Project Structure > SDKs > Packages. You can then add the package to your `requirements.txt` by activating the virtual environment for the project, and running `pip freeze > requirements.txt`.

#### API Specification

##### Swagger

We use Swagger to document the API endpoints created for this service. You can view this documentation by uploading the `swagger.json` file to the editor in [swagger.io](https://swagger.io/swagger-editor/)

##Build and Test

#####Run Lambdas

To run the Lambdas locally, first ensure you have the [CloudKey Powershell script](https://wiki.regn.net/wiki/Newlexis_AWS_console_and_API_based_access_information#CLI_.26_API_access_via_SAML_Authorization) running in the background. Then you can manually run the Lambdas by clicking the `main` runner in the relevant command Lambda.

##### Tests

To run the tests for this project, you can right click the `test_runner.py`  and click 'Run'.

##Contribute

If you are interested in contributing directly to the code base, please submit a pull request and for further information you can visit the [Project Wormhole Wiki page](https://wiki.regn.net/wiki/Project_Wormhole).