import logging
import os
import random
import time
import unittest
from datetime import datetime

from lng_datalake_client.Admin.remove_owner import remove_owner
from lng_datalake_client.Collection.create_collection import create_collection
from lng_datalake_client.Collection.get_collection import get_collection
from lng_datalake_client.Collection.remove_collection import remove_collection
from lng_datalake_client.Collection.update_collection import suspend_collection
from lng_datalake_client.Admin.get_owner import get_owner

__author__ = "Jason Feng, Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


class TestAdminOrchestration(unittest.TestCase):
    waiting_time = int(os.getenv('WAITING_TIME', 5))
    get_owner_loop_times = int(os.getenv('GET_OWNER_LOOP_TIMES', 30))
    update_owner_loop_times = int(os.getenv('UPDATE_OWNER_LOOP_TIMES', 30))
    remove_owner_loop_times = int(os.getenv('REMOVE_OWNER_LOOP_TIMES', 30))
    collection_loop_times = int(os.getenv('COLLECTION_LOOP_TIMES', 10))

    collections_created = []
    owners_created = []

    @classmethod
    def setUpClass(cls):  # NOSONAR
        logger.info("### Setting up tests for {} ###".format(cls.__name__))
        # nothing to set up in case of owner for now

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        logger.info("### Cleaning up tests for {} ###".format(cls.__name__))
        time.sleep(cls.waiting_time)

        logger.info("Fallback suspend collection")
        for collection in cls.collections_created:
            suspend_collection(collection)

        logger.info("Fallback terminate collection")
        for collection in cls.collections_created:
            remove_collection(collection)

        logger.info("Fallback remove owner")
        for owner in cls.owners_created:
            os.environ['api-key'] = owner['api-key']
            remove_owner(owner)

    def test_orchestration(self):
        created_owner_with_prefix, created_owner_with_collection, created_collection = self.create_owners()
        os.environ['X-API-KEY'] = created_owner_with_prefix['api-key']
        self.get_owner(created_owner_with_prefix)
        self.list_owners()
        updated_owner_with_prefix = self.update_owner(created_owner_with_prefix,
                                                      created_owner_with_collection['api-key'])
        updated_owner_with_collection = self.update_owner(created_owner_with_collection,
                                                          created_owner_with_prefix['api-key'])

        # remove owner to be the last to execute
        self.remove_owner(updated_owner_with_prefix, updated_owner_with_collection, created_collection)

    def create_owners(self):
        logger.info("### Running create owner tests ###")
        from Tests.Regression.regression_create_owner import TestCreateOwner
        test_create_owner_obj = TestCreateOwner()

        owner_name = "test_owner_{}".format(datetime.now().isoformat())
        email_list = ["test_owner@lexisnexis.com"]

        # Prefix has limit to 10 chars and I wanted a way to generate unique prefix every time regression run
        # So i am using Hex representation of Epoch time as prefix
        collection_prefix = '{0:X}'.format(round(time.time()))

        create_owner_request = {"owner-name": owner_name,
                                "email-distribution": email_list,
                                "collection-prefixes": [collection_prefix]
                                }

        logger.info('Creating a new owner: {}'.format(create_owner_request))

        owner_response_dict = test_create_owner_obj.test_create_owner(create_owner_request)
        os.environ['X-API-KEY'] = owner_response_dict['api-key']
        self.owners_created.append(owner_response_dict)

        # wait a little longer just to make sure API key is available
        time.sleep(self.waiting_time * 8)

        logger.info("Validating owner-id {}...".format(owner_response_dict['owner-id']))
        for i in range(0, self.get_owner_loop_times):
            time.sleep(self.waiting_time)
            get_owner_response = get_owner(owner_response_dict)
            if get_owner_response.status_code == 200:
                break
            if i == self.get_owner_loop_times - 1:
                raise Exception("Owner not created; status={}".format(get_owner_response.status_code))
        logger.info('Successfully created a new owner-id: {}'.format(owner_response_dict['owner-id']))

        # creating an owner which has a collection mapped to it
        owner_name_with_collection = "test_owner_{}".format(datetime.now().isoformat())
        logger.info("Creating an owner with collection: {}".format({"owner-name": owner_name_with_collection,
                                                                    "email-distribution": email_list}))
        owner_with_collection_response = test_create_owner_obj.test_create_owner(
            {"owner-name": owner_name_with_collection, "email-distribution": email_list})
        os.environ['X-API-KEY'] = owner_with_collection_response['api-key']
        self.owners_created.append(owner_with_collection_response)

        # wait a little longer just to make sure API key is available
        time.sleep(self.waiting_time * 8)

        logger.info("Validating owner-id {}...".format(owner_with_collection_response['owner-id']))
        for i in range(0, self.get_owner_loop_times):
            time.sleep(self.waiting_time)
            get_owner_response = get_owner(owner_response_dict)
            if get_owner_response.status_code == 200:
                break
            if i == self.get_owner_loop_times - 1:
                raise Exception("Owner not created; status={}".format(get_owner_response.status_code))
        logger.info('Successfully created a new owner-id: {}'.format(owner_with_collection_response['owner-id']))

        # create a collection with this owner
        logger.info("Creating collection for the owner-id: {}".format(owner_with_collection_response['owner-id']))
        collection_input_data = {"asset-id": 62}
        created_collection_response = create_collection({"body": collection_input_data,
                                                         "collection-id": "admin-regression-{}".format(
                                                             datetime.timestamp(datetime.now()))[:-7]})

        if created_collection_response.status_code != 202:
            if 'error' in created_collection_response.json():
                logger.error(
                    "Unable to create collection. error=[{}].".format(created_collection_response.json()['error']))
            raise Exception("Collection not created; status={}".format(created_collection_response.status_code))
        logger.info("Collection-id: {0} created for owner-id: {1}".format(
            created_collection_response.json()["collection"]["collection-id"],
            owner_with_collection_response["owner-id"]))
        self.collections_created.append(created_collection_response.json()['collection'])

        # TODO: wait for collection to exist

        # testing invalid create owner cases
        test_create_owner_obj.test_create_owner_duplicate(
            {"owner-name": owner_name, "email-distribution": email_list,
             "owner-id": owner_response_dict["owner-id"]})
        logger.info('Successfully tested create owner: duplicate owner')

        test_create_owner_obj.test_create_owner_prefix_registered_by_other_owner(
            {"owner-name": "test_owner_{}".format(datetime.now().isoformat()),
             "email-distribution": email_list,
             "collection-prefixes": [collection_prefix],
             "owner-id": owner_response_dict["owner-id"]})
        logger.info('Successfully tested create owner: with  prefix registered by another owner')
        test_create_owner_obj.test_create_owner_invalid_owner_name()
        logger.info('Successfully tested create owner: invalid owner-name')
        test_create_owner_obj.test_create_owner_invalid_email({"owner-name": owner_name})
        logger.info('Successfully tested create owner: invalid email')
        test_create_owner_obj.test_create_owner_empty_email_list({"owner-name": owner_name})
        logger.info('Successfully tested create owner: empty email list')
        test_create_owner_obj.test_create_owner_invalid_content_type()
        logger.info('Successfully tested create owner: invalid content type')
        test_create_owner_obj.test_create_owner_with_invalid_prefix_exceeded_limit_length()
        logger.info('Successfully tested create owner: invalid prefix length')
        test_create_owner_obj.test_create_owner_with_invalid_prefix()
        logger.info('Successfully tested create owner: invalid prefix')

        return owner_response_dict, owner_with_collection_response, created_collection_response.json()['collection']

    def get_owner(self, create_owner_response):
        logger.info("### Running get owner tests ###")
        from Tests.Regression.regression_get_owner import TestGetOwner
        test_get_owner_obj = TestGetOwner()

        # + valid get owner
        get_owner_response_dict = test_get_owner_obj.test_get_owner(create_owner_response)
        logger.info("Successfully got the owner with owner-id: {}".format(get_owner_response_dict["owner-id"]))

        # - Invalid get owner cases
        test_get_owner_obj.test_get_owner_invalid_owner_id({"owner-id": -100})
        logger.info("Successfully tested get owner: invalid owner-id")
        test_get_owner_obj.test_get_owner_invalid_x_api_key()
        logger.info("Successfully tested get owner: invalid x-api-key")
        test_get_owner_obj.test_get_owner_invalid_content_type()
        logger.info("Successfully tested get owner: invalid content type")

        return get_owner_response_dict

    def list_owners(self):
        logger.info("### Running list owners tests ###")
        from Tests.Regression.regression_list_owners import TestListOwners
        test_list_owners_obj = TestListOwners()

        # + valid list owners
        test_list_owners_obj.test_list_owners_all_results()
        logger.info("Successfully tested list owners: all results")
        test_list_owners_obj.test_list_owners_pagination()
        logger.info("Successfully tested list owners: pagination")

        # - Invalid list owners test cases
        test_list_owners_obj.test_list_owners_pagination_invalid({"max-items": 1002})
        logger.info("Successfully tested list owners: invalid max-items")
        test_list_owners_obj.test_list_owners_invalid_x_api_key()
        logger.info("Successfully tested list owners: invalid x-api-key")
        test_list_owners_obj.test_list_owners_invalid_content_type()
        logger.info("Successfully tested list owners: invalid content type")
        test_list_owners_obj.test_list_invalid_owners_pagination_diff_max_items()
        logger.info("Successfully tested list owners: invalid different max items")

    def update_owner(self, owner_data, incorrect_api_key):
        logger.info("### Running update owner tests ###")
        from Tests.Regression.regression_update_owner import TestUpdateOwner
        test_update_owner_obj = TestUpdateOwner()

        # make sure we are using the API key for this owner for these tests
        os.environ['X-API-KEY'] = owner_data['api-key']

        # + valid update owner test cases (need to keep api-key for other tests)
        update_owner_data = \
            {
                "api-key": owner_data["api-key"],
                "owner-name": "updated_owner_name_{}".format(datetime.now().isoformat()),
                "email-distribution": ["updated_owner_{}@lexisnexis.com".format(random.randrange(1, 1000))],
                "owner-id": owner_data["owner-id"]
            }
        if owner_data.get("collection-prefixes"):
            update_owner_data["collection-prefixes"] = owner_data["collection-prefixes"]

        logger.info("Updating owner: {}".format(update_owner_data))
        update_owner_response = test_update_owner_obj.test_update_owner(update_owner_data)

        logger.info("Validating owner-id {}...".format(update_owner_response['owner-id']))
        for i in range(0, self.update_owner_loop_times):
            time.sleep(self.waiting_time)
            get_owner_response = get_owner({"owner-id": update_owner_data["owner-id"]})
            get_owner_response_dict = get_owner_response.json()["owner"]
            if get_owner_response.status_code == 200 and get_owner_response_dict["owner-name"] == \
                    update_owner_data["owner-name"] and get_owner_response_dict["email-distribution"] == \
                    update_owner_data["email-distribution"]:
                break
            if i == self.update_owner_loop_times - 1:
                raise Exception("Owner not updated; status={}".format(get_owner_response.status_code))
        logger.info("Successfully updated owner with id: {}".format(update_owner_response["owner-id"]))

        # - Invalid update owner test cases
        test_update_owner_obj.test_update_owner_invalid_owner_id()
        logger.info("Successfully tested update owner: invalid owner id")
        test_update_owner_obj.test_update_owner_same_data(update_owner_data)
        logger.info("Successfully tested update owner: update with same data")
        test_update_owner_obj.test_update_owner_invalid_email_format(update_owner_data)
        logger.info("Successfully tested update owner: invalid email format")
        test_update_owner_obj.test_update_owner_invalid_x_api_key()
        logger.info("Successfully tested update owner: invalid x-api-key")
        test_update_owner_obj.test_update_owner_incorrect_x_api_key(update_owner_data, incorrect_api_key)
        logger.info("Successfully tested update owner: incorrect x-api-key")
        test_update_owner_obj.test_update_owner_invalid_content_type()
        logger.info("Successfully tested update owner: invalid content type")

        return update_owner_data

    def remove_owner(self, updated_owner, owner_with_collection, collection_response):
        logger.info("### Running remove owner tests ###")
        from Tests.Regression.regression_remove_owner import TestRemoveOwner

        test_remove_owner_obj = TestRemoveOwner()

        # ***Owner without collections (updated_owner)***

        # make sure we are using the API key for this owner for these tests
        os.environ['X-API-KEY'] = updated_owner['api-key']

        # Testing invalid cases before removing the owner successfully
        # - Invalid remove owner test cases
        test_remove_owner_obj.test_remove_owner_invalid_owner_id()
        logger.info("Successfully tested remove owner: invalid owner id")
        test_remove_owner_obj.test_remove_owner_invalid_x_api_key()
        logger.info("Successfully tested remove owner: invalid x-api-key")
        test_remove_owner_obj.test_remove_owner_incorrect_x_api_key(updated_owner, owner_with_collection["api-key"])
        logger.info("Successfully tested update owner: incorrect x-api-key")
        test_remove_owner_obj.test_remove_owner_invalid_content_type()
        logger.info("Successfully tested remove owner: invalid content type")

        # + Valid remove owner test cases
        logger.info("Removing owner: {}".format(updated_owner))
        test_remove_owner_obj.test_remove_owner(updated_owner)

        logger.info("Validating owner-id {}...".format(updated_owner['owner-id']))
        # ***Owner with collections (owner_with_collection)***

        # make sure we are using the API key for owner that has not been removed for these tests
        os.environ['X-API-KEY'] = owner_with_collection['api-key']
        for i in range(0, self.remove_owner_loop_times):
            time.sleep(self.waiting_time)
            get_owner_response = get_owner(updated_owner)
            if get_owner_response.status_code == 404:
                break
            if i == self.remove_owner_loop_times - 1:
                raise Exception("Unable to remove owner; status={}".format(get_owner_response.status_code))
        logger.info("Successfully tested remove owner with id: {}".format(updated_owner["owner-id"]))

        # Testing invalid cases
        # - Invalid remove owner test cases
        test_remove_owner_obj.test_remove_owner_with_collection(owner_with_collection)
        # Wait for remove owner event to get processed; verify owner was NOT removed
        time.sleep(self.waiting_time)
        get_owner_response = get_owner(owner_with_collection)
        if get_owner_response.status_code != 200:
            raise Exception("Owner removed with an active collection; status={}".format(get_owner_response.status_code))
        logger.info("Successfully tested remove owner: invalid remove owner with collection")

        # Testing remove owner after suspending and removing its collection
        # + Valid remove owner test cases
        logger.info("Removing owner id {} after suspending and removing its collection".format(
            owner_with_collection["owner-id"]))
        logger.info("Suspending owner id {}'s collection".format(owner_with_collection["owner-id"]))
        resp = suspend_collection(collection_response)

        logger.info("Checking that owner id {}'s collection was suspended".format(owner_with_collection["owner-id"]))
        for i in range(0, self.collection_loop_times):
            time.sleep(self.waiting_time)
            response = get_collection(collection_response)
            if response.status_code == 200 \
                    and response.json()['collection']['collection-state'] == "Suspended":
                break
            if i == self.collection_loop_times - 1:
                raise Exception("Collection id {} was not suspended; status={}"
                                .format(collection_response["collection-id"], response.status_code))

        logger.info("Terminating owner id {}'s collection".format(owner_with_collection["owner-id"]))
        remove_collection(collection_response)

        logger.info("Checking that owner id {}'s collection was terminated".format(owner_with_collection["owner-id"]))
        for i in range(0, self.collection_loop_times):
            time.sleep(self.waiting_time)
            response = get_collection(collection_response)
            if response.status_code == 200 \
                    and response.json()['collection']['collection-state'] == "Terminated":
                break
            if i == self.collection_loop_times - 1:
                raise Exception("Collection id {} was not terminated; status={}"
                                .format(collection_response["collection-id"], response.status_code))

        logger.info("Removing owner id {}".format(owner_with_collection["owner-id"]))
        test_remove_owner_obj.test_remove_owner(owner_with_collection)
        logger.info("Checking that owner id {} was removed".format(owner_with_collection["owner-id"]))
        for i in range(0, self.remove_owner_loop_times):
            time.sleep(self.waiting_time)
            get_owner_response = get_owner(owner_with_collection)
            # Requests without an authorized API key supplied in the x-api-key header will result in 403: Forbidden
            if get_owner_response.status_code == 403 and get_owner_response.reason == 'Forbidden':
                break
            if i == self.remove_owner_loop_times - 1:
                raise Exception("Unable to remove owner {}; status={}"
                                .format(owner_with_collection["owner-id"], get_owner_response.status_code))
        logger.info("Successfully tested remove owner after suspending and removing its collection id: {}"
                    .format(owner_with_collection["owner-id"]))


if __name__ == '__main__':
    unittest.main()
