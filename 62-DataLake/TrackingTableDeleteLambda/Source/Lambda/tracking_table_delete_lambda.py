import logging
import os

import orjson
from lng_aws_clients import session
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.collection_blocker_table import CollectionBlockerTable
from lng_datalake_dal.tracking_table import TrackingTable

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


@session_decorator.lng_aws_session()
@error_handler.handle
def lambda_handler(event, context) -> str:
    """
    Point of entry.
    :param event: SNS event of the tracking or collection blocker to delete
    :param context: Lambda Context
    :return: None
    """
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))
    delete_item_from_tracking_table(event)
    return event_handler_status.SUCCESS


def delete_item_from_tracking_table(event: dict) -> None:
    """
    1. Extracts event-id
    2. Retrieves item by tracking-id
    3. Deletes item from the tracking or collection blocker table
    :param event: SQS message
    """
    session.set_session()
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.TRACKING_REMOVE,
                                           event_names.TRACKING_COLLECTION_BLOCKER_REMOVE):
        raise TerminalErrorException(
            "Failed to extract event: {0} or event-name doesnt match {1} or {2}".format(event,
                                                                                        event_names.TRACKING_REMOVE,
                                                                                        event_names.TRACKING_COLLECTION_BLOCKER_REMOVE))

    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))
    if message['event-name'] == event_names.TRACKING_REMOVE:
        delete_tracking_item(message)
        logger.info("Successfully removed tracking record: {0}".format(message))
    else:
        delete_collection_blocker(message)
        logger.info("Successfully removed collection blocker record: {0}".format(message))


def delete_tracking_item(item: dict) -> None:
    """
    Deletes item of the tracking table.
    :param item: Item to be deleted
    :return: None
    """
    try:
        TrackingTable().delete_item({"tracking-id": item["tracking-id"], "request-time": item["request-time"]})
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Cannot delete item from the Tracking table.\nItem = {0}\nException = {1}"
                                     .format(item, e))


def delete_collection_blocker(item: dict) -> None:
    """
    Deletes item of the collection blocker table.
    :param item: Item to be deleted
    :return: None
    """
    try:
        collection_id = item["collection-id"]
        request_time = item["request-time"]
        CollectionBlockerTable().delete_item({"collection-id": collection_id, "request-time": request_time})
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Cannot delete item from the CollectionBlocker table.\nItem = {0}\nException = {1}"
                                     .format(item, e))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL TEST RUN - BEGIN]")

    ASSET_GROUP = 'feature-jwb'

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{0}-DataLake-TrackingTable'.format(ASSET_GROUP)
    os.environ['COLLECTION_BLOCKER_DYNAMODB_TABLE'] = '{0}-DataLake-CollectionBlockerTable'.format(ASSET_GROUP)

    session.set_session()

    tracker_msg = {"event-id": "bMLvkCFXcKwvGCLH", "event-name": "Tracking::Remove", "seen-count": 1,
                   "tracking-id": "5678", "request-time": "2019-04-30T13:30:00.000Z"}
    collection_blocker_msg = {"collection-id": "abc", "request-time": "2019-04-30T13:30:00.000Z",
                              "event-id": "bMLvkCFXcKwvGCLH", "event-name": "Tracking::Remove", "seen-count": 1}
    my_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(tracker_msg)
            }
        }]
    }
    lambda_handler(my_event, MockLambdaContext)
    logger.debug("[LOCAL TEST RUN - END]")
