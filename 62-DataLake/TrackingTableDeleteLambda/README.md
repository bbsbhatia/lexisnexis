# Introduction 
TrackingTableDeleteLambda is a periodically triggered lambda 
that retries failed deletes of the tracking 
table.
Trigger - CloudWatch events (fires every 5 sec)

Lambda polls of a retry queue specific to the lambda which contains
all the events that failed to get deleted of the tracking table.

Once the lambda deletes the failed event successfully of the
tracking table, the event is deleted of the retry queue as well.