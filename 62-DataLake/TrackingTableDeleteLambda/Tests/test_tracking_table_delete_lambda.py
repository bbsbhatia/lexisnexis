import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_constants import event_handler_status
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from tracking_table_delete_lambda import delete_tracking_item, \
    delete_item_from_tracking_table, delete_collection_blocker, lambda_handler

__author__ = "Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__)


class TrackingTableDeleteLambdaTest(TestCase):
    def setUp(self):  # NOSONAR
        self.session = patch("lng_aws_clients.session.set_session").start()
        self.valid_item = io_utils.load_data_json("valid_item.json")
        self.invalid_item = io_utils.load_data_json("invalid_item.json")
        self.valid_event = io_utils.load_data_json("valid_event.json")
        self.valid_message = io_utils.load_data_json("valid_message.json")
        self.valid_collection_blocker_item = io_utils.load_data_json("valid_collection_blocker_item.json")
        self.valid_collection_blocker_delete_event = io_utils.load_data_json(
            "valid_collection_blocker_delete_event.json")
        self.valid_event_with_tracking_id = io_utils.load_data_json("valid_event_with_tracking_id.json")

    # +lambda_handler - Valid tracking item
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    @patch("lng_datalake_dal.tracking_table.TrackingTable.get_item")
    def test_lambda_handler(self, getitem_mock, delete_mock):
        valid_event = io_utils.load_data_json("valid_event.json")
        getitem_mock.return_value = {"event-id": "LcshWxPdprEyyWZL", "request-time": "2018-10-17T16:18:55.475Z",
                                    "tracking-id": "48cf81db-762b-11e8-becb-7f9fb9037eee_object"}
        delete_mock.return_value = None
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)

    # +lambda_handler - Valid collection blocker
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_lambda_handler2(self, delete_mock):
        valid_event = io_utils.load_data_json("valid_collection_blocker_event.json")
        delete_mock.return_value = None
        self.assertEqual(lambda_handler(valid_event, MockLambdaContext), event_handler_status.SUCCESS)

    # +delete_item_from_tracking_table using tracking-id - Valid
    @patch("tracking_table_delete_lambda.delete_tracking_item")
    def test_delete_item_from_tracking_table_success(self, delete_tracking_item_mock):
        delete_tracking_item_mock.return_value = None
        self.assertIsNone(delete_item_from_tracking_table(self.valid_event_with_tracking_id))

    # +delete_item_from_collection_blocker - Valid
    @patch("tracking_table_delete_lambda.delete_collection_blocker")
    def test_delete_item_from_collection_blocker_table_success(self, delete_collection_blocker_mock):
        delete_collection_blocker_mock.return_value = None
        self.assertIsNone(delete_item_from_tracking_table(self.valid_collection_blocker_delete_event))

    # -delete_item_from_tracking_table - event error
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    def test_delete_item_from_tracking_table_fail(self, mock_validate):
        mock_validate.return_value = False
        event = self.valid_event
        with self.assertRaisesRegex(TerminalErrorException, "Failed to extract event"):
            delete_item_from_tracking_table(event)


    # +delete_tracking_item - Valid
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_success(self, delete_item_mock):
        delete_item_mock.return_value = None
        self.assertIsNone(delete_tracking_item(self.valid_item))

    # -delete_tracking_item - Invalid Item
    def test_delete_tracking_item_key_error(self):
        with self.assertRaisesRegex(TerminalErrorException, "Cannot delete item from the Tracking table"):
            delete_tracking_item(self.invalid_item)

    # -delete_tracking_item - Client Error
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_client_error(self, delete_item_mock):
        delete_item_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                    "Error": {"Code": "mock error code",
                                                              "Message": "mock error message"}},
                                                   "client-error-mock")
        with self.assertRaises(ClientError):
            delete_tracking_item(self.valid_item)

    # -delete_tracking_item - Exception
    @patch("lng_datalake_dal.tracking_table.TrackingTable.delete_item")
    def test_delete_tracking_item_generic_exception(self, delete_item_mock):
        delete_item_mock.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Cannot delete item from the Tracking table"):
            delete_tracking_item(self.valid_item)

    # +delete_collection_blocker - Valid
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_delete_collection_blocker_success(self, delete_item_mock):
        delete_item_mock.return_value = None
        self.assertIsNone(delete_collection_blocker(self.valid_collection_blocker_item))

    # -delete_collection_blocker - Client Error
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_delete_collection_blocker_client_error(self, delete_item_mock):
        delete_item_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                    "Error": {"Code": "mock error code",
                                                              "Message": "mock error message"}},
                                                   "client-error-mock")
        with self.assertRaises(ClientError):
            delete_collection_blocker(self.valid_collection_blocker_item)

    # -delete_collection_blocker - Exception
    @patch("lng_datalake_dal.collection_blocker_table.CollectionBlockerTable.delete_item")
    def test_delete_collection_blocker_generic_exception(self, delete_item_mock):
        delete_item_mock.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Cannot delete item from the CollectionBlocker table"):
            delete_collection_blocker(self.valid_collection_blocker_item)

    def tearDown(self):  # NOSONAR
        self.session.stop()


if __name__ == '__main__':
    unittest.main()
