import copy
import logging
import os
import unittest
from datetime import datetime

from lng_datalake_client.Admin.remove_owner import remove_owner
from lng_datalake_client.Catalog.describe_catalogs import describe_catalogs
from lng_datalake_client.Catalog.get_catalog import get_catalog
from lng_datalake_client.Collection.get_collection import get_collection
from lng_datalake_client.Collection.remove_collection import remove_collection
from lng_datalake_client.Collection.update_collection import suspend_collection
from lng_datalake_client.Utils import setup_utils, teardown_utils
from time import sleep

__author__ = "Jose Molinet, John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


class TestCatalogOrchestration(unittest.TestCase):
    get_owner_loop_times = int(os.getenv('GET_OWNER_LOOP_TIMES', 5))
    get_catalog_loop_times = int(os.getenv('GET_CATALOG_LOOP_TIMES', 5))
    get_collection_loop_times = int(os.getenv('GET_COLLECTION_LOOP_TIMES', 5))
    collection_loop_times = int(os.getenv('COLLECTION_LOOP_TIMES', 10))
    default_asset_id = int(os.getenv('ASSET_ID', 62))
    wait_time = int(os.getenv('WAIT_TIME', 5))
    collections = []

    @classmethod
    def setUpClass(cls):  # NOSONAR
        logger.info("### Setting up tests for {} ###".format(cls.__name__))

        # This owner will only be used for negative tests for API key authorization
        owner_body_input = \
            {
                "owner-name": "regression-catalog.1.{}".format(datetime.now().isoformat()),
                "email-distribution": ["regression-catalog@lexisnexis.com"]
            }
        logger.info('Creating owner: {}'.format(owner_body_input))
        owner_response = setup_utils.setup_create_owner(owner_body_input, cls.get_owner_loop_times, cls.wait_time)
        cls.owner_id2 = owner_response['owner']['owner-id']
        cls.api_key2 = owner_response['owner']['api-key']

        logger.info("Successfully created owner-id {}.".format(cls.owner_id2))

        # This is the primary owner that will be used for tests
        # - must be created last so API key is associated with Helper functions
        owner_body_input = \
            {
                "owner-name": "regression-catalog.2.{}".format(datetime.now().isoformat()),
                "email-distribution": ["regression-catalog@lexisnexis.com"]
            }
        logger.info('Creating owner: {}'.format(owner_body_input))
        owner_response = setup_utils.setup_create_owner(owner_body_input, cls.get_owner_loop_times, cls.wait_time)
        cls.owner_id = owner_response['owner']['owner-id']
        logger.info("Successfully created owner-id {}.".format(cls.owner_id))

        # Set Owner in environment variables
        os.environ['OWNER-ID'] = str(owner_response['owner']['owner-id'])
        os.environ['X-API-KEY'] = owner_response['owner']['api-key']

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        logger.info("### Cleaning up tests for {} ###".format(cls.__name__))

        logger.info("Checking if collections were removed in tests")
        for c in cls.collections:
            collection_state = get_collection(c).json().get('collection', {}).get('collection-state', '')
            if collection_state == 'Terminated':
                cls.collections.remove(c)

        # suspend all collections
        logger.info("Suspending collections")
        for c in cls.collections:
            suspend_collection(c)

        logger.info("Checking that all collections were suspended")
        expected_state = 'Suspended'
        for c in cls.collections:
            teardown_utils.validate_collection_state(
                expected_state, c, cls.__class__, cls.collection_loop_times, cls.wait_time)

        # remove all collections
        logger.info("Terminating collections")
        for c in cls.collections:
            remove_collection(c)

        logger.info("Checking that all collections were removed")
        expected_state = 'Terminated'
        for c in cls.collections:
            teardown_utils.validate_collection_state(
                expected_state, c, cls.__class__, cls.collection_loop_times, cls.wait_time)

        logger.info("Deleting owner-id {}...".format(cls.owner_id))
        owner_request = {
            "owner-id": cls.owner_id
        }
        owner_response = remove_owner(owner_request)
        if owner_response.status_code != 202:
            logger.error("Unable to delete owner-id {}. error=[{}].".format(cls.owner_id, owner_response.text))

        logger.info("Deleting owner-id {}...".format(cls.owner_id2))
        owner_request = {
            "headers": {
                "Content-Type": "application/json",
                "x-api-key": cls.api_key2
            },
            "owner-id": cls.owner_id2
        }
        owner_response = remove_owner(owner_request)
        if owner_response.status_code != 202:
            logger.error("Unable to delete owner-id {}. error=[{}].".format(cls.owner_id2, owner_response.text))

    def test_orchestration(self) -> None:
        """
        Steps performed for Catalog Regression Test:
        1- Create two valid catalogs, one of them with specified catalog-id and description
        2- Get both catalogs
        3- List all catalogs, and filtered by owner
        4- Create collections to be assigned to the catalogs
        5- Update catalog by adding/removing collections and replacing descriptions
        6- Remove catalogs and verify that the collections are no longer linked to them
        """
        catalog, catalog_with_optionals_1, catalog_with_optionals_2, catalog_with_optionals_3 = self.create_catalog()
        self.get_catalogs(catalog, catalog_with_optionals_1)
        self.list_catalogs([catalog, catalog_with_optionals_1, catalog_with_optionals_2, catalog_with_optionals_3])
        self._create_collections()
        catalog, catalog_with_details = self.update_catalog(catalog, self.collections)
        optionals_catalog_with_details_1 = copy.deepcopy(catalog_with_optionals_1)
        optionals_catalog_with_details_1['collection-ids'] = []
        optionals_catalog_with_details_1['truncated'] = False
        optionals_catalog_with_details_2 = copy.deepcopy(catalog_with_optionals_2)
        optionals_catalog_with_details_2['collection-ids'] = []
        optionals_catalog_with_details_2['truncated'] = False
        optionals_catalog_with_details_3 = copy.deepcopy(catalog_with_optionals_3)
        optionals_catalog_with_details_3['collection-ids'] = []
        optionals_catalog_with_details_3['truncated'] = False
        self.describe_catalogs([catalog_with_details, optionals_catalog_with_details_1, optionals_catalog_with_details_2,
                                optionals_catalog_with_details_3])
        self.remove_catalogs([catalog, catalog_with_optionals_1, catalog_with_optionals_2, catalog_with_optionals_3],
                             self.collections)

    def create_catalog(self) -> tuple:
        logger.info("### Running create catalog tests ###")
        # must import other test cases inside function so the unittest framework doesn't find them and try to run them
        from Tests.Regression.regression_create_catalog import TestCreateCatalog

        test_create_catalog = TestCreateCatalog()

        # + Create valid catalog to use in future tests.
        logger.info('Creating a valid catalog with no optional attributes.')
        catalog = test_create_catalog.test_create_catalog_valid_input_description_query()
        logger.info('Response from creating the catalog: {}'.format(catalog))

        # + Create a valid catalog with optional attributes - description as query param
        input_dict = \
            {
                'catalog-id': 'regression-catalog1-{}'.format(datetime.timestamp(datetime.now()))[:-7],
                'description': "Catalog for regression tests"
            }
        logger.info('Creating a valid catalog with optional attributes.')
        catalog_with_optionals_1 = test_create_catalog.test_create_catalog_valid_input_description_body_query(
            input_dict)
        logger.info(
            'Response from creating the catalog with the optional attributes with description as query param: {}'.format(
                catalog_with_optionals_1))

        # + Create a valid catalog with optional attributes - description in body
        input_dict = \
            {
                'catalog-id': 'regression-catalog2-{}'.format(datetime.timestamp(datetime.now()))[:-7],
                'description': "Catalog for regression tests"
            }
        logger.info('Creating a valid catalog with optional attributes.')
        catalog_with_optionals_2 = test_create_catalog.test_create_catalog_valid_input_description_body(input_dict)
        logger.info(
            'Response from creating the catalog with the optional attributes with description in body: {}'.format(
                catalog_with_optionals_2))

        # + Create a valid catalog with optional attributes - description as query param and in body
        input_dict = \
            {
                'catalog-id': 'regression-catalog3-{}'.format(datetime.timestamp(datetime.now()))[:-7],
                'description': "Catalog for regression tests"
            }
        logger.info('Creating a valid catalog with optional attributes.')
        catalog_with_optionals_3 = test_create_catalog.test_create_catalog_valid_input_description_body_query(
            input_dict)
        logger.info(
            'Response from creating the catalog with the optional attributes with description as query param '
            'and in body: {}'.format(catalog_with_optionals_3))

        for catalog_dict in [catalog, catalog_with_optionals_1, catalog_with_optionals_2, catalog_with_optionals_3]:
            for i in range(self.get_catalog_loop_times):
                sleep(self.wait_time)
                get_catalog_response = get_catalog(catalog_dict)
                if get_catalog_response.status_code == 200:
                    break
                if i == self.get_catalog_loop_times - 1:
                    raise Exception("Catalog not created; status={}".format(get_catalog_response.status_code))
            logger.info("Successfully created catalog {}".format(catalog_dict["catalog-id"]))

        # - Invalid create catalog tests
        logger.info('Moving to invalid test cases for create catalog...')
        test_create_catalog.test_create_catalog_id_invalid_length()
        logger.info("Successfully tested create catalog: invalid catalog-id length")
        test_create_catalog.test_create_catalog_id_invalid_character()
        logger.info("Successfully tested create catalog: invalid character in catalog-id")
        test_create_catalog.test_create_catalog_id_invalid_only_digits()
        logger.info("Successfully tested create catalog: invalid catalog-id contains only numbers")
        test_create_catalog.test_create_catalog_invalid_x_api_key()
        logger.info("Successfully tested create catalog: invalid x-api-key")
        test_create_catalog.test_create_catalog_unsupported_media()
        logger.info("Successfully tested create catalog: invalid content type")
        test_create_catalog.test_create_catalog_duplicated_id(input_dict)
        logger.info("Successfully tested create catalog: duplicated catalog-id")

        return catalog, catalog_with_optionals_1, catalog_with_optionals_2, catalog_with_optionals_3

    @staticmethod
    def get_catalogs(catalog: dict, catalog_with_optionals: dict) -> None:
        logger.info("### Running get catalog tests ###")
        from Tests.Regression.regression_get_catalog import TestGetCatalog

        # + Valid get catalog tests
        test_get_catalog = TestGetCatalog()

        test_get_catalog.test_get_catalog(catalog)
        test_get_catalog.test_get_catalog(catalog_with_optionals)
        logger.info('Successfully got the created catalog {} and {}'.format(catalog['catalog-id'],
                                                                            catalog_with_optionals['catalog-id']))

        # - Invalid get catalog tests
        logger.info('Moving to invalid test cases for get catalog...')
        test_get_catalog.test_get_catalog_invalid_catalog_id()
        logger.info("Successfully tested get catalog: invalid catalog id")
        test_get_catalog.test_get_catalog_invalid_x_api_key()
        logger.info("Successfully tested get catalog: invalid x-api-key")
        test_get_catalog.test_get_catalog_invalid_content_type()
        logger.info("Successfully tested get catalog: invalid content type")

    def list_catalogs(self, catalogs: list) -> None:
        logger.info("### Running list catalogs tests ###")
        from Tests.Regression.regression_list_catalogs import TestListCatalogs
        test_list_catalogs = TestListCatalogs()

        # + Valid list catalogs tests
        test_list_catalogs.test_list_catalogs_all_results()
        logger.info("Successfully tested list catalogs: all results")
        test_list_catalogs.test_list_catalogs_by_owner_id(catalogs, self.owner_id)
        logger.info("Successfully tested list catalogs: all results by owner id")
        test_list_catalogs.test_list_catalogs_pagination()
        logger.info("Successfully tested list catalogs: results by owner id and pagination")
        test_list_catalogs.test_list_catalogs_invalid_owner_id()
        logger.info("Successfully tested list catalogs: invalid owner id")

        # - Invalid list catalog tests
        logger.info('Moving to invalid test cases for list catalog...')
        test_list_catalogs.test_list_catalogs_changing_max_items()
        logger.info("Successfully tested list catalogs: changing max items")
        test_list_catalogs.test_list_catalogs_invalid_x_api_key()
        logger.info("Successfully tested list catalogs: invalid x-api-key")
        test_list_catalogs.test_list_catalogs_invalid_content_type()
        logger.info("Successfully tested list catalogs: invalid content type")

    def _create_collections(self) -> None:
        from lng_datalake_client.Collection.create_collection import create_collection

        # create collections to add into catalog
        number_collections = 3
        logger.info("Creating {} collections to be used in the update catalog tests...".format(number_collections))

        for i in range(number_collections):
            input_dict = {
                'collection-id': 'regression-catalog-{}-{}'.format(i, datetime.timestamp(datetime.now()))[:-7],
                'body': {
                    'asset-id': self.default_asset_id
                }
            }
            self.collections.append(create_collection(input_dict).json()['collection'])

        for collection in self.collections:
            for i in range(self.get_collection_loop_times):
                sleep(self.wait_time)
                get_collection_response = get_collection(collection)
                if get_collection_response.status_code == 200:
                    break
                if i == self.get_collection_loop_times - 1:
                    raise Exception("Collection not created; status={}".format(get_collection_response.status_code))
            logger.info("Successfully created collection {}".format(collection["collection-id"]))

    def update_catalog(self, catalog: dict, collections: list) -> tuple:
        logger.info("### Running update catalog tests ###")
        from Tests.Regression.regression_update_catalog import TestUpdateCatalog
        from lng_datalake_client.Collection.update_collection import suspend_collection
        from lng_datalake_client.Collection.remove_collection import remove_collection

        collection_ids = [c['collection-id'] for c in collections]

        # + Valid update catalog tests
        # Operations are applied in the same order they are specified in the patch document.
        # Removing collections not contained in the catalog will not change the catalog.
        logger.info("Updating catalog by adding collections and modifying its description...")
        input_dict = {
            "catalog-id": catalog['catalog-id'],
            "patch-operations": [
                {
                    "op": "remove",
                    "path": "/collection-ids",
                    "value": collection_ids
                },
                {
                    "op": "add",
                    "path": "/collection-ids",
                    "value": collection_ids
                },
                {
                    "op": "replace",
                    "path": "/description",
                    "value": "Modified catalog description"
                }
            ]
        }
        test_update_catalog = TestUpdateCatalog()
        updated_catalog = test_update_catalog.test_update_catalog_success(input_dict)

        # validate updated catalog contains all the collections and the new description
        self._validate_updated_catalog(updated_catalog, collection_ids)

        # for each collection assigned to catalog validate that get_collection returns the catalog id
        # TODO: Enable this once we have describe collections
        # self._validate_catalog_collections_mapping(updated_catalog['catalog-id'], collections)

        # Adding collections that already exist in the catalog will not change the catalog.
        logger.info("Updating catalog by removing one of its collections...")
        input_dict = {
            "catalog-id": updated_catalog['catalog-id'],
            "patch-operations": [
                {
                    "op": "add",
                    "path": "/collection-ids",
                    "value": collection_ids
                },
                {
                    "op": "remove",
                    "path": "/collection-ids",
                    "value": collection_ids[len(collection_ids) - 1:]
                }
            ]
        }
        updated_catalog = test_update_catalog.test_update_catalog_success(input_dict)

        # validate updated catalog contains all the collections except the last one
        self._validate_updated_catalog(updated_catalog, collection_ids[:-1])

        # for each collection assigned to catalog validate that get_collection returns the catalog id
        # TODO: Enable this once we have describe collections
        # self._validate_catalog_collections_mapping(updated_catalog['catalog-id'], collections[:-1])

        # - Invalid update catalog tests
        logger.info('Moving to invalid test cases for update catalog...')

        # Suspend and remove a collection to test update catalog with an invalid collection (state = Terminated)
        logger.info("Suspending collection {}...".format(collection_ids[-1]))
        suspend_collection(collections[-1])

        test_update_catalog.test_update_catalog_invalid_catalog_id()
        logger.info("Successfully tested update catalog: invalid catalog id")
        test_update_catalog.test_update_catalog_invalid_description_length(updated_catalog)
        logger.info("Successfully tested update catalog: description length exceed 256 characters")
        test_update_catalog.test_update_catalog_incorrect_x_api_key(updated_catalog, self.api_key2)
        logger.info("Successfully tested update catalog: x-api-key doesn't match the catalog owner's api-key")
        test_update_catalog.test_update_catalog_invalid_content_type()
        logger.info("Successfully tested update catalog: invalid content type")
        test_update_catalog.test_update_catalog_invalid_number_collection_ids(updated_catalog)
        logger.info("Successfully tested update catalog: number of collection ids exceed the 1000 maximum")
        test_update_catalog.test_update_catalog_invalid_x_api_key()
        logger.info("Successfully tested update catalog: invalid x-api-key")

        self._validate_collection_state(collections[-1], "Suspended")
        logger.info("Removing collection {}...".format(collection_ids[-1]))
        remove_collection(collections[-1])
        self._validate_collection_state(collections[-1], "Terminated")

        # collections are invalid to add to a catalog if they don't exist or their state is Terminated
        invalid_collection_ids = ['non-existent-collection-ABC-987', collection_ids[-1]]

        input_dict = {
            'invalid_collection-ids': invalid_collection_ids,
            "catalog-id": updated_catalog['catalog-id'],
            "patch-operations": [
                {
                    "op": "add",
                    "path": "/collection-ids",
                    "value": invalid_collection_ids
                }
            ]
        }
        test_update_catalog.test_update_catalog_invalid_collection_ids(input_dict)
        logger.info("Successfully tested update catalog: invalid collection ids")

        catalog_with_details = copy.deepcopy(updated_catalog)

        catalog_with_details['collection-ids'] = sorted(collection_ids[0:2])
        catalog_with_details['truncated'] = False

        return updated_catalog, catalog_with_details

    def describe_catalogs(self, catalogs: list) -> None:
        logger.info("### Running describe catalogs tests ###")
        from Tests.Regression.regression_describe_catalogs import TestDescribeCatalogs
        test_describe_catalogs = TestDescribeCatalogs()

        # + Valid describe catalogs tests
        test_describe_catalogs.test_describe_catalogs_all_results()
        logger.info("Successfully tested describe catalogs: all results")
        test_describe_catalogs.test_describe_catalogs_by_owner_id(catalogs, self.owner_id)
        logger.info("Successfully tested describe catalogs: all results by owner id")
        test_describe_catalogs.test_describe_catalogs_by_catalog_id([catalogs[0]], catalogs[0]['catalog-id'])
        logger.info("Successfully tested describe catalogs: describe by catalog-id")
        test_describe_catalogs.test_describe_catalogs_pagination()
        logger.info("Successfully tested describe catalogs: results by owner id and pagination")
        test_describe_catalogs.test_describe_catalogs_invalid_owner_id()
        logger.info("Successfully tested describe catalogs: invalid owner id")

        # - Invalid describe catalog tests
        logger.info('Moving to invalid test cases for describe catalog...')
        test_describe_catalogs.test_describe_catalogs_changing_max_items()
        logger.info("Successfully tested describe catalogs: changing max items")
        test_describe_catalogs.test_describe_catalogs_invalid_x_api_key()
        logger.info("Successfully tested describe catalogs: invalid x-api-key")
        test_describe_catalogs.test_describe_catalogs_invalid_content_type()
        logger.info("Successfully tested describe catalogs: invalid content type")

    def _validate_updated_catalog(self, catalog: dict, collection_ids: list) -> None:
        for i in range(self.get_catalog_loop_times):
            sleep(self.wait_time)
            response = describe_catalogs(catalog)
            if response.status_code == 200:
                catalog_response = response.json()['catalogs'][0]
                if catalog_response.get('description') == catalog['description'] and sorted(
                        catalog_response.get('collection-ids', [])) == sorted(collection_ids):
                    break
            if i == self.get_catalog_loop_times - 1:
                raise Exception("Catalog not updated; status={}".format(response.status_code))
        logger.info("Successfully updated catalog {}".format(catalog["catalog-id"]))

    def _validate_collection_state(self, collection: dict, state: str) -> None:
        for i in range(self.get_collection_loop_times):
            sleep(self.wait_time)
            response = get_collection(collection)
            if response.status_code == 200 and response.json()['collection']['collection-state'] == state:
                break
            if i == self.get_collection_loop_times - 1:
                raise Exception("Collection state was not updated; status={}".format(response.status_code))
        logger.info("Successfully updated the state of collection {} to {}".format(collection["collection-id"], state))

    def _validate_catalog_collections_mapping(self, catalog_id: str, collections: list) -> None:
        for collection in collections:
            for i in range(self.get_collection_loop_times):
                sleep(self.wait_time)
                response = get_collection(collection)
                if response.status_code == 200 and \
                        response.json()['collection'].get('catalog-ids', []) == [catalog_id]:
                    break
                if i == self.get_collection_loop_times - 1:
                    raise Exception("Collection not added to catalog; status={}".format(response.status_code))
            logger.info(
                "Successfully added collection {} to catalog {}".format(collection["collection-id"], catalog_id))

    def remove_catalogs(self, catalogs: list, collections: list) -> None:
        logger.info("### Running remove catalog tests ###")
        from Tests.Regression.regression_remove_catalog import TestRemoveCatalog
        test_remove_catalog = TestRemoveCatalog()

        # - Invalid test before removing the catalogs
        test_remove_catalog.test_remove_catalog_incorrect_x_api_key(catalogs[0], self.api_key2)
        logger.info("Successfully tested remove catalog: x-api-key doesn't match the catalog owner's api-key")

        # + Valid remove catalog test
        for catalog in catalogs:
            logger.info("Removing catalog {}".format(catalog['catalog-id']))
            test_remove_catalog.test_remove_catalog(catalog)

        self._validate_removed_catalogs(catalogs)
        # TODO: Enable this once we have describe collections
        # self._validate_catalog_collections_unlinked(collections)

        # - Invalid remove catalog tests
        logger.info('Moving to invalid test cases for remove catalog...')
        test_remove_catalog.test_remove_catalog_invalid_catalog_id()
        logger.info("Successfully tested remove catalog: invalid catalog id")
        test_remove_catalog.test_remove_catalog_invalid_x_api_key()
        logger.info("Successfully tested remove catalog: invalid x-api-key")
        test_remove_catalog.test_remove_catalog_invalid_content_type()
        logger.info("Successfully tested remove catalog: invalid content type")

    def _validate_removed_catalogs(self, catalogs: list) -> None:
        for catalog in catalogs:
            for i in range(self.get_catalog_loop_times):
                sleep(self.wait_time)
                response = get_catalog(catalog)
                if response.status_code == 404 and response.json()['error']['corrective-action'] == \
                        "Catalog ID does not exist":
                    break
                if i == self.get_catalog_loop_times - 1:
                    raise Exception("Catalog not removed; status={}".format(response.status_code))
            logger.info("Successfully removed catalog {}".format(catalog["catalog-id"]))

    def _validate_catalog_collections_unlinked(self, collections):
        for collection in collections:
            for i in range(self.get_collection_loop_times):
                sleep(self.wait_time)
                response = get_collection(collection)
                if response.status_code == 200 and 'catalog-ids' not in response.json()['collection']:
                    break
                if i == self.get_collection_loop_times - 1:
                    raise Exception("Collection is mapped to removed catalog; status={}".format(response.status_code))
            logger.info("Successfully unlinked collection {} to removed catalog".format(collection["collection-id"]))


if __name__ == '__main__':
    unittest.main()
