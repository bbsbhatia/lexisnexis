@startuml
skinparam BoxPadding 10
skinparam SequenceTitleFontSize 25
skinparam SequenceGroupHeaderFontSize 15
skinparam SequenceGroupFontSize 15
skinparam SequenceGroupBodyBackgroundColor transparent
skinparam Padding 4

title: Create Catalog Command

participant Cloudfront
box API Gateway #goldenrod
    participant "DataLakeApi-Catalog" as Api
end box
box Lambda #darkorange
    participant "CreateCatalogCommand" as Command
end box
box DynamoDB #royalblue
    participant OwnerTable
    participant CatalogTable
    participant CounterTable
end box

Cloudfront++

    Cloudfront -> Api ++: Request POST /catalogs/{catalogId}
        note right Cloudfront
            Required parameters:
                x-api-key (header)
                catalogId (path)
                CatalogRequest (body)
        endnote
        Api -> Api ++: validate Request: \n query params, headers
            Api --
        Api -> Api ++: Request integration
            Api --

        Api -> Command ++: invoke
            Command -> Command ++: [Commands.SessionDecorator.puml] Decorator
                Command --
            Command -> Command ++: [Commands.CommandWrapper.puml] Decorator
                Command --

            Command -> Command ++: Authenticate Request
                Command -> OwnerTable ++: query Owner by api-key-id
                    Command <<-- OwnerTable: Owner
                OwnerTable --
                opt if api-key-id is not valid for any Owner
                    Api <<-- Command: NotAuthorizedError (403)
                end
                Command --


            alt if catalog_id
                Command -> Command ++: validate catalog_id
                    note right
                        id limited to 40 characters.
                        Can only contain "^[A-Za-z0-9-]*$"
                        Cannot contain only digits.
                        Cannot already exist
                    endnote
                    opt if validation failure
                        Api <<-- Command: raise InvalidRequestPropertyValue (400)
                    end

                        Command -> CatalogTable ++: get_item() Catalog by catalog_id
                        Command <<-- CatalogTable: Catalog
                        CatalogTable --
                    opt if catalog exists
                        Api <<-- Command: raise CatalogAlreadyExists(409)
                    end
                Command --
            else
                Command -> CounterTable ++: update_counter()
                    Command <<-- CounterTable: catalog_id
                    CounterTable --
            end

            Command -> Command ++: Generate JSON Response Dict
                Command --
            Command -> Command ++: Generate Catalog::Create Event Store Dict
                Command --
            Command -> Command ++: [Commands.CommandWrapper.puml] Decorator
                Command --


            Api <<-- Command: JSON Response (Success)
            Command --

            Api -> Api ++: Response Integration
                Api --
            note right Api
                Response Integration Status Codes
                    Success: 202 (Accepted)
                    Exception:
                        Invalid 400
                        NotAuthorizedError 403
                        CatalogAlreadyExists 409
                        InternalError 500
            endnote
        Cloudfront <<-- Api: Response <Status Code>

    Api --

Cloudfront--
@enduml