
# Deprecate Post Catalogs


Date: 2019-07-29

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Our POST catalogs api is used very little compared to POST catalogs/{catalogId}.

If we deprecate this we will simplify our api, improve our swagger, and reduce the amount of code we have to maintain.

### Usage on prod
0 total executions in last 2 weeks
0 total executions in last 4 weeks
2 total executions in last 4 months

1 successful execution in the last 4 months (excluding executions by the DataLake team)

### Usage on cert
2 total executions in last 2 weeks
6 total executions in last 4 weeks
176 total executions in last 4 months

136 successful executions in 4 months (excluding executions by the DataLake team)
122/136 by a single owner
14 unique owners



## Decision
We have decided to deprecate POST Catalogs.

## Consequences
* POST catalogs gets removed from our swagger client v1
* Some time in the future, POST catalogs gets removed from the api
* Users will have to specify a catalog-id when creating a catalog
* Team wormhole will have to notify Users who have used this feature in the last 4 months that it has been deprecated

## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/TeamRoom.mvc/Show/26489)