{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "This stack creates a lambda function for the DataLake",
  "Outputs": {
    "FunctionName": {
      "Description": "Name of function",
      "Value": {
        "Ref": "LambdaFunction"
      }
    },
    "FunctionArn": {
      "Description": "Arn of function",
      "Value": {
        "Fn::GetAtt": [
          "LambdaFunction",
          "Arn"
        ]
      }
    }
  },
  "Parameters": {
    "MemorySize": {
      "Description": "The size of memory in MB for lambda function, in multiple of 64, minimum 128, maximum 1536.",
      "Type": "Number",
      "Default": "128",
      "MinValue": "128",
      "MaxValue": "1536"
    },
    "Timeout": {
      "Description": "The timeout for lambda function stop executing in seconds.",
      "Type": "Number",
      "Default": "300",
      "MinValue": "1"
    },
    "BuildNumber": {
      "Description": "Jenkins Build Number",
      "Type": "String",
      "Default": "1"
    },
    "AssetID": {
      "Description": "Asset id of GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "62"
    },
    "AssetName": {
      "Description": "Short name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "DataLake"
    },
    "AssetAreaName": {
      "Description": "Short asset area name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "DataLake"
    },
    "AssetGroup": {
      "Description": "AssetGroup where this is deployed in",
      "Type": "String",
      "Default": "Global"
    },
    "LambdaBucket": {
      "Description": "S3 bucket of the lambda function",
      "Type": "String",
      "Default": "ccs-sandbox-lambda-deploys"
    },
    "LambdaS3Object": {
      "Description": "S3Key for the lambda function",
      "Type": "String",
      "Default": "DataLake/62-DataLake-Catalog.zip"
    },
    "LambdaHandler": {
      "Description": "Handler for the lambda function",
      "Type": "String",
      "Default": "remove_catalog_command.lambda_handler"
    },
    "LambdaFunctionVersionV1": {
      "Description": "Version for the Lambda Function v1 alias",
      "Type": "String",
      "Default": "$LATEST"
    }
  },
  "Resources": {
    "CloudMetadata": {
      "Type": "Custom::CloudMetadata",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "AssetID": {
          "Ref": "AssetID"
        },
        "AssetGroup": {
          "Ref": "AssetGroup"
        },
        "AssetAreaName": {
          "Ref": "AssetAreaName"
        },
        "ExportsOnly": true,
        "Version": "2",
        "Filter": [
          "CatalogDynamoTable",
          "EventStoreDynamoTable",
          "OwnerDynamoTable",
          "TrackingDynamoTable",
          "TrackingDynamoEventIDIndex",
          "RetryQueueArn",
          "RetryQueueUrl"
        ],
        "LastUpdate": {
          "Ref": "BuildNumber"
        }
      }
    },
    "LambdaFunctionVersion": {
      "Type": "Custom::LambdaVersion",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "LambdaName": {
          "Ref": "LambdaFunction"
        },
        "BuildNumber": {
          "Ref": "BuildNumber"
        }
      }
    },
    "V1LambdaAlias": {
      "Type": "AWS::Lambda::Alias",
      "Properties": {
        "Description": "v1 version of LambdaVersioning Custom Resource",
        "FunctionName": {
          "Ref": "LambdaFunction"
        },
        "FunctionVersion": {
          "Ref": "LambdaFunctionVersionV1"
        },
        "Name": "v1"
      }
    },
    "LatestLambdaAlias": {
      "Type": "AWS::Lambda::Alias",
      "Properties": {
        "Description": "Latest version of the Lambda Function for API Stage Variable",
        "FunctionName": {
          "Ref": "LambdaFunction"
        },
        "FunctionVersion": "$LATEST",
        "Name": "LATEST"
      }
    },
    "LambdaRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": [
                  "lambda.amazonaws.com"
                ]
              },
              "Action": [
                "sts:AssumeRole"
              ]
            }
          ]
        },
        "ManagedPolicyArns": [
          "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
          "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
        ],
        "Policies": [
          {
            "PolicyName": "DataLakeDynamoAccess",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowDynamoGetAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:GetItem"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.CatalogDynamoTable}"
                    },
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.OwnerDynamoTable}"
                    }
                  ]
                },
                {
                  "Sid": "AllowDynamoEventStoreAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:PutItem"
                  ],
                  "Resource": {
                    "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.EventStoreDynamoTable}"
                  }
                },
                {
                  "Sid": "AllowTrackingTableAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:PutItem",
                    "dynamodb:Query",
                    "dynamodb:DeleteItem"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.TrackingDynamoTable}"
                    },
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.TrackingDynamoTable}/index/${CloudMetadata.asset.outputs.TrackingDynamoEventIDIndex}"
                    }
                  ]
                },
                {
                  "Sid": "AllowRetryQueueAccess",
                  "Effect": "Allow",
                  "Action": [
                    "sqs:SendMessage"
                  ],
                  "Resource": [
                    {
                      "Fn::GetAtt": [
                        "CloudMetadata",
                        "asset.outputs.RetryQueueArn"
                      ]
                    }
                  ]
                }
              ]
            }
          }
        ],
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "LambdaFunction": {
      "Type": "AWS::Lambda::Function",
      "Properties": {
        "FunctionName": {
          "Fn::Sub": "${AssetGroup}-catalog-command-RemoveCatalog"
        },
        "Code": {
          "S3Bucket": {
            "Ref": "LambdaBucket"
          },
          "S3Key": {
            "Ref": "LambdaS3Object"
          }
        },
        "Description": "General Lambda Function",
        "Handler": {
          "Ref": "LambdaHandler"
        },
        "MemorySize": {
          "Ref": "MemorySize"
        },
        "Environment": {
          "Variables": {
            "CATALOG_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.CatalogDynamoTable"
              ]
            },
            "TRACKING_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.TrackingDynamoTable"
              ]
            },
            "EVENT_STORE_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.EventStoreDynamoTable"
              ]
            },
            "OWNER_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.OwnerDynamoTable"
              ]
            },
            "RETRY_QUEUE_URL": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.RetryQueueUrl"
              ]
            }
          }
        },
        "Role": {
          "Fn::GetAtt": [
            "LambdaRole",
            "Arn"
          ]
        },
        "Runtime": "python3.6",
        "Timeout": {
          "Ref": "Timeout"
        },
        "TracingConfig": {
          "Mode": "PassThrough"
        },
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "LambdaFunctionApiGatewayPermission": {
      "Type": "AWS::Lambda::Permission",
      "Properties": {
        "Action": "lambda:invokeFunction",
        "FunctionName": {
          "Fn::GetAtt": [
            "LambdaFunction",
            "Arn"
          ]
        },
        "Principal": "apigateway.amazonaws.com",
        "SourceArn": {
          "Fn::Sub": "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:*"
        }
      }
    },
    "V1LambdaAliasApiGatewayPermission": {
      "Type": "AWS::Lambda::Permission",
      "Properties": {
        "Action": "lambda:invokeFunction",
        "FunctionName": {
          "Ref": "V1LambdaAlias"
        },
        "Principal": "apigateway.amazonaws.com",
        "SourceArn": {
          "Fn::Sub": "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:*/v1/*"
        }
      }
    },
    "LatestLambdaAliasApiGatewayPermission": {
      "Type": "AWS::Lambda::Permission",
      "Properties": {
        "Action": "lambda:invokeFunction",
        "FunctionName": {
          "Ref": "LatestLambdaAlias"
        },
        "Principal": "apigateway.amazonaws.com",
        "SourceArn": {
          "Fn::Sub": "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:*/LATEST/*"
        }
      }
    },
    "LambdaFunctionLogGroup": {
      "Type": "AWS::Logs::LogGroup",
      "Properties": {
        "LogGroupName": {
          "Fn::Sub": "/aws/lambda/${LambdaFunction}"
        },
        "RetentionInDays": "30"
      }
    },
    "CloudMetadataExport": {
      "Type": "Custom::CloudMetadataExport",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "AssetID": {
          "Ref": "AssetID"
        },
        "AssetGroup": {
          "Ref": "AssetGroup"
        },
        "AssetAreaName": {
          "Ref": "AssetAreaName"
        },
        "Exports": {
          "RemoveCatalogLambda": {
            "Ref": "LambdaFunction"
          }
        },
        "Version": "2"
      }
    }
  }
}