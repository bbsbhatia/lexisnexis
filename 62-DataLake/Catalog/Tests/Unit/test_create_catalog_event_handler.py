import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

import create_catalog_event_handler as create_module
from create_catalog_event_handler import create_catalog_event_handler, insert_to_catalog_table, generate_catalog_item

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CreateCatalogEventHandler")


class CatalogEventHandlerTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'TestArn'})
    def setUpClass(cls):  # NOSONAR
        reload(create_module)
        cls.arn = 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +Successful test: valid event
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_aws_clients.session.set_session")
    @patch("create_catalog_event_handler.generate_catalog_item")
    @patch("create_catalog_event_handler.insert_to_catalog_table")
    def test_create_catalog_event_handler_valid_event(self, mocked_put_item, mocked_item_transform, mocked_set_session,
                                                      mocked_valid_version, mocked_valid_message, mocked_sns_extractor):
        event = io_utils.load_data_json("valid_event.json")
        mocked_sns_extractor.return_value = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_valid_message.return_value = True
        mocked_valid_version.return_value = True
        mocked_put_item.return_value = None
        mocked_set_session.return_value = None
        mocked_item_transform.return_value = {
            "catalog-id": "catalog-01",
            "catalog-timestamp": "2017-11-13T10:16:08.829Z",
            "description": "My Catalog description",
            "owner-id": 101
        }
        self.assertEqual(create_catalog_event_handler(event, self.arn), event_handler_status.SUCCESS)

    # -Failed test: invalid event name
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_aws_clients.session.set_session")
    def test_create_catalog_event_handler_invalid_event_name(self, mocked_set_session, mocked_valid_message,
                                                             mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_name.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_valid_message.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "event-name does not match"):
            create_catalog_event_handler(event, self.arn)

    # -Failed test: invalid event version
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_aws_clients.session.set_session")
    def test_create_catalog_event_handler_invalid_event_version(self, mocked_set_session, mocked_valid_message,
                                                                mocked_valid_version, mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_version.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_valid_message.return_value = True
        mocked_valid_version.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "event-version does not match"):
            create_catalog_event_handler(event, self.arn)

    # -Failed test: invalid event stage
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_aws_clients.session.set_session")
    def test_create_catalog_event_handler_invalid_stage(self, mocked_set_session, mocked_valid_message,
                                                        mocked_valid_version, mocked_valid_stage,
                                                        mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_stage.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_valid_message.return_value = True
        mocked_valid_version.return_value = True
        mocked_valid_stage.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "stage does not match"):
            create_catalog_event_handler(event, self.arn)

    # +Successful test: insert catalog into CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.put_item")
    def test_insert_to_catalog_table_success(self, mocked_put_item):
        item = {"catalog-id": "catalog-01"}
        mocked_put_item.return_value = None
        self.assertIsNone(insert_to_catalog_table(item))

    # -Failed test: SchemaValidationError while inserting catalog into CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.put_item")
    def test_insert_to_catalog_table_schema_validation_error(self, mocked_put_item):
        item = {"catalog-id": "catalog-01"}
        mocked_put_item.side_effect = SchemaValidationError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into Catalog table."):
            insert_to_catalog_table(item)

    # -Failed test: ConditionError while inserting catalog into CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.put_item")
    def test_insert_to_catalog_table_condition_error(self, mocked_put_item):
        item = {"catalog-id": "catalog-01"}
        mocked_put_item.side_effect = ConditionError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into Catalog table."):
            insert_to_catalog_table(item)

    # -Failed test: SchemaError while inserting catalog into CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.put_item")
    def test_insert_to_catalog_table_schema_error(self, mocked_put_item):
        item = {"catalog-id": "catalog-01"}
        mocked_put_item.side_effect = SchemaError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into Catalog table."):
            insert_to_catalog_table(item)

    # -Failed test: SchemaLoadError while inserting catalog into CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.put_item")
    def test_insert_to_catalog_table_schema_load_error(self, mocked_put_item):
        item = {"catalog-id": "catalog-01"}
        mocked_put_item.side_effect = SchemaLoadError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into Catalog table."):
            insert_to_catalog_table(item)

    # -Failed test:  endpoint connection error while inserting catalog into CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.put_item")
    def test_insert_to_catalog_table_endpoint_connection_error(self, mocked_put_item):
        item = {"catalog-id": "catalog-01"}
        mocked_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            insert_to_catalog_table(item)

    # -Failed test: ClientError while inserting catalog into CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.put_item")
    def test_insert_to_catalog_table_client_error(self, mocked_put_item):
        item = {"catalog-id": "catalog-01"}
        mocked_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                   'Error': {
                                                       'Code': 'OTHER',
                                                       'Message': 'This is a mock'}},
                                                  "client-error-mock")
        with self.assertRaises(ClientError):
            insert_to_catalog_table(item)

    # -Failed test: General Exception while inserting catalog into CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.put_item")
    def test_insert_to_catalog_table_general_exception(self, mocked_put_item):
        item = {"catalog-id": "catalog-01"}
        mocked_put_item.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item into "
                                                            "Catalog table due to unknown reasons."):
            insert_to_catalog_table(item)

    # -Failed test: failed to transform item
    @patch("lng_datalake_commons.sns_extractor")
    @patch("create_catalog_event_handler.generate_catalog_item")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_aws_clients.session.set_session")
    def test_create_catalog_event_handler_failed_transform_item(self, mocked_set_session, mocked_valid_message,
                                                                mocked_valid_version, mocked_valid_stage,
                                                                mocked_item_transform, mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_stage.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_valid_message.return_value = True
        mocked_valid_version.return_value = True
        mocked_valid_stage.return_value = True
        mocked_item_transform.return_value = None
        with self.assertRaisesRegex(TerminalErrorException, "Failed to transform invalid item"):
            create_catalog_event_handler(event, self.arn)

    # +Successful test: transform valid message
    @patch("lng_datalake_dal.catalog_table.CatalogTable.get_required_schema_keys")
    @patch("lng_datalake_dal.catalog_table.CatalogTable.get_optional_schema_keys")
    def test_generate_catalog_item_valid_message(self, mocked_optional_schema_keys, mocked_required_schema_keys):
        message = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_required_schema_keys.return_value = ["catalog-timestamp", "catalog-id", "owner-id"]
        mocked_optional_schema_keys.return_value = ["description"]
        expected_response = {
            "catalog-timestamp": "2017-11-13T10:16:08.829Z",
            "owner-id": 101,
            "catalog-id": "catalog-01",
            "description": "My Catalog description"
        }
        self.assertDictEqual(generate_catalog_item(message), expected_response)

    # -Failed test: transform invalid message
    @patch("lng_datalake_dal.catalog_table.CatalogTable.get_required_schema_keys")
    @patch("lng_datalake_dal.catalog_table.CatalogTable.get_optional_schema_keys")
    def test_generate_catalog_item_invalid_message(self, mocked_optional_schema_keys, mocked_required_schema_keys):
        message = io_utils.load_data_json("valid_deserialized_sns_message.json")
        message.pop("catalog-id")
        mocked_required_schema_keys.return_value = ["catalog-timestamp", "catalog-id", "owner-id"]
        mocked_optional_schema_keys.return_value = ["description"]
        with self.assertRaisesRegex(TerminalErrorException, "Required Key = 'catalog-id' does "
                                                            "not exist in the message."):
            generate_catalog_item(message)


if __name__ == "__main__":
    unittest.main()
