import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, CatalogAlreadyExists
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_catalog_command import lambda_handler, create_catalog_command, validate_catalog_id, generate_response_json

__author__ = "Jose Molinet, Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateCatalogCommand')


class TestCreateCatalog(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    @patch('create_catalog_command.create_catalog_command')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self, mock_session, mock_event_store_put_item, mock_create_catalog_command_response):
        mock_session.return_value = None
        generated_response = io_util.load_data_json('mock_generated_response.json')
        mock_event_store_put_item.return_value = None
        create_catalog_command_response = {
            "response-dict": generated_response,
            "event-dict": {'event-id': '1234-request-id-wxyz', 'request-time': '1526671877813',
                           'event_version': 1, 'event_name': 'Catalog::Create', 'stage': 'LATEST',
                           'catalog-id': '324', 'owner-id': 123,
                           'description': 'My Catalog Description'}
        }
        mock_create_catalog_command_response.return_value = create_catalog_command_response
        request_input = io_util.load_data_json('apigateway.request1.json')
        expected_response = io_util.load_data_json('apigateway.response1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response, lambda_handler(request_input, MockLambdaContext()))

    # +Successful test: create_catalog_command, no catalog-id
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('create_catalog_command.generate_response_json')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.session.set_session')
    def test_create_catalog_command_no_catalog_id(self, session_mock, mock_owner_from_api_key,
                                                  mock_counter_update, mock_generate_response_json, mock_request_time):
        session_mock.return_value = None
        mock_owner_from_api_key.return_value = \
            {'owner-id': 123,
             'owner-name': 'owner',
             'email-distribution': ['abc@xyz.com']}
        mock_counter_update.return_value = 324
        generated_response = io_util.load_data_json('mock_generated_response.json')
        mock_generate_response_json.return_value = generated_response
        mock_request_time.return_value = '2018-05-18T19:31:17.813Z'

        request_input = \
            {
                "description": "My Catalog Description"
            }
        expected_response_output = \
            {
                "response-dict": generated_response,
                "event-dict": {'event-id': 'abcd', 'request-time': '2018-05-18T19:31:17.813Z',
                               'event-version': 1, 'event-name': 'Catalog::Create', 'stage': 'LATEST',
                               'catalog-id': '324', 'owner-id': 123,
                               'description': 'My Catalog Description'}
            }
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.assertDictEqual(create_catalog_command(request_input, "abcd", "LATEST", "testApiKeyId"),
                             expected_response_output)

    # +Successful test: create_catalog_command, with catalog-id
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('create_catalog_command.generate_response_json')
    @patch('create_catalog_command.validate_catalog_id')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.session.set_session')
    def test_create_catalog_command_with_catalog_id(self, session_mock, mock_owner_from_api_key,
                                                    mock_validate_catalog_id, mock_generate_response_json,
                                                    mock_request_time):
        session_mock.return_value = None
        mock_owner_from_api_key.return_value = \
            {'owner-id': 123,
             'owner-name': 'owner',
             'email-distribution': ['abc@xyz.com']}
        mock_validate_catalog_id.return_value = None
        generated_response = io_util.load_data_json('mock_generated_response.json')
        generated_response['catalog-id'] = "catalog-123"
        mock_generate_response_json.return_value = generated_response
        mock_request_time.return_value = '2018-05-18T19:31:17.813Z'

        request_input = \
            {
                "catalog-id": "catalog-123",
                "description": "My Catalog Description"
            }
        expected_response_output = \
            {
                "response-dict": generated_response,
                "event-dict": {'event-id': 'abcd', 'request-time': '2018-05-18T19:31:17.813Z',
                               'event-version': 1, 'event-name': 'Catalog::Create', 'stage': 'LATEST',
                               'catalog-id': 'catalog-123', 'owner-id': 123,
                               'description': 'My Catalog Description'}
            }
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.assertDictEqual(create_catalog_command(request_input, "abcd", "LATEST", "testApiKeyId"),
                             expected_response_output)

    # +Successful test: valid catalog-id
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_validate_catalog_id(self, mock_catalog_get_item):
        mock_catalog_get_item.return_value = {}
        self.assertIsNone(validate_catalog_id("Ab-1"))

    # -Failed test: catalog-id contains more than 40 characters
    def test_validate_catalog_id_long_catalog_id(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid Catalog ID super-long-catalog-id-more-than-40-characters||"
                                    "Catalog ID length should not exceed 40 characters"):
            validate_catalog_id("super-long-catalog-id-more-than-40-characters")

    # -Failed test: catalog-id contains invalid character
    def test_validate_catalog_id_invalid_character(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Catalog ID Ab1-#||CatalogID "
                                                                 "can only contain letters, digits and hyphens"):
            validate_catalog_id("Ab1-#")

    # -Failed test: catalog-id contains only digits
    def test_validate_catalog_id_only_digits(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Catalog ID 999||Catalog ID "
                                                                 "cannot contain only digits"):
            validate_catalog_id("999")

    # -Failed test: catalog-id already exists
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_validate_catalog_id_already_exist(self, mock_catalog_get_item):
        mock_catalog_get_item.return_value = {"catalog-id": "Ab-1"}
        with self.assertRaisesRegex(CatalogAlreadyExists, "Invalid Catalog ID Ab-1||Catalog ID already exists"):
            validate_catalog_id("Ab-1")

    # -Failed test: Lambda - ClientError while accessing CounterTable to update counter
    @patch('create_catalog_command.generate_response_json')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.session.set_session')
    def test_create_catalog_command_update_counter_client_error(self, session_mock, mock_owner_from_api_key,
                                                                mock_counter_update, mock_generate_response_json):
        session_mock.return_value = None
        mock_owner_from_api_key.return_value = \
            {'owner-id': 123,
             'owner-name': 'owner',
             'email-distribution': ['abc@xyz.com']}
        mock_counter_update.return_value = 324
        generated_response = io_util.load_data_json('mock_generated_response.json')
        mock_generate_response_json.return_value = generated_response

        request_input = \
            {
                "description": "My Catalog Description"
            }
        mock_counter_update.side_effect = ClientError({'ResponseMetadata': {},
                                                       'Error': {
                                                           'Code': 'OTHER',
                                                           'Message': 'This is a mock'}},
                                                      "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to update Counter Table"):
            create_catalog_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # -Failed test: Lambda - General Exception while accessing CounterTable to update counter
    @patch('create_catalog_command.generate_response_json')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.session.set_session')
    def test_create_catalog_command_update_counter_general_exception(self, session_mock, mock_owner_from_api_key,
                                                                     mock_counter_update, mock_generate_response_json):
        session_mock.return_value = None
        mock_owner_from_api_key.return_value = \
            {'owner-id': 123,
             'owner-name': 'owner',
             'email-distribution': ['abc@xyz.com']}
        mock_counter_update.return_value = 324
        generated_response = io_util.load_data_json('mock_generated_response.json')
        mock_generate_response_json.return_value = generated_response

        request_input = \
            {
                "description": "My Catalog Description"
            }
        mock_counter_update.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            create_catalog_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # -Successful test: generate response json
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_success(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        request = {
            "catalog-id": "123",
            "description": "My catalog description"
        }
        expected_response = \
            {
                "catalog-id": "123",
                "catalog-url": "/catalogs/LATEST/123",
                "owner-id": 101,
                "catalog-timestamp": "2018-05-18T19:31:17.813Z",
                "description": "My catalog description"
            }
        self.assertDictEqual(expected_response,
                             generate_response_json(request, catalog_id="123", stage="LATEST", owner_id=101))

    # -Failed test: InternalError exception due to missing required property
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp",
            "dummy-required-prop"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        request = {
            "description": "My catalog description"
        }
        with self.assertRaisesRegex(InternalError, "Missing required property: dummy-required-prop"):
            generate_response_json(request, catalog_id="123", stage="LATEST", owner_id=101)


if __name__ == '__main__':
    unittest.main()
