import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

from service_commons.catalog_command import get_catalog_by_id, get_catalog_list, get_catalog_collection_ids

io_utils = IOUtils(__file__, 'CatalogCommand')


class TestCatalogServiceCommons(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +Successful test: catalog-id exists
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_get_catalog_by_id(self, mock_catalog_get_item):
        item = {"catalog-id": "Ab-1"}
        mock_catalog_get_item.return_value = item
        self.assertEqual(get_catalog_by_id("Ab-1"), item)

    # -Failed test: ClientError while accessing CatalogTable to validate catalog-id
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_get_catalog_by_id_client_error(self, mock_catalog_get_item):
        mock_catalog_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                         'Error': {
                                                             'Code': 'Mock ClientError',
                                                             'Message': 'This is a mock'}},
                                                        "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to get item from Catalog Table"):
            get_catalog_by_id("Ab-1")

    # -Failed test: General Exception while accessing CatalogTable to validate catalog-id
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_get_catalog_by_id_general_exception(self, mock_catalog_get_item):
        mock_catalog_get_item.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_catalog_by_id("Ab-1")

    # - get_catalog_list: query by owner exception
    @patch('lng_datalake_dal.catalog_table.CatalogTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_list_query_by_owner_exception(self, session_mock, mock_cat_query):
        session_mock.return_value = None
        mock_cat_query.side_effect = [Exception("Mock Exception"),
                                      ClientError(
                                          {'ResponseMetadata': {},
                                           'Error': {
                                               'Code': 'OTHER',
                                               'Message': 'Client Error Mock'}}, "FAKE")]
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_catalog_list(1, 1000, None)
        with self.assertRaisesRegex(InternalError, "Unable to query items from Catalog Table"):
            get_catalog_list(1, 1000, None)

    # - get_catalog_list: get all items exceptions
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_all_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_list_get_all_items_exception(self, session_mock, mock_cat_table_get_all):
        session_mock.return_value = None
        mock_cat_table_get_all.side_effect = [Exception("Mock Exception"),
                                              ClientError(
                                                  {'ResponseMetadata': {},
                                                   'Error': {
                                                       'Code': 'OTHER',
                                                       'Message': 'Client Error Mock'}}, "FAKE")]
        with self.assertRaisesRegex(InternalError,
                                    "Unhandled exception occurred"):
            get_catalog_list(None, 1000, None)
        with self.assertRaisesRegex(InternalError, "Unable to get items from Catalog Table"):
            get_catalog_list(None, 1000, None)

    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_collection_ids_failure_1(self, mock_session,
                                                  mock_catalog_collection_mapping_table_query_items, ):
        mock_session.return_value = None
        catalog_id = "catalog-01"

        mock_catalog_collection_mapping_table_query_items.side_effect = ClientError({"ResponseMetadata": {},
                                                                                     "Error": {
                                                                                         "Code": "mock error code",
                                                                                         "Message": "mock error message"}},
                                                                                    "client-error-mock")

        with self.assertRaisesRegex(InternalError, "Unable to query items from Catalog Collection Mapping Table"):
            get_catalog_collection_ids(catalog_id, -1)

    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_collection_ids_failure_2(self, mock_session,
                                                  mock_catalog_collection_mapping_table_query_items, ):
        mock_session.return_value = None
        catalog_id = "catalog-01"

        mock_catalog_collection_mapping_table_query_items.side_effect = Exception

        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_catalog_collection_ids(catalog_id, -1)


if __name__ == "__main__":
    unittest.main()
