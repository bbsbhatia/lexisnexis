import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NoSuchCatalog
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from remove_catalog_command import lambda_handler, remove_catalog_command, generate_response_json

__author__ = "Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveCatalogCommand')


class TestRemoveCatalog(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):
        TableCache.clear()

    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('remove_catalog_command.remove_catalog_command')
    @patch("lng_datalake_dal.tracking_table.TrackingTable.put_item")
    @patch('lng_aws_clients.session.set_session')
    def test_remove_catalog_lambda_handler(self, session_mock, tt_put_item_mock, mock_remove_catalog_command_response,
                                           mock_event_store_put_item):
        session_mock.return_value = None
        tt_put_item_mock.return_value = None
        mock_event_store_put_item.return_value = None
        request_input = io_util.load_data_json('apigateway.request1.json')
        generated_response = io_util.load_data_json('mock_generated_response.json')
        remove_catalog_command_response = {
            "response-dict": generated_response,
            "event-dict": {'event-id': '1234-request-id-wxyz', 'request-time': '1526671877813',
                           'event-version': 1, 'event-name': 'Catalog::Remove', 'stage': 'LATEST',
                           'catalog-id': '324'}
        }
        mock_remove_catalog_command_response.return_value = remove_catalog_command_response
        expected_response = io_util.load_data_json('apigateway.response1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response, lambda_handler(request_input, MockLambdaContext()))

    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('remove_catalog_command.generate_response_json')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('service_commons.catalog_command.get_catalog_by_id')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_catalog_command_success(self, mock_session, mock_get_catalog, mock_owner_authorization,
                                            mock_generate_response_json, mock_request_time):
        mock_session.return_value = None
        mock_get_catalog.return_value = {"catalog-id": "324",
                                         "description": "My Catalog Description",
                                         "owner-id": 123,
                                         "catalog-timestamp": "2018-05-18T19:31:17.813Z"}
        mock_owner_authorization.return_value = {}
        mock_request_time.return_value = '2018-05-18T19:31:17.813Z'
        generated_response = io_util.load_data_json('mock_generated_response.json')
        mock_generate_response_json.return_value = generated_response
        request_input = \
            {
                "catalog-id": "324"
            }
        expected_response_output = \
            {
                "response-dict": generated_response,
                "event-dict": {'event-id': 'abcd', 'request-time': '2018-05-18T19:31:17.813Z',
                               'event-version': 1, 'event-name': 'Catalog::Remove', 'stage': 'LATEST',
                               'catalog-id': '324'}
            }
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(remove_catalog_command(request_input, "abcd", "LATEST", "testApiKeyId"),
                             expected_response_output)

    # - remove_catalog_command - invalid catalog-id
    @patch('service_commons.catalog_command.get_catalog_by_id')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_catalog_command_fail_1(self, mock_session, mock_get_catalog):
        mock_session.return_value = None
        mock_get_catalog.return_value = {}
        request_input = \
            {
                "catalog-id": "324"
            }
        with self.assertRaisesRegex(NoSuchCatalog,
                                    "Invalid Catalog ID 324||Catalog ID does not exist"):
            remove_catalog_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # + generate_response_json - Success
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_success(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        catalog_record = {
            "catalog-id": "123",
            "catalog-timestamp": "2018-05-18T19:31:17.813Z",
            "owner-id": 101,
            "description": "My catalog description"
        }
        expected_response = \
            {
                "catalog-id": "123",
                "catalog-url": "/catalogs/LATEST/123",
                "owner-id": 101,
                "catalog-timestamp": "2018-05-18T19:31:17.813Z",
                "description": "My catalog description"
            }
        self.assertDictEqual(expected_response,
                             generate_response_json(catalog_record, stage="LATEST"))

    # -Failed test: InternalError exception due to missing required property
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_fail(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp",
            "dummy-required-prop"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        catalog_record = {
            "catalog-id": "123",
            "catalog-timestamp": "2018-05-18T19:31:17.813Z",
            "owner-id": 101,
            "description": "My catalog description"
        }
        with self.assertRaisesRegex(InternalError, "Missing required property: dummy-required-prop"):
            generate_response_json(catalog_record, stage="LATEST")


if __name__ == '__main__':
    unittest.main()
