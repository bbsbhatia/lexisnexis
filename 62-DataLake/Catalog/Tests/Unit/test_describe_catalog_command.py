import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, InvalidRequestPropertyName
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from describe_catalogs_command import lambda_handler, describe_catalogs_command, \
    update_catalog_list_with_collection_ids, generate_response_json

__author__ = "Samuel Jackson Sanders, Brandon Sersion"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'DescribeCatalogCommand')


class TestDescribeCatalogs(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # -Schema validation error using lambda_handler with command_wrapper decorator
    # - request has max-results property that is too large
    @patch('lng_aws_clients.session.set_session')
    def test_command_decorator_fail1(self, session_mock):
        session_mock.return_value = None

        request_input = io_util.load_data_json('apigateway.request.failure_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaises(InvalidRequestPropertyName):
                lambda_handler(request_input, MockLambdaContext())

    # + lambda_handler - valid request
    @patch('describe_catalogs_command.describe_catalogs_command')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_valid(self, mock_session,
                                  mock_describe_catalogs_command):
        mock_session.return_value = None
        mock_describe_catalogs_command.return_value = \
            {"response-dict": {
                "catalogs": [
                    {
                        "catalog-timestamp": "2018-12-03T11:53:55.041700",
                        "catalog-id": "catalog-01",
                        "owner-id": 19,
                        "catalog-url": "/catalogs/LATEST/catalog-01",
                        "description": "My first catalog test",
                        "collection-ids": [
                            "PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP"
                        ]
                    },
                    {
                        "catalog-timestamp": "2018-12-07T09:52:09.193487",
                        "catalog-id": "catalog-02",
                        "owner-id": 10,
                        "catalog-url": "/catalogs/LATEST/catalog-02",
                        "description": "My first catalog test",
                        "collection-ids": [
                            "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ",
                            "OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO"
                        ]
                    }
                ],
                "item-count": 2
            }}

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.maxDiff = None
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response,
                                 lambda_handler(request_input, MockLambdaContext()))

    # + command - valid request
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.get_pagination_token')
    @patch('lng_datalake_commands.paginator_token.unpack_validate_pagination_token')
    @patch('describe_catalogs_command.generate_response_json')
    @patch('describe_catalogs_command.update_catalog_list_with_collection_ids')
    @patch('service_commons.catalog_command.get_catalog_list')
    @patch('lng_aws_clients.session.set_session')
    def test_describe_get_catalogs_by_catalog_id_and_collection_id_success(self, mock_session,
                                                                           mock_catalog_list,
                                                                           mock_update_catalog_list_with_collection_ids,
                                                                           mock_generate_response_json,
                                                                           mock_validate_pagination_token,
                                                                           mock_cat_coll_pagination_token):
        mock_cat_coll_pagination_token.return_value = 'Not_None_Value'
        mock_session.return_value = None
        mock_validate_pagination_token.return_value = {"max-items": 11, "pagination-token": "2|somerandomstring12345"}
        generated_response = io_util.load_data_json("valid_response.json")
        mock_catalog_list.return_value = generated_response["catalogs"]
        mock_update_catalog_list_with_collection_ids.return_value = [
            "not testing, generate_response_data mock overwrites this"]

        mock_generate_response_json.return_value = generated_response
        expected_response = {"response-dict": generated_response}
        request_input = {"owner-id": 100,
                         "max-items": 2,
                         "next-token": "2|somerandomstring12345"}
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('DescribeCatalogsCommand-ResponseSchema.json')):
            self.assertEqual(describe_catalogs_command(request_input, "LATEST"), expected_response)

        mock_update_catalog_list_with_collection_ids.assert_called_with(
            io_util.load_data_json("valid_response.json")['catalogs'],
            True)

    # + command - valid request
    @patch('lng_datalake_commands.paginator_token.unpack_validate_pagination_token')
    @patch('describe_catalogs_command.generate_response_json')
    @patch('describe_catalogs_command.update_catalog_list_with_collection_ids')
    @patch('service_commons.catalog_command.get_catalog_by_id')
    @patch('lng_aws_clients.session.set_session')
    def test_describe_catalogs_by_catalog_id_that_exists_success(self, mock_session,
                                                                 mock_catalog_by_id,
                                                                 mock_update_catalog_list_with_collection_ids,
                                                                 mock_generate_response_json,
                                                                 mock_validate_pagination_token):
        mock_session.return_value = None
        mock_validate_pagination_token.return_value = {"max-items": 11, "pagination-token": "2|somerandomstring12345"}
        generated_response = io_util.load_data_json("valid_catalog_response.json")
        mock_catalog_by_id.return_value = generated_response["catalogs"][0]
        mock_update_catalog_list_with_collection_ids.return_value = [
            "not testing, generate_response_data mock overwrites this"]

        mock_generate_response_json.return_value = generated_response
        expected_response = {"response-dict": generated_response}
        request_input = {"catalog-id": "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC",
                         "max-items": 2,
                         "next-token": "2|somerandomstring12345"}
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('DescribeCatalogsCommand-ResponseSchema.json')):
            self.assertEqual(describe_catalogs_command(request_input, "LATEST"), expected_response)

        mock_update_catalog_list_with_collection_ids.assert_called_with(
            io_util.load_data_json("valid_catalog_response.json")["catalogs"], False)

    # + command - valid request
    @patch('lng_datalake_commands.paginator_token.unpack_validate_pagination_token')
    @patch('describe_catalogs_command.generate_response_json')
    @patch('describe_catalogs_command.update_catalog_list_with_collection_ids')
    @patch('service_commons.catalog_command.get_catalog_by_id')
    @patch('lng_aws_clients.session.set_session')
    def test_describe_catalogs_by_catalog_id_that_does_not_exist_success(self, mock_session,
                                                                         mock_catalog_by_id,
                                                                         mock_update_catalog_list_with_collection_ids,
                                                                         mock_generate_response_json,
                                                                         mock_validate_pagination_token):
        mock_session.return_value = None
        mock_validate_pagination_token.return_value = None
        generated_response = io_util.load_data_json("valid_catalog_response_no_catalogs.json")
        mock_catalog_by_id.return_value = {}
        mock_update_catalog_list_with_collection_ids.return_value = "Should not call this"

        mock_generate_response_json.return_value = generated_response
        expected_response = {"response-dict": generated_response}
        request_input = {"catalog-id": "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"}
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('DescribeCatalogsCommand-ResponseSchema.json')):
            self.assertEqual(describe_catalogs_command(request_input, "LATEST"), expected_response)

        mock_update_catalog_list_with_collection_ids.assert_called_with([], False)

    # + build catalog list with collection ids, truncate
    @patch('describe_catalogs_command.CatalogCollectionMappingTable')
    @patch('service_commons.catalog_command.get_catalog_collection_ids')
    def test_update_catalog_list_do_truncate(self, mock_collection_list, mock_get_pagination_token):
        mock_collection_list.return_value = [{'catalog-id': 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC',
                                              'collection-id': 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'},
                                             {'catalog-id': 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC',
                                              'collection-id': 'OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO'}]
        mock_get_pagination_token.return_value.get_pagination_token.return_value = True

        generated_response = io_util.load_data_json("valid_catalog_response.json")
        catalog_list = generated_response["catalogs"]
        truncate = True
        self.assertIsNone(update_catalog_list_with_collection_ids(catalog_list, truncate))

    # + build catalog list with collection ids, do not truncate
    @patch('describe_catalogs_command.CatalogCollectionMappingTable')
    @patch('service_commons.catalog_command.get_catalog_collection_ids')
    def test_update_catalog_list_do_not_truncate(self, mock_collection_list,
                                                 mock_get_pagination_token):
        mock_collection_list.return_value = [{'catalog-id': 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC',
                                              'collection-id': 'ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ'},
                                             {'catalog-id': 'CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC',
                                              'collection-id': 'OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO'}]
        mock_get_pagination_token.return_value.get_pagination_token.return_value = False

        generated_response = io_util.load_data_json("valid_catalog_response.json")
        catalog_list = generated_response["catalogs"]
        truncate = False
        self.assertIsNone(update_catalog_list_with_collection_ids(catalog_list, truncate))

    # - InternalError exception due to missing required property
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_negative(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp",
            "dummy-required-prop"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        catalog_list = [
            {
                "catalog-id": "catalog-01",
                "owner-id": 104,
                "description": "My first catalog test",
                "catalog-timestamp": "2019-02-05T16:07:31.657155"
            }
        ]
        with self.assertRaisesRegex(InternalError, "Missing required property: dummy-required-prop"):
            generate_response_json(catalog_list, None, "LATEST", None)

    # - InternalError exception due to missing required property
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_positive(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        catalog_list = [
            {
                "catalog-id": "catalog-01",
                "catalog-timestamp": "2019-02-05T16:07:31.657155",
                "catalog-url": "/catalogs/LATEST/catalog-01",
                "owner-id": 104,
                "description": "My first catalog test"
            }
        ]
        valid_json = io_util.load_data_json("valid_json.json")
        self.assertEqual(generate_response_json(catalog_list, "somerandomstring12345", "LATEST", 10), valid_json)

    @patch('lng_datalake_commands.paginator_token.unpack_validate_pagination_token')
    @patch('lng_aws_clients.session.set_session')
    def test_describe_catalogs_command_failure_max_items_too_large(self, mock_session,
                                                                   mock_validate_pagination_token):
        mock_session.return_value = None
        mock_validate_pagination_token.return_value = {"max-items": 11, "pagination-token": "2|somerandomstring12345"}
        request_input = {"max-items": 11, "next-token": "c29tZXJhbmRvbXN0cmluZzEyMzQ1"}
        with self.assertRaisesRegex(InvalidRequestPropertyValue, 'Invalid max-items value: 11'):
            describe_catalogs_command(request_input, "LATEST")


if __name__ == '__main__':
    unittest.main()
