import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InvalidRequestPropertyValue, \
    NotAuthorizedError, InternalError, NoSuchCatalog, NoSuchCollection, SemanticError
from lng_datalake_commons import validate
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

from update_catalog_command import transform_patch_operations, update_catalog_command, generate_response_json, \
    invalid_collection_ids, validate_collections

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'UpdateCatalogCommand')


class TestUpdateCatalogCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +apig Schema test - valid
    def test_valid_update_json(self):
        # Data has all required attributes
        input_data = io_utils.load_data_json('apigateway.update_valid.json')
        schema = io_utils.load_schema_json("UpdateCatalogCommand-RequestSchema.json")
        self.assertTrue(validate.is_valid_input_json(input_data, schema))

    # -apig Schema test - invalid
    def test_invalid_input_json(self):
        # Data has missing required attribute - missing catalog-id
        input_data1 = io_utils.load_data_json('apigateway.update_invalid1.json')
        # Data has missing required attribute - missing patch-operations
        input_data2 = io_utils.load_data_json('apigateway.update_invalid2.json')
        # Data has missing required attribute - missing patch-operations has wrong path
        input_data3 = io_utils.load_data_json('apigateway.update_invalid3.json')
        # Data has missing required attribute - missing patch-operations has wrong operation
        input_data4 = io_utils.load_data_json('apigateway.update_invalid4.json')
        # Data has missing required attribute - missing patch-operations has operation applied on wrong attribute
        input_data5 = io_utils.load_data_json('apigateway.update_invalid5.json')
        # Data has missing required attribute - missing patch-operations has operation applied on wrong attribute
        input_data6 = io_utils.load_data_json('apigateway.update_invalid6.json')
        # Data has missing required attribute - missing patch-operations has more than 4 operations
        input_data7 = io_utils.load_data_json('apigateway.update_invalid7.json')
        # Data has missing required attribute - patch-operations does not have any action (empty)
        input_data8 = io_utils.load_data_json('apigateway.update_invalid8.json')
        # Data has missing required attribute - patch-operations has an action with missing value
        input_data9 = io_utils.load_data_json('apigateway.update_invalid9.json')
        # Data has missing required attribute - patch-operations has action with wrong type value
        input_data10 = io_utils.load_data_json('apigateway.update_invalid10.json')

        schema = io_utils.load_schema_json("UpdateCatalogCommand-RequestSchema.json")
        self.assertFalse(validate.is_valid_input_json(input_data1, schema))
        self.assertFalse(validate.is_valid_input_json(input_data2, schema))
        self.assertFalse(validate.is_valid_input_json(input_data3, schema))
        self.assertFalse(validate.is_valid_input_json(input_data4, schema))
        self.assertFalse(validate.is_valid_input_json(input_data5, schema))
        self.assertFalse(validate.is_valid_input_json(input_data6, schema))
        self.assertFalse(validate.is_valid_input_json(input_data7, schema))
        self.assertFalse(validate.is_valid_input_json(input_data8, schema))
        self.assertFalse(validate.is_valid_input_json(input_data9, schema))
        self.assertFalse(validate.is_valid_input_json(input_data10, schema))

    # +update_catalog_command
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('update_catalog_command.generate_response_json')
    @patch('update_catalog_command.validate_collections')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('service_commons.catalog_command.get_catalog_by_id')
    def test_update_catalog_command(self,
                                    mock_get_catalog_by_id, mock_owner_authorize, mock_validate_collections,
                                    mock_generate_response_json, mock_request_time):
        request = {
            'catalog-id': "catalog-01",
            "patch-operations": [
                {"op": "replace", "path": "/description", "value": "new value"},
                {"op": "add", "path": "/collection-ids", "value": ["1", "3", "4"]},
                {"op": "remove", "path": "/collection-ids", "value": ["5", "6", "7"]}
            ]
        }

        mock_get_catalog_by_id.return_value = {
            "owner-id": 101,
            "catalog-id": "catalog-01",
            "catalog-timestamp": "2018-11-13T10:16:08.829Z",
            "description": "This is a sample"

        }

        mock_owner_authorize.return_value = {'owner-id': 101,
                                             'owner-name': 'owner',
                                             'email-distribution': ['abc@xyz.com']}

        mock_validate_collections.return_value = None

        mock_generate_response_json.return_value = {
            "owner-id": 101,
            "catalog-id": "catalog-01",
            "catalog-timestamp": "2018-11-13T10:16:08.829Z",
            "description": "new value"

        }
        mock_request_time.return_value = '2018-05-18T19:31:17.813Z'

        expected_response_output = \
            {'response-dict': {
                "owner-id": 101,
                "catalog-id": "catalog-01",
                "catalog-timestamp": "2018-11-13T10:16:08.829Z",
                "description": "new value"

            },
                'event-dict': {'description': 'new value',
                               'collection-id-actions': {'add': ['1', '3', '4'], 'remove': ['5', '6', '7']},
                               'event-id': '123', 'request-time': '2018-05-18T19:31:17.813Z',
                               'event-name': 'Catalog::Update',
                               'event-version': 1, 'stage': 'LATEST', 'catalog-id': 'catalog-01'}}

        response = update_catalog_command(request, "123", "LATEST", "testApiKeyId")

        response['event-dict']['collection-id-actions']['add'].sort()
        response['event-dict']['collection-id-actions']['remove'].sort()

        self.assertEqual(response, expected_response_output)

    # -update_catalog_command - InvalidRequestPropertyValue (catalog-id)
    @patch('service_commons.catalog_command.get_catalog_by_id')
    def test_update_catalog_command_test_invalid_catalog_id(self,
                                                            mock_get_catalog_by_id):
        request = {
            'catalog-id': "catalog-01",
            "patch-operations": [
                {"op": "replace", "path": "/description", "value": "new value"},
                {"op": "add", "path": "/collection-ids", "value": ["1", "3", "4"]},
                {"op": "remove", "path": "/collection-ids", "value": ["5", "6", "7"]}
            ]
        }

        mock_get_catalog_by_id.return_value = {}
        with self.assertRaisesRegex(NoSuchCatalog,
                                    "Invalid Catalog ID {}".format(request["catalog-id"])):
            update_catalog_command(request, "123", "LATEST", "testApiKeyId")

    # -Failed test of Lambda - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_update_catalog_command_failed_owner_auth(self, ct_get_item_mock,
                                                      mock_owner_authorization):
        ct_get_item_mock.return_value = \
            {
                "owner-id": 101,
                "catalog-id": "catalog-01",
                "catalog-timestamp": "2018-11-13T10:16:08.829Z",
                "description": "This is a sample"

            }
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")

        request_input = \
            {
                'catalog-id': "catalog-01",
                "patch-operations": [
                    {"op": "replace", "path": "/description", "value": "new value"},
                    {"op": "add", "path": "/collection-ids", "value": ["1", "3", "4"]},
                    {"op": "remove", "path": "/collection-ids", "value": ["5", "6", "7"]}
                ]
            }
        with self.assertRaisesRegex(NotAuthorizedError, "API Key provided is not valid for Owner ID.*"):
            update_catalog_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # -update_catalog_command - invalid description
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('service_commons.catalog_command.get_catalog_by_id')
    def test_update_catalog_command_invalid_description(self,
                                                        mock_get_catalog_by_id,
                                                        mock_owner_authorization
                                                        ):
        import random
        import string
        request = {
            'catalog-id': "catalog-01",
            "patch-operations": [
                {"op": "add", "path": "/collection-ids", "value": ["1", "3", "4"]},
                {"op": "replace", "path": "/description",
                 "value": ''.join([random.choice(string.ascii_letters) for _ in range(257)])}

            ]}

        mock_owner_authorization.return_value = {}
        mock_get_catalog_by_id.return_value = {
            "owner-id": 101,
            "catalog-id": "catalog-01",
            "catalog-timestamp": "2018-11-13T10:16:08.829Z",
            "description": "This is a sample"
        }

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid Catalog Description *"):
            update_catalog_command(request, "123", "LATEST", "testApiKeyId")

    # -update_catalog_command - exceed collection ids limit
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('service_commons.catalog_command.get_catalog_by_id')
    def test_update_catalog_command_execeeded_collection_ids_limit(self,
                                                                   mock_get_catalog_by_id,
                                                                   mock_owner_authorization
                                                                   ):
        import random
        request = {
            'catalog-id': "catalog-01",
            "patch-operations": [
                {"op": "add", "path": "/collection-ids", "value": random.sample(range(1, 2000), 1001)}
            ]}

        mock_owner_authorization.return_value = {}
        mock_get_catalog_by_id.return_value = {
            "owner-id": 101,
            "catalog-id": "catalog-01",
            "catalog-timestamp": "2018-11-13T10:16:08.829Z",
            "description": "This is a sample"
        }

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid number of Collection IDs *"):
            update_catalog_command(request, "123", "LATEST", "testApiKeyId")

    def test_invalid_collection_ids(self):
        collection_items_1 = [{'collection-id': '01'}, {'collection-id': '02'}]
        collection_ids_1 = ["01", "02", "03"]
        expected_result_1 = {"03"}
        self.assertEqual(invalid_collection_ids(set(collection_ids_1), collection_items_1), expected_result_1)

        collection_items_2 = []
        collection_ids_2 = ["01", "02", "03"]
        expected_result_2 = {"01", "02", "03"}
        self.assertEqual(invalid_collection_ids(set(collection_ids_2), collection_items_2), expected_result_2)

    # -Successful test: generate response json
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_success(self, mock_optional_keys, mock_required_keys):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp"
        ]

        request = {
            "owner-id": 101,
            "catalog-id": "catalog-01",
            "catalog-timestamp": "2018-11-13T10:16:08.829Z",
            "description": "This is a sample"
        }
        expected_response = \
            {
                "catalog-id": "catalog-01",
                "catalog-url": "/catalogs/LATEST/catalog-01",
                "owner-id": 101,
                "catalog-timestamp": "2018-11-13T10:16:08.829Z",
                "description": "This is a sample"
            }
        self.assertDictEqual(expected_response,
                             generate_response_json(request, catalog_id="catalog-01", stage="LATEST"))

    # -Failed test: InternalError exception due to missing required property
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_missing_required_property(self, mock_optional_keys, mock_required_keys):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp"
        ]

        request = {
            "catalog-id": "catalog-01",
            "catalog-timestamp": "2018-11-13T10:16:08.829Z",
            "description": "This is a sample"
        }

        with self.assertRaisesRegex(InternalError, "Missing required property: owner-id"):
            generate_response_json(request, catalog_id="catalog-01", stage="LATEST")

    def test_transform_patch_document_success_1(self):
        patch_document_1 = [
            {"op": "replace", "path": "/description", "value": "catalog description"},
            {"op": "add", "path": "/collection-ids", "value": ["1", "2", "4"]},
            {"op": "remove", "path": "/collection-ids", "value": ["5", "6", "7"]}
        ]

        expected_update_item_1 = {
            'collection-id-actions': {
                'add': ["1", "2", "4"],
                'remove': ["5", "6", "7"]
            },
            'description': 'catalog description'
        }

        update_item_1 = transform_patch_operations(patch_document_1)

        update_item_1['collection-id-actions']['add'].sort()
        update_item_1['collection-id-actions']['remove'].sort()

        self.assertEqual(update_item_1, expected_update_item_1)

    def test_transform_patch_document_success_2(self):
        # Case 2
        patch_document_2 = [
            {"op": "replace", "path": "/description", "value": "catalog description"},
            {"op": "add", "path": "/collection-ids", "value": ["1", "2", "4"]},
            {"op": "remove", "path": "/collection-ids", "value": ["1", "2"]}
        ]

        expected_update_item_2 = {
            'collection-id-actions': {
                'add': ["4"],
                'remove': ["1", "2"]
            },
            'description': 'catalog description'
        }

        update_item_2 = transform_patch_operations(patch_document_2)

        update_item_2['collection-id-actions']['add'].sort()
        update_item_2['collection-id-actions']['remove'].sort()

        self.assertEqual(update_item_2, expected_update_item_2)

    def test_transform_patch_document_success_3(self):
        # Case 3
        patch_document_3 = [
            {"op": "replace", "path": "/description", "value": "catalog description"},
            {"op": "remove", "path": "/collection-ids", "value": ["1", "2"]},
            {"op": "add", "path": "/collection-ids", "value": ["1", "2", "4", "5"]},
        ]

        expected_update_item_3 = {
            'collection-id-actions': {
                'add': ["1", "2", "4", "5"],
                'remove': []
            },
            'description': 'catalog description'
        }

        update_item_3 = transform_patch_operations(patch_document_3)

        update_item_3['collection-id-actions']['add'].sort()
        update_item_3['collection-id-actions']['remove'].sort()

        self.assertEqual(update_item_3, expected_update_item_3)

    def test_transform_patch_document_success_4(self):
        # Case 4
        patch_document_4 = [
            {"op": "replace", "path": "/description", "value": "catalog description"},
            {"op": "add", "path": "/collection-ids", "value": ["1", "2", "4"]},
        ]

        expected_update_item_4 = {
            'collection-id-actions': {
                'add': ["1", "2", "4"],
                'remove': []
            },
            'description': 'catalog description'
        }

        update_item_4 = transform_patch_operations(patch_document_4)

        update_item_4['collection-id-actions']['add'].sort()
        update_item_4['collection-id-actions']['remove'].sort()

        self.assertEqual(update_item_4, expected_update_item_4)

    def test_transform_patch_document_success_5(self):
        # Case 5
        patch_document_5 = [
            {"op": "replace", "path": "/description", "value": "catalog description"},
            {"op": "remove", "path": "/collection-ids", "value": ["1", "2", "4"]},
        ]
        expected_update_item_5 = {
            'collection-id-actions': {
                'add': [],
                'remove': ["1", "2", "4"]
            },
            'description': 'catalog description'
        }

        update_item_5 = transform_patch_operations(patch_document_5)

        update_item_5['collection-id-actions']['add'].sort()
        update_item_5['collection-id-actions']['remove'].sort()

        self.assertEqual(update_item_5, expected_update_item_5)

    def test_transform_patch_document_success_6(self):
        # Case 6
        patch_document_6 = [
            {"op": "replace", "path": "/description", "value": "catalog description"}

        ]
        expected_update_item_6 = {
            'collection-id-actions': {
                'add': [],
                'remove': []
            },
            'description': 'catalog description'
        }

        update_item_6 = transform_patch_operations(patch_document_6)

        self.assertEqual(update_item_6, expected_update_item_6)

    def test_transform_patch_document_success_7(self):
        # Case 7
        patch_document_7 = [
            {"op": "replace", "path": "/description", "value": "catalog description"}

        ]
        expected_update_item_7 = {
            'collection-id-actions': {
                'add': [],
                'remove': []
            },
            'description': 'catalog description'
        }

        update_item_7 = transform_patch_operations(patch_document_7)

        self.assertEqual(update_item_7, expected_update_item_7)

    # +Successful test: batch_get_collection
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collections(self, mock_batch_get_collection):
        items = [{'collection-id': '123', 'description': 'My first collection test', 'collection-state': 'Created',
                  'owner-id': 101, 'asset-id': 62}]
        mock_batch_get_collection.return_value = items
        collection_ids = ["123"]
        self.assertIsNone(validate_collections(collection_ids))

    # -Failed test: missing collection ids
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collections_missing_collection(self, mock_batch_get_collection):
        items = [{'collection-id': '123', 'description': 'My first collection test', 'collection-state': 'Created',
                  'owner-id': 101, 'asset-id': 62}]
        mock_batch_get_collection.return_value = items
        collection_ids = ["123", "456"]
        with self.assertRaisesRegex(NoSuchCollection, "Invalid Collection IDs {'456'}"):
            validate_collections(collection_ids)

    # -Failed test: invalid state
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collections_invalid_collection(self, mock_batch_get_collection):
        items = io_utils.load_data_json('collection_data.json')
        mock_batch_get_collection.return_value = items
        collection_ids = ["123", "456", "789"]
        with self.assertRaisesRegex(SemanticError, "Collections must be in either a state of Created or Suspended"):
            validate_collections(collection_ids)

    # -Failed test: ClientError while accessing CollectionTable to validate collection-ids
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collections_client_error(self, mock_batch_get_collection):
        mock_batch_get_collection.side_effect = ClientError({'ResponseMetadata': {},
                                                             'Error': {
                                                                 'Code': 'Mock ClientError',
                                                                 'Message': 'This is a mock'}},
                                                            "FAKE")
        collection_ids = ["123", "456", "789"]
        with self.assertRaisesRegex(InternalError, "Unable to get item from Collection Table"):
            validate_collections(collection_ids)

    # -Failed test: General Exception while accessing CollectionTable to validate collection-ids
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_validate_collections_general_exception(self, mock_batch_get_collection):
        mock_batch_get_collection.side_effect = Exception
        collection_ids = ["123", "456", "789"]
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            validate_collections(collection_ids)


if __name__ == '__main__':
    unittest.main()
