import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyValue, InternalError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from list_catalogs_command import lambda_handler, list_catalogs_command, generate_response_json

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListCatalogsCommand')


class TestListCatalogs(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda_handler - valid request
    @patch('list_catalogs_command.list_catalogs_command')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_valid(self, mock_session,
                                  mock_list_catalogs_command):
        mock_session.return_value = None
        mock_list_catalogs_command.return_value = \
            {"response-dict": {
                "catalogs": [
                    {
                        "catalog-timestamp": "2018-12-03T11:53:55.041700",
                        "catalog-id": "catalog-01",
                        "owner-id": 19,
                        "catalog-url": "/catalogs/LATEST/catalog-01",
                        "description": "My first catalog test"
                    },
                    {
                        "catalog-timestamp": "2018-12-07T09:52:09.193487",
                        "catalog-id": "catalog-02",
                        "owner-id": 10,
                        "catalog-url": "/catalogs/LATEST/catalog-02",
                        "description": "My first catalog test"
                    }
                ],
                "item-count": 2
            }}

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.maxDiff = None
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response,
                                 lambda_handler(request_input, mock_lambda_context.MockLambdaContext()))

    # + command - valid request
    @patch('lng_datalake_commands.paginator_token.unpack_validate_pagination_token')
    @patch('list_catalogs_command.generate_response_json')
    @patch('service_commons.catalog_command.get_catalog_list')
    @patch('lng_aws_clients.session.set_session')
    def test_list_catalogs_command_success(self, mock_session,
                                           mock_catalog_list,
                                           mock_generate_response_json,
                                           mock_validate_pagination_token):
        mock_session.return_value = None
        mock_validate_pagination_token.return_value = {'max-items': 2, 'pagination-token': 'somerandomstring12345'}
        generated_response = io_util.load_data_json("valid_response.json")
        mock_catalog_list.return_value = generated_response["catalogs"]
        mock_generate_response_json.return_value = generated_response
        expected_response = {"response-dict": generated_response}
        request_input = {"owner-id": 100,
                         "max-items": 2,
                         "next-token": "2|somerandomstring12345"}
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('ListCatalogsCommand-ResponseSchema.json')):
            self.assertEqual(list_catalogs_command(request_input, "LATEST"), expected_response)

    # - InternalError exception due to missing required property
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_negative(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['description']
        mock_required_keys.return_value = [
            "catalog-id",
            "catalog-url",
            "owner-id",
            "catalog-timestamp",
            "dummy-required-prop"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        catalog_list = [
            {
                "catalog-id": "catalog-01",
                "owner-id": 104,
                "description": "My first catalog test",
                "catalog-timestamp": "2019-02-05T16:07:31.657155"
            }
        ]
        with self.assertRaisesRegex(InternalError, "Missing required property: dummy-required-prop"):
            generate_response_json(catalog_list, None, "LATEST", None)

    # - list_catalogs_command - max-items > 1000
    @patch('lng_datalake_commands.paginator_token.unpack_validate_pagination_token')
    @patch('lng_aws_clients.session.set_session')
    def test_list_catalogs_command_failure_max_items_too_large(self, mock_session,
                                                               mock_validate_pagination_token):
        mock_session.return_value = None
        mock_validate_pagination_token.return_value = {'max-items': 2, 'pagination-token': 'somerandomstring12345'}
        request_input = {"owner-id": 100,
                         "max-items": 2000,
                         "next-token": "2|somerandomstring12345"}
        with self.assertRaisesRegex(InvalidRequestPropertyValue, 'Invalid max-items value: 2000'):
            list_catalogs_command(request_input, "LATEST")

if __name__ == "__main__":
    unittest.main()