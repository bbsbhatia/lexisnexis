import os
import unittest
from importlib import reload
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

import remove_catalog_event_handler as remove_module
from remove_catalog_event_handler import remove_catalog_event_handler, remove_collections_from_catalog, \
    remove_catalog_record, get_catalog_collection_ids

__author__ = "Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, 'RemoveCatalogEventHandler')


class TestRemoveCatalogEventHandler(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'TestArn'})
    def setUpClass(cls):  # NOSONAR
        reload(remove_module)
        cls.arn = 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +Successful test: valid event
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_aws_clients.session.set_session")
    @patch('remove_catalog_event_handler.remove_collections_from_catalog')
    @patch("remove_catalog_event_handler.remove_catalog_record")
    def test_remove_catalog_event_handler_valid_event(self, mock_remove_catalog_record,
                                                      mock_remove_collections_from_catalog,
                                                      mocked_set_session,
                                                      mocked_valid_version,
                                                      mocked_valid_message,
                                                      mocked_sns_extractor):
        event = io_utils.load_data_json("valid_event.json")
        mocked_sns_extractor.return_value = io_utils.load_data_json("valid_deserialized_sns_message.json")
        mocked_valid_message.return_value = True
        mocked_valid_version.return_value = True
        mock_remove_catalog_record.return_value = None
        mock_remove_collections_from_catalog.return_value = None
        mocked_set_session.return_value = None
        self.assertEqual(remove_catalog_event_handler(event, self.arn), event_handler_status.SUCCESS)

    # -Failed test: invalid event name
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_aws_clients.session.set_session")
    def test_remove_catalog_event_handler_invalid_event_name(self, mocked_set_session, mocked_valid_message,
                                                             mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_name.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_valid_message.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "event-name does not match"):
            remove_catalog_event_handler(event, self.arn)

    # -Failed test: invalid event version
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_aws_clients.session.set_session")
    def test_remove_catalog_event_handler_invalid_event_version(self, mocked_set_session, mocked_valid_message,
                                                                mocked_valid_version, mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_version.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_valid_message.return_value = True
        mocked_valid_version.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "event-version does not match"):
            remove_catalog_event_handler(event, self.arn)

    # -Failed test: invalid event stage
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate.is_valid_event_stage")
    @patch("lng_datalake_commons.validate.is_valid_event_version")
    @patch("lng_datalake_commons.validate.is_valid_event_message")
    @patch("lng_aws_clients.session.set_session")
    def test_remove_catalog_event_handler_invalid_stage(self, mocked_set_session, mocked_valid_message,
                                                        mocked_valid_version, mocked_valid_stage,
                                                        mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_stage.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_valid_message.return_value = True
        mocked_valid_version.return_value = True
        mocked_valid_stage.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "stage does not match"):
            remove_catalog_event_handler(event, self.arn)

    # + remove_collections_from_catalog
    @patch('remove_catalog_event_handler.get_catalog_collection_ids')
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_remove_collections_from_catalog_success(self, mock_batch_write_items,
                                                     mock_get_catalog_collection_ids):
        mock_batch_write_items.return_value = None
        mock_get_catalog_collection_ids.return_value = [{'catalog-id': 'catalog-01',
                                                         'collection-id': 'collection-01'},
                                                        {'catalog-id': 'catalog-01',
                                                         'collection-id': 'collection-02'}]
        self.assertIsNone(remove_collections_from_catalog("catalog-01"))

    # -Failed test: EndpointConnectionError while removing collections from catalog
    @patch('remove_catalog_event_handler.get_catalog_collection_ids')
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_remove_collections_from_catalog_endpoint_connection_error_exception(self, mock_batch_write_items,
                                                                                 mock_get_catalog_collection_ids):
        mock_get_catalog_collection_ids.return_value = [{'catalog-id': 'catalog-01',
                                                         'collection-id': 'collection-01'},
                                                        {'catalog-id': 'catalog-01',
                                                         'collection-id': 'collection-02'}]
        mock_batch_write_items.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            remove_collections_from_catalog("catalog-01")

    # -Failed test: ClientError while removing collections from catalog
    @patch('remove_catalog_event_handler.get_catalog_collection_ids')
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_remove_collections_from_catalog_fail_1(self, mock_batch_write_items,
                                                    mock_get_catalog_collection_ids):
        mock_get_catalog_collection_ids.return_value = [{'catalog-id': 'catalog-01',
                                                         'collection-id': 'collection-01'},
                                                        {'catalog-id': 'catalog-01',
                                                         'collection-id': 'collection-02'}]
        mock_batch_write_items.side_effect = ClientError({'ResponseMetadata': {},
                                                          'Error': {
                                                              'Code': 'OTHER',
                                                              'Message': 'This is a mock'}},
                                                         "client-error-mock")
        with self.assertRaises(ClientError):
            remove_collections_from_catalog("catalog-01")

    # -Failed test: Exception while removing collections from catalog
    @patch('remove_catalog_event_handler.get_catalog_collection_ids')
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_remove_collections_from_catalog_fail_2(self, mock_batch_write_items,
                                                    mock_get_catalog_collection_ids):
        mock_get_catalog_collection_ids.return_value = [{'catalog-id': 'catalog-01',
                                                         'collection-id': 'collection-01'},
                                                        {'catalog-id': 'catalog-01',
                                                         'collection-id': 'collection-02'}]
        mock_batch_write_items.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "General exception occurred when removing "
                                                            "records from the CatalogCollectionMappingTable."):
            remove_collections_from_catalog("catalog-01")

    # -Failed test: EndpointConnectionError get_catalog_collection_ids
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_collection_ids_endpoint_connection_error_exception(self, mock_session,
                                                                            mock_catalog_collection_mapping_table_query_items):
        mock_session.return_value = None
        catalog_id = "catalog-01"

        mock_catalog_collection_mapping_table_query_items.side_effect = \
            EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')

        with self.assertRaises(EndpointConnectionError):
            get_catalog_collection_ids(catalog_id)

    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_collection_ids_failure_1(self, mock_session,
                                                  mock_catalog_collection_mapping_table_query_items):
        mock_session.return_value = None
        catalog_id = "catalog-01"
        mock_catalog_collection_mapping_table_query_items.side_effect = \
            ClientError({"ResponseMetadata": {},
                         "Error": {
                             "Code": "mock error code",
                             "Message": "mock error message"}},
                        "client-error-mock")

        with self.assertRaises(ClientError):
            get_catalog_collection_ids(catalog_id)

    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_collection_ids_failure_2(self, mock_session,
                                                  mock_catalog_collection_mapping_table_query_items, ):
        mock_session.return_value = None
        catalog_id = "catalog-01"
        mock_session.return_value = None

        mock_catalog_collection_mapping_table_query_items.side_effect = Exception

        with self.assertRaisesRegex(TerminalErrorException,
                                    "General exception occurred when getting records from the CatalogCollectionMappingTable by catalog-id."):
            get_catalog_collection_ids(catalog_id)

    # + remove_catalog_record
    @patch('lng_datalake_dal.catalog_table.CatalogTable.delete_item')
    def test_remove_catalog_record_success(self, mock_delete_item):
        mock_delete_item.return_value = None
        self.assertIsNone(remove_catalog_record("catalog-01"))

    # - Failed test: EndpointConnectionError while removing catalog record
    @patch('lng_datalake_dal.catalog_table.CatalogTable.delete_item')
    def test_remove_catalog_record_endpoint_connection_error_exception(self, mock_delete_item):
        mock_delete_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            remove_catalog_record("catalog-01")

    # - Failed test: ClientError while removing catalog record
    @patch('lng_datalake_dal.catalog_table.CatalogTable.delete_item')
    def test_remove_catalog_record_fail_1(self, mock_delete_item):
        mock_delete_item.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'OTHER',
                                                        'Message': 'This is a mock'}},
                                                   "client-error-mock")
        with self.assertRaises(ClientError):
            remove_catalog_record("catalog-01")

    # - Failed test: Exception while removing catalog record
    @patch('lng_datalake_dal.catalog_table.CatalogTable.delete_item')
    def test_remove_catalog_record_fail_2(self, mock_delete_item):
        mock_delete_item.side_effect = Exception

        with self.assertRaisesRegex(TerminalErrorException, "General exception occurred when removing catalog id"):
            remove_catalog_record("catalog-01")


if __name__ == "__main__":
    unittest.main()
