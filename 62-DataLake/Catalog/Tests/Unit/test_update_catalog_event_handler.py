import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils

import update_catalog_event_handler as update_module
from update_catalog_event_handler import update_catalog_table, update_catalog_collections_mapping, \
    update_catalog_event_handler, get_catalog_by_id

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "UpdateCatalogEventHandler")


class UpdateCatalogEventHandlerTest(TestCase):

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'TestArn'})
    def setUpClass(cls):  # NOSONAR
        reload(update_module)
        cls.arn = 'arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST'

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +Successful test: valid event with updated description only
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    @patch("update_catalog_event_handler.get_catalog_by_id")
    @patch("update_catalog_event_handler.update_catalog_table")
    def test_create_catalog_event_handler_valid_event(self, mocked_update_table, mocked_get_catalog, mocked_set_session,
                                                      mocked_validation, mocked_sns_extractor):
        event = io_utils.load_data_json("valid_event.json")
        mocked_sns_extractor.return_value = io_utils.load_data_json("valid_deserialized_sns_message_1.json")
        mocked_validation.is_valid_event_message.return_value = True
        mocked_validation.is_valid_event_version.return_value = True
        mocked_validation.is_valid_event_stage.return_value = True
        mocked_set_session.return_value = None
        mocked_update_table.return_value = None
        mocked_get_catalog.return_value = {'catalog-id': 'catalog-01'}

        self.assertEqual(update_catalog_event_handler(event, self.arn), event_handler_status.SUCCESS)

    # +Successful test: valid event with collection ids
    @patch("update_catalog_event_handler.update_catalog_table")
    @patch("update_catalog_event_handler.get_catalog_by_id")
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    @patch("update_catalog_event_handler.update_catalog_collections_mapping")
    @patch("lng_datalake_commons.error_handling.error_handler.set_message_additional_attribute")
    def test_create_catalog_event_handler_valid_event_with_collections(self, mocked_set_attribute, mocked_update,
                                                                       mocked_set_session, mocked_validation,
                                                                       mocked_sns_extractor, mocked_get_catalog,
                                                                       mocked_update_table):
        event = io_utils.load_data_json("valid_event_with_collection_ids.json")
        mocked_sns_extractor.return_value = io_utils.load_data_json("valid_deserialized_sns_message_2.json")
        mocked_validation.is_valid_event_message.return_value = True
        mocked_validation.is_valid_event_version.return_value = True
        mocked_validation.is_valid_event_stage.return_value = True
        mocked_set_session.return_value = None
        mocked_set_attribute.return_value = None
        mocked_get_catalog.return_value = {'catalog-id': 'catalog-01'}
        mocked_update_table.return_value = None
        index_coll_add = 3
        index_coll_del = 1
        mocked_update.return_value = index_coll_add, index_coll_del
        with self.assertRaisesRegex(RetryEventException,
                                    "Retry event to finish the update with the unprocessed collection ids"):
            update_catalog_event_handler(event, self.arn)

    # -Failed test: invalid event name
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    def test_create_catalog_event_handler_invalid_event_name(self, mocked_set_session, mocked_validation,
                                                             mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_name.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_validation.is_valid_event_message.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "event-name doesnt match"):
            update_catalog_event_handler(event, self.arn)

    # -Failed test: invalid event version
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    def test_create_catalog_event_handler_invalid_event_version(self, mocked_set_session, mocked_validation,
                                                                mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_version.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_validation.is_valid_event_message.return_value = True
        mocked_validation.is_valid_event_version.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "event-version does not match"):
            update_catalog_event_handler(event, self.arn)

    # -Failed test: invalid event stage
    @patch("lng_datalake_commons.sns_extractor")
    @patch("lng_datalake_commons.validate")
    @patch("lng_aws_clients.session.set_session")
    def test_create_catalog_event_handler_invalid_event_stage(self, mocked_set_session, mocked_validation,
                                                              mocked_sns_extractor):
        event = io_utils.load_data_json("invalid_event_stage.json")
        mocked_set_session.return_value = None
        mocked_sns_extractor.return_value = None
        mocked_validation.is_valid_event_message.return_value = True
        mocked_validation.is_valid_event_version.return_value = True
        mocked_validation.is_valid_event_stage.return_value = False
        with self.assertRaisesRegex(TerminalErrorException, "stage not match"):
            update_catalog_event_handler(event, self.arn)

    # +Successful test: update catalog in Catalog Table
    @patch("lng_datalake_dal.catalog_table.CatalogTable.update_item")
    def test_update_catalog_table_success(self, mocked_update_item):
        item = {"catalog-id": "catalog-01"}
        mocked_update_item.return_value = None
        self.assertIsNone(update_catalog_table(item))

    # -Failed test: SchemaValidationError while updating catalog in CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.update_item")
    def test_update_catalog_table_schema_validation_error(self, mocked_update_item):
        item = {"catalog-id": "catalog-01"}
        mocked_update_item.side_effect = SchemaValidationError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to update item in Catalog table"):
            update_catalog_table(item)

    # -Failed test: ConditionError while updating catalog in CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.update_item")
    def test_update_catalog_table_condition_error(self, mocked_update_item):
        item = {"catalog-id": "catalog-01"}
        mocked_update_item.side_effect = ConditionError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to update item in Catalog table"):
            update_catalog_table(item)

    # -Failed test: SchemaError while updating catalog in CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.update_item")
    def test_update_catalog_table_schema_error(self, mocked_update_item):
        item = {"catalog-id": "catalog-01"}
        mocked_update_item.side_effect = SchemaError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to update item in Catalog table"):
            update_catalog_table(item)

    # -Failed test: SchemaLoadError while updating catalog in CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.update_item")
    def test_update_catalog_table_schema_load_error(self, mocked_update_item):
        item = {"catalog-id": "catalog-01"}
        mocked_update_item.side_effect = SchemaLoadError
        with self.assertRaisesRegex(TerminalErrorException, "Failed to update item in Catalog table"):
            update_catalog_table(item)

    # -Failed test: EndpointConnectionError while updating catalog in CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.update_item")
    def test_update_catalog_table_endpoint_connection_error_exception(self, mocked_update_item):
        item = {"catalog-id": "catalog-01"}
        mocked_update_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            update_catalog_table(item)

    # -Failed test: ClientError while updating catalog in CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.update_item")
    def test_update_catalog_table_client_error(self, mocked_update_item):
        item = {"catalog-id": "catalog-01"}
        mocked_update_item.side_effect = ClientError({'ResponseMetadata': {},
                                                      'Error': {
                                                          'Code': 'OTHER',
                                                          'Message': 'This is a mock'}},
                                                     "client-error-mock")
        with self.assertRaises(ClientError):
            update_catalog_table(item)

    # -Failed test: General Exception while updating catalog in CatalogTable
    @patch("lng_datalake_dal.catalog_table.CatalogTable.update_item")
    def test_update_catalog_table_general_exception(self, mocked_update_item):
        item = {"catalog-id": "catalog-01"}
        mocked_update_item.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Failed to update item in Catalog table due to"
                                                            " unknown reasons."):
            update_catalog_table(item)

    # +Successful test: update CatalogCollectionMappingTable
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_update_catalog_collections_mapping(self, mock_batch_write_items):
        catalog_id = "101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        mock_batch_write_items.return_value = {}
        index_add, index_del = update_catalog_collections_mapping(catalog_id, 'test_event', collection_ids_to_add,
                                                                  collection_ids_to_delete, index_add=0, index_del=0)
        self.assertGreaterEqual(index_add, len(collection_ids_to_add))
        self.assertGreaterEqual(index_del, len(collection_ids_to_delete))

    # -Failed test: unprocessed items when updating the CatalogCollectionMappingTable
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_update_catalog_collections_mapping_unprocessed_items(self, mock_batch_write_items):
        catalog_id = "101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        mock_batch_write_items.return_value = {'PutRequest': {'catalog-id': '101', 'collection-id': '3'}}
        index_add, index_del = update_catalog_collections_mapping(catalog_id, 'test_event', collection_ids_to_add,
                                                                  collection_ids_to_delete, index_add=0, index_del=0)
        self.assertGreaterEqual(index_add, 0)
        self.assertGreaterEqual(index_del, 0)

    # -Failed test: EndpointConnectionError when updating CatalogCollectionMappingTable with batch_write_items
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_update_catalog_collections_mapping_endpoint_connection_error_exception(self, mock_batch_write_items):
        catalog_id = "101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        mock_batch_write_items.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        index_add, index_del = update_catalog_collections_mapping(catalog_id, 'test_event', collection_ids_to_add,
                                                                  collection_ids_to_delete, index_add=0, index_del=0)
        self.assertGreaterEqual(index_add, 0)
        self.assertGreaterEqual(index_del, 0)

    # -Failed test: ClientError when updating CatalogCollectionMappingTable with batch_write_items
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_update_catalog_collections_mapping_client_error(self, mock_batch_write_items):
        catalog_id = "101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        mock_batch_write_items.side_effect = ClientError({'ResponseMetadata': {},
                                                          'Error': {
                                                              'Code': 'Mock ClientError',
                                                              'Message': 'This is a mock'}},
                                                         "FAKE")
        index_add, index_del = update_catalog_collections_mapping(catalog_id, 'test_event', collection_ids_to_add,
                                                                  collection_ids_to_delete, index_add=0, index_del=0)
        self.assertGreaterEqual(index_add, 0)
        self.assertGreaterEqual(index_del, 0)

    # -Failed test: General Exception when updating CatalogCollectionMappingTable with batch_write_items
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.batch_write_items')
    def test_update_catalog_collections_mapping_general_exception(self, mock_batch_write_items):
        catalog_id = "101"
        collection_ids_to_add = ["1", "2", "3"]
        collection_ids_to_delete = ["4", "5"]
        mock_batch_write_items.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "General Exception Occurred"):
            update_catalog_collections_mapping(catalog_id, 'test_event', collection_ids_to_add, collection_ids_to_delete, index_add=0,
                                               index_del=0)

    # +Successful test: catalog-id exists
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_get_catalog_by_id(self, mock_catalog_get_item):
        item = {"catalog-id": "Ab-1"}
        mock_catalog_get_item.return_value = item
        self.assertEqual(get_catalog_by_id("Ab-1"), item)

    # -Failed test: EndpointConnectionError while accessing CatalogTable to validate catalog-id
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_get_catalog_by_id_endpoint_connection_error_exception(self, mock_catalog_get_item):
        mock_catalog_get_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaises(EndpointConnectionError):
            get_catalog_by_id("Ab-1")

    # -Failed test: ClientError while accessing CatalogTable to validate catalog-id
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_get_catalog_by_id_client_error(self, mock_catalog_get_item):
        mock_catalog_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                         'Error': {
                                                             'Code': 'Mock ClientError',
                                                             'Message': 'This is a mock'}},
                                                        "FAKE")
        with self.assertRaises(ClientError):
            get_catalog_by_id("Ab-1")

    # -Failed test: General Exception while accessing CatalogTable to validate catalog-id
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_get_catalog_by_id_general_exception(self, mock_catalog_get_item):
        catalog_id = "Ab-1"
        mock_catalog_get_item.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Failed to get catalog {0} from Catalog Table due to "
                                                            "unknown reasons".format(catalog_id)):
            get_catalog_by_id(catalog_id)


if __name__ == "__main__":
    unittest.main()
