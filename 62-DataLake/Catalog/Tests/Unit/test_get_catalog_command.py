import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import NoSuchCatalog, InternalError
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from get_catalog_command import get_catalog_command, generate_response_json, lambda_handler

__author__ = "Prashant Srivastava , Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetCatalogCommand')


class TestGetCatalogCommand(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    # + lambda_handler - valid request
    @patch('get_catalog_command.get_catalog_command')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_valid(self, mock_session,
                                  mock_get_catalog_command):
        mock_session.return_value = None
        mock_get_catalog_command.return_value = \
            {'response-dict': {"owner-id": 101,
                               "catalog-id": "catalog-01",
                               "catalog-timestamp": "2018-11-13T10:16:08.829Z",
                               "description": "This is a sample",
                               "catalog-url": "/catalogs/LATEST/catalog-01"
                               }
             }

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.maxDiff = None
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response,
                                 lambda_handler(request_input, mock_lambda_context.MockLambdaContext()))

    # +get_catalog_command - valid
    @patch('get_catalog_command.generate_response_json')
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_valid_get_catalog_command(self, mock_session,
                                       mock_catalog_table_get_item,
                                       mock_generate_response_json):
        mock_session.return_value = None

        mock_catalog_table_get_item.return_value = io_util.load_data_json('dynamodb.mock_get_response.json')
        generated_response = io_util.load_data_json('valid_response.json')
        mock_generate_response_json.return_value = generated_response
        request_input = {"catalog-id": "Catalog-01"}
        expected_response = \
            {
                "response-dict": generated_response
            }
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetCatalogCommand-ResponseSchema.json')):
            self.assertEqual(get_catalog_command(request_input, "LATEST"), expected_response)

    # - get_catalog_command - Invalid catalog-id
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_command_failure_1(self, mock_session,
                                           mock_catalog_table_get_item):
        mock_session.return_value = None
        mock_catalog_table_get_item.return_value = {}
        request_input = {'catalog-id': "catalog-20"}
        with self.assertRaisesRegex(NoSuchCatalog, "Invalid Catalog ID.*"):
            get_catalog_command(request_input, "LATEST")

    # - get_catalog_command - Invalid catalog-id
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_command_failure_2(self, mock_session,
                                           mock_catalog_table_get_item):
        mock_session.return_value = None
        mock_catalog_table_get_item.side_effect = Exception
        request_input = {'catalog-id': "catalog-20"}
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_catalog_command(request_input, "LATEST")

    # - get_catalog_command - raise errors
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_catalog_command_failure_3(self, mock_session,
                                           mock_catalog_table_get_item):
        mock_session.return_value = None
        request_input = {'catalog-id': "catalog-20"}

        mock_catalog_table_get_item.side_effect = ClientError({"ResponseMetadata": {},
                                                               "Error": {"Code": "mock error code",
                                                                         "Message": "mock error message"}},
                                                              "client-error-mock")

        with self.assertRaisesRegex(InternalError, "Unable to get item from Catalog Table"):
            get_catalog_command(request_input, "LATEST")

    # + generate_response_json - success
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json(self, mock_required_props,
                                    mock_optional_props):
        mock_required_props.return_value = ['catalog-id', 'catalog-timestamp', 'owner-id',
                                            'catalog-url']
        mock_optional_props.return_value = ['description']

        catalog_record = \
            {
                "owner-id": 101,
                "catalog-id": "catalog-01",
                "catalog-timestamp": "2018-11-13T10:16:08.829Z",
                "description": "This is a sample"

            }

        expected_response = \
            {
                "owner-id": 101,
                "catalog-id": "catalog-01",
                "catalog-timestamp": "2018-11-13T10:16:08.829Z",
                "description": "This is a sample",
                "catalog-url": "/catalogs/LATEST/catalog-01"
            }
        self.assertDictEqual(expected_response, generate_response_json(catalog_record, "LATEST"))

    # - generate_response_json - failed
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_failure(self, mock_required_props,
                                            mock_optional_props):
        mock_required_props.return_value = ['catalog-id', 'catalog-timestamp', 'owner-id',
                                            'catalog-url']
        mock_optional_props.return_value = ['description']

        catalog_record = \
            {
                "owner-id": 101,
                "catalog-id": "catalog-01",
                "description": "This is a sample"

            }
        with self.assertRaisesRegex(InternalError, "Missing required property: catalog-timestamp"):
            generate_response_json(catalog_record, "LATEST")


if __name__ == '__main__':
    unittest.main()
