import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import describe_catalogs_command

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'DescribeCatalogsCommand')


class TestDescribeCatalogs(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + test_lambda_handler: With owner-id, no pagination or max items
    @patch.dict(os.environ, {'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table',
                             'CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE': 'fake_catalog_collection_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success_without_pagination(self,
                                                       mock_session,
                                                       mock_dynamo_client):
        reload(describe_catalogs_command)
        mock_session.return_value = None

        def mock_query_items(TableName, PaginationConfig=None, IndexName=None, **kwargs):  # NOSONAR
            data = None

            class paginate_object():
                @staticmethod
                def build_full_result():
                    return data

            if TableName == 'fake_catalog_table':  # collection table: get item
                data = io_util.load_data_json('dynamodb.catalog.query_items_with_cats.json')
                return paginate_object
            elif TableName == 'fake_catalog_collection_table':
                data = io_util.load_data_json('dynamodb.catalog_collection_mapping.query_items.json')
                return paginate_object
            else:
                return None

        mock_dynamo_client.return_value.get_paginator.return_value.paginate = mock_query_items
        request_input = io_util.load_data_json('apigateway.request_valid.json')
        response_output = io_util.load_data_json('apigateway.response_valid_with_cats.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(describe_catalogs_command.lambda_handler(request_input,
                                                                          mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + test_lambda_handler: With owner-id, no pagination or max items
    @patch.dict(os.environ, {'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success_without_pag(self,
                                                mock_session,
                                                mock_dynamo_client):
        reload(describe_catalogs_command)
        mock_session.return_value = None
        mock_dynamo_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.catalog.query_items.json')
        request_input = io_util.load_data_json('apigateway.request_valid.json')
        response_output = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(describe_catalogs_command.lambda_handler(request_input,
                                                                          mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + test_lambda_handler: With pagination and max items
    @patch.dict(os.environ, {'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success_with_pag(self,
                                             mock_session,
                                             mock_dynamo_client):
        reload(describe_catalogs_command)
        mock_session.return_value = None
        mock_dynamo_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.catalog.scan_with_page.json')
        request_input = io_util.load_data_json('apigateway.request_valid_with_page.json')
        response_output = io_util.load_data_json('apigateway.response_valid_with_page.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(describe_catalogs_command.lambda_handler(request_input,
                                                                          mock_lambda_context.MockLambdaContext()),
                                 response_output)
