import os
import unittest
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from remove_catalog_event_handler import lambda_handler

__author__ = "Prashant S"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveCatalogEventHandler')


class TestRemoveCatalogEventHandler(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    @patch.dict(os.environ, {'CATALOG_DYNAMODB_TABLE': 'fake_subscription_table'},
                {'CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE': 'face_catalog_collection_mapping_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self, mock_session,
                            mock_dynamodb_client):
        mock_session.return_value = None
        mock_dynamodb_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.put_item.response.json')
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('lng.dynamodb.tracking_table.respond.json')
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('lng.dynamodb.tracking_table.respond.json'),
            io_util.load_data_json('lng.dynamodb.mapping_table.respond.json')
        ]
        mock_dynamodb_client.return_value.delete_item.return_value = None
        request_input = io_util.load_data_json("valid_event.json")

        self.assertEqual(event_handler_status.SUCCESS, lambda_handler(request_input, MockLambdaContext()))


if __name__ == '__main__':
    unittest.main()
