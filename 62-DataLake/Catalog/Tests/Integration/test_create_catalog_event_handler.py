import os
import unittest
from importlib import reload
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_catalog_event_handler

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_utils = IOUtils(__file__, "CreateCatalogEventHandler")


class TestCreateCatalogEventHandler(TestCase):

    @classmethod
    @patch.dict(os.environ, {
        'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic',
        'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table'})
    def setUpClass(cls):  # NOSONAR
        reload(create_catalog_event_handler)

    @classmethod
    @patch.dict(os.environ, {'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN': ''})
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +lambda_handler- happy path
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.sns.get_client')
    def test_lambda_handler(self, mock_sns, mock_aws_session, mock_get_client, mock_helper):
        aws_response_200 = io_utils.load_data_json('successful_publish_response.json')
        mock_get_client.return_value.put_item.return_value = aws_response_200
        mock_sns.return_value.publish.return_value = aws_response_200
        mock_helper.return_value = ['us-east-1']
        mock_aws_session.return_value = None
        request_input = io_utils.load_data_json("valid_event.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         create_catalog_event_handler.lambda_handler(request_input, MockLambdaContext()))


if __name__ == "__main__":
    unittest.main()
