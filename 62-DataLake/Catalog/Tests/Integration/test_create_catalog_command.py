import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_catalog_command import lambda_handler

__author__ = "Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateCatalogCommand')


class TestCreateCatalogCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # +Test for Lambda Handler no catalog id specified
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_catalog_command_handler_no_catalog_id(self,
                                                          session_mock,
                                                          mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamo.owner_data_valid.json')

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = io_util.load_data_json(
            'dynamo.owner_data_valid.json')
        mock_dynamodb_get_client.return_value.update_item.return_value = {"Attributes": {"AtomicCounter": {"N": 324}}}
        mock_dynamodb_get_client.return_value.put_item.return_value = None

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.input_valid_1.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_1.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)

    # +Test for Lambda Handler with catalog id specified
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table'})
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_create_catalog_command_handler_catalog_id_specified(self,
                                                                 session_mock,
                                                                 mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamo.owner_data_valid.json')
            elif TableName == 'fake_catalog_table':
                return io_util.load_data_json('dynamo.catalog_data_valid.json')

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.return_value = io_util.load_data_json(
            'dynamo.owner_data_valid.json')
        mock_dynamodb_get_client.return_value.update_item.return_value = {"Attributes": {"AtomicCounter": {"N": 324}}}
        mock_dynamodb_get_client.return_value.put_item.return_value = None

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway.input_valid_2.json')
        expected_response = io_util.load_data_json('apigateway.expected_response_2.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)


if __name__ == '__main__':
    unittest.main()
