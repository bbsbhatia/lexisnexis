import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from update_catalog_command import lambda_handler

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.1"

io_util = IOUtils(__file__, 'UpdateCatalogCommand')


class TestUpdateCatalogCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + Test for Lambda Handler
    @patch.dict(os.environ, {'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
                             'CATALOG_DYNAMODB_TABLE': 'fake_catalog_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table'
                             })
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_update_catalog_command(self,
                                    session_mock,
                                    mock_dynamodb_get_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.GetItem_2.json')
            if TableName == 'fake_catalog_table':
                return io_util.load_data_json('dynamodb.GetItem_1.json')
            return None

        session_mock.return_value = None
        # Get from Owner and catalog
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        # Put in tracking table and event store table
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.PutItem_1.json')
        mock_dynamodb_get_client.return_value.batch_get_item.return_value = io_util.load_data_json(
            'dynamodb.BatchGetItem_1.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_data = io_util.load_data_json('apigateway_request.json')
        expected_response = io_util.load_data_json('apigateway_response.json')
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertEqual(lambda_handler(input_data, MockLambdaContext), expected_response)


if __name__ == '__main__':
    unittest.main()
