import os
import unittest
from datetime import datetime

from lng_datalake_client.Catalog.update_catalog import update_catalog

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestUpdateCatalog(unittest.TestCase):
    default_catalog_id = 'catalog-01'
    default_description = 'regression_test_description_{}'.format(datetime.now())
    default_add_collection_ids = ["01", "02", "03"]
    default_remove_collection_ids = ["04", "05"]
    default_invalid_catalog_id = "non-existent-catalog-ABC-987"
    default_invalid_collection_ids = ["collection-ABC-not-exist", "collection-DEF-not-exist"]
    # catalog description maximum length is 256
    default_invalid_description_length = 'A' * 257
    # maximum number of collection ids to update a catalog is 1000
    default_invalid_number_collection_ids = [str(i) for i in range(1001)]

    default_patch_document = [
        {"op": "replace", "path": "/description", "value": default_description},
        {"op": "add", "path": "/collection-ids", "value": default_add_collection_ids},
        {"op": "remove", "path": "/collection-ids", "value": default_remove_collection_ids}]

    # + update catalog - status code 202
    def test_update_catalog_success(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        catalog_id = input_dict.get("catalog-id", self.default_catalog_id)
        patch_document = input_dict.get('patch-operations', self.default_patch_document)

        stage = os.environ['STAGE']

        request_dict = \
            {
                "body": {
                    "patch-operations": patch_document
                },
                "catalog-id": catalog_id
            }

        expected_response = \
            {
                "catalog-id": catalog_id,
                "catalog-url": "/catalogs/{0}/{1}".format(stage, catalog_id),
                "owner-id": input_dict.get('owner-id',
                                           int(os.environ['OWNER-ID']))
            }
        # get the description from the patch document if exist
        for operation in patch_document:
            if operation['path'] == "/description":
                expected_response["description"] = operation['value']
                break

        response = update_catalog(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()

        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("catalog", response_dict)
        self.assertIn("catalog-timestamp", response_dict["catalog"])
        expected_response['catalog-timestamp'] = response_dict['catalog']['catalog-timestamp']

        if 'description' not in expected_response and 'description' in response_dict['catalog']:
            expected_response["description"] = response_dict['catalog']['description']

        # test the response body
        self.assertDictEqual(expected_response, response_dict['catalog'])
        return response_dict['catalog']

    # - update catalog with invalid catalog id - status code 404
    def test_update_catalog_invalid_catalog_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        catalog_id = input_dict.get("catalog-id", self.default_invalid_catalog_id)

        request_dict = \
            {
                "body": {
                    "patch-operations": input_dict.get('patch-operations', self.default_patch_document)
                },
                "catalog-id": catalog_id
            }

        response = update_catalog(request_dict)

        self.assertEqual(404, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCatalog")
        self.assertEqual(json_error_resp['message'], "Invalid Catalog ID {}".format(catalog_id))
        self.assertEqual(json_error_resp['corrective-action'], "Catalog ID does not exist")

    # - update catalog with invalid collections  - 400
    def test_update_catalog_invalid_collection_ids(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        catalog_id = input_dict.get("catalog-id", self.default_catalog_id)
        default_invalid_patch_document = [
            {"op": "add", "path": "/collection-ids", "value": self.default_invalid_collection_ids}
        ]
        expected_invalid_collection_ids = input_dict.get('invalid_collection-ids', self.default_invalid_collection_ids)

        request_dict = \
            {
                "body": {
                    "patch-operations": input_dict.get('patch-operations', default_invalid_patch_document)
                },
                "catalog-id": catalog_id
            }

        response = update_catalog(request_dict)

        self.assertEqual(404, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['corrective-action'], "Collections must exist")
        # assert that the number of invalid collections ids are equal in both the expected message and received message
        # TODO: Rework this logic to validate properly
        self.assertEqual(1, len(json_error_resp['message'].split(',')))

        # check that all the invalid ids are in the error message
        # TODO: Rework this logic to validate properly
        self.assertRegex(json_error_resp['message'], '\'' + expected_invalid_collection_ids[0] + '\'')


    # TODO: Add test for trying to add a suspended collection

    # - update catalog with invalid catalog description length  - 400
    def test_update_catalog_invalid_description_length(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        catalog_id = input_dict.get("catalog-id", self.default_catalog_id)
        default_invalid_patch_document = [
            {"op": "replace", "path": "/description", "value": self.default_invalid_description_length}
        ]

        request_dict = \
            {
                "body": {
                    "patch-operations": input_dict.get('patch-operations', default_invalid_patch_document)
                },
                "catalog-id": catalog_id
            }

        response = update_catalog(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertRegex(json_error_resp['message'], "Invalid Catalog Description")
        self.assertEqual(json_error_resp['corrective-action'],
                         "Catalog Description length should not exceed 256 characters")

    # - update catalog with invalid number of collection ids - 400
    def test_update_catalog_invalid_number_collection_ids(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        catalog_id = input_dict.get("catalog-id", self.default_catalog_id)
        default_invalid_patch_document = [
            {"op": "add", "path": "/collection-ids", "value": self.default_invalid_number_collection_ids}
        ]

        request_dict = \
            {
                "body": {
                    "patch-operations": input_dict.get('patch-operations', default_invalid_patch_document)
                },
                "catalog-id": catalog_id
            }

        response = update_catalog(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertRegex(json_error_resp['message'], "Invalid number of Collection IDs")
        self.assertEqual(json_error_resp['corrective-action'], "Number of Collection IDs should not exceed 1000")

    # - update catalog with invalid x-api-key - status code 403
    def test_update_catalog_invalid_x_api_key(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "body": {
                "patch-operations": input_dict.get('patch-operations', self.default_patch_document)
            },
            "catalog-id": input_dict.get("catalog-id", self.default_catalog_id)
        }

        response = update_catalog(request_dict)

        self.assertEqual(403, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - update catalog with the wrong x-api-key - status code 403
    def test_update_catalog_incorrect_x_api_key(self, input_dict=None, api_key=None):
        if not input_dict:
            input_dict = {}
        if not api_key:
            # api key should be valid and different to the api key of the catalog owner
            # if the wrong api key is used, then the test will not pass because the error type will be INVALID_API_KEY
            return

        owner_id = input_dict.get("owner-id", int(os.environ['OWNER-ID']))
        request_dict = {
            'headers': {
                'Content-type': 'application/json',
                'x-api-key': api_key
            },
            "body": {
                "patch-operations": input_dict.get('patch-operations', self.default_patch_document)
            },
            "catalog-id": input_dict.get("catalog-id", self.default_catalog_id)
        }
        response = update_catalog(request_dict)

        self.assertEqual(403, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")

    # - update catalog with invalid content-type - status code 415
    def test_update_catalog_invalid_content_type(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "body": {
                "patch-operations": input_dict.get('patch-operations', self.default_patch_document)
            },
            "catalog-id": input_dict.get("catalog-id", self.default_catalog_id)
        }

        response = update_catalog(request_dict)

        self.assertEqual(415, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
