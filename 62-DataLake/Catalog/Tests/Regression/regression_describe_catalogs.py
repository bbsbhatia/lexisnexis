import os
import unittest

from lng_datalake_client.Catalog.describe_catalogs import describe_catalogs
from lng_datalake_testhelper.io_utils import IOUtils

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__)


class TestDescribeCatalogs(unittest.TestCase):

    # + Test the 200 status code and catalog properties for describe catalogs without an owner-id

    def test_describe_catalogs_all_results(self):
        response = describe_catalogs({})

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("catalogs", response_dict)

        # test the response body
        self.assertGreater(len(response_dict["catalogs"]), 0)
        self.assertTrue(isinstance(response_dict["item-count"], int))
        return response.json()["catalogs"]

        # + Test describe catalogs with pagination

    def test_describe_catalogs_pagination(self):
        request_input = {"max-items": 1}
        response_1 = describe_catalogs(request_input)
        self.assertEqual(200, response_1.status_code)
        response_dict_1 = response_1.json()
        self.assertIn("context", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ["STAGE"],
                         response_dict_1["context"]["stage"])
        self.assertIn("catalogs", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["item-count"], int))
        self.assertEqual(request_input["max-items"], len(response_dict_1['catalogs']))

        self.assertTrue(isinstance(response_dict_1["next-token"], str))

        # make a request again with the pagination token ------
        request_input["next-token"] = response_dict_1['next-token']
        response_2 = describe_catalogs(request_input)
        self.assertEqual(200, response_2.status_code)
        response_dict_2 = response_2.json()
        self.assertIn("context", response_dict_2)
        self.assertTrue(isinstance(response_dict_2["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ["STAGE"],
                         response_dict_2["context"]["stage"])
        self.assertIn("catalogs", response_dict_2)
        self.assertEqual(request_input["max-items"], len(response_dict_2['catalogs']))
        self.assertTrue(isinstance(response_dict_2["item-count"], int))

        self.assertNotEqual(response_dict_1["catalogs"], response_dict_2["catalogs"])

    # + Test the 200 status code and catalog properties for describe catalogs with an owner-id
    def test_describe_catalogs_by_owner_id(self,
                                           describe_catalogs_data=io_util.load_data_json("describe_catalogs_data.json"),
                                           owner_id=None):
        if owner_id is None:
            owner_id = int(os.environ['OWNER-ID'])
        response = describe_catalogs({"owner-id": owner_id})

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("catalogs", response_dict)

        response_catalogs = response_dict["catalogs"]
        response_catalogs.sort(key=lambda x: x['catalog-timestamp'])
        for cat in response_catalogs:
            cat['collection-ids'] = sorted(cat['collection-ids'])
        self.assertEqual(describe_catalogs_data, response_catalogs)
        self.assertTrue(isinstance(response_dict["item-count"], int))

        return response.json()["catalogs"]

    # + Test the 200 status code and catalog properties for describe catalogs with an catalog-id
    def test_describe_catalogs_by_catalog_id(self,
                                             describe_catalogs_data=io_util.load_data_json(
                                                 "describe_catalogs_by_id_data.json"),
                                             catalog_id=None):
        if catalog_id is None:
            catalog_id = int(os.environ['OWNER-ID'])
        response = describe_catalogs({"catalog-id": catalog_id})

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("catalogs", response_dict)

        response_catalogs = response_dict["catalogs"]
        response_catalogs.sort(key=lambda x: x['catalog-timestamp'])
        for cat in response_catalogs:
            cat['collection-ids'] = sorted(cat['collection-ids'])
        self.assertEqual(describe_catalogs_data, response_catalogs)
        self.assertTrue(isinstance(response_dict["item-count"], int))

        return response.json()["catalogs"]

    # + describe_catalogs with invalid owner id
    def test_describe_catalogs_invalid_owner_id(self):
        request_dict = {
            "owner-id": -1
        }
        response = describe_catalogs(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("catalogs", response_dict)

        # test the response body
        self.assertEqual(len(response_dict["catalogs"]), 0)
        self.assertEqual(response_dict["item-count"], 0)
        return response.json()["catalogs"]

        # - describe_catalogs changing max items

    def test_describe_catalogs_changing_max_items(self):
        response = describe_catalogs({'max-items': 1})
        # Make sure the initial response was successful and has a next-token
        self.assertEqual(200, response.status_code)
        self.assertIn('next-token', response.json())
        # Change the max-items amount
        response = describe_catalogs({'max-items': 2, 'next-token': response.json()['next-token']})

        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Continuation request must pass the same max-items as initial request")
        self.assertEqual(json_error_resp['corrective-action'], "Set max-items to 1 and try again")

    # - describe_catalogs with invalid x-api-key
    def test_describe_catalogs_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "owner-id": 2
        }

        response = describe_catalogs(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - describe_catalogs with invalid content-type
    def test_describe_catalogs_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "catalog-id": "catalog-01"
        }
        response = describe_catalogs(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
