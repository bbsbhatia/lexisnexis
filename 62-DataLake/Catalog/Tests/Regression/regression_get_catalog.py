import os
import unittest

from lng_datalake_client.Catalog.get_catalog import get_catalog

__author__ = "Prashant Srivastava , Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestGetCatalog(unittest.TestCase):
    default_catalog_id = "catalog-01"
    default_owner_id = 104
    default_description = 'My first catalog test'

    # + Test the 200 status code and catalog properties for GET_CATALOG
    def test_get_catalog(self, catalog_data=None):
        if not catalog_data:
            catalog_data = {}
        catalog_id = catalog_data.get('catalog-id', self.default_catalog_id)

        request_dict = \
            {
                "catalog-id": catalog_id
            }

        expected_response = \
            {
                'owner-id': catalog_data.get('owner-id',
                                             int(os.environ['OWNER-ID'])),
                'catalog-id': catalog_id,
                'catalog-url': '/catalogs/{0}/{1}'.format(os.environ['STAGE'],
                                                          catalog_id)
            }

        # Add any additional attributes to validate against the catalog
        for key, value in catalog_data.items():
            if key not in expected_response:
                expected_response[key] = value

        response = get_catalog(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("catalog", response_dict)
        expected_response['catalog-timestamp'] = response_dict['catalog'].get('catalog-timestamp')

        # test the response body
        self.assertDictEqual(expected_response, response_dict["catalog"])
        return response.json()["catalog"]

    # - get_catalog with invalid catalog id
    def test_get_catalog_invalid_catalog_id(self, catalog_data=None):
        if not catalog_data:
            catalog_data = {}

        invalid_catalog_id = catalog_data.get('catalog-id', 'regression-abcdefghijklmnopqrstuvwxyz')
        request_dict = {
            "catalog-id": invalid_catalog_id
        }
        response = get_catalog(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCatalog")
        self.assertEqual(json_error_resp['message'], "Invalid Catalog ID {}".format(invalid_catalog_id))
        self.assertEqual(json_error_resp['corrective-action'], "Catalog ID does not exist")

    # - get_catalog with invalid x-api-key
    def test_get_catalog_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "catalog-id": '1'
        }

        response = get_catalog(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - get_catalog with invalid content-type
    def test_get_catalog_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "catalog-id": "catalog-01"
        }

        response = get_catalog(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
