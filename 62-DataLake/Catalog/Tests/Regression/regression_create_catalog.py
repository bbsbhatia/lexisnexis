import os
import unittest

from lng_datalake_client.Catalog.create_catalog import create_catalog

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestCreateCatalog(unittest.TestCase):
    default_catalog_id_invalid_length = os.getenv("INVALID_LENGTH_CATALOG_ID",
                                                  "invalid_catalog_id_with_length_greater_than_40")
    default_catalog_id_invalid_characters = os.getenv("INVALID_CHARACTERS_CATALOG_ID", "_09.:4y")
    default_catalog_id_invalid_all_digits = os.getenv("INVALID_DIGITS_CATALOG_ID", "999")
    default_duplicated_catalog_id = os.getenv("DUPLICATE_DIGITS_CATALOG_ID", "Ab-1")
    default_catalog_description = os.getenv("CATALOG_DESCRIPTION", "catalog description")

    # + creation of catalog - status code 200 - description in query param
    def test_create_catalog_valid_input_description_query(self, input_dict=None):
        if not input_dict:
            input_dict = {
                "description": self.default_catalog_description
            }
        stage = os.environ['STAGE']

        # create catalog doesn't have required properties
        request_dict = {}

        expected_response = \
            {
                "owner-id": int(os.environ['OWNER-ID'])
            }

        # Add any additional attributes to create the catalog
        for key, value in input_dict.items():
            expected_response[key] = value
            request_dict[key] = value

        response = create_catalog(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("catalog", response_dict)
        self.assertIn("catalog-timestamp", response_dict["catalog"])
        expected_response['catalog-timestamp'] = response_dict['catalog']['catalog-timestamp']
        if 'catalog-id' not in input_dict.keys():
            expected_response['catalog-id'] = response_dict['catalog'].get('catalog-id')
        expected_response['catalog-url'] = '/catalogs/{0}/{1}'.format(stage,
                                                                      expected_response.get('catalog-id'))
        self.assertDictEqual(expected_response, response_dict['catalog'])
        return response_dict['catalog']

    # + creation of catalog - status code 200 - description in body
    # This will test backwards compatibility of description being passed in the body. This can be removed
    # once all the clients have been moved to use description in query param
    def test_create_catalog_valid_input_description_body(self, input_dict=None):
        if not input_dict:
            input_dict = {
                "description": self.default_catalog_description
            }
        stage = os.environ['STAGE']

        # create catalog doesn't have required properties
        request_dict = {}

        expected_response = \
            {
                "owner-id": int(os.environ['OWNER-ID'])
            }

        # Add any additional attributes to create the catalog
        for key, value in input_dict.items():
            expected_response[key] = value
            if key == 'catalog-id':
                request_dict[key] = value
            else:
                if "body" not in request_dict:
                    request_dict['body'] = {}
                request_dict['body'][key] = value

        response = create_catalog(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("catalog", response_dict)
        self.assertIn("catalog-timestamp", response_dict["catalog"])
        expected_response['catalog-timestamp'] = response_dict['catalog']['catalog-timestamp']
        if 'catalog-id' not in input_dict.keys():
            expected_response['catalog-id'] = response_dict['catalog'].get('catalog-id')
        expected_response['catalog-url'] = '/catalogs/{0}/{1}'.format(stage,
                                                                      expected_response.get('catalog-id'))
        self.assertDictEqual(expected_response, response_dict['catalog'])
        return response_dict['catalog']

    # + creation of catalog - status code 200 - description in body and query param
    # This will test description being passed both in the body and query param and verifies that we store description
    # from query param. This can be removed once all the clients have been moved to use description in query param
    def test_create_catalog_valid_input_description_body_query(self, input_dict=None):
        if not input_dict:
            input_dict = {
                "description": self.default_catalog_description
            }
        stage = os.environ['STAGE']

        # create catalog doesn't have required properties
        request_dict = {}

        expected_response = \
            {
                "owner-id": int(os.environ['OWNER-ID'])
            }

        # Add any additional attributes to create the catalog
        for key, value in input_dict.items():

            request_dict[key] = value
            if key == 'description':
                request_dict[key] = "{0} {1}".format(value, "query param")
                expected_response[key] = "{0} {1}".format(value, "query param")
                if "body" not in request_dict:
                    request_dict['body'] = {}
                request_dict['body'][key] = "{0} {1}".format(value, "body")
            else:
                request_dict[key] = value
                expected_response[key] = value

        response = create_catalog(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("catalog", response_dict)
        self.assertIn("catalog-timestamp", response_dict["catalog"])
        expected_response['catalog-timestamp'] = response_dict['catalog']['catalog-timestamp']
        if 'catalog-id' not in input_dict.keys():
            expected_response['catalog-id'] = response_dict['catalog'].get('catalog-id')
        expected_response['catalog-url'] = '/catalogs/{0}/{1}'.format(stage,
                                                                      expected_response.get('catalog-id'))
        self.assertDictEqual(expected_response, response_dict['catalog'])
        return response_dict['catalog']

    # - create catalog with invalid catalog-id length - status code 400
    def test_create_catalog_id_invalid_length(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "catalog-id": input_dict.get('catalog-id', self.default_catalog_id_invalid_length)
        }

        response = create_catalog(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Catalog ID {}".format(request_dict['catalog-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Catalog ID length should not exceed 40 characters")

    # - disallowed characters in catalog-id - status code 400
    def test_create_catalog_id_invalid_character(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "catalog-id": input_dict.get('catalog-id', self.default_catalog_id_invalid_characters)
        }

        response = create_catalog(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Catalog ID {}".format(request_dict['catalog-id']))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Catalog ID can only contain letters, digits and hyphens")

    # - invalid catalog-id contains only digits - status code 400
    def test_create_catalog_id_invalid_only_digits(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "catalog-id": input_dict.get('catalog-id', self.default_catalog_id_invalid_all_digits)
        }

        response = create_catalog(request_dict)

        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Catalog ID {}".format(request_dict['catalog-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Catalog ID cannot contain only digits")

    # - create catalog with invalid x-api-key - status code 403
    def test_create_catalog_invalid_x_api_key(self):
        request_dict = {
            'headers': {
                'Content-type': 'application/json'
            }
        }

        response = create_catalog(request_dict)

        self.assertEqual(403, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - create catalog with invalid content-type - status code 415
    def test_create_catalog_unsupported_media(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            'headers': {
                'Content-type': 'application/xml',
                'x-api-key': key
            }
        }

        response = create_catalog(request_dict)

        self.assertEqual(415, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - create catalog with duplicated catalog-id - status code 409
    def test_create_catalog_duplicated_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "catalog-id": input_dict.get('catalog-id', self.default_duplicated_catalog_id)
        }

        response = create_catalog(request_dict)

        self.assertEqual(409, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "CatalogAlreadyExists")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Catalog ID {}".format(request_dict['catalog-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Catalog ID already exists")


if __name__ == '__main__':
    unittest.main()
