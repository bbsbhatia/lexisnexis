import os
import unittest

from lng_datalake_client.Catalog.remove_catalog import remove_catalog

__author__ = "Prashant"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestRemoveCatalog(unittest.TestCase):
    default_catalog_id = '1'

    # + Test the 202 status code and response for remove_catalog
    def test_remove_catalog(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        catalog_id = input_dict.get("catalog-id", self.default_catalog_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "catalog-id": catalog_id
            }

        expected_response = \
            {
                "catalog-id": catalog_id,
                "catalog-url": "/catalogs/{0}/{1}".format(stage, catalog_id),
            }
        # Add any additional attributes to validate against the catalog
        for key, value in input_dict.items():
            expected_response[key] = value

        response = remove_catalog(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time-epoch"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn("catalog", response_dict)
        self.assertIn("catalog-timestamp", response_dict["catalog"])
        catalog_timestamp = response_dict["catalog"]["catalog-timestamp"]
        self.assertTrue(isinstance(catalog_timestamp, str))
        expected_response['catalog-timestamp'] = catalog_timestamp

        # test the response body
        self.assertDictEqual(expected_response, response_dict['catalog'])
        return response_dict['catalog']

    # - remove_catalog with invalid catalog id --404
    def test_remove_catalog_invalid_catalog_id(self):

        # must use a catalog-id we know doesn't exist (still has to contain valid chars or we get another error)
        invalid_catalog_id = "regression-abcdefghijklmnopqrstuvwxyz"
        request_dict = {
            "catalog-id": invalid_catalog_id
        }

        response = remove_catalog(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCatalog")
        self.assertEqual(json_error_resp['message'], "Invalid Catalog ID {}".format(invalid_catalog_id))
        self.assertEqual(json_error_resp['corrective-action'], "Catalog ID does not exist")

    # - remove_catalog with invalid x-api-key -- 403
    def test_remove_catalog_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "catalog-id": self.default_catalog_id
        }

        response = remove_catalog(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - wrong x-api-key, auth error
    def test_remove_catalog_incorrect_x_api_key(self, input_dict=None, api_key=None):
        if not api_key:
            api_key = "some-key"
        owner_id = input_dict.get("owner-id", int(os.environ['OWNER-ID']))
        request_dict = {
            'headers': {
                'Content-type': 'application/json',
                'x-api-key': api_key
            },
            "catalog-id": input_dict.get("catalog-id", self.default_catalog_id)
        }
        response = remove_catalog(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")

    # - remove_catalog with invalid content-type -- 415
    def test_remove_catalog_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "catalog-id": self.default_catalog_id
        }

        response = remove_catalog(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
