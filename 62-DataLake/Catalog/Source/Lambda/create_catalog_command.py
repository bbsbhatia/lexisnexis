import logging
import os
import re

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import owner_authorization, command_wrapper
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, CatalogAlreadyExists, \
    UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names
from lng_datalake_dal.counter_table import CounterTable

from service_commons import catalog_command

__author__ = "Jose Molinet, Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/CreateCatalogCommand-RequestSchema.json',
                         'Schemas/CreateCatalogCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return create_catalog_command(event['request'],
                                  event['context']['client-request-id'],
                                  event['context']['stage'],
                                  event['context']['api-key-id'])


def create_catalog_command(request, request_id, stage, api_key_id):
    # optional properties
    catalog_id = request.get('catalog-id')

    # Get OwnerID from authorization key
    owner_id = owner_authorization.get_owner_by_api_key_id(api_key_id)['owner-id']

    # Validate catalog-id if specified
    if catalog_id:
        validate_catalog_id(catalog_id)
    # generate catalog-id if one wasn't specified in request
    else:
        try:
            catalog_id = str(CounterTable().update_counter("catalog"))
        except ClientError as e:
            logger.error(e)
            raise InternalError("Unable to update Counter Table", e)
        except Exception as ex:
            logger.error(ex)
            raise UnhandledException(ex)

    response_dict = generate_response_json(request, catalog_id, stage, owner_id)

    event_dict = {
        'event-id': request_id,
        'request-time': command_wrapper.get_request_time(),
        'event-name': event_names.CATALOG_CREATE,
        'event-version': event_version,
        'stage': stage,
        'catalog-id': response_dict['catalog-id'],
        'owner-id': response_dict['owner-id']
    }
    if 'description' in response_dict:
        event_dict['description'] = response_dict['description']

    return {'response-dict': response_dict, 'event-dict': event_dict}


def validate_catalog_id(catalog_id):
    if len(catalog_id) > 40:
        raise InvalidRequestPropertyValue("Invalid Catalog ID {0}".format(catalog_id),
                                          "Catalog ID length should not exceed 40 characters")
    elif not re.match("^[A-Za-z0-9-]*$", catalog_id):
        raise InvalidRequestPropertyValue("Invalid Catalog ID {0}".format(catalog_id),
                                          "Catalog ID can only contain letters, digits and hyphens")
    elif catalog_id.isdigit():
        raise InvalidRequestPropertyValue("Invalid Catalog ID {0}".format(catalog_id),
                                          "Catalog ID cannot contain only digits")
    elif catalog_command.get_catalog_by_id(catalog_id):
        raise CatalogAlreadyExists("Invalid Catalog ID {0}".format(catalog_id),
                                   "Catalog ID already exists")


def generate_response_json(request, catalog_id, stage, owner_id):
    response_dict = {}

    required_props_defaults = \
        {
            "catalog-id": catalog_id,
            "catalog-url": "/catalogs/{0}/{1}".format(stage, catalog_id),
            "owner-id": owner_id,
            "catalog-timestamp": command_wrapper.get_request_time()
        }

    required_props = command_wrapper.get_required_response_schema_keys('catalog-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('catalog-properties')

    for prop in required_props:
        if prop in request:
            response_dict[prop] = request[prop]
        elif prop in required_props_defaults:
            response_dict[prop] = required_props_defaults[prop]
        else:
            raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in request:
            response_dict[prop] = request[prop]

    return response_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    from datetime import datetime

    # needed since we don't define a handler and the default is to log at the WARNING level
    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE CATALOG COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-mas-DataLake-CatalogTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-mas-DataLake-OwnerTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-mas-DataLake-EventStoreTable'
    os.environ['COUNTER_DYNAMODB_TABLE'] = 'feature-mas-DataLake-CounterTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "POST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "catalog-id": "catalog-04",
                "description": "Description of my first Catalog"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE CATALOG COMMAND LAMBDA]")
