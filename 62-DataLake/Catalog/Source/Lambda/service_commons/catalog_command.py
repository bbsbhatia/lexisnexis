import logging
import os

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, UnhandledException
from lng_datalake_dal.catalog_collection_mapping_table import CatalogCollectionMappingTable
from lng_datalake_dal.catalog_table import CatalogTable

__author__ = "Jose Molinet, Samuel Jackson Sanders"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


def get_catalog_by_id(catalog_id: str) -> dict:
    """
    Get the catalog item
    :param catalog_id: the id of the catalog to retrieve
    :return: the catalog item if exist
    """
    try:
        return CatalogTable().get_item({'catalog-id': catalog_id})
    except ClientError as e:
        logger.error(e)
        raise InternalError("Unable to get item from Catalog Table", e)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)


def get_catalog_list(owner_id: int, max_items: int, pagination_token: str) -> list:
    if owner_id:
        try:
            catalog_list = CatalogTable().query_items(index="catalog-by-owner-id-index",
                                                      max_items=max_items,
                                                      pagination_token=pagination_token,
                                                      KeyConditionExpression="OwnerID=:owner_id",
                                                      ExpressionAttributeValues={":owner_id": {"N": str(owner_id)}})
        except ClientError as e1:
            logger.error(e1)
            raise InternalError(
                "Unable to query items from Catalog Table index {0}".format("catalog-by-owner-id-index"), e1)
        except Exception as e2:
            logger.error(e2)
            raise UnhandledException(e2)

    else:  # return all catalogs
        try:
            catalog_list = CatalogTable().get_all_items(max_items=max_items,
                                                        pagination_token=pagination_token)
        except ClientError as e1:
            logger.error(e1)
            raise InternalError("Unable to get items from Catalog Table", e1)
        except Exception as e2:
            logger.error(e2)
            raise UnhandledException(e2)

    return catalog_list


def get_catalog_collection_ids(catalog_id: str, max_items: int) -> tuple:
    try:
        return CatalogCollectionMappingTable().query_items(
            max_items=max_items,
            index="collection-by-catalog-id-index",
            KeyConditionExpression='CatalogID=:catalog_id',
            ExpressionAttributeValues={":catalog_id": {"S": catalog_id}})

    except ClientError as error:
        logger.error(error)
        raise InternalError("Unable to query items from Catalog Collection Mapping Table"
                            " index {0}".format("collection-by-catalog-id-index"), error)

    except Exception as error:
        logger.error(error)
        raise UnhandledException(error)
