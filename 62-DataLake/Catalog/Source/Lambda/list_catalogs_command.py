import logging
import os

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper, paginator_token, command_validator
from lng_datalake_commands.exceptions import InternalError
from lng_datalake_commons import session_decorator
from lng_datalake_dal.catalog_table import CatalogTable

from service_commons import catalog_command

__author__ = "Prashant S"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 1000))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/ListCatalogsCommand-RequestSchema.json',
                         'Schemas/ListCatalogsCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return list_catalogs_command(event['request'], event['context']['stage'])


def list_catalogs_command(request, stage):
    # optional properties
    max_items = request.get('max-items', DEFAULT_MAX_ITEMS)
    next_token = request.get('next-token')
    owner_id = request.get('owner-id')

    pagination_token = None
    # If we have a next-token validate the max-items matches the original request
    # Dyanmo doesn't handle pagination if the max-items changes
    if next_token:
        tokens = paginator_token.unpack_validate_pagination_token(next_token, max_items)
        pagination_token = tokens['pagination-token']

    # sanity check for max-items (request schema should prevent)
    command_validator.validate_max_results(max_items, DEFAULT_MAX_ITEMS)

    catalog_list = catalog_command.get_catalog_list(owner_id, max_items, pagination_token)

    return {
        "response-dict": generate_response_json(catalog_list, CatalogTable().get_pagination_token(), stage, max_items)}


def generate_response_json(catalog_list: list, pagination_token: str, stage: str, max_items: int) -> dict:
    catalog_response = []
    for catalog_data in catalog_list:
        required_props_defaults = \
            {
                "catalog-url": "/catalogs/{0}/{1}".format(stage, catalog_data['catalog-id'])
            }

        required_props = command_wrapper.get_required_response_schema_keys('catalogs-n-pagination', 'properties',
                                                                           'catalogs', 'items')
        optional_props = command_wrapper.get_optional_response_schema_keys('catalogs-n-pagination', 'properties',
                                                                           'catalogs', 'items')

        response_item = {}

        for prop in required_props:
            if prop in catalog_data:
                response_item[prop] = catalog_data[prop]
            elif prop in required_props_defaults:
                response_item[prop] = required_props_defaults[prop]
            else:
                raise InternalError("Missing required property: {0}".format(prop))

        for prop in optional_props:
            if prop in catalog_data:
                response_item[prop] = catalog_data[prop]

        catalog_response.append(response_item)

    response = {"catalogs": catalog_response}

    if pagination_token:
        response['next-token'] = paginator_token.generate_pagination_token(max_items, pagination_token)

    response['item-count'] = len(catalog_list)

    return response


if __name__ == '__main__':
    import json
    from datetime import datetime
    from lng_datalake_testhelper import mock_lambda_context

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-djh-DataLake-CatalogTable'

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - LIST CATALOGS COMMAND LAMBDA]")

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                # "owner-id": 104,
                # "max-items": 1
                # "next-token": "1|eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ2F0YWxvZ0lEIjogeyJTIjogImNhdGFsb2ctMDEifX19"
            }

        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    lambda_response = lambda_handler(lambda_event, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - LIST CATALOGS COMMAND LAMBDA]")
