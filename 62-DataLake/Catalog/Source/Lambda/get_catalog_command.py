import logging
import os

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NoSuchCatalog
from lng_datalake_commons import session_decorator

from service_commons import catalog_command

__author__ = "Prashant Srivastava , Maen nanaa"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/GetCatalogCommand-RequestSchema.json',
                         'Schemas/GetCatalogCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return get_catalog_command(event['request'], event['context']['stage'])


def get_catalog_command(request: dict, stage: str) -> dict:
    catalog_id = request["catalog-id"]

    catalog_response = catalog_command.get_catalog_by_id(catalog_id)

    if not catalog_response:
        raise NoSuchCatalog("Invalid Catalog ID {0}".format(catalog_id), "Catalog ID does not exist")

    return {"response-dict": generate_response_json(catalog_response, stage)}


def generate_response_json(catalog_record: dict, stage: str) -> dict:
    required_props_defaults = {
        "catalog-url": "/catalogs/{0}/{1}".format(stage, catalog_record['catalog-id'])
    }
    required_props = command_wrapper.get_required_response_schema_keys('catalog-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('catalog-properties')

    response_dict = {}

    for prop in required_props:
        if prop in catalog_record:
            response_dict[prop] = catalog_record[prop]
        elif prop in required_props_defaults:
            response_dict[prop] = required_props_defaults[prop]
        else:
            raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in catalog_record:
            response_dict[prop] = catalog_record[prop]

    return response_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    from datetime import datetime

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - GET COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-djh-DataLake-CatalogTable'

    lambda_context = \
        {
            "context":
                {
                    "client-request-id": "12345-request-id-wxyz",
                    "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                    "client-id": "abcd-client-id-7890",
                    "http-method": "GET",
                    "stage": "LATEST",
                    "api-key-id": "testApiKeyId"
                },
            "request":
                {
                    "catalog-id": "6"
                }
        }


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    lambda_response = lambda_handler(lambda_context, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN STOP - GET COMMAND LAMBDA]")
