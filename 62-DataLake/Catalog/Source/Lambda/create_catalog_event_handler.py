import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.catalog_table import CatalogTable
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))
    logger.info("Context: {0}".format(vars(context)))
    return create_catalog_event_handler(event, context.invoked_function_arn)


def create_catalog_event_handler(event: dict, lambda_arn: str) -> str:
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.CATALOG_CREATE):
        raise TerminalErrorException(
            "Failed to extract event: {} event-name does not match {}".format(event, event_names.CATALOG_CREATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage does not match {}".format(event, lambda_arn))

    catalog_data = generate_catalog_item(message)
    if catalog_data:
        insert_to_catalog_table(catalog_data)
    else:
        raise TerminalErrorException("Failed to insert item into Catalog table. Failed to transform invalid "
                                     "item. Item = {0}".format(message))

    return event_handler_status.SUCCESS


def insert_to_catalog_table(item: dict) -> None:
    """
    Inserts catalog event as an item into Catalog table.

    :param item: Catalog Event item to be inserted
    """
    try:
        CatalogTable().put_item(item)
        logger.info("Insertion to Catalog table succeeded.")
    except (SchemaValidationError, ConditionError, SchemaError, SchemaLoadError) as e:
        raise TerminalErrorException(
            "Failed to insert item into Catalog table. Item = {0} | Exception = {1}".format(item, e))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Failed to insert item into Catalog table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(item, e))


def generate_catalog_item(message: dict) -> dict:
    """
    Transform the event message to a catalog item following the required and optional properties of the catalog schema
    :param message: event message with catalog and event properties
    :return: catalog item dictionary
    """
    item = {}
    try:
        for key in CatalogTable().get_required_schema_keys():
            if key == "catalog-timestamp":
                item[key] = message["request-time"]
            elif key == "event-ids":
                item[key] = CatalogTable().build_event_ids_list({}, message['event-id'])
            else:
                item[key] = message[key]
    except KeyError as e:
        raise TerminalErrorException("Required Key = {} does not exist in the message.".format(e))

    for key in CatalogTable().get_optional_schema_keys():
        if key in message:
            item[key] = message[key]

    return item


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE CATALOG EVENT HANDLER LAMBDA]")
    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/CatalogEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-DataLake-CatalogTable'

    error_handler.terminal_errors_bucket = "ccs-sandbox-lambda-terminal-errors"

    msg = {
        "description": "Description of catalog",
        "request-time": "2018-12-07T17:37:11.932Z",
        "event-name": "Catalog::Create",
        "owner-id": 205,
        "catalog-id": "catalog20181207",
        "event-id": "nTTcqbDeImYQqRkV",
        "stage": "LATEST",
        "event-version": 1,
        "seen-count": 0
    }
    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - CREATE CATALOG EVENT HANDLER LAMBDA]")
