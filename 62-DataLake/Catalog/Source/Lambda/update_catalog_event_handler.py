import logging
import orjson
import os
from inspect import stack, getmodulename

from aws_xray_sdk.core import patch
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.catalog_collection_mapping_table import CatalogCollectionMappingTable
from lng_datalake_dal.catalog_table import CatalogTable
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError

__author__ = "Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

# Set module name for error handling
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
@error_handler.handle
@tracker.track_event_handler(event_names.CATALOG_UPDATE)
def lambda_handler(event, context):
    logger.info("Event: {}".format(orjson.dumps(event).decode()))
    logger.info("Context: {}".format(vars(context)))
    return update_catalog_event_handler(event, context.invoked_function_arn)


def update_catalog_event_handler(event, lambda_arn):
    """
    Encapsulates all the Lambda handler logic

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.CATALOG_UPDATE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.CATALOG_UPDATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    if not message.get('additional-attributes', {}):
        # update catalog table
        catalog_item = get_catalog_by_id(message["catalog-id"])
        catalog_item["event-ids"] = CatalogTable().build_event_ids_list(catalog_item, message['event-id'])

        if 'description' in message:
            catalog_item['description'] = message['description']

        update_catalog_table(catalog_item)

    # get the collection ids to add and remove from the mapping with catalog
    collection_ids_to_add = message.get("collection-id-actions", {}).get('add', [])
    collection_ids_to_del = message.get("collection-id-actions", {}).get('remove', [])

    if collection_ids_to_add or collection_ids_to_del:
        # get the index of the first unprocessed collection id in the add and remove list. If this is a retry event,
        # the index indicates the collection id from which to continue the mapping update.
        index_add = message.get('additional-attributes', {}).get('collection-ids-index', {}).get('add', 0)
        index_del = message.get('additional-attributes', {}).get('collection-ids-index', {}).get('remove', 0)

        new_index_add, new_index_del = update_catalog_collections_mapping(message['catalog-id'], message['event-id'],
                                                                          collection_ids_to_add,
                                                                          collection_ids_to_del,
                                                                          index_add, index_del)

        # update the indexes for the next retry in case of unprocessed collections id
        if new_index_add < len(collection_ids_to_add) or new_index_del < len(collection_ids_to_del):
            error_handler.set_message_additional_attribute('collection-ids-index',
                                                           {'add': new_index_add, 'remove': new_index_del})
            raise RetryEventException("Retry event to finish the update with the unprocessed collection ids")

    return event_handler_status.SUCCESS


def update_catalog_table(item: dict) -> None:
    """
    Insert a catalog item into Catalog table

    :param item: updated catalog item to be inserted
    """
    try:
        CatalogTable().update_item(item)
        logger.info("Update for CatalogId: {} was successful".format(item["catalog-id"]))
    except (ConditionError, SchemaError, SchemaLoadError, SchemaValidationError) as e:
        raise TerminalErrorException(
            "Failed to update item in Catalog table. Item = {0} | Exception = {1}".format(item, e))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Failed to update item in Catalog table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(item, e))


def update_catalog_collections_mapping(catalog_id: str, event_id: str,
                                       collection_ids_to_add: list,
                                       collection_ids_to_del: list,
                                       index_add: int, index_del: int) -> tuple:
    """
    Use batch_write_items to put and delete collections from the CatalogCollectionMappingTable.

    :param catalog_id: catalog id
    :param event_id: event id
    :param collection_ids_to_add: list of collection ids to add to the catalog
    :param collection_ids_to_del: list of collection ids to remove from the catalog
    :param index_add: first position of the unprocessed elements in collections_ids_to_add
    :param index_del: first position of the unprocessed elements in collections_ids_to_del
    :return: indexes of the first unprocessed elements in collections_ids_to_add and collections_ids_to_del
    """
    # DynamoDB BatchWriteItem has limit of 25 items per batch
    max_batch_size = 25
    while index_add < len(collection_ids_to_add) or index_del < len(collection_ids_to_del):
        add_batch_size = max_batch_size
        items_to_add = [{'catalog-id': catalog_id, 'collection-id': coll, 'event-id': event_id} for coll in
                        collection_ids_to_add[index_add:index_add + add_batch_size]]
        add_batch_size = len(items_to_add)

        del_batch_size = max_batch_size - add_batch_size
        items_to_del = [{'catalog-id': catalog_id, 'collection-id': coll} for coll in
                        collection_ids_to_del[index_del:index_del + del_batch_size]]
        del_batch_size = len(items_to_del)

        try:
            unprocessed_items = CatalogCollectionMappingTable().batch_write_items(items_to_add, items_to_del)
            if unprocessed_items:
                break
            index_add += add_batch_size
            index_del += del_batch_size
        except error_handler.retryable_exceptions as e:
            logger.error(e)
            break
        except Exception as e:
            logger.error(e)
            raise TerminalErrorException("General Exception Occurred: {}".format(e))

    return index_add, index_del


def get_catalog_by_id(catalog_id: str) -> dict:
    """
    Get the catalog item
    :param catalog_id: the id of the catalog to retrieve
    :return: the catalog item if exist
    """
    try:
        return CatalogTable().get_item({'catalog-id': catalog_id})
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Failed to get catalog {0} from Catalog Table due to unknown reasons. "
                                     "| Exception = {1}".format(catalog_id, e))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE CATALOG EVENT HANDLER LAMBDA]")

    os.environ["AWS_PROFILE"] = "c-sand"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/CatalogEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ[
        'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN'] = 'arn:aws:sns:us-east-1:288044017584:SubscriptionNotificationTopic'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CollectionTable'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CatalogTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-TrackingTable'

    target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {"collection-id-actions": {"add": ["411"]},
           "description": "new value",
           "owner-id": 442,
           "event-id": "123",
           "item-name": "new name",
           "stage": "LATEST",
           "request-time": "2018-05-18T19:31:17.813Z",
           "event-name": "Catalog::Update",
           "catalog-id": "104",
           "event-version": 1,
           "seen-count": 0}

    sample_event = {
        "Records": [{
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:us-east-1:288044017584:UpdateCatalogEventHandlerTopic:49f4a938"
                                    "-0394-419e-a739-2ffdb3a75190",
            "Sns": {
                "Type": "Notification",
                "MessageId": "85b707ad-c852-52e4-b10a-55488ee08219",
                "TopicArn": "arn:aws:sns:us-east-1:288044017584:UpdateCatalogEventHandlerTopic",
                "Subject": "",
                "Message": json.dumps(msg),
                "Timestamp": "2018-01-26T15:08:34.053Z",
                "SignatureVersion": "1",
                "Signature": "f0ykfJhySH"
                             "+JyMQ58gnCNNp29diBMUjv5rNzqCwG886yvU7dbWaX3zIZlzVepXgoQtfYRluAinx5U5qeHh7CzCZzLYcOTpbIv1"
                             "blto/qdNVnCm68FXVtF/vKMR+/6m+Dk/vIQAQsRutrTCs5Y5/uZ1PmlTzLoa0Z1/+vfFs3zmblvG+Va/D4gEtm2v"
                             "zzF0a/tO0kSpBHacbMEQ7k7PLZDHt8VAoZEqaSHuXRvHCuuRqHzMhzqxZkmFXVFm1F/imbmKdC7QXjLeytJNBhzH"
                             "Czr645LhTcfjbD7pwOBhqQzMIuWpV6CxL++Uk4NxQVGeHkrpcnAn1VoN/Ap329e5PlXw==",
                "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService"
                                  "-433026a4050d206028891664da859041.pem",
                "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws"
                                  ":sns:us-east-1:288044017584:UpdateCatalogEventHandlerTopic:49f4a938-0394-419e"
                                  "-a739-2ffdb3a75190",
                "MessageAttributes": {}
            }
        }
        ]
    }
    lambda_handler(sample_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - UPDATE CATALOG EVENT HANDLER LAMBDA]")
