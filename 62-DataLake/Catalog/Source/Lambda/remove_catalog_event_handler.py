import logging
import os
from inspect import stack, getmodulename

import orjson
from aws_xray_sdk.core import patch
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.catalog_collection_mapping_table import CatalogCollectionMappingTable
from lng_datalake_dal.catalog_table import CatalogTable

__author__ = "Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set environment variables
target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

# Set module name
error_handler.lambda_name = getmodulename(stack()[0][1])


@session_decorator.lng_aws_session()
@error_handler.handle
@tracker.track_event_handler(event_names.CATALOG_REMOVE)
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))
    logger.info("Context: {0}".format(vars(context)))
    return remove_catalog_event_handler(event, context.invoked_function_arn)


def remove_catalog_event_handler(event: dict, lambda_arn: str) -> str:
    """
    Encapsulates all the Lambda handler logic.

    :param event: Lambda event
    :param lambda_arn: Lambda ARN which should include alias
    """
    message = sns_extractor.extract_sns_message(event)

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.CATALOG_REMOVE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name does not match {}".format(event, event_names.CATALOG_REMOVE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage does not match {}".format(event, lambda_arn))

    logger.info("Message = {0}".format(message))
    catalog_id = message['catalog-id']

    remove_collections_from_catalog(catalog_id)
    remove_catalog_record(catalog_id)

    logger.debug("Done")
    return event_handler_status.SUCCESS


def remove_collections_from_catalog(catalog_id):
    mapping_table_response = get_catalog_collection_ids(catalog_id)
    if mapping_table_response:
        try:
            CatalogCollectionMappingTable().batch_write_items(items_to_delete=mapping_table_response)
        except error_handler.retryable_exceptions as error:
            raise error
        except Exception as error:
            raise TerminalErrorException(
                "General exception occurred when removing records from the CatalogCollectionMappingTable. Error: {}".format(
                    error))


def get_catalog_collection_ids(catalog_id: str) -> list:
    try:
        return CatalogCollectionMappingTable().query_items(
            index="collection-by-catalog-id-index",
            KeyConditionExpression='CatalogID=:catalog_id',
            ExpressionAttributeValues={":catalog_id": {"S": catalog_id}},
            ProjectionExpression="CollectionID, CatalogID")
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException(
            "General exception occurred when getting records from the CatalogCollectionMappingTable by catalog-id."
            " Error: {}".format(error))


def remove_catalog_record(catalog_id):
    try:
        CatalogTable().delete_item({'catalog-id': catalog_id})
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException(
            "General exception occurred when removing catalog id {0} from table. Error: {1}".format(
                catalog_id, error))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE CATALOG EVENT HANDLER LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'

    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ['SUBSCRIPTION_NOTIFICATION_TOPIC_ARN'] = 'arn:aws:' \
                                                        'sns:us-east-1:288044017584:' \
                                                        'SubscriptionNotificationLambda-' \
                                                        'SubscriptionNotificationTopic-1OB8OXXGDBQ5R'
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/CatalogEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-mas-DataLake-CatalogTable'
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = 'feature-mas-DataLake-CatalogCollectionMappingTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-mas-DataLake-TrackingTable'
    os.environ['MAPPING_DYNAMO_CATALOG_ID_INDEX'] = 'collection-by-catalog-id-index'

    target_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {
        "event-name": event_names.CATALOG_REMOVE,
        "event-id": "a2b9d7aa-345a-11e9-97ce-f563dd3f7b7c",
        "request-time": "2019-02-19T15:25:49.330Z",
        "event-version": 1,
        "stage": "LATEST",
        "catalog-id": "1",
        "owner-id": 1
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }
    mock_context = mock_lambda_context.MockLambdaContext()
    lambda_handler(sample_event, mock_context)
    logger.debug("[LOCAL RUN END - REMOVE CATALOG EVENT HANDLER LAMBDA]")
