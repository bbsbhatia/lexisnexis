import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import NoSuchCatalog, InternalError, NoSuchCollection, SemanticError, \
    InvalidRequestPropertyValue, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names, collection_status
from lng_datalake_dal.collection_table import CollectionTable

from service_commons import catalog_command

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/UpdateCatalogCommand-RequestSchema.json',
                         'Schemas/UpdateCatalogCommand-ResponseSchema.json', event_names.CATALOG_UPDATE)
def lambda_handler(event, context):
    return update_catalog_command(event['request'],
                                  event['context']['client-request-id'],
                                  event['context']['stage'],
                                  event['context']['api-key-id'])


def update_catalog_command(request, request_id, stage, api_key_id):
    # required properties
    catalog_id = request["catalog-id"]
    patch_operations = request["patch-operations"]

    current_catalog = catalog_command.get_catalog_by_id(catalog_id)

    if not current_catalog:
        raise NoSuchCatalog("Invalid Catalog ID {}".format(catalog_id),
                            "Catalog ID does not exist")

    # owner authorization
    owner_authorization.authorize(current_catalog['owner-id'], api_key_id)

    update_catalog_item = transform_patch_operations(patch_operations)

    collections_to_add = update_catalog_item['collection-id-actions']['add']

    if collections_to_add:
        validate_collections(collections_to_add)

    if update_catalog_item.get('description'):
        current_catalog['description'] = update_catalog_item['description']

    response_dict = generate_response_json(current_catalog, catalog_id, stage)

    event_dict = {
        'event-id': request_id,
        'request-time': command_wrapper.get_request_time(),
        'event-name': event_names.CATALOG_UPDATE,
        'event-version': event_version,
        'stage': stage,
        'catalog-id': catalog_id,
        'collection-id-actions': update_catalog_item['collection-id-actions']
    }
    if 'description' in update_catalog_item:
        event_dict['description'] = update_catalog_item['description']

    return {'response-dict': response_dict, 'event-dict': event_dict}


def validate_collections(collection_ids: list) -> None:
    """
    Retrieve a batch of collections and validate the items that are valid to insert into a Catalog
    :param collection_ids: list of collection ids
    :return: None
    """
    try:
        items = CollectionTable().batch_get_items('collection-id', collection_ids)
    except ClientError as e:
        logger.error(e)
        raise InternalError("Unable to get item from Collection Table", e)

    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)

    collection_id_set = set(collection_ids)
    if len(collection_id_set) != len(items):
        invalid_ids = invalid_collection_ids(collection_id_set, items)
        raise NoSuchCollection("Invalid Collection IDs {}".format(invalid_ids),
                               "Collections must exist")

    invalid_collections_state = []
    for item in items:
        if item['collection-state'] not in [collection_status.CREATED, collection_status.SUSPENDED]:
            invalid_collections_state.append(item['collection-id'])

    if invalid_collections_state:
        raise SemanticError("Invalid Collection IDs {}".format(invalid_collections_state),
                            "Collections must be in either a state of Created or Suspended")


def generate_response_json(request, catalog_id, stage):
    response_dict = {}

    required_props_defaults = \
        {
            "catalog-id": catalog_id,
            "catalog-url": "/catalogs/{0}/{1}".format(stage, catalog_id)
        }

    required_props = command_wrapper.get_required_response_schema_keys('catalog-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('catalog-properties')

    for prop in required_props:
        if prop in request and prop != 'catalog-id':
            response_dict[prop] = request[prop]
        elif prop in required_props_defaults:
            response_dict[prop] = required_props_defaults[prop]
        else:
            raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in request:
            response_dict[prop] = request[prop]

    return response_dict


def transform_patch_operations(patch_document: list) -> dict:
    add_collection_ids = set()
    remove_collection_ids = set()
    description = None
    total_collection_ids = 0

    for action in patch_document:
        if action["path"] == "/collection-ids":
            total_collection_ids += len(action["value"])
            validate_collection_id_limit(total_collection_ids)
            value = set(action["value"])
            if action["op"] == "add":
                add_collection_ids.update(value)
                remove_collection_ids.difference_update(value)
            if action["op"] == "remove":
                remove_collection_ids.update(value)
                add_collection_ids.difference_update(value)

        if action["path"] == "/description":
            validate_description_size_limit(action["value"])
            description = action["value"]

    result = {'collection-id-actions': {'add': list(add_collection_ids), 'remove': list(remove_collection_ids)}}

    if description is not None:
        result['description'] = description

    return result


def validate_collection_id_limit(total_collection_ids: int) -> None:
    if total_collection_ids > 1000:
        raise InvalidRequestPropertyValue("Invalid number of Collection IDs {}".format(total_collection_ids),
                                          "Number of Collection IDs should not exceed 1000")


def validate_description_size_limit(description: str) -> None:
    if len(description) > 256:
        raise InvalidRequestPropertyValue("Invalid Catalog Description {}".format(description),
                                          "Catalog Description length should not exceed 256 characters")


def invalid_collection_ids(collection_ids: set, collection_items: list) -> set:
    valid_ids = set([item['collection-id'] for item in collection_items])
    return collection_ids.difference(valid_ids)


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - GET COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-man-DataLake-CatalogTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-man-DataLake-OwnerTable'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-man-DataLake-CollectionTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-man-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-man-DataLake-TrackingTable'

    lambda_context = \
        {
            "context": {
                "client-request-id": "123",
                "client-request-time": "1526671877813",
                "client-id": "abcd-client-id-7890",
                "http-method": "PATCH",
                "stage": "LATEST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                'catalog-id': "catalog-01",
                "patch-operations": [
                    {"op": "replace", "path": "/description", "value": "new value"},
                    {"op": "add", "path": "/collection-ids", "value": ["01", "03", "05"]},
                    {"op": "remove", "path": "/collection-ids", "value": ["02", "04"]}
                ]
            }
        }


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)

    lambda_response = lambda_handler(lambda_context, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN STOP - GET COMMAND LAMBDA]")
