import logging
import os

from aws_xray_sdk.core import patch, xray_recorder
from lng_datalake_commands import command_wrapper, paginator_token, command_validator
from lng_datalake_commands.exceptions import InternalError
from lng_datalake_commons import session_decorator
from lng_datalake_dal.catalog_collection_mapping_table import CatalogCollectionMappingTable
from lng_datalake_dal.catalog_table import CatalogTable

from service_commons import catalog_command

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
libraries = ('botocore',)
patch(libraries)

DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 10))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/DescribeCatalogsCommand-RequestSchema.json',
                         'Schemas/DescribeCatalogsCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return describe_catalogs_command(event['request'], event['context']['stage'])


def describe_catalogs_command(request: dict, stage: str) -> dict:
    owner_id = request.get('owner-id')
    catalog_id = request.get('catalog-id')
    max_items = request.get('max-items', DEFAULT_MAX_ITEMS)
    next_token = request.get('next-token')

    pagination_token = None

    # If we have a next-token validate the max-results matches the original request
    # Dynamo doesn't handle pagination if the max-items changes
    if next_token:
        tokens = paginator_token.unpack_validate_pagination_token(next_token, max_items)
        pagination_token = tokens['pagination-token']

    # sanity check for max-items (request schema should prevent)
    command_validator.validate_max_results(max_items, DEFAULT_MAX_ITEMS)

    # Check if a single catalog ID was requested
    if catalog_id:
        single_catalog = catalog_command.get_catalog_by_id(catalog_id)
        # get_catalog_by_id returns a dict; if there is no catalog with that catalog_id it will return an empty dict
        catalog_list = [single_catalog] if single_catalog else []
        truncate = False
    # Get list and prepare to truncate if necessary
    else:
        catalog_list = catalog_command.get_catalog_list(owner_id, max_items, pagination_token)
        truncate = True

    update_catalog_list_with_collection_ids(catalog_list, truncate)

    return {
        "response-dict": generate_response_json(catalog_list, CatalogTable().get_pagination_token(), stage, max_items)}


def update_catalog_list_with_collection_ids(catalog_list: list, truncate: bool) -> None:
    for item in catalog_list:
        truncated = False
        if truncate:
            collections = catalog_command.get_catalog_collection_ids(item['catalog-id'], 14000)
            if CatalogCollectionMappingTable().get_pagination_token():
                truncated = True
        else:
            collections = catalog_command.get_catalog_collection_ids(item['catalog-id'], -1)

        cols = list(col['collection-id'] for col in collections)
        item['collection-ids'] = cols
        item['truncated'] = truncated


def generate_response_json(catalog_list: list, pagination_token: str, stage: str, max_items: int) -> dict:
    catalog_response = []
    for catalog_data in catalog_list:
        required_props_defaults = \
            {
                "catalog-url": "/catalogs/{0}/{1}".format(stage, catalog_data['catalog-id'])
            }

        required_props = command_wrapper.get_required_response_schema_keys('describe-catalogs-response', 'properties',
                                                                           'catalogs', 'items')
        optional_props = command_wrapper.get_optional_response_schema_keys('describe-catalogs-response', 'properties',
                                                                           'catalogs', 'items')

        response_item = {}

        for prop in required_props:
            if prop in catalog_data:
                response_item[prop] = catalog_data[prop]
            elif prop in required_props_defaults:
                response_item[prop] = required_props_defaults[prop]
            else:
                raise InternalError("Missing required property: {0}".format(prop))

        for prop in optional_props:
            if prop in catalog_data:
                response_item[prop] = catalog_data[prop]

        catalog_response.append(response_item)

    response = {"catalogs": catalog_response}

    if pagination_token:
        response['next-token'] = paginator_token.generate_pagination_token(max_items, pagination_token)

    response['item-count'] = len(catalog_list)

    return response


if __name__ == '__main__':
    import json
    from datetime import datetime
    from lng_datalake_testhelper import mock_lambda_context

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'DataLake-CatalogTable'
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = 'DataLake-CatalogCollectionMappingTable'
    ct_catalog_by_owner_id_index = "catalog-by-owner-id-index"

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - DESCRIBE CATALOGS COMMAND LAMBDA]")

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "catalog-id": "CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC",
                "owner-id": 11
                # "next-token": "1|eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ2F0YWxvZ0lEIjogeyJTIjogImNhdGFsb2ctMDEifX19"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)
    xray_recorder.context._context_missing = "LOG_ERROR"

    lambda_response = lambda_handler(lambda_event, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - DESCRIBE CATALOGS COMMAND LAMBDA]")
