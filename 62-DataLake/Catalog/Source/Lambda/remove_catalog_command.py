import logging
import os

from aws_xray_sdk.core import patch
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, NoSuchCatalog
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names

from service_commons import catalog_command

__author__ = "Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/RemoveCatalogCommand-RequestSchema.json',
                         'Schemas/RemoveCatalogCommand-ResponseSchema.json', event_names.CATALOG_REMOVE)
def lambda_handler(event, context):
    return remove_catalog_command(event["request"],
                                  event['context']['client-request-id'],
                                  event['context']['stage'],
                                  event['context']['api-key-id'])


def remove_catalog_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # required properties
    catalog_id = request['catalog-id']

    catalog_response = catalog_command.get_catalog_by_id(catalog_id)
    if not catalog_response:
        raise NoSuchCatalog("Invalid Catalog ID {0}".format(catalog_id),
                            "Catalog ID does not exist")

    # owner authorization
    owner_authorization.authorize(catalog_response['owner-id'], api_key_id)
    response_dict = generate_response_json(catalog_response, stage)
    event_dict = \
        {
            'catalog-id': catalog_id,
            'event-id': request_id,
            'request-time': command_wrapper.get_request_time(),
            'event-name': event_names.CATALOG_REMOVE,
            'event-version': event_version,
            'stage': stage
        }

    return {"response-dict": response_dict, "event-dict": event_dict}


def generate_response_json(catalog, stage):
    required_props_defaults = \
        {
            "catalog-url": "/catalogs/{0}/{1}".format(stage, catalog['catalog-id'])
        }

    required_props = command_wrapper.get_required_response_schema_keys('catalog-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('catalog-properties')

    response_dict = {}

    for prop in required_props:
        if prop in catalog:
            response_dict[prop] = catalog[prop]
        elif prop in required_props_defaults:
            response_dict[prop] = required_props_defaults[prop]
        else:
            raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in catalog:
            response_dict[prop] = catalog[prop]

    return response_dict


if __name__ == '__main__':
    import json
    from lng_datalake_dal.tracking_table import TrackingTable
    from lng_datalake_testhelper import mock_lambda_context
    from datetime import datetime
    import random
    import string

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE CATALOG COMMAND LAMBDA]")

    asset_group = "feature-mas"
    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['CATALOG_DYNAMODB_TABLE'] = '{}-DataLake-CatalogTable'.format(asset_group)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{}-DataLake-OwnerTable'.format(asset_group)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{}-DataLake-EventStoreTable'.format(asset_group)
    os.environ['TRACKING_DYNAMODB_TABLE'] = '{}-DataLake-TrackingTable'.format(asset_group)

    TrackingTable().table_name = os.getenv("TRACKING_DYNAMODB_TABLE")


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "DELETE",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "catalog-id": "100000"
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)
    lambda_response = lambda_handler(lambda_event, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - REMOVE CATALOG COMMAND LAMBDA]")
