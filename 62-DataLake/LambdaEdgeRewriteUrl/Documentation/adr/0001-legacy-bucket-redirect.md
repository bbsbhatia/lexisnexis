
# Add ability to redirect for legacy bucket


Date: 2019-08-20

## Status:  Accepted

* Deciders: Planet X

## Context
As part of the update to how we store DataLake Objects in S3 [ADR New Object Buckets](../../../Object/Documentation/adr/0007-new-object-store-buckets.md)
we need to have the existing /objects/store path point to the new s3 buckets.  Any existing HTTP calls to this
path that need to find objects in the old format will need to be redirected to the previous s3 bucket origin group.

## Decision

We will update the Edge Rewriter to handle viewer-requests only on the /objects/store path.  If the request is a single file
ie. /objects/store/foobar then we will redirect to /legacy/objects/store/foobar.  If a request is for the newly stored object pattern
ie. /objects/store/collection/foobar, we will continue on.

We also will update the code to use Python 3.7 for now.  We will need to do a performance analysis for how Lambda Edge itself performs between NodeJS and Python
to determine which language is preformat at the Edge location.  Also should look at minification for performance down the road. For now we stick with python because the team is well versed 
and this is critical logic.
## Consequences

Existing calls to the old object format will take longer because they are being redirected.
Cost will also be slightly increased for /objects/store calls since we are hitting edge function twice

## Links
[Update Cloufront Origin Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:838784)