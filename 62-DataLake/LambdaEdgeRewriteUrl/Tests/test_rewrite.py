import unittest

from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from rewrite import update_uri, lambda_handler

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__)


class TestRewrite(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.maxDiff = None

    # +lambda_handler - Valid origin request (/administration/LATEST/owners/1800)
    def test_lambda_handler_valid(self):
        event = io_util.load_data_json("object_store_input.json")
        resp = io_util.load_data_json('object_store_response.json')
        self.assertDictEqual(lambda_handler(event, MockLambdaContext()), resp)

    # +lambda_handler - Valid legacy object viewer request (/objects/store/abcd)
    def test_lambda_handler_valid_old(self):
        event = io_util.load_data_json("object_store_old_input.json")
        resp = io_util.load_data_json('object_store_old_response.json')
        self.assertDictEqual(lambda_handler(event, MockLambdaContext()), resp)

    # +lambda_handler - Valid legacy object viewer request (/objects/store/00158bc66dc7fd951ecf1264be19608af1f7d4ff)
    def test_lambda_handler_valid_viewer_legacy(self):
        event = io_util.load_data_json("viewer_legacy_input.json")
        resp = io_util.load_data_json('viewer_legacy_response.json')
        self.assertDictEqual(lambda_handler(event, MockLambdaContext()), resp)

    # +lambda_handler - Valid collection hash object viewer request
    def test_lambda_handler_valid_viewer_collection_hash(self):
        event = io_util.load_data_json("viewer_collection_hash_input.json")
        resp = io_util.load_data_json('viewer_collection_hash_response.json')
        print(lambda_handler(event, MockLambdaContext()))
        self.assertDictEqual(lambda_handler(event, MockLambdaContext()), resp)

    # +lambda_handler - Valid legacy object origin request (/legacy/objects/store/00158bc66dc7fd951ecf1264be19608af1f7d4ff)
    def test_lambda_handler_valid_origin_legacy(self):
        event = io_util.load_data_json("origin_legacy_object_input.json")
        resp = io_util.load_data_json('origin_legacy_object_response.json')
        self.assertDictEqual(lambda_handler(event, MockLambdaContext()), resp)

    # +lambda_handler - Valid temp object origin request (/objects/temp/00158bc66dc7fd951ecf1264be19608af1f7d4ff)
    def test_lambda_handler_valid_origin_temp(self):
        event = io_util.load_data_json("origin_temp_object_input.json")
        resp = io_util.load_data_json('origin_temp_object_response.json')
        self.assertDictEqual(lambda_handler(event, MockLambdaContext()), resp)

    # +lambda_handler - Valid object origin response (s3 headers)
    def test_lambda_handler_valid_origin_response_s3(self):
        event = io_util.load_data_json("origin-response_s3_input.json")
        resp = io_util.load_data_json('origin-response_s3_response.json')
        print(lambda_handler(event, MockLambdaContext()))
        self.assertDictEqual(lambda_handler(event, MockLambdaContext()), resp)

    # + update_uri - Valid object store
    def test_update_uri_valid(self):
        self.assertEqual(update_uri("/object/store"), "/store")
        self.assertEqual(update_uri("/objects/store/xyzabcdef"), "/xyzabcdef")
        self.assertEqual(update_uri("/objects/store/prefix/xyzabcdef"), "/prefix/xyzabcdef")
        self.assertEqual(update_uri("/objects/storecatalogdata/xyzabcdef"), "/storecatalogdata/xyzabcdef")
        self.assertEqual(update_uri("/objects/storecatalogdata"), "/storecatalogdata")
        self.assertEqual(update_uri("/objects/store"), "/")
        self.assertEqual(update_uri("/objects/store/legacy/"), "/legacy/")
        self.assertEqual(update_uri("/legacy/objects/store/"), "/")

    # + update_uri - Valid api calls
    def test_update_uri_api_valid(self):
        self.assertEqual(update_uri("/administration/store"), "/store")
        self.assertEqual(update_uri("/owners/me"), "/me")
        self.assertEqual(update_uri("/subscription/21"), "/21")
        self.assertEqual(update_uri("/objects/foobar/multipart/complete"), "/foobar/multipart/complete")


if __name__ == '__main__':
    print(unittest.main())
