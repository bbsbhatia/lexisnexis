import re

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


def lambda_handler(event, context):
    cf_obj = event['Records'][0]['cf']
    request_type = cf_obj['config']['eventType']
    request = cf_obj['request']
    request_headers = request['headers']
    log = True if 'x-dl-trace-id' in request_headers else False

    if log:
        print("Request: {0}".format(event))

    if request_type == 'origin-request':
        response = request
        response['uri'] = update_uri(request['uri'])

    elif request_type == 'viewer-request':
        response = request
        uri = request['uri']

        # To handle legacy requests for now.
        # Only on a viewer-request do we check to see if the path is for /objects/store and also if we have a
        # single object (ie no path) and redirect to the legacy/objects/store/ path.
        if uri.startswith('/objects/store/'):
            path = uri.replace('/objects/store/', '')
            # If the path is empty now and we have a querystring, assume it's a collection-hash-url and continue on
            if not path and request.get('querystring'):
                pass #NOSONAR
            # Must be the legacy object store bucket since we don't have a '/' in the object key
            elif '/' not in path:
                response = {
                    'status': '302',
                    'statusDescription': 'Found',
                    'headers': {
                        'location': [
                            {'value': 'https://{0}/legacy/objects/store/{1}'.format(request_headers['host'][0]['value'],
                                                                                    path)}]
                    }
                }

    elif request_type == 'origin-response':
        response = cf_obj['response']
        response['headers'] = update_headers(response['headers'])

    # Viewer Response CloudFront Events are not currently configured to run LambdaEdgeRewriteUrl for objects/store/*
    else:  # request_type == 'viewer-response':
        response = cf_obj['response']

    if log:
        print("Response: {0}".format(response))

    return response


def update_uri(uri: str) -> str:
    if uri.startswith('/legacy/objects/store/'):
        resp = uri.replace('/legacy/objects/store/', '/')
    elif uri.startswith('/objects/store/'):
        resp = uri.replace("/objects/store/", '/')
    elif uri == '/objects/store':
        resp = '/'
    elif uri.startswith('/objects/temp/'):
        resp = uri.replace("/objects/temp/", '/')
    else:
        resp = re.sub(r'^/[^/]+/', '/', uri)

    return resp


def update_headers(response_headers: dict) -> dict:
    modified_headers = {}
    for header_name, header_value in response_headers.items():
        if header_name.startswith('x-amz-meta-'):
            new_header_name = header_name.replace('x-amz-meta-', 'x-dl-meta-', 1)
            modified_headers[new_header_name] = [
                {
                    "key": new_header_name,
                    "value": header_value[0]['value']
                }
            ]
        else:
            modified_headers[header_name] = header_value
    return modified_headers


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    event1 = {
        "Records": [
            {
                "cf": {
                    "config": {
                        "distributionDomainName": "EXAMPLE",
                        "distributionId": "EXAMPLE",
                        "eventType": "origin-request"
                    },
                    "request": {
                        "uri": "/objects/v1test",
                        "method": "GET",
                        "clientIp": "10.0.0.0",
                        "headers": {
                            "user-agent": [
                                {
                                    "key": "User-Agent",
                                    "value": "Test Agent"
                                }
                            ],
                            "host": [
                                {
                                    "key": "Host",
                                    "value": "d123.cf.net"
                                }
                            ],
                            "cookie": [
                                {
                                    "key": "Cookie",
                                    "value": "SomeCookie=1; AnotherOne=A; X-Experiment-Name=B"
                                }
                            ],
                            "x-amz-meta-name1": [
                                {
                                    "key": "somekey",
                                    "value": "somevalue"
                                }
                            ]
                        }
                    }
                }
            }
        ]
    }
    event2 = {
        "Records": [
            {
                "cf": {
                    "config": {
                        "distributionDomainName": "d94bpo6b3s3oe.cloudfront.net",
                        "distributionId": "E3GKHZ284ZOCSF",
                        "eventType": "origin-response"
                    },
                    "request": {
                        "clientIp": "198.176.80.34",
                        "headers": {
                            "if-modified-since": [
                                {
                                    "key": "If-Modified-Since",
                                    "value": "Fri, 11 Oct 2019 13:21:49 GMT"
                                }
                            ],
                            "if-none-match": [
                                {
                                    "key": "If-None-Match",
                                    "value": "\"b4dd0318e344966caafd0564380f57d3\""
                                }
                            ],
                            "x-forwarded-for": [
                                {
                                    "key": "X-Forwarded-For",
                                    "value": "198.176.80.34"
                                }
                            ],
                            "user-agent": [
                                {
                                    "key": "User-Agent",
                                    "value": "Amazon CloudFront"
                                }
                            ],
                            "via": [
                                {
                                    "key": "Via",
                                    "value": "1.1 32af773f3dd64168996070b317094c54.cloudfront.net (CloudFront)"
                                }
                            ],
                            "x-dl-trace-id": [
                                {
                                    "key": "x-dl-trace-id",
                                    "value": "1"
                                }
                            ],
                            "postman-token": [
                                {
                                    "key": "Postman-Token",
                                    "value": "d32ed99a-e371-4ce6-a8f6-fd0cc10d45de"
                                }
                            ],
                            "accept-encoding": [
                                {
                                    "key": "Accept-Encoding",
                                    "value": "gzip"
                                }
                            ],
                            "host": [
                                {
                                    "key": "Host",
                                    "value": "feature-ajb-dl-object-store-use1.s3.amazonaws.com"
                                }
                            ],
                            "cache-control": [
                                {
                                    "key": "Cache-Control",
                                    "value": "no-cache"
                                }
                            ]
                        },
                        "method": "GET",
                        "origin": {
                            "s3": {
                                "authMethod": "origin-access-identity",
                                "customHeaders": {},
                                "domainName": "feature-ajb-dl-object-store-use1.s3.amazonaws.com",
                                "path": "",
                                "region": "us-east-1"
                            }
                        },
                        "querystring": "",
                        "uri": "/cbe07645ce7dad9eb5a110865dcefcc6f059789d/c1c3c72aa72d21722ef32ac52dbaf9a9ee82abff"
                    },
                    "response": {
                        "headers": {
                            "x-amz-id-2": [
                                {
                                    "key": "x-amz-id-2",
                                    "value": "iy9uef54Ow5hEIr0WDtYYsKXw1X8egT9yuomwIo3RmP4EPcLtYlap37MOXe8qX/wb2AT97rJZ+8="
                                }
                            ],
                            "x-amz-request-id": [
                                {
                                    "key": "x-amz-request-id",
                                    "value": "476F37F9BCBC4722"
                                }
                            ],
                            "date": [
                                {
                                    "key": "Date",
                                    "value": "Fri, 11 Oct 2019 15:53:42 GMT"
                                }
                            ],
                            "last-modified": [
                                {
                                    "key": "Last-Modified",
                                    "value": "Fri, 11 Oct 2019 13:21:49 GMT"
                                }
                            ],
                            "etag": [
                                {
                                    "key": "ETag",
                                    "value": "\"b4dd0318e344966caafd0564380f57d3\""
                                }
                            ],
                            "x-amz-meta-dl-version-timestamp": [
                                {
                                    "key": "x-amz-meta-dl-version-timestamp",
                                    "value": "2019-10-11T13:21:43.624Z"
                                }
                            ],
                            "x-amz-meta-name1": [
                                {
                                    "key": "x-amz-meta-name1",
                                    "value": "value1"
                                }
                            ],
                            "x-amz-meta-name2": [
                                {
                                    "key": "x-amz-meta-name2",
                                    "value": "value2"
                                }
                            ],
                            "x-amz-meta-dl-content-sha1": [
                                {
                                    "key": "x-amz-meta-dl-content-sha1",
                                    "value": "42a78a206849e11070eb96d43a3fde8c7e10c558"
                                }
                            ],
                            "x-amz-meta-dl-object-id": [
                                {
                                    "key": "x-amz-meta-dl-object-id",
                                    "value": "aaron3-binary"
                                }
                            ],
                            "x-amz-meta-dl-version-number": [
                                {
                                    "key": "x-amz-meta-dl-version-number",
                                    "value": "1"
                                }
                            ],
                            "x-amz-meta-dl-collection-id": [
                                {
                                    "key": "x-amz-meta-dl-collection-id",
                                    "value": "AARON_2"
                                }
                            ],
                            "server": [
                                {
                                    "key": "Server",
                                    "value": "AmazonS3"
                                }
                            ]
                        },
                        "status": "304",
                        "statusDescription": "Not Modified"
                    }
                }
            }
        ]
    }
    event3 = {
        "Records": [
            {
                "cf": {
                    "config": {
                        "distributionDomainName": "d1en9p5b9g9igr.cloudfront.net",
                        "distributionId": "E1NO9IBDUFB7X7",
                        "eventType": "origin-request",
                        "requestId": "oglVS8ix6jPOCLEF2_UIOw-vYaiqUbRHZ8bV-g0FJTBK_pR4fEqnpg=="
                    },
                    "request": {
                        "clientIp": "198.176.81.34",
                        "headers": {
                            "host": [
                                {
                                    "key": "Host",
                                    "value": "foobar.content.aws.lexis.com"
                                }
                            ],
                            "user-agent": [
                                {
                                    "key": "User-Agent",
                                    "value": "PostmanRuntime/7.15.2"
                                }
                            ],
                            "accept": [
                                {
                                    "key": "Accept",
                                    "value": "*/*"
                                }
                            ],
                            "accept-encoding": [
                                {
                                    "key": "Accept-Encoding",
                                    "value": "gzip, deflate"
                                }
                            ],
                            "referer": [
                                {
                                    "key": "Referer",
                                    "value": "https://foobar.content.aws.lexis.com/objects/store/00158bc66dc7fd951ecf1264be19608af1f7d4ff"
                                }
                            ]
                        },
                        "method": "GET",
                        "querystring": "",
                        "uri": "/00158bc66dc7fd951ecf1264be19608af1f7d4ff"
                    }
                }
            }
        ]
    }

    print(lambda_handler(event3, MockLambdaContext()))
