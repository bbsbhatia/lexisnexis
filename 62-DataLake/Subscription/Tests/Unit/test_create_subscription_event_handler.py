import unittest
from unittest import TestCase
from unittest.mock import patch, call

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_subscription_event_handler import create_sns_subscription, \
    create_subscription_insert_dict, insert_to_subscription_table, \
    update_subscription_record, create_subscription_event_handler, lambda_handler, is_subscription_confirmed, \
    initiate_subscription, retry_subscription_confirmation

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateSubscriptionEventHandler')


class CreateSubscriptionEventHandlerTest(TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    @patch("create_subscription_event_handler.create_subscription_event_handler")
    def test_lambda_handler(self, handler_mock):
        handler_mock.return_value = event_handler_status.SUCCESS
        event = io_util.load_data_json("original_event.json")
        self.assertEqual(lambda_handler(event, MockLambdaContext), event_handler_status.SUCCESS)

    @patch("create_subscription_event_handler.create_subscription_event_handler")
    def test_lambda_handler(self, handler_mock):
        handler_mock.return_value = event_handler_status.SUCCESS
        event = io_util.load_data_json("sqs_event.json")
        self.assertEqual(lambda_handler(event, MockLambdaContext), [event_handler_status.SUCCESS])

    # + create_subscription_event_handler - Initial Valid
    @patch('create_subscription_event_handler.initiate_subscription')
    def test_create_subscription_event_handler_initial_valid(self, mock_sub_create):
        message = io_util.load_data_json("original_event.json")
        mock_sub_create.side_effect = RetryEventException(
            "Initial creation of subscription, retrying to validate confirmation")

        with self.assertRaisesRegex(RetryEventException,
                                    "Initial creation of subscription, retrying to validate confirmation"):
            create_subscription_event_handler(message, MockLambdaContext().invoked_function_arn)

    # + create_subscription_event_handler - Retry Valid
    @patch('create_subscription_event_handler.update_subscription_record')
    @patch('create_subscription_event_handler.is_subscription_confirmed')
    @patch('create_subscription_event_handler.initiate_subscription')
    def test_create_subscription_event_handler_retry_valid(self, mock_sub_create, mock_sub_confirmed,
                                                           mock_update_record):
        message = io_util.load_data_json("pending_event.json")
        mock_sub_confirmed.return_value = True
        mock_update_record.return_value = None

        self.assertEqual(create_subscription_event_handler(message, MockLambdaContext().invoked_function_arn),
                         event_handler_status.SUCCESS)

        mock_sub_create.assert_not_called()

    # + create_subscription_event_handler - Retry Valid Not confirmed
    @patch('create_subscription_event_handler.retry_subscription_confirmation')
    @patch('create_subscription_event_handler.current_time')
    @patch('create_subscription_event_handler.is_subscription_confirmed')
    @patch('create_subscription_event_handler.initiate_subscription')
    def test_create_subscription_event_handler_retry_valid_no_confirmation(self, mock_sub_create, mock_sub_confirmed,
                                                                           mock_get_time, mock_retry):
        message = io_util.load_data_json("pending_event.json")
        mock_sub_confirmed.return_value = False
        mock_get_time.return_value = 1525595000
        mock_retry.return_value = None

        self.assertEqual(create_subscription_event_handler(message, MockLambdaContext().invoked_function_arn),
                         event_handler_status.SUCCESS)

        mock_sub_create.assert_not_called()

    # + create_subscription_event_handler - Retry Valid Not confirmed expired
    @patch('create_subscription_event_handler.current_time')
    @patch('create_subscription_event_handler.is_subscription_confirmed')
    @patch('create_subscription_event_handler.initiate_subscription')
    def test_create_subscription_event_handler_retry_valid_expired(self, mock_sub_create, mock_sub_confirmed,
                                                                   mock_get_time):
        message = io_util.load_data_json("pending_event.json")
        mock_sub_confirmed.return_value = False
        mock_get_time.return_value = 1525595999

        self.assertEqual(create_subscription_event_handler(message, MockLambdaContext().invoked_function_arn),
                         event_handler_status.SUCCESS)

        mock_sub_create.assert_not_called()

    # - create_subscription_event_handler - invalid event
    @patch('lng_datalake_commons.sns_extractor.extract_sns_message')
    def test_create_subscription_event_handler_invalid_event(self, mock_extract_message):
        mock_extract_message.return_value = {'event-name': 'mock'}

        with self.assertRaisesRegex(TerminalErrorException, "Failed to extract event"):
            create_subscription_event_handler({}, MockLambdaContext)

    # - create_subscription_event_handler - invalid version
    @patch('lng_datalake_commons.sns_extractor.extract_sns_message')
    def test_create_subscription_event_handler_invalid_version(self, mock_extract_message):
        mock_extract_message.return_value = {'event-name': 'Subscription::Create', 'event-version': 0}

        with self.assertRaisesRegex(TerminalErrorException, "Failed to process event"):
            create_subscription_event_handler({}, MockLambdaContext)

    # - create_subscription_event_handler - invalid stage
    @patch('lng_datalake_commons.sns_extractor.extract_sns_message')
    def test_create_subscription_event_handler_invalid_stage(self, mock_extract_message):
        mock_extract_message.return_value = {'event-name': 'Subscription::Create', 'event-version': 1, 'stage': 'MOCK'}

        with self.assertRaisesRegex(TerminalErrorException, "Failed to process event"):
            create_subscription_event_handler({}, MockLambdaContext().invoked_function_arn)

    # +initiate_subscription - valid
    @patch('lng_datalake_commons.error_handling.error_handler.set_message_additional_attribute')
    @patch('create_subscription_event_handler.insert_to_subscription_table')
    @patch('lng_aws_clients.sns.get_client')
    def test_initiate_subscription_valid(self, mock_sns_client, mock_insert, mock_error_handler):
        message = io_util.load_data_json('subscription_record.json')
        mock_sns_client.return_value.subscribe.return_value = io_util.load_data_json("subscribe_response.json")
        mock_insert.return_value = None
        mock_error_handler.return_value = None
        with self.assertRaisesRegex(RetryEventException,
                                    "Initial creation of subscription, retrying to validate confirmation"):
            initiate_subscription(message)

        expected_calls = [call('pending-confirmation', True),
                          call('subscription-arn', "arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:"
                                                   "7157fd55-f110-46c2-9af6-9fed366b312b"),
                          call('retry-count', 0)]

        mock_error_handler.assert_has_calls(expected_calls, any_order=True)
        self.assertEqual(mock_error_handler.call_count, 3)

    # +create_sns_subscription - Valid - one schema version in the filter
    @patch('lng_aws_clients.sns.get_client')
    def test_create_sns_subscription_success(self, mock_sns_client):
        mock_sns_client.return_value.subscribe.return_value = io_util.load_data_json("subscribe_response.json")
        subscription_record = io_util.load_data_json("subscription_record.json")
        expected = io_util.load_data_json("subscribe_response.json")
        self.assertEqual(create_sns_subscription(subscription_record), expected)

        mock_sns_client.return_value.subscribe.assert_called_with(
            TopicArn=None, Protocol="sqs",
            Endpoint=subscription_record['endpoint'],
            ReturnSubscriptionArn=True,
            Attributes={
                'FilterPolicy': '{"event-name":["Object::Create"],"collection-id":["10000"],"notification-type":["event","99"],"schema-version":["v0"]}'})

    # +create_sns_subscription - Valid - two schema version in the filter
    @patch('lng_aws_clients.sns.get_client')
    def test_create_sns_subscription_success_two_schema_versions(self, mock_sns_client):
        mock_sns_client.return_value.subscribe.return_value = io_util.load_data_json("subscribe_response.json")
        subscription_record = io_util.load_data_json("subscription_record_2.json")
        expected = io_util.load_data_json("subscribe_response.json")
        self.assertEqual(create_sns_subscription(subscription_record), expected)

        mock_sns_client.return_value.subscribe.assert_called_with(
            TopicArn=None, Protocol="sqs",
            Endpoint=subscription_record['endpoint'],
            ReturnSubscriptionArn=True,
            Attributes={
                'FilterPolicy': '{"event-name":["Object::Create"],"collection-id":["10000"],"notification-type":["event","99"],"schema-version":["v0v1"]}'})

    # -create_sns_subscription - EndpointConnectionError
    @patch('lng_aws_clients.sns.get_client')
    def test_create_sns_subscription_endpoint_connection_error_exception(self, mock_sns_client):
        subscription_record = io_util.load_data_json("subscription_record.json")
        mock_sns_client.return_value.subscribe.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            create_sns_subscription(subscription_record)

    # -create_sns_subscription - Client Error
    @patch('lng_aws_clients.sns.get_client')
    def test_create_sns_subscription_client_error(self, mock_sns_client):
        subscription_record = io_util.load_data_json("subscription_record.json")
        mock_sns_client.return_value.subscribe.side_effect = ClientError({"ResponseMetadata": {},
                                                                          "Error": {"Code": "mock error code",
                                                                                    "Message": "mock error message"}},
                                                                         "client-error-mock")
        with self.assertRaises(ClientError):
            create_sns_subscription(subscription_record)

    # -create_sns_subscription - Exception
    @patch('lng_aws_clients.sns.get_client')
    def test_create_sns_subscription_exception(self, mock_sns_client):
        subscription_record = io_util.load_data_json("subscription_record.json")
        mock_sns_client.return_value.subscribe.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException,
                                    "An unknown error occurred when attempting to creation a subscription"):
            create_sns_subscription(subscription_record)

    # +create_subscription_insert_dict - valid
    def test_create_subscription_insert_dict_success(self):
        subscription_record = io_util.load_data_json("subscription_record.json")
        subscription_arn = "arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:7157fd55-f110-" \
                           "46c2-9af6-9fed366b312b"
        expected = io_util.load_data_json("insert_dict.json")
        self.assertEqual(create_subscription_insert_dict(subscription_record, subscription_arn), expected)

    # -create_subscription_insert_dict - missing required key
    def test_create_subscription_insert_dict_failure(self):
        subscription_record = {}
        subscription_arn = "arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:7157fd55-f110-46c2-9af6-" \
                           "9fed366b312b"
        with self.assertRaisesRegex(TerminalErrorException, "does not exist"):
            create_subscription_insert_dict(subscription_record, subscription_arn)

    # +insert_to_subscription_table - valid
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.put_item")
    def test_insert_to_subscription_table_success(self, put_item):
        put_item.return_value = None
        item = io_util.load_data_json("item.json")
        self.assertIsNone(insert_to_subscription_table(item))

    # -insert_to_subscription_table - EndpointConnectionError
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.put_item")
    def test_insert_to_subscription_table_failure_endpoint_connection_error_exception(self, put_item):
        item = io_util.load_data_json("item.json")
        put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            insert_to_subscription_table(item)

    # -insert_to_subscription_table - ClientError
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.put_item")
    def test_insert_to_subscription_table_failure_clienterror(self, put_item):
        item = io_util.load_data_json("item.json")
        put_item.side_effect = ClientError({"ResponseMetadata": {},
                                            "Error": {"Code": "mock error code",
                                                      "Message": "mock error message"}},
                                           "client-error-mock")
        with self.assertRaises(ClientError):
            insert_to_subscription_table(item)

    # -insert_to_subscription_table - Exception
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.put_item")
    def test_insert_to_subscription_table_failure_exception(self, put_item):
        item = io_util.load_data_json("item.json")
        put_item.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Failed to insert item"):
            insert_to_subscription_table(item)

    # + is_subscription_confirmed - Valid unconfirmed
    @patch('lng_aws_clients.sns.get_client')
    def test_is_subscription_confirmed_valid_unconfirmed(self, mock_sns_client):
        mock_sns_client.return_value.get_subscription_attributes.return_value = io_util.load_data_json(
            'get_sub_attributes_unconfirmed.json')
        message = {'additional-attributes': {
            'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:7157fd55-f110-46c2-'
                                '9af6-9fed366b312b'}}
        self.assertFalse(is_subscription_confirmed(message))

    # + is_subscription_confirmed - Valid unconfirmed deleted
    @patch('lng_aws_clients.sns.get_client')
    def test_is_subscription_confirmed_valid_unconfirmed_deleted(self, mock_sns_client):
        mock_sns_client.return_value.get_subscription_attributes.side_effect = \
            ClientError({'ResponseMetadata': {},
                         'Error': {
                             'Code': 'NotFound',
                             'Message': 'This is a mock'}},
                        "FAKE")
        message = {'additional-attributes': {
            'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:7157fd55-f110-46c2-'
                                '9af6-9fed366b312b'}}
        self.assertFalse(is_subscription_confirmed(message))

    # + is_subscription_confirmed - Valid confirmed
    @patch('lng_aws_clients.sns.get_client')
    def test_is_subscription_confirmed_valid_confirmed(self, mock_sns_client):
        mock_sns_client.return_value.get_subscription_attributes.return_value = io_util.load_data_json(
            'get_sub_attributes_confirmed.json')
        message = {'additional-attributes': {
            'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:7157fd55-f110-46c2-9af6-9fed366b312b'}}
        self.assertTrue(is_subscription_confirmed(message))

    # - is_subscription_confirmed - ClientError
    @patch('lng_aws_clients.sns.get_client')
    def test_is_subscription_confirmed_invalid_clienterror(self, mock_sns_client):
        mock_sns_client.return_value.get_subscription_attributes.side_effect = \
            ClientError({'ResponseMetadata': {},
                         'Error': {
                             'Code': 'Mock Error',
                             'Message': 'This is a mock'}},
                        "FAKE")
        message = {'additional-attributes': {
            'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:7157fd55-f110-46c2-'
                                '9af6-9fed366b312b'}}
        with self.assertRaises(ClientError):
            is_subscription_confirmed(message)

    # - is_subscription_confirmed - EndpointConnectionError
    @patch('lng_aws_clients.sns.get_client')
    def test_is_subscription_confirmed__endpoint_connection_error_exception(self, mock_sns_client):
        mock_sns_client.return_value.get_subscription_attributes.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')

        message = {'additional-attributes': {
            'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:7157fd55-f110-46c2-'
                                '9af6-9fed366b312b'}}
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            is_subscription_confirmed(message)

    # - is_subscription_confirmed - Exception
    @patch('lng_aws_clients.sns.get_client')
    def test_is_subscription_confirmed_invalid_exception(self, mock_sns_client):
        mock_sns_client.return_value.get_subscription_attributes.side_effect = Exception
        message = {'additional-attributes': {
            'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:feature-mas-DataLakeEvents:7157fd55-f110-46c2-'
                                '9af6-9fed366b312b'}}
        with self.assertRaisesRegex(TerminalErrorException, "General Exception Occurred"):
            is_subscription_confirmed(message)

    # +update_subscription_record - Valid
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.update_item")
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_success(self, get_item, update_item):
        get_item.return_value = io_util.load_data_json("item.json")
        update_item.return_value = None
        self.assertIsNone(update_subscription_record(99, "test_event"))

    # +update_subscription_record - valid: event-ids already exists
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.update_item")
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_success_2(self, get_item, update_item):
        get_item.return_value = io_util.load_data_json("item_with_event_ids.json")
        update_item.return_value = None
        self.assertIsNone(update_subscription_record(99, "test_event"))

    # -update_subscription_record - Exception on get
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_failure_exception_get(self, get_item):
        get_item.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Failed to get item"):
            update_subscription_record(99, "test_event")

    # -update_subscription_record - EndpointConnectionError on get
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_endpoint_connection_error_exception_get(self, get_item):
        get_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            update_subscription_record(99, "test_event")

    # -update_subscription_record - Client Error on get
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_failure_clienterror_get(self, get_item):
        get_item.side_effect = ClientError({'ResponseMetadata': {},
                                            'Error': {
                                                'Code': 'Unit Test',
                                                'Message': 'This is a mock'}},
                                           "FAKE")
        with self.assertRaises(ClientError):
            update_subscription_record(99, "test_event")

    # -update_subscription_record - EndpointConnectionError on update
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.update_item")
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_endpoint_connection_error_exception_update(self, get_item, update_item):
        update_item.return_value = None
        get_item.return_value = io_util.load_data_json("item.json")
        update_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            update_subscription_record(99, "test_event")

    # -update_subscription_record - Client Error on update
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.update_item")
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_failure_clienterror_update(self, get_item, update_item):
        update_item.return_value = None
        get_item.return_value = io_util.load_data_json("item.json")
        update_item.side_effect = ClientError({'ResponseMetadata': {},
                                               'Error': {
                                                   'Code': 'Unit Test',
                                                   'Message': 'This is a mock'}},
                                              "FAKE")
        with self.assertRaises(ClientError):
            update_subscription_record(99, "test_event")

    # - update_subscription_record - Exception raised on update
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.update_item")
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_failure_exception_update(self, get_item, update_item):
        get_item.return_value = io_util.load_data_json("item.json")
        update_item.side_effect = Exception
        with self.assertRaisesRegex(TerminalErrorException, "Failed to update item"):
            update_subscription_record(99, "test_event")

    # - update_subscription_record - No Subscription record
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_record_failure_no_record(self, get_item):
        get_item.return_value = None
        with self.assertRaisesRegex(TerminalErrorException, "Item in Subscription table was missing"):
            update_subscription_record(99, "test_event")

    # + retry_subscription_confirmation - Valid Event
    @patch('lng_aws_clients.sqs.get_client')
    def test_retry_subscription_confirmation(self, mock_sqs):
        mock_sqs.return_value.send_message.return_value = None
        event = io_util.load_data_json("pending_event.json")
        message = io_util.load_data_json("pending_event_message.json")
        self.assertIsNone(retry_subscription_confirmation(RetryEventException('Subscription has yet to be confirmed'),
                                                          event, message))

    # - retry_subscription_confirmation - Exception
    @patch('lng_aws_clients.sqs.get_client')
    def test_retry_subscription_confirmation_exception(self, mock_sqs):
        mock_sqs.return_value.send_message.side_effect = Exception
        event = io_util.load_data_json("pending_event.json")
        message = io_util.load_data_json("pending_event_message.json")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to send retry message'):
            retry_subscription_confirmation(RetryEventException('Subscription has yet to be confirmed'),
                                            event, message)

    def tearDown(self):  # NOSONAR
        self.session.stop()


if __name__ == '__main__':
    unittest.main()
