import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, command_validator
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, SemanticError, \
    NoSuchSubscription
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from update_subscription_command import lambda_handler, generate_response_json, update_subscription_command, \
    generate_event_dict, replace_op_commands

__author__ = "Prashant Srivastava, Jose Molinet"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateSubscription')


class TestUpdateSubscription(unittest.TestCase):
    session = None

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + lambda_handler - valid request updates name, filter, schema version
    @patch('lng_datalake_dal.catalog_table.CatalogTable.item_exists')
    @patch('lng_datalake_dal.collection_table.CollectionTable.item_exists')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    def test_lambda_handler_valid(self, mock_put_event, mock_put_tracking, mock_get_subscription, mock_authorize,
                                  mock_collection_exists, mock_catalog_exists):
        mock_put_event.return_value = None
        mock_put_tracking.return_value = None
        mock_authorize.return_value = None
        mock_catalog_exists.return_value = True
        mock_collection_exists.return_value = True
        mock_get_subscription.return_value = io_util.load_data_json('get_item_response.json')
        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response,
                                 lambda_handler(request_input, mock_lambda_context.MockLambdaContext()))
        mock_put_event.assert_called_with(io_util.load_data_json('valid_event.json'))

    # + update_subscription_command - update only subscription name
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_success(self, mock_get_subscription, mock_authorize, mock_required_props,
                                                 mock_optional_props, mock_request_time):
        mock_required_props.return_value = ["subscription-name", "subscription-state", "subscription-id", "protocol",
                                            "endpoint", "schema-version"]
        mock_optional_props.return_value = ["filter"]
        mock_get_subscription.return_value = io_util.load_data_json('get_item_response.json')
        mock_authorize.return_value = None
        mock_request_time.return_value = "2018-07-12T15:58:59.862Z"
        request_input = {
            "subscription-id": 15,
            "patch-operations": [
                {
                    "op": "replace",
                    "path": "/subscription-name",
                    "value": "test_subscription_1"
                }
            ]
        }
        expected_response = io_util.load_data_json('update_command_valid_response.json')
        self.assertEqual(expected_response,
                         update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId"))

    # - update_subscription_command - invalid path
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_invalid_path(self, mock_get_subscription, mock_authorize, mock_required_props,
                                                      mock_optional_props, mock_request_time):
        mock_required_props.return_value = ["subscription-name", "subscription-state", "subscription-id", "protocol",
                                            "endpoint", "schema-version"]
        mock_optional_props.return_value = ["filter"]
        mock_get_subscription.return_value = io_util.load_data_json('get_item_response.json')
        mock_authorize.return_value = None
        mock_request_time.return_value = "2018-07-12T15:58:59.862Z"
        request_input = {
            "subscription-id": 15,
            "patch-operations": [
                {
                    "op": "replace",
                    "path": "/invalid-path",
                    "value": "test_subscription_1"
                }
            ]
        }
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Valid operation paths are"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # TODO delete this when v0 schema is removed
    # - update_subscription_command - invalid filter for event-version
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_invalid_filter_for_event_version(self, mock_get_subscription, mock_authorize,
                                                                          mock_required_props,
                                                                          mock_optional_props, mock_request_time):
        mock_required_props.return_value = ["subscription-name", "subscription-state", "subscription-id", "protocol",
                                            "endpoint", "schema-version"]
        mock_optional_props.return_value = ["filter"]
        mock_get_subscription.return_value = io_util.load_data_json('get_item_response.json')
        mock_authorize.return_value = None
        mock_request_time.return_value = "2018-07-12T15:58:59.862Z"
        request_input = {
            "subscription-id": 15,
            "patch-operations": [
                {
                    "op": "replace",
                    "path": "/schema-version",
                    "value": ['v0', 'v1']
                }
            ]
        }
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(SemanticError,
                                    "Invalid filter for given event-version||v0 subscriptions do not allow filtering on Object::UpdateNoChange events"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - update_subscription_command - invalid op
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_invalid_op(self, mock_get_subscription, mock_authorize, mock_required_props,
                                                    mock_optional_props, mock_request_time):
        mock_required_props.return_value = ["subscription-name", "subscription-state", "subscription-id", "protocol",
                                            "endpoint", "schema-version"]
        mock_optional_props.return_value = ["filter"]
        mock_get_subscription.return_value = io_util.load_data_json('get_item_response.json')
        mock_authorize.return_value = None
        mock_request_time.return_value = "2018-07-12T15:58:59.862Z"
        request_input = {
            "subscription-id": 15,
            "patch-operations": [
                {
                    "op": "badOp",
                    "path": "/subscription-name",
                    "value": "test_subscription_1"
                }
            ]
        }
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Validation error for operation path /subscription-name||data.op must be one of ['replace']"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - update_subscription_command - invalid type
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_invalid_type(self, mock_get_subscription, mock_authorize, mock_required_props,
                                                      mock_optional_props, mock_request_time):
        mock_required_props.return_value = ["subscription-name", "subscription-state", "subscription-id", "protocol",
                                            "endpoint", "schema-version"]
        mock_optional_props.return_value = ["filter"]
        mock_get_subscription.return_value = io_util.load_data_json('get_item_response.json')
        mock_authorize.return_value = None
        mock_request_time.return_value = "2018-07-12T15:58:59.862Z"
        request_input = {
            "subscription-id": 15,
            "patch-operations": [
                {
                    "op": "replace",
                    "path": "/subscription-name",
                    "value": 1
                }
            ]
        }
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "400||Validation error for operation path /subscription-name||data.value must be string"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - update_subscription_command - Invalid subscription-id
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_invalid_id(self, mock_get_subscription):
        mock_get_subscription.return_value = {}
        request_input = {'subscription-id': 20,
                         "patch-operations": [
                             {
                                 "op": "replace",
                                 "path": "/subscription-name",
                                 "value": "test_subscription_1"
                             }
                         ]}
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(NoSuchSubscription, "Invalid Subscription ID 20"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - update_subscription_command - Subscription get_item -> ClientError
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_client_error(self, mock_get_subscription):
        mock_get_subscription.side_effect = ClientError({'ResponseMetadata': {},
                                                         'Error': {
                                                             'Code': 'Mock Error',
                                                             'Message': 'This is a mock'}},
                                                        "FAKE")
        request_input = {'subscription-id': 20,
                         "patch-operations": [
                             {
                                 "op": "replace",
                                 "path": "/subscription-name",
                                 "value": "test_subscription_1"
                             }
                         ]}
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(InternalError, "Unable to get item from Subscription Table"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - update_subscription_command - Subscription get_item -> General Exception
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_general_exception(self, mock_get_subscription):
        mock_get_subscription.side_effect = Exception
        request_input = {'subscription-id': 20,
                         "patch-operations": [
                             {
                                 "op": "replace",
                                 "path": "/subscription-name",
                                 "value": "test_subscription_1"
                             }
                         ]}
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - update_subscription_command - Pending subscription-state
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_pending_state(self, mock_get_subscription, mock_owner_authorization):
        mock_owner_authorization.return_value = None
        mock_get_subscription.return_value = io_util.load_data_json('get_item_response_pending.json')
        request_input = {'subscription-id': 20,
                         "patch-operations": [
                             {
                                 "op": "replace",
                                 "path": "/subscription-name",
                                 "value": "test_subscription_1"
                             }
                         ]}
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with self.assertRaisesRegex(SemanticError, "Subscription ID 20 is pending confirmation"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - update_subscription_command - no updates
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_update_subscription_command_no_changes(self, mock_get_subscription, mock_authorize):
        mock_authorize.return_value = None
        mock_get_subscription.return_value = io_util.load_data_json('get_item_response.json')
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        request_input = io_util.load_data_json('request_no_changes.json')
        with self.assertRaisesRegex(SemanticError, "No data changes"):
            update_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - replace_op_commands - invalid op
    def test_replace_op_commands(self):
        invalid_request = {"subscription-id": 15,
                           "patch-operations": [{
                               "op": "add",
                               "path": "/filter",
                               "value": {
                                   "collection-id": [
                                       "14"
                                   ],
                                   "event-name": [
                                       "Object::Update"
                                   ]}}]
                           }
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid operation verb supplied||Only replace is currently supported"):
            replace_op_commands(invalid_request)

    # - generate_response_json - failed
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_failure(self, mock_required_props, mock_optional_props):
        mock_required_props.return_value = ["subscription-name", "subscription-state", "subscription-id", "protocol",
                                            "endpoint", "schema-version"]
        mock_optional_props.return_value = ["filter"]

        subscription_record = {'subscription-id': 20}

        with self.assertRaisesRegex(InternalError, "Missing required attribute: subscription-name"):
            generate_response_json(subscription_record)

    # + generate_event_dict - success
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_event_dict(self, mock_request_time):
        mock_request_time.return_value = "2018-07-12T15:58:59.862Z"
        subscription_response = io_util.load_data_json("apigateway.response_valid.json")["response"]
        expected_event = io_util.load_data_json("valid_event.json")
        self.assertDictEqual(expected_event,
                             generate_event_dict(subscription_response, "563r1jug9i", "LATEST", True, True, True))


if __name__ == "__main__":
    unittest.main()
