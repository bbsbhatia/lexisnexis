import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, NoSuchSubscription, SemanticError, \
    InvalidRequestPropertyValue, NoSuchCollection, NoSuchCatalog, NoSuchChangeset
from lng_datalake_commons import validate
from lng_datalake_constants import event_names, changeset_status
from lng_datalake_testhelper.io_utils import IOUtils

from create_republish_command import get_subscription, validate_request_filter, get_valid_catalogs, \
    get_valid_collections, validate_changeset, generate_response_json, generate_event_dict, create_republish_command

__author__ = "John Konderla, Daniel Wang"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateRepublish')


class TestCreateRepublishCommand(unittest.TestCase):
    request_id = 'sample_request_id'
    stage = 'LATEST'
    api_key_id = 'testApiKeyId'

    def test_valid_input_json(self):
        # Data has all required attributes
        input_data = io_util.load_data_json('apigateway.input_valid.json')
        schema = io_util.load_schema_json("CreateRepublishCommand-RequestSchema.json")

        self.assertTrue(validate.is_valid_input_json(input_data, schema))

    def test_invalid_input_json(self):
        # Data has missing required attribute - invalid event-name
        input_data = io_util.load_data_json('apigateway.input_invalid.json')
        schema = io_util.load_schema_json("CreateRepublishCommand-RequestSchema.json")
        self.assertFalse(validate.is_valid_input_json(input_data, schema))

    # + create_republish_command: collections
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_create_republish_command_collections(self, mock_subscription_get_item, mock_owner_authorization,
                                                  mock_batch_get_collections, mock_pending_epoch,
                                                  mock_get_request_time):
        input_data = io_util.load_data_json('apigateway.input_valid.json')['request']
        mock_subscription_get_item.return_value = io_util.load_data_json('dynamo.subscription_data_valid.json')
        mock_owner_authorization.return_value = {}
        mock_batch_get_collections.return_value = io_util.load_data_json('dynamo.collection_data_valid.json')
        mock_pending_epoch.return_value = 1234567890
        expected_response_dict = io_util.load_data_json('apigateway.response_valid.json')
        expected_event_dict = io_util.load_data_json('event_store.response_valid.json')
        mock_get_request_time.return_value = '1970-01-01T00:00:00.000Z'
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json(
                       'CreateRepublishCommand-ResponseSchema.json')):
            response_dict = create_republish_command(input_data, self.request_id, self.stage,
                                                     self.api_key_id)
            for val in response_dict.values():
                val['collection-ids'].sort()
            self.assertDictEqual(response_dict['event-dict'], expected_event_dict)
            self.assertDictEqual(response_dict['response-dict'], expected_response_dict)

    # + valid get subscription
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_get_subscription_valid(self, mock_subscription_get_item):
        mock_subscription_record = {
            'subscription-id': 1,
            'filter': {
                'event-name': [
                    "Object::Create",
                    "Collection::Suspend"
                ],
                "collection-id": [
                    "test-collection"
                ]
            },
            'subscription-state': "Created",
            'owner-id': 1,
            "schema-version": ["v0"]
        }
        mock_subscription_get_item.return_value = io_util.load_data_json('dynamo.subscription_data_valid.json')
        self.assertDictEqual(get_subscription(1), mock_subscription_record)

    # - invalid get subscription: No record
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_get_subscription_no_record(self, mock_subscription_get_item):
        mock_subscription_record = {}
        mock_subscription_get_item.return_value = mock_subscription_record
        with self.assertRaisesRegex(NoSuchSubscription, 'Subscription 1 does not exist'):
            get_subscription(1)

    # - invalid get subscription: Pending record
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_get_subscription_terminated_subscription(self, mock_subscription_get_item):
        mock_subscription_record = {
            'subscription-id': 1,
            'filter': {
                'event-name': [
                    "Object::Create",
                    "Collection::Suspend"
                ],
                "collection-id": [
                    "test-collection"
                ]
            },
            'subscription-state': "Pending"
        }
        mock_subscription_get_item.return_value = mock_subscription_record
        with self.assertRaisesRegex(SemanticError, 'Subscription 1 is in Pending state'):
            get_subscription(1)

    # - invalid get subscription: Client Error
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_get_subscription_client_error(self, mock_subscription_get_item):
        mock_subscription_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                              'Error': {
                                                                  'Code': 'Unit Test',
                                                                  'Message': 'This is a mock'}},
                                                             "FAKE")
        with self.assertRaisesRegex(InternalError, 'Unable to get item from Subscription Table'):
            get_subscription(1)

    # - invalid get subscription: Exception
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    def test_get_subscription_general_exception(self, mock_subscription_get_item):
        mock_subscription_get_item.side_effect = Exception("Mock Error")
        with self.assertRaisesRegex(InternalError, 'Unhandled exception occurred'):
            get_subscription(1)

    # + valid collections
    @patch('create_republish_command.get_valid_collections')
    def test_validate_request_filter_collection(self, mock_get_valid_collections):
        sample_request = {
            "collection-ids": ["TestCollection1", "TestCollection2"]
        }
        mock_get_valid_collections.return_value = sample_request['collection-ids']

        self.assertDictEqual(validate_request_filter(sample_request), sample_request)

    # + valid catalogs:
    @patch('create_republish_command.get_valid_catalogs')
    def test_validate_request_filter_catalog(self, mock_get_valid_catalogs):
        sample_request = {
            "catalog-ids": ["TestCatalog1", "TestCatalog2"]
        }
        valid_filter = {
            "catalog-ids": ["TestCatalog1", "TestCatalog2"]
        }
        mock_get_valid_catalogs.return_value = sample_request['catalog-ids']
        self.assertDictEqual(validate_request_filter(sample_request), valid_filter)

    # - invalid: catalog and collection
    def test_validate_request_filter_invalid_catalog_collection(self):
        sample_request = {
            "catalog-ids": ["TestCatalog1", "TestCatalog2"],
            'collection-ids': ['TestCollection']
        }
        with self.assertRaisesRegex(
                InvalidRequestPropertyValue,
                'Request must only have one of the following: '
                'a list of Catalog IDs, a list of Collection IDs, or a Changeset ID'):
            validate_request_filter(sample_request)

    # - invalid: catalog and changeset
    def test_validate_request_filter_invalid_catalog_changeset(self):
        sample_request = {
            "catalog-ids": ["TestCatalog1", "TestCatalog2"],
            'changeset-id': ['TestChangeset']
        }
        with self.assertRaisesRegex(
                InvalidRequestPropertyValue,
                'Request must only have one of the following: '
                'a list of Catalog IDs, a list of Collection IDs, or a Changeset ID'):
            validate_request_filter(sample_request)

    # - invalid: collection and changeset
    def test_validate_request_filter_invalid_collection_changeset(self):
        sample_request = {
            'collection-ids': ['TestCollection'],
            'changeset-id': ['TestChangeset']
        }
        with self.assertRaisesRegex(
                InvalidRequestPropertyValue,
                'Request must only have one of the following: '
                'a list of Catalog IDs, a list of Collection IDs, or a Changeset ID'):
            validate_request_filter(sample_request)

    # - invalid: catalog and collection and changeset
    def test_validate_request_filter_invalid_all(self):
        sample_request = {
            "catalog-ids": ["TestCatalog1", "TestCatalog2"],
            'collection-ids': ['TestCollection'],
            'changeset-id': ['TestChangeset']
        }
        with self.assertRaisesRegex(
                InvalidRequestPropertyValue,
                'Request must only have one of the following: '
                'a list of Catalog IDs, a list of Collection IDs, or a Changeset ID'):
            validate_request_filter(sample_request)

    # - invalid: nothing to republish
    def test_validate_request_filter_invalid_no_collections_catalogs_changeset(self):
        sample_request = {
        }
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "No Catalog IDs or Collection IDs or Changeset ID to Republish"):
            validate_request_filter(sample_request)

    # + valid get_valid_catalogs
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_get_valid_catalogs(self, mock_batch_get_catalogs):
        mock_batch_get_catalogs.return_value = io_util.load_data_json('dynamo.catalog_data_valid.json')
        sample_catalogs = ["catalog-01", "catalog-02", "catalog-03"]
        self.assertListEqual(get_valid_catalogs(sample_catalogs), sample_catalogs)

    # - invalid get_valid_catalogs: Client Error
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_get_valid_catalogs_client_error(self, mock_batch_get_catalogs):
        mock_batch_get_catalogs.side_effect = ClientError({'ResponseMetadata': {},
                                                           'Error': {
                                                               'Code': 'Unit Test',
                                                               'Message': 'This is a mock'}},
                                                          "FAKE")
        sample_catalogs = ["catalog-01", "catalog-02", "catalog-03"]
        with self.assertRaisesRegex(InternalError, 'Unit Test'):
            get_valid_catalogs(sample_catalogs)

    # - invalid get_valid_catalogs: General Exception
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_get_valid_catalogs_general_exception(self, mock_batch_get_catalogs):
        mock_batch_get_catalogs.side_effect = Exception("Mock Exception")
        sample_catalogs = ["catalog-01", "catalog-02", "catalog-03"]
        with self.assertRaisesRegex(InternalError, 'Mock Exception'):
            get_valid_catalogs(sample_catalogs)

    # - invalid get_valid_catalogs: no catalogs
    @patch('lng_datalake_dal.catalog_table.CatalogTable.batch_get_items')
    def test_get_valid_catalogs_no_catalogs(self, mock_batch_get_catalogs):
        mock_batch_get_catalogs.return_value = []
        sample_catalogs = ["catalog-01", "catalog-02", "catalog-03"]
        with self.assertRaisesRegex(NoSuchCatalog, 'Invalid Catalog IDs'):
            get_valid_catalogs(sample_catalogs)

    ################# Collections ######################
    # + valid get_valid_collections
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_get_valid_collections(self, mock_batch_get_collections):
        mock_batch_get_collections.return_value = io_util.load_data_json('dynamo.collection_data_valid.json')
        sample_collections = ["274", "testCollection", "123"]
        self.assertListEqual(get_valid_collections(sample_collections), sample_collections)

    # - invalid get_valid_collections: Client Error
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_get_valid_collections_client_error(self, mock_batch_get_catalogs):
        mock_batch_get_catalogs.side_effect = ClientError({'ResponseMetadata': {},
                                                           'Error': {
                                                               'Code': 'Unit Test',
                                                               'Message': 'This is a mock'}},
                                                          "FAKE")
        sample_collections = ["274", "testCollection", "123"]
        with self.assertRaisesRegex(InternalError, 'Unit Test'):
            get_valid_collections(sample_collections)

    # - invalid get_valid_collections: General Exception
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_get_valid_collections_general_exception(self, mock_batch_get_collections):
        mock_batch_get_collections.side_effect = Exception("Mock Exception")
        sample_collections = ["274", "testCollection", "123"]
        with self.assertRaisesRegex(InternalError, 'Mock Exception'):
            get_valid_collections(sample_collections)

    # - invalid get_valid_collections: no collections
    @patch('lng_datalake_dal.collection_table.CollectionTable.batch_get_items')
    def test_get_valid_catalogs_no_collections(self, mock_batch_get_collections):
        mock_batch_get_collections.return_value = []
        sample_collections = ["274", "testCollection", "123"]
        with self.assertRaisesRegex(NoSuchCollection, 'Invalid Collection IDs'):
            get_valid_collections(sample_collections)

    ################# Changeset ######################
    # + valid get_valid_changeset
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_changeset(self, mock_get_changeset):
        mock_get_changeset.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': changeset_status.CLOSED
        }
        sample_changeset = "test-changeset"
        validate_changeset(sample_changeset)

    # - invalid get_changeset: Client Error
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_changeset_client_error(self, mock_get_changeset):
        mock_get_changeset.side_effect = ClientError({ 'ResponseMetaData': {},
                                           'Error': {
                                               'Code': 'Unit Test',
                                               'Message': 'This is a mock'}
                                           }, "FAKE")
        sample_changeset = "test-changeset"
        with self.assertRaisesRegex(InternalError, 'Unit Test'):
            validate_changeset(sample_changeset)

    # - invalid get_changeset: General Exception
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_changeset_general_exception(self, mock_get_changeset):
        mock_get_changeset.side_effect = Exception("Mock Exception")
        sample_changeset = "test-changeset"
        with self.assertRaisesRegex(InternalError, 'Mock Exception'):
            validate_changeset(sample_changeset)

    # - invalid get_changeset: no changeset
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_changeset_no_changeset(self, mock_get_changeset):
        mock_get_changeset.return_value = {}
        sample_changeset = 'test_changeset'
        with self.assertRaisesRegex(NoSuchChangeset, 'Invalid Changeset ID'):
            validate_changeset(sample_changeset)

    # - invalid get_changeset: not CLOSED state
    @patch('lng_datalake_dal.changeset_table.ChangesetTable.get_item')
    def test_validate_changeset_not_closed(self, mock_get_changeset):
        mock_get_changeset.return_value = {
            "changeset-id": 'jek-changeset',
            'owner-id': 100,
            'pending-expiration-epoch': 1582544504,
            'changeset-timestamp': '2019-10-03T16:09:33.861Z',
            'changeset-state': changeset_status.OPENED
        }
        sample_changeset = 'test-changeset'
        with self.assertRaisesRegex(SemanticError, 'Changeset must be in Closed State'):
            validate_changeset(sample_changeset)

    # + generate_response_json
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_response_json(self, mock_wrapper):
        validated_request = {
            "catalog-ids": ['cat1', 'cat2'],
            'event-name': [
                "Object::Create"
            ],
        }
        subscription_record = {
            'subscription-id': 1,
            "filter": {
                "event-name": [
                    "Object::Create",
                    "Object::Update"
                ]
            }
        }

        mock_wrapper.return_value = '2019-06-25T00:00:00.000Z'

        expected_response = {'catalog-ids': ['cat1', 'cat2'],
                             'republish-id': self.request_id,
                             'republish-state': 'Pending',
                             'republish-timestamp': '2019-06-25T00:00:00.000Z',
                             'subscription-id': 1,
                             'republish-expiration-date': '2009-02-13T23:31:30.000Z'
                             }
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json(
                       'CreateRepublishCommand-ResponseSchema.json')):
            self.assertDictEqual(generate_response_json(validated_request, subscription_record,
                                                        self.request_id, 1234567890),
                                 expected_response)

    # - generate_response_json: missing timestamp
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_response_json_missing_required_prop(self, mock_wrapper):
        validated_request = {
            "catalog-id": ['cat1', 'cat2']
        }
        subscription_record = {
            "filter": {
                "event-name": [
                    "Object::Create",
                    "Object::Update",
                ]
            }
        }

        mock_wrapper.return_value = '2019-06-25T00:00:00.000Z'
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json(
                       'CreateRepublishCommand-ResponseSchema.json')):
            with self.assertRaisesRegex(InternalError, 'Missing required attribute: subscription-id'):
                generate_response_json(validated_request, subscription_record, self.request_id, 1234567890)

    # + generate_event_dict
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_event_dict(self, mock_wrapper):
        republish_record = {
            'subscription-id': 1,
            'catalog-ids': [
                'cat-1',
                'cat2'
            ]
        }
        stage = "LATEST"
        mock_wrapper.return_value = '2019-06-25T00:00:00.000Z'
        expected_event = {
            'pending-expiration-epoch': 1234567890,
            'subscription-id': 1,
            'event-id': self.request_id,
            'republish-id': self.request_id,
            'stage': stage,
            'event-version': 1,
            'request-time': '2019-06-25T00:00:00.000Z',
            'event-name': event_names.SUBSCRIPTION_REPUBLISH_CREATE,
            'catalog-ids': [
                'cat-1',
                'cat2'
            ],
            "schema-version": ["v0"]
        }
        with patch('create_republish_command.event_version', 1):
            self.assertEqual(generate_event_dict(republish_record, self.request_id, stage, 1234567890, ['v0']),
                             expected_event)


if __name__ == '__main__':
    unittest.main()
