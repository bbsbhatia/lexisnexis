import json
import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import subscription_status, event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils

from remove_subscription_event_handler import remove_subscription_event_handler, \
    remove_sns_subscription, remove_subscription_record

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveSubscriptionEventHandler')


class TestRemoveSubscriptionEventHandler(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls): #NOSONAR
        cls.session.stop()

    # + remove_subscription_handler - Success
    @patch('remove_subscription_event_handler.remove_subscription_record')
    @patch('remove_subscription_event_handler.remove_sns_subscription')
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_commons.sns_extractor.extract_sns_message')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_handler(self, mock_session,
                                         mock_extract_sns_message,
                                         mock_is_valid_message,
                                         mock_is_valid_event_version,
                                         mock_get_item,
                                         mock_remove_sns_subscription,
                                         mock_remove_subscription_record):
        mock_session.return_value = None
        mock_extract_sns_message.return_value = json.loads(
            io_util.load_data_json("event.json")['Records'][0]['Sns']['Message'])
        mock_is_valid_message.return_value = True
        mock_is_valid_event_version.return_value = True
        mock_get_item.return_value = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        mock_remove_sns_subscription.return_value = None
        mock_remove_subscription_record.return_value = None

        event = io_util.load_data_json("event.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         remove_subscription_event_handler(event,
                                                           "arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST"))

    # + remove_subscription_handler - Success No subscription record
    @patch('remove_subscription_event_handler.remove_subscription_record')
    @patch('remove_subscription_event_handler.remove_sns_subscription')
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_commons.sns_extractor.extract_sns_message')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_handler_no_record(self, mock_session,
                                         mock_extract_sns_message,
                                         mock_is_valid_message,
                                         mock_is_valid_event_version,
                                         mock_get_item,
                                         mock_remove_sns_subscription,
                                         mock_remove_subscription_record):
        mock_session.return_value = None
        mock_extract_sns_message.return_value = json.loads(
            io_util.load_data_json("event.json")['Records'][0]['Sns']['Message'])
        mock_is_valid_message.return_value = True
        mock_is_valid_event_version.return_value = True
        mock_get_item.return_value = {}
        mock_remove_sns_subscription.return_value = None
        mock_remove_subscription_record.return_value = None

        event = io_util.load_data_json("event.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         remove_subscription_event_handler(event,
                                                           "arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST"))
        mock_remove_sns_subscription.assert_not_called()
        mock_remove_subscription_record.assert_not_called()

    # - remove_subscription_handler - Failure
    @patch('remove_subscription_event_handler.remove_subscription_record')
    @patch('remove_subscription_event_handler.remove_sns_subscription')
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_commons.sns_extractor.extract_sns_message')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_handler_invalid_message(self, mock_session,
                                                         mock_extract_sns_message,
                                                         mock_is_valid_message,
                                                         mock_is_valid_event_version,
                                                         mock_get_item,
                                                         mock_remove_sns_subscription,
                                                         mock_remove_subscription_record):
        mock_session.return_value = None
        mock_extract_sns_message.return_value = json.loads(
            io_util.load_data_json("event.json")['Records'][0]['Sns']['Message'])
        mock_is_valid_message.return_value = False
        mock_is_valid_event_version.return_value = True
        mock_get_item.return_value = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        mock_remove_sns_subscription.return_value = None
        mock_remove_subscription_record.return_value = None

        event = io_util.load_data_json("event.json")
        with self.assertRaises(TerminalErrorException):
            remove_subscription_event_handler(event, "arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST")

    # - remove_subscription_handler - Failure
    @patch('remove_subscription_event_handler.remove_subscription_record')
    @patch('remove_subscription_event_handler.remove_sns_subscription')
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_commons.sns_extractor.extract_sns_message')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_handler_invalid_event_version(self, mock_session,
                                                               mock_extract_sns_message,
                                                               mock_is_valid_message,
                                                               mock_is_valid_event_version,
                                                               mock_get_item,
                                                               mock_remove_sns_subscription,
                                                               mock_remove_subscription_record):
        mock_session.return_value = None
        mock_extract_sns_message.return_value = json.loads(
            io_util.load_data_json("event.json")['Records'][0]['Sns']['Message'])
        mock_is_valid_message.return_value = True
        mock_is_valid_event_version.return_value = False
        mock_get_item.return_value = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        mock_remove_sns_subscription.return_value = None
        mock_remove_subscription_record.return_value = None

        event = io_util.load_data_json("event.json")
        with self.assertRaises(TerminalErrorException):
            remove_subscription_event_handler(event, "arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST")

    # - remove_subscription_handler - Failure
    @patch('remove_subscription_event_handler.remove_subscription_record')
    @patch('remove_subscription_event_handler.remove_sns_subscription')
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    @patch('lng_datalake_commons.validate.is_valid_event_stage')
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    @patch('lng_datalake_commons.sns_extractor.extract_sns_message')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_handler_invalid_event_stage(self, mock_session,
                                                             mock_extract_sns_message,
                                                             mock_is_valid_message,
                                                             mock_is_valid_event_version,
                                                             mock_is_valid_event_stage,
                                                             mock_get_item,
                                                             mock_remove_sns_subscription,
                                                             mock_remove_subscription_record):
        mock_session.return_value = None
        mock_extract_sns_message.return_value = json.loads(
            io_util.load_data_json("event.json")['Records'][0]['Sns']['Message'])
        mock_is_valid_message.return_value = True
        mock_is_valid_event_version.return_value = True
        mock_is_valid_event_stage.return_value = False
        mock_get_item.return_value = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        mock_remove_sns_subscription.return_value = None
        mock_remove_subscription_record.return_value = None

        event = io_util.load_data_json("event.json")
        with self.assertRaises(TerminalErrorException):
            remove_subscription_event_handler(event, "arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST")

    # + remove_sns_subscription - Success
    @patch('lng_aws_clients.sns.get_client')
    def test_remove_sns_subscription(self, mock_sns_client):
        mock_sns_client.return_value.unsubscribe.return_value = None
        subscription_arn = 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6'
        self.assertIsNone(remove_sns_subscription(subscription_arn))

    # - remove_sns_subscription - Failure
    @patch('lng_aws_clients.sns.get_client')
    def test_remove_sns_subscription_failure(self, mock_sns_client):
        subscription_arn = 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6'
        for side_effect in [ClientError({"ResponseMetadata": {},
                                         "Error": {"Code": "mock error code",
                                                   "Message": "mock error message"}},
                                        "client-error-mock"), EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com'), Exception]:
            mock_sns_client.return_value.unsubscribe.side_effect = side_effect
            if type(side_effect) == EndpointConnectionError:
                with self.assertRaises(EndpointConnectionError):
                    remove_sns_subscription(subscription_arn)
            elif type(side_effect) == ClientError:
                with self.assertRaises(ClientError):
                    remove_sns_subscription(subscription_arn)
            elif side_effect == Exception:
                with self.assertRaises(TerminalErrorException):
                    remove_sns_subscription(subscription_arn)

    # + remove_subscription_record - Success
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.delete_item")
    def test_remove_subscription_record(self, mock_delete_item):
        mock_delete_item.return_value = None
        self.assertIsNone(remove_subscription_record(20))

    # - remove_subscription_record - Failure
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.delete_item")
    def test_remove_subscription_record_failure(self, mock_delete_item):
        for side_effect in [ClientError({"ResponseMetadata": {},
                                         "Error": {"Code": "mock error code",
                                                   "Message": "mock error message"}},
                                        "client-error-mock"), EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com'), Exception]:
            mock_delete_item.side_effect = side_effect
            if type(side_effect) == EndpointConnectionError:
                with self.assertRaises(EndpointConnectionError):
                    remove_subscription_record(20)
            if type(side_effect) == ClientError:
                with self.assertRaises(ClientError):
                    remove_subscription_record(20)
            elif side_effect == Exception:
                with self.assertRaises(TerminalErrorException):
                    remove_subscription_record(20)


if __name__ == "__main__":
    unittest.main()
