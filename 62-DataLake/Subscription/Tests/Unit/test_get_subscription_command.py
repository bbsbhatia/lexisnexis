import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NoSuchSubscription
from lng_datalake_constants import subscription_status
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from get_subscription_command import lambda_handler, get_subscription_command, generate_response_json

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetSubscription')


class TestGetSubscription(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + lambda_handler - valid request
    @patch('get_subscription_command.get_subscription_command')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_valid(self, mock_session,
                                  mock_get_subscription_command):
        mock_session.return_value = None
        mock_get_subscription_command.return_value = \
            {'response-dict': {'subscription-id': 20,
                               'subscription-name': 'test_subscription',
                               'subscription-state': subscription_status.CREATED,
                               'protocol': 'email',
                               'endpoint': 'test_user@lexisnexis.com',
                               "schema-version": ["v0"]
                               }
             }

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.maxDiff = None
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response,
                                 lambda_handler(request_input, mock_lambda_context.MockLambdaContext()))

    # + get_subscription_command - valid subscription-id
    @patch('get_subscription_command.generate_response_json')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_subscription_command(self, mock_session,
                                      mock_subscription_table_get_item,
                                      mock_generate_response_json):
        mock_session.return_value = None
        mock_subscription_table_get_item.return_value = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                "schema-version": ["v0"],
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        generated_response = {
            'subscription-id': 20,
            'subscription-name': 'test_subscription',
            'subscription-state': subscription_status.CREATED,
            'protocol': 'email',
            'endpoint': 'test_user@lexisnexis.com',
            'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
            "schema-version": ["v0"],
        }

        mock_generate_response_json.return_value = generated_response

        expected_response = \
            {
                'response-dict': generated_response

            }
        request_input = {'subscription-id': 20}
        self.assertDictEqual(expected_response, get_subscription_command(request_input))

    # - get_subscription_command - Invalid subscription-id
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_subscription_command_failure_1(self, mock_session,
                                                mock_subscription_table_get_item):
        mock_session.return_value = None
        mock_subscription_table_get_item.return_value = {}
        request_input = {'subscription-id': 20}
        with self.assertRaisesRegex(NoSuchSubscription, "Invalid Subscription ID 20"):
            get_subscription_command(request_input)

    # - get_subscription_command - raise errors
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_subscription_command_failure_3(self, mock_session,
                                                mock_subscription_table_get_item):
        mock_session.return_value = None
        request_input = {'subscription-id': 20}

        for side_effect in [ClientError({"ResponseMetadata": {},
                                         "Error": {"Code": "mock error code",
                                                   "Message": "mock error message"}},
                                        "client-error-mock"), Exception]:
            mock_subscription_table_get_item.side_effect = side_effect
            if type(side_effect) == ClientError:
                with self.assertRaisesRegex(InternalError, "Unable to get item from Subscription Table"):
                    get_subscription_command(request_input)
            elif side_effect == Exception:
                with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
                    get_subscription_command(request_input)

    # + generate_response_json - success
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json(self, mock_required_props,
                                    mock_optional_props):
        mock_required_props.return_value = ['subscription-name', 'subscription-id', 'subscription-state', 'protocol',
                                            'endpoint', 'schema-version']
        mock_optional_props.return_value = ['subscription-arn']

        subscription_record = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                "schema-version": ["v0"],
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        expected_response = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                "schema-version": ["v0"]
            }
        self.assertDictEqual(expected_response, generate_response_json(subscription_record))

    # - generate_response_json - failed
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_failure(self, mock_required_props,
                                            mock_optional_props):
        mock_required_props.return_value = ['subscription-name', 'subscription-id', 'subscription-state', 'protocol',
                                            'endpoint', 'schema-version', 'dummy_property']
        mock_optional_props.return_value = []

        subscription_record = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                "schema-version": ["v0"],
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }

        with self.assertRaisesRegex(InternalError, "Missing required property: dummy_property"):
            generate_response_json(subscription_record)


if __name__ == "__main__":
    unittest.main()
