import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, RetryEventException
from lng_datalake_constants import republish_status, event_handler_status
from lng_datalake_dal.exceptions import ConditionError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_republish_event_handler import create_republish_event_handler, insert_republish_record, \
    create_republish_record, insert_republish_collection_records, insert_collections_from_changeset, \
    get_collection_batch_from_changeset, republish_collection_batch_write, set_republish_state, insert_collections, \
    insert_collections_from_catalogs, get_collection_batch_from_catalog, start_state_machine

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateRepublishEventHandler')


class TestCreateRepublishEventHandler(TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        cls.get_remaining_in_millis = patch('create_republish_event_handler.get_remaining_time_in_millis')
        cls.get_remaining_in_millis.return_value = MockLambdaContext.get_remaining_time_in_millis()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + create_republish_event_handler: catalogs
    @patch('lng_aws_clients.session.client')
    @patch('create_republish_event_handler.republish_collection_batch_write')
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_datalake_dal.republish_table.RepublishTable.put_item')
    def test_create_republish_event_handler_catalogs(self, mock_republish_put_item, mock_query_items, mock_batch_write,
                                                     mock_client):
        valid_catalog_event_message = io_util.load_data_json('valid_catalog_event_message.json')
        mock_republish_put_item.return_value = None
        mock_query_items.return_value = [{'collection-id': 'coll-1'}, {'collection-id': 'coll-2'}]
        mock_batch_write.return_value = 2

        mock_client.return_value.client.return_value.start_execution.return_value = None

        self.assertEqual(create_republish_event_handler(valid_catalog_event_message,
                                                        MockLambdaContext.invoked_function_arn,
                                                        MockLambdaContext.get_remaining_time_in_millis),
                         event_handler_status.SUCCESS)

    # + create_republish_event_handler: collections
    @patch('lng_aws_clients.session.client')
    @patch('create_republish_event_handler.republish_collection_batch_write')
    @patch('lng_datalake_dal.republish_table.RepublishTable.put_item')
    def test_create_republish_event_handler_collections(self, mock_republish_put_item, mock_batch_write,
                                                        mock_client):
        valid_catalog_event_message = io_util.load_data_json('valid_collections_event_message.json')
        mock_republish_put_item.return_value = None
        mock_batch_write.return_value = 2

        self.assertEqual(create_republish_event_handler(valid_catalog_event_message,
                                                        MockLambdaContext.invoked_function_arn,
                                                        MockLambdaContext.get_remaining_time_in_millis),
                         event_handler_status.SUCCESS)

    # + create_republish_event_handler: retry catalogs
    @patch('lng_aws_clients.session.client')
    @patch('create_republish_event_handler.republish_collection_batch_write')
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_datalake_dal.republish_table.RepublishTable.update_item')
    def test_create_republish_event_handler_retry_catalogs(self, mock_republish_put_item, mock_query_items,
                                                           mock_batch_write, mock_client):
        valid_catalog_event_message = io_util.load_data_json('valid_retry_catalog_event_message.json')
        mock_republish_put_item.return_value = None
        mock_query_items.return_value = [{'collection-id': 'coll-1'}, {'collection-id': 'coll-2'}]
        mock_batch_write.return_value = 2

        mock_client.return_value.client.return_value.start_execution.return_value = None

        self.assertEqual(create_republish_event_handler(valid_catalog_event_message,
                                                        MockLambdaContext.invoked_function_arn,
                                                        MockLambdaContext.get_remaining_time_in_millis),
                         event_handler_status.SUCCESS)

    # + create_republish_event_handler: no collections in catalogs
    @patch('lng_aws_clients.session.client')
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    @patch('lng_datalake_dal.republish_table.RepublishTable.update_item')
    def test_create_republish_event_handler_no_collections_in_catalogs(self, mock_republish_put_item,
                                                                       mock_query_items,
                                                                       mock_client):
        valid_catalog_event_message = io_util.load_data_json('valid_catalog_event_message.json')
        mock_republish_put_item.return_value = None
        mock_query_items.return_value = []

        mock_client.return_value.client.return_value.start_execution.return_value = None

        self.assertEqual(create_republish_event_handler(valid_catalog_event_message,
                                                        MockLambdaContext.invoked_function_arn,
                                                        MockLambdaContext.get_remaining_time_in_millis),
                         event_handler_status.SUCCESS)

    # - create_republish_event_handler: invalid event message
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    def test_create_republish_event_handler_invalid_message(self, is_valid_message):
        valid_catalog_event_message = io_util.load_data_json('valid_retry_catalog_event_message.json')
        is_valid_message.return_value = False

        with self.assertRaisesRegex(TerminalErrorException, 'event-name doesnt match'):
            create_republish_event_handler(valid_catalog_event_message,
                                           MockLambdaContext.invoked_function_arn,
                                           MockLambdaContext.get_remaining_time_in_millis)

    # - create_republish_event_handler: invalid event message
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    def test_create_republish_event_handler_invalid_message_version(self, is_valid_message):
        valid_catalog_event_message = io_util.load_data_json('valid_retry_catalog_event_message.json')
        is_valid_message.return_value = False

        with self.assertRaisesRegex(TerminalErrorException, 'event-version does not match'):
            create_republish_event_handler(valid_catalog_event_message,
                                           MockLambdaContext.invoked_function_arn,
                                           MockLambdaContext.get_remaining_time_in_millis)

    # - create_republish_event_handler: invalid event message
    @patch('lng_datalake_commons.validate.is_valid_event_stage')
    def test_create_republish_event_handler_invalid_message_stage(self, is_valid_message):
        valid_catalog_event_message = io_util.load_data_json('valid_retry_catalog_event_message.json')
        is_valid_message.return_value = False

        with self.assertRaisesRegex(TerminalErrorException, 'stage not match'):
            create_republish_event_handler(valid_catalog_event_message,
                                           MockLambdaContext.invoked_function_arn,
                                           MockLambdaContext.get_remaining_time_in_millis)

    # + create_republish_record
    @patch('lng_datalake_dal.republish_table.RepublishTable.put_item')
    def test_insert_republish_record(self, mock_republish_put_item):
        message = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "event-id": "gqZpHORIYuGaLlic",
            "republish-id": "gqZpHORIYuGaLlic",
            "event-name": 'Subscription::RepublishCreate',
            "event-version": 1,
            "stage": "LATEST",
            "request-time": "2019-07-12T20:45:16.386Z"
        }

        republish_record = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            'event-ids': ['gqZpHORIYuGaLlic'],
            "republish-id": "gqZpHORIYuGaLlic",
            "republish-timestamp": "2019-07-12T20:45:16.386Z",
            'republish-state': 'Pending'

        }
        mock_republish_put_item.return_value = None
        self.assertDictEqual(insert_republish_record(message), republish_record)

    # - create_republish_record:EndpointConnectionError
    @patch('lng_datalake_dal.republish_table.RepublishTable.put_item')
    def test_insert_republish_record_endpoint_connection_error_exception(self, mock_republish_put_item):
        message = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "event-id": "gqZpHORIYuGaLlic",
            "republish-id": "gqZpHORIYuGaLlic",
            "event-name": 'Subscription::RepublishCreate',
            "event-version": 1,
            "stage": "LATEST",
            "request-time": "2019-07-12T20:45:16.386Z"
        }
        mock_republish_put_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            insert_republish_record(message)

    # - create_republish_record: client error
    @patch('lng_datalake_dal.republish_table.RepublishTable.put_item')
    def test_insert_republish_record_client_error(self, mock_republish_put_item):
        message = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "event-id": "gqZpHORIYuGaLlic",
            "republish-id": "gqZpHORIYuGaLlic",
            "event-name": 'Subscription::RepublishCreate',
            "event-version": 1,
            "stage": "LATEST",
            "request-time": "2019-07-12T20:45:16.386Z"
        }
        mock_republish_put_item.side_effect = ClientError({'ResponseMetadata': {},
                                                           'Error': {
                                                               'Code': 'MockClientError',
                                                               'Message': 'This is a mock'}},
                                                          "FAKE")
        with self.assertRaisesRegex(ClientError, 'MockClientError'):
            insert_republish_record(message)

    # - create_republish_record: condition error
    @patch('lng_datalake_dal.republish_table.RepublishTable.put_item')
    def test_insert_republish_record_condition_error(self, mock_republish_put_item):
        message = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "event-id": "gqZpHORIYuGaLlic",
            "republish-id": "gqZpHORIYuGaLlic",
            "event-name": 'Subscription::RepublishCreate',
            "event-version": 1,
            "stage": "LATEST",
            "request-time": "2019-07-12T20:45:16.386Z"
        }
        mock_republish_put_item.side_effect = ConditionError({'ResponseMetadata': {},
                                                              'Error': {
                                                                  'Code': 'NotFound',
                                                                  'Message': 'This is a mock'}},
                                                             "FAKE")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to insert item into Republish table.'):
            insert_republish_record(message)

    # - create_republish_record: general exception
    @patch('lng_datalake_dal.republish_table.RepublishTable.put_item')
    def test_insert_republish_record_general_exception(self, mock_republish_put_item):
        message = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "event-id": "gqZpHORIYuGaLlic",
            "republish-id": "gqZpHORIYuGaLlic",
            "event-name": 'Subscription::RepublishCreate',
            "event-version": 1,
            "stage": "LATEST",
            "request-time": "2019-07-12T20:45:16.386Z"
        }
        mock_republish_put_item.side_effect = Exception()
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to insert item into Republish table due to unknown reasons.'):
            insert_republish_record(message)

    # - create_republish_record: no subscription
    def test_create_republish_record_no_subscription(self):
        message = {
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "event-id": "gqZpHORIYuGaLlic",
            "republish-id": "gqZpHORIYuGaLlic",
            "event-name": 'Subscription::RepublishCreate',
            "event-version": 1,
            "stage": "LATEST",
            "request-time": "2019-07-12T20:45:16.386Z"
        }
        with self.assertRaisesRegex(TerminalErrorException,
                                    "Required Key = 'subscription-id' does not exist in the message."):
            create_republish_record(message)

    # + insert_republish_collection_records
    @patch('create_republish_event_handler.insert_republish_collection_records')
    def test_insert_collections(self, mock_batch_write):
        collection_list = ['coll-1', 'coll-2']
        republish_record = {
            'pending-expiration-epoch': 1234567890,
            'subscription-id': 1,
            'republish-id': '1234567890'
        }
        mock_batch_write.return_value = 2
        self.assertEqual(
            insert_collections(collection_list, republish_record, 0), 2)

    # - insert_republish_collection_records: retry
    @patch('create_republish_event_handler.set_republish_state')
    @patch('create_republish_event_handler.insert_republish_collection_records')
    def test_insert_republish_collection_records_retry(self, mock_batch_write, mock_retry):
        collection_list = ['coll-1', 'coll-2']
        republish_record = {
            'pending-expiration-epoch': 1234567890,
            'subscription-id': 1,
            'republish-id': '1234567890'
        }
        mock_retry.return_value = None
        mock_batch_write.return_value = 0
        with self.assertRaisesRegex(RetryEventException, 'Retry event to finish the update with the unprocessed'):
            insert_collections(collection_list, republish_record, 0)

    # - insert_republish_collection_records: retry event
    @patch('create_republish_event_handler.republish_collection_batch_write')
    def test_insert_collections_retry(self, mock_batch_write):
        collection_list = ['coll-1', 'coll-2']

        republish_record = {
            'pending-expiration-epoch': 1234567890,
            'subscription-id': 1,
            'republish-id': '1234567890'
        }
        mock_batch_write.return_value = 1
        self.assertEqual(insert_republish_collection_records(collection_list, republish_record), 1)

    # + insert_collections_from_catalogs
    @patch('create_republish_event_handler.insert_republish_collection_records')
    @patch('create_republish_event_handler.get_collection_batch_from_catalog')
    def test_insert_collections_from_catalogs(self, mock_get_batch_collection, mock_batch_write):
        catalog_list = ['cat-1', 'cat-2']
        mock_get_batch_collection.side_effect = [['coll-1', 'coll-2'], ['coll-3', 'coll-4', 'coll-5']]

        republish_record = {
            'pending-expiration-epoch': 1234567890,
            'subscription-id': 1,
            'republish-id': '1234567890'
        }
        mock_batch_write.side_effect = [2, 3]
        self.assertEqual(insert_collections_from_catalogs(catalog_list, republish_record, '', 0), 5)

    # - insert_collections_from_catalogs: throttled read retry
    @patch('create_republish_event_handler.set_republish_state')
    @patch('create_republish_event_handler.get_collection_batch_from_catalog')
    def test_insert_collections_from_catalogs_client_error(self, mock_get_batch_collection,
                                                           mock_retry):
        catalog_list = ['cat-1', 'cat-2']
        mock_get_batch_collection.side_effect = ClientError({'ResponseMetadata': {},
                                                             'Error': {
                                                                 'Code': 'MockClientError',
                                                                 'Message': 'This is a mock'}},
                                                            "FAKE")

        republish_record = {
            'pending-expiration-epoch': 1234567890,
            'subscription-id': 1,
            'republish-id': '1234567890'
        }
        mock_retry.return_value = None
        with self.assertRaisesRegex(ClientError, 'MockClientError'):
            insert_collections_from_catalogs(catalog_list, republish_record, '', 0)

    # - insert_collections_from_catalogs: retry
    @patch('create_republish_event_handler.set_republish_state')
    @patch('create_republish_event_handler.insert_republish_collection_records')
    @patch('create_republish_event_handler.get_collection_batch_from_catalog')
    def test_insert_collections_from_catalogs_retry(self, mock_get_batch_collection, mock_batch_write, mock_retry):
        catalog_list = ['cat-1', 'cat-2']
        mock_get_batch_collection.side_effect = [['coll-1', 'coll-2'], ['coll-3', 'coll-4', 'coll-5']]

        republish_record = {
            'pending-expiration-epoch': 1234567890,
            'subscription-id': 1,
            'republish-id': '1234567890'
        }
        mock_batch_write.side_effect = [2, 2]
        mock_retry.return_value = None
        with self.assertRaisesRegex(RetryEventException,
                                    'Retry event to finish the update with the unprocessed catalog ids'):
            insert_collections_from_catalogs(catalog_list, republish_record, '', 0)

    # + get_collection_batch_from_catalog
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    def test_get_collection_batch_from_catalog(self, mock_query_items):
        mock_query_items.return_value = [{'collection-id': 'coll-1'}, {'collection-id': 'coll-2'}]
        collections = ['coll-1', 'coll-2']
        fake_pagination_token = 'eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ29sbGVjdGlvbklEIjogeyJTIjogIjk5In'
        self.assertEqual(get_collection_batch_from_catalog('cat-1', fake_pagination_token), collections)

    # - get_collection_batch_from_catalog: EndpointConnectionError
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    def test_get_collection_batch_from_catalog_endpoint_connection_error_exception(self, mock_query_items):
        mock_query_items.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        fake_pagination_token = 'eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ29sbGVjdGlvbklEIjogeyJTIjogIjk5In'
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            get_collection_batch_from_catalog('cat-1', fake_pagination_token)

    # - get_collection_batch_from_catalog: Client Error
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    def test_get_collection_batch_from_catalog_client_error(self, mock_query_items):
        mock_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'MockClientError',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        fake_pagination_token = 'eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ29sbGVjdGlvbklEIjogeyJTIjogIjk5In'
        with self.assertRaisesRegex(ClientError, 'MockClientError'):
            get_collection_batch_from_catalog('cat-1', fake_pagination_token)

    # - get_collection_batch_from_catalog: Exception
    @patch('lng_datalake_dal.catalog_collection_mapping_table.CatalogCollectionMappingTable.query_items')
    def test_get_collection_batch_from_catalog_general_exception(self, mock_query_items):
        mock_query_items.side_effect = Exception("FakeException")

        fake_pagination_token = 'eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ29sbGVjdGlvbklEIjogeyJTIjogIjk5In'
        with self.assertRaisesRegex(TerminalErrorException, 'General exception occurred when getting records'):
            get_collection_batch_from_catalog('cat-1', fake_pagination_token)

    # + insert_collections_from_changeset
    @patch('create_republish_event_handler.get_collection_batch_from_changeset')
    @patch('create_republish_event_handler.insert_republish_collection_records')
    def test_insert_collections_from_changeset_success(self, mock_batch_write, mock_get_batch_collection):
        mock_get_batch_collection.side_effect = [['coll-1', 'coll-2', 'coll-3']]
        mock_batch_write.return_value = 3
        changeset_id = 'test-changeset'
        republish_record = {
            'republish-id': 'test-republish',
            'changeset-id': 'test-changeset',
            'pending-expiration-epoch': 1234567890,
            'republish-state': 'Pending',
            'republish-timestamp': "2019-01-01T01:01:00.000Z",
            "subscription-id": 1,
            'event-ids': ['test-republish']

        }

        self.assertEqual(insert_collections_from_changeset(changeset_id, republish_record, '', 0), 3)

    # - insert_collections_from_changeset: throttled read retry
    @patch('lng_datalake_dal.changeset_collection_table.ChangesetCollectionTable.get_pagination_token')
    @patch('create_republish_event_handler.set_republish_state')
    @patch('create_republish_event_handler.get_collection_batch_from_changeset')
    def test_insert_collections_from_changeset_client_error(self, mock_get_batch_collection,
                                                            mock_retry, mock_get_pagination_token):
        mock_get_batch_collection.side_effect = ClientError({'ResponseMetadata': {},
                                                             'Error': {
                                                                 'Code': 'MockClientError',
                                                                 'Message': 'This is a mock'}},
                                                            "FAKE")
        mock_get_pagination_token.return_value = 'fake-page'
        mock_retry.return_value = None
        changeset_id = 'test-changeset'
        republish_record = {
            'republish-id': 'test-republish',
            'changeset-id': 'test-changeset',
            'pending-expiration-epoch': 1234567890,
            'republish-state': 'Pending',
            'republish-timestamp': "2019-01-01T01:01:00.000Z",
            "subscription-id": 1,
            'event-ids': ['test-republish']

        }
        with self.assertRaisesRegex(ClientError, 'MockClientError'):
            insert_collections_from_changeset(changeset_id, republish_record, '', 0)

    # - insert_collections_from_changeset: retry
    @patch('lng_datalake_dal.changeset_collection_table.ChangesetCollectionTable.get_pagination_token')
    @patch('create_republish_event_handler.set_republish_state')
    @patch('create_republish_event_handler.get_collection_batch_from_changeset')
    @patch('create_republish_event_handler.insert_republish_collection_records')
    def test_insert_collections_from_changeset_retry(self, mock_batch_write, mock_get_batch_collection,
                                                     mock_retry, mock_get_pagination_token):
        mock_get_batch_collection.side_effect = [['coll-1', 'coll-2', 'coll-3'], ['coll-4', 'coll-5']]
        mock_batch_write.side_effect = [3, 1]
        mock_get_pagination_token.return_value = 'fake-page'
        mock_retry.return_value = None
        changeset_id = 'test-changeset'
        republish_record = {
            'republish-id': 'test-republish',
            'changeset-id': 'test-changeset',
            'pending-expiration-epoch': 1234567890,
            'republish-state': 'Pending',
            'republish-timestamp': "2019-01-01T01:01:00.000Z",
            "subscription-id": 1,
            'event-ids': ['test-republish']

        }
        with self.assertRaisesRegex(RetryEventException,
                                    'Retry event to finish the update with the unprocessed collection ids in changeset'):
            insert_collections_from_changeset(changeset_id, republish_record, '', 0)

    # + get_collection_batch_from_changeset
    @patch('lng_datalake_dal.changeset_collection_table.ChangesetCollectionTable.query_items')
    def test_get_collection_batch_from_changeset_success(self, mock_query_items):
        mock_query_items.return_value = [{'collection-id': 'coll-1'}, {'collection-id': 'coll-2'}]
        collections = ['coll-1', 'coll-2']
        fake_pagination_token = 'eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ29sbGVjdGlvbklEIjogeyJTIjogIjk5In'
        self.assertEqual(get_collection_batch_from_changeset('cat-1', fake_pagination_token), collections)

    # - get_collection_batch_from_changeset: Client Error
    @patch('lng_datalake_dal.changeset_collection_table.ChangesetCollectionTable.query_items')
    def test_get_collection_batch_from_changeset_client_error(self, mock_query_items):
        mock_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'MockClientError',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")
        fake_pagination_token = 'eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ29sbGVjdGlvbklEIjogeyJTIjogIjk5In'
        with self.assertRaisesRegex(ClientError, 'MockClientError'):
            get_collection_batch_from_changeset('cat-1', fake_pagination_token)

    # - get_collection_batch_from_changeset: Exception
    @patch('lng_datalake_dal.changeset_collection_table.ChangesetCollectionTable.query_items')
    def test_get_collection_batch_from_changeset_general_exception(self, mock_query_items):
        mock_query_items.side_effect = Exception("FakeException")

        fake_pagination_token = 'eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ29sbGVjdGlvbklEIjogeyJTIjogIjk5In'
        with self.assertRaisesRegex(TerminalErrorException, 'General exception occurred when getting records'):
            get_collection_batch_from_changeset('cat-1', fake_pagination_token)

    # + republish_collection_batch_write
    @patch('lng_datalake_dal.republish_collection_table.RepublishCollectionTable.batch_write_items')
    def test_republish_collection_batch_write(self, mock_republish_batch_write):
        collection_list = ['coll-1', 'coll-2']
        mock_republish_batch_write.return_value = []

        self.assertEqual(republish_collection_batch_write(collection_list), 2)

    # - republish_collection_batch_write: return 1 item from second page
    @patch('lng_datalake_dal.republish_collection_table.RepublishCollectionTable.batch_write_items')
    def test_republish_collection_batch_write_return_list(self, mock_republish_batch_write):
        collection_list = ['coll-1', 'coll-2', 'coll-3', 'coll-4', 'coll-1', 'coll-2', 'coll-3', 'coll-4',
                           'coll-1', 'coll-2', 'coll-3', 'coll-4', 'coll-1', 'coll-2', 'coll-3', 'coll-4',
                           'coll-1', 'coll-2', 'coll-3', 'coll-4', 'coll-1', 'coll-2', 'coll-3', 'coll-4',
                           'coll-1', 'coll-2', 'coll-3', 'coll-4', 'coll-1', 'coll-2', 'coll-3', 'coll-4']
        mock_republish_batch_write.side_effect = [[], ['coll-2']]

        self.assertEqual(republish_collection_batch_write(collection_list), 25)

    # - republish_collection_batch_write: EndpointConnectionError
    @patch('lng_datalake_dal.republish_collection_table.RepublishCollectionTable.batch_write_items')
    def test_republish_collection_batch_write_endpoint_connection_error_exception(self, mock_republish_batch_write):
        collection_list = ['coll-1', 'coll-2']
        mock_republish_batch_write.side_effect = EndpointConnectionError(
            endpoint_url='https://fake.content.aws.lexis.com')

        self.assertEqual(republish_collection_batch_write(collection_list), 0)

    # - republish_collection_batch_write: client error
    @patch('lng_datalake_dal.republish_collection_table.RepublishCollectionTable.batch_write_items')
    def test_republish_collection_batch_write_client_error(self, mock_republish_batch_write):
        collection_list = ['coll-1', 'coll-2']
        mock_republish_batch_write.side_effect = ClientError({'ResponseMetadata': {},
                                                              'Error': {
                                                                  'Code': 'MockClientError',
                                                                  'Message': 'This is a mock'}},
                                                             "FAKE")

        self.assertEqual(republish_collection_batch_write(collection_list), 0)

    # - republish_collection_batch_write: exception
    @patch('lng_datalake_dal.republish_collection_table.RepublishCollectionTable.batch_write_items')
    def test_republish_collection_batch_write_exception(self, mock_republish_batch_write):
        collection_list = ['coll-1', 'coll-2']
        mock_republish_batch_write.side_effect = Exception("mock general exception")

        with self.assertRaisesRegex(TerminalErrorException, 'mock general exception'):
            republish_collection_batch_write(collection_list)

    # + set_republish_to_retry: pending to retry
    @patch('lng_datalake_dal.republish_table.RepublishTable.update_item')
    def test_set_republish_to_retry(self, mock_update_item):
        republish_record = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "republish-id": "gqZpHORIYuGaLlic",
            "republish-timestamp": "2019-07-12T20:45:16.386Z",
            'republish-state': 'Pending'
        }

        mock_update_item.return_value = None

        self.assertIsNone(set_republish_state(republish_record, republish_status.PENDING, republish_status.RETRY))

    # + set_republish_to_retry: retry to pending
    @patch('lng_datalake_dal.republish_table.RepublishTable.update_item')
    def test_set_republish_to_pending(self, mock_update_item):
        republish_record = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "republish-id": "gqZpHORIYuGaLlic",
            "republish-timestamp": "2019-07-12T20:45:16.386Z",
            'republish-state': 'Retry'

        }

        mock_update_item.return_value = None

        self.assertIsNone(set_republish_state(republish_record, republish_status.RETRY, republish_status.PENDING))

    # - set_republish_to_retry: EndpointConnectionError
    @patch('lng_datalake_dal.republish_table.RepublishTable.update_item')
    def test_set_republish_to_retry_endpoint_connection_error_exception(self, mock_update_item):
        republish_record = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "republish-id": "gqZpHORIYuGaLlic",
            "republish-timestamp": "2019-07-12T20:45:16.386Z",
            'republish-state': 'Retry'

        }

        mock_update_item.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')

        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            set_republish_state(republish_record, republish_status.RETRY, republish_status.PENDING)

    # - set_republish_to_retry: client error
    @patch('lng_datalake_dal.republish_table.RepublishTable.update_item')
    def test_set_republish_to_retry_client_error(self, mock_update_item):
        republish_record = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "republish-id": "gqZpHORIYuGaLlic",
            "republish-timestamp": "2019-07-12T20:45:16.386Z",
            'republish-state': 'Retry'

        }

        mock_update_item.side_effect = ClientError({'ResponseMetadata': {},
                                                    'Error': {
                                                        'Code': 'MockClientError',
                                                        'Message': 'This is a mock'}},
                                                   "FAKE")

        with self.assertRaisesRegex(ClientError, 'MockClientError'):
            set_republish_state(republish_record, republish_status.RETRY, republish_status.PENDING)

    # - set_republish_to_retry: condition error
    @patch('lng_datalake_dal.republish_table.RepublishTable.update_item')
    def test_set_republish_to_retry_condition_error(self, mock_update_item):
        republish_record = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "republish-id": "gqZpHORIYuGaLlic",
            "republish-timestamp": "2019-07-12T20:45:16.386Z",
            'republish-state': 'Retry'

        }

        mock_update_item.side_effect = ConditionError('MockConditionError')

        with self.assertRaisesRegex(TerminalErrorException, 'MockConditionError'):
            set_republish_state(republish_record, republish_status.RETRY, republish_status.PENDING)

    # - set_republish_to_retry: general exception
    @patch('lng_datalake_dal.republish_table.RepublishTable.update_item')
    def test_set_republish_to_retry_general_exception(self, mock_update_item):
        republish_record = {
            "subscription-id": 1,
            "catalog-ids": ["test1"],
            "pending-expiration-epoch": 100000000,
            "republish-id": "gqZpHORIYuGaLlic",
            "republish-timestamp": "2019-07-12T20:45:16.386Z",
            'republish-state': 'Retry'

        }

        mock_update_item.side_effect = Exception('MockException')

        with self.assertRaisesRegex(TerminalErrorException, 'MockException'):
            set_republish_state(republish_record, republish_status.RETRY, republish_status.PENDING)

    # + start_state_machine
    @patch('lng_aws_clients.session.client')
    def test_start_state_machine(self, mock_client):
        mock_client.return_value.client.return_value.start_execution.return_value = None
        self.assertIsNone(
            start_state_machine(2, 'mock-id', ['v0'], 'mock-table', 'mock-index', '2019-07-03T19:15:41.621Z'))

    # + start_state_machine: EndpointConnectionError
    @patch('lng_aws_clients.session.client')
    def test_start_state_machine_endpoint_connection_error_exception(self, mock_client):
        mock_client.side_effect = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            start_state_machine(2, 'request-id', ['v0'], 'mock-table', 'mock-index', '2019-07-03T19:15:41.621Z')

    # + start_state_machine: client error
    @patch('lng_aws_clients.session.client')
    def test_start_state_machine_client_error(self, mock_client):
        mock_client.side_effect = ClientError({'ResponseMetadata': {},
                                               'Error': {
                                                   'Code': 'MockClientError',
                                                   'Message': 'This is a mock'}},
                                              "FAKE")
        with self.assertRaisesRegex(ClientError, 'MockClientError'):
            start_state_machine(2, 'request-id', ['v0'], 'mock-table', 'mock-index', '2019-07-03T19:15:41.621Z')

    # - start_state_machine: general exception
    @patch('lng_aws_clients.session.client')
    def test_start_state_machine_exception(self, mock_client):
        mock_client.side_effect = Exception('MockException')
        with self.assertRaisesRegex(TerminalErrorException,
                                    'Failed to start republish state machine due to unknown reasons.'):
            start_state_machine(2, 'request-id', ['v0'], 'mock-table', 'mock-index', '2019-07-03T19:15:41.621Z')


if __name__ == '__main__':
    unittest.main()
