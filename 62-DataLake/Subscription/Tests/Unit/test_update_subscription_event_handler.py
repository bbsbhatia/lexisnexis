import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError, EndpointConnectionError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_testhelper.io_utils import IOUtils

from update_subscription_event_handler import update_subscription_event_handler, update_subscription_table, \
    set_subscription_filter, map_event_store_to_subscription_table, initialize_dict_with_required_keys

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateSubscriptionEventHandler')


class TestUpdateSubscriptionEventHandler(unittest.TestCase):
    session = None

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        cls.get_item_response = io_util.load_data_json("subscription_table_get_item.json")

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + update_subscription_handler - Success - updated name, filter and schema version
    @patch('update_subscription_event_handler.update_subscription_table')
    @patch('update_subscription_event_handler.set_subscription_filter')
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_handler(self, mock_get_item, mock_set_subscription_filter,
                                         mock_update_subscription_table):
        mock_set_subscription_filter.return_value = None
        mock_get_item.return_value = self.get_item_response
        mock_update_subscription_table.return_value = None
        event = io_util.load_data_json("event.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         update_subscription_event_handler(event,
                                                           "arn:aws:lambda:us-east-1:12342:function:TestLambda:LATEST"))
        mock_set_subscription_filter.assert_called_once()

    # + update_subscription_handler - Success - updated name, filter and schema version : new event id appended
    @patch('update_subscription_event_handler.update_subscription_table')
    @patch('update_subscription_event_handler.set_subscription_filter')
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_handler_2(self, mock_get_item, mock_set_subscription_filter,
                                           mock_update_subscription_table):
        mock_set_subscription_filter.return_value = None
        mock_get_item.return_value = io_util.load_data_json("subscription_table_get_item_with_event_ids.json")
        mock_update_subscription_table.return_value = None
        event = io_util.load_data_json("event.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         update_subscription_event_handler(event,
                                                           "arn:aws:lambda:us-east-1:12342:function:TestLambda:LATEST"))
        mock_set_subscription_filter.assert_called_once()
        updated_item = io_util.load_data_json("subscription_table_updated_item_with_event_ids.json")
        mock_update_subscription_table.assert_called_with(updated_item)

    # + update_subscription_handler - Success - only name updated
    @patch('update_subscription_event_handler.update_subscription_table')
    @patch('update_subscription_event_handler.set_subscription_filter')
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_update_subscription_handler_2(self, mock_get_item, mock_set_subscription_filter,
                                           mock_update_subscription_table):
        mock_get_item.return_value = self.get_item_response
        mock_update_subscription_table.return_value = None
        event = io_util.load_data_json("event_only_name_updated.json")
        self.assertEqual(event_handler_status.SUCCESS,
                         update_subscription_event_handler(event,
                                                           "arn:aws:lambda:us-east-1:12342:function:TestLambda:LATEST"))
        mock_set_subscription_filter.assert_not_called()

    # - update_subscription_handler - invalid event name
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    def test_update_subscription_handler_invalid_event(self, mock_is_valid_message):
        mock_is_valid_message.return_value = False
        event = io_util.load_data_json("event.json")
        with self.assertRaisesRegex(TerminalErrorException, "event-name does not match Subscription::Update"):
            update_subscription_event_handler(event, "arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST")

    # - update_subscription_handler - invalid event version
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    def test_update_subscription_handler_invalid_version(self, mock_is_valid_message, mock_is_valid_event_version):
        mock_is_valid_message.return_value = True
        mock_is_valid_event_version.return_value = False
        event = io_util.load_data_json("event.json")
        with self.assertRaisesRegex(TerminalErrorException, "event-version does not match"):
            update_subscription_event_handler(event, "arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST")

    # - update_subscription_handler - invalid stage
    @patch('lng_datalake_commons.validate.is_valid_event_stage')
    @patch('lng_datalake_commons.validate.is_valid_event_version')
    @patch('lng_datalake_commons.validate.is_valid_event_message')
    def test_update_subscription_handler_invalid_stage(self, mock_is_valid_message, mock_is_valid_event_version,
                                                       mock_is_valid_event_stage):
        mock_is_valid_message.return_value = True
        mock_is_valid_event_version.return_value = True
        mock_is_valid_event_stage.return_value = False
        event = io_util.load_data_json("event.json")
        with self.assertRaisesRegex(TerminalErrorException, "stage does not match"):
            update_subscription_event_handler(event, "arn:aws:lambda:us-east-1:123456789012:function:TestLambda:LATEST")

    # - update_subscription_record - Failure
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.update_item")
    def test_update_subscription_record_failure(self, mock_update_item):
        for side_effect in [ClientError({"ResponseMetadata": {},
                                         "Error": {"Code": "mock error code",
                                                   "Message": "mock error message"}},
                                        "client-error-mock"),
                            EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com'),
                            Exception, SchemaValidationError, ConditionError,
                            SchemaError, SchemaLoadError]:
            mock_update_item.side_effect = side_effect
            if type(side_effect) == EndpointConnectionError:
                with self.assertRaises(EndpointConnectionError):
                    update_subscription_table(20)
            elif type(side_effect) == ClientError:
                with self.assertRaises(ClientError):
                    update_subscription_table(20)
            else:
                with self.assertRaises(TerminalErrorException):
                    update_subscription_table(20)

    # + set_subscription_filter - Success - one schema version specified
    @patch('lng_aws_clients.sns.get_client')
    def test_set_subscription_filter_one_schema_version(self, mock_sns):
        mock_sns.return_value.set_subscription_attributes.return_value = None
        updated_item = {
            "subscription-id": 99,
            "schema-version": [
                "v0"
            ],
            "filter": {
                "collection-id": ["123"],
                "event-name": "Object::Create"
            },
            "subscription-arn": "arn:aws:sns:us-east-1:288044017584:release-DataLake"
        }
        self.assertIsNone(set_subscription_filter(updated_item))
        mock_sns.return_value.set_subscription_attributes.assert_called_with(
            SubscriptionArn="arn:aws:sns:us-east-1:288044017584:release-DataLake",
            AttributeName='FilterPolicy',
            AttributeValue='{"collection-id":["123"],"event-name":"Object::Create","schema-version":["v0"],"notification-type":["event","99"]}'
        )

    # + set_subscription_filter - Success - two schema versions specified
    @patch('lng_aws_clients.sns.get_client')
    def test_set_subscription_filter_two_schema_versions(self, mock_sns):
        mock_sns.return_value.set_subscription_attributes.return_value = None
        updated_item = {
            "subscription-id": 99,
            "schema-version": [
                "v0",
                "v1"
            ],
            "filter": {
                "collection-id": ["123"],
                "event-name": "Object::Create"
            },
            "subscription-arn": "arn:aws:sns:us-east-1:288044017584:release-DataLake"
        }
        self.assertIsNone(set_subscription_filter(updated_item))
        mock_sns.return_value.set_subscription_attributes.assert_called_with(
            SubscriptionArn="arn:aws:sns:us-east-1:288044017584:release-DataLake",
            AttributeName='FilterPolicy',
            AttributeValue='{"collection-id":["123"],"event-name":"Object::Create","schema-version":["v0v1"],"notification-type":["event","99"]}'
        )

    # - set_subscription_filter - Failure
    @patch('lng_aws_clients.sns.get_client')
    def test_set_subscription_filter_failure(self, get_client):
        updated_item = {
            "subscription-id": 99,
            "schema-version": [
                "v0",
                "v1"
            ],
            "filter": {
                "collection-id": ["123"],
                "event-name": "Object::Create"
            },
            "subscription-arn": "arn:aws:sns:us-east-1:288044017584:release-DataLake"
        }
        exception = EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com')
        get_client.return_value.set_subscription_attributes.side_effect = exception
        with self.assertRaisesRegex(EndpointConnectionError,
                                    'Could not connect to the endpoint URL: "https://fake.content.aws.lexis.com"'):
            set_subscription_filter(updated_item)

    # - map_event_store_to_subscription_table - no item
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_map_event_store_to_subscription_table_1(self, mock_get_item):
        mock_get_item.return_value = None
        with self.assertRaisesRegex(TerminalErrorException, 'Can not update a non existent subscription*'):
            map_event_store_to_subscription_table({"subscription-id": 12})

    # - map_event_store_to_subscription_table - Exeptions
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_item")
    def test_map_event_store_to_subscription_table_2(self, mock_get_item):
        for side_effect in [Exception, EndpointConnectionError(endpoint_url='https://fake.content.aws.lexis.com'),
                            TerminalErrorException("Generic Dynamo Error"),
                            ClientError({"ResponseMetadata": {}, "Error": {"Code": "mock error code",
                                                                           "Message": "mock error message"}},
                                        "client-error-mock")]:
            mock_get_item.side_effect = side_effect
            if type(side_effect) == TerminalErrorException or side_effect == Exception:
                with self.assertRaises(TerminalErrorException):
                    map_event_store_to_subscription_table({"subscription-id": 1})
            elif type(side_effect) == EndpointConnectionError:
                with self.assertRaises(EndpointConnectionError):
                    map_event_store_to_subscription_table({"subscription-id": 1})
            elif type(side_effect) == ClientError:
                with self.assertRaises(ClientError):
                    map_event_store_to_subscription_table({"subscription-id": 1})

    # - initialize_dict_with_required_keys - key error
    @patch("lng_datalake_dal.subscription_table.SubscriptionTable.get_required_schema_keys")
    def test_initialize_dict_with_required_keys_key_error(self, mock_schema_keys):
        mock_schema_keys.return_value = ['bogus-key-1', 'bogus-key-2']
        error_dict = {
            "error-key": "some error"
        }
        with self.assertRaises(KeyError):
            initialize_dict_with_required_keys(error_dict, error_dict)


if __name__ == "__main__":
    unittest.main()
