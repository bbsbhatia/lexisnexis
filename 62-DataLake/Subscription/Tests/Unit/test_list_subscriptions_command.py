import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyValue, InternalError
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import list_subscriptions_command as list_subscriptions_command_module
from list_subscriptions_command import lambda_handler, list_subscriptions_command, get_subscription_list, \
    generate_response_json

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListSubscriptions')

list_subscriptions_command_module.ct_owner_and_name_index = "subscription-by-owner-and-name-index"


class TestListSubscriptions(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda_handler - valid request
    @patch('list_subscriptions_command.list_subscriptions_command')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_valid(self, mock_session,
                                  mock_list_subscriptions_command):
        mock_session.return_value = None
        mock_list_subscriptions_command.return_value = \
            {"response-dict": {
                "subscriptions": [
                    {
                        "subscription-name": "test_subscription",
                        "subscription-state": "Created",
                        "subscription-id": 20,
                        "protocol": "email",
                        "endpoint": "test_user@lexisnexis.com",
                        "schema-version": ["v0"]
                    },
                    {
                        "subscription-name": "test_subscription_2",
                        "subscription-state": "Created",
                        "subscription-id": 22,
                        "protocol": "email",
                        "endpoint": "test_user_2@lexisnexis.com",
                        "schema-version": ["v0"]
                    }
                ],
                "item-count": 2
            }}

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.maxDiff = None
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response,
                                 lambda_handler(request_input, mock_lambda_context.MockLambdaContext()))

    # - lambda_handler - response failed schema validation - duplicated event-name in filter
    @patch('list_subscriptions_command.list_subscriptions_command')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_invalid_response(self, mock_session, mock_list_subscriptions_command):
        mock_session.return_value = None
        mock_list_subscriptions_command.return_value = \
            {"response-dict": {
                "subscriptions": [
                    {
                        "subscription-name": "test_subscription",
                        "subscription-state": "Created",
                        "subscription-id": 20,
                        "protocol": "email",
                        "endpoint": "test_user@lexisnexis.com",
                        "schema-version": ["v0"],
                        "filter": {"collection-id": ["collection-test"],
                                   "event-name": ["Object::Create", "Object::Create"]}
                    }
                ],
                "item-count": 1
            }}
        request_input = io_util.load_data_json('apigateway.input_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InternalError,
                                        r"data.subscriptions\[0\].filter.event-name must contain unique items"):
                lambda_handler(request_input, mock_lambda_context.MockLambdaContext())

    # + command - valid request
    @patch('list_subscriptions_command.generate_response_json')
    @patch('list_subscriptions_command.get_subscription_list')
    @patch('lng_aws_clients.session.set_session')
    def test_list_subscriptions_command_success(self, mock_session,
                                                mock_subscription_list,
                                                mock_generate_response_json):
        mock_session.return_value = None
        generated_response = io_util.load_data_json("valid_response.json")
        mock_subscription_list.return_value = generated_response["subscriptions"]
        mock_generate_response_json.return_value = generated_response
        expected_response = {"response-dict": generated_response}
        request_input = {"owner-id": 100,
                         "max-items": 2,
                         "next-token": "eyJtYXgtaXRlbXMiOiAyLCAicGFnaW5hdGlvbi10b2tlbiI6ICJzb21lcmFuZG9tc3RyaW5nMTIzNDUifQ=="}
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('ListSubscriptionsCommand-ResponseSchema.json')):
            self.assertEqual(list_subscriptions_command(request_input), expected_response)

    # + generate_response_json - Success
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_success(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['subscription-arn', 'filter']
        mock_required_keys.return_value = [
            "subscription-name",
            "subscription-id",
            "subscription-state",
            "protocol",
            "endpoint",
            "schema-version"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        self.maxDiff = None
        subscription_list = [
            {
                "subscription-name": "test_subscription",
                "subscription-state": "Created",
                "subscription-id": 20,
                "protocol": "email",
                "endpoint": "test_user@lexisnexis.com",
                "schema-version": ["v0"]
            },
            {
                "subscription-name": "test_subscription_2",
                "subscription-state": "Created",
                "subscription-id": 22,
                "protocol": "email",
                "endpoint": "test_user_2@lexisnexis.com",
                "schema-version": ["v0"]
            }
        ]
        expected_response = io_util.load_data_json("valid_response.json")
        self.assertDictEqual(expected_response, generate_response_json(subscription_list,
                                                                       "eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ2F0YWxvZ0lEIjogeyJTIjogImNhdGFsb2ctMDIifX19",
                                                                       2))

    # - InternalError exception due to missing required property
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    def test_generate_response_json_negative(self, mock_optional_keys, mock_required_keys, mock_get_request_time):
        mock_optional_keys.return_value = ['subscription-arn', 'filter']
        mock_required_keys.return_value = [
            "subscription-name",
            "subscription-id",
            "subscription-state",
            "protocol",
            "endpoint",
            "owner-id",
            "schema-version"
        ]
        mock_get_request_time.return_value = "2018-05-18T19:31:17.813Z"
        subscription_list = [
            {
                "subscription-name": "test_subscription",
                "subscription-state": "Created",
                "subscription-id": 20,
                "protocol": "email",
                "endpoint": "test_user@lexisnexis.com",
                "schema-version": ["v0"]
            }
        ]
        with self.assertRaisesRegex(InternalError, "Missing required property:"):
            generate_response_json(subscription_list, None, None)

    # - list_subscriptions_command - max-items > 1000
    @patch('lng_aws_clients.session.set_session')
    def test_list_subscriptions_command_failure_max_items_too_large(self, mock_session):
        mock_session.return_value = None
        request_input = {"owner-id": 100,
                         "max-items": 2000}
        with self.assertRaisesRegex(InvalidRequestPropertyValue, 'Invalid max-items value'):
            list_subscriptions_command(request_input)

    # + get_subscription_list: success
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    def test_get_subscription_list_success(self, mock_st_query_items):
        subscription_list = io_util.load_data_json("valid_response.json")["subscriptions"]
        mock_st_query_items.return_value = subscription_list
        self.assertEqual(subscription_list, get_subscription_list(121, 1000, None))

    # - get_subscription_list: query by owner exception
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    def test_get_subscription_list_query_by_owner_exception(self, mock_st_query_items):
        mock_st_query_items.side_effect = [Exception("Mock Exception"),
                                           ClientError(
                                               {'ResponseMetadata': {},
                                                'Error': {
                                                    'Code': 'OTHER',
                                                    'Message': 'Client Error Mock'}}, "FAKE")]
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            get_subscription_list(1, 1000, None)
        with self.assertRaisesRegex(InternalError, "Unable to query items from Subscription Table index"):
            get_subscription_list(1, 1000, None)

    # - get_subscription_list: get all items exceptions
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_all_items')
    def test_get_subscription_list_get_all_items_exception(self, mock_st_get_all_items):
        mock_st_get_all_items.side_effect = [Exception("Mock Exception"),
                                             ClientError(
                                                 {'ResponseMetadata': {},
                                                  'Error': {
                                                      'Code': 'OTHER',
                                                      'Message': 'Client Error Mock'}}, "FAKE")]
        with self.assertRaisesRegex(InternalError,
                                    "Unhandled exception occurred"):
            get_subscription_list(None, 1000, None)
        with self.assertRaisesRegex(InternalError, "Unable to get items from Subscription Table"):
            get_subscription_list(None, 1000, None)


if __name__ == "__main__":
    unittest.main()
