import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NoSuchSubscription, NotAuthorizedError
from lng_datalake_constants import subscription_status, event_names
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from remove_subscription_command import lambda_handler, remove_subscription_command, generate_response_json, \
    generate_event_dict

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveSubscription')


class TestRemoveSubscription(unittest.TestCase):

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + lambda_handler - valid request
    @patch('remove_subscription_command.remove_subscription_command')
    @patch('lng_datalake_dal.tracking_table.TrackingTable.put_item')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_valid(self, mock_session,
                                  mock_store_event_table_put_item,
                                  mock_tracking_table_put_item,
                                  mock_remove_subscription_command):
        mock_session.return_value = None
        mock_store_event_table_put_item.return_value = None
        mock_tracking_table_put_item.return_value = None
        mock_remove_subscription_command.return_value = \
            {'response-dict': {'subscription-id': 20,
                               'subscription-name': 'test_subscription',
                               'subscription-state': subscription_status.TERMINATING,
                               'protocol': 'email',
                               'endpoint': 'test_user@lexisnexis.com'},
             'event-dict': {'subscription-id': 20,
                            'event-id': '563r1jug9i',
                            'item-name': 'test_subscription',
                            'event-name': event_names.SUBSCRIPTION_REMOVE,
                            'request-time': command_wrapper.get_request_time(),
                            'event-version': 1,
                            'stage': 'LATEST'
                            }
             }

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(expected_response,
                                 lambda_handler(request_input, mock_lambda_context.MockLambdaContext()))

    # + remove_subscription_command - valid subscription-id
    @patch('remove_subscription_command.generate_event_dict')
    @patch('remove_subscription_command.generate_response_json')
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_command(self, mock_session,
                                         mock_subscription_table_get_item,
                                         mock_owner_authorization,
                                         mock_generate_response_json,
                                         mock_generate_event_dict):
        mock_session.return_value = None
        mock_owner_authorization.return_value = {}
        mock_subscription_table_get_item.return_value = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'owner-id': 100,
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        generated_response = {
            'subscription-id': 20,
            'subscription-name': 'test_subscription',
            'subscription-state': subscription_status.TERMINATING,
            'protocol': 'email',
            'endpoint': 'test_user@lexisnexis.com'
        }
        generated_event_dictionary = {
            'subscription-id': 20,
            'event-id': '563r1jug9i',
            'item-name': 'test_subscription',
            'event-name': event_names.SUBSCRIPTION_REMOVE,
            'request-time': command_wrapper.get_request_time(),
            'event-version': 1.0
        }
        mock_generate_response_json.return_value = generated_response
        mock_generate_event_dict.return_value = generated_event_dictionary

        expected_response = \
            {
                'response-dict': generated_response,
                'event-dict': generated_event_dictionary
            }
        request_input = {'subscription-id': 20}
        self.assertDictEqual(expected_response,
                             remove_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId"))

    # - remove_subscription_command - Invalid subscription-id
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_command_failure_1(self, mock_session,
                                                   mock_subscription_table_get_item):
        mock_session.return_value = None
        mock_subscription_table_get_item.return_value = {}
        request_input = {'subscription-id': 20}
        with self.assertRaisesRegex(NoSuchSubscription, "Invalid Subscription ID 20"):
            remove_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - remove_subscription_command - raise errors
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_command_failure_3(self, mock_session,
                                                   mock_subscription_table_get_item):
        mock_session.return_value = None
        request_input = {'subscription-id': 20}

        for side_effect in [ClientError({"ResponseMetadata": {},
                                         "Error": {"Code": "mock error code",
                                                   "Message": "mock error message"}},
                                        "client-error-mock"), Exception]:
            mock_subscription_table_get_item.side_effect = side_effect
            if type(side_effect) == ClientError:
                with self.assertRaisesRegex(InternalError, "Unable to get item from Subscription Table"):
                    remove_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")
            elif side_effect == Exception:
                with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
                    remove_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.authorize')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_remove_subscription_command_owner_auth(self, mock_session,
                                                    mock_subscription_table_get_item,
                                                    mock_owner_authorization):
        mock_session.return_value = None
        mock_owner_authorization.side_effect = NotAuthorizedError("API Key provided is not valid for Owner ID", "")
        mock_subscription_table_get_item.return_value = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'owner-id': 100,
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }

        request_input = {'subscription-id': 20}
        with self.assertRaisesRegex(NotAuthorizedError, "API Key provided is not valid for Owner ID"):
            remove_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # + generate_response_json - success
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json(self, mock_required_props,
                                    mock_optional_props):
        mock_required_props.return_value = ['subscription-name', 'subscription-id', 'subscription-state', 'protocol',
                                            'endpoint']
        mock_optional_props.return_value = ['endpoint']

        subscription_record = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'owner-id': 100,
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        expected_response = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.TERMINATING,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com'
            }
        self.assertDictEqual(expected_response, generate_response_json(subscription_record))

    # - generate_response_json - failed
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    def test_generate_response_json_failure(self, mock_required_props,
                                            mock_optional_props):
        mock_required_props.return_value = ['subscription-name', 'subscription-id', 'subscription-state', 'protocol',
                                            'endpoint', 'dummy_property']
        mock_optional_props.return_value = []

        subscription_record = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'owner-id': 100,
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }

        with self.assertRaisesRegex(InternalError, "Missing required property: dummy_property"):
            generate_response_json(subscription_record)

    # + generate_event_dict
    def test_generate_event_dict(self):
        expected_response = \
            {
                'subscription-id': 20,
                'event-id': '563r1jug9i',
                'item-name': 'test_subscription',
                'event-name': event_names.SUBSCRIPTION_REMOVE,
                'request-time': command_wrapper.get_request_time(),
                'event-version': 1,
                'stage': "LATEST"
            }
        subscription_record = \
            {
                'subscription-id': 20,
                'subscription-name': 'test_subscription',
                'subscription-state': subscription_status.CREATED,
                'subscription-pending-confirmation': True,
                'protocol': 'email',
                'endpoint': 'test_user@lexisnexis.com',
                'subscription-arn': 'arn:aws:sns:us-east-1:288044017584:master-SubscriptionNotificationTopic-SubscriptionNotificationTopic-L1RHHPE0UOBB:c39ecb00-92cf-4325-a267-03d35770abd6',
                'owner-id': 100,
                'filter': {
                    "collection-id": [
                        "14"
                    ],
                    "event-name": [
                        "Object::Create"
                    ]
                }
            }
        self.assertDictEqual(expected_response, generate_event_dict(subscription_record, "563r1jug9i", "LATEST"))


if __name__ == "__main__":
    unittest.main()
