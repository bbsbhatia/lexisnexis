import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_republish_status import lambda_handler, is_republish_id_oldest, get_collections_in_process_count

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RepublishStatus')


class RepublishStatus(TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + lambda_handler
    @patch("get_republish_status.get_collections_in_process_count")
    @patch("get_republish_status.is_republish_id_oldest")
    def test_lambda_handler_1(self,
                              is_republish_id_oldest_mock,
                              get_collections_in_process_count_mock):
        input_event = {
            "republish-id": "1234"
        }
        is_republish_id_oldest_mock.return_value = True
        get_collections_in_process_count_mock.return_value = 2

        with patch('get_republish_status.glue_job_max_concurrency', 10):
            self.assertEqual(lambda_handler(input_event, MockLambdaContext()), {'Status': 'Ready'})

    # + lambda_handler
    @patch("get_republish_status.get_collections_in_process_count")
    @patch("get_republish_status.is_republish_id_oldest")
    def test_lambda_handler_2(self,
                              is_republish_id_oldest_mock,
                              get_collections_in_process_count_mock):
        input_event = {
            "republish-id": "1234"
        }
        is_republish_id_oldest_mock.return_value = True
        get_collections_in_process_count_mock.return_value = 10

        with patch('get_republish_status.glue_job_max_concurrency', 10):
            self.assertEqual(lambda_handler(input_event, MockLambdaContext()), {'Status': 'Waiting'})

    # + lambda_handler
    @patch("get_republish_status.is_republish_id_oldest")
    def test_lambda_handler_3(self, is_republish_id_oldest_mock):
        input_event = {
            "republish-id": "1234"
        }
        is_republish_id_oldest_mock.return_value = False

        self.assertEqual(lambda_handler(input_event, MockLambdaContext()), {'Status': 'Waiting'})

    # - lambda_handler: throttled read
    @patch("get_republish_status.is_republish_id_oldest")
    def test_lambda_handler_4(self, is_republish_id_oldest_mock):
        input_event = {
            "republish-id": "1234"
        }
        is_republish_id_oldest_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                               "Error": {"Code": "mock error code",
                                                                         "Message": "mock error message"}},
                                                              "client-error-mock")

        self.assertEqual(lambda_handler(input_event, MockLambdaContext()), {'Status': 'Waiting'})

    # + is_republish_id_oldest_pending success - one item returned
    @patch("lng_datalake_dal.republish_table.RepublishTable.get_all_items")
    def test_is_republish_id_oldest_success_1(self, get_all_items_mock):
        get_all_items_mock.return_value = [
            {'republish-timestamp': '2019-06-02T16:09:33.861Z', 'republish-id': 'mock_id', 'republish-state': 'Pending',
             'subscription-id': 99}]
        self.assertEqual(is_republish_id_oldest(republish_id="mock_id"), True)

    # + is_republish_id_oldest_pending success - multiple items returned
    @patch("lng_datalake_dal.republish_table.RepublishTable.get_all_items")
    def test_is_republish_id_oldest_success_2(self, get_all_items_mock):
        get_all_items_mock.return_value = [
            {'republish-timestamp': '2019-07-02T16:09:33.861Z', 'republish-id': 'mock_id', 'republish-state': 'Pending',
             'subscription-id': 99},
            {'republish-timestamp': '2019-06-02T16:09:33.861Z', 'republish-id': 'foo_bar', 'republish-state': 'Pending',
             'subscription-id': 8}]
        self.assertEqual(is_republish_id_oldest(republish_id="mock_id"), False)

    # - is_republish_id_oldest_pending - zero items returned
    @patch("lng_datalake_dal.republish_table.RepublishTable.get_all_items")
    def test_is_republish_id_oldest_fail_0(self, get_all_items_mock):
        get_all_items_mock.return_value = {}
        with self.assertRaisesRegex(TerminalErrorException, 'Republish ID mock_id not found in Pending state'):
            is_republish_id_oldest(republish_id="mock_id")

    # - is_republish_id_oldest_pending - client error
    @patch("lng_datalake_dal.republish_table.RepublishTable.get_all_items")
    def test_is_republish_id_oldest_fail_1(self, get_all_items_mock):
        get_all_items_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                      "Error": {"Code": "mock error code",
                                                                "Message": "mock error message"}},
                                                     "client-error-mock")
        with self.assertRaisesRegex(ClientError, 'mock error message'):
            is_republish_id_oldest(republish_id="mock_id")

    # - is_republish_id_oldest_pending - exception
    @patch("lng_datalake_dal.republish_table.RepublishTable.get_all_items")
    def test_is_republish_id_oldest_fail_2(self, get_all_items_mock):
        get_all_items_mock.side_effect = Exception("Mock Error")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to get item by republish-id'):
            is_republish_id_oldest(republish_id="mock_id")

    # + get_collections_in_process_count
    @patch("lng_datalake_dal.republish_collection_table.RepublishCollectionTable.count_query_items")
    def test_get_collections_in_process_count(self, count_items_mock):
        count_items_mock.return_value = 2
        self.assertEqual(get_collections_in_process_count(), 2)

    # - get_collections_in_process_count - client error
    @patch("lng_datalake_dal.republish_collection_table.RepublishCollectionTable.count_query_items")
    def test_get_collections_in_process_count_fail1(self, count_items_mock):
        count_items_mock.side_effect = ClientError({"ResponseMetadata": {},
                                                    "Error": {"Code": "mock error code",
                                                              "Message": "mock error message"}},
                                                   "client-error-mock")
        with self.assertRaisesRegex(ClientError, 'mock error message'):
            get_collections_in_process_count()

    # - get_collections_in_process_count - exception
    @patch("lng_datalake_dal.republish_collection_table.RepublishCollectionTable.count_query_items")
    def test_get_collections_in_process_count_fail2(self, count_items_mock):
        count_items_mock.side_effect = Exception("Mock Error")
        with self.assertRaisesRegex(TerminalErrorException, 'Failed to get count for collections in process'):
            get_collections_in_process_count()


if __name__ == '__main__':
    unittest.main()
