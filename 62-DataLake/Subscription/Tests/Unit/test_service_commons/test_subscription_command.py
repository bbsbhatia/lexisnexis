import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, NoSuchCatalog, NoSuchCollection
from lng_datalake_testhelper.io_utils import IOUtils

from service_commons.subscription_command import validate_filter, is_valid_catalog_id, is_valid_collection_id, \
    validate_schema_version

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.1"

io_util = IOUtils(__file__, 'SubscriptionCommand')


class ServiceCommonSubscriptionCommand(unittest.TestCase):

    # + validate filter
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_filter_success(self, mock_collection_get_item,
                                     mock_catalog_get_item):
        input_data_collection = io_util.load_data_json('dynamo.collection_data_valid.json')
        input_data_catalog = io_util.load_data_json('dynamo.catalog_data_valid.json')
        mock_collection_get_item.return_value = input_data_collection['Items']
        mock_catalog_get_item.return_value = input_data_catalog['Items']

        filter_inp = {
            "collection-id": [
                "293A"
            ],
            "event-name": [
                "Object::Create"
            ],
            "catalog-id": [
                "catalog-01"
            ]
        }
        self.assertIsNone(validate_filter(filter_inp))

    # - validate filter fail collection
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_filter_fail_collection(self, mock_collection_get_item,
                                             mock_catalog_get_item):
        input_data_collection = io_util.load_data_json('dynamo.collection_data_invalid.json')
        input_data_catalog = io_util.load_data_json('dynamo.catalog_data_valid.json')
        mock_collection_get_item.return_value = input_data_collection['Items']
        mock_catalog_get_item.return_value = input_data_catalog['Items']

        filter_inp = {
            "collection-id": [
                "-1"
            ],
            "event-name": [
                "Object::Create"
            ],
            "catalog-id": [
                "catalog-01"
            ]
        }
        with self.assertRaisesRegex(NoSuchCollection,
                                    "Invalid Collection ID"):
            validate_filter(filter_inp)

    # - validate filter fail catalog
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_filter_fail_catalog(self, mock_collection_get_item,
                                          mock_catalog_get_item):
        input_data_collection = io_util.load_data_json('dynamo.collection_data_valid.json')
        input_data_catalog = io_util.load_data_json('dynamo.catalog_data_invalid.json')
        mock_collection_get_item.return_value = input_data_collection['Items']
        mock_catalog_get_item.return_value = input_data_catalog['Items']

        filter_inp = {
            "collection-id": [
                "293"
            ],
            "event-name": [
                "Object::Create"
            ],
            "catalog-id": [
                "-1"
            ]
        }
        with self.assertRaisesRegex(NoSuchCatalog,
                                    "Invalid Catalog ID"):
            validate_filter(filter_inp)

    # - validate filter fail empty list
    def test_validate_filter_fail_empty_list(self):
        filter_inp = {
            "collection-id": [
                "293"
            ],
            "event-name": [
                "Object::Create"
            ],
            "catalog-id": [

            ]
        }
        with self.assertRaisesRegex(InvalidRequestPropertyValue,
                                    "Invalid filter key catalog-id"):
            validate_filter(filter_inp)

    # + is valid collection id success
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_valid_collection_id(self, mock_collection_get_item):
        input_data = io_util.load_data_json('dynamo.collection_data_valid.json')
        mock_collection_get_item.return_value = input_data['Items']

        self.assertTrue(is_valid_collection_id(999))

    # + is valid catalog id success
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_invalid_catalog_id(self, mock_catalog_get_item):
        input_data = io_util.load_data_json('dynamo.catalog_data_valid.json')
        mock_catalog_get_item.return_value = input_data['Items']

        self.assertTrue(is_valid_catalog_id("catlaog-01"))

    # - is valid collection id fail
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_invalid_collection_id_fail(self, mock_collection_get_item):
        input_data = io_util.load_data_json('dynamo.collection_data_invalid.json')
        mock_collection_get_item.return_value = input_data['Items']

        self.assertFalse(is_valid_collection_id(-1))

        mock_collection_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                            'Error': {
                                                                'Code': 'Unit Test',
                                                                'Message': 'This is a mock'}},
                                                           "FAKE")

        self.assertRaises(InternalError, is_valid_collection_id, -1)

        mock_collection_get_item.side_effect = Exception("FAKE")

        self.assertRaises(InternalError, is_valid_collection_id, -1)

    # - in valid catalog id fail
    @patch('lng_datalake_dal.catalog_table.CatalogTable.get_item')
    def test_invalid_catalog_id_fail(self, mock_catalog_get_item):
        # Data simulates DynamoDB response of table missing collection-id
        input_data = io_util.load_data_json('dynamo.catalog_data_invalid.json')
        mock_catalog_get_item.return_value = input_data['Items']

        self.assertFalse(is_valid_catalog_id("-1"))

        mock_catalog_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                         'Error': {
                                                             'Code': 'Unit Test',
                                                             'Message': 'This is a mock'}},
                                                        "FAKE")

        self.assertRaises(InternalError, is_valid_catalog_id, "-1")

        mock_catalog_get_item.side_effect = Exception("FAKE")

        self.assertRaises(InternalError, is_valid_catalog_id, "-1")

    # + validate_schema_version - success - valid schema version specified
    def test_validate_schema_version_success(self):
        request = {
            "schema-version": [
                "v0"
            ],
            "subscription-id": "101",
            "filter": {
                "collection-id": [
                    "293A"
                ],
                "event-name": [
                    "Object::Create"
                ]}
        }
        self.assertIsNone(validate_schema_version(request))

    # + validate_schema_version - success - no schema version specified and old one ('v0') is assigned
    def test_validate_schema_version_success_2(self):
        request = {
            "subscription-id": "101",
            "filter": {
                "collection-id": [
                    "293A"
                ],
                "event-name": [
                    "Object::Create"
                ]}
        }
        self.assertIsNone(validate_schema_version(request))
        self.assertEqual(["v1"], request["schema-version"])

    # - validate_schema_version - invalid schema version 'v2' (current schema version is 'v0' and 'v1')
    def test_validate_schema_version_invalid_version(self):
        request = {
            "schema-version": [
                "v2"
            ],
            "subscription-id": "101"
        }
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid schema versions: ['v2']||"
                                                                 "Schema version must be one of ['v0', 'v1']"):
            validate_schema_version(request)

    # - validate_schema_version - schema version is an empty array
    def test_validate_schema_version_empty_array(self):
        request = {
            "schema-version": [],
            "subscription-id": "101"
        }
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Invalid schema version||"
                                                                 "Schema version must not be an empty array"):
            validate_schema_version(request)


if __name__ == "__main__":
    unittest.main()
