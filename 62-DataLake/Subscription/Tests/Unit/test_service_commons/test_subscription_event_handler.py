import unittest
from importlib import reload

from lng_datalake_testhelper.io_utils import IOUtils

import service_commons.subscription_event_handler as subscription_event_handler_module
from service_commons import subscription_event_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.1"

io_util = IOUtils(__file__, 'SubscriptionEventHandler')


class ServiceCommonSubscriptionEventHandler(unittest.TestCase):
    @classmethod
    def setUpClass(cls):  # NOSONAR
        reload(subscription_event_handler_module)

    # + set_input_queue_url
    def test_set_input_queue_url(self):
        arn = 'arn:aws:sqs:us-east-1:288044017584:feature-kgg-LATEST-subscription-CreateSubscription-Queue'
        expected_url = 'https://sqs.us-east-1.amazonaws.com/288044017584/feature-kgg-LATEST-subscription-CreateSubscription-Queue'
        self.assertEqual(subscription_event_handler.input_sqs_url, '')
        self.assertIsNone(subscription_event_handler.set_input_queue_url(arn))
        self.assertEqual(subscription_event_handler.input_sqs_url, expected_url)


if __name__ == "__main__":
    unittest.main()
