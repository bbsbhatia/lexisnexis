import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, NotAuthorizedError, InvalidRequestPropertyValue, \
    SemanticError, InvalidRequestPropertyName, NoSuchCollection
from lng_datalake_commons import validate
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

from create_subscription_command import create_subscription_command, \
    generate_subscription_id, generate_response_json, validate_request, \
    is_unique_subscription, generate_next_action_message, generate_event_dict, generate_sqs_policy, lambda_handler

__author__ = "Aaron Pohl, Kiran G, Jonathan Mitchall"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.1"

io_util = IOUtils(__file__, 'CreateSubscription')


class TestCreateSubscription(unittest.TestCase):

    def test_valid_input_json(self):
        # Data has all required attributes
        input_data = io_util.load_data_json('apigateway.input_valid.json')
        schema = io_util.load_schema_json("CreateSubscriptionCommand-RequestSchema.json")

        self.assertTrue(validate.is_valid_input_json(input_data, schema))

    def test_invalid_input_json(self):
        # Data has missing required attribute - missing asset-id
        input_data = io_util.load_data_json('apigateway.input_invalid.json')
        schema = io_util.load_schema_json("CreateSubscriptionCommand-RequestSchema.json")
        self.assertFalse(validate.is_valid_input_json(input_data, schema))

    # - lambda_handler - request failed schema validation - duplicated event-name in filter
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_invalid_request(self, mock_session):
        mock_session.return_value = None
        request_input = io_util.load_data_json('apigateway.input_invalid_duplicated_event.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            with self.assertRaisesRegex(InvalidRequestPropertyName,
                                        r"data.filter.event-name must contain unique items"):
                lambda_handler(request_input, mock_lambda_context.MockLambdaContext())

    # + Successful test of SQS no Filter - no schema-version specified then old version is assigned ('v0')
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_sqs_success(self, session_mock, get_pending_expiration_epoch_mock,
                                                     mock_collection_get_item, mock_subscription_query_items,
                                                     mock_counter_update, mock_event_store_put_item,
                                                     mock_session_get_account_id, mock_sns_get_client_region,
                                                     mock_get_owner_by_api_key_id):
        session_mock.return_value = None
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_valid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_counter_update.return_value = 324
        mock_event_store_put_item.return_value = None
        mock_session_get_account_id.return_value = 195052678233
        mock_sns_get_client_region.return_value = "us-east-1"
        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        endpoint_index = 'subscription-endpoint-index'
        get_pending_expiration_epoch_mock.return_value = 1525982039
        mock_get_owner_by_api_key_id.return_value = io_util.load_data_json("dynamo.owner_data_valid_2.json")
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:668290025664:SampleQueue",
            }

        expected_response_output = io_util.load_data_json('apigateway.output_valid.json')
        with patch('create_subscription_command.topic_arn', topic_arn):
            with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
                with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                    with patch(
                            'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                        mock_is_valid_service_region.return_value = True
                        mock_is_valid_account_id.return_value = True
                        mock_is_valid_resource_name.return_value = True
                        with patch('lng_datalake_commands.command_wrapper._response_schema',
                                   io_util.load_schema_json(
                                       'CreateSubscriptionCommand-ResponseSchema.json')):
                            output = create_subscription_command(request_input, "abcd", "LATEST",
                                                                 "testApiKeyId")[
                                'response-dict']
                            self.assertEqual(expected_response_output, output)

    # - Failure - Unable to Access SubscriptionTable
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_failed_subscription_table(self, session_mock,
                                                                   get_pending_expiration_epoch_mock,
                                                                   mock_collection_get_item,
                                                                   mock_subscription_query_items,
                                                                   mock_counter_update, mock_event_store_put_item,
                                                                   mock_session_get_account_id,
                                                                   mock_sns_get_client_region,
                                                                   mock_get_owner_by_api_key_id):
        session_mock.return_value = None
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_valid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_subscription_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                                 'Error': {
                                                                     'Code': 'Unit Test',
                                                                     'Message': 'This is a mock'}},
                                                                "FAKE")
        mock_counter_update.return_value = 324
        mock_event_store_put_item.return_value = None
        mock_session_get_account_id.return_value = 195052678233
        mock_sns_get_client_region.return_value = "us-east-1"
        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        endpoint_index = 'subscription-endpoint-index'
        get_pending_expiration_epoch_mock.return_value = 1525982039
        mock_get_owner_by_api_key_id.return_value = io_util.load_data_json("dynamo.owner_data_valid_2.json")
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub",
            }
        with patch('create_subscription_command.topic_arn', topic_arn):
            with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
                with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                    with patch(
                            'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                        mock_is_valid_service_region.return_value = True
                        mock_is_valid_account_id.return_value = True
                        mock_is_valid_resource_name.return_value = True
                        with patch('lng_datalake_commands.command_wrapper._response_schema',
                                   io_util.load_schema_json(
                                       'CreateSubscriptionCommand-ResponseSchema.json')):
                            with self.assertRaisesRegex(InternalError,
                                                        "Unable to query items from Subscription Table"):
                                create_subscription_command(request_input, "abcd", "LATEST",
                                                            "testApiKeyId")

    # - Failure - Unknown Exception SubscriptionTable
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_failed_unknown_subscription_table(self, session_mock,
                                                                           get_pending_expiration_epoch_mock,
                                                                           mock_collection_get_item,
                                                                           mock_subscription_query_items,
                                                                           mock_counter_update,
                                                                           mock_event_store_put_item,
                                                                           mock_session_get_account_id,
                                                                           mock_sns_get_client_region,
                                                                           mock_get_owner_by_api_key_id):
        session_mock.return_value = None
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_valid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_subscription_query_items.side_effect = Exception
        mock_counter_update.return_value = 324
        mock_event_store_put_item.return_value = None
        mock_session_get_account_id.return_value = 195052678233
        mock_sns_get_client_region.return_value = "us-east-1"
        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        endpoint_index = 'subscription-endpoint-index'
        get_pending_expiration_epoch_mock.return_value = 1525982039,
        mock_get_owner_by_api_key_id.return_value = io_util.load_data_json("dynamo.owner_data_valid_2.json")
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub",
            }
        with patch('create_subscription_command.topic_arn', topic_arn):
            with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
                with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                    with patch(
                            'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                        mock_is_valid_service_region.return_value = True
                        mock_is_valid_account_id.return_value = True
                        mock_is_valid_resource_name.return_value = True
                        with patch('lng_datalake_commands.command_wrapper._response_schema',
                                   io_util.load_schema_json(
                                       'CreateSubscriptionCommand-ResponseSchema.json')):
                            with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
                                create_subscription_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # - Failure - Empty Query SubscriptionTable
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_failed_empty_subscription_table(self, session_mock,
                                                                         get_pending_expiration_epoch_mock,
                                                                         mock_collection_get_item,
                                                                         mock_subscription_query_items,
                                                                         mock_counter_update, mock_event_store_put_item,
                                                                         mock_session_get_account_id,
                                                                         mock_sns_get_client_region,
                                                                         mock_get_owner_by_api_key_id):
        session_mock.return_value = None
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_valid.json')['Items']
        mock_subscription_query_items.return_value = [{
            "Endpoint": "arunprasath9586@gmail.com",
            "Filter": {
                "collection-id": [
                    "10000"
                ],
                "event-name": [
                    "Object::Create"
                ]
            },
            "PendingExpirationEpoch": 1525595334,
            "Protocol": "email",
            "SubscriptionArn": "pending confirmation",
            "SubscriptionID": 99,
            "SubscriptionName": "My Subscription 7",
            "SubscriptionState": "Subscription::Create",
            "SchemaVersion": ["v0"]
        }]
        mock_counter_update.return_value = 324
        mock_event_store_put_item.return_value = None
        mock_session_get_account_id.return_value = 195052678233
        mock_sns_get_client_region.return_value = "us-east-1"
        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        endpoint_index = 'subscription-endpoint-index'
        get_pending_expiration_epoch_mock.return_value = 1525982039
        mock_get_owner_by_api_key_id.return_value = io_util.load_data_json("dynamo.owner_data_valid_2.json")
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub",
            }
        with patch('create_subscription_command.topic_arn', topic_arn):
            with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
                with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                    with patch(
                            'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                        mock_is_valid_service_region.return_value = True
                        mock_is_valid_account_id.return_value = True
                        mock_is_valid_resource_name.return_value = True
                        with patch('lng_datalake_commands.command_wrapper._response_schema',
                                   io_util.load_schema_json(
                                       'CreateSubscriptionCommand-ResponseSchema.json')):
                            with self.assertRaisesRegex(SemanticError, "Subscription for endpoint already exists"):
                                create_subscription_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # - Failure - Invalid email Arn
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_failed_email_addr(self, session_mock, get_pending_expiration_epoch_mock,
                                                           mock_collection_get_item, mock_subscription_query_items,
                                                           mock_counter_update, mock_event_store_put_item,
                                                           mock_session_get_account_id, mock_sns_get_client_region,
                                                           mock_get_owner_by_api_key_id):
        session_mock.return_value = None
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_valid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_counter_update.return_value = 324
        mock_event_store_put_item.return_value = None
        mock_session_get_account_id.return_value = 195052678233
        mock_sns_get_client_region.return_value = "us-east-1"
        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        endpoint_index = 'subscription-endpoint-index'
        get_pending_expiration_epoch_mock.return_value = 1525982039
        mock_get_owner_by_api_key_id.return_value = io_util.load_data_json("dynamo.owner_data_valid_2.json")
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "email",
                "endpoint": "arn:aws-lambda:us-east-1:288044017584:function:AWSTEST-"
                            "GetChangeSetInCollectionLam-LambdaFunction-1JT9C4T67FEW8",
                "filter": {
                    "event-name": [
                        "Object::Create"
                    ],
                    "collection-id": [
                        "123"
                    ]
                }
            }

        with patch('create_subscription_command.topic_arn', topic_arn):
            with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
                with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                    with patch(
                            'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                        mock_is_valid_service_region.return_value = True
                        mock_is_valid_account_id.return_value = True
                        mock_is_valid_resource_name.return_value = True
                        with patch('lng_datalake_commands.command_wrapper._response_schema',
                                   io_util.load_schema_json(
                                       'CreateSubscriptionCommand-ResponseSchema.json')):
                            with self.assertRaisesRegex(InvalidRequestPropertyValue, "Endpoint is invalid"):
                                create_subscription_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # - Failure - Invalid Lambda Arn
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_failed_lambda_arn(self, session_mock, get_pending_expiration_epoch_mock,
                                                           mock_collection_get_item, mock_subscription_query_items,
                                                           mock_counter_update, mock_event_store_put_item,
                                                           mock_session_get_account_id, mock_sns_get_client_region,
                                                           mock_get_owner_by_api_key_id):
        session_mock.return_value = None
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_valid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_counter_update.return_value = 324
        mock_event_store_put_item.return_value = None
        mock_session_get_account_id.return_value = 195052678233
        mock_sns_get_client_region.return_value = "us-east-1"
        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        endpoint_index = 'subscription-endpoint-index'
        get_pending_expiration_epoch_mock.return_value = 1525982039
        mock_get_owner_by_api_key_id.return_value = io_util.load_data_json("dynamo.owner_data_valid_2.json")
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "lambda",
                "endpoint": "arn:aws-lambda:us-east-1:288044017584:function:AWSTEST-"
                            "GetChangeSetInCollectionLam-LambdaFunction-1JT9C4T67FEW8",
                "filter": {
                    "event-name": [
                        "Object::Create"
                    ],
                    "collection-id": [
                        "123"
                    ]
                }
            }

        with patch('create_subscription_command.topic_arn', topic_arn):
            with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
                with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                    with patch(
                            'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                        mock_is_valid_service_region.return_value = True
                        mock_is_valid_account_id.return_value = True
                        mock_is_valid_resource_name.return_value = True
                        with patch('lng_datalake_commands.command_wrapper._response_schema',
                                   io_util.load_schema_json(
                                       'CreateSubscriptionCommand-ResponseSchema.json')):
                            with self.assertRaisesRegex(InvalidRequestPropertyValue, "Endpoint is invalid"):
                                create_subscription_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # - Failure - Invalid Collection ID
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_failed_collection_id(self, session_mock, get_pending_expiration_epoch_mock,
                                                              mock_collection_get_item, mock_subscription_query_items,
                                                              mock_counter_update, mock_event_store_put_item,
                                                              mock_session_get_account_id, mock_sns_get_client_region,
                                                              mock_get_owner_by_api_key_id):
        session_mock.return_value = None
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_invalid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_counter_update.return_value = 324
        mock_event_store_put_item.return_value = None
        mock_session_get_account_id.return_value = 195052678233
        mock_sns_get_client_region.return_value = "us-east-1"
        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        endpoint_index = 'subscription-endpoint-index'
        get_pending_expiration_epoch_mock.return_value = 1525982039
        mock_get_owner_by_api_key_id.return_value = io_util.load_data_json("dynamo.owner_data_valid_2.json")
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub",
                "filter": {
                    "event-name": [
                        "Object::Create"
                    ],
                    "collection-id": [
                        "123"
                    ]
                }
            }

        with patch('create_subscription_command.topic_arn', topic_arn):
            with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
                with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                    with patch(
                            'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                        mock_is_valid_service_region.return_value = True
                        mock_is_valid_account_id.return_value = True
                        mock_is_valid_resource_name.return_value = True
                        with patch('lng_datalake_commands.command_wrapper._response_schema',
                                   io_util.load_schema_json(
                                       'CreateSubscriptionCommand-ResponseSchema.json')):
                            with self.assertRaisesRegex(NoSuchCollection,
                                                        "Invalid Collection ID {}".format(
                                                            request_input['filter']['collection-id'][0])):
                                create_subscription_command(request_input, "abcd", "LATEST", "testApiKeyId")

    # - owner authorization error (wrong API Key)
    @patch('lng_datalake_commands.owner_authorization.get_owner_by_api_key_id')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_owner_auth(self, mock_session,
                                                    mock_get_owner_by_api_key_id):
        mock_session.return_value = None
        mock_get_owner_by_api_key_id.side_effect = NotAuthorizedError("API Key provided is not valid for any Owner", "")

        request_input = {'subscription-id': 20}
        with self.assertRaisesRegex(NotAuthorizedError, "API Key provided is not valid for any Owner"):
            create_subscription_command(request_input, "563r1jug9i", "LATEST", "testApiKeyId")

    # + Successful test of validate_request
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_request_success(self,
                                      mock_collection_get_item, mock_subscription_query_items,
                                      mock_event_store_put_item,
                                      mock_sns_get_client_region):
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_valid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_event_store_put_item.return_value = None
        mock_sns_get_client_region.return_value = "us-east-1"
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub",
                "filter": {
                    "event-name": [
                        "Object::Create"
                    ],
                    "collection-id": [
                        "123"
                    ]
                }
            }
        with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
            with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                with patch(
                        'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                    mock_is_valid_service_region.return_value = True
                    mock_is_valid_account_id.return_value = True
                    mock_is_valid_resource_name.return_value = True
                    with patch('lng_datalake_commands.command_wrapper._response_schema',
                               io_util.load_schema_json(
                                   'CreateSubscriptionCommand-ResponseSchema.json')):
                        self.assertIsNone(validate_request(request_input))

    # - failure test of validate_request
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_request_failure(self,
                                      mock_collection_get_item, mock_subscription_query_items,
                                      mock_event_store_put_item,
                                      mock_sns_get_client_region):
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_invalid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_event_store_put_item.return_value = None
        mock_sns_get_client_region.return_value = "us-east-1"
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub",
                "filter": {
                    "event-name": [
                        "Object::Create"
                    ],
                    "collection-id": [
                        "123"
                    ]
                }
            }
        with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
            with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                with patch(
                        'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                    mock_is_valid_service_region.return_value = True
                    mock_is_valid_account_id.return_value = True
                    mock_is_valid_resource_name.return_value = True
                    with patch('lng_datalake_commands.command_wrapper._response_schema',
                               io_util.load_schema_json(
                                   'CreateSubscriptionCommand-ResponseSchema.json')):
                        with self.assertRaisesRegex(NoSuchCollection,
                                                    "Invalid Collection ID {}".format(
                                                        request_input['filter']['collection-id'][0])):
                            validate_request(request_input)

    # - validate_request - invalid endpoint fifo queue
    @patch('lng_aws_clients.sns.get_client_region')
    def test_validate_request_invalid_fifo(self,
                                           mock_sns_get_client_region):
        mock_sns_get_client_region.return_value = "us-east-1"
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:195052678233:test.fifo",
                "filter": {
                    "event-name": [
                        "Object::Create"
                    ],
                    "collection-id": [
                        "123"
                    ]
                }
            }
        with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
            with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                with patch(
                        'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                    mock_is_valid_service_region.return_value = True
                    mock_is_valid_account_id.return_value = True
                    mock_is_valid_resource_name.return_value = True
                    with self.assertRaisesRegex(InvalidRequestPropertyValue, "FIFO queues are currently not supported"):
                        validate_request(request_input)

    # TODO delete this when v0 schema is removed
    # - validate_request - invalid filter for event-version
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_datalake_dal.event_store_table.EventStoreTable.put_item')
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    @patch('lng_datalake_dal.collection_table.CollectionTable.get_item')
    def test_validate_request_invalid_filter_for_event_version(self,
                                                               mock_collection_get_item, mock_subscription_query_items,
                                                               mock_event_store_put_item,
                                                               mock_sns_get_client_region):
        mock_collection_get_item.return_value = \
            io_util.load_data_json('dynamo.collection_data_valid.json')['Items']
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        mock_event_store_put_item.return_value = None
        mock_sns_get_client_region.return_value = "us-east-1"
        request_input = \
            {
                "subscription-name": "My Subscription",
                "protocol": "sqs",
                "endpoint": "arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub",
                "filter": {
                    "event-name": [
                        "Object::UpdateNoChange"
                    ],
                    "collection-id": [
                        "123"
                    ]
                },
                "schema-version": ["v0"]
            }
        with patch('lng_aws_clients.session.helpers.is_valid_service_region') as mock_is_valid_service_region:
            with patch('lng_aws_clients.session.helpers.is_valid_account_id') as mock_is_valid_account_id:
                with patch(
                        'lng_aws_clients.session.helpers.is_valid_resource_name') as mock_is_valid_resource_name:
                    mock_is_valid_service_region.return_value = True
                    mock_is_valid_account_id.return_value = True
                    mock_is_valid_resource_name.return_value = True
                    with patch('lng_datalake_commands.command_wrapper._response_schema',
                               io_util.load_schema_json(
                                   'CreateSubscriptionCommand-ResponseSchema.json')):
                        with self.assertRaisesRegex(SemanticError,
                                                    "Filter is invalid for given event-version||v0 subscriptions do not allow filtering on Object::UpdateNoChange events"):
                            validate_request(request_input)

    # + Successful test of is_unique_subscription
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    def test_is_unique_subscription_success(self,
                                            mock_subscription_query_items,
                                            ):
        mock_subscription_query_items.return_value = \
            io_util.load_data_json('dynamo.subscription_name_owner_data_valid.json')['Items']
        self.assertTrue(is_unique_subscription, 'arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub')
        mock_subscription_query_items.return_value = [{
            'subscription-id': 0,
            "endpoint": ""
        }]
        result = is_unique_subscription('arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub')
        self.assertFalse(result)

    # - failure test of is_unique_subscription
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.query_items')
    def test_is_unique_subscription_failure(self,
                                            mock_subscription_query_items,
                                            ):
        mock_subscription_query_items.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            is_unique_subscription('arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub')

        mock_subscription_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                                 'Error': {
                                                                     'Code': 'Unit Test',
                                                                     'Message': 'This is a mock'}},
                                                                "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to query items from Subscription Table"):
            is_unique_subscription('arn:aws:sqs:us-east-1:288044017584:JohnnyKLambdaSub')

    # - generate new subscription id - ClientError
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    def test_generate_subscription_id_exception_1(self, mock_counter_update):
        mock_counter_update.side_effect = ClientError({'ResponseMetadata': {},
                                                       'Error': {
                                                           'Code': 'OTHER',
                                                           'Message': 'This is a mock'}},
                                                      "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to update Counter Table"):
            generate_subscription_id()

    # - generate new subscription id - Generic Exception
    @patch('lng_datalake_dal.counter_table.CounterTable.update_counter')
    def test_generate_subscription_id_exception_2(self, mock_counter_update):
        mock_counter_update.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            generate_subscription_id()

    # Success test of generate_next_action_message
    def test_generate_next_action_message(self):
        self.assertEqual(generate_next_action_message('email'), 'Check your email, there will be an email containing a'
                                                                ' URL that needs to be confirmed before '
                                                                'the subscription is live')
        self.assertEqual(generate_next_action_message('d'),
                         'Check your SQS queue for a message to confirm your subscription. '
                         'Please refer to the documentation for help here: https://wiki.regn.net/wiki/DataLake_Hello_World#Subscriptions')

    # - FAILED contain required field
    @patch('lng_datalake_commands.command_wrapper.get_optional_response_schema_keys')
    @patch('lng_datalake_commands.command_wrapper.get_required_response_schema_keys')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    def test_generate_response_json_failure(self, get_pending_expiration_epoch_mock, required_prop_mock,
                                            optional_props_mock):
        required_prop_mock.return_value = ['subscription-name', 'subscription-id', 'state', 'pending-expiration-epoch'
            , 'next-action']
        optional_props_mock.return_value = ['commands']
        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        get_pending_expiration_epoch_mock.return_value = 1525982039
        request_input = \
            {
                'subscription-name': 'My Subscription',
                'protocol': 'email',
                'endpoint': 'Code@java.python.perl.scala.csharp.net',
                'filter': {
                    'event-name': ['Object::Create'],
                    'collection-id': ['123']
                }
            }
        with patch('create_subscription_command.topic_arn', topic_arn):
            with self.assertRaisesRegex(InternalError, "Missing required attribute: state"):
                generate_response_json(request_input, 324)

    def test_generate_sqs_policy_success(self):
        request = {
            "filter": {
                "collection-id": [
                    "collection-mutable-id-regression-1541435966"
                ],
                "event-name": [
                    "Object::Create"
                ]
            },
            "endpoint": "arn:aws:sqs:us-east-1:668290025664:SampleQueue",
            "protocol": "sqs",
            "subscription-name": "test_sub_06",
            "description": "desc"
        }

        topic_arn = "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"
        expected_response = {
            "Version": "2008-10-17",
            "Id": "arn:aws:sqs:us-east-1:668290025664:SampleQueue/SQSDefaultPolicy",
            "Statement": [
                {"Sid": "DataLakeSubscription",
                 "Effect": "Allow",
                 "Principal": "*",
                 "Action": "SQS:SendMessage",
                 "Resource": "arn:aws:sqs:us-east-1:668290025664:SampleQueue",
                 "Condition":
                     {
                         "ArnEquals": {
                             "aws:SourceArn": "arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c"}
                     }
                 }
            ]
        }
        with patch('create_subscription_command.topic_arn', topic_arn):
            self.assertDictEqual(expected_response, generate_sqs_policy(request))

    # Success test of generate_next_action_message
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_datalake_commands.command_wrapper.get_request_time')
    def test_generate_event_dict(self, mock_time, mock_expiration_date):
        mock_time.return_value = '2018-05-18T19:31:17.813Z'
        mock_expiration_date.return_value = 15161611
        response_dict = {'subscription': {'subscription-name': 'My Subscription', 'subscription-id': 324,
                                          'subscription-state': 'Subscription::Create'},
                         'completion-metadata': {'pending-expiration-epoch': 15161611,
                                                 'next-action': 'Check your email, there will be an email containing a '
                                                                'URL that needs to be confirmed before the subscription is live'}}
        request = {'subscription-name': 'My Subscription', 'protocol': 'email',
                   'endpoint': 'Code@java.python.perl.scala.csharp.net',
                   'schema-version': ['v0'],
                   'filter': {'event-name': ['Object::Create'], 'collection-id': ['123']},
                   'next-action': 'Check your email, there will be an email containing a URL that '
                                  'needs to be confirmed before the subscription is live'}
        expected_result = {
            'subscription-name': 'My Subscription',
            'event-id': '563r1jug9i',
            'request-time': mock_time.return_value,
            'event-name': 'Subscription::Create',
            'subscription-id': 324,
            'endpoint': 'Code@java.python.perl.scala.csharp.net',
            'protocol': 'email',
            'filter': {
                'event-name': ['Object::Create'],
                'collection-id': ['123']
            },
            'pending-expiration-epoch': 15161611,
            'event-version': 1,
            'stage': 'LATEST',
            'owner-id': 1,
            'schema-version': ['v0'],
        }

        result = generate_event_dict(response_dict, request, '563r1jug9i', "LATEST", 1)
        self.assertDictEqual(result, expected_result)


if __name__ == '__main__':
    unittest.main()
