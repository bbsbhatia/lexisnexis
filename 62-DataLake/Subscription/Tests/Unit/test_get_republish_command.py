import unittest
from unittest.mock import patch

from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import NoSuchRepublish, InternalError, UnhandledException, \
    NoSuchSubscription, SemanticError
from lng_datalake_commons import validate
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_republish_command import lambda_handler, get_validated_republish_record, validate_subscription, \
    generate_response

__author__ = "John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetRepublish')


class TestGetRepublishCommand(unittest.TestCase):
    stage = 'LATEST'

    def test_valid_input_json(self):
        # Data has all required attributes
        input_data = io_util.load_data_json('apigateway.input_valid.json')
        schema = io_util.load_schema_json("GetRepublishCommand-RequestSchema.json")
        self.assertTrue(validate.is_valid_input_json(input_data, schema))

    def test_invalid_input_json(self):
        # Data has missing required attribute - invalid event-name
        input_data = io_util.load_data_json('apigateway.input_invalid.json')
        schema = io_util.load_schema_json("GetRepublishCommand-RequestSchema.json")
        self.assertFalse(validate.is_valid_input_json(input_data, schema))

    # + lambda handler
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_datalake_dal.republish_table.RepublishTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success(self,
                                    session_mock,
                                    mock_republish_get_item,
                                    mock_subscription_get_item
                                    ):
        session_mock.return_value = None
        mock_republish_get_item.return_value = io_util.load_data_json('dynamo.republish_data_valid.json')
        mock_subscription_get_item.return_value = io_util.load_data_json('dynamo.subscription_data_valid.json')

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response_output = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            response = lambda_handler(request_input, MockLambdaContext())
            self.assertDictEqual(response, expected_response_output)

    # - test_get_validated_republish_record_client_error => General Exception
    @patch('lng_datalake_dal.republish_table.RepublishTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_validated_republish_record_general_exception(self,
                                                         session_mock,
                                                         mock_republish_get_item,
                                                         ):
        session_mock.return_value = None
        mock_republish_get_item.side_effect = Exception('MockError')

        with self.assertRaisesRegex(UnhandledException, 'MockError'):
            get_validated_republish_record('123', 1)

    # - test_get_validated_republish_record_client_error => ClientError
    @patch('lng_datalake_dal.republish_table.RepublishTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_validated_republish_record_client_error(self,
                                                         session_mock,
                                                         mock_republish_get_item,
                                                         ):
        session_mock.return_value = None
        mock_republish_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                           'Error': {
                                                               'Code': 'MockClientError',
                                                               'Message': 'This is a mock'}},
                                                          "FAKE")

        with self.assertRaisesRegex(InternalError, 'Unable to get item from Republish Table'):
            get_validated_republish_record('123', 1)

    # - test_get_validated_republish_record_mismatch_subscription => NoSuchRepublish
    @patch('lng_datalake_dal.republish_table.RepublishTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_validated_republish_record_mismatch_subscription(self,
                                                                  session_mock,
                                                                  mock_republish_get_item,
                                                                  ):
        session_mock.return_value = None
        republish_item = io_util.load_data_json('dynamo.republish_data_invalid_subscript_id.json')
        mock_republish_get_item.return_value = republish_item
        exception_msg = 'Republish ID {0} does not exist in subscription ID {1}'. \
            format(republish_item['republish-id'], 1)

        with self.assertRaisesRegex(NoSuchRepublish, exception_msg):
            get_validated_republish_record(republish_item['republish-id'], 1)

    # - test_get_validated_republish_record_no_republish => NoSuchRepublish
    @patch('lng_datalake_dal.republish_table.RepublishTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_get_validated_republish_record_no_republish(self,
                                                         session_mock,
                                                         mock_republish_get_item,
                                                         ):
        invalid_republish_id = '123'
        session_mock.return_value = None
        mock_republish_get_item.return_value = {}
        exception_msg = 'Republish ID {} does not exist'.format(invalid_republish_id)

        with self.assertRaisesRegex(NoSuchRepublish, exception_msg):
            get_validated_republish_record(invalid_republish_id, 1)

    # + test_validate_subscription_success
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_subscription_success(self,
                                           session_mock,
                                           mock_subscription_get_item
                                           ):
        session_mock.return_value = None
        mock_subscription_get_item.return_value = io_util.load_data_json('dynamo.subscription_data_valid.json')

        self.assertIsNone(validate_subscription(1))
        mock_subscription_get_item.assert_called_once_with({"subscription-id": 1})

    # - test_validate_subscription_client_error => InternalError
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_subscription_client_error(self,
                                                session_mock,
                                                mock_subscription_get_item
                                                ):
        session_mock.return_value = None
        mock_subscription_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                              'Error': {
                                                                  'Code': 'MockClientError',
                                                                  'Message': 'This is a mock'}},
                                                             "FAKE")

        with self.assertRaisesRegex(InternalError, 'Unable to get item from Subscription Table'):
            validate_subscription(1)

    # - test_validate_subscription_exception => UnhandledException
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_subscription_exception(self,
                                             session_mock,
                                             mock_subscription_get_item
                                             ):
        session_mock.return_value = None
        mock_subscription_get_item.side_effect = Exception('Mock Exception')

        with self.assertRaisesRegex(UnhandledException, 'Mock Exception'):
            validate_subscription(1)

    # - test_validate_subscription_return_none => NoSuchSubscription
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_subscription_return_none(self,
                                               session_mock,
                                               mock_subscription_get_item
                                               ):
        session_mock.return_value = None
        mock_subscription_get_item.return_value = None

        with self.assertRaisesRegex(NoSuchSubscription, 'Invalid Subscription ID 1'):
            validate_subscription(1)

    # - test_validate_subscription_terminated => NoSuchSubscription
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_subscription_terminated(self,
                                              session_mock,
                                              mock_subscription_get_item
                                              ):
        session_mock.return_value = None
        mock_subscription_get_item.return_value = io_util.load_data_json(
            'dynamo.subscription_data_valid_terminated.json')

        with self.assertRaisesRegex(NoSuchSubscription, 'Invalid Subscription ID 1'):
            validate_subscription(1)

    # - test_validate_subscription_pending => SemanticError
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_subscription_pending(self,
                                           session_mock,
                                           mock_subscription_get_item
                                           ):
        session_mock.return_value = None
        mock_subscription_get_item.return_value = io_util.load_data_json('dynamo.subscription_data_valid_pending.json')

        with self.assertRaisesRegex(SemanticError, 'Subscription ID 1 is in Pending state'):
            validate_subscription(1)

    # - test_validate_subscription_pending => SemanticError
    @patch('lng_datalake_dal.subscription_table.SubscriptionTable.get_item')
    @patch('lng_aws_clients.session.set_session')
    def test_validate_subscription_pending(self,
                                           session_mock,
                                           mock_subscription_get_item
                                           ):
        session_mock.return_value = None
        mock_subscription_get_item.return_value = io_util.load_data_json('dynamo.subscription_data_valid_pending.json')

        with self.assertRaisesRegex(SemanticError, 'Subscription ID 1 is in Pending state'):
            validate_subscription(1)

    # + test_generate_response_success
    def test_generate_response_success(self):
        republish_record = io_util.load_data_json('dynamo.republish_data_valid.json')
        expected_response_record = io_util.load_data_json('lambda.republish_response_valid.json')
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetRepublishCommand-ResponseSchema.json')):
            response_record = generate_response(republish_record)
            self.assertDictEqual(response_record, expected_response_record)

    # - test_generate_response_missing_required_attribute => InternalError
    def test_generate_response_missing_required_attribute(self):
        republish_record = io_util.load_data_json('dynamo.republish_data_invalid_missing_subscipt_id.json')
        with patch('lng_datalake_commands.command_wrapper._response_schema',
                   io_util.load_schema_json('GetRepublishCommand-ResponseSchema.json')):
            with self.assertRaisesRegex(InternalError, r'Missing required attribute: subscription-id'):
                generate_response(republish_record)


if __name__ == '__main__':
    unittest.main()
