import json
import os
import unittest

from lng_datalake_client.Subscription.get_subscription import get_subscription

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestGetSubscription(unittest.TestCase):
    default_subscription_name = os.getenv("SUBSCRIPTION_NAME", "subs_test_13")
    default_subscription_state = os.getenv("SUBSCRIPTION_STATE", "Created")
    default_subscription_id = int(os.getenv("SUBSCRIPTION_ID", 14))
    default_protocol = os.getenv("PROTOCOL", "email")
    default_endpoint = os.getenv("ENDPOINT", "srivaspm@legal.regn.net")
    default_filter = os.getenv("FILTER",
                               '{"collection-id": ["14"], "event-name": ["Object::Create", "Collection::Create"]}')
    default_subscription_arn = os.getenv("SUBSCRIPTION_ARN", "pending confirmation")
    current_schema_version = ["v0"]

    # + Test the 200 status code and body
    def test_get_subscription(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }
        expected_response = \
            {
                "subscription-name": input_dict.get('subscription-name', self.default_subscription_name),
                "subscription-state": input_dict.get('subscription-state', self.default_subscription_state),
                "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
                "protocol": input_dict.get('protocol', self.default_protocol),
                "endpoint": input_dict.get('endpoint', self.default_endpoint),
                "schema-version": input_dict.get('schema-version', self.current_schema_version),
                "filter": input_dict.get('filter', json.loads(self.default_filter))
            }

        response = get_subscription(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn('context', response_dict)
        self.assertTrue(isinstance(response_dict['context'].get('request-id'), str))
        self.assertTrue(isinstance(response_dict['context'].get("api-id"), str))
        self.assertTrue(isinstance(response_dict['context'].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        # test the response body
        self.assertIn('subscription', response_dict)
        self.assertDictEqual(expected_response, response_dict['subscription'])
        return response_dict['subscription']

    # - get_subscription with invalid subscription id --400
    def test_get_subscription_invalid_subscription_id(self):

        request_dict = {
            "subscription-id": -1
        }

        response = get_subscription(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchSubscription")
        self.assertEqual(json_error_resp['message'], "Invalid Subscription ID {}".format(-1))
        self.assertEqual(json_error_resp['corrective-action'], "Subscription ID does not exist")

    # - get_subscription with invalid x-api-key -- 403
    def test_get_subscription_invalid_x_api_key(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "headers": {
                    'Content-type': 'application/json'
                },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = get_subscription(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - get_subscription with invalid content-type -- 415
    def test_get_subscription_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "subscription-id": self.default_subscription_id
        }

        response = get_subscription(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
