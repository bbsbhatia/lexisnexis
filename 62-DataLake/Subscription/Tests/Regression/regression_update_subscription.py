import json
import os
import unittest
from datetime import datetime

from lng_datalake_client.Subscription.update_subscription import update_subscription

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestUpdateSubscription(unittest.TestCase):
    default_subscription_name = 'Object Updated Subscription for Collection Ab-1_{}'.format(datetime.now())
    default_subscription_state = os.getenv("SUBSCRIPTION_STATE", "Created")
    default_subscription_id = int(os.getenv("SUBSCRIPTION_ID", 169))
    default_protocol = os.getenv("PROTOCOL", "email")
    default_endpoint = os.getenv("ENDPOINT", "regression_test@lexisnexis.com")
    default_filter = os.getenv("FILTER", '{"event-name": ["Object::Update"]}')
    current_schema_versions = ["v0", "v1"]
    invalid_schema_version = ["v2"]
    invalid_op = "invalidOp"
    invalid_path = "/invalid-path"
    invalid_val = -1

    # + Test the 202 status code and body
    def test_update_subscription(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        new_name = input_dict.get('subscription-name', self.default_subscription_name)
        new_schema_version = input_dict.get('schema-version', self.current_schema_versions[-1:])
        new_filter = input_dict.get('filter', json.loads(self.default_filter))
        request_dict = \
            {
                "body": {
                    "patch-operations": [
                        {
                            "op": "replace",
                            "path": "/subscription-name",
                            "value": new_name
                        },
                        {
                            "op": "replace",
                            "path": "/schema-version",
                            "value": new_schema_version
                        },
                        {
                            "op": "replace",
                            "path": "/filter",
                            "value": new_filter
                        }
                    ]
                },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id),
            }

        expected_response = {
            "subscription-name": new_name,
            'schema-version': new_schema_version,
            "filter": new_filter,
            "subscription-id": request_dict['subscription-id']
        }

        response = update_subscription(request_dict)

        # test the status code 202
        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn('context', response_dict)
        self.assertTrue(isinstance(response_dict['context'].get('request-id'), str))
        self.assertTrue(isinstance(response_dict['context'].get("api-id"), str))
        self.assertTrue(isinstance(response_dict['context'].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn('subscription', response_dict)
        for key in expected_response.keys():
            self.assertEqual(expected_response[key], response_dict['subscription'][key])
        return response_dict['subscription']

    # - update_subscription with invalid subscription id --400
    def test_update_subscription_invalid_subscription_id(self):

        request_dict = {
            "body": {
                "patch-operations": [
                    {"op": "replace",
                     "path": "/subscription-name",
                     "value": "subscription with invalid id"
                     }]
            },
            "subscription-id": -1
        }

        response = update_subscription(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchSubscription")
        self.assertEqual(json_error_resp['message'], "Invalid Subscription ID {}".format(-1))
        self.assertEqual(json_error_resp['corrective-action'], "Subscription ID does not exist")

    # - update_subscription with invalid schema version --400
    def test_update_subscription_invalid_schema_version(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        invalid_schema_version = input_dict.get('schema-version', self.invalid_schema_version)
        request_dict = \
            {
                "body":
                    {
                        "patch-operations": [{
                            "op": "replace",
                            "path": "/schema-version",
                            "value": invalid_schema_version
                        }]
                    },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = update_subscription(request_dict)

        # test the status code 400
        self.assertEqual(400, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid schema versions: {0}".format(invalid_schema_version))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Schema version should be one of {0}".format(self.current_schema_versions))

    # - update_subscription with invalid op --400
    def test_update_subscription_invalid_op(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "body":
                    {
                        "patch-operations": [{
                            "op": self.invalid_op,
                            "path": "/subscription-name",
                            "value": "test"
                        }]
                    },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = update_subscription(request_dict)

        # test the status code 400
        self.assertEqual(400, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Validation error for operation path /subscription-name")
        self.assertEqual(json_error_resp['corrective-action'],
                         "data.op must be one of ['replace']")

    # - update_subscription with invalid path --400
    def test_update_subscription_invalid_path(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "body":
                    {
                        "patch-operations": [{
                            "op": "replace",
                            "path": self.invalid_path,
                            "value": "test"
                        }]
                    },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = update_subscription(request_dict)

        # test the status code 400
        self.assertEqual(400, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Invalid operation path {0}".format(self.invalid_path))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Valid operation paths are ['/filter', "
                         "'/schema-version', '/subscription-name']")

    # - update_subscription with invalid value --400
    def test_update_subscription_invalid_value(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "body":
                    {
                        "patch-operations": [{
                            "op": "replace",
                            "path": "/subscription-name",
                            "value": self.invalid_val
                        }]
                    },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = update_subscription(request_dict)

        # test the status code 400
        self.assertEqual(400, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Validation error for operation path /subscription-name")
        self.assertEqual(json_error_resp['corrective-action'], "data.value must be string")

    # - update_subscription with pending subscription --422
    def test_update_subscription_pending_subscription(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "body":
                    {
                        "patch-operations": [
                            {
                                "op": "replace",
                                "path": "/subscription-name",
                                "value": "subscription with pending state"
                            }
                        ]
                    },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = update_subscription(request_dict)

        # test the status code 422
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Subscription ID {} is pending confirmation".format(request_dict['subscription-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Confirm subscription before updating it")

    # - update_subscription with pending subscription --422
    def test_update_subscription_no_data_changed(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "body":
                    {
                        "patch-operations": [{
                            "op": "replace",
                            "path": "/subscription-name",
                            "value": input_dict.get('subscription-name', self.default_subscription_name)
                        }]
                    },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = update_subscription(request_dict)

        # test the status code 422
        self.assertEqual(422, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'], "No data changes")

    # - update_subscription with invalid x-api-key -- 403
    def test_update_subscription_invalid_x_api_key(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "headers": {
                    'Content-type': 'application/json'
                },
                "body":
                    {
                        "patch-operations": [{
                            "op": "replace",
                            "path": "/subscription-name",
                            "value": input_dict.get('subscription-name', self.default_subscription_name)
                        }]
                    },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = update_subscription(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - wrong x-api-key, auth error
    def test_update_subscription_incorrect_x_api_key(self, input_dict=None, api_key=None, owner_id=None):
        if not api_key:
            api_key = "some-key"
        subscription_id = input_dict.get('subscription-id', self.default_subscription_id)
        request_dict = {
            'headers': {
                'Content-type': 'application/json',
                'x-api-key': api_key
            },
            "body":
                {
                    "patch-operations": [{
                        "op": "replace",
                        "path": "/subscription-name",
                        "value": input_dict.get('subscription-name', self.default_subscription_name)
                    }]
                },
            "subscription-id": subscription_id
        }
        response = update_subscription(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")

    # - update_subscription with invalid content-type -- 415
    def test_update_subscription_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "subscription-id": self.default_subscription_id
        }

        response = update_subscription(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
