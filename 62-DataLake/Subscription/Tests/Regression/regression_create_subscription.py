import json
import os
import unittest
from datetime import datetime

from lng_datalake_client.Subscription.create_subscription import create_subscription

__author__ = "John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestCreateSubscription(unittest.TestCase):
    default_protocol = os.getenv("PROTOCOL", "email")
    default_endpoint = os.getenv("ENDPOINT", "regression_test@lexisnexis.com")
    default_filter = os.getenv("FILTER", '{}')
    default_subscription_name = 'regression_test_{}'.format(datetime.now())
    current_schema_versions = ["v0", "v1"]
    invalid_schema_version = ["v2"]

    # + Test the 201 status code and body
    def test_create_subscription(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                'body': {
                    'subscription-name': input_dict.get('subscription-name', self.default_subscription_name),
                    'endpoint': input_dict.get('endpoint', self.default_endpoint),
                    'protocol': input_dict.get('protocol', self.default_protocol),
                    'schema-version': input_dict.get('schema-version', [self.current_schema_versions[-1]]),
                    'filter': input_dict.get('filter', json.loads(self.default_filter))
                }

            }
        for key, value in input_dict.items():
            request_dict['body'][key] = value

        expected_response = \
            {
                "subscription-name": input_dict.get('subscription-name', self.default_subscription_name),
                "subscription-state": "Pending",
                "protocol": input_dict.get('protocol', self.default_protocol),
                "endpoint": input_dict.get('endpoint', self.default_endpoint),
                "schema-version": input_dict.get('schema-version', [self.current_schema_versions[-1]]),
                "filter": input_dict.get('filter', json.loads(self.default_filter))
            }
        for key, value in input_dict.items():
            expected_response[key] = value

        response = create_subscription(request_dict)

        self.assertEqual(201, response.status_code)

        response_dict = response.json()
        self.assertIn('context', response_dict)
        self.assertTrue(isinstance(response_dict['context'].get('request-id'), str))
        self.assertTrue(isinstance(response_dict['context'].get("api-id"), str))
        self.assertTrue(isinstance(response_dict['context'].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn('subscription', response_dict)
        subscription_resp = response_dict['subscription']
        expected_response['subscription-id'] = subscription_resp['subscription-id']

        self.assertIn('completion-metadata', response_dict)
        completion_resp = response_dict['completion-metadata']

        self.assertTrue(isinstance(completion_resp['subscription-expiration-date'], str))

        if request_dict['body']['protocol'] == 'email':
            self.assertRegex(completion_resp['next-action'], "Check your email")
        else:
            self.assertRegex(completion_resp['next-action'], "documentation for help here: https://wiki.regn.net/wiki/"
                                                             "DataLake_Hello_World#Subscriptions")

        if request_dict['body']['protocol'] != 'email':
            self.assertRegex(completion_resp['topic-arn'], "arn:aws:sns")

        if request_dict['body']['protocol'] == 'sqs':
            self.assertRegex(completion_resp['queue-url'], "https://sqs")
            self.assertIn("Id", completion_resp['sqs-policy'])

        self.assertDictEqual(expected_response, subscription_resp)
        return response_dict

    # - create_subscription invalid email: 400
    def test_create_subscription_invalid_email(self):
        invalid_email = "no-email.address"
        request_dict = \
            {
                'body': {
                    "subscription-name": self.default_subscription_name,
                    "endpoint": invalid_email,
                    'protocol': "email"
                }

            }

        response = create_subscription(request_dict)
        # test the status code 400
        self.assertEqual(400, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Endpoint is invalid")
        self.assertEqual(json_error_resp['corrective-action'], "The email address provided is not a valid email format")

    # - create_subscription invalid arn: 400
    def test_create_subscription_invalid_sqs_arn(self):
        invalid_arn = "123:::SQS"
        request_dict = \
            {
                'body': {
                    "subscription-name": self.default_subscription_name,
                    "endpoint": invalid_arn,
                    'protocol': "sqs"
                }

            }

        response = create_subscription(request_dict)
        response_dict = response.json()
        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response_dict['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Endpoint is invalid")
        self.assertEqual(json_error_resp['corrective-action'], "The endpoint arn provided is not a valid arn format")

    # - create_subscription invalid schema version: 400
    def test_create_subscription_invalid_schema_version(self):
        request_dict = \
            {
                'body': {
                    "subscription-name": self.default_subscription_name,
                    "endpoint": self.default_endpoint,
                    "protocol": self.default_protocol,
                    "schema-version": self.invalid_schema_version
                }

            }

        response = create_subscription(request_dict)
        # test the status code 400
        self.assertEqual(400, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid schema versions: {0}".format(self.invalid_schema_version))
        self.assertEqual(json_error_resp['corrective-action'],
                         "Schema version should be one of {0}".format(self.current_schema_versions))

    # - create_subscription with invalid x-api-key -- 403
    def test_create_subscription_invalid_x_api_key(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "headers": {
                    'Content-type': 'application/json'
                },
                'body': {
                    "subscription-name": input_dict.get('subscription-name', self.default_subscription_name),
                    "endpoint": input_dict.get('end-point', self.default_endpoint),
                    'protocol': input_dict.get('protocol', self.default_protocol),
                    'filter': input_dict.get('filter', json.loads(self.default_filter))
                }

            }

        response = create_subscription(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - create_subscription with invalid content-type 415
    def test_create_subscription_invalid_content_type(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            'body': {
                "subscription-name": input_dict.get('subscription-name', self.default_subscription_name),
                "endpoint": input_dict.get('end-point', self.default_endpoint),
                'protocol': input_dict.get('protocol', self.default_protocol),
                'filter': input_dict.get('filter', json.loads(self.default_filter))
            }

        }

        response = create_subscription(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - create_subscription invalid collection-id: 422
    def test_create_subscription_invalid_collection_id(self):
        request_dict = \
            {
                'body': {
                    "subscription-name": self.default_subscription_name,
                    "endpoint": self.default_endpoint,
                    'protocol': self.default_protocol,
                    "filter": {
                        "collection-id": ["-1"],
                        "event-name": ["Object::Create"]
                    }
                }

            }

        response = create_subscription(request_dict)
        # test the status code 404
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'], "Invalid Collection ID {0}".format(-1))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID does not exist")

    # - create_subscription duplicate subscription endpoint.
    # Named with a Z to ensure that this test runs last.
    def test_z_create_subscription_duplicate(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                'body': {
                    "subscription-name": input_dict.get('subscription-name', self.default_subscription_name),
                    "endpoint": input_dict.get('endpoint', self.default_endpoint),
                    'protocol': input_dict.get('protocol', self.default_protocol),
                    'filter': input_dict.get('filter', json.loads(self.default_filter))
                }
            }

        response = create_subscription(request_dict)
        # test the status code 422
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Subscription for endpoint already exists: {}".format(request_dict['body']['endpoint']))


if __name__ == '__main__':
    unittest.main()
