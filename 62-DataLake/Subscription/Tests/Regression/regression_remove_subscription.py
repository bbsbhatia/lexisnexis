import os
import unittest

from lng_datalake_client.Subscription.remove_subscription import remove_subscription

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestRemoveSubscription(unittest.TestCase):
    default_subscription_name = os.getenv("SUBSCRIPTION_NAME", "subs_test_10")
    default_subscription_state = os.getenv("SUBSCRIPTION_STATE", "Terminating")
    default_subscription_id = int(os.getenv("SUBSCRIPTION_ID", 10))
    default_protocol = os.getenv("PROTOCOL", "email")
    default_endpoint = os.getenv("ENDPOINT", "srivaspm@legal.regn.net")

    # + Test the 202 status code and body
    def test_remove_subscription(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }
        response = remove_subscription(request_dict)

        # test the status code 202
        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn('context', response_dict)
        self.assertTrue(isinstance(response_dict['context'].get('request-id'), str))
        self.assertTrue(isinstance(response_dict['context'].get("api-id"), str))
        self.assertTrue(isinstance(response_dict['context'].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        # test the response body
        self.assertIn('subscription', response_dict)
        self.assertEqual(request_dict['subscription-id'], response_dict['subscription']['subscription-id'])
        self.assertEqual(self.default_subscription_state,
                         response_dict['subscription']['subscription-state'])

        return response_dict['subscription']

    # - remove_subscription with invalid subscription id --400
    def test_remove_subscription_invalid_subscription_id(self):

        request_dict = {
            "subscription-id": -1
        }

        response = remove_subscription(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchSubscription")
        self.assertEqual(json_error_resp['message'], "Invalid Subscription ID {}".format(-1))
        self.assertEqual(json_error_resp['corrective-action'], "Subscription ID does not exist")

    # - remove_subscription with pending subscription --422
    def test_remove_subscription_pending_subscription(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = remove_subscription(request_dict)

        # test the status code 422
        self.assertEqual(422, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Subscription ID {} is pending confirmation".format(request_dict['subscription-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Confirm subscription before removing")

    # - remove_subscription with invalid x-api-key -- 403
    def test_remove_subscription_invalid_x_api_key(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = \
            {
                "headers": {
                    'Content-type': 'application/json'
                },
                "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
            }

        response = remove_subscription(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - wrong x-api-key, auth error
    def test_remove_subscription_incorrect_x_api_key(self, input_dict=None, api_key=None, owner_id=None):
        if not api_key:
            api_key = "some-key"
        subscription_id = input_dict.get('subscription-id', self.default_subscription_id)
        request_dict = {
            'headers': {
                'Content-type': 'application/json',
                'x-api-key': api_key
            },
            "subscription-id": subscription_id
        }
        response = remove_subscription(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")

    # - remove_subscription with invalid content-type -- 415
    def test_remove_subscription_invalid_content_type(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "subscription-id": input_dict.get("subscription-id", self.default_subscription_id)
        }

        response = remove_subscription(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
