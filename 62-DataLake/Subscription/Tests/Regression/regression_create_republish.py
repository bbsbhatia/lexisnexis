import os
import unittest

from lng_datalake_client.Subscription.create_republish import create_republish
from lng_datalake_constants import subscription_status

__author__ = "John Morelock, Daniel Wang"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestCreateRepublish(unittest.TestCase):
    default_subscription_id = int(os.getenv("SUBSCRIPTION_ID", 172))
    default_collection_ids = list(os.getenv("COLLECTION_IDS", 'JohnK').split(','))
    default_catalog_ids = list(os.getenv("CATALOG_IDS", 'JohnK-Cat').split(','))
    default_changeset_id = os.getenv("CHANGESET_ID", 'jek-changeset-backup')
    default_owner_id = int(os.getenv('OWNER_ID', '1'))

    # + Test the 202 status code and body
    def test_create_republish(self, input_dict=None):
        if not input_dict:
            input_dict = {
                "catalog-ids": self.default_catalog_ids,
            }

        subscription_id = input_dict.get('subscription-id', self.default_subscription_id)
        request_dict = {
            "subscription-id": subscription_id,
            'body': {}
        }
        expected_response = {
            "subscription-id": subscription_id,
            "republish-state": "Pending"
        }
        for key in ('collection-ids', 'catalog-ids', 'changeset-id'):
            if key in input_dict:
                if key == 'changeset-id':
                    request_dict['body'][key] = input_dict[key]
                    expected_response[key] = input_dict[key]
                else:
                    request_dict['body'][key] = sorted(input_dict[key])
                    expected_response[key] = sorted(input_dict[key])

        response = create_republish(request_dict)

        self.assertEqual(202, response.status_code)

        response_dict = response.json()
        self.assertIn('context', response_dict)
        self.assertTrue(isinstance(response_dict['context'].get('request-id'), str))
        self.assertTrue(isinstance(response_dict['context'].get("api-id"), str))
        self.assertTrue(isinstance(response_dict['context'].get("resource-id"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn('republish', response_dict)
        republish_resp = response_dict['republish']

        self.assertIn('republish-id', republish_resp)
        self.assertTrue(isinstance(republish_resp['republish-id'], str))
        expected_response['republish-id'] = republish_resp['republish-id']

        self.assertIn('republish-timestamp', republish_resp)
        self.assertTrue(isinstance(republish_resp['republish-timestamp'], str))
        expected_response['republish-timestamp'] = republish_resp.get('republish-timestamp')

        self.assertIn('republish-expiration-date', republish_resp)
        self.assertTrue(isinstance(republish_resp['republish-expiration-date'], str))
        expected_response['republish-expiration-date'] = republish_resp.get('republish-expiration-date')

        # Assert one of collection-ids or catalog-ids or changeset-id in response and only one
        self.assertTrue("collection-ids" in republish_resp or
                        "catalog-ids" in republish_resp or
                        "changeset-id" in republish_resp)

        self.assertFalse("collection-ids" in republish_resp and "catalog-ids" in republish_resp)
        self.assertFalse("changeset-id" in republish_resp and "catalog-ids" in republish_resp)
        self.assertFalse("collection-ids" in republish_resp and "changeset-id" in republish_resp)

        if 'collection-ids' in input_dict:
            republish_resp['collection-ids'] = sorted(republish_resp['collection-ids'])
        if 'catalog-ids' in input_dict:
            republish_resp['catalog-ids'] = sorted(republish_resp['catalog-ids'])

        self.assertDictEqual(expected_response, republish_resp)
        return republish_resp

    # - create_subscription with invalid x-api-key -- 403
    def test_create_republish_invalid_x_api_key(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            "headers": {
                'Content-type': 'application/json'
            },
            'body': {
            }
        }
        for key in ('collection-ids', 'catalog-ids', 'changeset-id'):
            if key in input_dict:
                request_dict['body'][key] = input_dict[key]

        response = create_republish(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - create_republish with invalid content-type -- 415
    def test_create_republish_invalid_content_type(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        key = os.environ['X-API-KEY']
        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            'body': {
            }
        }
        for key in ('collection-ids', 'catalog-ids', 'changeset-id'):
            if key in input_dict:
                request_dict['body'][key] = input_dict[key]

        response = create_republish(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")

    # - create_republish - invalid subscription id -- 404
    def test_create_republish_invalid_subscription_id(self):

        subscription_id = -1
        request_dict = {
            "subscription-id": subscription_id,
            'body': {}
        }

        response = create_republish(request_dict)
        # test the status code 404
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchSubscription")
        self.assertEqual(json_error_resp['message'], "Subscription {} does not exist".format(subscription_id))
        self.assertEqual(json_error_resp['corrective-action'], "Create subscription before beginning republish")

    # - create_republish - invalid subscription state pending - 422
    def test_create_republish_subscription_pending(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        subscription_id = input_dict.get('subscription-id', self.default_subscription_id)
        request_dict = {
            "subscription-id": subscription_id,
            'body': {}
        }
        for key in ('collection-ids', 'catalog-ids', 'changeset-id'):
            if key in input_dict:
                request_dict['body'][key] = input_dict[key]

        response = create_republish(request_dict)
        # test the status code 422
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Subscription {} is in {} state".format(subscription_id, subscription_status.PENDING))
        self.assertEqual(json_error_resp['corrective-action'], "Confirm subscription before creating republish event")

    # - create_republish - no catalog or collection specified -- 400
    def test_create_republish_no_catalog_collection_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
        }
        response = create_republish(request_dict)
        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "No Catalog IDs or Collection IDs or Changeset ID to Republish")
        self.assertEqual(json_error_resp['corrective-action'], 'Request must only have one of the following:'
                                                               ' a list of Catalog IDs, a list of Collection IDs,'
                                                               ' or a Changeset ID')

    # - create_republish - catalog and collection specified -- 400
    def test_create_republish_catalog_collection_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            'body': {'collection-ids': input_dict.get('collection-ids', self.default_collection_ids),
                     'catalog-ids': input_dict.get('catalog-ids', self.default_catalog_ids)
                     }
        }
        response = create_republish(request_dict)
        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         'More than one of either Catalog IDs or Collection IDs or a Changeset ID submitted')
        self.assertEqual(json_error_resp['corrective-action'], 'Request must only have one of the following:'
                                                               ' a list of Catalog IDs, a list of Collection IDs,'
                                                               ' or a Changeset ID')

    # TODO add changeset tests to test_orchestration
    # - create_republish - catalog and changeset specified  -- 400
    def test_create_republish_catalog_changeset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            'body': {'catalog-ids': input_dict.get('catalog-ids', self.default_catalog_ids),
                     'changeset-id': input_dict.get('changeset-id', self.default_changeset_id)}
        }
        response = create_republish(request_dict)
        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         'More than one of either Catalog IDs or Collection IDs or a Changeset ID submitted')
        self.assertEqual(json_error_resp['corrective-action'], 'Request must only have one of the following:'
                                                               ' a list of Catalog IDs, a list of Collection IDs,'
                                                               ' or a Changeset ID')

    # - create_republish - collection and changeset specified  -- 400
    def test_create_republish_collection_changeset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            'body': {'collection-ids': input_dict.get('collection-ids', self.default_collection_ids),
                     'changeset-id': input_dict.get('changeset-id', self.default_changeset_id)}
        }
        response = create_republish(request_dict)
        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         'More than one of either Catalog IDs or Collection IDs or a Changeset ID submitted')
        self.assertEqual(json_error_resp['corrective-action'], 'Request must only have one of the following:'
                                                               ' a list of Catalog IDs, a list of Collection IDs,'
                                                               ' or a Changeset ID')

    # - create_republish - catalog and collection and changeset specified  -- 400
    def test_create_republish_catalog_collection_changeset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            'body': {'collection-ids': input_dict.get('collection-ids', self.default_collection_ids),
                     'catalog-ids': input_dict.get('catalog-ids', self.default_catalog_ids),
                     'changeset-id': input_dict.get('changeset-id', self.default_changeset_id)}
        }
        response = create_republish(request_dict)
        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         'More than one of either Catalog IDs or Collection IDs or a Changeset ID submitted')
        self.assertEqual(json_error_resp['corrective-action'], 'Request must only have one of the following:'
                                                               ' a list of Catalog IDs, a list of Collection IDs,'
                                                               ' or a Changeset ID')

    # - create_republish invalid collection-id -- 404 (single collection-id only)
    def test_create_republish_invalid_collection_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            "body": {
                "collection-ids": ['-1']
            }
        }
        response = create_republish(request_dict)
        # test the status code 404
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCollection")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Collection IDs {0}".format(set(request_dict['body']['collection-ids'])))
        self.assertEqual(json_error_resp['corrective-action'], "Collection ID(s) does not exist")

    # - create_republish invalid collection-id -- 404 (single catalog-id only)
    def test_create_republish_invalid_catalog_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            "body": {
                "catalog-ids": ['-1']
            }
        }
        response = create_republish(request_dict)
        # test the status code 404
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchCatalog")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Catalog IDs {0}".format(set(request_dict['body']['catalog-ids'])))
        self.assertEqual(json_error_resp['corrective-action'], "Catalog ID(s) does not exist")

    # TODO add changeset tests to test_orchestration
    # - create_republish invalid changeset-id -- 404 (single changeset-id only)
    def test_create_republish_invalid_changeset_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            "body": {
                "changeset-id": '-1'
            }
        }

        response = create_republish(request_dict)
        # test status code 404
        self.assertEqual(404, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchChangeset")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Changeset ID {0}".format(request_dict['body']['changeset-id']))
        self.assertEqual(json_error_resp['corrective-action'], "Changeset ID does not exist")

    # - create_republish changset open -- 422
    def test_create_republish_changeset_open(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            "body": {
                "changeset-id": input_dict.get('changeset-id', self.default_changeset_id)
            }
        }
        response = create_republish(request_dict)
        # test the status code 422
        self.assertEqual(422, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "SemanticError")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Changeset State: Opened")
        self.assertEqual(json_error_resp['corrective-action'], "Changeset must be in Closed State")

    # - wrong x-api-key, auth error
    def test_create_republish_incorrect_x_api_key(self, input_dict=None, api_key=None, owner_id=None):
        if not input_dict:
            input_dict = {}

        if not api_key:
            api_key = "some-key"
        subscription_id = input_dict.get('subscription-id', self.default_subscription_id)
        request_dict = {
            'headers': {
                'Content-type': 'application/json',
                'x-api-key': api_key
            },
            "subscription-id": subscription_id,
            "body": {
                "collection-ids": input_dict.get('collection-ids', self.default_collection_ids),
            }
        }
        response = create_republish(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NotAuthorizedError")
        self.assertEqual(json_error_resp['message'], "API Key provided is not valid for Owner ID {}".format(owner_id))
        self.assertEqual(json_error_resp['corrective-action'], "Use a valid API Key")


if __name__ == '__main__':
    unittest.main()
