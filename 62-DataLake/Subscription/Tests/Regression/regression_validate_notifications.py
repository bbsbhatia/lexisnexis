import logging
import os
import unittest

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


class TestValidateCollectionNotifications(unittest.TestCase):

    def test_validate_notification_v0(self, event_name: str, message: dict, collection_data: dict) -> None:
        error_message = "Attribute {} in message is not consistent with the value retrieved through GET Collection API"
        try:
            default_properties = {
                "event-name": event_name,
                "item-state": collection_data['collection-state']
            }
            if "object-expiration-hours" in collection_data:
                default_properties["object-expiration"] = str(collection_data["object-expiration-hours"]) + 'h'
            for prop in message:
                # When processing v0v1 messages we need to ignore the additional values in v1 that we validate there
                if prop == "v1":
                    continue
                if prop in default_properties:
                    self.assertEqual(message[prop], default_properties[prop], error_message.format(prop))
                else:
                    self.assertEqual(message[prop], collection_data[prop], error_message.format(prop))
        except KeyError as e:
            logger.error('Unable to validate collection data based on missing data. {} is missing in GET Collection '
                         'API response but sent in notification'.format(e))
            raise e

    def test_validate_notification_v1(self, event_name: str, message: dict, collection_data: dict,
                                      notification_type: str) -> None:
        for prop in ('SchemaVersion', 'Schema', 'Type', 'collection', 'context'):
            self.assertIn(prop, message)

        self.assertEqual(message['SchemaVersion'], 'v1')
        # Not testing the Schema value as this might become dynamic and require additional information to test
        self.assertTrue(isinstance(message["Schema"], str))
        self.assertEqual(message['Type'], notification_type)
        self.assertTrue(isinstance(message["collection"], dict))
        self.assertTrue(isinstance(message["context"], dict))

        # TODO: check that the notification context values match the request
        msg_context = message["context"]
        for prop in ('request-time', 'request-id'):
            self.assertIn(prop, msg_context)
        self.assertTrue(isinstance(msg_context["request-time"], str))
        self.assertTrue(isinstance(msg_context["request-id"], str))

        msg_collection_data = message['collection']
        error_message = "Attribute {} in message is not consistent with the value retrieved through GET Collection API"
        try:
            default_properties = {
                "event-name": event_name,
            }
            for prop in msg_collection_data:
                if prop in default_properties:
                    self.assertEqual(msg_collection_data[prop], default_properties[prop], error_message.format(prop))
                else:
                    self.assertEqual(msg_collection_data[prop], collection_data[prop], error_message.format(prop))
        except KeyError as e:
            logger.error('Unable to validate collection data based on missing data. {} is missing in GET Collection '
                         'API response but sent in notification'.format(e))
            raise e


class TestValidateObjectNotifications(unittest.TestCase):
    renamed_v0_only_properties = ('bucket-name', 'object-key', 'pending-expiration-epoch')
    extra_remove_object_v0_notification_props = ('content-type', 'raw-content-length', 'replicated-buckets',
                                                 'version-number', 'version-timestamp')
    extra_remove_object_v1_notification_props = ('version-number', 'version-timestamp', 'object-expiration-date')
    describe_only_props = ('asset-id', 'collection-url', 'owner-id')

    def test_validate_notification_v0(self, event_name: str, message: dict, object_data: dict) -> None:
        error_message = "Attribute {} in message is not consistent with the value retrieved through GET Object API"
        try:
            default_properties = {
                "event-name": event_name,
                "item-state": object_data['object-state']
            }
            for prop in message:
                # When processing v0v1 messages we need to ignore the additional values in v1 that we validate there
                if prop == "v1":
                    continue
                if prop in default_properties:
                    self.assertEqual(message[prop], default_properties[prop], error_message.format(prop))
                # there are props in the notification not present in the describe
                # response ('bucket-name', 'object-key'...) as well as properties that show up in the
                # notification in a remove that do not appear in describe ('content-type', 'raw-content-length'...)
                elif (event_name == 'Object::Remove' and prop in self.extra_remove_object_v0_notification_props) or \
                        prop in self.renamed_v0_only_properties:
                    self.validate_object_notification_only_properties(prop, message[prop])
                elif prop not in self.describe_only_props:
                    self.assertEqual(message[prop], object_data[prop], error_message.format(prop))

        except KeyError as e:
            logger.error('Unable to validate object data based on missing data. {} is missing in Describe Objects '
                         'API response but sent in notification'.format(e))
            raise e

    def test_validate_notification_v1(self, event_name: str, message: dict, object_data: dict,
                                      notification_type: str) -> None:
        for prop in ('SchemaVersion', 'Schema', 'Type', 'context', 'object'):
            self.assertIn(prop, message)

        self.assertEqual(message['SchemaVersion'], 'v1')
        # Not testing the Schema value as this might become dynamic and require additional information to test
        self.assertTrue(isinstance(message["Schema"], str))
        self.assertEqual(message['Type'], notification_type)
        self.assertTrue(isinstance(message["object"], dict))
        self.assertTrue(isinstance(message["context"], dict))

        # TODO: check that the notification context values match the request
        msg_context = message["context"]
        for prop in ('request-time', 'request-id'):
            self.assertIn(prop, msg_context)
        self.assertTrue(isinstance(msg_context["request-time"], str))
        self.assertTrue(isinstance(msg_context["request-id"], str))

        msg_object_data = message['object']
        error_message = "Attribute {} in message is not consistent with the value retrieved through GET Object API"
        try:
            default_properties = {
                "event-name": event_name,
            }
            for prop in msg_object_data:
                if prop in default_properties:
                    self.assertEqual(msg_object_data[prop], default_properties[prop], error_message.format(prop))
                # Some values not available in Describe Objects, checking the value's type in the message
                elif event_name == 'Object::Remove' and prop in self.extra_remove_object_v1_notification_props:
                    self.validate_object_notification_only_properties(prop, msg_object_data[prop])
                # asset-id, collection-url, and owner-id are present in the describe response but not present in the
                # notification
                elif prop not in self.describe_only_props:
                    self.assertEqual(msg_object_data[prop], object_data[prop], error_message.format(prop))
        except KeyError as e:
            logger.error('Unable to validate collection data based on missing data. {} is missing in Describe Objects '
                         'API response but sent in notification'.format(e))
            raise e

    def validate_object_notification_only_properties(self, key: str, value) -> None:
        """
            Validates the values for attributes present in a message but that do not appear in
            Describe Objects
            :param key: the attribute name as a string
            :param value: the value of the attribute which could be of many different types
        """
        if key == 'version-number':
            self.assertTrue(isinstance(value, int))
            return
        if key == 'version-timestamp':
            self.assertTrue(isinstance(value, str))
            return
        if key == 'object-expiration-date':
            self.assertTrue(isinstance(value, str))
            return
        if key == 'bucket-name':
            self.assertTrue(isinstance(value, str))
            return
        if key == 'object-key':
            self.assertTrue(isinstance(value, str))
            return
        if key == 'content-type':
            self.assertTrue(isinstance(value, str))
            return
        if key == 'raw-content-length':
            self.assertTrue(isinstance(value, int))
            return
        if key == 'replicated-buckets':
            self.assertTrue(isinstance(value, list))
            return
        if key == 'pending-expiration-epoch':
            self.assertTrue(isinstance(value, int))
            return
        self.assertTrue(False, 'Unexpected value in remove object notification properties {}'.format(key))
