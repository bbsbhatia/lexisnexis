import os
import unittest

from lng_datalake_client.Subscription.get_republish import get_republish

__author__ = "John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.1"


class TestGetRepublish(unittest.TestCase):
    default_subscription_id = os.getenv("SUBSCRIPTION_ID", 341)
    default_subscription_not_in_republish = os.getenv("SUBSCRIPTION_ID", 302)
    default_republish_id = os.getenv("REPUBLISH_ID", "1a317503-b3b2-11e9-a696-ff565b20aa94")
    default_unknown_republish_id = os.getenv("REPUBLISH_ID", "unknown-b3b2-11e9-a696-ff565b20aa94")
    default_collection_ids = list(os.getenv("COLLECTION_IDS", ['1027']))
    default_republish_state = os.getenv("REPUBLISH_STATE", "Complete")

    # + Test the 202 status code and republish properties for GET_REPUBLISH
    def test_get_republish(self, input_dict=None):
        if not input_dict:
            input_dict = {}
        subscription_id = input_dict.get('subscription-id', self.default_subscription_id)
        republish_id = input_dict.get('republish-id', self.default_republish_id)
        stage = os.environ['STAGE']

        request_dict = \
            {
                "subscription-id": subscription_id,
                "republish-id": republish_id
            }

        expected_response = \
            {
                "republish-id": republish_id,
                "subscription-id": subscription_id,
                "collection-ids": input_dict.get('collection-ids', self.default_collection_ids),
                "republish-state": input_dict.get('republish-state', self.default_republish_state)
            }
        # Add any additional attributes to validate against the collection
        for key, value in input_dict.items():
            if key not in expected_response:
                expected_response[key] = value

        response = get_republish(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertEqual(stage, response_dict['context']['stage'])

        self.assertIn('republish', response_dict)
        republish_resp = response_dict['republish']

        self.assertIn('republish-timestamp', republish_resp)
        self.assertTrue(isinstance(republish_resp['republish-timestamp'], str))
        expected_response['republish-timestamp'] = republish_resp.get('republish-timestamp')

        self.assertIn('republish-expiration-date', republish_resp)
        self.assertTrue(isinstance(republish_resp['republish-expiration-date'], str))
        expected_response['republish-expiration-date'] = republish_resp.get('republish-expiration-date')

        # Assert one of collection-ids or catalog-ids in response but not both
        self.assertTrue("collection-ids" in republish_resp or "catalog-ids" in republish_resp)
        self.assertFalse("collection-ids" in republish_resp and "catalog-ids" in republish_resp)

        # test the response body
        self.assertDictEqual(expected_response, republish_resp)
        return republish_resp

    # - get_republish with invalid x-api-key -- 403
    def test_get_republish_invalid_x_api_key(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            "republish-id": input_dict.get('republish-id', self.default_republish_id),
            "headers": {
                'Content-type': 'application/json'
            },
            'body': {
            }
        }
        for key in ('collection-ids', 'catalog-ids'):
            if key in input_dict:
                request_dict['body'][key] = input_dict[key]

        response = get_republish(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - get_republish with unknown republish-id -- 404
    def test_get_republish_no_such_republish(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        republish_id = input_dict.get('republish-id', self.default_unknown_republish_id)
        request_dict = {
            "subscription-id": input_dict.get('subscription-id', self.default_subscription_id),
            "republish-id": republish_id,
            "body": {}
        }
        for key in ('collection-ids', 'catalog-ids'):
            if key in input_dict:
                request_dict['body'][key] = input_dict[key]

        response = get_republish(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchRepublish")
        self.assertEqual(json_error_resp['message'], "Republish ID {} does not exist".format(republish_id))
        self.assertEqual(json_error_resp['corrective-action'],
                         "A valid Republish ID is required. Republish IDs expire after 30 days")

    # - get_republish no such republish -- 400
    def test_get_republish_invalid_subscription_id(self, input_dict=None):
        if not input_dict:
            input_dict = {}

        subscription_id = int(input_dict.get('subscription-id', self.default_subscription_not_in_republish))
        republish_id = input_dict.get('republish-id', self.default_republish_id)
        request_dict = {
            "subscription-id": subscription_id,
            "republish-id": republish_id,
            "body": {}
        }
        for key in ('collection-ids', 'catalog-ids'):
            if key in input_dict:
                request_dict['body'][key] = input_dict[key]

        response = get_republish(request_dict)

        # test the status code 404
        self.assertEqual(404, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "NoSuchSubscription")
        self.assertEqual(json_error_resp['message'],
                         "Invalid Subscription ID {}".format(subscription_id))
        self.assertRegex(json_error_resp['corrective-action'],
                         "Subscription ID does not exist")


if __name__ == '__main__':
    unittest.main()
