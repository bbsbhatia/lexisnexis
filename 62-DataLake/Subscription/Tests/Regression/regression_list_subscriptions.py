import os
import unittest

from lng_datalake_client.Subscription.list_subscriptions import list_subscriptions
from lng_datalake_testhelper.io_utils import IOUtils

__author__ = "Brandon Sersion"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__)


class TestListSubscriptions(unittest.TestCase):

    # + Test the 200 status code and subscription properties for list subscriptions without an owner-id
    def test_list_subscriptions_all_results(self):
        response = list_subscriptions({})

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("subscriptions", response_dict)

        # test the response body
        self.assertGreater(len(response_dict["subscriptions"]), 0)
        self.assertTrue(isinstance(response_dict["item-count"], int))
        return response.json()["subscriptions"]

    # + Test the 200 status code and subscription properties for list subscriptions with an owner-id
    def test_list_subscriptions_by_owner_id(self, list_subscriptions_data=io_util.load_data_json(
        "list_subscriptions_data.json"),
                                            owner_id=None):
        if owner_id is None:
            owner_id = int(os.environ['OWNER-ID'])
        response = list_subscriptions({"owner-id": owner_id})

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("subscriptions", response_dict)

        response_subscriptions = response_dict["subscriptions"]
        response_subscriptions.sort(key=lambda x: x['subscription-id'])
        self.assertEqual(list_subscriptions_data, response_subscriptions)
        self.assertTrue(isinstance(response_dict["item-count"], int))

        return response.json()["subscriptions"]

    # + Test list subscriptions with pagination
    def test_list_subscriptions_pagination(self):
        request_input = {"max-items": 1}
        response_1 = list_subscriptions(request_input)
        self.assertEqual(200, response_1.status_code)
        response_dict_1 = response_1.json()
        self.assertIn("context", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict_1["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ["STAGE"],
                         response_dict_1["context"]["stage"])
        self.assertIn("subscriptions", response_dict_1)
        self.assertTrue(isinstance(response_dict_1["item-count"], int))
        self.assertEqual(request_input["max-items"], len(response_dict_1['subscriptions']))

        self.assertTrue(isinstance(response_dict_1["next-token"], str))

        # make a request again with the pagination token ------
        request_input["next-token"] = response_dict_1['next-token']
        response_2 = list_subscriptions(request_input)
        self.assertEqual(200, response_2.status_code)
        response_dict_2 = response_2.json()
        self.assertIn("context", response_dict_2)
        self.assertTrue(isinstance(response_dict_2["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict_2["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ["STAGE"],
                         response_dict_2["context"]["stage"])
        self.assertIn("subscriptions", response_dict_2)
        self.assertIn("item-count", response_dict_2)
        item_count = response_dict_2["item-count"]
        self.assertTrue(isinstance(item_count, int))
        if item_count > 0:
            self.assertEqual(request_input["max-items"], len(response_dict_2['subscriptions']))
            self.assertNotEqual(response_dict_1["subscriptions"], response_dict_2["subscriptions"])

    # + list_subscriptions with invalid owner id
    def test_list_subscriptions_invalid_owner_id(self):
        request_dict = {
            "owner-id": -1
        }
        response = list_subscriptions(request_dict)

        # test the status code 200
        self.assertEqual(200, response.status_code)

        response_dict = response.json()
        self.assertIn("context", response_dict)
        self.assertTrue(isinstance(response_dict["context"].get("request-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("api-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("resource-id"), str))
        self.assertTrue(isinstance(response_dict["context"].get("request-time-epoch"), str))
        self.assertEqual(os.environ['STAGE'], response_dict['context']['stage'])

        self.assertIn("subscriptions", response_dict)

        # test the response body
        self.assertEqual(len(response_dict["subscriptions"]), 0)
        self.assertEqual(response_dict["item-count"], 0)
        return response.json()["subscriptions"]

    # - list_subscriptions with invalid max items value
    def test_list_subscriptions_invalid_max_items(self):
        response = list_subscriptions({"max-items": 1001})
        self.assertEqual(400, response.status_code)
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'], "Invalid max-items value: 1001")
        self.assertEqual(json_error_resp['corrective-action'], "Value must be between 1 and 1000")

    # - list_subscriptions changing max items
    def test_list_subscriptions_changing_max_items(self):
        response = list_subscriptions({'max-items': 1})
        # Make sure the initial response was successful and has a next-token
        self.assertEqual(200, response.status_code)
        self.assertIn('next-token', response.json())
        # Change the max-items amount
        response = list_subscriptions({'max-items': 2, 'next-token': response.json()['next-token']})

        # test the status code 400
        self.assertEqual(400, response.status_code)
        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "InvalidRequestPropertyValue")
        self.assertEqual(json_error_resp['message'],
                         "Continuation request must pass the same max-items as initial request")
        self.assertEqual(json_error_resp['corrective-action'], "Set max-items to 1 and try again")

    # - list_subscriptions with invalid x-api-key
    def test_list_subscriptions_invalid_x_api_key(self):
        request_dict = {
            "headers": {
                'Content-type': 'application/json'
            },
            "owner-id": 2
        }

        response = list_subscriptions(request_dict)

        # test the status code 403
        self.assertEqual(403, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "INVALID_API_KEY")
        self.assertEqual(json_error_resp['message'], "Forbidden")

    # - list_subscriptions with invalid content-type
    def test_list_subscriptions_invalid_content_type(self):
        key = os.environ['X-API-KEY']
        request_dict = {
            "headers": {
                'Content-type': 'application/atom+xml',
                'x-api-key': key
            },
            "subscription-id": "subscription-01"
        }
        response = list_subscriptions(request_dict)

        # test the status code 415
        self.assertEqual(415, response.status_code)

        # test the response message
        json_error_resp = response.json()['error']
        self.assertEqual(json_error_resp['type'], "UNSUPPORTED_MEDIA_TYPE")
        self.assertEqual(json_error_resp['message'], "Unsupported Media Type")


if __name__ == '__main__':
    unittest.main()
