import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_republish_command import lambda_handler

__author__ = "John Konderla, Daniel Wang"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateRepublishCommand')

table_dict = {
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
    'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_table',
    'CHANGESET_DYNAMODB_TABLE': 'fake_changeset_table'
}


class CreateRepublishCommandIT(unittest.TestCase):

    # + create_republish_command: collection ids
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_republish_command_collections(self,
                                               session_mock, mock_datetime_now,
                                               mock_dynamodb_get_client
                                               ):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamo.get_item_owner_data_valid.json')
            elif TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamo.subscription_data_valid.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.batch_get_item.return_value = io_util.load_data_json(
            'dynamo.batch_get_items.collection_data_valid.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        request_input = io_util.load_data_json('apigateway.input_valid_collections.json')
        expected_response_output = io_util.load_data_json('apigateway.response_valid_collections.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mock_datetime_now.return_value = 15161611
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            response = lambda_handler(request_input, MockLambdaContext())
            response['response']['collection-ids'].sort()
            self.assertDictEqual(response, expected_response_output)

    # + create_republish_command: changeset id
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_republish_command_changeset(self,
                                                session_mock, mock_datetime_now,
                                                mock_dynamodb_get_client):
        def fake_get_item(TableName, Key): # NOSONAR
            if TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamo.get_item_owner_data_valid.json')
            elif TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamo.subscription_data_valid.json')
            elif TableName == 'fake_changeset_table':
                return io_util.load_data_json('dynamo.get_item.changeset_valid.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')

        request_input = io_util.load_data_json('apigateway.input_valid_changeset.json')
        expected_response_output = io_util.load_data_json('apigateway.response_valid_changeset.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mock_datetime_now.return_value = 15161611
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            response = lambda_handler(request_input, MockLambdaContext())
            self.assertDictEqual(response, expected_response_output)


if __name__ == '__main__':
    unittest.main()
