import os
import unittest
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from update_subscription_event_handler import lambda_handler

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateSubscriptionEventHandler')


class TestUpdateSubscriptionEventHandler(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda_handler -  success - Update name, filter and schema version
    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table'})
    @patch('lng_datalake_testhelper.mock_lambda_context.MockLambdaContext')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self, mock_session,
                            mock_sns_client,
                            mock_dynamodb_client,
                            mock_lambda_context):
        mock_session.return_value = None

        mock_dynamodb_client.return_value.get_item.return_value = io_util.load_data_json(
            "dynamodb.subscription.get_item.json")
        mock_dynamodb_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.put_item.response.json')
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('lng.dynamodb.tracking_table.respond.json')
        mock_sns_client.return_value.unsubscribe.return_value = None
        mock_dynamodb_client.return_value.delete_item.return_value = None
        request_input = io_util.load_data_json("valid_event.json")

        self.assertEqual(event_handler_status.SUCCESS, lambda_handler(request_input, MockLambdaContext()))

    # + lambda_handler -  success - Update name only
    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table'})
    @patch('lng_datalake_testhelper.mock_lambda_context.MockLambdaContext')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler(self, mock_session,
                            mock_dynamodb_client,
                            mock_lambda_context):
        mock_session.return_value = None

        mock_dynamodb_client.return_value.get_item.return_value = io_util.load_data_json(
            "dynamodb.subscription.get_item.json")
        mock_dynamodb_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.put_item.response.json')
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('lng.dynamodb.tracking_table.respond.json')
        mock_dynamodb_client.return_value.delete_item.return_value = None
        request_input = io_util.load_data_json("valid_event_2.json")

        self.assertEqual(event_handler_status.SUCCESS, lambda_handler(request_input, MockLambdaContext()))


if __name__ == '__main__':
    unittest.main()
