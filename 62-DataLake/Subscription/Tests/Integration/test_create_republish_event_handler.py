import unittest
from datetime import datetime
from unittest.mock import patch

from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_republish_event_handler import lambda_handler

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateRepublishEventHandler')


class CreateRepublishEventHandlerIT(unittest.TestCase):

    # + create_republish_event_handler: catalogs
    @patch('lng_aws_clients.stepfunctions.get_client')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_create_republish_event_handler_catalogs(self, mock_dynamodb_client, mock_session,
                                                     mock_stepfunction_client):
        mock_session.return_value = None
        valid_catalog_event_message = io_util.load_data_json('valid_catalog_event_message.json')
        mock_dynamodb_client.return_value.put_item.return_value = io_util.load_data_json('dynamo.put_response.json')
        mock_dynamodb_client.return_value.batch_write_item.return_value = \
            io_util.load_data_json('dynamodb_batch_write_response.json')
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('lng.dynamodb.cat_coll_map_table.respond.json')

        mock_stepfunction_client.return_value.start_execution.return_value = {
            'executionArn': '123',
            'startDate': datetime(2015, 1, 1)
        }

        self.assertEqual(lambda_handler(valid_catalog_event_message, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_stepfunction_client.return_value.start_execution.assert_called_with(
            input='{"republish-id":"TkJQHpKFNZQRxfFF","num-collections":3,"event-message":"{\\"republish-id\\":\\"TkJQHpKFNZQRxfFF\\",\\"table-name\\":null,\\"index-name\\":\\"objectstore-collection-hash-and-state-index\\",\\"catalog-collection-mapping-table\\":null,\\"republish-collection-table\\":null,\\"subscription-notification-topic-arn\\":null,\\"datalake-url\\":null,\\"schema-version\\":\\"v0\\",\\"request-time\\":\\"2019-07-03T19:15:41.621Z\\"}"}',
            name='republish-job-TkJQHpKFNZQRxfFF', stateMachineArn=None)

    # + create_republish_event_handler: collections
    @patch('lng_aws_clients.stepfunctions.get_client')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_create_republish_event_handler_collections(self, mock_dynamodb_client, mock_session,
                                                        mock_stepfunction_client):
        mock_session.return_value = None
        valid_catalog_event_message = io_util.load_data_json('valid_collections_event_message.json')
        mock_dynamodb_client.return_value.put_item.return_value = io_util.load_data_json('dynamo.put_response.json')
        mock_dynamodb_client.return_value.batch_write_item.return_value = \
            io_util.load_data_json('dynamodb_batch_write_response.json')

        mock_stepfunction_client.return_value.start_execution.return_value = {
            'executionArn': '123',
            'startDate': datetime(2015, 1, 1)
        }

        self.assertEqual(lambda_handler(valid_catalog_event_message, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_stepfunction_client.return_value.start_execution.assert_called_with(
            input='{"republish-id":"TkJQHpKFNZQRxfFF","num-collections":2,"event-message":"{\\"republish-id\\":\\"TkJQHpKFNZQRxfFF\\",\\"table-name\\":null,\\"index-name\\":\\"objectstore-collection-hash-and-state-index\\",\\"catalog-collection-mapping-table\\":null,\\"republish-collection-table\\":null,\\"subscription-notification-topic-arn\\":null,\\"datalake-url\\":null,\\"schema-version\\":\\"v0\\",\\"request-time\\":\\"2019-07-03T19:15:41.621Z\\"}"}',
            name='republish-job-TkJQHpKFNZQRxfFF', stateMachineArn=None)

    # + create_republish_event_handler: retry catalogs
    @patch('lng_aws_clients.stepfunctions.get_client')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_create_republish_event_handler_retry_catalogs(self, mock_dynamodb_client, mock_session,
                                                           mock_stepfunction_client):
        valid_catalog_event_message = io_util.load_data_json('valid_retry_catalog_event_message.json')
        mock_session.return_value = None

        mock_dynamodb_client.return_value.update_item.return_value = io_util.load_data_json('dynamo.put_response.json')

        mock_dynamodb_client.return_value.batch_write_item.return_value = \
            io_util.load_data_json('dynamodb_batch_write_response.json')
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('lng.dynamodb.cat_coll_map_table.respond.json')
        mock_stepfunction_client.return_value.start_execution.return_value = {
            'executionArn': '123',
            'startDate': datetime(2015, 1, 1)
        }
        self.assertEqual(lambda_handler(valid_catalog_event_message, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_stepfunction_client.return_value.start_execution.assert_called_with(
            input='{"republish-id":"TkJQHpKFNZQRxfFF","num-collections":23,"event-message":"{\\"republish-id\\":\\"TkJQHpKFNZQRxfFF\\",\\"table-name\\":null,\\"index-name\\":\\"objectstore-collection-hash-and-state-index\\",\\"catalog-collection-mapping-table\\":null,\\"republish-collection-table\\":null,\\"subscription-notification-topic-arn\\":null,\\"datalake-url\\":null,\\"schema-version\\":\\"v0\\",\\"request-time\\":\\"2019-07-03T19:15:41.621Z\\"}"}',
            name='republish-job-TkJQHpKFNZQRxfFF', stateMachineArn=None)

    # + create_republish_event_handler: No collections in the catalog
    @patch('lng_datalake_commons.time_helper.get_current_timestamp')
    @patch('lng_aws_clients.stepfunctions.get_client')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_create_republish_event_handler_no_collections(self, mock_dynamodb_client, mock_session,
                                                           mock_stepfunction_client, mock_timestamp):
        valid_catalog_event_message = io_util.load_data_json('valid_catalog_event_message.json')
        mock_session.return_value = None

        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json(
            'lng.dynamodb.cat_coll_map_table_no_items.respond.json')
        mock_stepfunction_client.return_value.start_execution.return_value = {
            'executionArn': '123',
            'startDate': datetime(2015, 1, 1)
        }
        mock_timestamp.return_value = "2019-04-10T12:00:00.000Z"
        self.assertEqual(lambda_handler(valid_catalog_event_message, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_stepfunction_client.return_value.start_execution.assert_not_called()
        mock_dynamodb_client.return_value.put_item.assert_called_with(
            ConditionExpression='attribute_exists(RepublishID) AND RepublishState=:state',
            ExpressionAttributeValues={':state': {'S': 'Pending'}},
            Item={'RepublishID': {'S': 'TkJQHpKFNZQRxfFF'}, 'SubscriptionID': {'N': '128'},
                  'RepublishState': {'S': 'Completed'}, 'RepublishTimestamp': {'S': '2019-07-03T19:15:41.621Z'},
                  'CatalogIds': {'L': [{'S': 'cat-1'}]}, 'EventIds': {'L': [{'S': 'TkJQHpKFNZQRxfFF'}]},
                  'PendingExpirationEpoch': {'N': '1564773342'}, 'dl:LastUpdated': {'S': '2019-04-10T12:00:00.000Z'}},
            TableName=None
        )

    # + create_republish_event_handler: changeset-id
    @patch('lng_aws_clients.stepfunctions.get_client')
    @patch('lng_aws_clients.session.set_session')
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_create_republish_event_handler_changeset_id(self, mock_dynamodb_client, mock_session,
                                                         mock_stepfunction_client):
        valid_catalog_event_message = io_util.load_data_json('valid_changeset_event_message.json')
        mock_session.return_value = None

        mock_dynamodb_client.return_value.put_item.return_value = io_util.load_data_json('dynamo.put_response.json')

        mock_dynamodb_client.return_value.batch_write_item.return_value = \
            io_util.load_data_json('dynamodb_batch_write_response.json')
        mock_dynamodb_client.return_value.get_paginator.return_value.paginate.return_value. \
            build_full_result.return_value = io_util.load_data_json('lng.dynamodb.changeset_coll_table.respond.json')
        mock_stepfunction_client.return_value.start_execution.return_value = {
            'executionArn': '123',
            'startDate': datetime(2015, 1, 1)
        }
        self.assertEqual(lambda_handler(valid_catalog_event_message, MockLambdaContext()),
                         event_handler_status.SUCCESS)
        mock_stepfunction_client.return_value.start_execution.assert_called_with(
            input='{"republish-id":"TkJQHpKFNZQRxfFF","num-collections":3,"event-message":"{\\"republish-id\\":\\"TkJQHpKFNZQRxfFF\\",\\"table-name\\":null,\\"index-name\\":\\"changeset-collection-hash-index\\",\\"catalog-collection-mapping-table\\":null,\\"republish-collection-table\\":null,\\"subscription-notification-topic-arn\\":null,\\"datalake-url\\":null,\\"schema-version\\":\\"v0\\",\\"request-time\\":\\"2019-07-03T19:15:41.621Z\\"}"}',
            name='republish-job-TkJQHpKFNZQRxfFF', stateMachineArn=None)


if __name__ == '__main__':
    unittest.main()
