import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import remove_subscription_command

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'RemoveSubscriptionCommand')


class TestRemoveSubscriptionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    @patch.dict(os.environ, {'EVENT_STORE_DYNAMODB_TABLE': 'fake_event_table',
                             'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table'},)
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success(self,
                                    mock_session,
                                    mock_dynamo_client):

        # Fake function for mocking multiple get_item calls to different table
        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamodb.subscription.get_item.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner.get_item.json')
            else:
                return None

        reload(remove_subscription_command)
        mock_session.return_value = None
        mock_dynamo_client.return_value.get_item = fake_get_item
        mock_dynamo_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamodb.put_item.response.json')
        request_input = io_util.load_data_json('apigateway.request_valid.json')
        response_output = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(remove_subscription_command.lambda_handler(request_input,
                                                                            mock_lambda_context.MockLambdaContext()),
                                 response_output)


if __name__ == '__main__':
    unittest.main()