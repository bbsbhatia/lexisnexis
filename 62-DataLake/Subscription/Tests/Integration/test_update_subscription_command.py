import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper, command_validator
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import update_subscription_command

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'UpdateSubscriptionCommand')


class TestUpdateSubscriptionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + lambda_handler -update name, filter, schema version
    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success(self,
                                    mock_session,
                                    mock_dynamodb_get_client):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamodb.subscription.get_item.json')
            elif TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamo.collection_get_item.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner.get_item.json')
            else:
                return None

        reload(update_subscription_command)
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.request_valid.json')
        response_output = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(update_subscription_command.lambda_handler(request_input,
                                                                            mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + lambda_handler -update name only
    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success_2(self,
                                      mock_session,
                                      mock_dynamodb_get_client):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamodb.subscription.get_item.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner.get_item.json')
            else:
                return None

        reload(update_subscription_command)
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.request_valid_2.json')
        response_output = io_util.load_data_json('apigateway.response_valid_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(update_subscription_command.lambda_handler(request_input,
                                                                            mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + lambda_handler - update filter for sqs subscription
    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
                             'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success_3(self,
                                      mock_session,
                                      mock_dynamodb_get_client):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamodb.subscription.get_item_2.json')
            elif TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamo.collection_get_item.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner.get_item.json')
            else:
                return None

        reload(update_subscription_command)
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.request_valid_3.json')
        response_output = io_util.load_data_json('apigateway.response_valid_3.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(update_subscription_command.lambda_handler(request_input,
                                                                            mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + lambda_handler - update schema version only
    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
                             'OWNER_DYNAMODB_TABLE': 'fake_owner_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success_4(self,
                                      mock_session,
                                      mock_dynamodb_get_client):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamodb.subscription.get_item.json')
            elif TableName == 'fake_owner_table':
                return io_util.load_data_json('dynamodb.owner.get_item.json')
            else:
                return None

        reload(update_subscription_command)
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.request_valid_4.json')
        response_output = io_util.load_data_json('apigateway.response_valid_4.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        command_validator.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(update_subscription_command.lambda_handler(request_input,
                                                                            mock_lambda_context.MockLambdaContext()),
                                 response_output)


if __name__ == '__main__':
    unittest.main()
