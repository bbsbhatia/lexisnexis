import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from get_republish_status import lambda_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetRepublishStatus')

table_dict = {
    'REPUBLISH_DYNAMODB_TABLE': 'fake_republish_table',
    'REPUBLISH_COLLECTION_DYNAMODB_TABLE': 'fake_republish_collection_table'
}


class RepublishStatusTest(unittest.TestCase):

    # + lambda handler
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_1(self,
                              session_mock,
                              mock_dynamodb_get_client
                              ):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.republish_get_all_items.json'),
            io_util.load_data_json('dynamo.republish_collection_count_items.json')]

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_event = {
            "republish-id": "1234-1234567890-1234-123456790"
        }
        expected_response_output = {'Status': 'Ready'}
        with patch('get_republish_status.glue_job_max_concurrency', 10):
            self.assertDictEqual(lambda_handler(input_event, MockLambdaContext()), expected_response_output)

    # + lambda handler - waiting because of not oldest pending rebulish id
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_2(self,
                              session_mock,
                              mock_dynamodb_get_client
                              ):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.republish_get_all_items.json'),
            io_util.load_data_json('dynamo.republish_collection_count_items.json')]

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_event = {
            "republish-id": "1234"
        }
        expected_response_output = {'Status': 'Waiting'}
        with patch('get_republish_status.glue_job_max_concurrency', 10):
            self.assertDictEqual(lambda_handler(input_event, MockLambdaContext()), expected_response_output)

    # + lambda handler - waiting because of collections_in_process = max_concurrency
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_3(self,
                              session_mock,
                              mock_dynamodb_get_client
                              ):
        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.republish_get_all_items.json'),
            io_util.load_data_json('dynamo.republish_collection_count_items.json')]

        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        input_event = {
            "republish-id": "1234-1234567890-1234-123456790"
        }
        expected_response_output = {'Status': 'Waiting'}
        with patch('get_republish_status.glue_job_max_concurrency', 2):
            self.assertDictEqual(lambda_handler(input_event, MockLambdaContext()), expected_response_output)


if __name__ == '__main__':
    unittest.main()
