import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import get_subscription_command

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetSubscriptionCommand')


class TestGetSubscriptionCommand(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table'})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success(self,
                                    mock_session,
                                    mock_dynamo_client):
        reload(get_subscription_command)
        mock_session.return_value = None
        mock_dynamo_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.subscription.get_item.json')
        request_input = io_util.load_data_json('apigateway.request_valid.json')
        response_output = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(get_subscription_command.lambda_handler(request_input,
                                                                         mock_lambda_context.MockLambdaContext()),
                                 response_output)


if __name__ == '__main__':
    unittest.main()
