import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper import mock_lambda_context
from lng_datalake_testhelper.io_utils import IOUtils

import list_subscriptions_command

__author__ = "Prashant S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'ListSubscriptionsCommand')


class TestListSubscriptions(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        TableCache.clear()

    # + test_lambda_handler: With owner-id, no pagination or max items
    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
                             "OWNER_DYNAMODB_TABLE": "fake_owner_table"})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success_without_pag(self,
                                                mock_session,
                                                mock_dynamo_client):
        reload(list_subscriptions_command)
        mock_session.return_value = None
        mock_dynamo_client.return_value.get_item.return_value = io_util.load_data_json(
            'dynamodb.owner.get_item.json')
        mock_dynamo_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.subscription.query_items.json')
        request_input = io_util.load_data_json('apigateway.request_valid.json')
        response_output = io_util.load_data_json('apigateway.response_valid.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.maxDiff = None
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(list_subscriptions_command.lambda_handler(request_input,
                                                                      mock_lambda_context.MockLambdaContext()),
                                 response_output)

    # + test_lambda_handler: With pagination and max items
    @patch.dict(os.environ, {'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
                             "OWNER_DYNAMODB_TABLE": "fake_owner_table"})
    @patch("lng_aws_clients.dynamodb.get_client")
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_success_with_pag(self,
                                             mock_session,
                                             mock_dynamo_client):
        reload(list_subscriptions_command)
        mock_session.return_value = None
        mock_dynamo_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result. \
            return_value = io_util.load_data_json('dynamodb.subscription.scan_with_page.json')
        request_input = io_util.load_data_json('apigateway.request_valid_with_page.json')
        response_output = io_util.load_data_json('apigateway.response_valid_with_page.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        self.maxDiff = None
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            self.assertDictEqual(list_subscriptions_command.lambda_handler(request_input,
                                                                      mock_lambda_context.MockLambdaContext()),
                                 response_output)


if __name__ == '__main__':
    unittest.main()
