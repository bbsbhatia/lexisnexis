import os
import unittest
from importlib import reload
from unittest.mock import patch

from lng_datalake_commons.error_handling import error_handler
from lng_datalake_constants import event_handler_status
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import create_subscription_event_handler

__author__ = "Kiran G, Arun S"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateSubscriptionEventHandler')

patch_dict = {
    'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
    'S3_TERMINAL_ERRORS_BUCKET_NAME': 'terminal_bucket_name',
    'RETRY_QUEUE_URL': "https://sqs.us-east-1.amazonaws.com/123456789012/123"
}


class CreateSubscriptionEventHandlerIT(unittest.TestCase):

    @classmethod
    @patch.dict(os.environ, patch_dict)
    def setUpClass(cls):  # NOSONAR
        reload(error_handler)

    # + lambda handler email event
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_email(self,
                                  mock_session,
                                  mock_dynamodb_get_client,
                                  mock_sns_get_client,
                                  mock_get_regions,
                                  mock_sqs_get_client):
        mock_session.return_value = None
        mock_get_regions.return_value = ['us-east-1']
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'email_get_item_response.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_sns_get_client.return_value.subscribe.return_value = io_util.load_data_json(
            'sns.subscription_response.json')
        mock_sqs_get_client.return_value.send_message.return_value = None
        self.assertIsNone(create_subscription_event_handler.lambda_handler(io_util.load_data_json("email_event.json"),
                                                                           MockLambdaContext()))
        mock_sqs_get_client.return_value.send_message.assert_called_once()

    # + lambda handler sqs event
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_sqs(self,
                                mock_session,
                                mock_dynamodb_get_client,
                                mock_sns_get_client,
                                mock_get_regions,
                                mock_sqs_get_client):
        mock_session.return_value = None
        mock_get_regions.return_value = ['us-east-1']
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'sqs_get_item_response.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_sns_get_client.return_value.subscribe.return_value = io_util.load_data_json(
            'sns.subscription_response.json')
        mock_sqs_get_client.return_value.send_message.return_value = None
        self.assertIsNone(create_subscription_event_handler.lambda_handler(io_util.load_data_json("sqs_event.json"),
                                                                           MockLambdaContext()))
        mock_sqs_get_client.return_value.send_message.assert_called_once()

    # + lambda handler retry email event
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_retry_email(self,
                                        mock_session,
                                        mock_dynamodb_get_client,
                                        mock_sns_get_client):
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'email_get_item_response.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.subscription_table_put_response.json')
        mock_sns_get_client.return_value.get_subscription_attributes.return_value = io_util.load_data_json(
            'sns.get_subscription_attributes.json')
        self.assertEqual(
            create_subscription_event_handler.lambda_handler(io_util.load_data_json("retry_email_event.json"),
                                                             MockLambdaContext()), event_handler_status.SUCCESS)

    # + lambda handler retry sqs event
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_retry_sqs(self,
                                      mock_session,
                                      mock_dynamodb_get_client,
                                      mock_sns_get_client):
        mock_session.return_value = None
        mock_dynamodb_get_client.return_value.get_item.return_value = io_util.load_data_json(
            'sqs_get_item_response.json')
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.subscription_table_put_response.json')
        mock_sns_get_client.return_value.get_subscription_attributes.return_value = io_util.load_data_json(
            'sns.get_subscription_attributes.json')
        self.assertEqual(
            create_subscription_event_handler.lambda_handler(io_util.load_data_json("retry_sqs_event.json"),
                                                             MockLambdaContext()), event_handler_status.SUCCESS)

    # + lambda handler retry non expired event
    @patch('lng_aws_clients.sqs.get_client')
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('create_subscription_event_handler.current_time')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_retry_non_expired(self,
                                              mock_session,
                                              mock_sns_get_client,
                                              mock_current_time,
                                              mock_get_regions,
                                              mock_sqs_get_client):
        mock_session.return_value = None
        mock_get_regions.return_value = ['us-east-1']
        mock_current_time.return_value = 1
        mock_sns_get_client.return_value.get_subscription_attributes.return_value = io_util.load_data_json(
            'sns.unconfirmed_get_subscription_attributes.json')
        mock_sqs_get_client.return_value.send_message.return_value = None

        self.assertEqual(
            create_subscription_event_handler.lambda_handler(io_util.load_data_json("retry_sqs_event.json"),
                                                             MockLambdaContext()), event_handler_status.SUCCESS)
        mock_sqs_get_client.return_value.send_message.assert_called_once()

    # + lambda handler retry expired event
    @patch('create_subscription_event_handler.current_time')
    @patch('lng_aws_clients.sns.get_client')
    @patch('lng_aws_clients.session.set_session')
    def test_lambda_handler_retry_expired(self,
                                          mock_session,
                                          mock_sns_get_client,
                                          mock_current_time):
        mock_session.return_value = None
        mock_current_time.return_value = 98765432123456
        mock_sns_get_client.return_value.get_subscription_attributes.return_value = io_util.load_data_json(
            'sns.unconfirmed_get_subscription_attributes.json')

        self.assertEqual(
            create_subscription_event_handler.lambda_handler(io_util.load_data_json("retry_sqs_event.json"),
                                                             MockLambdaContext()), event_handler_status.SUCCESS)


if __name__ == '__main__':
    unittest.main()
