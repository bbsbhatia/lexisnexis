import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_dal.table import TableCache
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

import get_republish_command

__author__ = "John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'GetRepublishCommand')

table_dict = {
    'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_subscription_table',
    'REPUBLISH_DYNAMODB_TABLE': 'fake_republish_table',
}


class GetRepublishCommand(unittest.TestCase):
    session = None

    @classmethod
    def setUpClass(cls):  # NOSONAR
        cls.session = patch("lng_aws_clients.session.set_session").start()
        TableCache.clear()

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        cls.session.stop()

    # + Get Republish lambda handler - catalog
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_get_republish_command_catalog(self, mock_dynamodb_get_client):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamo.subscription_data_valid.json')
            elif TableName == 'fake_republish_table':
                return io_util.load_data_json('dynamo.republish_data_valid_catalog.json')
            else:
                return None

        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response_output = io_util.load_data_json('apigateway.response_valid_catalog.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            response = get_republish_command.lambda_handler(request_input, MockLambdaContext())
            self.assertDictEqual(response, expected_response_output)

    # + Get Republish lambda handler - collections
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_get_republish_command_collections(self, mock_dynamodb_get_client):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamo.subscription_data_valid.json')
            elif TableName == 'fake_republish_table':
                return io_util.load_data_json('dynamo.republish_data_valid_collections.json')
            else:
                return None

        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response_output = io_util.load_data_json('apigateway.response_valid_collections.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            response = get_republish_command.lambda_handler(request_input, MockLambdaContext())
            self.assertDictEqual(response, expected_response_output)

    # + Get Republish lambda handler - changeset
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.dynamodb.get_client')
    def test_get_republish_command_changeset(self, mock_dynamodb_get_client):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_subscription_table':
                return io_util.load_data_json('dynamo.subscription_data_valid.json')
            elif TableName == 'fake_republish_table':
                return io_util.load_data_json('dynamo.republish_data_valid_changeset.json')
            else:
                return None

        mock_dynamodb_get_client.return_value.get_item = fake_get_item

        request_input = io_util.load_data_json('apigateway.input_valid.json')
        expected_response_output = io_util.load_data_json('apigateway.response_valid_changeset.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
            response = get_republish_command.lambda_handler(request_input, MockLambdaContext())
            self.assertDictEqual(response, expected_response_output)


if __name__ == '__main__':
    unittest.main()
