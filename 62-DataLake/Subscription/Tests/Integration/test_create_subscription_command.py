import os
import unittest
from unittest.mock import patch

from lng_datalake_commands import command_wrapper
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from create_subscription_command import lambda_handler

__author__ = "Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CreateSubscriptionCommand')

table_dict = {
    'OWNER_DYNAMODB_TABLE': 'fake_owner_table',
    'COLLECTION_DYNAMODB_TABLE': 'fake_collection_table',
    'SUBSCRIPTION_DYNAMODB_TABLE': 'fake_collection_table',
    'COUNTER_DYNAMODB_TABLE': 'fake_collection_table',
    'EVENT_STORE_DYNAMODB_TABLE': 'fake_collection_table'
}


class CreateSubscriptionCommandIT(unittest.TestCase):

    # + lambda handler - success - no schema version specified - defaulted to old version ('v0')
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_email_success(self,
                                                       session_mock, mock_datetime_now,
                                                       mock_dynamodb_get_client,
                                                       mock_session_get_account_id,
                                                       mock_sns_get_client_region,
                                                       ):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamo.collection_data_valid.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.query_owner_data_valid.json'),
            io_util.load_data_json('dynamo.query_subscription_data_valid.json')]
        mock_dynamodb_get_client.return_value.update_item.return_value = {
            "Attributes":
                {
                    "AtomicCounter":
                        {
                            "N": 324
                        }
                }
        }
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_session_get_account_id.return_value = 123456789012
        mock_sns_get_client_region.return_value = "us-east-1"

        request_input = io_util.load_data_json('apigateway.input_request_email.json')
        expected_response_output = io_util.load_data_json('apigateway.expected_email_response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mock_datetime_now.return_value = 15161611
        with patch('create_subscription_command.topic_arn',
                   'arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()), expected_response_output)

    # + lambda handler - success - schema version specified
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_email_schema_version(self, session_mock, mock_datetime_now,
                                                              mock_dynamodb_get_client, mock_session_get_account_id,
                                                              mock_sns_get_client_region):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamo.collection_data_valid.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.query_owner_data_valid.json'),
            io_util.load_data_json('dynamo.query_subscription_data_valid.json')]
        mock_dynamodb_get_client.return_value.update_item.return_value = {
            "Attributes":
                {
                    "AtomicCounter":
                        {
                            "N": 324
                        }
                }
        }
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_session_get_account_id.return_value = 123456789012
        mock_sns_get_client_region.return_value = "us-east-1"

        request_input = io_util.load_data_json('apigateway.input_request_email_2.json')
        expected_response_output = io_util.load_data_json('apigateway.expected_email_response_2.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mock_datetime_now.return_value = 15161611
        with patch('create_subscription_command.topic_arn',
                   'arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()), expected_response_output)

    # + lambda handler
    @patch.dict(os.environ, table_dict)
    @patch('lng_aws_clients.helpers.get_available_service_regions')
    @patch('lng_aws_clients.sns.get_client_region')
    @patch('lng_aws_clients.session.get_account_id')
    @patch('lng_aws_clients.dynamodb.get_client')
    @patch('service_commons.subscription_command.get_pending_expiration_epoch')
    @patch('lng_aws_clients.session.set_session')
    def test_create_subscription_command_sqs_success(self,
                                                     session_mock, mock_datetime_now,
                                                     mock_dynamodb_get_client,
                                                     mock_session_get_account_id,
                                                     mock_sns_get_client_region,
                                                     mock_helper):

        def fake_get_item(TableName, Key):  # NOSONAR
            if TableName == 'fake_collection_table':
                return io_util.load_data_json('dynamo.collection_data_valid.json')
            else:
                return None

        session_mock.return_value = None
        mock_dynamodb_get_client.return_value.get_item = fake_get_item
        mock_helper.return_value = ['us-east-1']
        mock_dynamodb_get_client.return_value.get_paginator.return_value.paginate.return_value.build_full_result.side_effect = [
            io_util.load_data_json('dynamo.query_owner_data_valid.json'),
            io_util.load_data_json('dynamo.query_subscription_data_valid.json')]
        mock_dynamodb_get_client.return_value.update_item.return_value = {
            "Attributes":
                {
                    "AtomicCounter":
                        {
                            "N": 324
                        }
                }
        }
        mock_dynamodb_get_client.return_value.put_item.return_value = io_util.load_data_json(
            'dynamo.event_store_put_response.json')
        mock_session_get_account_id.return_value = 123456789012
        mock_sns_get_client_region.return_value = "us-east-1"

        request_input = io_util.load_data_json('apigateway.input_request_sqs.json')
        expected_response_output = io_util.load_data_json('apigateway.expected_sqs_response.json')
        command_wrapper.WORKING_DIRECTORY = io_util.lambda_path
        mock_datetime_now.return_value = 15161611
        with patch('create_subscription_command.topic_arn',
                   'arn:aws:sns:us-east-1:195052678233:Client-Subscription-Topic-0245c534b7c030383977835ba2c9ae0c'):
            with patch('lng_datalake_commands.command_wrapper._is_initialized', False):
                self.assertDictEqual(lambda_handler(request_input, MockLambdaContext()), expected_response_output)


if __name__ == '__main__':
    unittest.main()
