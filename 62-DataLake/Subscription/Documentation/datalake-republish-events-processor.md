#Running Glue Jobs Locally

To expedite testing changes it is suggested that you run Glue scripts 
locally for evaluating and debugging. 

## Modifying the Script
### Imports
##### Add
    import os
##### Remove (comment out)
    # from awsglue.utils import getResolvedOptions


### Main
##### Add
    
    # sample event message

    event_message = {'republish-id': 'd64e1187-adbf-11e9-bd8b-258cfc1454d6',
             'object-store-table': 'feature-jek-DataLake-ObjectStoreTable',
             'republish-collection-table': 'feature-jek-DataLake-RepublishCollectionTable',
             'catalog-collection-mapping-table': 'feature-jek-DataLake-CatalogCollectionMappingTable',
             'subscription-notification-topic-arn': 'arn:aws:sns:us-east-1:288044017584:jtm-republish-topic',
             'datalake-url': 'https://datalake-feature-jek.content.aws.lexis.com',
             'schema-version': 'v0'}

    args = []

##### Remove (comment out)
    # args = getResolvedOptions(sys.argv, ['event_message'])
    # event_message = json.loads(args['event_message'])
    
### Run Configuration
* Right click and 'create datalake-republish-events-processor'
* In 'Environment' -> 'Environment Variables:' 
* Add <AWS_PROFILE=c-sand> (or your dev aws profile name)
* Run ->