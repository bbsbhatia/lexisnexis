
# Get Republish API Endpoint


Date: 2019-07-30

## Status:  Accepted

* Deciders: Planet X

## Context
Technical Story: [S-73157 Republish Get](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:806088)

* Datalake clients need a way to get information about their republish request(s)
* Regression tests also require a method to retrieve the existence and state of republish requests

## Decision

A GET Republish API endpoint will be added to the existing republish resource:

* GET /subscriptions/v1/{subscription-id}/republish/{republish-id}

This will allow a client to retrieve information related to a specified republish-id.
It should remain light-weight and only return information from the republish table as contained
in the original request except that the current state of the republish is returned. It should
not use filtering or pagination that is normally performed by a GET/Describe or return multiple
republish items that a GET/List would normally provide. It should not try to summarize the number
or state of any of the collections taking part in the republish.

The Create Republish Command Lambda and Republish Table will be modified to save the collection-ids
attribute so the collection list can be return in the GET response without querying the
Republish Collection Table. This will also allow the response schema to be shared
between the Create and Get Republish API endpoints. Note the following Create Republish restrictions
that help satifiy the ligh-weight requirements of this GET Republish Command:
* Number of Catalog IDs requested limited to 5
* Number Collection IDs requested limited to 1000
* One of Catalog or Collection IDs can be requested but not both

Republish API endpoints for List and Describe will be created in another story.

Lambda Request
```json
{
  "subscription-id": "string",
  "republish-id": "string"
}
```
Response 
```json
{
  "republish": {
    "subscription-id": 0,
    "republish-id": "string",
    "republish-expiration-date": "string",
    "republish-state": "string",
    "republish-timestamp": "string",
    "catalog-ids": [
      "string"
    ],
    "collection-ids": [
      "string"
    ]
  }
}
```

## Consequences
Datalake clients will be able to get the status of their Republish Request.

## Links

* Version 1 Story [S-73157](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:806088)
