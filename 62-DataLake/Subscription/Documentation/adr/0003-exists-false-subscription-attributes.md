
# Too Many SNS Filter Attributes


Date: 2019-11-14

## Status:  Accepted 

* Deciders: Planet-X

## Context

SNS filters can have a maximum of 5 attributes, currently there is a possibility of a subscriber choosing collection-id,
catalog-id, event-name, and changeset-only. The DataLake internally has 2 attributes that are added to all subscriptions
notification-type (used for republish), and schema-version.

Because we are unable to support every possible arrangement of these attributes an alternative implementation must be
devised.

1. Add a new topic for Changeset subscribers
    * This topic will emit Changeset::Open and Changeset::Close events.
    * Subscribers can republish a changeset and the DataLake will use the subscription pointing towards the changeset 
    topic and filter on subscription-id
    * All events (including Changeset::open and Changeset::Close) will continue to be published to the existing 
    DataLakeEvents topic
        * This will not negatively impact 'hybrid' subscribers that can sequester objects participating in a changeset 
            until the Changeset::Close event is received

2. Add exists[False] as an option to our current filters.
    * Remove changeset-only as a filter attribute
    * Because we know that our events will publish with the required message attributes we can say that if a message is 
    missing all of the optional attributes of an event the notification is from republish
    * Sample subscription filter attributes
    ```json5
    {
      "schema-version": [
        "v1"
      ],
      "notification-type": [
        "event",
        "1"
      ],
      "collection-id": [
        "coll-1",
        {
          "exists": false
        }
      ],
      "event-name": [
        "Object::Create",
        "Object::Update",
        "Object::Remove",
        {
          "exists": false
        }
      ]
    }
    ```
    * Sample Object::Create message attributes
    ```json5
    {
        "schema-version": "v1",
        "notification-type": "event",
        "collection-id": ["coll-1"],
        "catalog-id": ["cat-a", "cat-b"],
        "event-name": "Object::Create"
    }
    ```
    * Sample Changeset::Create message attributes
    ```json5
    {
        "schema-version": "v1",
        "notification-type": "event",
        "event-name": "Changeset::Create"
    }
    ``` 
    * Sample Changeset::Close message attributes
    ```json5
    {
        "schema-version": "v1",
        "notification-type": "event",
        "collection-id": ["coll-1", "coll-2"],
        "catalog-id": ["cat-a", "cat-b"],
        "event-name": "Changeset::Close"
    }
    ```
    * Sample republish message attributes 
    ```json5
    {
        "schema-version": "v1",
        "notification-type": "1"
    }
    ```
3. Create new republish event names
    * Remove the changeset-only property from our system.
    * Introduce 'Republish::ObjectCreate', 'Republish::ObjectUpdate' and 'Republish::ObjectRemove' event names
    * Subscribers who are interested in only changeset events would subscribe to the following events:
        * 'Changeset::Close'
        * 'Republish::ObjectCreate'
        * 'Republish::ObjectUpdate'
        * 'Republish::ObjectRemove' 
    * Both Object::Create and Republish::ObjectCreate or Object::Remove and Republish::ObjectRemove will be added to 
    the message attributes
4. Hybrid approach: add {'exists': false} and Republish event names.
    * Use the filter additions of {'exists': false} to collection-id and catalog-id to the sns filters
    * Append all sns filters with Republish event names
    * This allows us to push republish events to any subscription that initiated it, regardless 
    of the filter on the subscription


## Decision

Option 3

## Consequences

* Documentation will have to be updated to allow subscribers to set their filter attributes in a way that allows
 republish notifications but not regular object events
* Republish will have to add a list of event names to the notifications being sent out
