
# Republish Regression


Date: 2019-08-08

## Status:  accepted 

* Deciders: Planet X

## Context
Technical Story: [S-70399 Republish Regression Tests](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:771386)
Create regression tests for Create Republish workflow and orchestration for republish.

* Regression of all facets is required for continued deployment excellence

## Decision

To enable the below test cases current Subscription Regression will have to
be extended in the following ways:
* Create an additional Owner
    * test NotAuthorized flow
* Create an additional Collection
    * test Collection not belonging to Catalogs
* Create an additional Catalog
    * Catalog with no Collections
* Create 4 additional Objects

Due to SNS having eventually consistent filter policy updates we have 3 options:
* Create 2 separate queues and Subscriptions to accommodate a Subscription to a Catalog
as well as a Subscription to a Collection
* Update the initial queue's policy to receive messages from the Collection,
however this could take up to 10 minutes to take place and would require regular polling
of the type of event we want.
* Create a single queue, test the Catalog Subscription, remove the Subscription
and recreate it with the Collection filter. -- Preferred by John Konderla 


### Full flow use cases:
* Collection not belonging to Catalogs
* Collection belonging to Catalogs
* Catalog with no Collections
* Catalog with Collections

### Tests of events require:
* Checking that the event is correct
* Check that we have limited duplicates, in the event of a duplicate check the messageId to see if it was a duplicate sns message.
* Compare with ListObjects to ensure we have all the object events for each of the objects in the collections.

### Create republish negative cases:
* Subscription doesn't exist
* Pending Subscription
* Subscription doesn't belong to owner
* Catalog doesn't exist
* Collection doesn't exist
* Request with both Collections and Catalogs

## Consequences

* No separation in v1/LATEST backend processing for republish jobs creates the potential for a
lengthy regression test if jobs are being executed while testing. 

* Regression tests will take away from the 50 possible processing nodes, impacting overall performance.

* SNS filter policy is eventually consistent on updates; the regression test will have to delete
the Subscription and create a new one using the same queue. 

## Links

* Version 1 Story [S-70399](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:771386)