import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, paginator_token, command_validator
from lng_datalake_commands.exceptions import InternalError, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_dal.subscription_table import SubscriptionTable

__author__ = "Prashant S"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

DEFAULT_MAX_ITEMS = int(os.getenv("DEFAULT_MAX_ITEMS", 1000))


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/ListSubscriptionsCommand-RequestSchema.json',
                         'Schemas/ListSubscriptionsCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return list_subscriptions_command(event['request'])


def list_subscriptions_command(request):
    # optional properties
    max_items = request.get('max-items', DEFAULT_MAX_ITEMS)
    next_token = request.get('next-token')
    owner_id = request.get('owner-id')

    pagination_token = None
    # If we have a next-token validate the max-items matches the original request
    # Dyanmo doesn't handle pagination if the max-items changes
    if next_token:
        tokens = paginator_token.unpack_validate_pagination_token(next_token, max_items)
        pagination_token = tokens['pagination-token']

    # sanity check for max-items (request schema should prevent)
    command_validator.validate_max_results(max_items, DEFAULT_MAX_ITEMS)

    subscription_list = get_subscription_list(owner_id, max_items, pagination_token)

    return {"response-dict": generate_response_json(subscription_list, SubscriptionTable().get_pagination_token(),
                                                    max_items)}


def get_subscription_list(owner_id: int, max_items: int, pagination_token: str) -> list:
    if owner_id:
        try:
            subscription_list = SubscriptionTable().query_items(index="subscription-by-owner-id-index",
                                                                max_items=max_items,
                                                                pagination_token=pagination_token,
                                                                KeyConditionExpression="OwnerID=:owner_id",
                                                                ExpressionAttributeValues={
                                                                    ":owner_id": {"N": str(owner_id)}})
        except ClientError as e1:
            logger.error(e1)
            raise InternalError(
                "Unable to query items from Subscription Table index {0}".format("subscription-by-owner-id-index"), e1)
        except Exception as e2:
            logger.error(e2)
            raise UnhandledException(e2)

    else:  # return all subscriptions
        try:
            subscription_list = SubscriptionTable().get_all_items(max_items=max_items,
                                                                  pagination_token=pagination_token)
        except ClientError as e1:
            logger.error(e1)
            raise InternalError("Unable to get items from Subscription Table", e1)
        except Exception as e2:
            logger.error(e2)
            raise UnhandledException(e2)

    return subscription_list


def generate_response_json(subscription_list: list, pagination_token: str, max_items: int) -> dict:
    subscription_response = []
    required_props = command_wrapper.get_required_response_schema_keys('subscriptions-n-pagination', 'properties',
                                                                       'subscriptions', 'items')
    optional_props = command_wrapper.get_optional_response_schema_keys('subscriptions-n-pagination', 'properties',
                                                                       'subscriptions', 'items')
    for subscription_data in subscription_list:
        response_item = {}
        for prop in required_props:
            try:
                response_item[prop] = subscription_data[prop]
            except Exception as e:
                logger.error(e)
                raise InternalError("Missing required property: {0}".format(prop))

        for prop in optional_props:
            if prop in subscription_data:
                response_item[prop] = subscription_data[prop]

        subscription_response.append(response_item)

    response = {"subscriptions": subscription_response}

    if pagination_token:
        response['next-token'] = paginator_token.generate_pagination_token(max_items, pagination_token)

    response['item-count'] = len(subscription_list)

    return response


if __name__ == '__main__':
    import json
    from datetime import datetime
    from lng_datalake_testhelper import mock_lambda_context

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = 'feature-jek-DataLake-SubscriptionTable'

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - LIST SUBSCRIPTIONS COMMAND LAMBDA]")

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                # "owner-id": 104,
                "max-items": 50
                # "next-token": "1|eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ2F0YWxvZ0lEIjogeyJTIjogImNhdGFsb2ctMDEifX19"
            }

        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    lambda_response = lambda_handler(lambda_event, mock_lambda_context.MockLambdaContext())
    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - LIST SUBSCRIPTIONS COMMAND LAMBDA]")
