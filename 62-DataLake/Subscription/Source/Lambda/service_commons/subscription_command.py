import logging
import os
from datetime import datetime, timedelta

from botocore.exceptions import ClientError
from lng_datalake_commands.exceptions import InternalError, UnhandledException, InvalidRequestPropertyValue, \
    NoSuchCollection, NoSuchCatalog
from lng_datalake_dal.catalog_table import CatalogTable
from lng_datalake_dal.collection_table import CollectionTable

__author__ = "Kiran G, Jose Molinet"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# schema versions for published messages
current_schema_versions = ['v0', 'v1']


def validate_filter(updated_filter: dict) -> None:
    for key, value in updated_filter.items():
        if not value:
            raise InvalidRequestPropertyValue("Invalid filter key {0}".format(key),
                                              "Filter must not contain empty array values")

    for collection_id in updated_filter.get('collection-id', []):
        if not is_valid_collection_id(collection_id):
            raise NoSuchCollection("Invalid Collection ID {0}".format(collection_id),
                                   "Collection ID does not exist")

    for catalog_id in updated_filter.get('catalog-id', []):
        if not is_valid_catalog_id(catalog_id):
            raise NoSuchCatalog("Invalid Catalog ID {0}".format(catalog_id),
                                "Catalog ID does not exist")


def is_valid_collection_id(collection_id: str) -> None:
    try:
        return CollectionTable().item_exists({'collection-id': collection_id})
    except ClientError as e:
        logger.error(e)
        raise InternalError("Unable to get item from Collection Table", e)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)


def is_valid_catalog_id(catalog_id: str) -> None:
    try:
        return CatalogTable().item_exists({'catalog-id': catalog_id})
    except ClientError as e:
        logger.error(e)
        raise InternalError("Unable to get item from Catalog Table", e)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)


def get_pending_expiration_epoch(days: int) -> int:
    return int((datetime.now() + timedelta(days=days)).timestamp())


def validate_schema_version(request: dict) -> None:
    if 'schema-version' not in request:
        request['schema-version'] = [current_schema_versions[-1]]
    elif not request['schema-version']:
        raise InvalidRequestPropertyValue("Invalid schema version", "Schema version must not be an empty array")
    else:
        invalid_versions = [v for v in request['schema-version'] if v not in current_schema_versions]
        if invalid_versions:
            raise InvalidRequestPropertyValue("Invalid schema versions: {0}".format(invalid_versions),
                                              "Schema version should be one of {0}".format(current_schema_versions))
