import logging
import os
from functools import wraps

from lng_datalake_commons import sqs_extractor

__author__ = "Team Wormhole"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

# Init Logging
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

input_sqs_url = ''


def queue_wrapper(func):
    @wraps(func)
    def func_wrapper(event, context):
        try:
            logger.debug("Sqs Event: {0}".format(event))
            logger.debug("Sqs Length: {0}".format(len(event['Records'])))

            resp = []
            input_sqs_url = ''
            for record in event['Records']:

                if not input_sqs_url:
                    set_input_queue_url(record['eventSourceARN'])

                # Need to form event that looks like AWS SNS for downstream data
                base_sns_event = {'EventSource': 'aws::sns', 'EventVersion': '1.0', 'EventSubscriptionArn': 'SQS'}

                sns_event = sqs_extractor.extract_sqs_message(record)
                base_sns_event['Sns'] = sns_event
                new_event = {'Records': [base_sns_event]}
                resp.append(func(new_event, context))

            return resp

        except Exception as e:
            logger.warning("Error extracting queue message: {}".format(e))
            return func(event, context)

    return func_wrapper


def set_input_queue_url(arn: str) -> None:
    global input_sqs_url
    arn_parts = arn.split(':')
    input_sqs_url = "https://sqs.{0}.amazonaws.com/{1}/{2}".format(arn_parts[3], arn_parts[4], arn_parts[5])
