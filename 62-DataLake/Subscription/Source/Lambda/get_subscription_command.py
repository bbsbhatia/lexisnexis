import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError, UnhandledException, NoSuchSubscription
from lng_datalake_commons import session_decorator
from lng_datalake_dal.subscription_table import SubscriptionTable

__author__ = "Maen Nanaa"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/GetSubscriptionCommand-RequestSchema.json',
                         'Schemas/GetSubscriptionCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return get_subscription_command(event['request'])


def get_subscription_command(request: dict) -> dict:
    subscription_id = request['subscription-id']

    try:
        subscription_record = SubscriptionTable().get_item({"subscription-id": subscription_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Subscription Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not subscription_record:
        raise NoSuchSubscription("Invalid Subscription ID {0}".format(subscription_id),
                                 "Subscription ID does not exist")

    response_dict = generate_response_json(subscription_record)

    return {'response-dict': response_dict}


def generate_response_json(subscription_record):
    response_dict = {}

    required_props = command_wrapper.get_required_response_schema_keys('get-subscription-response-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('get-subscription-response-properties')

    for prop in required_props:
        try:
            response_dict[prop] = subscription_record[prop]
        except Exception as e:
            logger.error(e)
            raise InternalError("Missing required property: {0}".format(prop))

    for prop in optional_props:
        if prop in subscription_record:
            response_dict[prop] = subscription_record[prop]

    return response_dict


if __name__ == '__main__':
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - GET SUBSCRIPTION COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-SubscriptionTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": "1526671877813",
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                'subscription-id': 169
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)
    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - GET SUBSCRIPTION COMMAND LAMBDA]")
