import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, SemanticError, NoSuchSubscription, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_constants import subscription_status, event_names
from lng_datalake_dal.subscription_table import SubscriptionTable

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/RemoveSubscriptionCommand-RequestSchema.json',
                         'Schemas/RemoveSubscriptionCommand-ResponseSchema.json',
                         event_names.SUBSCRIPTION_REMOVE)
def lambda_handler(event, context):
    return remove_subscription_command(event['request'],
                                       event['context']['client-request-id'],
                                       event['context']['stage'],
                                       event['context']['api-key-id'])


def remove_subscription_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    subscription_id = request['subscription-id']

    try:
        subscription_record = SubscriptionTable().get_item({"subscription-id": subscription_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Subscription Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not subscription_record:
        raise NoSuchSubscription("Invalid Subscription ID {0}".format(subscription_id),
                                 "Subscription ID does not exist")

    # owner authorization
    owner_authorization.authorize(subscription_record['owner-id'], api_key_id)

    # validate subscription is not pending
    subscription_state = subscription_record['subscription-state']
    if subscription_state == subscription_status.PENDING:
        raise SemanticError("Subscription ID {0} is pending confirmation".format(subscription_id),
                            "Confirm subscription before removing")

    response_dict = generate_response_json(subscription_record)

    event_dict = generate_event_dict(response_dict, request_id, stage)

    return {'response-dict': response_dict, 'event-dict': event_dict}


def generate_response_json(subscription_record):
    response_dict = {}

    required_props = command_wrapper.get_required_response_schema_keys('remove-subscription-response-properties')

    for prop in required_props:
        try:
            if prop == 'subscription-state':
                response_dict[prop] = subscription_status.TERMINATING
            else:
                response_dict[prop] = subscription_record[prop]
        except Exception as e:
            logger.error(e)
            raise InternalError("Missing required property: {0}".format(prop))

    return response_dict


def generate_event_dict(subscription_record, request_id, stage):
    event_dict = \
        {
            'subscription-id': subscription_record['subscription-id'],
            'event-id': request_id,
            'item-name': subscription_record['subscription-name'],
            'event-name': event_names.SUBSCRIPTION_REMOVE,
            'request-time': command_wrapper.get_request_time(),
            'event-version': event_version,
            'stage': stage
        }
    return event_dict


if __name__ == '__main__':
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE SUBSCRIPTION COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = 'feature-DataLake-SubscriptionTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-DataLake-OwnerTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-DataLake-TrackingTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": "1526671877813",
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "DELETE",
                "api-key-id": "<SET ME>"
            },
            "request": {
                'subscription-id': 20
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - REMOVE SUBSCRIPTION COMMAND LAMBDA]")
