import logging
import orjson
import os
from datetime import datetime
from inspect import getmodulename, stack

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_aws_clients import session, sns, sqs
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, \
    RetryEventException
from lng_datalake_constants import event_names, subscription_status, event_handler_status
from lng_datalake_dal.subscription_table import SubscriptionTable

from service_commons import subscription_event_handler

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Error Handling variables
error_handler.lambda_name = getmodulename(stack()[0][1])

topic_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@subscription_event_handler.queue_wrapper
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))
    logger.info("Context: {0}".format(vars(context)))
    return create_subscription_event_handler(event, context.invoked_function_arn)


def create_subscription_event_handler(event, lambda_arn):

    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.SUBSCRIPTION_CREATE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.SUBSCRIPTION_CREATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    subscription_id = message['subscription-id']
    pending_expiration_epoch = message['pending-expiration-epoch']

    # If we don't have pending-confirmation we need to create the subscription
    # This will raise an exception to retry since we know we won't have immediate confirmation
    if not message.get('additional-attributes', {}).get('pending-confirmation', False):
        initiate_subscription(message)

    # If we have already sent confirmation before, see if it's accepted or not
    # We fall out of the if/else condition if we have no confirmation and the expiration has passed so everything is gone
    if is_subscription_confirmed(message):
        update_subscription_record(subscription_id, message['event-id'])
    elif current_time() <= pending_expiration_epoch:
        # If we still have time before we expire then retry
        retry_subscription_confirmation(RetryEventException('Subscription has yet to be confirmed'), event,
                                        message)
    else:
        logger.warning("Subscription not confirmed in 3 days")

    return event_handler_status.SUCCESS


def initiate_subscription(message: dict) -> None:
    subscription_arn = create_sns_subscription(message)['SubscriptionArn']
    insert_record = create_subscription_insert_dict(message, subscription_arn)
    insert_to_subscription_table(insert_record)
    error_handler.set_message_additional_attribute('subscription-arn', subscription_arn)
    error_handler.set_message_additional_attribute('pending-confirmation', True)
    error_handler.set_message_additional_attribute('retry-count', 0)
    raise RetryEventException('Initial creation of subscription, retrying to validate confirmation')


def create_sns_subscription(subscription_record: dict) -> dict:
    # Add notification-type and schema-version to filter before it is added to the sns filter policy
    # copy the values out of this dictionary as we don't need to put notification-type in the db
    # in case of more than one schema version, the concatenation will be used: ["v0", "v1"] -> ["v0v1"]
    filter_policy = subscription_record.get('filter', {}).copy()
    filter_policy['notification-type'] = ['event', str(subscription_record['subscription-id'])]
    filter_policy['schema-version'] = [''.join(subscription_record['schema-version'])]
   
    try:
        return sns.get_client().subscribe(TopicArn=topic_arn,
                                          Protocol=subscription_record['protocol'],
                                          Endpoint=subscription_record['endpoint'],
                                          ReturnSubscriptionArn=True,
                                          Attributes={"FilterPolicy": orjson.dumps(filter_policy).decode()})
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            'An unknown error occurred when attempting to creation a subscription: {0} Error: {1}'.format(
                subscription_record, e))


def create_subscription_insert_dict(subscription_record: dict, subscription_arn: str) -> dict:
    insert_record = {}
    try:
        for key in SubscriptionTable().get_required_schema_keys():
            if key == 'subscription-state':
                insert_record[key] = subscription_status.PENDING
            elif key == "event-ids":
                insert_record[key] = SubscriptionTable().build_event_ids_list({}, subscription_record['event-id'])
            else:
                insert_record[key] = subscription_record[key]
    except KeyError as e:
        raise TerminalErrorException("Key = {} does not exist".format(e))

    for key in SubscriptionTable().get_optional_schema_keys():
        if key == 'subscription-arn':
            insert_record[key] = subscription_arn
        if key in subscription_record:
            insert_record[key] = subscription_record[key]

    return insert_record


def insert_to_subscription_table(item: dict) -> None:
    try:
        SubscriptionTable().put_item(item)
        logger.info("Insertion to Subscription table succeeded")
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to insert item {} into Subscription table due to unknown reasons: {}".format(item, e))


def is_subscription_confirmed(message: dict) -> bool:
    try:
        resp = sns.get_client().get_subscription_attributes(
            SubscriptionArn=message['additional-attributes']['subscription-arn'])
        # values are string true/false so return the boolean compare to true/false
        return not resp['Attributes']['PendingConfirmation'] == 'true'
    except ClientError as e:
        if e.response["Error"]["Code"] == "NotFound":
            return False
        raise e
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        logger.error(e)
        raise TerminalErrorException("General Exception Occurred")


def current_time() -> int:
    return int(datetime.now().timestamp())


def update_subscription_record(subscription_id: int, event_id: str) -> None:
    try:
        record = SubscriptionTable().get_item({'subscription-id': subscription_id})
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to get item {0} into Subscription table due to unknown reasons. {1}".format(subscription_id, e))

    if not record:
        raise TerminalErrorException(
            "Item in Subscription table was missing: {0}".format(subscription_id))

    record['subscription-state'] = subscription_status.CREATED
    record.pop('pending-expiration-epoch')

    # TODO remove this block later
    # for older records set event-ids if not set already
    if not record.get('event-ids', []):
        record["event-ids"] = SubscriptionTable().build_event_ids_list({}, event_id)

    try:
        SubscriptionTable().update_item(record)
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to update item {0} into Subscription table due to unknown reasons. {1}".format(subscription_id, e))


def retry_subscription_confirmation(e: RetryEventException, event: dict, message: dict) -> None:
    logger.warning(e)
    retry_count = message.get('additional-attributes', {}).get('retry-count', 0) + 1

    # update retry-count for new retry message
    message['additional-attributes']['retry-count'] = retry_count
    retry_sqs_message = event['Records'][0]['Sns']
    retry_sqs_message['Message'] = orjson.dumps(message).decode()

    # Adding back off for each retry
    sqs_delay = max(5, min(retry_count * 5, 900))  # 900 seconds is the longest delay supported by SQS

    if subscription_event_handler.input_sqs_url:
        sqs_url = subscription_event_handler.input_sqs_url
    # TODO : Adding this to protect against messages in flight pending subscription. Can be removed later
    else:
        sqs_url = os.getenv("RETRY_QUEUE_URL")

    try:
        sqs.get_client().send_message(QueueUrl=sqs_url,
                                      MessageBody=orjson.dumps(retry_sqs_message).decode(),
                                      DelaySeconds=sqs_delay)

    except Exception as e:
        raise TerminalErrorException(
            "Failed to send retry message {0} to SQS {1} error: {2}".format(sqs_url,
                                                                            retry_sqs_message, e))

    logger.info('Successfully placed event on queue {} for retry with seen-count = {}'.format(sqs_url,
                                                                                              retry_count))


if __name__ == '__main__':
    from random import randint
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    import random
    import string

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE SUBSCRIPTION EVENT HANDLER LAMBDA]")

    ASSET_GROUP = 'feature-kgg'

    os.environ['AWS_PROFILE'] = 'c-sand'

    os.environ['SUBSCRIPTION_NOTIFICATION_TOPIC_ARN'] = 'arn:aws:sns:us-east-1:288044017584:{0}-DataLakeEvents'.format(
        ASSET_GROUP)
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = '{0}-DataLake-SubscriptionTable'.format(ASSET_GROUP)
    os.environ[
        "RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/feature-mas-DataLake-RetryQueues-RetryQueue-YAKX13ITDLSF"

    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"

    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")
    topic_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")

    session.set_session()


    def generate_random_id_as_string(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    def generate_random_id_as_int(n):
        return int(''.join([str(randint(1, 9)) for _ in range(n)]))


    lambda_event = {
        "Records": [
            {
                "messageId": "4f6ae4b4-de4f-4a89-ad35-598bb4106f0d",
                "receiptHandle": "AQEBux8wvUXQWhoQmfLllPjWApPEgRATN+eqoBPeQm/s6Jg4SUkOA9ESb0SKhinU9s3WyUSO9kV5Nq7ko/3EFGeO7VdqFz6UVIBEXLhfReF1bHAeGZ69NHgbYyQWsX2qau2A96q23uPoifQdprcrSvXF0MA4UzGQLTIHbH9Kl8k5jgfvtmIdbr2THiowuz4ctsJs8KzlclOxYpZzDW2PgJIkHaq5r6eADr+x5ykJCYY4LmqZtLImp/x/VS9IhGVAhP4yh552jRmCTJqpoei29plGYJNrs6gnCgQQKOkZWW5SHP6OIrPZjB/NDO+bMgB41L/pRhlz/7YDrnPLEJag9BLiXRnpEl/0zoMVpLCwRtUdF1S7OK/KPRvggMDSSjSgeV1pqgpIlvKD/dq+FnxA87LRy5YlWvrKd1+qIDG/3m7FkQPOT7UAXzUoG/iuYiKNbyq5jOelM5SBa+SMDXVQBY6Mvg==",
                "body": "{\n  \"Type\" : \"Notification\",\n  \"MessageId\" : \"b241b348-cc87-5b54-b355-e505dd241ecc\",\n  \"TopicArn\" : \"arn:aws:sns:us-east-1:288044017584:feature-kgg-DataLake-EventDispatcher-EventDispatcherTopic-1Q26EYGWDJBYU\",\n  \"Message\" : \"{\\\"schema-version\\\": [\\\"v0\\\", \\\"v1\\\"], \\\"request-time\\\": \\\"2019-11-18T21:38:38.521Z\\\", \\\"event-name\\\": \\\"Subscription::Create\\\", \\\"subscription-name\\\": \\\"regression_subscription_publish_2019-11-18T21:38:38.321726\\\", \\\"subscription-id\\\": 313, \\\"endpoint\\\": \\\"arn:aws:sqs:us-east-1:284211348336:feature-kgg-Subscription-1-SubscriptionQueue-S8NIBYR5UA7J\\\", \\\"owner-id\\\": 1694, \\\"filter\\\": {\\\"collection-id\\\": [\\\"604\\\"], \\\"event-name\\\": [\\\"Object::Create\\\", \\\"Object::Update\\\", \\\"Object::Remove\\\", \\\"Collection::Update\\\", \\\"Collection::Resume\\\", \\\"Collection::Suspend\\\"]}, \\\"pending-expiration-epoch\\\": 1574372322, \\\"event-id\\\": \\\"89bb13f8-1d5c-4984-b7af-516fd2a194bb\\\", \\\"stage\\\": \\\"LATEST\\\", \\\"protocol\\\": \\\"sqs\\\", \\\"changesets-only\\\": false, \\\"event-version\\\": 1, \\\"seen-count\\\": 0}\",\n  \"Timestamp\" : \"2019-11-18T21:38:43.648Z\",\n  \"SignatureVersion\" : \"1\",\n  \"Signature\" : \"D0H3951+hiOYdmlI6/s6ROW7bBVMHs/72c8eRm67taYNwOBtlXKHlo8aPsRDQFTvVY8Y07d2LTlOGSgtROqQ+X9FBL5L+hcbyZvEkXeiEO5FvcSWlJ7NDEC7F0yc8aw2+sMH7M0vB1iHPBD6jBdOjstideCBTG0obtbuBOnY3lfm5rVkAywfpGTggMmVo9wkpInQ8pciEVvYu3bQtmigKBcYlfD1QS4OTXN4On20AJ6QAXW4h1cM7GE2PLLBjCBBZXgigdaq0qZ8JH6V3uOaas3FKJePytLjKOFJkU88PJKLTCp+0PSULVGj2ZQSPsMqnycBlqRoJxtT4mq8tYeS2Q==\",\n  \"SigningCertURL\" : \"https://sns.us-east-1.amazonaws.com/SimpleNotificationService-6aad65c2f9911b05cd53efda11f913f9.pem\",\n  \"UnsubscribeURL\" : \"https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:feature-kgg-DataLake-EventDispatcher-EventDispatcherTopic-1Q26EYGWDJBYU:ac836e47-83bd-4d2b-95ef-4d86e79bf607\",\n  \"MessageAttributes\" : {\n    \"stage\" : {\"Type\":\"String\",\"Value\":\"LATEST\"},\n    \"event-name\" : {\"Type\":\"String\",\"Value\":\"Subscription::Create\"},\n    \"event-version\" : {\"Type\":\"Number\",\"Value\":\"1\"}\n  }\n}",
                "attributes": {
                    "ApproximateReceiveCount": "1",
                    "SentTimestamp": "1574113123706",
                    "SenderId": "AIDAIT2UOQQY3AUEKVGXU",
                    "ApproximateFirstReceiveTimestamp": "1574113123712"
                },
                "messageAttributes": {},
                "md5OfBody": "b30f02b0d8a8dc5ade2e05afaeb8f856",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn:aws:sqs:us-east-1:288044017584:feature-kgg-LATEST-subscription-CreateSubscription-Queue",
                "awsRegion": "us-east-1"
            }
        ]
    }

    lambda_handler(lambda_event, MockLambdaContext)
    logger.debug("[LOCAL RUN END - CREATE SUBSCRIPTION EVENT HANDLER LAMBDA]")
