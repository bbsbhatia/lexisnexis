import logging
import orjson
import os
from inspect import getmodulename, stack

import lng_datalake_commons.error_handling.error_handler as error_handler
from aws_xray_sdk.core import patch
from lng_aws_clients import sns
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.subscription_table import SubscriptionTable

from service_commons import subscription_event_handler

__author__ = "Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Error Handling variables
error_handler.lambda_name = getmodulename(stack()[0][1])

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@subscription_event_handler.queue_wrapper
@error_handler.handle
@tracker.track_event_handler(event_names.SUBSCRIPTION_REMOVE)
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))
    logger.info("Context: {0}".format(vars(context)))
    return remove_subscription_event_handler(event, context.invoked_function_arn)


def remove_subscription_event_handler(event, lambda_arn):
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.SUBSCRIPTION_REMOVE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name doesnt match {}".format(event, event_names.SUBSCRIPTION_REMOVE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    subscription_id = message['subscription-id']
    subscription_record = SubscriptionTable().get_item({"subscription-id": subscription_id})

    # It's possible the record expired before we process the remove
    if subscription_record:
        remove_sns_subscription(subscription_record['subscription-arn'])
        remove_subscription_record(subscription_id)

    return event_handler_status.SUCCESS


def remove_sns_subscription(subscription_arn):
    try:
        return sns.get_client().unsubscribe(
            SubscriptionArn=subscription_arn
        )
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException(
            'An unknown error occurred when attempting to remove a subscription: {0} Error: {1}'.format(
                subscription_arn, error))


def remove_subscription_record(subscription_id):
    try:
        SubscriptionTable().delete_item({"subscription-id": subscription_id})
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException(
            "General exception occurred when removing subscription id {0} from table. Error: {1}".format(
                subscription_id, error))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - REMOVE SUBSCRIPTION EVENT HANDLER LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'

    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/SubscriptionEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = 'feature-DataLake-DataLakeDynamoTables-SubscriptionTable-17Q8TF74TGVBD'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-DataLake-DataLakeDynamoTables-TrackingTable-3BRUJZODZGI'

    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {
        "event-name": event_names.SUBSCRIPTION_REMOVE,
        'subscription-id': 8,
        "event-id": "sevQJiUVwEFIMAbn",
        "request-time": "2017-11-13T10:16:08.829Z",
        "item-name": 'subs_test_1',
        "event-version": 1,
        "stage": "LATEST"
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    mock_context = mock_lambda_context.MockLambdaContext()
    lambda_handler(sample_event, mock_context)
    logger.debug("[LOCAL RUN END - REMOVE SUBSCRIPTION EVENT HANDLER LAMBDA]")
