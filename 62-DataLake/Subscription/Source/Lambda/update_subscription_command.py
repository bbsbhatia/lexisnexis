import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization, command_validator
from lng_datalake_commands.exceptions import InternalError, SemanticError, InvalidRequestPropertyValue, \
    NoSuchSubscription, UnhandledException
from lng_datalake_commons import session_decorator
from lng_datalake_constants import event_names, subscription_status
from lng_datalake_dal.subscription_table import SubscriptionTable

from service_commons import subscription_command

__author__ = "Maen Nanaa, John Konderla, Kiran G"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/UpdateSubscriptionCommand-RequestSchema.json',
                         'Schemas/UpdateSubscriptionCommand-ResponseSchema.json',
                         event_names.SUBSCRIPTION_UPDATE)
def lambda_handler(event, context):
    return update_subscription_command(event['request'],
                                       event['context']['client-request-id'],
                                       event['context']['stage'],
                                       event['context']['api-key-id'])


def update_subscription_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    # validate patch operations
    patch_operations = request['patch-operations']
    command_validator.validate_patch_operations("Schemas/Patch/Subscriptions", patch_operations)

    subscription_id = request['subscription-id']

    try:
        subscription_record = SubscriptionTable().get_item({"subscription-id": subscription_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Subscription Table", e)
    except Exception as e:
        raise UnhandledException(e)

    if not subscription_record:
        raise NoSuchSubscription("Invalid Subscription ID {0}".format(subscription_id),
                                 "Subscription ID does not exist")

    # owner authorization
    owner_authorization.authorize(subscription_record['owner-id'], api_key_id)

    # validate subscription is not pending
    if subscription_record['subscription-state'] == subscription_status.PENDING:
        raise SemanticError("Subscription ID {0} is pending confirmation".format(subscription_id),
                            "Confirm subscription before updating it")

    request = replace_op_commands(request)

    subscription_name = request.get('subscription-name', subscription_record['subscription-name'])
    is_name_changed = subscription_name != subscription_record['subscription-name']

    current_filter = subscription_record.get('filter', {})
    updated_filter = request.get('filter', current_filter)
    is_filter_changed = current_filter != updated_filter

    schema_version = request.get('schema-version', subscription_record['schema-version'])
    is_schema_version_changed = schema_version != subscription_record['schema-version']

    is_update_event = is_name_changed or is_filter_changed or is_schema_version_changed

    # find if there is any changes in update request to process
    if not is_update_event:
        raise SemanticError("No data changes")

    # Validate collection and catalog id's in filter if exist
    if is_filter_changed:
        subscription_command.validate_filter(updated_filter)

    if is_schema_version_changed:
        subscription_command.validate_schema_version(request)

    # Deny because we do not send out no-op events for the v0 schema
    # TODO delete this when v0 schema is removed
    if "v0" in schema_version \
            and event_names.OBJECT_UPDATE_NO_CHANGE in updated_filter.get('event-name', []):
        raise SemanticError("Invalid filter for given event-version",
                            "v0 subscriptions do not allow filtering on Object::UpdateNoChange events")

    # we don't return this in the request so remove it
    subscription_record.pop('subscription-arn')

    subscription_record.update(request)

    response_dict = generate_response_json(subscription_record)

    event_dict = generate_event_dict(response_dict, request_id, stage, is_name_changed, is_filter_changed,
                                     is_schema_version_changed)

    return {'response-dict': response_dict, 'event-dict': event_dict}


# TODO: Add ability for smart patch merging (ie add) today we only support full replace
def replace_op_commands(request: dict) -> dict:
    for item in request['patch-operations']:
        if item['op'] != "replace":
            raise InvalidRequestPropertyValue("Invalid operation verb supplied", "Only replace is currently supported")

        request[item['path'].split('/')[1]] = item['value']

    del request['patch-operations']
    return request


def generate_response_json(subscription_record: dict) -> dict:
    response_dict = {}
    prop = None

    required_props = command_wrapper.get_required_response_schema_keys('update-subscription-response')
    optional_props = command_wrapper.get_optional_response_schema_keys('update-subscription-response')

    try:
        for prop in required_props:
            response_dict[prop] = subscription_record[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError('Missing required attribute: {0}'.format(prop))

    for prop in optional_props:
        if prop in subscription_record:
            response_dict[prop] = subscription_record[prop]

    return response_dict


def generate_event_dict(subscription_response: dict, request_id: str, stage: str, is_name_changed: bool,
                        is_filter_changed: bool, is_schema_version_changed: bool) -> dict:
    event_dict = \
        {
            'subscription-id': subscription_response['subscription-id'],
            'event-id': request_id,
            'event-name': event_names.SUBSCRIPTION_UPDATE,
            'request-time': command_wrapper.get_request_time(),
            'event-version': event_version,
            'stage': stage
        }
    if is_name_changed:
        event_dict['subscription-name'] = subscription_response['subscription-name']
    if is_filter_changed:
        event_dict['filter'] = subscription_response['filter']
    if is_schema_version_changed:
        event_dict['schema-version'] = subscription_response['schema-version']

    return event_dict


if __name__ == '__main__':
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string
    import json

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE SUBSCRIPTION COMMAND LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-SubscriptionTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-EventStoreTable'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-TrackingTable'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CollectionTable'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CatalogTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-OwnerTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": "141b1f73-29fa-43fb-bcdd-2a6098157df7",
                "client-request-time": "1569552701794",
                "client-type": "aws-apigateway",
                "stage": "LATEST",
                "client-id": "zsgp22ldcl",
                "http-method": "PATCH",
                "resource-id": "8w3but",
                "resource-path": "/{subscriptionId}",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "subscription-id": 169,
                "patch-operations": [
                    {
                        "op": "replace1",
                        "path": "/changesets-only",
                        "value": False
                    }
                ]
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)
    command_validator.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - UPDATE SUBSCRIPTION COMMAND LAMBDA]")
