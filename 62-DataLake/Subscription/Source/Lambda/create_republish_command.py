import logging
import os
from datetime import datetime

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, SemanticError, \
    NoSuchCollection, NoSuchCatalog, NoSuchChangeset, UnhandledException, NoSuchSubscription
from lng_datalake_commons import session_decorator, time_helper
from lng_datalake_constants import event_names, collection_status, subscription_status, republish_status, changeset_status
from lng_datalake_dal.catalog_table import CatalogTable
from lng_datalake_dal.collection_table import CollectionTable
from lng_datalake_dal.subscription_table import SubscriptionTable
from lng_datalake_dal.changeset_table import ChangesetTable

from service_commons import subscription_command

__author__ = "John Konderla, Daniel Wang"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/CreateRepublishCommand-RequestSchema.json',
                         'Schemas/CreateRepublishCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return create_republish_command(event['request'],
                                    event['context']['client-request-id'],
                                    event['context']['stage'],
                                    event['context']['api-key-id'])


def create_republish_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    subscription_record = get_subscription(request['subscription-id'])

    owner_authorization.authorize(subscription_record['owner-id'], api_key_id)

    validated_request = validate_request_filter(request)

    pending_expiration_epoch = subscription_command.get_pending_expiration_epoch(days=30)

    # response json to be returned
    response_dict = generate_response_json(validated_request, subscription_record, request_id, pending_expiration_epoch)

    event_dict = generate_event_dict(response_dict, request_id, stage, pending_expiration_epoch,
                                     subscription_record['schema-version'])

    return {'response-dict': response_dict, 'event-dict': event_dict}


def get_subscription(subscription_id: int) -> dict:
    try:
        response = SubscriptionTable().get_item({'subscription-id': subscription_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Subscription Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not response or response['subscription-state'] == subscription_status.TERMINATED:
        raise NoSuchSubscription('Subscription {} does not exist'.format(subscription_id),
                                 'Create subscription before beginning republish')
    if response['subscription-state'] == subscription_status.PENDING:
        raise SemanticError('Subscription {} is in {} state'.format(response['subscription-id'],
                                                                    subscription_status.PENDING),
                            'Confirm subscription before creating republish event')

    return response


def validate_request_filter(request_filter: dict) -> dict:
    catalog_ids = set(request_filter.get('catalog-ids', []))
    collection_ids = set(request_filter.get('collection-ids', []))
    changeset_id = request_filter.get('changeset-id', '')

    valid_republish_types = iter([catalog_ids, collection_ids, changeset_id])

    if not catalog_ids and not collection_ids and not changeset_id:
        raise InvalidRequestPropertyValue(
            "No Catalog IDs or Collection IDs or Changeset ID to Republish",
            "Request must only have one of the following: "
            "a list of Catalog IDs, a list of Collection IDs, or a Changeset ID")
    if not(any(valid_republish_types) and not any(valid_republish_types)):
        raise InvalidRequestPropertyValue(
            "More than one of either Catalog IDs or Collection IDs or a Changeset ID submitted",
            "Request must only have one of the following: "
            "a list of Catalog IDs, a list of Collection IDs, or a Changeset ID")

    response_dict = {}
    if catalog_ids:
        response_dict['catalog-ids'] = get_valid_catalogs(list(catalog_ids))
    if collection_ids:
        response_dict['collection-ids'] = get_valid_collections(list(collection_ids))
    if changeset_id:
        validate_changeset(changeset_id)
        response_dict['changeset-id'] = changeset_id

    return response_dict


def get_valid_catalogs(catalog_ids: list) -> list:
    """
    Retrieve a batch of catalogs and return the items that are valid
    :param catalog_ids: list of catalog ids
    :return: set of existent catalog items
    """
    try:
        items = CatalogTable().batch_get_items('catalog-id', catalog_ids)
    except ClientError as e:
        logger.error(e)
        raise InternalError("Unable to get item from Catalog Table", e)

    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)

    if len(items) != len(catalog_ids):
        invalid_ids = get_invalid_catalog_ids(catalog_ids, items)
        raise NoSuchCatalog("Invalid Catalog IDs {}".format(invalid_ids),
                            "Catalog ID(s) does not exist")
    return catalog_ids


def get_invalid_catalog_ids(catalog_ids: list, catalog_items: list) -> set:
    valid_ids = set([item['catalog-id'] for item in catalog_items])
    return set(catalog_ids).difference(valid_ids)


def get_valid_collections(collection_ids: list) -> list:
    """
    Retrieve a batch of collections and return the items that are valid
    :param collection_ids: list of collection ids
    :return: list of existent collection items with state CREATED or SUSPENDED
    """
    try:
        items = CollectionTable().batch_get_items('collection-id', collection_ids)
    except ClientError as e:
        logger.error(e)
        raise InternalError("Unable to get item from Collection Table", e)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)

    if len(collection_ids) != len(items):
        invalid_ids = get_invalid_collection_ids(collection_ids, items)
        raise NoSuchCollection("Invalid Collection IDs {}".format(invalid_ids),
                               "Collection ID(s) does not exist")

    valid_items = []
    for item in items:
        if item['collection-state'] in [collection_status.CREATED, collection_status.SUSPENDED]:
            valid_items.append(item)

    if len(valid_items) != len(collection_ids):
        invalid_collections_state = get_invalid_collection_ids(collection_ids, valid_items)
        raise SemanticError("Invalid Collection IDs {}".format(invalid_collections_state),
                            "Collections must be in either a state of Created or Suspended")
    return collection_ids


def get_invalid_collection_ids(collection_ids: list, collection_items: list) -> set:
    valid_ids = set([item['collection-id'] for item in collection_items])
    return set(collection_ids).difference(valid_ids)


def validate_changeset(changeset_id: str) -> None:
    """
    Retrieve a valid changeset and check if it exists and is in closed state
    :param changeset_id
    """
    try:
        changeset = ChangesetTable().get_item({'changeset-id': changeset_id})
    except ClientError as e:
        logger.error(e)
        raise InternalError("Unable to get item from Changeset Table", e)
    except Exception as ex:
        logger.error(ex)
        raise UnhandledException(ex)

    if not changeset:
        raise NoSuchChangeset("Invalid Changeset ID {}".format(changeset_id), "Changeset ID does not exist")

    if changeset['changeset-state'] != changeset_status.CLOSED:
        raise SemanticError("Invalid Changeset State: {}".format(changeset['changeset-state']),
                            "Changeset must be in Closed State")


def generate_response_json(validated_request: dict, subscription_record: dict, request_id: str,
                           pending_expiration_epoch: int) -> dict:
    republish_record = {}

    required_props_generated = \
        {
            "republish-state": republish_status.PENDING,
            "republish-id": request_id,
            'republish-timestamp': command_wrapper.get_request_time(),
        }

    required_props = command_wrapper.get_required_response_schema_keys('republish-response')
    optional_props = command_wrapper.get_optional_response_schema_keys('republish-response')
    prop = None
    try:
        for prop in required_props:
            if prop in required_props_generated:
                republish_record[prop] = required_props_generated[prop]
            elif prop in validated_request:
                republish_record[prop] = validated_request[prop]
            elif prop == 'republish-expiration-date':
                republish_record[prop] = time_helper.format_time(pending_expiration_epoch)
            else:
                republish_record[prop] = subscription_record[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError('Missing required attribute: {0}'.format(prop))

    for prop in optional_props:
        if prop in validated_request:
            republish_record[prop] = validated_request[prop]

    return republish_record


def generate_event_dict(republish_record: dict, request_id: str, stage: str, pending_expiration_epoch: int,
                        schema_version: list) -> dict:
    event_dict = \
        {
            'event-id': request_id,
            'request-time': command_wrapper.get_request_time(),
            'event-name': event_names.SUBSCRIPTION_REPUBLISH_CREATE,
            'republish-id': request_id,
            'pending-expiration-epoch': pending_expiration_epoch,
            'event-version': event_version,
            'stage': stage,
            'subscription-id': republish_record['subscription-id'],
            'schema-version': schema_version
        }
    if 'collection-ids' in republish_record:
        event_dict['collection-ids'] = republish_record['collection-ids']
    if 'catalog-ids' in republish_record:
        event_dict['catalog-ids'] = republish_record['catalog-ids']
    if 'changeset-id' in republish_record:
        event_dict['changeset-id'] = republish_record['changeset-id']
    return event_dict


if __name__ == '__main__':
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import json
    import string

    logging.lastResort.setLevel(logging.DEBUG)

    os.environ['AWS_PROFILE'] = 'c-sand'

    ASSET_GROUP = 'feature-jek'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = '{}-DataLake-CollectionTable'.format(ASSET_GROUP)
    os.environ['CATALOG_DYNAMODB_TABLE'] = '{}-DataLake-CatalogTable'.format(ASSET_GROUP)
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = '{}-DataLake-SubscriptionTable'.format(ASSET_GROUP)
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = '{}-DataLake-EventStoreTable'.format(ASSET_GROUP)
    os.environ['OWNER_DYNAMODB_TABLE'] = '{}-DataLake-OwnerTable'.format(ASSET_GROUP)


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": generate_random_id(16),
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "stage": "LATEST",
                "http-method": "POST",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "collection-ids": [
                    "JohnK"
                ],
                'subscription-id': 128
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE SUBSCRIPTION COMMAND LAMBDA]")
