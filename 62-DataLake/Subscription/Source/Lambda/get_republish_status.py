import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commons import session_decorator
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_constants import republish_status
from lng_datalake_dal.republish_collection_table import RepublishCollectionTable
from lng_datalake_dal.republish_table import RepublishTable
from lng_datalake_commons.error_handling.error_handler import retryable_exceptions

__author__ = "Kiran G, John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Get environment variables
glue_job_max_concurrency = int(os.getenv("REPUBLISH_GLUE_JOB_MAX_CONCURRENCY", 3))  # defaulting for local testing


@session_decorator.lng_aws_session()
def lambda_handler(event, context):
    logger.info("Event: {0}".format(event))
    logger.info("Context: {0}".format(vars(context)))
    return get_republish_status(event["republish-id"])


def get_republish_status(republish_id: str) -> dict:
    current_republish_status = 'Waiting'
    try:
        if is_republish_id_oldest(republish_id):
            logger.debug("RepublishID: {0} is oldest".format(republish_id))
            collections_in_process = get_collections_in_process_count()
            logger.debug("Collections in process: {0}; Glue Job Max concurrency: {1}".format(
                collections_in_process, glue_job_max_concurrency))

            if collections_in_process < glue_job_max_concurrency:
                current_republish_status = 'Ready'
        else:
            logger.debug("RepublishID: {0} is NOT oldest pending".format(republish_id))
    except retryable_exceptions:
        logger.error("Encountered a retryable exception, returning status to continue Waiting")

    response_dict = {'Status': current_republish_status}
    logger.info("Republish Status response: {0}".format(response_dict))
    return response_dict


def is_republish_id_oldest(republish_id: str) -> bool:
    try:
        pending_republish_items = RepublishTable().get_all_items(
            FilterExpression="RepublishState = :pending_state",
            ExpressionAttributeValues={":pending_state": {"S": republish_status.PENDING}},
            ConsistentRead=True)
    except retryable_exceptions as e:
        logger.error(
            "Error checking for oldest republish in RepublishTable: {0}".format(e))
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to get item by republish-id. | Republish ID = {0} | Exception = {1}".format(republish_id, e))

    if pending_republish_items:
        pending_republish_items.sort(key=lambda x: x['republish-timestamp'])
        # Returns True if RepublishID in Pending state is oldest; False otherwise
        for idx, republish_item in enumerate(pending_republish_items):
            if republish_item["republish-id"] == republish_id:
                return True if idx == 0 else False

    # If no items returned or RepublishID not in the list of Pending items then raise exception
    raise TerminalErrorException("Republish ID {} not found in Pending state".format(republish_id))


def get_collections_in_process_count() -> int:
    try:
        collections_in_process = \
            RepublishCollectionTable().count_query_items(index='republish-collection-state-index',
                                                         KeyConditionExpression='RepublishState=:republish_state',
                                                         ExpressionAttributeValues={
                                                             ":republish_state": {
                                                                 "S": republish_status.PROCESSING}})

    except retryable_exceptions as e:
        logger.error(
            "Error counting the current Collections being republished in RepublishCollectionTable: {0}".format(e))
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to get count for collections in process.| Exception = {0}".format(e))

    return collections_in_process


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string

    logging.lastResort.setLevel(logging.DEBUG)

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['REPUBLISH_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-RepublishTable'
    os.environ['REPUBLISH_COLLECTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-RepublishCollectionTable'


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    input_event = \
        {
            "republish-id": "1234"
        }

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(input_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("Done get_republish_lambda")
