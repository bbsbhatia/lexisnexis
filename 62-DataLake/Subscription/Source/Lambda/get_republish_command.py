import json
import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InternalError
from lng_datalake_commands.exceptions import UnhandledException, NoSuchSubscription, SemanticError, NoSuchRepublish
from lng_datalake_commons import time_helper, session_decorator
from lng_datalake_constants import subscription_status
from lng_datalake_dal.republish_table import RepublishTable
from lng_datalake_dal.subscription_table import SubscriptionTable

__author__ = "John Morelock"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/GetRepublishCommand-RequestSchema.json',
                         'Schemas/GetRepublishCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return get_republish_command(event['request'])


def get_republish_command(request: dict):
    # Required parameters
    republish_id = request['republish-id']
    subscription_id = request['subscription-id']

    # Validate subscription exists and is in a valid state
    validate_subscription(subscription_id)

    republish_record = get_validated_republish_record(republish_id, subscription_id)

    # response json to be returned
    response_dict = generate_response(republish_record)

    return {"response-dict": response_dict}


def validate_subscription(subscription_id: int) -> None:
    """
    Validate subscription
    :param subscription_id: Subscription ID
    """
    try:
        response = SubscriptionTable().get_item({'subscription-id': subscription_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Subscription Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not response or response['subscription-state'] == subscription_status.TERMINATED:
        raise NoSuchSubscription("Invalid Subscription ID {0}".format(subscription_id),
                                 "Subscription ID does not exist")
    if response['subscription-state'] == subscription_status.PENDING:
        raise SemanticError('Subscription ID {} is in {} state'.format(response['subscription-id'],
                                                                       subscription_status.PENDING),
                            'Confirm subscription before getting republish')


def get_validated_republish_record(republish_id: str, subscription_id: int) -> dict:
    try:
        republish_record = RepublishTable().get_item({'republish-id': republish_id})
    except ClientError as e:
        raise InternalError("Unable to get item from Republish Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not republish_record:
        raise NoSuchRepublish('Republish ID {} does not exist'.format(republish_id),
                              'A valid Republish ID is required. Republish IDs expire after 30 days')

    if subscription_id != republish_record['subscription-id']:
        raise NoSuchRepublish(
            "Republish ID {0} does not exist in subscription ID {1}".format(republish_id, subscription_id),
            "Subscription ID must match republish's subscription ID value: {}".format(
                republish_record['subscription-id']))
    return republish_record


def generate_response(republish_record: dict) -> dict:
    """
    Generate get republish response to caller
    :param republish_record: Republish DynamoDB item
    :return: Repubish get item response dictionary
    """
    response_record = {}

    required_props = command_wrapper.get_required_response_schema_keys('republish-response')
    optional_props = command_wrapper.get_optional_response_schema_keys('republish-response')
    prop = None
    try:
        for prop in required_props:
            if prop == 'republish-expiration-date':
                response_record[prop] = time_helper.format_time(republish_record['pending-expiration-epoch'])
            else:
                response_record[prop] = republish_record[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError('Missing required attribute: {0}'.format(prop))

    for prop in optional_props:
        if prop in republish_record:
            response_record[prop] = republish_record[prop]

    return response_record


if __name__ == '__main__':
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext
    from datetime import datetime

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - GET REPUBLISH COMMAND LAMBDA]")

    ASSET_GROUP = 'feature-jek'

    os.environ['AWS_PROFILE'] = 'product-datalake-dev-wormholedeveloper'

    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = '{0}-DataLake-SubscriptionTable'.format(ASSET_GROUP)
    os.environ['REPUBLISH_DYNAMODB_TABLE'] = '{0}-DataLake-RepublishTable'.format(ASSET_GROUP)

    lambda_event = \
        {
            "context": {
                "client-request-id": "1234-request-id-wxyz",
                "client-request-time": str(int(datetime.now().timestamp() * 1000)),
                "client-id": "abcd-client-id-7890",
                "http-method": "GET",
                "stage": "LATEST",
                "api-key-id": "testApiKeyId"
            },
            "request": {
                "republish-id": "3d80e143-b548-11e9-867f-a14db7b44afcx",
                "subscription-id": 386
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    json_response = lambda_handler(lambda_event, MockLambdaContext())
    logger.debug(json.dumps(json_response, indent=4))
    logger.debug("[LOCAL RUN END - GET REPUBLISH COMMAND LAMBDA]")
