import logging
import os

from aws_xray_sdk.core import patch
from botocore.exceptions import ClientError
from lng_aws_clients import sqs
from lng_aws_clients.helpers import is_valid_arn, get_account_id_from_arn, get_resource_name_from_arn, \
    get_region_from_arn
from lng_datalake_commands import command_wrapper, owner_authorization
from lng_datalake_commands.exceptions import InternalError, InvalidRequestPropertyValue, SemanticError, \
    UnhandledException
from lng_datalake_commons import email_helper, session_decorator, time_helper
from lng_datalake_constants import event_names, subscription_status
from lng_datalake_dal.counter_table import CounterTable
from lng_datalake_dal.subscription_table import SubscriptionTable

from service_commons import subscription_command

__author__ = "Mark Seitter, Jonathan A. Mitchall, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Set environment variables
topic_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")

# Get environment variables
event_version = os.getenv("EVENT_VERSION", 1)


@session_decorator.lng_aws_session()
@command_wrapper.command('Schemas/CreateSubscriptionCommand-RequestSchema.json',
                         'Schemas/CreateSubscriptionCommand-ResponseSchema.json')
def lambda_handler(event, context):
    return create_subscription_command(event['request'],
                                       event['context']['client-request-id'],
                                       event['context']['stage'],
                                       event['context']['api-key-id'])


def create_subscription_command(request: dict, request_id: str, stage: str, api_key_id: str) -> dict:
    owner_id = owner_authorization.get_owner_by_api_key_id(api_key_id)['owner-id']

    validate_request(request)

    subscription_protocol = request['protocol']
    request['next-action'] = generate_next_action_message(subscription_protocol)

    if subscription_protocol != 'email':
        request['topic-arn'] = topic_arn
        if request['protocol'] == 'sqs':
            request['queue-url'] = sqs.build_queue_url(get_account_id_from_arn(request['endpoint']),
                                                       get_resource_name_from_arn(request['endpoint']),
                                                       region=get_region_from_arn(request['endpoint']))
            request['sqs-policy'] = generate_sqs_policy(request)

    # increments the counter table
    subscription_id = generate_subscription_id()

    # response json to be returned
    response_dict = generate_response_json(request, subscription_id)

    event_dict = generate_event_dict(response_dict, request, request_id, stage, owner_id)

    return {'response-dict': response_dict, 'event-dict': event_dict}


def validate_request(request: dict) -> None:
    subscription_endpoint = request['endpoint']
    subscription_protocol = request['protocol']
    subscription_filter = request.get('filter', {})

    subscription_command.validate_schema_version(request)

    # Validate the data is correct
    if subscription_protocol == "email":
        if not email_helper.is_valid_email_format(subscription_endpoint):
            raise InvalidRequestPropertyValue("Endpoint is invalid",
                                              "The email address provided is not a valid email format")
    # If not email then it's a queue
    else:
        if not is_valid_arn(subscription_endpoint, subscription_protocol):
            raise InvalidRequestPropertyValue("Endpoint is invalid",
                                              "The endpoint arn provided is not a valid arn format")
        if subscription_endpoint.endswith('.fifo'):
            raise InvalidRequestPropertyValue("Endpoint is invalid",
                                              "FIFO queues are currently not supported, use a standard queue instead")

    if not is_unique_subscription(subscription_endpoint):
        raise SemanticError("Subscription for endpoint already exists: {0}".format(subscription_endpoint))

    # Validate collection and catalog id's in filter if exist
    subscription_command.validate_filter(subscription_filter)

    # Deny because we do not send out no-op events for the v0 schema
    # TODO delete this when v0 schema is removed
    if "v0" in request['schema-version'] \
            and event_names.OBJECT_UPDATE_NO_CHANGE in subscription_filter.get('event-name', []):
        raise SemanticError("Filter is invalid for given event-version",
                            "v0 subscriptions do not allow filtering on Object::UpdateNoChange events")


def is_unique_subscription(subscription_endpoint: str) -> bool:
    try:
        response = SubscriptionTable().query_items(index='subscription-endpoint-index',
                                                   KeyConditionExpression='Endpoint=:subscription_key',
                                                   ExpressionAttributeValues={
                                                       ":subscription_key": {"S": subscription_endpoint}})
    except ClientError as e:
        raise InternalError("Unable to query items from Subscription Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    return True if not response else False


def generate_subscription_id() -> int:
    try:
        return CounterTable().update_counter("subscription")
    except ClientError as e:
        raise InternalError("Unable to update Counter Table", e)
    except Exception as ex:
        raise UnhandledException(ex)


def generate_next_action_message(protocol: str) -> str:
    if protocol == 'email':
        message = 'Check your email, there will be an email containing a URL that needs to be confirmed before ' \
                  'the subscription is live'
    else:
        message = 'Check your SQS queue for a message to confirm your subscription. ' \
                  'Please refer to the documentation for help here: ' \
                  'https://wiki.regn.net/wiki/DataLake_Hello_World#Subscriptions'

    return message


def generate_response_json(input_dict: dict, subscription_id: int) -> dict:
    response_dict = {}
    subscription_prop = {}
    completion_prop = {}

    required_props_generated = \
        {
            "subscription-state": subscription_status.PENDING,
            "subscription-id": subscription_id
        }

    required_props = command_wrapper.get_required_response_schema_keys('subscription-properties')
    optional_props = command_wrapper.get_optional_response_schema_keys('subscription-properties')

    prop = None
    try:
        for prop in required_props:
            if prop in required_props_generated:
                subscription_prop[prop] = required_props_generated[prop]
            else:
                subscription_prop[prop] = input_dict[prop]

        for prop in command_wrapper.get_required_response_schema_keys('completion-metadata'):
            # 3 days from now to auto-expire
            if prop == 'subscription-expiration-date':
                completion_prop[prop] = time_helper.format_time(
                    subscription_command.get_pending_expiration_epoch(days=3))
            else:
                completion_prop[prop] = input_dict[prop]
    except Exception as e:
        logger.error(e)
        raise InternalError('Missing required attribute: {0}'.format(prop))

    for prop in optional_props:
        if prop in input_dict:
            subscription_prop[prop] = input_dict[prop]

    for prop in command_wrapper.get_optional_response_schema_keys('completion-metadata'):
        if prop in input_dict:
            completion_prop[prop] = input_dict[prop]

    response_dict['subscription'] = subscription_prop
    response_dict['completion-metadata'] = completion_prop
    return response_dict


def generate_sqs_policy(request: dict) -> dict:
    policy_document = {
        "Version": "2008-10-17",
        "Id": "{}/SQSDefaultPolicy".format(request['endpoint']),
        "Statement": [
            {"Sid": "DataLakeSubscription",
             "Effect": "Allow",
             "Principal": "*",
             "Action": "SQS:SendMessage",
             "Resource": "{}".format(request['endpoint']),
             "Condition":
                 {
                     "ArnEquals":
                         {
                             "aws:SourceArn": topic_arn
                         }
                 }
             }
        ]
    }

    return policy_document


def generate_event_dict(response_dict: dict, request: dict, request_id: str, stage: str, owner_id: int) -> dict:
    event_dict = \
        {
            'subscription-name': request['subscription-name'],
            'event-id': request_id,
            'request-time': command_wrapper.get_request_time(),
            'event-name': event_names.SUBSCRIPTION_CREATE,
            'subscription-id': response_dict['subscription']['subscription-id'],
            'endpoint': request['endpoint'],
            'protocol': request['protocol'],
            'schema-version': request['schema-version'],
            'filter': request.get('filter', {}),
            'pending-expiration-epoch': subscription_command.get_pending_expiration_epoch(days=3),
            'event-version': event_version,
            'stage': stage,
            'owner-id': owner_id
        }
    return event_dict


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context
    import random
    import string

    logging.lastResort.setLevel(logging.DEBUG)

    os.environ['AWS_PROFILE'] = 'c-sand'
    os.environ['COLLECTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CollectionTable'
    os.environ['CATALOG_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CatalogTable'
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-SubscriptionTable'
    os.environ['EVENT_STORE_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-EventStoreTable'
    os.environ['COUNTER_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-CounterTable'
    os.environ['OWNER_DYNAMODB_TABLE'] = 'feature-kgg-DataLake-OwnerTable'
    os.environ[
        'SUBSCRIPTION_NOTIFICATION_TOPIC_ARN'] = 'arn:aws:sns:us-east-1:288044017584:feature-DataLake-' \
                                                 'SubscriptionNotificationTopic-SubscriptionNotificationTopic' \
                                                 '-P2T5BBR14HU4'

    topic_arn = os.getenv("SUBSCRIPTION_NOTIFICATION_TOPIC_ARN")


    def generate_random_id(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    lambda_event = \
        {
            "context": {
                "client-request-id": "dcce13c9-c84d-4a19-85ee-5ddb38956ad3",
                "client-request-time": "1569545041648",
                "client-type": "aws-apigateway",
                "stage": "LATEST",
                "client-id": "zsgp22ldcl",
                "http-method": "POST",
                "resource-id": "r6ai7239y7",
                "resource-path": "/",
                "api-key-id": "<SET ME>"
            },
            "request": {
                "subscription-name": "test",
                "protocol": "email",
                "endpoint": "test@test.com",
                "filter": {}
            }
        }

    command_wrapper.WORKING_DIRECTORY = os.path.dirname(__file__)

    mock_context = mock_lambda_context.MockLambdaContext()
    mock_context.aws_request_id = generate_random_id(16)
    lambda_response = lambda_handler(lambda_event, mock_context)

    logger.debug(json.dumps(lambda_response, indent=4))
    logger.debug("[LOCAL RUN END - CREATE SUBSCRIPTION COMMAND LAMBDA]")
