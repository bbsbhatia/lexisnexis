import logging
import orjson
import os
from inspect import getmodulename, stack

import lng_datalake_commons.error_handling.error_handler as error_handler
from aws_xray_sdk.core import patch
from lng_aws_clients import sns
from lng_datalake_commons import sns_extractor, validate, session_decorator
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException
from lng_datalake_commons.tracking import tracker
from lng_datalake_constants import event_names, event_handler_status
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.subscription_table import SubscriptionTable

from service_commons import subscription_event_handler

__author__ = "Maen Nanaa, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Error Handling variables
error_handler.lambda_name = getmodulename(stack()[0][1])

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)


@session_decorator.lng_aws_session()
@subscription_event_handler.queue_wrapper
@error_handler.handle
@tracker.track_event_handler(event_names.SUBSCRIPTION_UPDATE)
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))
    logger.info("Context: {0}".format(vars(context)))
    return update_subscription_event_handler(event, context.invoked_function_arn)


def update_subscription_event_handler(event, lambda_arn):
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.SUBSCRIPTION_UPDATE):
        raise TerminalErrorException(
            "Failed to extract event: {} or event-name does not match {}".format(event,
                                                                                 event_names.SUBSCRIPTION_UPDATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage does not match {}".format(event, lambda_arn))

    updated_item = map_event_store_to_subscription_table(message)

    # update SNS filter policy only if there is a new filter or schema version
    if 'filter' in message or 'schema-version' in message:
        set_subscription_filter(updated_item)

    update_subscription_table(updated_item)

    return event_handler_status.SUCCESS


def map_event_store_to_subscription_table(message):
    subscription_id = message["subscription-id"]
    try:
        existing_subscription = SubscriptionTable().get_item({'subscription-id': subscription_id})
        if not existing_subscription:
            raise TerminalErrorException('Can not update a non existent subscription {0} '.format(message))
    except error_handler.retryable_exceptions as e:
        raise e
    except TerminalErrorException as e:
        raise e
    except Exception as eError:
        raise TerminalErrorException('Can not update a non existent {} due to {}'.format(subscription_id, eError))

    # initialize_dict_with_required_keys will raise Terminal Error for key_error
    return_dict = initialize_dict_with_required_keys(message, existing_subscription)

    for key in SubscriptionTable().get_optional_schema_keys():
        # Because these keys are Optional it is still necessary to verify they existed before
        if key in message:
            return_dict[key] = message[key]
        elif key in existing_subscription:
            return_dict[key] = existing_subscription[key]

    return return_dict


def initialize_dict_with_required_keys(message, existing_subscription):
    return_dict = {}
    try:
        for key in SubscriptionTable().get_required_schema_keys():
            if key == "event-ids":
                return_dict[key] = SubscriptionTable().build_event_ids_list(existing_subscription, message['event-id'])
            else:
                return_dict[key] = message.get(key, existing_subscription[key])
    except KeyError as e:
        raise TerminalErrorException("Key = {0} does not exist in subscription {1}".format(e,
                                                                                           message['subscription-id']))

    return return_dict


def update_subscription_table(item):
    """
    Inserts Subscription event as an item into Subscription table
    :return: True or False
    :param item: Subscription Event item to be inserted
    """
    try:
        SubscriptionTable().update_item(item)
        logger.info("Insertion to Subscription table succeeded.")
    except SchemaValidationError as schema_validation_error:
        raise TerminalErrorException("Failed to insert item: {} into Subscription table due to SchemaValidationError "
                                     "from DAL. {}".format(item, schema_validation_error))
    except ConditionError as condition_error:
        raise TerminalErrorException("Failed to insert item {} into Subscription table due to ConditionError "
                                     "from DAL. {}".format(item, condition_error))
    except SchemaError as schema_error:
        raise TerminalErrorException("Failed to insert item {} into Subscription table due to SchemaError "
                                     "from DAL. {}".format(item, schema_error))
    except SchemaLoadError as schema_load_error:
        raise TerminalErrorException("Failed to insert item {} into Subscription table due to SchemaLoadError "
                                     "from DAL. {}".format(item, schema_load_error))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException(
            "Failed to insert item {} into Subscription table due to unknown reasons. {}".format(item, e))


def set_subscription_filter(updated_item: dict) -> None:
    # Add notification-type and schema-version to filter before it is added to the sns filter policy
    # copy the values out of this dictionary as we don't need to put notification-type in the db
    # in case of more than one schema version, the concatenation will be used: ["v0", "v1"] -> ["v0v1"]
    sns_filter_policy = updated_item['filter'].copy()
    sns_filter_policy['schema-version'] = [''.join(updated_item['schema-version'])]
    sns_filter_policy['notification-type'] = ['event', str(updated_item['subscription-id'])]
    try:
        sns.get_client().set_subscription_attributes(SubscriptionArn=updated_item['subscription-arn'],
                                                     AttributeName='FilterPolicy',
                                                     AttributeValue=orjson.dumps(sns_filter_policy).decode())
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException('Unknown error occurred when attempting to set subscription filter for '
                                     'subscription arn: {0} Error: {1}'.format(updated_item['subscription-arn'], e))


if __name__ == '__main__':
    import json
    from lng_datalake_testhelper import mock_lambda_context

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - UPDATE SUBSCRIPTION EVENT HANDLER LAMBDA]")

    os.environ['AWS_PROFILE'] = 'c-sand'

    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "ccs-sandbox-lambda-terminal-errors"
    os.environ["RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/SubscriptionEventHandler" \
                                    "-RetryQueue-G87IJGWHMFY9"
    os.environ['SUBSCRIPTION_DYNAMODB_TABLE'] = 'feature-DataLake-DataLakeDynamoTables-SubscriptionTable-17Q8TF74TGVBD'
    os.environ['TRACKING_DYNAMODB_TABLE'] = 'feature-DataLake-DataLakeDynamoTables-TrackingTable-3BRUJZODZGI'

    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")
    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")

    msg = {
        "event-name": event_names.SUBSCRIPTION_UPDATE,
        'subscription-id': 15,
        "event-id": "nChCxCvEOtcLfWMj",
        "request-time": "2017-11-13T10:16:08.829Z",
        "item-name": 'test_subscription-name',
        'filter': {"collection-id": ["14", "15"],
                   "event-name": ["Object::Create"]},
        "event-version": 1,
        "stage": "LATEST"
    }

    sample_event = {
        "Records": [{
            "Sns": {
                "Message": json.dumps(msg)
            }
        }]
    }

    mock_context = mock_lambda_context.MockLambdaContext()
    lambda_handler(sample_event, mock_context)
    logger.debug("[LOCAL RUN END - UPDATE SUBSCRIPTION EVENT HANDLER LAMBDA]")
