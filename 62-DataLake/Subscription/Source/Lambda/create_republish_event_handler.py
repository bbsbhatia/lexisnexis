import logging
import orjson
import os
from inspect import getmodulename, stack

from aws_xray_sdk.core import patch
from lng_aws_clients import session, stepfunctions
from lng_datalake_commons import session_decorator
from lng_datalake_commons import sns_extractor, validate
from lng_datalake_commons.error_handling import error_handler
from lng_datalake_commons.error_handling.exceptions import TerminalErrorException, \
    RetryEventException
from lng_datalake_constants import event_names, republish_status, event_handler_status
from lng_datalake_dal.catalog_collection_mapping_table import CatalogCollectionMappingTable
from lng_datalake_dal.changeset_collection_table import ChangesetCollectionTable
from lng_datalake_dal.exceptions import SchemaValidationError, ConditionError, SchemaError, SchemaLoadError
from lng_datalake_dal.republish_collection_table import RepublishCollectionTable
from lng_datalake_dal.republish_table import RepublishTable

from service_commons import subscription_event_handler

__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))

# Xray Trace Patching
if __name__ == '__main__':
    from aws_xray_sdk import global_sdk_config

    global_sdk_config.set_sdk_enabled(False)
libraries = ('botocore',)
patch(libraries)

# Error Handling variables
error_handler.lambda_name = getmodulename(stack()[0][1])

# Get environment variables
event_handler_version = os.getenv("EVENT_HANDLER_VERSION", 1)

timeout_delay = int(os.getenv('MAX_REMAINING_MILLIS', '90000'))
state_machine_arn = os.getenv('STATE_MACHINE_ARN')
object_store_table = os.getenv('OBJECT_STORE_DYNAMODB_TABLE')
changeset_object_table = os.getenv('CHANGESET_OBJECT_DYNAMODB_TABLE')
object_store_index_name = 'objectstore-collection-hash-and-state-index'
changeset_object_index_name = 'changeset-collection-hash-index'
catalog_collection_mapping_table = os.getenv('CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE')
subscription_notification_topic_arn = os.getenv('SUBSCRIPTION_NOTIFICATION_TOPIC_ARN')
republish_collection_table = os.getenv('REPUBLISH_COLLECTION_DYNAMODB_TABLE')
datalake_url = os.getenv('DATA_LAKE_URL')

get_remaining_time_in_millis = None


@session_decorator.lng_aws_session()
@subscription_event_handler.queue_wrapper
@error_handler.handle
def lambda_handler(event, context):
    logger.info("Event: {0}".format(orjson.dumps(event).decode()))
    logger.info("Context: {0}".format(vars(context)))
    return create_republish_event_handler(event, context.invoked_function_arn, context.get_remaining_time_in_millis)


def create_republish_event_handler(event, lambda_arn, _get_remaining_time_in_millis):
    message = sns_extractor.extract_sns_message(event)
    logger.info("Event Message: {0}".format(orjson.dumps(message).decode()))

    # Validity check because in a high load situation, the sns filter can take a minute to be created and invalid events
    # could flow through briefly on a new deploy
    if not validate.is_valid_event_message(message, event_names.SUBSCRIPTION_REPUBLISH_CREATE):
        raise TerminalErrorException(
            "Failed to extract event: {} "
            "or event-name doesnt match {}".format(event, event_names.SUBSCRIPTION_REPUBLISH_CREATE))

    if not validate.is_valid_event_version(message, event_handler_version):
        raise TerminalErrorException(
            "Failed to process event: {} event-version does not match {}".format(event, event_handler_version))

    if not validate.is_valid_event_stage(message, lambda_arn):
        raise TerminalErrorException(
            "Failed to process event: {} stage not match {}".format(event, lambda_arn))

    global get_remaining_time_in_millis
    get_remaining_time_in_millis = _get_remaining_time_in_millis

    pagination_token = message.get('additional-attributes', {}).get('pagination-token', None)
    collections = message.get('additional-attributes', {}).get('republish-collections', [])
    catalogs = message.get('additional-attributes', {}).get('republish-catalogs', [])
    inserted_collections = message.get('additional-attributes', {}).get('inserted-collections', 0)

    if 'additional-attributes' in message:
        republish_record = create_republish_record(message)
        set_republish_state(republish_record, republish_status.RETRY, republish_status.PENDING)

    else:
        catalogs = message.get('catalog-ids', [])
        collections = message.get('collection-ids', [])
        republish_record = insert_republish_record(message)

    if catalogs:
        inserted_collections = insert_collections_from_catalogs(catalogs, republish_record, pagination_token,
                                                                inserted_collections)
    elif collections:
        inserted_collections = insert_collections(collections, republish_record, inserted_collections)
    else:
        inserted_collections = insert_collections_from_changeset(message['changeset-id'], republish_record,
                                                                 pagination_token,
                                                                 inserted_collections)

    # Set the table name and index name values if a changeset-id is in the message to point towards
    #   changeset-object-table otherwise we want to go against the object-store-table
    table_name = changeset_object_table if 'changeset-id' in message else object_store_table
    index_name = changeset_object_index_name if 'changeset-id' in message else object_store_index_name

    # If there are collections for the state machine to publish start, otherwise set the republish to Completed
    if inserted_collections:
        start_state_machine(inserted_collections, republish_record['republish-id'],
                            message['schema-version'], table_name, index_name, message['request-time'])
    else:
        set_republish_state(republish_record, republish_status.PENDING, republish_status.COMPLETED)

    return event_handler_status.SUCCESS


def insert_republish_record(message: dict) -> dict:
    item = create_republish_record(message)

    try:
        RepublishTable().put_item(item)
        logger.info("Insertion to Republish table succeeded.")
    except (SchemaValidationError, ConditionError, SchemaError, SchemaLoadError) as e:
        raise TerminalErrorException(
            "Failed to insert item into Republish table. Item = {0} | Exception = {1}".format(item, e))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Failed to insert item into Republish table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(item, e))

    return item


def create_republish_record(message: dict) -> dict:
    item = {}
    try:
        for key in RepublishTable().get_required_schema_keys():
            if key == "republish-timestamp":
                item[key] = message["request-time"]
            elif key == 'republish-state':
                item[key] = republish_status.PENDING
            elif key == "event-ids":
                item[key] = RepublishTable().build_event_ids_list({}, message['event-id'])
            else:
                item[key] = message[key]
    except KeyError as e:
        raise TerminalErrorException("Required Key = {} does not exist in the message.".format(e))

    for key in RepublishTable().get_optional_schema_keys():
        if key in message:
            item[key] = message[key]

    return item


def insert_collections_from_catalogs(catalogs: list, republish_record: dict,
                                     pagination_token: str, inserted_collections: int) -> int:
    # Set variable in scope so we're not overwriting the pagination_token passed in as a parameter
    inner_pagination = pagination_token
    for i in range(len(catalogs)):
        while True:
            # update the indexes for the next retry in case of unprocessed collections id
            # take current catalog and pagination token to place in the retry message
            error_handler.set_message_additional_attribute('republish-catalogs', catalogs[i:])
            error_handler.set_message_additional_attribute('pagination-token', inner_pagination)
            error_handler.set_message_additional_attribute('inserted-collections', inserted_collections)
            try:
                collection_ids = get_collection_batch_from_catalog(catalogs[i], inner_pagination)
            except error_handler.retryable_exceptions as e:
                set_republish_state(republish_record, republish_status.PENDING, republish_status.RETRY)
                raise e

            new_index_add = insert_republish_collection_records(collection_ids, republish_record)
            if new_index_add < len(collection_ids):
                set_republish_state(republish_record, republish_status.PENDING, republish_status.RETRY)
                raise RetryEventException("Retry event to finish the update with the unprocessed catalog ids")

            inserted_collections += len(collection_ids)
            inner_pagination = CatalogCollectionMappingTable().get_pagination_token()
            if not inner_pagination:
                break
    return inserted_collections


def get_collection_batch_from_catalog(catalog: str, pagination_token: str) -> list:
    try:
        query_params = {'IndexName': "collection-by-catalog-id-index",
                        'KeyConditionExpression': 'CatalogID=:catalog_id',
                        'ExpressionAttributeValues': {
                            ":catalog_id": {
                                "S": catalog}},
                        'ProjectionExpression': "CollectionID",
                        'max_items': 25,
                        'pagination_token': pagination_token}
        collection_ids = [i['collection-id'] for i in CatalogCollectionMappingTable().query_items(**query_params)]
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException("General exception occurred when getting records from the "
                                     "CatalogCollectionMappingTable by catalog-id. Error: {}".format(error))
    return collection_ids


def insert_collections(collections: list, republish_record: dict, inserted_collections: int) -> int:
    collection_length = len(collections)
    new_index_add = insert_republish_collection_records(collections, republish_record)

    # update the indexes for the next retry in case of unprocessed collections id
    if new_index_add < collection_length:
        error_handler.set_message_additional_attribute('republish-collections', collections[new_index_add:])
        error_handler.set_message_additional_attribute('inserted-collections',
                                                       len(collections[:new_index_add]) + inserted_collections)
        set_republish_state(republish_record, republish_status.PENDING, republish_status.RETRY)
        raise RetryEventException("Retry event to finish the update with the unprocessed collection ids")

    return collection_length


def insert_collections_from_changeset(changeset_id: str, republish_record: dict,
                                      pagination_token: str, inserted_collections: int) -> int:
    inner_pagination = pagination_token
    while True:
        # take current pagination token and number of collections already inserted to place in the retry message
        error_handler.set_message_additional_attribute('pagination-token', inner_pagination)
        error_handler.set_message_additional_attribute('inserted-collections', inserted_collections)
        try:
            collection_ids = get_collection_batch_from_changeset(changeset_id, inner_pagination)
        except error_handler.retryable_exceptions as e:
            set_republish_state(republish_record, republish_status.PENDING, republish_status.RETRY)
            raise e
        new_index_add = insert_republish_collection_records(collection_ids, republish_record)
        if new_index_add < len(collection_ids):
            set_republish_state(republish_record, republish_status.PENDING, republish_status.RETRY)
            raise RetryEventException(
                "Retry event to finish the update with the unprocessed collection ids in changeset")
        inserted_collections += len(collection_ids)
        inner_pagination = ChangesetCollectionTable().get_pagination_token()
        if not inner_pagination:
            break
    return inserted_collections


def get_collection_batch_from_changeset(changeset_id: str, pagination_token: str) -> list:
    try:
        query_params = {'KeyConditionExpression': 'ChangesetID=:changeset_id',
                        'ExpressionAttributeValues': {
                            ":changeset_id": {
                                "S": changeset_id}},
                        'ProjectionExpression': "CollectionID",
                        'max_items': 25,
                        'pagination_token': pagination_token}
        collection_ids = [i['collection-id'] for i in ChangesetCollectionTable().query_items(**query_params)]
    except error_handler.retryable_exceptions as error:
        raise error
    except Exception as error:
        raise TerminalErrorException("General exception occurred when getting records from the "
                                     "ChangesetCollectionTable by changeset-id. Error: {}".format(error))
    return collection_ids


def insert_republish_collection_records(collections: list, republish_record: dict) -> int:
    collection_republish_records = []

    for collection in collections:
        collection_republish_records.append(
            create_republish_collection_table_record(collection, republish_record['subscription-id'],
                                                     republish_record['republish-id'],
                                                     republish_record['pending-expiration-epoch'],
                                                     republish_record.get('changeset-id', '')))

    new_index_add = republish_collection_batch_write(collection_republish_records)

    return new_index_add


def create_republish_collection_table_record(collection_id: str, subscription_id: int, republish_id: str,
                                             pending_expiration_epoch: int, changeset_id: str) -> dict:
    collection_republish_record = {
        'collection-id': collection_id,
        'republish-id': republish_id,
        'subscription-id': subscription_id,
        'pending-expiration-epoch': pending_expiration_epoch,
        'republish-state': republish_status.PENDING
    }

    if changeset_id:
        collection_republish_record['changeset-id'] = changeset_id
    return collection_republish_record


def republish_collection_batch_write(collections_to_add: list) -> int:
    """
    Use batch_write_items to put into the RepublishCollectionTable.

    :param collections_to_add: list of republish collections records
    :return: indexes of the first unprocessed elements in collections_to_add
    """

    global get_remaining_time_in_millis

    index_add = 0
    # DynamoDB BatchWriteItem has limit of 25 items per batch
    max_batch_size = 25
    while index_add < len(collections_to_add) and get_remaining_time_in_millis() > timeout_delay:
        add_batch_size = max_batch_size
        items_to_add = [coll for coll in
                        collections_to_add[index_add:index_add + add_batch_size]]
        add_batch_size = len(items_to_add)

        try:
            unprocessed_items = RepublishCollectionTable().batch_write_items(items_to_put=items_to_add)
            if unprocessed_items:
                break
            index_add += add_batch_size
        except error_handler.retryable_exceptions as e:
            logger.error(e)
            break
        except Exception as e:
            logger.error(e)
            raise TerminalErrorException("General Exception Occurred: {}".format(e))

    return index_add


def set_republish_state(republish_record: dict, current_state: str, update_state: str) -> None:
    republish_record['republish-state'] = update_state
    condition = 'attribute_exists(RepublishID) AND RepublishState=:state'
    try:
        RepublishTable().update_item(republish_record, condition=condition, expression_values={':state': current_state})
        logger.info("Updated {0} in Republish table to {1} succeeded.".format(republish_record['republish-id'],
                                                                              update_state))
    except (SchemaValidationError, ConditionError, SchemaError, SchemaLoadError) as e:
        raise TerminalErrorException(
            "Failed to insert item into Republish table. Item = {0} | Exception = {1}".format(republish_record, e))
    except error_handler.retryable_exceptions as e:
        raise e
    except Exception as e:
        raise TerminalErrorException("Failed to insert item into Republish table due to unknown reasons. "
                                     "Item = {0} | Exception = {1}".format(republish_record, e))


def start_state_machine(collection_count: int, republish_id: str, schema_version: list,
                        table_name: str, index_name: str, request_time: str) -> None:
    glue_event_message = {
        'republish-id': republish_id,
        'table-name': table_name,
        'index-name': index_name,
        'catalog-collection-mapping-table': catalog_collection_mapping_table,
        'republish-collection-table': republish_collection_table,
        'subscription-notification-topic-arn': subscription_notification_topic_arn,
        'datalake-url': datalake_url,
        'schema-version': ''.join(schema_version),
        'request-time': request_time
    }
    republish_job_params = {
        "republish-id": republish_id,
        "num-collections": collection_count,
        'event-message': orjson.dumps(glue_event_message).decode()
    }
    try:
        response = stepfunctions.get_client().start_execution(
            stateMachineArn=state_machine_arn,
            name='republish-job-{}'.format(republish_id),
            input=orjson.dumps(republish_job_params).decode()
        )
        logger.debug('Job {0} started with {1} collections'.format(response['executionArn'], collection_count))
    except error_handler.retryable_exceptions:
        raise
    except Exception as e:
        logger.error("Error starting step function {0} with Republish ID {1}".format(state_machine_arn, republish_id))
        raise TerminalErrorException("Failed to start republish state machine due to unknown reasons. "
                                     "RepublishID = {0} | StepFunctionArn = {1} | Exception = {2}".format(republish_id,
                                                                                                          state_machine_arn,
                                                                                                          e))


if __name__ == '__main__':
    import json
    import random
    import string
    from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

    logging.lastResort.setLevel(logging.DEBUG)
    logger.debug("[LOCAL RUN START - CREATE REPUBLISH EVENT HANDLER LAMBDA]")

    ASSET_GROUP = 'feature-jek'

    os.environ['AWS_PROFILE'] = 'c-sand'

    os.environ['REPUBLISH_COLLECTION_DYNAMODB_TABLE'] = \
        '{0}-DataLake-RepublishCollectionTable'.format(ASSET_GROUP)
    os.environ['REPUBLISH_DYNAMODB_TABLE'] = \
        '{0}-DataLake-RepublishTable'.format(ASSET_GROUP)
    os.environ['CATALOG_COLLECTION_MAPPING_DYNAMODB_TABLE'] = \
        '{0}-DataLake-CatalogCollectionMappingTable'.format(ASSET_GROUP)
    os.environ['CHANGESET_COLLECTION_DYNAMODB_TABLE'] = \
        '{0}-DataLake-ChangesetCollectionTable'.format(ASSET_GROUP)
    os.environ['CHANGESET_OBJECT_DYNAMODB_TABLE'] = \
        '{0}-DataLake-ChangesetObjectTable'.format(ASSET_GROUP)
    os.environ[
        "RETRY_QUEUE_URL"] = "https://sqs.us-east-1.amazonaws.com/288044017584/{}-DataLake-RetryQueues-RetryQueue-YAKX13ITDLSF".format(
        ASSET_GROUP)

    os.environ["S3_TERMINAL_ERRORS_BUCKET_NAME"] = "{}-lambda-terminal-errors".format(ASSET_GROUP)
    os.environ['MAPPING_DYNAMO_CATALOG_ID_INDEX'] = "collection-by-catalog-id-index"

    error_handler.terminal_errors_bucket = os.getenv("S3_TERMINAL_ERRORS_BUCKET_NAME")
    error_handler.retry_queue_url = os.getenv("RETRY_QUEUE_URL")

    session.set_session()


    def generate_random_id_as_string(n):
        return ''.join([random.choice(string.ascii_letters) for _ in range(n)])


    msg = {
        # "catalog-ids": ["JohnK"],
        # 'collection-ids': ['0001044', '0006994', '0006370', '0007843', '0002099', '0007271', '0002580', '0003357', '0005659', '0009031', '0005778', '0003463', '0000383', '0006542', '0002389', '0000944', '0003555', '0002115', '0000225', '0007548', '0005443', '0004239', '0008206', '0004093', '0009414', '0001044', '0006994', '0006370', '0007843', '0002099', '0007271', '0002580', '0003357', '0005659', '0009031', '0005778', '0003463', '0000383', '0006542', '0002389', '0000944', '0003555', '0002115', '0000225', '0007548', '0005443', '0004239', '0008206', '0004093', '0009414'],
        'changeset-id': 'jek-changeset',
        "subscription-id": 128,
        "republish-id": "TkJQHpKFNZQRxfFF123",
        "pending-expiration-epoch": 1574773342,
        "event-id": "TkJQHpKFNZQRxfFF123",
        "stage": "LATEST",
        "request-time": "2019-07-03T19:15:41.621Z",
        "event-name": "Subscription::RepublishCreate",
        "event-version": 1,
        "seen-count": 0,
        "schema-version": ['v1']
    }

    lambda_event = {
        "Records": [
            {
                "EventSource": "aws:sns",
                "EventVersion": "1.0",
                "EventSubscriptionArn": "arn:aws:sns:us-east-1:288044017584:JONTEST-EventDispatcher-EventDispatcherTopic-1147JNNQ4Q6I6:6a0eba68-1d60-4209-9a2b-9453f63f8dfc",
                "Sns": {
                    "Type": "Notification",
                    "MessageId": "7ee1a4ac-bb9d-597c-9b15-f72c648442ec",
                    "TopicArn": "arn:aws:sns:us-east-1:288044017584:JONTEST-EventDispatcher-EventDispatcherTopic-1147JNNQ4Q6I6",
                    "Subject": None,
                    "Message": json.dumps(msg),
                    "Timestamp": "2018-05-24T15:40:09.774Z",
                    "SignatureVersion": "1",
                    "Signature": "ZQ+okQmsZtlo7/8LC8dBC3DPmtjO3og+unqgSAz2K2Su3qGnSm6/e2kbcyuvX+0GpwRE5E7i8R3GnoVRwljNwAr6ADK3EDp1sICfnA97l0MkLR7iX+uhGe6bQY5VKLmCnRVMUF9064k8jegnGWiYbQhpCPrxv0zJA/HOxXqX+fZ73fdWm5wGaIy42mR+FtxjGEezzSTs98tDQqGfApapOsgvWdzgOnqKPZ5ocdhtTz7505EUGRik+rmPZyEANtw9I98NwQHOTHW4NuqG4szfhOdRAeGOb7PSHQqQQJrzuvLDQpj5aBMV84j3ZiJxrFrMHUyqigKW9Mrx9VHsRt0H9g==",
                    "SigningCertUrl": "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-eaea6120e66ea12e88dcd8bcbddca752.pem",
                    "UnsubscribeUrl": "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:288044017584:JONTEST-EventDispatcher-EventDispatcherTopic-1147JNNQ4Q6I6:6a0eba68-1d60-4209-9a2b-9453f63f8dfc",
                    "MessageAttributes": {}
                }
            }
        ]
    }

    lambda_handler(lambda_event, MockLambdaContext())
    logger.debug("[LOCAL RUN END - CREATE REPUBLISH EVENT HANDLER LAMBDA]")
