{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "This stack creates the get_republish_status lambda function and associated step function for the DataLake",
  "Outputs": {
    "FunctionName": {
      "Description": "Name of function",
      "Value": {
        "Ref": "LambdaFunction"
      }
    },
    "FunctionArn": {
      "Description": "Arn of function",
      "Value": {
        "Fn::GetAtt": [
          "LambdaFunction",
          "Arn"
        ]
      }
    },
    "RepublishEventsStateMachineArn": {
      "Value": {
        "Ref": "RepublishSubscriptionEventsStateMachine"
      }
    }
  },
  "Parameters": {
    "MemorySize": {
      "Description": "The size of memory in MB for lambda function, in multiple of 64, minimum 128, maximum 1536.",
      "Type": "Number",
      "Default": "128",
      "MinValue": "128",
      "MaxValue": "1536"
    },
    "Timeout": {
      "Description": "The timeout for lambda function stop executing in seconds.",
      "Type": "Number",
      "Default": "300",
      "MinValue": "1"
    },
    "BuildNumber": {
      "Description": "Jenkins Build Number",
      "Type": "String",
      "Default": "1"
    },
    "AssetID": {
      "Description": "Asset id of GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "62"
    },
    "AssetName": {
      "Description": "Short name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "DataLake"
    },
    "AssetAreaName": {
      "Description": "Short asset area name of the GLITz Asset(to be used in tags)",
      "Type": "String",
      "Default": "DataLake"
    },
    "AssetGroup": {
      "Description": "AssetGroup where this is deployed in",
      "Type": "String",
      "Default": "Global"
    },
    "LambdaBucket": {
      "Description": "S3 bucket of the lambda function",
      "Type": "String",
      "Default": "ccs-sandbox-lambda-deploys"
    },
    "LambdaS3Object": {
      "Description": "S3Key for the lambda function",
      "Type": "String",
      "Default": "DataLake/62-DataLake-Subscription.zip"
    },
    "LambdaHandler": {
      "Description": "Handler for the lambda function",
      "Type": "String",
      "Default": "get_republish_status.lambda_handler"
    },
    "LambdaFunctionVersionV1": {
      "Description": "Version for the Lambda Function v1 alias",
      "Type": "String",
      "Default": "$LATEST"
    },
    "S3StepFunctionLocation": {
      "Description": "S3 path where the Step Functions templates live. Must start with s3:// and NOT end with a /",
      "Type": "String",
      "Default": ""
    }
  },
  "Resources": {
    "CloudMetadata": {
      "Type": "Custom::CloudMetadata",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "AssetID": {
          "Ref": "AssetID"
        },
        "AssetGroup": {
          "Ref": "AssetGroup"
        },
        "AssetAreaName": {
          "Ref": "AssetAreaName"
        },
        "ExportsOnly": true,
        "Version": "2",
        "Filter": [
          "RepublishDynamoTable",
          "RepublishCollectionDynamoTable",
          "RepublishCollectionDynamoStateIndex",
          "GlueRepublishEventsProcessor"
        ],
        "LastUpdate": {
          "Ref": "BuildNumber"
        }
      }
    },
    "LambdaFunctionVersion": {
      "Type": "Custom::LambdaVersion",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "LambdaName": {
          "Ref": "LambdaFunction"
        },
        "BuildNumber": {
          "Ref": "BuildNumber"
        }
      }
    },
    "V1LambdaAlias": {
      "Type": "AWS::Lambda::Alias",
      "Properties": {
        "Description": "v1 version of LambdaVersioning Custom Resource",
        "FunctionName": {
          "Ref": "LambdaFunction"
        },
        "FunctionVersion": {
          "Ref": "LambdaFunctionVersionV1"
        },
        "Name": "v1"
      }
    },
    "LatestLambdaAlias": {
      "Type": "AWS::Lambda::Alias",
      "Properties": {
        "Description": "Latest version of the Lambda Function for API Stage Variable",
        "FunctionName": {
          "Ref": "LambdaFunction"
        },
        "FunctionVersion": "$LATEST",
        "Name": "LATEST"
      }
    },
    "LambdaRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": [
                  "lambda.amazonaws.com"
                ]
              },
              "Action": [
                "sts:AssumeRole"
              ]
            }
          ]
        },
        "ManagedPolicyArns": [
          "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
          "arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess"
        ],
        "Policies": [
          {
            "PolicyName": "AllowGlueGetJobAccess",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowGlueGetJob",
                  "Effect": "Allow",
                  "Action": [
                    "glue:GetJob"
                  ],
                  "Resource": "*"
                }
              ]
            }
          },
          {
            "PolicyName": "DataLakeDynamoLambdaAccess",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowDynamoRepublishTableAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:Scan"
                  ],
                  "Resource": {
                    "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.RepublishDynamoTable}"
                  }
                },
                {
                  "Sid": "AllowDynamoRepublishCollectionTableAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:Query"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.RepublishCollectionDynamoTable}/index/${CloudMetadata.asset.outputs.RepublishCollectionDynamoStateIndex}"
                    }
                  ]
                }
              ]
            }
          }
        ],
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "LambdaFunction": {
      "Type": "AWS::Lambda::Function",
      "Properties": {
        "FunctionName": {
          "Fn::Sub": "${AssetGroup}-subscription-GetRepublishStatus"
        },
        "Code": {
          "S3Bucket": {
            "Ref": "LambdaBucket"
          },
          "S3Key": {
            "Ref": "LambdaS3Object"
          }
        },
        "Description": "General Lambda Function",
        "Handler": {
          "Ref": "LambdaHandler"
        },
        "MemorySize": {
          "Ref": "MemorySize"
        },
        "Environment": {
          "Variables": {
            "REPUBLISH_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.RepublishDynamoTable"
              ]
            },
            "REPUBLISH_COLLECTION_DYNAMODB_TABLE": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.RepublishCollectionDynamoTable"
              ]
            },
            "GLUE_REPUBLISH_SUBSCRIPTION_JOB_NAME": {
              "Fn::GetAtt": [
                "CloudMetadata",
                "asset.outputs.GlueRepublishEventsProcessor"
              ]
            },
            "REPUBLISH_GLUE_JOB_MAX_CONCURRENCY": "50"
          }
        },
        "Role": {
          "Fn::GetAtt": [
            "LambdaRole",
            "Arn"
          ]
        },
        "Runtime": "python3.6",
        "Timeout": {
          "Ref": "Timeout"
        },
        "TracingConfig": {
          "Mode": "PassThrough"
        },
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "LambdaFunctionLogGroup": {
      "Type": "AWS::Logs::LogGroup",
      "Properties": {
        "LogGroupName": {
          "Fn::Sub": "/aws/lambda/${LambdaFunction}"
        },
        "RetentionInDays": "30"
      }
    },
    "ExecuteStateMachineLambdaRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": [
                  "states.amazonaws.com"
                ]
              },
              "Action": [
                "sts:AssumeRole"
              ]
            }
          ]
        },
        "Policies": [
          {
            "PolicyName": "V1StatesExecutionPolicy",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Action": [
                    "lambda:InvokeFunction"
                  ],
                  "Resource": {
                    "Ref": "V1LambdaAlias"
                  }
                }
              ]
            }
          },
          {
            "PolicyName": "LatestStatesExecutionPolicy",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Action": [
                    "lambda:InvokeFunction"
                  ],
                  "Resource": {
                    "Ref": "LatestLambdaAlias"
                  }
                }
              ]
            }
          },
          {
            "PolicyName": "GlueStartJobRun",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowGlueStartJobRun",
                  "Effect": "Allow",
                  "Action": [
                    "glue:StartJobRun",
                    "glue:GetJobRun"
                  ],
                  "Resource": "*"
                }
              ]
            }
          },
          {
            "PolicyName": "StateMachineDataLakeDynamoAccess",
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Sid": "AllowRepublishTableUpdateAccess",
                  "Effect": "Allow",
                  "Action": [
                    "dynamodb:UpdateItem"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${CloudMetadata.asset.outputs.RepublishDynamoTable}"
                    }
                  ]
                }
              ]
            }
          }
        ],
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "RepublishSubscriptionEventsStateMachine": {
      "Type": "AWS::StepFunctions::StateMachine",
      "Properties": {
        "StateMachineName": {
          "Fn::Sub": "${AssetGroup}-${AssetName}-RepublishEvents"
        },
        "DefinitionString": {
          "Fn::Transform": {
            "Name": "AWS::Include",
            "Parameters": {
              "Location": {
                "Fn::Sub": "${S3StepFunctionLocation}/RepublishSubscriptionEventsStateMachine.json"
              }
            }
          }
        },
        "RoleArn": {
          "Fn::GetAtt": [
            "ExecuteStateMachineLambdaRole",
            "Arn"
          ]
        },
        "Tags": [
          {
            "Key": "AssetID",
            "Value": {
              "Ref": "AssetID"
            }
          },
          {
            "Key": "AssetName",
            "Value": {
              "Ref": "AssetName"
            }
          },
          {
            "Key": "AssetAreaName",
            "Value": {
              "Ref": "AssetAreaName"
            }
          },
          {
            "Key": "AssetGroup",
            "Value": {
              "Ref": "AssetGroup"
            }
          }
        ]
      }
    },
    "CloudMetadataExport": {
      "Type": "Custom::CloudMetadataExport",
      "Properties": {
        "ServiceToken": {
          "Fn::Sub": "arn:aws:sns:${AWS::Region}:${AWS::AccountId}:CustomResourceGateway"
        },
        "AssetID": {
          "Ref": "AssetID"
        },
        "AssetGroup": {
          "Ref": "AssetGroup"
        },
        "AssetAreaName": {
          "Ref": "AssetAreaName"
        },
        "Exports": {
          "RepublishSubscriptionEventsStateMachineArn": {
            "Ref": "RepublishSubscriptionEventsStateMachine"
          }
        },
        "Version": "2"
      }
    }
  }
}