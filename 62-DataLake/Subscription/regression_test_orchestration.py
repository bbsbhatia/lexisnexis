"""
steps performed for Subscription Regression test:
    Read SQS ARN from Environment
    Create Owner, Collection and Catalog. Add Collection to Catalog
    Create Subscription with {filters: {"collection-id": Collection ID, "event-name": [Object::Create]}} and (-)
     test cases Authorize Subscription
    Create second Subscription to test Republish with only Collection IDs
    Get Subscription (+) and (-) test cases
    List Subscriptions (+) and (-) test cases
    Update Subscription {filters: {"collection-id": [Collection ID],
                                        "event-name": [Object::Create, Object::Update,
                                        Collection::Update, Collection::Resume, Collection::Suspend],
                                        "catalog-id": [Catalog ID]}}
    (get the subscription to validate that the filter was updated) and (-) test cases
    Go through the following, if unable to get notification for specified event reset the state of the resources
    and try again:
        Update Collection (get collection and verify that it was updated) and
            validate Collection::Update notification msg is sent
        Suspend Collection (get collection and verify its status is Suspended) and
            validate Collection::Suspend notification msg is sent
        Resume Collection (get collection and verify its status is Created) and
            validate Collection::Resume notification msg is sent
        Create Object in Collection (check datalake created the object with status code 200) and
            validate Object::Create notification msg is sent
        Update Object in Collection (get the object and verify it was updated) and
            validate Object::Update notification msg is sent
        Remove Object in Collection (get the object and verify it was removed with status code 422) and
            validate Object::Remove notification msg is sent

    ************************************ Republish Regression *************************************************
    Create Objects in Collections to Republish
    Create Subscription with {filters: {"collection-id": Collection ID, "event-name": [Object::Create]}} and (-)
     test cases Authorize Subscription
    Republish a list of Collections
        Receive messages
        Validate Objects
        Check for duplicates
    Republish a Catalog without a Collection
        Ensure no messages are sent
    Republish a Catalog with a Collection
        Receive messages
        Validate Objects
        Check for duplicates
    Republish (-) test cases

    Get Republish (+) and (-) test cases

    Remove Subscription (+) and (-) test cases

    Cleanup everything - note you can't delete owner even when collection is terminated,
        x-api-key and api-key-id = REMOVED
"""

import json
import logging
import os
import unittest
from datetime import datetime

import boto3
import requests
import time
from lng_aws_clients import sqs
from lng_datalake_client.Admin.remove_owner import remove_owner
from lng_datalake_client.Catalog.create_catalog import create_catalog
from lng_datalake_client.Catalog.get_catalog import get_catalog
from lng_datalake_client.Catalog.remove_catalog import remove_catalog
from lng_datalake_client.Catalog.update_catalog import update_catalog
from lng_datalake_client.Collection.create_collection import create_collection
from lng_datalake_client.Collection.get_collection import get_collection
from lng_datalake_client.Collection.remove_collection import remove_collection
from lng_datalake_client.Collection.update_collection import update_collection, suspend_collection, resume_collection
from lng_datalake_client.Object import create_object, update_object, remove_object
from lng_datalake_client.Object.describe_objects import describe_objects
from lng_datalake_client.Subscription.get_republish import get_republish
from lng_datalake_client.Subscription.get_subscription import get_subscription
from lng_datalake_client.Subscription.remove_subscription import remove_subscription
from lng_datalake_client.Utils import setup_utils, teardown_utils

__author__ = "Jose Molinet, John Konderla"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

logging.lastResort.setLevel(logging.DEBUG)
logger = logging.getLogger(__name__)
logger.setLevel(os.getenv("LOG_LEVEL", logging.DEBUG))


class RetryState:
    UPDATE_COLLECTION = 0
    SUSPEND_COLLECTION = 1
    RESUME_COLLECTION = 2
    FINISHED_COLLECTION = 3
    CREATE_OBJECT = 4
    UPDATE_OBJECT = 5
    REMOVE_OBJECT = 6
    END = 99


class TestSubscriptionOrchestration(unittest.TestCase):
    get_owner_loop_times = int(os.getenv('CREATE_OWNER_LOOP_TIMES', 5))
    get_object_loop_times = int(os.getenv('GET_OBJECT_LOOP_TIMES', 10))
    get_collection_loop_times = int(os.getenv('GET_COLLECTION_LOOP_TIMES', 10))
    get_catalog_loop_times = int(os.getenv('GET_CATALOG_LOOP_TIMES', 10))
    get_subscription_loop_times = int(os.getenv('GET_SUBSCRIPTION_LOOP_TIMES', 15))
    get_republish_loop_times = int(os.getenv('GET_REPUBLISH_LOOP_TIMES', 60))
    check_notification_loop_times = int(os.getenv('CHECK_NOTIFICATION_LOOP_TIMES', 10))
    confirm_subscription_loop_times = int(os.getenv('CONFIRM_SUBSCRIPTION_LOOP_TIMES', 30))
    check_notification_workflow_times = int(os.getenv('CHECK_NOTIFICATION_WORKFLOW_TIMES', 10))

    asset_id = int(os.getenv('ASSET_ID', 62))
    wait_time = int(os.getenv('WAIT_TIME', 5))
    sqs_url_republish_collection = os.getenv('SQS_URL_PUBLISH_COLLECTION',
                                             'https://sqs.us-east-1.amazonaws.com/284211348336/sep29a')
    sqs_url_publish = os.getenv("SQS_URL_PUBLISH",
                                'https://sqs.us-east-1.amazonaws.com/284211348336/sep29b')
    sqs_url_changesets = os.getenv("SQS_URL_CHANGESETS",
                                   'https://sqs.us-east-1.amazonaws.com/284211348336/sep29c')
    default_region = sqs_url_publish.split('.')[1]
    session = boto3.Session(region_name=default_region)
    sqs_client = session.client('sqs')
    collection_id_publish = None
    collection_id_with_catalog = None
    collection_id_no_catalog = None
    catalog_id_publish = None
    catalog_id_with_collections = None
    catalog_id_no_collections = None
    owner_id_valid_tests = None
    api_key_valid_tests = None
    owner_id_invalid_tests = None
    owner_api_key_invalid_tests = None
    subscription_id_publish = None

    # test both schema versions ('v0', 'v1') currently published
    schema_versions_list = ['v0', 'v1']
    schema_version = ''.join(schema_versions_list)

    @classmethod
    def setUpClass(cls):  # NOSONAR
        logger.info("### Setting up resources for {} ###".format(cls.__name__))

        # Create Owners---------------------------------------------------------------
        cls.owner_id_valid_tests, cls.api_key_valid_tests = cls.create_owner(
            "regression-subscription-1.{}".format(datetime.now().isoformat()),
            ["regression-subscription-1@lexisnexis.com"])
        # set owner-id and api-key in environment variables
        os.environ['OWNER-ID'] = str(cls.owner_id_valid_tests)
        os.environ['X-API-KEY'] = cls.api_key_valid_tests

        # create an owner for NotAuthorized test cases
        cls.owner_id_invalid_tests, cls.owner_api_key_invalid_tests = cls.create_owner(
            "regression-subscription-2.{}".format(datetime.now().isoformat()),
            ["regression-subscription-2@lexisnexis.com"])

        # Create Collection------------------------------------------------------------
        cls.collection_id_publish = cls.create_collection(object_expiration_hours=24)
        cls.collection_id_with_catalog = cls.create_collection()
        cls.collection_id_no_catalog = cls.create_collection()

        # Create Catalog---------------------------------------------------------------
        cls.catalog_id_publish = cls.create_catalog()
        cls.catalog_id_with_collections = cls.create_catalog()
        cls.catalog_id_no_collections = cls.create_catalog()

        # Update Catalog---------add collection to catalog-----------------------------
        cls.update_catalog(cls.catalog_id_with_collections, cls.collection_id_with_catalog)
        cls.update_catalog(cls.catalog_id_publish, cls.collection_id_publish)

        cls.republished_objects = sorted([cls.create_object(cls.collection_id_with_catalog),
                                          cls.create_object(cls.collection_id_no_catalog)],
                                         key=lambda k: k['collection-id'])

    @classmethod
    def tearDownClass(cls):  # NOSONAR
        logger.info("### Cleaning up resources for {} ###".format(cls.__name__))

        # Delete Collections----------------------------------------------------------------------
        for collection_id in (cls.collection_id_publish, cls.collection_id_with_catalog, cls.collection_id_no_catalog):
            logger.info("Deleting collection-id {}...".format(collection_id))
            collection_dict = {"collection-id": collection_id}

            logger.info("Suspending collection ...")
            suspend_collection(collection_dict)
            teardown_utils.validate_collection_state('Suspended', collection_dict, cls.__class__,
                                                     cls.get_collection_loop_times, cls.wait_time)

            logger.info("Terminating collection ...")
            remove_collection(collection_dict)
            teardown_utils.validate_collection_state('Terminated', collection_dict, cls.__class__,
                                                     cls.get_collection_loop_times, cls.wait_time)

        # Delete Subscriptions--------------------------------------------------------------------
        subscription_id_publish = os.getenv('SUBSCRIPTION_ID_PUBLISH', '')
        subscription_id_republish = os.getenv('SUBSCRIPTION_ID_COLLECTION_REPUBLISH', '')
        subscription_id_changesets = os.getenv('SUBSCRIPTION_ID_CHANGESETS', '')

        for subscription_id in (subscription_id_publish, subscription_id_republish, subscription_id_changesets):
            logger.info("Removing subscription {0}".format(subscription_id))
            remove_subscription({'subscription-id': subscription_id})

        # Delete Catalogs------------------------------------------------------------------------
        for catalog_id in (cls.catalog_id_no_collections, cls.catalog_id_publish, cls.catalog_id_with_collections):
            logger.info("Deleting catalog-id {}...".format(catalog_id))
            remove_catalog({"catalog-id": catalog_id})

        # Delete Owners--------------------------------------------------------------------------
        logger.info("Deleting owner-id {}...".format(cls.owner_id_valid_tests))
        remove_owner({"owner-id": cls.owner_id_valid_tests})

        logger.info("Deleting owner-id {}...".format(cls.owner_id_invalid_tests))
        os.environ['X-API-KEY'] = cls.owner_api_key_invalid_tests
        remove_owner({"owner-id": cls.owner_id_invalid_tests})

    @classmethod
    def create_owner(cls, name: str, emails: list) -> (str, str):
        body_input = {"owner-name": name, "email-distribution": emails}
        logger.info('Creating owner with name {0} and email {1}'.format(name, emails))
        owner = setup_utils.setup_create_owner(body_input, cls.get_owner_loop_times, cls.wait_time)['owner']
        logger.info("Successfully created owner-id {}.".format(owner['owner-id']))
        return owner['owner-id'], owner['api-key']

    @classmethod
    def create_collection(cls, object_expiration_hours: int = None) -> str:
        body_input = {'body': {"asset-id": cls.asset_id},
                      "collection-id": "regression-{}".format(str(datetime.timestamp(datetime.now()))[:-7])}
        if object_expiration_hours:
            body_input['body']['object-expiration-hours'] = object_expiration_hours
        logger.info('Creating collection with {} ...'.format(body_input))
        response = create_collection(body_input)

        if response.status_code != 202:
            if 'message' in response.json():
                logger.error("Unable to create collection. Message=[{}].".format(response.json()['message']))
            raise Exception("Collection was not created; status={}.".format(response.status_code))

        collection_id = response.json()['collection']['collection-id']

        logger.info("Validating collection-id {}...".format(collection_id))
        for _ in range(cls.get_collection_loop_times):
            time.sleep(cls.wait_time)
            response = get_collection({'collection-id': collection_id})
            if response.status_code == 200:
                logger.info('Successfully created collection-id {}.'.format(collection_id))
                return collection_id

        raise Exception("Collection was not created; status={}.".format(response.status_code))

    @classmethod
    def create_catalog(cls) -> str:
        logger.info('Creating catalog ...')
        response = create_catalog({'body': {"description": "Catalog for regression tests"}})

        if response.status_code != 202:
            if 'message' in response.json():
                logger.error("Unable to create catalog. Message=[{}].".format(response.json()['message']))
            raise Exception("Catalog was not created; status={}.".format(response.status_code))

        catalog_id = response.json()['catalog']['catalog-id']

        logger.info("Validating catalog-id {}...".format(catalog_id))
        for _ in range(cls.get_catalog_loop_times):
            time.sleep(cls.wait_time)
            response = get_catalog({'catalog-id': catalog_id})
            if response.status_code == 200:
                logger.info('Successfully created catalog-id {}.'.format(catalog_id))
                return catalog_id

        raise Exception("Catalog was not created; status={}.".format(response.status_code))

    @classmethod
    def update_catalog(cls, catalog_id: str, collection_id: str) -> None:
        update_input = {
            'body': {"patch-operations": [{"op": "add", "path": "/collection-ids", "value": [collection_id]}]},
            "catalog-id": catalog_id
        }
        logger.info('Updating catalog-id {} with collection-id {} ...'.format(catalog_id, collection_id))

        response = update_catalog(update_input)

        if response.status_code != 202:
            if 'message' in response.json():
                logger.error("Unable to update catalog. Message=[{}].".format(response.json()['message']))
            raise Exception("Catalog was not updated; status={}.".format(response.status_code))

        logger.info("Validating update of catalog-id {}...".format(catalog_id))
        for _ in range(cls.get_catalog_loop_times):
            time.sleep(cls.wait_time)
            response = get_collection({'collection-id': collection_id})
            if response.status_code == 200:
                catalog_ids = response.json()['collection'].get('catalog-ids')
                if catalog_ids and catalog_ids[0] == catalog_id:
                    logger.info('Successfully updated catalog-id {}.'.format(catalog_id))
                    return

        raise Exception("Catalog was not updated; status={}.".format(response.status_code))

    def test_orchestration(self):
        logger.info("### Running Subscription Regression tests ###")

        self.validate_publish()
        self.validate_republish()
        self.remove_subscription(self.subscription_id_publish)

    def validate_publish(self):
        self.create_subscription()
        accepted_publish_response = self.accept_create_subscription(self.subscription_id_publish, self.sqs_url_publish)
        accepted_collection_republish_response = self.accept_create_subscription(
            self.subscription_id_collection_republish,
            self.sqs_url_republish_collection)
        accepted_changesets_subscription_response = self.accept_create_subscription(
            self.subscription_id_changests,
            self.sqs_url_changesets)
        get_response = self.get_subscription(accepted_publish_response)
        self.list_subscriptions(
            [get_response, accepted_collection_republish_response, accepted_changesets_subscription_response])
        self.update_subscription(self.subscription_id_publish)

        successful_notifications = False
        retry_state = RetryState.UPDATE_COLLECTION
        new_asset_id = 1
        for i in range(self.check_notification_workflow_times):
            logger.info("Iteration {} of {} to check notifications".format(i, self.check_notification_workflow_times))
            retry_state, new_asset_id = self.validate_collection_notifications(retry_state, new_asset_id)
            if retry_state < RetryState.FINISHED_COLLECTION:
                continue
            retry_state = self.validate_object_notification(retry_state)
            if retry_state == RetryState.END:
                successful_notifications = True
                break
        self.assertTrue(successful_notifications, msg='Unable to successfully test the specified notifications')

    def validate_collection_notifications(self, retry_state: int, new_asset_id: int) -> (int, int):
        from Tests.Regression.regression_validate_notifications import TestValidateCollectionNotifications
        validator = TestValidateCollectionNotifications()

        if retry_state <= RetryState.UPDATE_COLLECTION:
            event_name = 'Collection::Update'
            updated_collection = self.update_collection(self.collection_id_publish, new_asset_id)
            notification = self.get_collection_notification(event_name, self.collection_id_publish,
                                                            self.sqs_url_publish)
            if not notification:
                logger.error("Update notification for Collection ID {} was not sent. "
                             "Retrying ...".format(self.collection_id_publish))
                return RetryState.UPDATE_COLLECTION, 1 if new_asset_id == 62 else 62

            self.validate_message(validator, event_name, notification, updated_collection, self.schema_version)

        if retry_state <= RetryState.SUSPEND_COLLECTION:
            event_name = 'Collection::Suspend'
            suspended_collection = self.suspend_collection(self.collection_id_publish)
            notification = self.get_collection_notification(event_name, self.collection_id_publish,
                                                            self.sqs_url_publish)
            if not notification:
                logger.error("Suspend notification for Collection ID {} was not sent. "
                             "Retrying ...".format(self.collection_id_publish))
                self.resume_collection(self.collection_id_publish)
                self.get_collection_notification('Collection::Resume', self.collection_id_publish, self.sqs_url_publish)
                return RetryState.SUSPEND_COLLECTION, new_asset_id

            self.validate_message(validator, event_name, notification, suspended_collection, self.schema_version)

        if retry_state <= RetryState.RESUME_COLLECTION:
            event_name = 'Collection::Resume'
            resumed_collection = self.resume_collection(self.collection_id_publish)
            notification = self.get_collection_notification(event_name, self.collection_id_publish,
                                                            self.sqs_url_publish)
            if not notification:
                logger.error("Resume notification for Collection ID {} was not sent. "
                             "Retrying ...".format(self.collection_id_publish))
                self.suspend_collection(self.collection_id_publish)
                self.get_collection_notification('Collection::Suspend', self.collection_id_publish,
                                                 self.sqs_url_publish)
                return RetryState.RESUME_COLLECTION, new_asset_id

            self.validate_message(validator, event_name, notification, resumed_collection, self.schema_version)

        return RetryState.FINISHED_COLLECTION, new_asset_id

    def validate_object_notification(self, retry_state: int) -> int:
        from Tests.Regression.regression_validate_notifications import TestValidateObjectNotifications
        validator = TestValidateObjectNotifications()

        if retry_state <= RetryState.CREATE_OBJECT:
            event_name = 'Object::Create'
            created_object = self.create_object(self.collection_id_publish)
            self.object_id = created_object["object-id"]
            notifications = self.get_object_notification(event_name, [created_object],
                                                         self.sqs_url_publish)
            if not notifications:
                logger.error("Create notification for Object ID {} was not sent. Retrying ...".format(self.object_id))
                removed_object = self.remove_object(self.collection_id_publish, self.object_id)
                self.get_object_notification('Object::Remove', [removed_object],
                                             self.sqs_url_publish)
                return RetryState.CREATE_OBJECT

            self.validate_message(validator, event_name, notifications[0], created_object, self.schema_version)

        if retry_state <= RetryState.UPDATE_OBJECT:
            event_name = 'Object::Update'
            updated_object = self.update_object(self.collection_id_publish, self.object_id)
            notifications = self.get_object_notification(event_name, [updated_object],
                                                         self.sqs_url_publish)
            if not notifications:
                logger.error("Update notification for Object ID {} was not sent. Retrying ...".format(self.object_id))
                return RetryState.UPDATE_OBJECT

            self.validate_message(validator, event_name, notifications[0], updated_object, self.schema_version)

        if retry_state <= RetryState.REMOVE_OBJECT:
            event_name = 'Object::Remove'
            removed_object = self.remove_object(self.collection_id_publish, self.object_id)
            notifications = self.get_object_notification(event_name, [removed_object],
                                                         self.sqs_url_publish)
            if not notifications:
                logger.error("Remove notification for Object ID {} was not sent. Retrying ...".format(self.object_id))
                recreated_object = self.create_object(self.collection_id_publish)
                self.object_id = recreated_object['object-id']
                self.get_object_notification('Object::Create', [recreated_object],
                                             self.sqs_url_publish)
                return RetryState.REMOVE_OBJECT

            self.validate_message(validator, event_name, notifications[0], removed_object, self.schema_version)
            # no more retries are needed if the last step in notification validation is successful
            return RetryState.END

        return retry_state

    def suspend_collection(self, collection_id: str) -> dict:
        logger.info('Suspending collection: {}...'.format(collection_id))
        suspend_collection({"collection-id": collection_id})

        response = {}
        for _ in range(self.get_collection_loop_times):
            time.sleep(self.wait_time)
            response = get_collection({'collection-id': collection_id})
            if response.status_code == 200 and response.json()['collection']['collection-state'] == 'Suspended':
                logger.info('Successfully suspended collection-id: {}'.format(collection_id))
                return response.json()['collection']

        raise Exception("Collection was not suspended; status={}.".format(response.status_code))

    def resume_collection(self, collection_id: str) -> dict:
        logger.info('Resuming collection: {}...'.format(collection_id))
        resume_collection({"collection-id": collection_id})

        response = {}
        for _ in range(self.get_collection_loop_times):
            time.sleep(self.wait_time)
            response = get_collection({'collection-id': collection_id})
            if response.status_code == 200 and response.json()['collection']['collection-state'] == 'Created':
                logger.info('Successfully resumed collection-id: {}'.format(collection_id))
                return response.json()['collection']

        raise Exception("Collection was not resumed; status={}.".format(response.status_code))

    def update_collection(self, collection_id: str, new_asset_id: int) -> dict:
        sub_info = {
            "collection-id": collection_id,
            "body": {
                "patch-operations": [{
                    "op": "replace",
                    "path": "/asset-id",
                    "value": new_asset_id
                }]
            }
        }
        logger.info('Updating collection: {}...'.format(collection_id))
        update_collection(sub_info)

        response = {}
        for _ in range(self.get_collection_loop_times):
            time.sleep(self.wait_time)
            response = get_collection({'collection-id': collection_id})
            if response.status_code == 200 and response.json()['collection']['asset-id'] == new_asset_id:
                logger.info('Successfully updated collection-id: {}'.format(collection_id))
                return response.json()['collection']

        raise Exception("Collection was not updated; status={}.".format(response.status_code))

    def create_subscription(self) -> None:
        logger.info("### Running create subscription tests ###")

        # Create one subscription to test events and catalog republish
        sub_info = {
            "subscription-name": 'regression_subscription_publish_{}'.format(datetime.now().isoformat()),
            "protocol": "sqs",
            "endpoint": self.build_sqs_arn(self.sqs_url_publish),
            "schema-version": self.schema_versions_list,
            "filter":
                {
                    "collection-id": [self.collection_id_publish],
                    "event-name": ["Object::Create", "Object::Update", "Object::Remove", "Collection::Update",
                                   "Collection::Resume", "Collection::Suspend"]

                }
        }
        from Tests.Regression.regression_create_subscription import TestCreateSubscription
        obj_create_subscription = TestCreateSubscription()
        subscription = self.create_subscription_resource(obj_create_subscription, sub_info)

        self.subscription_id_publish = subscription['subscription-id']

        os.environ['SUBSCRIPTION_ID_PUBLISH'] = str(self.subscription_id_publish)

        # Create a second subscription to test republish for collections only
        republish_collection_sub_info = {
            "subscription-name": 'regression_subscription_publish_{}'.format(datetime.now().isoformat()),
            "protocol": "sqs",
            "endpoint": self.build_sqs_arn(self.sqs_url_republish_collection),
            "schema-version": self.schema_versions_list,
            "filter":
                {
                    "collection-id": [self.collection_id_with_catalog,
                                      self.collection_id_no_catalog],
                    "event-name": ["Object::Create"]

                }
        }
        subscription_collection = self.create_subscription_resource(obj_create_subscription,
                                                                    republish_collection_sub_info)
        self.subscription_id_collection_republish = subscription_collection['subscription-id']

        os.environ['SUBSCRIPTION_ID_COLLECTION_REPUBLISH'] = str(self.subscription_id_collection_republish)

        # Create third subscription to test changesets
        sub_changesets_info = {
            "subscription-name": 'regression_subscription_publish_{}'.format(datetime.now().isoformat()),
            "protocol": "sqs",
            "endpoint": self.build_sqs_arn(self.sqs_url_changesets),
            "schema-version": self.schema_versions_list,
            "filter":
                {
                    "collection-id": [self.collection_id_publish],
                    "event-name": ["Republish::ObjectCreate", "Republish::ObjectUpdate", "Republish::ObjectRemove",
                                   "Changeset::Close"]

                }
        }
        subscription_changesets = self.create_subscription_resource(obj_create_subscription, sub_changesets_info)

        self.subscription_id_changests = subscription_changesets['subscription-id']

        os.environ['SUBSCRIPTION_ID_CHANGESETS'] = str(self.subscription_id_changests)

        # - Invalid create subscription tests
        logger.info('Moving to invalid test cases for create subscription...')

        obj_create_subscription.test_create_subscription_invalid_email()
        logger.info("Successfully tested create subscription: Invalid email")

        obj_create_subscription.test_create_subscription_invalid_sqs_arn()
        logger.info("Successfully tested create subscription: Invalid sqs arn")

        obj_create_subscription.test_create_subscription_invalid_schema_version()
        logger.info("Successfully tested create subscription: Invalid schema version")

        obj_create_subscription.test_create_subscription_invalid_x_api_key()
        logger.info("Successfully tested create subscription: Invalid x-api-key")

        obj_create_subscription.test_create_subscription_invalid_content_type()
        logger.info("Successfully tested create subscription: Invalid content type")

        obj_create_subscription.test_create_subscription_invalid_collection_id()
        logger.info("Successfully tested create subscription: Invalid collection id")

        obj_create_subscription.test_z_create_subscription_duplicate(subscription)
        logger.info("Successfully tested create subscription: duplicate subscription endpoint")

        # - update and remove tests for subscription with pending status
        from Tests.Regression.regression_update_subscription import TestUpdateSubscription
        TestUpdateSubscription().test_update_subscription_pending_subscription(subscription)
        logger.info("Successfully tested update subscription: subscription with pending state")

        from Tests.Regression.regression_remove_subscription import TestRemoveSubscription
        TestRemoveSubscription().test_remove_subscription_pending_subscription(subscription)
        logger.info("Successfully tested remove subscription: subscription with pending state")

        # - create republish test with subscription in Pending state
        from Tests.Regression.regression_create_republish import TestCreateRepublish
        TestCreateRepublish().test_create_republish_subscription_pending(
            {"subscription-id": self.subscription_id_publish})
        logger.info("Successfully tested create republish: Subscription in Pending state")

    def create_subscription_resource(self, obj_create_subscription, sub_info: dict) -> dict:
        # + Valid created subscription to use in future tests.
        logger.info('Creating a subscription with endpoint: {}'.format(sub_info['endpoint']))
        create_response = obj_create_subscription.test_create_subscription(sub_info)
        subscription = create_response['subscription']

        subscription_id_publish = subscription['subscription-id']

        self.validate_subscription_state(subscription_id_publish, 'Pending')
        logger.info('Successfully created subscription-id: {}'.format(subscription_id_publish))
        return subscription

    @staticmethod
    def build_sqs_arn(sqs_url: str) -> str:
        _, region, _, account = sqs_url.split('.')
        _, account_id, queue_name = account.split('/')
        arn = sqs.build_arn(region, account_id, queue_name)
        return arn

    @staticmethod
    def get_subscription(subscription) -> dict:
        logger.info("### Running get subscription tests ###")

        # + Valid get subscription test.
        logger.info('Getting subscription-id: {}'.format(subscription['subscription-id']))
        from Tests.Regression.regression_get_subscription import TestGetSubscription
        obj_get_subscription = TestGetSubscription()
        response = obj_get_subscription.test_get_subscription(subscription)
        logger.info("Successfully tested get subscription-id: {}".format(subscription['subscription-id']))

        # - Invalid get subscription tests
        logger.info('Moving to invalid test cases for get subscription...')

        obj_get_subscription.test_get_subscription_invalid_subscription_id()
        logger.info("Successfully tested get subscription: invalid subscription id")

        obj_get_subscription.test_get_subscription_invalid_x_api_key()
        logger.info("Successfully tested get subscription: invalid x-api-key")

        obj_get_subscription.test_get_subscription_invalid_content_type()
        logger.info("Successfully tested get subscription: invalid content type")
        return response

    def list_subscriptions(self, subscriptions: list) -> None:
        logger.info("### Running list subscriptions tests ###")
        from Tests.Regression.regression_list_subscriptions import TestListSubscriptions
        test_list_subscriptions = TestListSubscriptions()

        # + Valid list subscriptions tests
        test_list_subscriptions.test_list_subscriptions_all_results()
        logger.info("Successfully tested list subscriptions: all results")
        test_list_subscriptions.test_list_subscriptions_by_owner_id(subscriptions, self.owner_id_valid_tests)
        logger.info("Successfully tested list subscriptions: all results by owner id")
        test_list_subscriptions.test_list_subscriptions_pagination()
        logger.info("Successfully tested list subscriptions: results by pagination")
        test_list_subscriptions.test_list_subscriptions_invalid_owner_id()
        logger.info("Successfully tested list subscriptions: invalid owner id")

        # - Invalid list subscriptions tests
        logger.info('Moving to invalid test cases for list subscriptions...')
        test_list_subscriptions.test_list_subscriptions_invalid_max_items()
        logger.info("Successfully tested list subscriptions: invalid max items")
        test_list_subscriptions.test_list_subscriptions_changing_max_items()
        logger.info("Successfully tested list subscriptions: changing max items")
        test_list_subscriptions.test_list_subscriptions_invalid_x_api_key()
        logger.info("Successfully tested list subscriptions: invalid x-api-key")
        test_list_subscriptions.test_list_subscriptions_invalid_content_type()
        logger.info("Successfully tested list subscriptions: invalid content type")

    def update_subscription(self, subscription_id: int) -> None:
        logger.info("### Running update subscription tests ###")

        sub_inf = {
            "subscription-id": subscription_id,
            "subscription-name": 'regression_subscriptions_update{}'.format(datetime.now().isoformat()),
            "schema-version": self.schema_versions_list,
            "filter": {
                "collection-id": [self.collection_id_publish, self.collection_id_with_catalog,
                                  self.collection_id_no_catalog],
                "event-name": ["Object::Create", "Object::Update", "Object::Remove", "Collection::Update",
                               "Collection::Resume", "Collection::Suspend"],
                "catalog-id": [self.catalog_id_publish, self.catalog_id_with_collections]
            }
        }
        # + Valid update subscription test.
        logger.info('Updating subscription {} name and filter policy'.format(subscription_id))
        from Tests.Regression.regression_update_subscription import TestUpdateSubscription
        obj_update_subscription = TestUpdateSubscription()
        response = obj_update_subscription.test_update_subscription(sub_inf)

        self.validate_subscription_update(subscription_id, sub_inf)
        logger.info('Successfully updated subscription-id: {}'.format(response['subscription-id']))

        # - Invalid update subscription tests
        logger.info('Moving to invalid test cases for update subscription...')

        obj_update_subscription.test_update_subscription_invalid_subscription_id()
        logger.info("Successfully tested update subscription: Invalid subscription id")

        obj_update_subscription.test_update_subscription_no_data_changed(response)
        logger.info("Successfully tested update subscription: no data changed")

        obj_update_subscription.test_update_subscription_invalid_schema_version(
            {"subscription-id": response['subscription-id'], "schema-version": ["v2"]})
        logger.info("Successfully tested update subscription: invalid schema version")

        obj_update_subscription.test_update_subscription_invalid_op(
            {"subscription-id": response['subscription-id']})
        logger.info("Successfully tested update subscription: invalid patch op")

        obj_update_subscription.test_update_subscription_invalid_path(
            {"subscription-id": response['subscription-id']})
        logger.info("Successfully tested update subscription: invalid patch path")

        obj_update_subscription.test_update_subscription_invalid_path(
            {"subscription-id": response['subscription-id']})
        logger.info("Successfully tested update subscription: invalid patch path")

        obj_update_subscription.test_update_subscription_invalid_value(
            {"subscription-id": response['subscription-id']})
        logger.info("Successfully tested update subscription: invalid patch value")

        obj_update_subscription.test_update_subscription_invalid_x_api_key()
        logger.info("Successfully tested update subscription: invalid x-api-key")

        obj_update_subscription.test_update_subscription_incorrect_x_api_key(sub_inf, self.owner_api_key_invalid_tests,
                                                                             self.owner_id_valid_tests)
        logger.info("Successfully tested update subscription: NotAuthorized Owner")

        obj_update_subscription.test_update_subscription_invalid_content_type()
        logger.info("Successfully tested update subscription: invalid content type")

    def remove_subscription(self, subscription_id: int) -> None:
        logger.info("### Running remove subscription tests ###")

        sub_info = \
            {
                "subscription-id": subscription_id
            }
        from Tests.Regression.regression_remove_subscription import TestRemoveSubscription
        obj_remove_subscription = TestRemoveSubscription()

        # - Invalid remove subscription tests
        logger.info('Invalid test cases for remove subscription...')

        obj_remove_subscription.test_remove_subscription_invalid_subscription_id()
        logger.info("Successfully tested remove subscription: Invalid subscription id")

        obj_remove_subscription.test_remove_subscription_invalid_x_api_key(sub_info)
        logger.info("Successfully tested remove subscription: invalid x-api-key")

        obj_remove_subscription.test_remove_subscription_incorrect_x_api_key(sub_info, self.owner_api_key_invalid_tests,
                                                                             self.owner_id_valid_tests)
        logger.info("Successfully tested remove subscription: NotAuthorized Owner")

        obj_remove_subscription.test_remove_subscription_invalid_content_type(sub_info)
        logger.info("Successfully tested remove subscription: invalid content type")

        # + Valid remove subscription test.
        logger.info('Removing subscription-id: {}'.format(sub_info['subscription-id']))
        obj_remove_subscription.test_remove_subscription(sub_info)

        response = {}
        for _ in range(self.get_subscription_loop_times):
            time.sleep(self.wait_time)
            response = get_subscription(sub_info)
            if response.status_code == 404:
                logger.info('Successfully removed subscription-id: {}'.format(subscription_id))
                return

        raise Exception("Subscription was not removed; status={}.".format(response.status_code))

    def validate_republish(self):

        created_republish = self.create_republish()

        self.get_republish(created_republish)

    def create_republish(self) -> dict:
        logger.info("### Running create Republish tests ###")

        # + create republish with collection ids
        from Tests.Regression.regression_create_republish import TestCreateRepublish
        obj_create_republish = TestCreateRepublish()

        catalog_publish = {
            'subscription-id': self.subscription_id_publish,
            'catalog-ids': [self.catalog_id_with_collections]
        }

        self.create_and_validate_republish(obj_create_republish, catalog_publish,
                                           self.republished_objects[:-1], self.sqs_url_publish)

        logger.info('Successfully tested catalog republish with collections')

        input_dict_catalog_no_collections = {"subscription-id": self.subscription_id_publish,
                                             "catalog-ids": [self.catalog_id_no_collections]}

        logger.info('Starting republish for catalog without collections: {}'.format(self.catalog_id_no_collections))
        self.create_and_validate_republish(obj_create_republish, input_dict_catalog_no_collections,
                                           [], self.sqs_url_publish)
        logger.info('Successfully tested catalog republish without collections')

        input_dict = {"subscription-id": self.subscription_id_collection_republish,
                      "collection-ids": [self.collection_id_with_catalog, self.collection_id_no_catalog]}
        create_response = self.create_and_validate_republish(obj_create_republish, input_dict,
                                                             self.republished_objects,
                                                             self.sqs_url_republish_collection)

        # - Invalid create republish tests
        obj_create_republish.test_create_republish_invalid_x_api_key(catalog_publish)
        logger.info("Successfully tested create republish: Invalid x-api-key")

        obj_create_republish.test_create_republish_invalid_content_type(catalog_publish)
        logger.info("Successfully tested create republish: Invalid content type")

        obj_create_republish.test_create_republish_invalid_subscription_id()
        logger.info("Successfully tested create republish: Invalid subscription id")

        obj_create_republish.test_create_republish_no_catalog_collection_id(
            {"subscription-id": self.subscription_id_publish})
        logger.info("Successfully tested create republish: no catalog and no collection specified")

        obj_create_republish.test_create_republish_catalog_collection_id(
            {"subscription-id": self.subscription_id_publish,
             "collection-ids": [self.collection_id_with_catalog],
             "catalog-ids": [self.catalog_id_publish]})
        logger.info("Successfully tested create republish: catalogs and collections specified")

        obj_create_republish.test_create_republish_invalid_collection_id(
            {"subscription-id": self.subscription_id_publish})
        logger.info("Successfully tested create republish: Invalid collection id")

        obj_create_republish.test_create_republish_invalid_catalog_id(
            {"subscription-id": self.subscription_id_publish})
        logger.info("Successfully tested create republish: Invalid catalog id")

        obj_create_republish.test_create_republish_incorrect_x_api_key(input_dict, self.owner_api_key_invalid_tests,
                                                                       self.owner_id_valid_tests)
        logger.info("Successfully tested create republish: Invalid x-api-key")

        return create_response

    def create_and_validate_republish(self, obj_create_republish, input_dict: dict, republished_objects: list,
                                      sqs_url: str):
        for i in range(self.check_notification_workflow_times):

            create_response = obj_create_republish.test_create_republish(input_dict)
            logger.info(
                'Created Republish ID {}, waiting for state = Completed'.format(create_response['republish-id']))
            valid_republish = self.validate_republish_state(create_response['republish-id'],
                                                            create_response['subscription-id'],
                                                            'Completed')
            logger.info('Successfully completed republish-id: {}'.format(create_response['republish-id']))

            if self.validate_republish_notifications(republished_objects, sqs_url):
                logger.info(
                    "Successfully received notifications for Republish ID {}".format(create_response['republish-id']))
                return valid_republish

        raise Exception(
            "Notifications for Republish ID {} not confirmed".format(create_response['republish-id']))

    @staticmethod
    def get_republish(created_republish: dict) -> None:
        logger.info("### Running get Republish tests ###")

        # + get republish
        from Tests.Regression.regression_get_republish import TestGetRepublish
        obj_get_republish = TestGetRepublish()
        republish_resp = obj_get_republish.test_get_republish(created_republish)

        # - Invalid get republish tests
        obj_get_republish.test_get_republish_invalid_x_api_key(republish_resp)
        logger.info("Successfully tested create republish: Invalid x-api-key")

        obj_get_republish.test_get_republish_no_such_republish(
            {'republish-id': 'invalid-republish-id-12345',
             'subscription-id': republish_resp['subscription-id']})
        logger.info("Successfully tested get republish: Invalid republish id")

        obj_get_republish.test_get_republish_invalid_subscription_id(
            {'republish-id': republish_resp['republish-id'],
             'subscription-id': 999999999})
        logger.info("Successfully tested get republish: Invalid subscription id")

    def validate_republish_notifications(self, republished_objects: list, sqs_url: str) -> bool:
        from Tests.Regression.regression_validate_notifications import TestValidateObjectNotifications
        validator = TestValidateObjectNotifications()
        validation_list = [obj for obj in republished_objects]
        notifications = sorted(self.get_object_notification("Object::Create", validation_list,
                                                            sqs_url), key=lambda k: k['collection-id'])
        if len(notifications) < len(republished_objects):
            return False
        for i in range(len(notifications)):
            self.validate_message(validator, "Object::Create", notifications[i], republished_objects[i],
                                  self.schema_version, 'Republish')
        return True

    def accept_create_subscription(self, subscription_id: int, sqs_url: str) -> dict:
        logger.info("### Accepting create subscription ###")
        confirmed = False

        # wait for the confirmation message to show up in the queue and accept it
        for i in range(self.confirm_subscription_loop_times):
            time.sleep(self.wait_time)
            messages = self.poll_sqs_messages(sqs_url)

            for item in messages:
                body_message = json.loads(item['Body'])
                if body_message['Type'] != 'SubscriptionConfirmation':
                    continue
                resp = requests.post(url=body_message['SubscribeURL'])
                if resp.status_code == 200:
                    self.delete_sqs_message(item, sqs_url)
                    # set to allow us to break the outer loop below
                    confirmed = True
                    break
                else:
                    raise Exception(
                        "Unable to confirm subscription off the queue: {0} {1}".format(resp.status_code, resp.text))

            # pull the break to this loop so we don't keep trying for loop number times
            if confirmed:
                break

            if i == self.confirm_subscription_loop_times - 1:
                raise Exception("Subscription ID {} was not able to be confirmed".format(subscription_id))

        logger.info('Successfully confirmed subscription-id: {}'.format(subscription_id))

        subscription_dict = self.validate_subscription_state(subscription_id, 'Created')
        logger.info('Successfully updated subscription-id: {} to state Created'.format(subscription_id))

        return subscription_dict

    def validate_subscription_state(self, subscription_id: int, state: str) -> dict:
        """
        Get the subscription and validate the state
        :param subscription_id: Subscription ID
        :param state: Subscription State (Created, Pending, Terminated)
        :return: Subscription Item from the GET response
        """
        response = {}
        for _ in range(self.get_subscription_loop_times):
            time.sleep(self.wait_time)
            response = get_subscription({'subscription-id': subscription_id})
            if response.status_code == 200 and response.json()['subscription']['subscription-state'] == state:
                return response.json()['subscription']

        raise Exception("Subscription ID {} was not updated to state {}; status={} ".format(subscription_id, state,
                                                                                            response.status_code))

    def validate_subscription_update(self, subscription_id: int, subscription_record: dict):
        for i in range(self.get_subscription_loop_times):
            time.sleep(self.wait_time)
            get_response = get_subscription({"subscription-id": subscription_id})
            if get_response.status_code == 200 and \
                    get_response.json()['subscription']['filter'] == subscription_record['filter'] and \
                    get_response.json()['subscription']['subscription-name'] == \
                    subscription_record['subscription-name'] and \
                    get_response.json()['subscription']['schema-version'] == subscription_record['schema-version']:
                break
            if i == self.get_subscription_loop_times - 1:
                raise Exception("Subscription was not updated; status={}.".format(get_response.status_code))

    def validate_republish_state(self, republish_id: str, subscription_id: int, state: str) -> dict:
        """
        Get the republish and validate the state
        :param republish_id: Republish ID
        :param subscription_id: Subscription ID
        :param state: Republish State (Pending, Processing, Completed, Retry, Failed)
        :return: Republish Item from the GET response
        """
        response = {}
        for _ in range(self.get_republish_loop_times):
            time.sleep(self.wait_time)
            response = get_republish({'republish-id': republish_id, 'subscription-id': subscription_id})
            if response.status_code == 200:
                response_state = response.json()['republish']['republish-state']
                if response_state == state:
                    return response.json()['republish']
                # If we are in a failed state and we were looking for Completed then bail out
                elif state == 'Completed' and response_state == 'Failed':
                    Exception(
                        "Republish ID {} was not updated to state {}; current state={}; status={} ".format(republish_id,
                                                                                                           state,
                                                                                                           response_state,
                                                                                                           response.status_code))

        raise Exception(
            "Republish ID {} was not updated to state {}; status={} ".format(republish_id, state, response.status_code))

    @classmethod
    def create_object(cls, collection_id) -> dict:
        object_input = {
            "collection-id": collection_id,
            "object-id": "regression-subscription-{}".format(datetime.now().isoformat()),
            "body": "content from regression create object test {}".format(datetime.now().isoformat())
        }
        logger.info('Creating a new object: {}'.format(object_input))
        response = create_object.create_object(object_input)
        object_id = response.json()['object']['object-id']

        for _ in range(cls.get_object_loop_times):
            time.sleep(cls.wait_time)
            response = describe_objects({'object-id': object_id, 'collection-id': collection_id})
            if response.status_code == 200:
                logger.info('Successfully created a new object-id: {}'.format(object_id))
                return response.json()['objects'][0]

        raise Exception("Object was not created; status={}.".format(response.status_code))

    def update_object(self, collection_id: str, object_id: str) -> dict:
        update_input = {
            "collection-id": collection_id,
            "object-id": object_id,
            "body": "content from update orchestration {}".format(datetime.now().isoformat())
        }
        logger.info('Updating object: {}...'.format(update_input))
        update_object.update_object(update_input)

        response = {}
        for _ in range(self.get_object_loop_times):
            time.sleep(self.wait_time)
            response = describe_objects({'collection-id': collection_id, 'object-id': object_id})
            if response.status_code == 200 and response.json()['objects'][0]['version-number'] == 2:
                logger.info('Successfully updated object with id: {}'.format(object_id))
                return response.json()['objects'][0]

        raise Exception("Object was not updated; status={}.".format(response.status_code))

    def remove_object(self, collection_id: str, object_id: str) -> dict:
        remove_input = {
            "collection-id": collection_id,
            "object-id": object_id
        }
        logger.info('Removing object: {}...'.format(remove_input))
        remove_object.remove_object(remove_input)

        response = {}
        for _ in range(self.get_object_loop_times):
            time.sleep(self.wait_time)
            response = describe_objects({'collection-id': collection_id, 'object-id': object_id})
            if response.status_code == 200 and response.json()['objects'][0]['object-state'] == "Removed":
                logger.info('Successfully removed object with id: {}'.format(object_id))
                return response.json()['objects'][0]

        raise Exception("Object was not updated; status={}.".format(response.status_code))

    def get_object_notification(self, event_type: str, objects: list, sqs_url: str) -> list:
        """
        Gets, validates, and deletes the notification from the queue
        :param event_type: the expected event of the notification
        :param objects: The list of sorted object properties expected to receive messages
        :param sqs_url: SQS URL used as endpoint in the subscription
        :return: notification message
        """
        logger.info('Checking SQS for {} notification, object ids, collection ids: {},'
                    .format(event_type, [(obj['object-id'],
                                          obj['collection-id']) for obj in objects]))
        successful_messages = []
        expected_receive_count = len(objects)
        received_message_ids = []

        for _ in range(0, self.check_notification_loop_times):
            time.sleep(self.wait_time)
            messages = self.poll_sqs_messages(sqs_url)
            for message in messages:

                object_props = self.extract_object_notification(message)

                if self.validate_object_message(object_props, event_type, objects):
                    successful_messages.append(object_props)
                self.delete_sqs_message(message, sqs_url)
                received_message_ids.append(message['MessageId'])
            if len(successful_messages) == expected_receive_count:
                break

        received_count = len(set(received_message_ids))
        if expected_receive_count < received_count:
            raise Exception("Unexpected messages in queue. Received {0}, Expected {1}.".format(received_count,
                                                                                               expected_receive_count))
        return successful_messages

    def extract_object_notification(self, message: dict) -> dict:
        message_dict = self.transform_message(message)
        if not message_dict:
            return {}

        # in schema version 'v0' the object properties are at root level in the JSON
        if self.schema_version in ['v0', 'v0v1']:
            object_props = message_dict
        else:
            # in schema version 'v1' the notification props are under the version label {'v1': {...}}
            object_props = message_dict[self.schema_version]["object"]

        return object_props

    @staticmethod
    def validate_object_message(object_props: dict, event_type: str, objects: list) -> bool:
        correct_event = False
        for i in range(len(objects)):
            if object_props.get('object-id', '') == objects[i]['object-id'] and \
                    object_props.get('collection-id', '') == objects[i]['collection-id'] \
                    and object_props.get('event-name', '') == event_type:
                logger.info('received notification for event : {}'.format(event_type))
                correct_event = True
                objects.pop(i)
                break
        return correct_event

    def get_collection_notification(self, event_type: str, collection_id: str, sqs_url: str) -> dict:
        """
        Gets, validates, and deletes the notification from the queue
        :param event_type: the expected event of the notification
        :param collection_id: collection id
        :param sqs_url: SQS URL used as endpoint in the subscription
        :return: notification message
        """
        logger.info('Checking SQS for {} notification for collection id: {}'.format(event_type, collection_id))

        for _ in range(self.check_notification_loop_times):
            time.sleep(self.wait_time)
            messages = self.poll_sqs_messages(sqs_url)
            for message in messages:
                message_dict = self.transform_message(message)
                if not message_dict:
                    continue

                # in schema version 'v0' the collection properties are at root level in the JSON
                if self.schema_version in ['v0', 'v0v1']:
                    collection_props = message_dict
                else:
                    # in schema version 'v1' the notification props are under the version label {'v1': {...}}
                    collection_props = message_dict[self.schema_version]["collection"]

                if collection_props['collection-id'] == collection_id and collection_props['event-name'] == event_type:
                    logger.info('received notification for event : {}'.format(event_type))
                    self.delete_sqs_message(message, sqs_url)
                    return message_dict

        logger.info('did not received notification for event : {}'.format(event_type))
        return {}

    @staticmethod
    def validate_message(validator, event_name: str, message: dict, api_response: dict, schema_version: str,
                         notification_type: str = 'Publish') -> None:
        """
        Determine if the notification message received is consistent with the data retrieved through the GET API
        :param validator: instance of the test class (object or collection) to validate the message
        :param event_name: event name ('Collection::Update', 'Object::Create', etc.)
        :param message: message polled from the subscribed queue
        :param api_response: data obtained with a GET or LIST request
        :param schema_version: subscription's schema version
        :param notification_type: notification type ('Publish' or 'Republish')
        """
        # in schema version 'v0' the collection properties are at root level in the JSON
        if schema_version == 'v0':
            validator.test_validate_notification_v0(event_name, message, api_response)

        # in schema version 'v1' the notification props are under the version label {'v1': {...}}
        if schema_version == 'v1':
            validator.test_validate_notification_v1(event_name, message['v1'], api_response, notification_type)

        if schema_version == 'v0v1':
            validator.test_validate_notification_v0(event_name, message, api_response)
            validator.test_validate_notification_v1(event_name, message['v1'], api_response, notification_type)

        logger.info("Successfully validated notification received for event {}".format(event_name))

    def poll_sqs_messages(self, sqs_url: str) -> list:
        response = self.sqs_client.receive_message(QueueUrl=sqs_url, MaxNumberOfMessages=10)
        return response.get("Messages", [])

    @staticmethod
    def transform_message(sqs_message) -> dict:
        message_dict = {}
        message_body = json.loads(sqs_message['Body'])
        if message_body['Type'] == 'Notification':
            message_dict = json.loads(message_body['Message'])
        return message_dict

    def delete_sqs_message(self, message: dict, sqs_url: str) -> None:
        receipt_handler = message['ReceiptHandle']
        self.sqs_client.delete_message(QueueUrl=sqs_url, ReceiptHandle=receipt_handler)


if __name__ == '__main__':
    unittest.main()
