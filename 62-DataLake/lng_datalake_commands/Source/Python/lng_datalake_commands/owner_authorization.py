from botocore.exceptions import ClientError
from lng_datalake_dal.owner_table import OwnerTable

from lng_datalake_commands.exceptions import InternalError, NotAuthorizedError, NoSuchOwner, UnhandledException

__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


def authorize(owner_id: int, api_key_id: str) -> dict:
    """
    Validates api-key-id from OwnerTable matches api_key_id from request.

    :param owner_id: Owner id.
    :param api_key_id: API key from request.
    """
    try:
        owner_response = OwnerTable().get_item({'owner-id': owner_id})
    except ClientError as e:
        raise InternalError("Unable to access Owner Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not owner_response:
        raise NoSuchOwner("Invalid Owner ID {0}".format(owner_id), "Owner ID does not exist")

    if owner_response['api-key-id'] != api_key_id:
        raise NotAuthorizedError("API Key provided is not valid for Owner ID {0}".format(owner_id),
                                 "Use a valid API Key")

    return owner_response


def get_owner_by_api_key_id(api_key_id: str) -> dict:
    """
    Get owner from OwnerTable by api-key-id.

    :param api_key_id: API key from request.
    """
    try:
        owner_response = OwnerTable().query_items(index="owner-by-api-key-id-index",
                                                  KeyConditionExpression='ApiKeyID=:api_key_id',
                                                  ExpressionAttributeValues={":api_key_id": {"S": api_key_id}})
    except ClientError as e:
        raise InternalError("Unable to query items from Owner Table", e)
    except Exception as ex:
        raise UnhandledException(ex)

    if not owner_response:
        raise NotAuthorizedError("API Key provided is not valid for any Owner ID", "Use a valid API Key")
    return owner_response[0]
