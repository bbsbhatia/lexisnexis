"""
Python decorator (@command) that wraps AWS Lambda handlers that implement Data Lake 'command' processing pattern.

The decorator includes common boilerplate code like JSON request / response validation, decorating the response
with the necessary context information and then validating the response.

The decorator also encompasses a 2nd decorator from the tracker module (@tracker.track_command). The purpose of this
decorator is to ensure incoming events to the command lambdas are put to the tracking table first. The function call
is then re-routed back to the main decorator (@command).

Order of execution of core logic of this module is as follows:

1. Validate client request schema
2. Put event to tracking table if applicable (for valid event types)
3. Execute business logic of wrapped command lambda via func call
4. Validate constructed response schema
5. Put event to event store
"""
import logging
import os
from functools import wraps

import fastjsonschema
import orjson
from botocore.exceptions import ClientError
from lng_datalake_commons import time_helper
from lng_datalake_commons.tracking import tracker
from lng_datalake_dal.event_store_table import EventStoreTable

from lng_datalake_commands.exceptions import DataLakeException, InternalError, InvalidRequestPropertyName

__author__ = "Bruce Maxfield, Arunprasath Shankar, Jose Molinet"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

WORKING_DIRECTORY = os.getenv('LAMBDA_TASK_ROOT')

this = os.sys.modules[__name__]
this._is_initialized = False
this._response_schema = None
this._request_schema_validator = None
this._response_schema_validator = None
this._request_time = None

this.get_remaining_time_in_millis_func = None

logger = logging.getLogger(__name__)
logger.setLevel(os.getenv('LOG_LEVEL', logging.DEBUG))


def command(request_schema_rel_path, response_schema_rel_path, event_name=None):
    def cmd_decorator(func):
        @wraps(func)
        @tracker.track_command(event_name)
        def func_wrapper(event, lambda_context):
            if not this._is_initialized:
                this._request_schema_validator = generate_schema_validator(request_schema_rel_path)
                this._response_schema_validator = generate_schema_validator(response_schema_rel_path)
                this._response_schema = load_json_schema(response_schema_rel_path)
                this._is_initialized = True
            if not this.get_remaining_time_in_millis_func:
                this.get_remaining_time_in_millis_func = lambda_context.get_remaining_time_in_millis

            request_id = event.get('context', {}).get('client-request-id')
            try:
                validate_lambda_request_against_schema(event)

                api_gateway_context = event['context']
                # client-request-time is a string so that it can be tested in the API Gateway console
                try:
                    epoch_ms = int(api_gateway_context['client-request-time'])
                except ValueError as e:
                    raise InternalError("Failed to convert request time epoch value to integer", e)
                # strip trailing 000 and add "Z" to indicate Zulu time zone
                this._request_time = time_helper.format_time(epoch_ms)

                log_event(event)

                logger.info("Lambda Context: {0}".format(vars(lambda_context)))

                response_dict = func(event, lambda_context)

                command_result = response_dict["response-dict"]
                event_dict = response_dict.get("event-dict")
                lambda_response = build_lambda_response(command_result, request_id, lambda_context)
                validate_lambda_response_against_schema(lambda_response)
                if event_dict:
                    put_to_event_store(event_dict)
                return lambda_response
            except DataLakeException as e:
                if request_id and event_name:
                    tracker.un_track_command_by_event(event, event_name, event['context']['stage'])

                raise e
            except Exception as unhandled_error:
                logger.exception('Lambda handler raised unhandled exception::{0}::arguments - {1}'.format(
                    type(unhandled_error).__name__,
                    unhandled_error))
                if request_id and event_name:
                    tracker.un_track_command_by_event(event, event_name, event['context']['stage'])
                raise InternalError('The request failed due to an unhandled internal error {0}'.format(
                    type(unhandled_error).__name__),
                    'See log file details for Lambda request id {0}, API request id {1}'.format(
                        lambda_context.aws_request_id, request_id))

        return func_wrapper

    return cmd_decorator


def generate_schema_validator(schema_path):
    schema_json = load_json_schema(schema_path)
    try:
        return fastjsonschema.compile(schema_json)
    except fastjsonschema.JsonSchemaDefinitionException as e:
        logger.exception('Failed validation due to invalid schema {}. Error: {}'.format(schema_path, e.message))
        raise InternalError("Failed to compile the schema {}".format(schema_path), e.message)


def load_json_schema(rel_path):
    norm_rel_path = os.path.normpath(rel_path)
    with open(os.path.join(WORKING_DIRECTORY, norm_rel_path)) as file:
        try:
            return orjson.loads(file.read())
        except orjson.JSONDecodeError as decode_error:
            logger.exception('Error loading JSON schema {0}::{1}'.format(rel_path, decode_error.msg))
            raise InternalError('Error loading JSON schema {0}'.format(rel_path), decode_error.msg)


def validate_lambda_request_against_schema(event):
    logger.debug('Validating client request JSON schema')
    try:
        this._request_schema_validator(event)
    except fastjsonschema.JsonSchemaException as validation_error:
        logger.exception('Request event did not match JSON request schema')
        raise InvalidRequestPropertyName("Request did not match JSON schema", validation_error.message)
    else:
        logger.debug('Successfully validated Lambda event against JSON request schema')


def build_lambda_response(command_response, request_id, lambda_context):
    logger.debug('Building lambda response')
    try:
        lambda_response = {'response': command_response, 'context': {}}
        lambda_response['context']['request-id'] = request_id
        lambda_response['context']['request-time'] = get_request_time()
        lambda_response['context']['function-arn'] = lambda_context.invoked_function_arn
        lambda_response['context']['log-group-name'] = lambda_context.log_group_name
        lambda_response['context']['log-stream-name'] = lambda_context.log_stream_name
    except AttributeError as missing_attribute:
        logger.exception('Failed while building lambda response for handler due to attribute error')
        raise InternalError("Failed while building lambda response for handler due to attribute error",
                            missing_attribute)
    else:
        # Strip out the object-body when logging due to high cost of logging the body data
        if 'object-body' in lambda_response['response']:
            temp_body = lambda_response['response'].pop('object-body')
            logger.debug("LambdaResponse stripped: {0}".format(orjson.dumps(lambda_response).decode()))
            lambda_response['response']['object-body'] = temp_body
        else:
            logger.debug("LambdaResponse: {0}".format(orjson.dumps(lambda_response).decode()))

        return lambda_response


def validate_lambda_response_against_schema(lambda_response):
    # validate the response from command before returning result to client
    try:
        this._response_schema_validator(lambda_response)
    except fastjsonschema.JsonSchemaException as validation_error:
        logger.exception('Lambda response failed JSON schema validation')
        raise InternalError("Lambda response did not match JSON schema", validation_error.message)
    else:
        logger.debug('Successfully validated Lambda response against JSON response schema')


def put_to_event_store(event_dict: dict) -> None:
    logger.info("Putting event to EventStore table")
    logger.debug("EventDict: {0}".format(orjson.dumps(event_dict).decode()))
    try:
        EventStoreTable().put_item(event_dict)
    except ClientError as e:
        raise InternalError("Unable to insert row into Event Store Table", e)
    except Exception as e:
        raise InternalError("Unhandled exception occurred", e)


def log_event(event: dict):
    """
    Log event without long attributes like 'body'. The word 'stripped' is added to the message if a long attribute
    is removed from the logged event message.

    :param event:
    """
    if logger.isEnabledFor(logging.INFO):
        long_attrs = {}
        # Remove long attributes from event before logging
        for long_attr in ('body', 'object-ids'):
            if long_attr in event.get('request', {}):
                long_attrs[long_attr] = event['request'].pop(long_attr)

        if long_attrs:
            logger.info("Input Event stripped: {0}".format(orjson.dumps(event).decode()))
            # restore any long attributes to the event
            event['request'].update(long_attrs)
        else:
            logger.info("Input Event: {0}".format(orjson.dumps(event).decode()))


def get_required_response_schema_keys(*args):
    return _get_nested_dictionary_definition_item(*args).get('required', [])


def get_optional_response_schema_keys(*args):
    # call _get_nested_dictionary_definition_item once to get both properties and required
    nested_dict_item = _get_nested_dictionary_definition_item(*args)
    required_props = nested_dict_item.get('required', [])
    all_props = list(nested_dict_item.get('properties', {}).keys())
    return list(set(all_props) - set(required_props))


def _get_nested_dictionary_definition_item(*args):
    try:
        pointer = dict(this._response_schema['definitions'])
        for next_name in args:
            pointer = pointer[next_name]
        return pointer
    except Exception as e:
        logger.exception(e)
    return {}


def get_request_time():
    return this._request_time
