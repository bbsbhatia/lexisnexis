import os
from os import listdir

import fastjsonschema

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyValue

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

schema_validators = {}

WORKING_DIRECTORY = os.getenv('LAMBDA_TASK_ROOT')


def validate_patch_operations(patch_schema_path: str, patch_operations: list) -> None:
    """
    This exists to make invalid request property value errors pretty for the client
    :param patch_schema_path: path to your patch schemas, file names should match expect path
    for example a path of /description must have a schema of description.json at this path
    :param patch_operations: the patch-operations passed by the client
    :return: None: Means you have a good patch-operations, raises bad InvalidRequestPropertyValues
    """
    valid_paths = ["/{0}".format(path.replace(".json", "")) for path in
                   listdir(os.path.join(WORKING_DIRECTORY, patch_schema_path))]
    for patch_operation in patch_operations:
        if patch_operation['path'] not in schema_validators:
            try:
                valid_operation_schema = command_wrapper.generate_schema_validator(
                    "{0}{1}.json".format(patch_schema_path, patch_operation['path']))
            except FileNotFoundError:
                raise InvalidRequestPropertyValue("Invalid operation path {0}".format(patch_operation['path']),
                                                  "Valid operation paths are {0}".format(valid_paths))
            schema_validators[patch_operation['path']] = valid_operation_schema

        try:
            schema_validators[patch_operation['path']](patch_operation)
        except fastjsonschema.JsonSchemaException as e:
            raise InvalidRequestPropertyValue("Validation error for operation path {0}".format(patch_operation['path']),
                                              e.message)


def validate_max_results(request_max_items: int, allowed_max_items: int) -> None:
    if 1 > request_max_items or request_max_items > allowed_max_items:
        raise InvalidRequestPropertyValue("Invalid max-items value: {0}".format(request_max_items),
                                          "Value must be between 1 and {0}".format(allowed_max_items))
