import base64
import orjson
from .exceptions import InvalidRequestPropertyValue, InternalError, UnhandledException


def generate_pagination_token(max_items: int, dynamo_page_token: str) -> str:
    try:
        val = {'max-items': int(max_items), 'pagination-token': dynamo_page_token}
        encoded_val = orjson.dumps(val)
        return base64.b64encode(encoded_val).decode('utf-8')
    except Exception as e:
        raise e


def unpack_validate_pagination_token(pagination_token: str, max_items: int) -> dict:
    tokens = {}
    try:
        decoded_token = base64.b64decode(pagination_token).decode('utf-8')
        tokens = orjson.loads(decoded_token)
    except orjson.JSONDecodeError as e:
        raise InternalError("Failed to decode next-token properly",
                            "Provide this information to the DataLake team for assistance")
    except (UnicodeDecodeError, base64.binascii.Error) as e:
        raise InvalidRequestPropertyValue("The next-token is not properly formed",
                                          'Validate the entire token was copied from the previous request properly')
    except Exception as e:
        raise UnhandledException(e)

    _validate_pagination_token(tokens['max-items'], max_items)
    return tokens


def _validate_pagination_token(token_max_result: int, max_items: int) -> None:
    if token_max_result != max_items:
        raise InvalidRequestPropertyValue("Continuation request must pass the same max-items as initial request",
                                          "Set max-items to {0} and try again".format(token_max_result))
