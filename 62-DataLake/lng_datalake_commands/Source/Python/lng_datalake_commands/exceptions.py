__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

message_pattern = "{0}||{1}||{2}"


class DataLakeException(Exception):
    def __init__(self, *args):
        # Fail safe to guarantee we always have 3 parts since the vm tempaltes expect this
        if len(args) != 3:
            message = message_pattern.format("500", "Args dont match: {0}".format(args),
                                             "Contact DL team with request-id")
        else:
            message = message_pattern.format(*args).replace('\n', '')
        super().__init__(message)


class InvalidRequestPropertyName(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("400", description, corrective_action)


class InvalidRequestPropertyValue(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("400", description, corrective_action)


class NotAuthorizedError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("403", description, corrective_action)


class NotFoundError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("404", description, corrective_action)


#####
# Sub Class for NotFoundErrors
####
class NoSuchAsset(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchOwner(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchRelationship(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchCatalog(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchCollection(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchObject(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchObjectVersion(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchObjectRelationship(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchObjectRelationshipVersion(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchSubscription(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchRepublish(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchIngestion(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class NoSuchChangeset(NotFoundError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


#####
# /Sub Class for NotFoundErrors
####

class NotAllowedError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("405", description, corrective_action)


class ForbiddenStateTransitionError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("409", description, corrective_action)


class ConflictingRequestError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("409", description, corrective_action)


#####
# Sub Class for ConflictingRequestError
####

class RelationshipAlreadyExists(ConflictingRequestError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class CollectionPrefixAlreadyExists(ConflictingRequestError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class CollectionAlreadyExists(ConflictingRequestError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class CatalogAlreadyExists(ConflictingRequestError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


class ObjectAlreadyExists(ConflictingRequestError):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__(description, corrective_action)


#####
# /Sub Class for ConflictingRequestError
####

# This is named specifically to match API Gateways error type response
class UNSUPPORTED_MEDIA_TYPE(DataLakeException):  # NOSONAR
    def __init__(self):
        super().__init__("415", "Unsupported Media Type", "")


class SemanticError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("422", description, corrective_action)


class InternalError(DataLakeException):
    def __init__(self, description: str, corrective_action: object = ""):
        super().__init__("500", description, corrective_action)


class UnhandledException(InternalError):
    def __init__(self, corrective_action: object = ""):
        super().__init__("Unhandled exception occurred", corrective_action)
