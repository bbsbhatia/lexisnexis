import re
from os import getenv

from setuptools import setup


def build_version():
    major = '1'
    minor = '2'
    rev = getenv('BUILD_NUMBER', '0')  # This is provided as a Jenkins Global Variable in the build system
    branch = getenv('GIT_BRANCH', 'local')  # This is provided as a Jenkins Global Variable when using Pipeline SCM

    base_version = '.'.join([major, minor, rev])

    if branch.endswith('master'):
        return base_version
    else:
        sanitized_branch = re.sub(r'[^a-zA-Z0-9]+', '.', branch)
        return '+'.join([base_version, sanitized_branch])


setup(
    name='lng_datalake_commands',
    packages=['lng_datalake_commands'],  # this must be the same as the name above
    version=build_version(),
    description='LNG command_wrapper decorator',
    author='Shekhar Ralhan',
    author_email='Shekhar.Ralhan@lexisnexis.com',
    url='https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/62-DataLakeUtils',
    keywords=['lng_datalake_commands'],  # arbitrary keywords
    install_requires=['lng_aws_clients', 'fastjsonschema', 'lng_datalake_testhelper', 'lng_datalake_commons',
                      'lng_datalake_dal', 'lng_orjson']
)
