import logging
import os
import unittest

from lng_datalake_testhelper.io_utils import IOUtils

from lng_datalake_commands import command_validator
from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyValue

__author__ = "Samuel Jackson Sanders"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CommandValidator')
logger = logging.getLogger(__name__)

class TestCommandValidator(unittest.TestCase):

    def setUp(self):  # NOSONAR
        command_validator.WORKING_DIRECTORY = io_util.data_path
        command_wrapper.WORKING_DIRECTORY = io_util.data_path
        logger.setLevel(logging.CRITICAL)

    # + validate_patch_operation - Valid
    def test_valid_patch_operation(self):
        patch_operations = [{"op": "replace", "path": "/description", "value": "hello"}]
        schema_path = "Schemas/Patch/Relationships"

        self.assertIsNone(command_validator.validate_patch_operations(schema_path, patch_operations))

    # + valid_patch_operation with extra - Valid
    def test_valid_patch_operation_a_bunch(self):
        patch_operations = [{"op": "replace", "path": "/description", "value": "hello"},
                            {"op": "add", "path": "/collection-commitments", "value": ["hello"]},
                            {"op": "remove", "path": "/catalog-commitments", "value": ["hello"]}] * 5
        schema_path = "Schemas/Patch/Relationships"

        self.assertIsNone(command_validator.validate_patch_operations(schema_path, patch_operations))

    # - valid_patch_operation - Exception
    # Please note the list in this error, these lists aren't always raised in the same order
    def test_valid_patch_operation_bad_path(self):
        patch_operations = [{"op": "replace", "path": "/descripion", "value": "hello"}]
        schema_path = "Schemas/Patch/Owners"

        with self.assertRaises(InvalidRequestPropertyValue) as e:
            command_validator.validate_patch_operations(schema_path, patch_operations)
        self.assertEqual(str(e.exception), str(InvalidRequestPropertyValue('Invalid operation path /descripion',
                                                                           'Valid operation paths are [\'/name\']')))

    # - valid_patch_operation - Exception
    def test_valid_patch_operation_bad_data_value(self):
        patch_operations = [{"op": "replace", "path": "/description", "value": ["hello"]}]
        schema_path = "Schemas/Patch/Relationships"

        with self.assertRaises(InvalidRequestPropertyValue) as irpv:
            command_validator.validate_patch_operations(schema_path, patch_operations)
        self.assertEqual(str(irpv.exception), str(InvalidRequestPropertyValue('Validation error for operation '
                                                                              'path /description',
                                                                              'data.value must be string')))

    # - valid_patch_operation - Exception
    def test_valid_patch_operation_bad_data_op(self):
        patch_operations = [{"op": "add", "path": "/description", "value": ["hello"]}]
        schema_path = "Schemas/Patch/Relationships"

        with self.assertRaises(InvalidRequestPropertyValue) as e:
            command_validator.validate_patch_operations(schema_path, patch_operations)
        self.assertEqual(str(e.exception), str(InvalidRequestPropertyValue('Validation error for operation path '
                                                                           '/description',
                                                                           'data.op must be one of [\'replace\']')))

    # + validate_max_results - is valid
    def test_validate_max_results_when_max_results_is_less_than_default(self):
        # Given
        max_results = 899

        # When
        result = command_validator.validate_max_results(max_results, 1000)

        # Then
        self.assertIsNone(result)

    # - validate_max_results - is not valid
    def test_validate_max_results_when_max_results_is_higher_than_default(self):
        # Given
        max_results = 1001
        error_message = InvalidRequestPropertyValue("Invalid max-items value: {0}".format(max_results),
                                                    "Value must be between 1 and 1000")

        # When
        with self.assertRaises(InvalidRequestPropertyValue) as context:
            command_validator.validate_max_results(max_results, 1000)

        # Then
        self.assertEqual(str(error_message), str(context.exception))
        self.assertTrue(isinstance(context.exception, InvalidRequestPropertyValue))


if __name__ == '__main__':
    unittest.main()
