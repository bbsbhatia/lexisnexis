import unittest
from unittest import TestCase
from unittest.mock import patch

from botocore.exceptions import ClientError

from lng_datalake_commands.exceptions import InternalError, NoSuchOwner, NotAuthorizedError, UnhandledException
from lng_datalake_commands.owner_authorization import authorize, get_owner_by_api_key_id


class OwnerAuthorizationTest(TestCase):

    # + Success (API Key ID matches what is in OwnerTable)
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    def test_authorize_success(self, mock_owner_get_item):
        owner_data = {
            "owner-id": 999,
            "owner-name": "Aaron Belvo",
            "email-distribution-list": ["aaron.belvo@lexisnexis.com"],
            "api-key-id": "abcd"
        }
        mock_owner_get_item.return_value = owner_data

        self.assertEqual(authorize(999, "abcd"), owner_data)

    # - Authorization error (API Key ID doesn't match what is in OwnerTable)
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    def test_authorize_authorization_error(self, mock_owner_get_item):
        mock_owner_get_item.return_value = {
            "owner-id": 999,
            "owner-name": "Aaron Belvo",
            "email-distribution-list": ["aaron.belvo@lexisnexis.com"],
            "api-key-id": "abcd"
        }

        with self.assertRaisesRegex(NotAuthorizedError, "API Key provided is not valid for Owner ID"):
            authorize(999, "API-KEY-ID-bad")

    # - Owner ID does not exist in OwnerTable
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    def test_authorize_invalid_owner_id(self, mock_owner_get_item):
        mock_owner_get_item.return_value = {}

        with self.assertRaisesRegex(NoSuchOwner, "Invalid Owner ID"):
            authorize(999, "API-KEY-ID")

    # - ClientError when attempting to read OwnerTable
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    def test_authorize_invalid_owner_id_client_error(self, mock_owner_get_item):
        mock_owner_get_item.side_effect = ClientError({'ResponseMetadata': {},
                                                       'Error': {
                                                           'Code': 'OTHER',
                                                           'Message': 'This is a mock'}},
                                                      "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to access Owner Table"):
            authorize(999, "API-KEY-ID")

    # - Exception when attempting to read OwnerTable
    @patch('lng_datalake_dal.owner_table.OwnerTable.get_item')
    def test_authorize_invalid_owner_id_exception(self, mock_owner_get_item):
        mock_owner_get_item.side_effect = Exception
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            authorize(999, "API-KEY-ID")

    # + Success: get owner using api-key-id
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_get_owner_by_api_key_id_success(self, mock_owner_query_items):
        item = {"owner-id": 101, "api-key": "some-key", "api-key-id": "some-key-id", "collection-prefix": "pref"}
        mock_owner_query_items.return_value = [item]
        self.assertDictEqual(get_owner_by_api_key_id("some-key-id"), item)

    # - ClientError while querying OwnerTable
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_get_owner_by_api_key_id_client_error(self, mock_owner_query_items):
        mock_owner_query_items.side_effect = ClientError({'ResponseMetadata': {},
                                                          'Error': {
                                                              'Code': 'Mock ClientError',
                                                              'Message': 'This is a mock'}},
                                                         "FAKE")
        with self.assertRaisesRegex(InternalError, "Unable to query items from Owner Table"):
            get_owner_by_api_key_id("some-key-id")

    # - NotAuthorized Error while querying OwnerTable
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_get_owner_by_api_key_id_not_authorized_error(self, mock_owner_query_items):
        mock_owner_query_items.return_value = []
        with self.assertRaisesRegex(NotAuthorizedError, "API Key provided is not valid for any Owner ID"):
            get_owner_by_api_key_id("some-key-id")

    # - General Exception while querying OwnerTable
    @patch('lng_datalake_dal.owner_table.OwnerTable.query_items')
    def test_get_owner_by_api_key_id_general_exception(self, mock_owner_query_items):
        mock_owner_query_items.side_effect = Exception
        with self.assertRaisesRegex(UnhandledException, "Unhandled exception occurred"):
            get_owner_by_api_key_id("some-key-id")


if __name__ == '__main__':
    unittest.main()
