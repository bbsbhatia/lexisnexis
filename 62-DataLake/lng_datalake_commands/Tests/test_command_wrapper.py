import orjson
import logging
import os
import unittest
from unittest.mock import patch, MagicMock

from botocore.exceptions import ClientError
from lng_datalake_testhelper.io_utils import IOUtils
from lng_datalake_testhelper.mock_lambda_context import MockLambdaContext

from lng_datalake_commands import command_wrapper
from lng_datalake_commands.exceptions import InvalidRequestPropertyName, InternalError, SemanticError, \
    InvalidRequestPropertyValue, ForbiddenStateTransitionError, NoSuchObject

__author__ = "John Morelock, Arunprasath Shankar"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

io_util = IOUtils(__file__, 'CommandWrapper')
logger = logging.getLogger(__name__)


class TestCommandWrapper(unittest.TestCase):

    def setUp(self):  # NOSONAR
        command_wrapper.WORKING_DIRECTORY = os.path.join(os.path.dirname(__file__), "Data", "CommandWrapper", "Schema")
        logger.setLevel(logging.CRITICAL)

    # +command_wrapper.command - can validate input and output json to schema
    @patch("lng_datalake_commands.command_wrapper.put_to_event_store")
    def test_command_wrapper(self, mock_put):
        mock_put.return_value = None
        request_json = io_util.load_data_json("valid_command_request.json")
        response_json = io_util.load_data_json("valid_command_response.json")

        @command_wrapper.command('valid_request_schema.json',
                                 'valid_response_schema.json')
        def command(event, context):
            return {"response-dict": response_json["response"], "event-dict": {"event-id": "abc001",
                                                                               "request-time": "2018-05-18T15:31:17.813000"}}

        # Call command() to invoke command_wrapper.command
        command_resp = command(request_json, MockLambdaContext())
        self.assertDictEqual(command_resp["response"], response_json["response"])

    # +command_wrapper.command - can validate input and output json to schema with body
    @patch("lng_datalake_commands.command_wrapper.put_to_event_store")
    def test_command_wrapper_body(self, mock_put):
        mock_put.return_value = None
        request_json = io_util.load_data_json("valid_command_request_with_body.json")
        response_json = io_util.load_data_json("valid_command_response.json")

        @command_wrapper.command('valid_request_schema.json',
                                 'valid_response_schema.json')
        def command(event, context):
            return {"response-dict": response_json["response"], "event-dict": {"event-id": "abc001",
                                                                               "request-time": "2018-05-18T15:31:17.813000"}}

        # Call command() to invoke command_wrapper.command
        command_resp = command(request_json, MockLambdaContext())
        self.assertDictEqual(command_resp["response"], response_json["response"])

    # +command_wrapper.command - can validate input and output json to schema with object-ids
    @patch("lng_datalake_commands.command_wrapper.put_to_event_store")
    def test_command_wrapper_object_ids(self, mock_put):
        mock_put.return_value = None
        request_json = io_util.load_data_json("valid_command_request_with_object_ids.json")
        response_json = io_util.load_data_json("valid_command_response.json")

        @command_wrapper.command('valid_request_schema.json',
                                 'valid_response_schema.json')
        def command(event, context):
            return {"response-dict": response_json["response"], "event-dict": {"event-id": "abc001",
                                                                               "request-time": "2018-05-18T15:31:17.813000"}}

        # Call command() to invoke command_wrapper.command
        command_resp = command(request_json, MockLambdaContext())
        self.assertDictEqual(command_resp["response"], response_json["response"])

    # -command_wrapper.command - invalid request schema
    @patch("lng_datalake_commons.tracking.tracker.un_track_command_by_event")
    def test_command_wrapper_fail(self, un_track_mock):
        request_json = io_util.load_data_json("invalid_command_request.json")
        response_json = io_util.load_data_json("valid_command_response.json")
        un_track_mock.return_value = None

        @command_wrapper.command('valid_request_schema.json',
                                 'valid_response_schema.json')
        def command(event, context):
            return {'response-dict': response_json["response"], 'event-dict': {}}

        # Call command() to invoke command_wrapper.command
        with self.assertRaises(type(InvalidRequestPropertyName(description="InvalidRequestPropertyName"))):
            command(request_json, MockLambdaContext())

    # -command_wrapper.command - command raises InternalError
    @patch("lng_datalake_commons.tracking.tracker.un_track_command_by_event")
    def test_command_wrapper_fail_1(self, un_track_mock):
        request_json = io_util.load_data_json("valid_command_request.json")
        un_track_mock.return_value = None

        @command_wrapper.command('valid_request_schema.json',
                                 'valid_response_schema.json')
        def command(event, context):
            raise InternalError("Internal Error Test")

        # Call command() to invoke command_wrapper.command
        with self.assertRaises(InternalError):
            command(request_json, MockLambdaContext())

    # -command_wrapper.command - command raises InternalError because of unhandled Exception
    @patch("lng_datalake_commons.tracking.tracker.un_track_command_by_event")
    def test_command_wrapper_fail_2(self, un_track_mock):
        request_json = io_util.load_data_json("valid_command_request.json")
        un_track_mock.return_value = None

        @command_wrapper.command('valid_request_schema.json',
                                 'valid_response_schema.json')
        def command(event, context):
            raise Exception("Unhandled Error Test")

        # Call command() to invoke command_wrapper.command
        with self.assertRaises(InternalError):
            command(request_json, MockLambdaContext())

    # -command_wrapper.load_json_schema - Invalid schema raises InternalError
    def test_command_wrapper_fail_3(self):
        with self.assertRaises(InternalError):
            command_wrapper.load_json_schema('invalid_request_schema_json.json')

    # -command_wrapper.validate_lambda_request_against_schema - validation error => InternalError
    def test_command_wrapper_fail_4(self):
        request_event = io_util.load_data_json("invalid_command_request.json")
        command_wrapper._request_schema_validator = command_wrapper.generate_schema_validator(
            'valid_request_schema.json')
        with self.assertRaisesRegex(InvalidRequestPropertyName, "Request did not match JSON schema"):
            command_wrapper.validate_lambda_request_against_schema(request_event)

    # -command_wrapper.build_lambda_response - raises InternalError from AttributeError
    def test_command_wrapper_fail_5(self):
        response_json = io_util.load_data_json("valid_command_response.json")
        with self.assertRaisesRegex(InternalError,
                                    'Failed while building lambda response for handler due to attribute error'):
            command_wrapper.build_lambda_response(response_json, "abc", None)

    # -command_wrapper.validate_lambda_response_against_schema - raises InternalError from ValidationError
    def test_command_wrapper_fail_6(self):
        response_json = io_util.load_data_json("invalid_command_response.json")
        command_wrapper._response_schema_validator = command_wrapper.generate_schema_validator(
            'valid_response_schema.json')
        with self.assertRaisesRegex(InternalError, 'Lambda response did not match JSON schema'):
            command_wrapper.validate_lambda_response_against_schema(response_json)

    # -command_wrapper.generate_schema_validator - raises InternalError from schema definition error
    def test_command_wrapper_fail_7(self):
        schema_file = 'invalid_response_schema.json'
        with self.assertRaisesRegex(InternalError, "Failed to compile the schema {}".format(schema_file)):
            command_wrapper.generate_schema_validator(schema_file)

    @patch("lng_datalake_commons.tracking.tracker.un_track_command_by_event")
    def test_command_wrapper_fail_8(self, untrack_mock):
        command_wrapper._request_schema = command_wrapper.load_json_schema('valid_request_schema.json')
        command_wrapper._response_schema = command_wrapper.load_json_schema('valid_response_schema.json')
        untrack_mock.return_value = None
        wrapper = command_wrapper.command("valid_request_schema.json", "valid_response_schema.json")
        valid_event = io_util.load_data_json("valid_event.json")
        func_mock = MagicMock()
        side_effects = [
            SemanticError(description="SemanticError"),
            InvalidRequestPropertyName(description="InvalidRequestPropertyName"),
            InvalidRequestPropertyValue(description="InvalidRequestPropertyValue"),
            ForbiddenStateTransitionError(description="ForbiddenStateTransitionError"),
            NoSuchObject(description="NotFound"),
            InternalError(description="InternalError")
        ]
        for side_effect in side_effects:
            func_mock.side_effect = side_effect
            func_wrapper = wrapper(func_mock)
            with self.assertRaises(type(side_effect)):
                func_wrapper(valid_event, MockLambdaContext)

    # -command_wrapper.command - command raises InternalError because of ValueError for request-time-epoch
    @patch("lng_datalake_commons.tracking.tracker.un_track_command_by_event")
    def test_command_wrapper_fail_9(self, un_track_mock):
        request_json = io_util.load_data_json("invalid_command_request_bad_epoch.json")
        un_track_mock.return_value = None

        @command_wrapper.command('valid_request_schema.json',
                                 'valid_response_schema.json')
        def command(event, context):
            return None

        # Call command() to invoke command_wrapper.command
        with self.assertRaisesRegex(InternalError, "Failed to convert request time epoch value to integer"):
            command(request_json, MockLambdaContext())

    # +command_wrapper.get_required_response_schema_keys - return array of required keys for response
    def test_get_required_response_schema_keys(self):
        response_schema_dictionary = io_util.load_data_json(
            os.path.join('Schema', 'CreateCollectionCommand-ResponseSchema.json'))
        expected_value = set(response_schema_dictionary['definitions']['collection-properties'].get('required', []))
        with patch('lng_datalake_commands.command_wrapper._response_schema', response_schema_dictionary):
            returned_value = set(command_wrapper.get_required_response_schema_keys('collection-properties'))
            self.assertEqual(expected_value, returned_value)

    # -command_wrapper.get_optional_response_schema_keys - return array of required keys for response
    def test_get_required_response_schema_keys_fail_none(self):
        response_schema_dictionary = io_util.load_data_json(
            os.path.join('Schema', 'CreateCollectionCommand-ResponseSchema.json'))
        with patch('lng_datalake_commands.command_wrapper._response_schema', response_schema_dictionary):
            returned_value = command_wrapper.get_required_response_schema_keys(None)
            self.assertEqual([], returned_value)

            returned_value = command_wrapper.get_required_response_schema_keys('collection properties')
            self.assertEqual([], returned_value)

            returned_value = command_wrapper.get_required_response_schema_keys(32)
            self.assertEqual([], returned_value)

    # +command_wrapper.get_optional_response_schema_keys - return array of optional keys for response
    def test_get_optional_response_schema_keys(self):
        response_schema_dictionary = io_util.load_data_json(
            os.path.join('Schema', 'CreateCollectionCommand-ResponseSchema.json'))
        expected_value = set(io_util.load_data_json("collection_properties_optional_list.json"))
        with patch('lng_datalake_commands.command_wrapper._response_schema', response_schema_dictionary):
            returned_value = set(command_wrapper.get_optional_response_schema_keys('collection-properties'))
            self.assertEqual(expected_value, returned_value)

    # -command_wrapper.get_optional_response_schema_keys - return array of optional keys for response
    def test_get_optional_response_schema_keys_property_attribute_error_(self):
        response_schema_dictionary = io_util.load_data_json(
            os.path.join('Schema', 'property_attribute_error_CreateCollectionCommand-ResponseSchema.json'))
        with patch('lng_datalake_commands.command_wrapper._response_schema', response_schema_dictionary):
            returned_value = command_wrapper.get_required_response_schema_keys('collection properties')
            self.assertEqual([], returned_value)

    # -command_wrapper.get_optional_response_schema_keys - return array of optional keys for response
    def test_get_optional_response_schema_keys_property_key_error_(self):
        response_schema_dictionary = io_util.load_data_json(
            os.path.join('Schema', 'key_error_CreateCollectionCommand-ResponseSchema.json'))
        with patch('lng_datalake_commands.command_wrapper._response_schema', response_schema_dictionary):
            returned_value = command_wrapper.get_required_response_schema_keys('collection properties')
            self.assertEqual([], returned_value)

    @patch("lng_datalake_dal.table.Table.put_item")
    def test_put_to_event_store_success(self, mock_put):
        mock_put.return_value = None
        self.assertIsNone(command_wrapper.put_to_event_store(event_dict={"event-id": "abc001",
                                                                         "timestamp": "2018-12-01"}))

    @patch("lng_datalake_dal.table.Table.put_item")
    def test_put_to_event_store_fail(self, mock_put):
        side_effects = [ClientError({"ResponseMetadata": {},
                                     "Error": {"Code": "code",
                                               "Message": "message"}},
                                    "client-error-mock"), Exception]
        for side_effect in side_effects:
            mock_put.side_effect = side_effect
            with self.assertRaises(type(InternalError(description="InternalError"))):
                self.assertIsNone(command_wrapper.put_to_event_store(event_dict={"event-id": "abc001",
                                                                                 "timestamp": "2018-12-01"}))

    @patch("lng_datalake_commons.tracking.tracker.un_track_command_by_event")
    @patch("lng_datalake_commons.tracking.tracker._put_event_to_tracking_table")
    @patch("lng_aws_clients.session.set_session")
    def test_untrack_called_on_schema_fail(self, mock_session, mock_track, mock_untrack):
        mock_session.return_value = None
        mock_untrack.return_value = None
        mock_track.return_value = None
        request_json = io_util.load_data_json("valid_command_request.json")
        response_json = io_util.load_data_json("valid_command_response.json")

        @command_wrapper.command('valid_request_schema.json',
                                 'valid_response_schema.json',
                                 'Collection::Update')
        def command(event, context):
            return {"response-dict": response_json["response"], "event-dict": {"event-id": "abc001",
                                                                               "request-time": "2018-05-18T15:31:17.813000"}}

        # Call command() to invoke command_wrapper.command
        with self.assertRaisesRegex(InternalError, 'Items did not match interface schema'):
            command(request_json, MockLambdaContext())

        mock_untrack.assert_called_once()

    # +log_event_success - level = CRITICAL so nothing to log
    def test_log_event_critical_success(self):
        input_json = io_util.load_data_json("valid_command_request.json")
        with patch('lng_datalake_commands.command_wrapper.logger.info') as log_mock:
            command_wrapper.logger.setLevel(logging.CRITICAL)
            self.assertIsNone(command_wrapper.log_event(input_json))
            log_mock.assert_not_called()

    # +log_event_success - no attributes removed
    def test_log_event_success(self):
        input_json = io_util.load_data_json("valid_command_request.json")
        with patch('lng_datalake_commands.command_wrapper.logger.info') as log_mock:
            command_wrapper.logger.setLevel(logging.INFO)
            self.assertIsNone(command_wrapper.log_event(input_json))
            log_mock.assert_called_once_with("Input Event: {0}".format(orjson.dumps(input_json).decode()))

    # +test_trucate_event_success - body removed
    def test_truncate_event_body_success(self):
        input_json = io_util.load_data_json("valid_command_request_with_body.json")
        expected_json = io_util.load_data_json("valid_command_request.json")
        with patch('lng_datalake_commands.command_wrapper.logger.info') as log_mock:
            command_wrapper.logger.setLevel(logging.INFO)
            self.assertIsNone(command_wrapper.log_event(input_json))
            log_mock.assert_called_once_with("Input Event stripped: {0}".format(orjson.dumps(expected_json).decode()))
            self.assertIn('body', input_json['request'])

    # +test_trucate_event_success - object-ids removed
    def test_truncate_event_object_ids_success(self):
        input_json = io_util.load_data_json("valid_command_request_with_object_ids.json")
        expected_json = io_util.load_data_json("valid_command_request.json")
        with patch('lng_datalake_commands.command_wrapper.logger.info') as log_mock:
            command_wrapper.logger.setLevel(logging.INFO)
            self.assertIsNone(command_wrapper.log_event(input_json))
            log_mock.assert_called_once_with("Input Event stripped: {0}".format(orjson.dumps(expected_json).decode()))
            self.assertIn('object-ids', input_json['request'])


if __name__ == '__main__':
    unittest.main()
