import unittest

from lng_datalake_commands.exceptions import *

__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"


class TestExceptions(unittest.TestCase):
    # +DataLakeException - too few passed
    def test_data_lake_exception_1(self):
        with self.assertRaisesRegex(DataLakeException,
                                    "'500||Args dont match: (\'test\\n failure\',)||Contact DL team with request-id'"):
            raise DataLakeException("test\n failure")

    # +InvalidRequestPropertyName - can create object
    def test_invalid_request_property_name(self):
        with self.assertRaisesRegex(InvalidRequestPropertyName, "400||test failure||"):
            raise InvalidRequestPropertyName("test\n failure")

    # +InvalidRequestPropertyValue - can create object
    def test_invalid_request_property_value(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "400||test failure||"):
            raise InvalidRequestPropertyValue("test\n failure")

    # +NotAuthorizedError - can create object
    def test_not_authorized_error(self):
        with self.assertRaisesRegex(NotAuthorizedError, "403||est failure||"):
            raise NotAuthorizedError("test\n failure")

    # +NotFoundError - Generic
    def test_not_found_error(self):
        with self.assertRaisesRegex(NotFoundError, "404||est failure||"):
            raise NotFoundError("NoSuchEntity", "test\n failure")

    # +NoSuchAsset - Asset
    def test_not_found_error_asset(self):
        with self.assertRaisesRegex(NoSuchAsset, "404||test failure||"):
            raise NoSuchAsset("test\n failure")

    # +NoSuchOwner - Owner
    def test_not_found_error_owner(self):
        with self.assertRaisesRegex(NoSuchOwner, "404||test failure||"):
            raise NoSuchOwner("test\n failure")

    # +NoSuchRelationship - Relationship
    def test_not_found_error_relationship(self):
        with self.assertRaisesRegex(NoSuchRelationship, "404||test failure||"):
            raise NoSuchRelationship("test\n failure")

    # +NoSuchCatalog - Catalog
    def test_not_found_error_catalog(self):
        with self.assertRaisesRegex(NoSuchCatalog, "404||test failure||"):
            raise NoSuchCatalog("test\n failure")

    # +NoSuchCollection - Collection
    def test_not_found_error_collection(self):
        with self.assertRaisesRegex(NoSuchCollection, "404||test failure||"):
            raise NoSuchCollection("test\n failure")

    # +NoSuchObject - Object
    def test_not_found_error_object(self):
        with self.assertRaisesRegex(NoSuchObject, "404||test failure||"):
            raise NoSuchObject("test\n failure")

    # +NoSuchObjectVersion - ObjectVersion
    def test_not_found_error_object_version(self):
        with self.assertRaisesRegex(NoSuchObjectVersion, "404||test failure||"):
            raise NoSuchObjectVersion("test\n failure")

    # +NoSuchObjectRelationship - ObjectRelationship
    def test_not_found_error_object_relationship(self):
        with self.assertRaisesRegex(NoSuchObjectRelationship, "404||test failure||"):
            raise NoSuchObjectRelationship("test\n failure")

    # +NoSuchObjectRelationshipVersion - ObjectRelationshipVersion
    def test_not_found_error_object_relationship_version(self):
        with self.assertRaisesRegex(NoSuchObjectRelationshipVersion, "404||test failure||"):
            raise NoSuchObjectRelationshipVersion("test\n failure")

    # +NoSuchSubscription - Subscription
    def test_not_found_error_subscription(self):
        with self.assertRaisesRegex(NoSuchSubscription, "404||test failure||"):
            raise NoSuchSubscription("test\n failure")

    # +NoSuchRepublish - Republish
    def test_not_found_error_republish(self):
        with self.assertRaisesRegex(NoSuchRepublish, "404||test failure||"):
            raise NoSuchRepublish("test\n failure")

    # +NoSuchIngestion - Republish
    def test_not_found_error_ingestion(self):
        with self.assertRaisesRegex(NoSuchIngestion, "404||test failure||"):
            raise NoSuchIngestion("test\n failure")

    # +NoSuchChangeset - Changeset
    def test_not_found_error_changeset(self):
        with self.assertRaisesRegex(NoSuchChangeset, "404||test failure||"):
            raise NoSuchChangeset("test\n failure")

    # +NotAllowedError - can create object
    def test_not_allowed_error(self):
        with self.assertRaisesRegex(NotAllowedError, "405||test failure||"):
            raise NotAllowedError("test\n failure")

    # +ForbiddenStateTransitionError - can create object
    def test_forbidden_state_transition_error(self):
        with self.assertRaisesRegex(ForbiddenStateTransitionError,
                                    "409||test failure||"):
            raise ForbiddenStateTransitionError("test\n failure")

    # +ConflictingRequestError - can create object
    def test_conflicting_request_error(self):
        with self.assertRaisesRegex(ConflictingRequestError, "409||test failure||"):
            raise ConflictingRequestError("ConflictingRequestError", "test\n failure")

    # +RelationshipAlreadyExists - can create object
    def test_relationship_already_exists_error(self):
        with self.assertRaisesRegex(RelationshipAlreadyExists, "409||test failure||"):
            raise RelationshipAlreadyExists("test\n failure")

    # +CollectionPrefixAlreadyExists - can create object
    def test_collection_prefix_already_exists_error(self):
        with self.assertRaisesRegex(CollectionPrefixAlreadyExists,
                                    "409||test failure||"):
            raise CollectionPrefixAlreadyExists("test\n failure")

    # +CollectionAlreadyExists - can create object
    def test_collection_already_exists_error(self):
        with self.assertRaisesRegex(CollectionAlreadyExists, "409||test failure||"):
            raise CollectionAlreadyExists("test\n failure")

    # +CatalogAlreadyExists - can create object
    def test_catalog_already_exists_error(self):
        with self.assertRaisesRegex(CatalogAlreadyExists, "409||test failure||"):
            raise CatalogAlreadyExists("test\n failure")

    # +ObjectAlreadyExists - can create object
    def test_object_already_exists_error(self):
        with self.assertRaisesRegex(ObjectAlreadyExists, "409||test failure||"):
            raise ObjectAlreadyExists("test\n failure")

    # +UNSUPPORTED_MEDIA_TYPE - can create object
    def test_unsupported_media_type(self):
        with self.assertRaisesRegex(UNSUPPORTED_MEDIA_TYPE, "415||Unsupported Media Type||"):
            raise UNSUPPORTED_MEDIA_TYPE()

    # +SemanticError - can create object
    def test_semantic_error(self):
        with self.assertRaisesRegex(SemanticError, "422||test failure||"):
            raise SemanticError("test\n failure")

    # +InternalError - can create object
    def test_internal_error(self):
        with self.assertRaisesRegex(InternalError, "500||test failure||"):
            raise InternalError("test\n failure")

    # -InternalError - stacktrace
    def test_internal_error_stacktrace(self):
        with self.assertRaisesRegex(InternalError, "500||test failure||can only join an iterable"):
            try:
                "".join(object)
            except Exception as e:
                raise InternalError("test failure", e)

    # -InternalError - exception
    def test_internal_error_exception(self):
        with self.assertRaisesRegex(InternalError, "500||test failure||foobar"):
            raise InternalError("test failure", Exception("foobar"))

    # +UnhandledException - Valid
    def test_unhandled_exception(self):
        with self.assertRaisesRegex(UnhandledException, "500||Unhandled exception occurred||foobar"):
            raise UnhandledException(Exception("foobar"))


if __name__ == '__main__':
    print(unittest.main())
