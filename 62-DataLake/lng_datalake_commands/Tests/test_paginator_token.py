import unittest
from unittest import TestCase
from unittest.mock import patch

from lng_datalake_commands.paginator_token import generate_pagination_token, unpack_validate_pagination_token, _validate_pagination_token
from lng_datalake_commands.exceptions import InvalidRequestPropertyValue, InternalError

class PaginatorTokenTest(TestCase):

    # + generate_pagination_token - Valid
    def test_generate_pagination_token_valid(self):
        resp = "eyJtYXgtaXRlbXMiOjEyLCJwYWdpbmF0aW9uLXRva2VuIjoiZXlKRmVHTnNkWE5wZG1WVGRHRnlkRXRsZVNJNklIc2lRMkYwWVd4dlowbEVJam9nZXlKVElqb2dJbU5oZEdGc2IyY3RNREVpZlgxOSJ9"
        self.assertEqual(generate_pagination_token(12, "eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ2F0YWxvZ0lEIjogeyJTIjogImNhdGFsb2ctMDEifX19"), resp)

    # - generate_pagination_token - Exception
    def test_generate_pagination_token_exception(self):
        with self.assertRaisesRegex(Exception, 'invalid literal for'):
            generate_pagination_token('foobar', 'xyz')

    # + unpack_validate_pagination_token - Valid
    def test_unpack_validate_pagination_token_valid(self):
        input_token = "eyJtYXgtaXRlbXMiOjEyLCJwYWdpbmF0aW9uLXRva2VuIjoiZXlKRmVHTnNkWE5wZG1WVGRHRnlkRXRsZVNJNklIc2lRMkYwWVd4dlowbEVJam9nZXlKVElqb2dJbU5oZEdGc2IyY3RNREVpZlgxOSJ9"

        expected = {'max-items': 12, 'pagination-token': 'eyJFeGNsdXNpdmVTdGFydEtleSI6IHsiQ2F0YWxvZ0lEIjogeyJTIjogImNhdGFsb2ctMDEifX19'}
        self.assertEqual(unpack_validate_pagination_token(input_token, 12), expected)

    # - unpack_validate_pagination_token - bad padding
    def test_unpack_validate_pagination_token_bad_padding(self):
        input_token = "JtYXgtaXRlbXMiOjEyLCJwYWdpbmF0aW9uLXRva2VuIjoiZXlKRmVHTnNkWE5wZG1WVGRHRnlkRXRsZVNJNklIc2lRMkYwWVd4dlowbEVJam9nZXlKVElqb2dJbU5oZEdGc2IyY3RNREVpZlgxOSJ9"

        with self.assertRaisesRegex(InvalidRequestPropertyValue, "The next-token is not properly formed"):
            unpack_validate_pagination_token(input_token, 12)

    # - unpack_validate_pagination_token - decode error
    def test_unpack_validate_pagination_token_decode_error(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "The next-token is not properly formed"):
            unpack_validate_pagination_token('asdf', 12)

    # - unpack_validate_pagination_token - json error
    def test_unpack_validate_pagination_token_json_error(self):
        with self.assertRaisesRegex(InternalError, "Failed to decode next-token properly"):
            unpack_validate_pagination_token('ZXlKRmVHTnNkWE5wZG1WVGRHRnlkRXRsZVNJNklIc2lRWE56WlhSSlJDSTZJSHNpVGlJNklDSXlOeUo5ZlgwPQ==', 12)

    # - unpack_validate_pagination_token - Exception
    @patch('base64.b64decode')
    def test_unpack_validate_pagination_token_exception(self, mock_b64decode):
        mock_b64decode.side_effect = Exception('Forced Error')
        with self.assertRaisesRegex(InternalError, "Unhandled exception occurred"):
            unpack_validate_pagination_token('ZXlKRmVHTnNkWE5wZG1WVGRHRnlkRXRsZVNJNklIc2lRWE56WlhSSlJDSTZJSHNpVGlJNklDSXlOeUo5ZlgwPQ==', 12)

    # + validate_pagination_token - valid
    def test_validate_pagination_token_valid(self):
        self.assertIsNone(_validate_pagination_token(1, 1))

    # + validate_pagination_token - invalid
    def test_validate_pagination_token_invalid(self):
        with self.assertRaisesRegex(InvalidRequestPropertyValue, "Continuation request must pass the same max-items as initial request"):
            self.assertIsNone(_validate_pagination_token(20, 1))

if __name__ == '__main__':
    unittest.main()
