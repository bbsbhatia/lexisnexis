
# Patch Validator Command Function for patch operation request validation

Date: 2019-07-30

## Status: Accepted

* Deciders: Planet X

## Context
Technical Story: Currently other non PATCH APIs have more generic request parameters that can be validated 
using the API Gateway model. They generate good error messages whilst also generating good 
swagger models and clients. Old PATCH APIs are using lambda command wrapper schema
validation so simple API gateway models can be used to support good swagger models and
generated clients, however, the error messages in the response to the user are
primitive at best. 

Option 1:
Validate patch operations using API Gateway Models and oneOf schema validation.
This does work, but the error messages are even more
lackluster than current PATCH implementation and
the swagger is unable to generate a good model or client.

Option 2:
Let the lambda command wrapper schema validate the patch operations.
Continues the old tried and true PATCH operation validation, but
the error messages sent in response to the user are not very informative.

Option 3:
Build a custom schema for patch operation and custom curate the messages
when the patch operation doesn't validate against the schema.

## Decision

Option 3 was selected because it enables both a straight forward common approach to 
validating patch operations, but it also provides informative error messages in the
response to the user.

## Consequences
* More schemas will be needed for patch operations

## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:722681)