# Deploying DataIndex

DataIndex is deployed via Jenkins, but the process for ingesting DataLake events is not 100% automated.  In addition to the build pipelines, a user will need to create a DataLake subscription and authorize the DataLake to write the the SQS Queue (and a create DataLake Owner if one has not already been created).  

## Deployment Steps

1.  Execute the Jenkins "build" Pipeline.  This will deploy the cloudformation templates that will be used in the deploy steps below.

2.  Execute the Jenkins "deploy-sqs-datalake-objects" pipeline.  This will create an SQS queue that can hold messages that will be converted and stored into the Elasticsearch index.  Select the target DataLake environment when deploying.  This will grant that environment access to write to your queue.

3.  (Optional) Create a DataLake Owner.  This step is not automated.  See [Creating an Owner](https://wiki.regn.net/wiki/DataLake_Hello_World#Creating_an_Owner) for information.

4.  Create a DataLake Subscription.  This step is not automated.  You'll need to use the output information from the "deploy-sqs-datalake-objects" pipeline to create a subscription.  The Logstash instance that you'll create in the next step assumes that you'll be using schema version "v1".  See [Create Subscription](https://wiki.regn.net/wiki/DataLake_Hello_World#Create_Subscription)  for more information.

5.  Execute the Jenkins "deploy-elasticsearch" pipeline.  This will create an Elasticsearch cluster and the Cognito User and Identity pools used to protect it.  You'll want to carefully choose the size of the cluster.  See ElasticsearchSizingGuidelines.md for more information.

6.  Before setting up Logstash, you'll want to prep your Elasticsearch index(es).  By default, Elasticsearch will auto-generate index mappings.  These mappings will not be efficient and will not scale to a large number of documents.  After Elasticsearch is running, you'll want to register a user by going to the Kibana URL, which will give you the option to self-register.  An administrator will need to "promote" this user by using the AWS Cognito User group console and add this user to the "Administrators" group.  After this is done, that user will have access to configure the mappings and aliases for the Elasticsearch index(es) by entering commands into the Kibana development tool.  See Source/Elasticsearch/objects.txt for the commands to execute.  You may want to adjust the number of shards for your implementation.  See ElasticsearchSizingGuidelines.md for more information.  NOTE: These commands may change during the evolution of the DataIndex.

7.   Execute the Jenkins "deploy-logstash-datalake" pipeline.  This will create the Logstash instances that will populate the Elasticsearch index(es) with the data on the SQS Queue.

8.  (Optional) Republish the DataLake Object Events.  This will need to be done with external code.  See [the Subscription Swagger files](https://datalake-staging.content.aws.lexis.com/v1/DataLake-Subscription-SwaggerDocumentation.json) for the republish API.

## Re-deploying parts of DataIndex

Executing all of the Deployments steps will only be required a single time.  After execution, the elasticsearch and logstash pipelines can be used to re-size their respective clusters.  Additional queues and logstash instances can be added to supply additional content to existing indexes or into new ones.

Elasticsearch will scale in and out without down-time.  Logstash can be scaled in and out as well to accommodate the size of the queue and the number of messages that a queue gets.

## Depoyment Steps for DataIndex Developers

Developers can create and enhance their own copy of a DataIndex by following a variation of "Deployment" steps above.

1.  Skip this step.

2.  Instead of executing the build pipeline for sqs, use cloudformation and create a stack using Infrastructure/sqs-datalake-objects.template.

3.  This step stays the same.

4.  This step stays the same.

5.  Instead of executing the build pipeline for elasticsearch, use cloudformation and create a stack using Infrastructure/elasticsearch.template instead.  The default values are generally correct for development use.  Adjust as needed.

6.  This step stays the same.

7.  Instead of executing the build pipeline for logstash, use cloudformation and create a stack using Infrastructure/logstash-datalake.template instead.

8.  Be careful republishing to a development instance.  This is only advised if the subscription filter created in step 4 produces a manageable number of events.



 