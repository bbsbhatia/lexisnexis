
# Hosting Logstash on EC2


Date: 2019-09-13

## Status:  Accepted

* Deciders: Wormhole Team

## Context
The Logstash servers that transform and load the Elasticsearch index for the dataindex need a hosting solution.  AWS doesn't have a managed service for this like it does with Elasticsearch.


## Decision
Logstash will be hosted on EC2 instances for MVP.  This solution is well understood within LexisNexis.  This solutions is currently considered TACTICAL for the consequences mentioned below.


## Consequences
* Using EC2 instances isn't portable to another cloud vendor.
    * A docker container approach is portable
    * AWS Fargate would allow us to run and manage the docker instances while this feature is hosted on AWS.
* Logstash configuration isn't testable with this approach.  Although there is no "code" for the Logstash instance, the JSON configuration file that controls Logstash's behavior is prone to errors/bugs. 
    * A docker container approach would allow for command-line execution of the filter and transformation steps within the Logstash configuration.  This feature would greatly improve future development time and reduce the chance for failure when logstash updates are deployed.


## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/TeamRoom.mvc/Show/TODO)

