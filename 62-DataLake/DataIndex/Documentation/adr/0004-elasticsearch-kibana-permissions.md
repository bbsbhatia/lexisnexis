
# Elasticsearch and Kibana Permissions for Self-Registered Users


Date: 2019-10-23

## Status:  Accepted

* Deciders: Wormhole Team

## Context
Elasticsearch and Kibana can do some powerful things: modify/delete content, create impressive reports and create custom fields using Painless scripting.  As with most things, great power is joined by great responsibility.  Cognito allows some control over what self-registered users are allowed to do.  The question now becomes, where should the lines be drawn?


## Decision
The DataIndex was not intended to be a source of information for the select few; therefore, self-registered users will be allowed to use most of Kibana's features.  Self-registered users will be allowed to create and remove index profiles, custom fields, visualizations and dashboards.  They will also be able to execute any ad-hoc read-only* query from the Kibana's Dev Tools page.

\* The exception will be with the .kibana index, which will be writable in order to allow users to modify profiles, fields, visualizations and dashboards through Kibana's other UI features. 

## Consequences
* Users will be able to destroy each others' work.  
    * This is true of many of our tools and applications we use.
    * It should be possible to pull AWS's periodic snapshots to recover from extensive damage.
* Users can write queries which can damage the health of the system.  Some Painless queries and custom fields, in particular, can cause some fairly extreme CPU and memory spikes.
    * The DataIndex is optimized for data writes.  Writing content may be delayed when these events occur, but SQS and Logstash are built to handle interruptions in service.  
* Users are not anonymous, but there are no known, readily-available tracking tools which can monitor kibana activity.  Elasticsearch X-Pack has a tool, but at the time of this writing this feature has not been replicated on the AWS open Elasticsearch implementation. 


## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/TeamRoom.mvc/Show/TODO)

