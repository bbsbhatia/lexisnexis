
# DataIndex Design (Choosing Elasticsearch)


Date: 2019-08-05

## Status:  Accepted

* Deciders: Wormhole Team

## Context
The state of DataLake objects need to be visible to users of the DataIndex platform, an Elasticsearch cluster that permits read-only query and analytics features to LexisNexis employees.


## Decision
Data will be indexed with Elasticsearch.  Elasticsearch is a massively scalable and popular open source no-SQL database that was chosen for many reasons.
* Elasticsearch integrates with Kibana, which offers a rich set of visualizations.
* Elasticsearch offers a rich set of REST apis.
* Elasticsearch has numerous open-source libraries.
* Elasticsearch is massively scalable and can scale out (or in) without system down-time.
* Elasticsearch can be executed as an AWS managed service.
* Elasticsearch on AWS integrates with AWS Cognito, which allows for read and write protection of the data index.
* Elasticsearch is no-SQL, which provides a significant amount of integration flexibility.
* Elasticsearch stores documents as JSON, which is used throughout the DataLake and other new infrastructure assets.  JSON is a popular data markup at this time and is extremely compatible with most programming languages and tools. 
* Elasticsearch supports SQL syntax queries, which makes writing queries easier for users already familiar with SQL.
* Elasticsearch uses the well used and frequently proven Lucine package to index content.


## Consequences
* Elasticsearch is not serverless.
    * DynamoDB has a significantly smaller feature list and has issues with scaling (hot spots).
    * RDS doesn't have the APIs that Elasticsearch has.
* Elasticsearch can't do some of the things that Relation databases can do, like large inner joins and advanced cross-index intersection capabilities.
    * This is by far the most regrettable of the consequences.  Unfortunately traditional SQL databases can't do many of the things that Elasticsearch has, out-of-the-box.  The cross-index requirement will need to be mitigated by creating aggregate indexes, which will need to be carefully updated.  For this reason, the update instances will probably need to controlled by the DataIndex (customers will not be allowed to perform direct PUT/POST statements into certain indexes, in order to prevent database corruption.

    


## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/TeamRoom.mvc/Show/TODO)
