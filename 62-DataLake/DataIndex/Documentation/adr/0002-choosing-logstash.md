
# DataIndex Design (DataLake as Data Provider)


Date: 2019-09-13

## Status:  Accepted

* Deciders: Wormhole Team

## Context
The state of DataLake objects need to be visible to users of the DataIndex platform, an Elasticsearch cluster that permits read-only query and analytics features to LexisNexis employees.


## Decision
Data will be supplied to the DataIndex Elasticsearch Cluster by way of Logstash.  Logstash will take all datalake object events off of a DataLake Subscription that was created to watch all DataLake objects and put records of those events into one of DataIndex's indexes.


## Consequences
* Logstash requires an EC2 instance to operate.  Lambdas and Kinesis could have been used to support this feature instead, which would have resulted in a serverless solution.  There were several reasons for not pursuing this approach.
    * Logstash's out-of-the box input, output, filter and mutation capabilities make the management of this task extremely easy to use, manage and extend to other applications.
    * The ELK (Elasticsearch Logstash Kibana) stack is massively popular and well understood.
    * Cost is easier to control with a frequently used system that is hosted on an EC2 instance.


## Links

* [Version One Story](https://www4.v1host.com/LexisNexis/TeamRoom.mvc/Show/TODO)

