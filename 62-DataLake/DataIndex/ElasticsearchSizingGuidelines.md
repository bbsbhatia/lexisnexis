# Elasticsearch Sizing guidelines


## Shards
Setting the number of Elasticsearch shards could be the most important thing a developer can do when creating a new, production index.  Why are shards so important?  You can only set them once when you create the index.  If you want to change it, you have to create a new index and move everything over.  This process can be time consuming, expensive and may result in service interruption.  Generally, search performance suffers when you have too many shards, so a balance must be found to maximize space and performance.

Guidelines:
1.	Shards should be between 10-50 GB (per Amazon) or 20-40 GB (per Elastic).
2.	Shards per node rules follow the amount of JVM memory available.  Amazon didn’t say anything about this, but Elastic’s rule is that there should be no more than 20 shards per GB of JVM memory on the machine.  The JVM takes up 99% of the memory on the AWS boxes. 

## EC2 Instance Type
Instance type depends a lot on the load of the system.  For DataIndex, we're projecting high load rates and low query rates.

Load Rate testing produced the following results:

| ES Server Type | # ES Data Nodes | # Logstash Loaders	| Events Ingested / minute (roughly) |
| --- | --- | --- | --- |
| m5.large | 6 | 1 | 24,000 |
| m5.large	| 6	| 2	| 49,000 |
| m5.large	| 6	| 4	| 95,000 |
| m5.large	| 6	| 8	| 120,000 |
| m5.xlarge	| 6	| 8	| 180,000 |
| m5.xlarge	| 6	| 16 | 180,000 |
| m5.xlarge	| 12 | 16 | 360,000 |

Rules of Thumb (takeaways from this test):
1.  Expect 2x more throughput by doubling the number of boxes 
2.  Expect 1.5x improvement by using an instance type with twice as many vcpus and twice the memory.  
3.  Make sure that the instance type used can be attached to enough storage to hold your final index size.*
4.  Make sure that your cluster does not exceed the maximum number of nodes permitted in a cluster.*

\* See [AWS Service Limits](https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-limits.html) for these limitations.

## Storage

See [AWS Service Limits](https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-limits.html) for the maximum amount of storage that can be attached to an instance.

Cost: See the [AWS EBS Pricing Page](https://aws.amazon.com/ebs/pricing/) for the cost of EBS storage.

## Encryption
A test was set up to compare encrypted storage vs unencrypted storage. The the average document sizes were identical (495 bytes per document +/- 1 byte) for both tests.

A test was not performed on node-to-node encryption.   

# Estimates used for V1 of the DataIndex

Initial Assumptions:
1.  Documents are roughly 750 bytes per document, indexed.  (Actual test size for 402,610,760 object events was 495 bytes.)
2.  DataIndex will store one replica. (The cluster will be AZ aware to avoid data loss should a single Availability Zone go out.)
3.  Actual space to index a document is (1 + 1 replica) * 750 bytes.  To ensure that we will not run out of space when additional features are added, the size estimate will be doubled, leaving (1 + 1) * 1,500 bytes = 3,000 bytes / document.  
4.  DataIndex needs to store information on 10 billion documents, initially, with the ability to scale to over 50 billion documents.
5.  DataIndex needs to ingest at least 2,000 events per second, which the current maximum rate that DataLake can produce object events.

## Math on EC2s required to meet ingestion rates.
To keep up with datalake’s current publication speed limit of 2,000 events per second, Discovery will need at least 3 m5.2xlarge.elasticsearch boxes.  Because of the AZ’s and replication rules (to ensure no data loss), I need an even number (for two AZs), which means the real minimum is 4 m5.2xlarge.elasticsearch boxes.

## Math on Shards and Storage
Shards per node rules follow the amount of JVM memory available.  Amazon didn’t say anything about this, but Elastic’s rule is that there should be no more than 20 shards per GB of JVM memory on the machine.  The JVM takes up 99% of the memory on the AWS boxes, but to keep some head-room, I’m using a limit of 600 shards per node (32GB * 20 = 640).

To hold 10 billion events, we are going to need (10B * 3k = ) 30TB of storage.  If we hit the minimum AWS recommendation at this count we will need (30TB / 10GB) = 3,000 shards.

For shards, we need 3,000 shards / 600 = 5 data nodes, which needs to be rounded to 6 for our AZ requirement.  This is higher than the 4 that we need to keep up with load rates, so at a minimum, we need 6 data m5.2xlarge.elasticsearch data nodes.  However, 6 nodes * 1.5TB = 9TB / 3k = 3 billion events, which isn’t enough.  We’d need to hold at least 6 billion events, which puts us at a starting point of 12 data nodes.

If we’re going to need 12 nodes to start out, which can handle (600 shards per instance * 12 instances = ) 7200 shards, we can up our shard count to 6000 shards so that we can maximize our headroom (and hypothetically hit 100 billion datalake documents).

We will always need at least 3 master nodes.  The good news with master nodes is that we can scale those with a cloud formation change too, so we can use smaller nodes to start and scale them up as we get more load.  c5.large instances might work for us to begin with instead of using c5.2xlarge that I should have had in my original plan.

## Summary: Configuration
* 6000 shards
* 12 m5.2xlarge.elasticsearch data nodes
* 3 c5.large.elasticsearch master nodes
* 18 TB of storage (1.5TB * 12 data nodes) 

## Summary: Initial Capacity
* Storage: 6 - 12 billion datalake objects (12 billion if size per doc = 750 bytes)
* Hypothetical max potential storage (elastic - w/o changing shards): 100 billion datalake objects
* 9,000 events per second ingestion rate (over 30,000,000 per hour – enough to load all of DataLake in about 7 days)

## Summary: Initial Cost

### Storage
Amounts taken from [Amazon EBS Pricing](https://aws.amazon.com/ebs/pricing/)

$0.1 per GB-month of provisioned storage * 18,000 = $1,800 / mo  * 12 = 

* $21,600 / year.

### Master Nodes
Amounts taken from [Amazon Elasticsearch Service pricing](https://aws.amazon.com/elasticsearch-service/pricing/)

$0.125 per Hour * 24 * 365.25 =

* $1,095.75 / year

### Data Nodes
Amounts taken from [Amazon Elasticsearch Service pricing](https://aws.amazon.com/elasticsearch-service/pricing/)

$0.566 per Hour * 24 * 365.25 =

*  $4,961.56 / year

### Total Cost

* $27,657.31‬ / year ( ~ $2,300.00 / month )

### Potential Savings with Reserved Instances
Reserved instance pricing for m5.2xlarge.elasticsearch instances is $0.368 per hour, if paid upfront, which would reduce the cost by 35%.  The resulting cost would be $3,225.00.  This is a savings of $1,736.56 per year or roughly $145 per month.  

Given that the instance types and numbers are projections based a sample of data, the cost savings by using dedicated instances is less valuable than the flexibility to change the environment to suit our needs.  As the cluster grows and becomes more stable, the decision not to buy Reserved Instances will need to be revisited.
 