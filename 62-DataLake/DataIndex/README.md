# Introduction 

The DataIndex is an enterprise-wide Data Warehouse for storing information related to the state of content as it's being processed.

# Using the DataIndex

TODO: write this section

#### Writing to the DataIndex
#### Kibana: queries and visualizations 
#### Executing your own queries

# How the DataIndex is Implemented

The DataIndex is an implementation of the ELK stack (Elasticsearch + Logstash + Kibana). 

## Elasticsearch and Kibana

Elasticsearch and Kibana are implemented as managed AWS services.  The hardware configuration for these services is controlled by the Elasticsearch Cloudformation template found within this project.  

#### Index Configuration
The index aliases and mappings are manually created by an Administrator (see "Administrators" below).  This process was intentionally not automated.  Automated construction and destruction of indexes could lead to massive data outages that could take a very, very long time to recover.

#### Authentication and Authorization
Elasticsearch is protected by cognito, which, by default allows any user that registers to have read-only access.  Read-only access also grants registered users the ability to view any of the Kibana visualizations that Administrators create.  

##### Administrators
Registered users can be promoted to Administrators by a user that is authorized to access the Cognito User Groups in the AWS console.  This can be done by adding the user the "Administrators" group. 

Administrators have full privileges to the Elasticsearch cluster.  Authorizing a user to be an Administrator should be carefully considered and should not be handed out without necessary justification.

## Logstash
Logstash is installed on an EC2 instances that are brought up and down by an autoscaling group.  The Logstash instances read from an SQS Queue, transform the messages it receives and pushes the transformed JSON documents in the Elasticsearch Index(es).  

Which queue Logstash listens to and which Elasticsearch cluster, index and type the documents are stored to are controlled by the cloudformation template via the Jenkins deploy pipeline parameters.  

#### Autoscaling Logstash
As of the initial (MVP) release, no autoscaling policy is on the autoscaling group for logstash.  (The necessary homework was not done to project throughput of the system and system capacity.)  For now, the adjusting the number of EC2 instances executing the Logstash processes must be manually administered via deployment Jenkins pipeline parameters.

# Scaling the DataIndex cluster

TODO: write this section

# Logging

Elasticsearch application, slow index and slow search logs can be found in CloudWatch under /aws/aes/domains/${Environment}/logs/.  In order for the cloudformation templates to attach the log group to CloudWatch, the owner of the target AWS account will need to execute the following command, which grants Elasticsearch access to write into CloudWatch.

    aws logs put-resource-policy --policy-name AES-any-log --policy-document 
    '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"Service":"es.amazonaws.com"},
    "Action":["logs:PutLogEvents","logs:CreateLogStream"],"Resource":"arn:aws:logs:*"}]}' 
    --profile OWNER_PROFILE_FOR_ACCOUNT
  
  
