import unittest

from lng_datalake_constants import catalog_status, collection_status, event_names, object_status, subscription_status, \
    asset_status, owner_status, file_status, event_handler_status, ingestion_status, object_relationship_status, \
    relationship_status, ingestion_counter, republish_status, changeset_status

__author__ = "John Morelock"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"


class TestCatalogTable(unittest.TestCase):

    def test_constants(self):
        self.assertTrue(catalog_status.CREATED)

        self.assertTrue(collection_status.SUSPENDED)
        self.assertTrue(collection_status.TERMINATING_FAILED)
        self.assertTrue(collection_status.TERMINATING)
        self.assertTrue(collection_status.TERMINATED)

        self.assertTrue(event_names.COLLECTION_CREATE)
        self.assertTrue(event_names.COLLECTION_SUSPEND)
        self.assertTrue(event_names.COLLECTION_RESUME)
        self.assertTrue(event_names.COLLECTION_TERMINATE)
        self.assertTrue(event_names.COLLECTION_UPDATE)
        self.assertTrue(event_names.OBJECT_UPDATE)
        self.assertTrue(event_names.OBJECT_UPDATE_NO_CHANGE)
        self.assertTrue(event_names.OBJECT_CREATE)
        self.assertTrue(event_names.OBJECT_CREATE_PENDING)
        self.assertTrue(event_names.OBJECT_UPDATE_PENDING)
        self.assertTrue(event_names.OBJECT_CREATE_FOLDER)
        self.assertTrue(event_names.OBJECT_UPDATE_FOLDER)
        self.assertTrue(event_names.OBJECT_CREATE_INGESTION)
        self.assertTrue(event_names.OBJECT_CANCEL_INGESTION)
        self.assertTrue(event_names.OBJECT_REMOVE)
        self.assertTrue(event_names.OBJECT_REMOVE_NO_CHANGE)
        self.assertTrue(event_names.OBJECT_BATCH_REMOVE)
        self.assertTrue(event_names.OWNER_UPDATE)
        self.assertTrue(event_names.OWNER_CREATE)
        self.assertTrue(event_names.OWNER_REMOVE)
        self.assertTrue(event_names.ASSET_UPDATE)
        self.assertTrue(event_names.ASSET_CREATE)
        self.assertTrue(event_names.ASSET_REMOVE)
        self.assertTrue(event_names.SUBSCRIPTION_CREATE)
        self.assertTrue(event_names.SUBSCRIPTION_REMOVE)
        self.assertTrue(event_names.SUBSCRIPTION_UPDATE)
        self.assertTrue(event_names.TRACKING_REMOVE)
        self.assertTrue(event_names.TRACKING_COLLECTION_BLOCKER_REMOVE)
        self.assertTrue(event_names.RELATIONSHIP_CREATE)
        self.assertTrue(event_names.RELATIONSHIP_UPDATE)
        self.assertTrue(event_names.RELATIONSHIP_REMOVE)
        self.assertTrue(event_names.OBJECT_RELATIONSHIP_CREATE)
        self.assertTrue(event_names.OBJECT_RELATIONSHIP_UPDATE)
        self.assertTrue(event_names.OBJECT_RELATIONSHIP_REMOVE)
        self.assertTrue(event_names.SUBSCRIPTION_REPUBLISH_CREATE)
        self.assertTrue(event_names.CHANGESET_OPEN)
        self.assertTrue(event_names.CHANGESET_CLOSE)

        self.assertTrue(object_status.CREATED)
        self.assertTrue(object_status.REMOVED)
        self.assertTrue(object_status.PENDING)

        self.assertTrue(subscription_status.TERMINATED)
        self.assertTrue(subscription_status.TERMINATING)
        self.assertTrue(subscription_status.CREATED)
        self.assertTrue(subscription_status.PENDING)

        self.assertTrue(owner_status.CREATED)
        self.assertTrue(owner_status.REMOVED)
        self.assertTrue(owner_status.UPDATED)

        self.assertTrue(asset_status.CREATED)
        self.assertTrue(asset_status.REMOVED)
        self.assertTrue(asset_status.UPDATED)

        self.assertTrue(file_status.FILE_CREATED)
        self.assertTrue(file_status.FILE_REMOVED)
        self.assertTrue(file_status.FILE_UPDATED)

        self.assertTrue(event_handler_status.SUCCESS)
        self.assertTrue(ingestion_status.PENDING)
        self.assertTrue(ingestion_status.CANCELLED)
        self.assertTrue(ingestion_status.COMPLETED)
        self.assertTrue(ingestion_status.ERROR)
        self.assertTrue(ingestion_status.PROCESSING)
        self.assertTrue(ingestion_status.VALIDATING)

        self.assertTrue(object_relationship_status.CREATED)
        self.assertTrue(object_relationship_status.UPDATED)
        self.assertTrue(object_relationship_status.REMOVED)

        self.assertTrue(relationship_status.CREATED)
        self.assertTrue(relationship_status.UPDATED)
        self.assertTrue(relationship_status.REMOVED)

        self.assertTrue(ingestion_counter.PROCESSED)
        self.assertTrue(ingestion_counter.ERROR)
        self.assertTrue(ingestion_counter.UPDATED)
        self.assertTrue(ingestion_counter.TRACKED)

        self.assertTrue(republish_status.PENDING)
        self.assertTrue(republish_status.RETRY)
        self.assertTrue(republish_status.PROCESSING)
        self.assertTrue(republish_status.COMPLETED)
        self.assertTrue(republish_status.FAILED)

        self.assertTrue(changeset_status.OPENED)
        self.assertTrue(changeset_status.CLOSING)
        self.assertTrue(changeset_status.CLOSED)
        self.assertTrue(changeset_status.EXPIRED)


if __name__ == '__main__':
    unittest.main()
