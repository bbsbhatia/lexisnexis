__author__ = "jonathan A. Mitchall, Prashant Srivastava"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

PENDING = "Pending"
CREATED = "Created"
TERMINATING = "Terminating"
TERMINATED = "Terminated"
