__author__ = "Aaron Belvo"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

CREATED = "Created"
SUSPENDED = "Suspended"
TERMINATING = "Terminating"
TERMINATING_FAILED = "TerminatingFailed"
TERMINATED = "Terminated"

