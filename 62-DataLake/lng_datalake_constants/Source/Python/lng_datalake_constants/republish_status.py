__author__ = "John Konderla"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

PENDING = "Pending"
PROCESSING = "Processing"
COMPLETED = "Completed"
RETRY = "Retry"
FAILED = "Failed"
