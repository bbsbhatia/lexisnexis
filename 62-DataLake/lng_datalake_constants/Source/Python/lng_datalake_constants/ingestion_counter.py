__author__ = "Mark Seitter"
__copyright__ = "Copyright 2019, LexisNexis"
__version__ = "1.0"

ERROR = "ObjectErrorCount"
PROCESSED = "ObjectProcessedCount"
TRACKED = "ObjectTrackedCount"
UPDATED = "ObjectUpdatedCount"
