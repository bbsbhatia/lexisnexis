# PlantUML

We write our DataLake UMLs as code using PlantUML. Install IntelliJ's PlantUML Integration Plugin
    to view .puml files as rendered diagrams: https://plugins.jetbrains.com/plugin/7017-plantuml-integration


#### Optional: Install Graphviz To Support More Diagram Types

1. Install Graphviz for Windows stable: https://graphviz.gitlab.io/_pages/Download/Download_windows.html
2. Open one of the .puml files. Click on the wrench dropdown above the rendered diagram and select `Open Settings`
3. Set `Graphviz dot executable` to the location of your Graphviz installation.
    On my computer, this is `C:/Program Files (x86)/Graphviz2.38/bin/dot.exe`


#### Optional: Configure PlantUML Markdown Implementation

Note - Our current UMLs are not written to support the Markdown Implementation.
    (The Markdown Implementation does not support all of PlantUML syntax.)

1. Install the IntelliJ Markdown plugin: https://plugins.jetbrains.com/plugin/7793-markdown/
2. Add Markdown to `File and Code Templates`
    * Go to `File > Settings > File and Code Templates`
    * Click the plus sign and add a code template with `Name:` Markdown, `Extension:` md
3. Install PlantUML Framework for Markdown
    * Go to `File > Settings > Languages & Frameworks > Markdown`
    * Click install PlantUML Framework
    
Once this is successfully configured, you can view .md files as rendered diagrams.

What the body of the md file should look like:
    ```plantuml
    
        <PLACE UML HERE>
    ```
