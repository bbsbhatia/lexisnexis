##Steps to compile Cython package on EC2 t3.micro
These are the steps that we had to follow to compile "Orjson 2.0.6" on EC2 t3.micro instance: 
 
1. Install development tools  
    sudo yum -y groupinstall "Development Tools"
2. Install [rust](https://rustup.rs/)  
sudo curl ht<span>tps://sh.rustup.rs -sSf | sh
3. Set [Cargo](https://github.com/rust-lang/cargo)'s bin directory ($HOME/.cargo/bin) in your PATH   
    source $HOME/.cargo/env
4. Install [pyo3-pack](https://github.com/PyO3/pyo3-pack)  
sudo pip install pyo3-pack
5. Clone [Orjson](https://pypi.org/project/orjson/)  
    git clone ht<span>tps://github.com/ijl/orjson.git
6. Change current directory to Orjson   
Cd orjson
7. Change package name to lng_orjson  
 Open Cargo.toml in text editor and change attribute "name" under "package" section from "orjson" to "lng_orjson"
8. Build and compile   
    pyo3-pack build --release --strip --interpreter python3.6

## Testing the package on the same Linux box: 

1. Create python virtual environment 
python3 -m venv v_orjsob

2. Activate python virtual environment  
source v_orjson/bin/activate

3. Install the compiled package:  
pip install orjson/target/wheels/lng_orjson-2.0.6-cp36-cp36m-manylinux1_x86_64.whl
4. Run python3  
import orjson as json   
json.dumps({'foo':'bar'})

5. Should get the following output from step 4:  
b'{"foo":"bar"}'

##Steps to compile Cython package on windows 10

1. Install [Microsoft C++ build tools](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019)  
2. Install [rust](https://rustup.rs/)   
Your best option on Windows is the rust-init.exe installer. Just go to the rustup website to find it. 
3. Install [pyo3-pack](https://github.com/PyO3/pyo3-pack)    
 pip install pyo3-pack 
4. Clone [Orjson](https://pypi.org/project/orjson/)  
    git clone https://github.com/ijl/orjson.git
5. Change current directory to Orjson   
Cd orjson 
6. Change package name to lng_orjson  
 Open Cargo.toml in text editor and change attribute "name" under "package" section from "orjson" to "lng_orjson"   
7. Build and compile   
    pyo3-pack build --release --strip --interpreter python

