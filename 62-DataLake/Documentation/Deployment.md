# How to Deploy Datalake Code

What you need before you start : 
* Access to Datalake AWS dev,cert and prod environment.
    you can request access using [ESAR Self Service]( https://esar3.reedelsevier.com/identity/faces/home?_afrLoop=5405044465232058&_afrWindowMode=0&_adf.ctrl-state=161dbbgylp_299)
* Basic understanding of [Jenkins](https://jenkins.io/doc/) and CI/CD process.
* Basic understanding of [Git](https://git-scm.com/docs) and [Git Integration - Plugins from JetBrains](https://www.jetbrains.com/help/idea/using-git-integration.html).

### Working on a new feature 

#### Build
 
1. Make sure your current git branch is master.
2. Update your local master branch by pulling the latest changes from remote master.
3. Create your new feature branch using git for example: feature/demo_feature.
4. Once you finished working on your feature branch push your changes to VSTS. 
5. Go to Jenkins at http://jenkins.content.aws.lexis.com/.
6. Navigate to Wormhole project.
7. Navigate to the repo root e.g. '62-DataLake-Administration'.you will see 4 multibranch pipelines: build, deploy, regression and Teardown.
8. Click on 'Build'. 
9. Click on 'Scan Multibranch Pipeline Now'. Give it a couple of seconds and your feature branch should appear under Branches.
10. Click on your feature branch e.g. 'feature/demo_feature'.
11. Click on 'Build'. on the left down panel. you should see build job started with number 1. Wait for it to finish, next step will be deploy this build.

#### Scan Multibranch Pipeline for Regression :
Before start deploying,you should go to the Regression directory and execute a 'Scan Multibranch Pipeline Now',because it's the first time that feature branch is created.

1. Navigate back to the repo root e.g. '62-DataLake-Administration'.you can do that by clicking on '62-DataLake-Administration' at the top navigation bar. 
Jenkins -- > Wormhole --> 62-DataLake-Administration --> Build --> feature/demo_feature.
2. Click on 'Regression'.
3. Click on 'Scan Multibranch Pipeline Now'. Wait for it to finish and verify that your feature branch 'feature/demo_feature' appeared under 'Branches'.
4. Click on 'Build', it is expected that the first job will fail.

#### Deploy:
1. Navigate back to the repo root e.g. '62-DataLake-Administration', you can do that by clicking on '62-DataLake-Administration' at the top navigation bar. 
Jenkins -- > Wormhole --> 62-DataLake-Administration --> Regression --> feature/demo_feature.
2. Click on 'Scan Multibranch Pipeline Now'. Wait for it to finish and verify that your feature branch 'feature/demo_feature' appeared under 'Branches'.
3. Click on your feature branch 'feature/demo_feature'.  
4. Click on 'Build' and let it fail.this is necessary for Jenkins to define the required parameters for the second build that should succeed.
5. Refresh the current page.(F5 in Chrome, Firefox and Internet Explorer).   
6. Click on 'Build with Parameters' and you should get form with the following inputs:
    * 'releaseUnitBuild' : this is your build number from the previous build step. in our case should be 1. 
    * 'assetGroup': the environment that you are deploying into.the value should be one of the following:
        - 'staging' for Cert.
        - 'release' for prod.
        - 'feature-{your environment}'for dev. for example: 'feature-mas'.
    * 'targetAccount': this is DataLake AWS account number- for example 288044017584 is the account number for AWS Datalake dev.
    * 'lambdaList': this is the list of lambdas that you would like to deploy. leave it empty to deploy all lambdas. 
    * 'runFullRegression': this attribute can either be 'false' or 'true'. 
    When deploying to dev: setting this to true runs all DataLake regression tests, setting this to false runs only this Repo’s regression test.
Most likely you will set this to false while you are still working on your feature. 
once you think you are done developing your feature, and you are ready to create pull request- you want to make sure that all your code passes regression on dev.At this stage you should set runFullRegression flag to true and run full regression in dev.
Note when we deploy to cert and prod it ignores this parameter and always runs all Regression tests.
7. click 'Build'.
8. Smoke test your changes in dev.



### Release to Cert: 
Once you are done with your code review and have completed your pull request in VSTS, your new feature code will be merged with master.
VSTS merge process will kick off build process and deploy process to CERT.
You can find these jobs in Jenkins at {Repo}/Build/master and {Repo}/Deploy/master.
You need at this point to smoke test your changes in Cert.
Watch the deploy to cert and make sure it succeeds. Then smoke test. If the smoke test is successful, then you can deploy to prod.
### Release to prod: 


1. Go to Jenkins at 'http://jenkins.content.aws.lexis.com/'.
2. Navigate to Wormhole project. 
3. Navigate back to the repo root e.g. '62-DataLake-Administration'. 
4. Under Branches click on 'master'.
5. Click on deploy. here you will find the last job that started by VSTS. it is the job that released your code to Cert.
6. Click on that Job.
7. Click on 'rebuild' on the left panel. 
8. In the deploy form you will need to change the deployment information to point to DataLake prod. 
    - Set 'assetGroup' to 'release'.
    - Set 'targetAccount' to '195052678233' :this is the datalake production account.
9. Click on 'Rebuild'.
10. Watch your job until it reaches the 'Approvals' stage,click 'approve'. 

Your code will be released to DataLake prod once the job is finished successfully.Next step is to smoke test prod.

 ### Known issues:
- Issue1: DynamoDB indexes creations or updates can only execute serially for the same table.
In other words, an update will be initiated and the following one can't execute until the first has completed.  
If a second update is attempted before the first can complete, an exception will be raised.  
Solution : if you have changes to more than one index for the same table, you have to deploy one change at a time.
- Issue2 : When you deploy, Jenkins doesnt't show 'Build with parameters' option for first time deployment.  
Solution: Click on Build and let it fail, then refresh your page (hit F5 in Chrome, Firefox or Internet Explorer).
 

### Troubleshooting:

#### Deployment failed at 'Update Remote Documentation' step :
This problem only could happen when deploying one of the datalake APIs (administrator, collection, object, catalog),so any repo other than '62-DataLake-Core'.
Solution :   
1. In the 'Build History' panel, find your Job number and click on the arrow next to it. You should get dropDown list with several options.
2. Click on 'Restart from Stage'.
3. From the 'Stage name' dropDown list choose 'Update Remote Documentation'.
4. Click 'Run'.     
Note: The reason we used 'Restart from Stage' as a solution vs re-deploying is that
we can't promote the same build to v1 multiple times.

#### CloudFront Failure to Update Cache Behavior Through CloudFormation
This problem occurs when there is a Origin Group mapped to a behavior in the 
Distribution and an update is being issued for the Distribution through CloudFormation.
The error `Cannot use an API version earlier than 2018-11-05 to change the cache behavior's target origin from origin group`
will be presented. To fix this and finish the deploy follow the below steps:
1. In the CloudFront console select your Web Distribution
2. Navigate to the `Behaviors` tab
3. Select path `/objects/store/*` and Edit
4. Change `Origin or Origin Group` from Object-Store-Buckets to object-store-use1-bucket
5. Select Yes, Edit
6. Select path `legacy/objects/store/*` and Edit
7. Change `Origin or Origin Group` from DataLake-Object-Legacy-Buckets to US-East-1-Bucket
8. Select Yes, Edit
9. Run CloudFormation stack updates (redeploy UsagePlans)
