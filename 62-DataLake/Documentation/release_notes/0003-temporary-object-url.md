# Immediate Access to DataLake Objects
2019-10-30

## What's New?
New "temporary-object-key-url" property will be returned in the responses of the below APIs to allow immediate access
to the object (see limitations below):

  * CreateObject (POST /{objectId})
  * UpdateObject (PUT /{objectId})
  * CreateMultipartObject (POST /{objectId}/multipart)
  * UpdateMultipartObject (PUT /{objectId}/multipart)
  * CompleteFolderTransaction (POST /{objectId}/folder/{transactionId}/complete)

Below is an example of a CreateObject API response with the new "temporary-object-key-url":
```json5
{
  "context": {
    "request-id": "446d33a0-bc88-408e-92fc-733a3b44bb07",
    "request-time-epoch": "1572025691237",
    "api-id": "01oew1oab0",
    "resource-id": "29xkj0",
    "stage": "v1"
  },
  "object": {
    "object-state": "Created",
    "collection-id": "dataindexheitkadjtest",
    "collection-url": "/collections/v1/dataindexheitkadjtest",
    "object-id": "alterego",
    "object-url": "/objects/v1/alterego?collection-id=dataindexheitkadjtest",
    "temporary-object-key-url": "https://datalake.content.aws.lexis.com/objects/temp/76e8b564ee14d2ff08d622ed6aa32f36d905924e/da0f8a0c8574bf967e6bc1df1ccb270b0a68a6a9",
    "owner-id": 2405,
    "asset-id": 62
  }
}
```

##### Limitations on temporary location
1) Will be available immediately for the Create/UpdateObject and CompleteFolderTransaction APIs.
2) Will not be available until the S3 multipart upload is completed for the Create/UpdateMultipartUpload APIs.
3) Will only be guaranteed to be available for **four** hours.
4) Will not meet High Availability requirements.
5) Will not include object metadata.

## Why?
Some publishers require immediate access to objects stored in the DataLake. These publishers do not have the luxury of
waiting for the DataLake's native, eventual consistency architecture to process object creates and updates.