# API Response request-id and request-time
2019-12-02

## What's New?
We've added request-id and request-time to DataLake API responses, event notifications, and republish
notifications. These are part of the "context" object, which contains information about the request.
request-time is in UTC time, and is in ISO 8601 format:
```
YYYY-MM-DDThh-mm-ss.sssZ
```


## Why?
DataLake is eventually consistent. If you make a number of requests on a single object - Create, Update, and Remove -
DataLake will process the requests in order. However, you may not receive event notifications in the same
order that the requests were sent and processed.

Using request-id and request-time, you can match the response of an accepted request to its event notification.
This can help you process event notifications in order.

Example Create Object Success response:
<pre>
{
  "context": {
    <b>"request-id": "83210435-b599-49f3-aff8-39b49b09c83e",</b>
    <b>"request-time": "2019-11-26T14:43:15.141Z",</b>
    "request-time-epoch": "1574779395141",
    "api-id": "bg0w8osd35",
    "resource-id": "42spn7",
    "stage": "v1"
  },
  "object": {
    "object-state": "Created",
    "collection-id": "sample-collection",
    "collection-url": "/collections/v1/sample-collection",
    "object-id": "sample-object",
    "object-url": "/objects/v1/sample-object?collection-id=sample-collection",
    "temporary-object-key-url": "https://datalake-staging.content.aws.lexis.com/objects/temp/66945d091a1f7fb0b2bb931e07eb87c43c4a09a6/ddf0630ff4c78cbe36e45b0badab34953ec31e43",
    "owner-id": 1395,
    "asset-id": 62,
    "object-expiration-date": "2019-12-03T14:43:15.000Z"
  }
}
</pre>

Corresponding event notification:
<pre>
{
    "v1": {
        "SchemaVersion": "v1",
        "Schema": "https://datalake.content.aws.lexis.com/schemas/v1/create-object.json",
        "Type": "Publish",
        "context": {
            <b>"request-id": "83210435-b599-49f3-aff8-39b49b09c83e",</b>
            <b>"request-time": "2019-11-26T14:43:15.141Z"</b>
        },
        "object": {
            "object-id": "sample-object",
            "collection-id": "sample-collection",
            "object-state": "Created",
            "content-type": "application/xml",
            "raw-content-length": 71,
            "version-number": 1,
            "version-timestamp": "2019-11-26T14:43:15.141Z",
            "event-name": "Object::Create",
            "S3": {
                "Key": "66945d091a1f7fb0b2bb931e07eb87c43c4a09a6/ddf0630ff4c78cbe36e45b0badab34953ec31e43",
                "Bucket": "staging-dl-object-store-use1"
            },
            "object-key-url": "https://datalake-staging.content.aws.lexis.com/objects/store/66945d091a1f7fb0b2bb931e07eb87c43c4a09a6/ddf0630ff4c78cbe36e45b0badab34953ec31e43",
            "replicated-buckets": [
                "staging-dl-object-store-usw2"
            ],
            "object-expiration-date": "2019-12-03T14:43:15.000Z"
        }
    }
}
</pre>
Note the request-id and request-time match.

The response of a failed request contains request-id but not request-time. Example Create Collection Failure response:
<pre>
{
  "context": {
    <b>"request-id": "35131b70-2f64-4a61-825f-5fb44769c758",</b>
    "request-time-epoch": "1574873460526",
    "api-id": "lowwb4azbi",
    "resource-id": "7rkc67",
    "stage": "v1"
  },
  "error": {
    "message": "Invalid request body",
    "type": "BAD_REQUEST_BODY",
    "corrective-action": "[object has missing required properties ([\"asset-id\"])]"
  }
}
</pre>
A failed request will not trigger an event notification.

Republish notifications have a request-id and request-time that matches the Create Republish request.

Note this and other recent feature additions to DataLake Subscriptions are restricted to the new Subscription v1 schema.
We're trying to migrate everyone from v0 to v1. If you would like help, you can get in touch using [DataLake's support page](https://teams.microsoft.com/l/channel/19%3a8644e959bc3645d3bd304e53dbc7dfc4%40thread.skype/Support?groupId=18303be8-7c0e-4497-9ed5-b43e87f30194&tenantId=9274ee3f-9425-4109-a27f-9fb15c10675d).


## Links
[DataLake Swagger Staging - Create Subscription](https://datalake-staging.content.aws.lexis.com/subscription.html#/Subscriptions/CreateSubscription)

DataLake Event Notification v1 Schemas:
 - [create object notification](https://datalake.content.aws.lexis.com/schemas/v1/create-object.json)
 - [update object notification](https://datalake.content.aws.lexis.com/schemas/v1/update-object.json)
 - [remove object notification](https://datalake.content.aws.lexis.com/schemas/v1/remove-object.json)
 - [update collection notification](https://datalake.content.aws.lexis.com/schemas/v1/update-collection.json)
 - [remove collection notification](https://datalake.content.aws.lexis.com/schemas/v1/remove-collection.json)
