# Object::UpdateNoChange Events
2019-11-01

## What's New?
Subscribe to Object::UpdateNoChange events. Receive a notification when DataLake finishes processing 
an Object request, but it does not produce any changes.

## Why?
A Subscription to Object::Update events will only trigger a notification if the given request produces a change
to the underlying Object's content, content-type, or metadata. That way, Subscribers aren't bothered with notifications
if there is nothing new to process. It means that if you make the same request multiple times, DataLake will send
out a notification once the original request is finished, and ignore the other requests.

Content Publishers asked, 'If a duplicate request does not trigger a notification, then how can I tell when the DataLake
is done processing it?' In response, we've added Subscriptions to Object::UpdateNoChange events.

A sample request to subscribe to Object::UpdateNoChange events:
```
POST / Subscriptions
{
  "filter": {
    "collection-id": [
      "flowers"
    ],
    "event-name": [
      "Object::UpdateNoChange"
    ]
  },
  "endpoint": "my::sqs_queue::arn",
  "protocol": "sqs",
  "subscription-name": "string",
  "description": "string",
  "schema-version": [
    "v1"
  ]
}
```

A sample notification to an Object::UpdateNoChange event:
```
{
    "v1": {
        "SchemaVersion": "v1",
        "Schema": "https://datalake.content.aws.lexis.com/schemas/v1/update-object.json",
        "Type": "Publish",
        "context": {
            "request-time": "2019-10-30T22:41:21.629Z",
            "request-id": "46581784-753b-475a-bf8e-71cd660c2689"
        },
        "object": {
            "object-id": "posie",
            "collection-id": "flowers",
            "object-state": "Created",
            "content-type": "application/xml",
            "raw-content-length": 71,
            "version-number": 1,
            "version-timestamp": "2019-10-30T22:40:22.464Z",
            "event-name": "Object::UpdateNoChange",
            "S3": {
                "Key": "b342eef4be56e5efys4affae58ac8281885760f9/a7b840e43482517eire3oda22oe29e79f1ab1847",
                "Bucket": "release-dl-object-store-use1"
            },
            "object-key-url": "https://datalake.content.aws.lexis.com/objects/store/b342eef4be56e5efys4affae58ac8281885760f9/a7b840e43482517eire3oda22oe29e79f1ab1847",
            "replicated-buckets": [
                "release-dl-object-store-usw2"
            ],
            "object-expiration-date": "2019-11-06T22:40:22.000Z"
        }
    }
}
```
"context" - contains information about the request that kicked off the notification.

"object" - contains data about the underlying DataLake Object. Note that an Object::UpdateNoChange event does not change
the underlying DataLake object, including its version-number and version-timestamp.

The current full list of DataLake events you can subscribe to:
```
[ "Object::Create", "Object::Update", "Object::UpdateNoChange", "Object::Remove", "Collection::Resume",
"Collection::Suspend", "Collection::Update", "Collection::Terminate" ]
```

## Links
[DataLake Swagger Staging - Create Subscription](https://datalake-staging.content.aws.lexis.com/subscription.html#/Subscriptions/CreateSubscription)
