# New Object Metadata
2019-10-04

## What's New?
A limited amount of user metadata can be optionally passed to the the following object APIs:
  * CreateObject (POST /{objectId})
  * UpdateObject (PUT /{objectId})
  * CreateMultipartObject (POST /{objectId}/multipart)
  * UpdateMultipartObject (PUT /{objectId}/multipart)

New optional headers will be used for specifying object metadata to the above 4 object APIs that support metadata. These
headers must start with "x-dl-meta-". Below is an example:
   ```text
   x-dl-meta-name1: value1
   x-dl-meta-name2: value2
   ```

The above example will result in 2 pieces of metadata: name1 and name2.

Restrictions on "x-dl-meta-" headers:
  * Cannot use "x-dl-meta-dl-" prefix for any metadata key since this is reserved for DataLake metadata. For example,
x-dl-meta-dl-name would be invalid.
  * Limited to a total of 1024 bytes which includes the total length of all user provided header names and values.
 
Object metadata will be available to clients in the following ways:
  * Subscription Publish and Republish messages for Object::Create and Object::Update (v1 schema only).
  * DescribeObjects API (GET /{objectId}/describe). Object metadata will be returned in the "object-metadata" property.
   Below is an example:
  ```json5
  {
  "object-metadata" :
    {
    "name1": "value1",
    "name2": "value2" 
    }
  }
  ```
  * GetObject API (GET /{objectId}). Object metadata will be in response headers that are prefixed with "x-dl-meta-" 
  (same prefix used on request headers when metadata was added with Create/UpdateObject APIs). DataLake specific 
  metadata will also be present and prefixed with "x-dl-meta-dl-".
  * Accessing the S3 objects directly using the "object-key-url" returned in the DescribeObjects and ListObjects APIs. 
  Metadata will be returned in headers and will be prefixed with "x-dl-meta-" (same prefix used on request headers when 
  metadata was added with Create/UpdateObject APIs). DataLake specific metadata will also be present and prefixed with
  "x-dl-meta-dl-".
  * Performing a head or get operation on the S3 object using AWS REST APIs (either directly, using an SDK, CLI or 
  console). Object metadata will be in response headers that are prefixed with "x-amz-meta-".

## Links
[DataLake Object Metadata Documentation](https://datalake-staging.content.aws.lexis.com/#ObjectMetaData)

[Subscription Schema for CreateObject (v1)](https://datalake.content.aws.lexis.com/schemas/v1/create-object.json)

[Subscription Schema for UpdateObject (v1)](https://datalake.content.aws.lexis.com/schemas/v1/update-object.json)
