# Create Catalog Description as query-param
2019-12-04

## What's New?
Create Catalog (POST) request has been updated to accept Description as query-param instead of body. This change is 
backwards compatible.

## Why?
This change has been made for consistency across all Datalake APIs.

## What does this Mean to You?
Current users of Create Catalog will still be able to pass the Description in body. This functionality will be 
supported until 2020-02-01. Users are requested to update their code to pass Description as query-param.
If you would like help, you can get in touch using [DataLake's support page](https://teams.microsoft.com/l/channel/19%3a8644e959bc3645d3bd304e53dbc7dfc4%40thread.skype/Support?groupId=18303be8-7c0e-4497-9ed5-b43e87f30194&tenantId=9274ee3f-9425-4109-a27f-9fb15c10675d).

## Links
[DataLake Swagger Staging - Create Catalog](https://datalake-staging.content.aws.lexis.com/catalog.html#/Catalogs/CreateCatalog)