<!---
DataLake OpenAPI3
10/04/19
-->

# What's New?
## OpenAPI3 DataLake API
DataLake APIs now are published in [OpenApi 3.0 Specification](http://spec.openapis.org/oas/v3.0.2). OpenAPI3 has 
[new features](https://swagger.io/blog/news/whats-new-in-openapi-3-0/) and fixes issues with the old DataLake APIs, 
which were published in Swagger 2.0 Specification. DataLake API Swagger2 Schemas are now deprecated.

&nbsp;

You can download the OpenApi3 DataLake API Schemas from DataLake Swagger UI.

&nbsp;

## OpenAPI3 DataLake Client - [Video Attached](https://reedelsevier.sharepoint.com/:v:/s/OG-Wormhole658/ETrbMdXoQbtKqOxpisVtvN0BBPbc3-nl0glbVToyqQwxfw?e=Ypx9Rr)
Generate a DataLake Client written in the programming language of your choice from DataLake API Schemas using [Swagger Editor](https://editor.swagger.io/).

&nbsp;

A couple breaking changes from the old Client:
* Changed parameter order in some functions.
* Parameters named like *_request were renamed to body.
* Some schema validation is no longer handled within the Client. DataLake will return the error.
These will not impact you until you regenerate your DataLake Client using the latest DataLake API Schemas.

&nbsp;

## Postman Integration
Import DataLake API Schemas into [Postman](https://www.getpostman.com/). Postman is a free application that makes HTTP 
Requests easy.

&nbsp;

## OpenAPI3 [DataLake Swagger UI](https://datalake-staging.content.aws.lexis.com/) - [Video Attached](](https://reedelsevier.sharepoint.com/:v:/s/OG-Wormhole658/ETvo-BRBbi1OmqCK_UlPp20BCRIcI0M87YSZQUA9wqUvdw?e=1MbOcg))
Upload files using Create Object and Update Object. This is supported for Image files, Video Files, and Audio Files. 
The file upload widget appears when you select the corresponding content-type using the Response body dropdown.

&nbsp;

When you Get an Object that is an image, it displays directly in the UI.

&nbsp;

Request and response models are much better documented in the UI.

&nbsp;

## Other Recent Improvements
We now have API grouping according to resource. This provides a better user experience for both DataLake Swagger UI and 
DataLake Client. Example: DataLake Client package ObjectsAPI was broken up into ObjectsApi, MultipartObjectsApi, 
IngestionApi, and FolderObjectsApi.

&nbsp;

Most DataLake Client functions were given more descriptive names, i.e. root_get() is now list_objects().

&nbsp;

## Video Attachments
The first video shows how to generate a DataLake Client in the programming language of your choice. The second video 
shows how to use DataLake Swagger UI.
