# 1. Consolidate Repo's 

Date: 2019-07-08

## Status

Accepted

## Context

Tooling and the ability to isolate deployments by API and package. The need to have multiple repo's is 
no longer needed.

## Decision

Consolidate all of 62-Datalake Repos into a single repo.

## Consequences

All 62-Datalake repos will reside in this one repo.
- Deployments must be done at module level
- Pull request must not cross boundaries  
