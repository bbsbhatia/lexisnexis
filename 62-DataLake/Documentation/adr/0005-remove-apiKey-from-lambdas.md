
# Remove apiKey from Lambdas

Date: 2019-10-08

## Status:  Accepted

* Deciders: Planet X (DataLake)

## Context
Currently, we pass the user provided apiKey into the lambda to handle authorization. Changing this will improve our
security profile.

## Decision
Moving forward, we will pass the Api Gateway provided apiKeyId into the lambda instead of the user provided apiKey.
We will do authorization by comparing the Api Gateway provided apiKeyId against the apiKeyId we already have stored in
the Owner Table.

## Consequences
* Owner Table, new index on apiKeyId.
* Pass $context.identity.apiKeyId through VM templates, request models, and request schemas.
* Remove $context.identity.apiKey from VM templates, request models, and request schemas.
* Authorize using the apiKeyId index instead of the apiKey index.
* This needs a double release.
    * First release, pass api-id and api-key-id into the lambda.
    * Second release, remove api-id, authorize using api-key-id

## Links
* [Version One Story S-81279](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story%3A911841)
