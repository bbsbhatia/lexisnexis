
# Specify the name of Lambda Functions in Datalake 

Date: 2019-09-05

## Status: Accepted 

* Deciders: Planet X (DataLake) 

## Context
Currently we don't specify the name of lambda functions in our CloudFormation templates. Aws CloudFormation generates
 one. This makes it difficult to support the system when looking at the logs.
 
Examples of autogenerated names:  
* release-DataLake-ProcessIngestionZi-LambdaFunction-1FVTQMA54UI8I (ProcessIngestionZipObjectLambda)
* release-DataLake-ProcessIngestionZi-LambdaFunction-12XJP85S9P5B4 (ProcessIngestionZipLambda)
* release-DataLake-ProcessIngestionZi-LambdaFunction-14PFU22CTE74  (ProcessIngestionZipPartLambda)  

The challenge is to update the name of our lambdas without interruption in the system. In order to do that we have
 two options:
1. Utilizing the “UpdateReplacePolicy” attribute to retain the existing physical instance of the lambda and its logs
 (we will apply the attribute on the lambda resource and the log resource) when it is replaced during a stack update
  operation.
2. Create a new Lambda resource and all the other related resources (like lambda aliases) with the lambda name
 specified and change the lambda output parameter in the CF template to point to the new lambda resource.
 
## Decision
Option 1 will be used for <b>Object</b>, <b>Catalog</b>, <b>Subscription</b>, <b>EventDispatcher</b> and
 <b>TrackingTableDeleteLambda</b> since the cloud formation templates for these API's follow the current naming
  standard.

Option 2 will be used for <b>Admin</b> and <b>Collection</b> since the cloudFormation templates for these API's
 do not follow the current naming standard. Renaming the templates will cause a new cloudFormation template and
 associated resources to be created.
 
<b>LambdaEdgeRewriteUrl</b> will not be renamed since it is an API Gateway Lambda with unique deployment requirements.

The following table will be used to rename the Lambdas :  
 
| Module                    | template name                        | LambdaName                                         |
|---------------------------|--------------------------------------|----------------------------------------------------|
| Object                    | CancelIngestionCommand               | release-object-command-CancelIngestion             |
| Object                    | CancelIngestionEventHandler          | release-object-event-CancelIngestion               |
| Object                    | CreateFolderUploadCommand            | release-object-command-CreateFolderUpload          |
| Object                    | CreateIngestionCommand               | release-object-command-CreateIngestion             |
| Object                    | CreateIngestionEventHandler          | release-object-event-CreateIngestion               |
| Object                    | CreateObjectCommand                  | release-object-command-CreateObject                |
| Object                    | CreateObjectEventHandler             | release-object-event-CreateObject                  |
| Object                    | CreateObjectMultipartUploadCommand   | release-object-command-CreateObjectMultipartUpload |
| Object                    | DescribeObjectsCommand               | release-object-command-DescribeObjects             |
| Object                    | FinishFolderUploadCommand            | release-object-command-FinishFolderUpload          |
| Object                    | FolderObjectDeletionLambda           | release-object-FolderObjectDeletion                |
| Object                    | FolderUploadDispatcher               | release-object-FolderUploadDispatcher              |
| Object                    | GetIngestionCommand                  | release-object-command-GetIngestion                |
| Object                    | GetObjectCommand                     | release-object-command-GetObject                   |
| Object                    | GetObjectRelationshipCommand         | release-object-command-GetObjectRelationship       |
| Object                    | GlueDispatcherLambda                 | release-object-GlueDispatcher                      |
| Object                    | ListObjectsCommand                   | release-object-command-ListObjects                 |
| Object                    | ObjectDeletionListenerLambda         | release-object-ObjectDeletionListener              |
| Object                    | ProcessIngestionCounterLambda        | release-object-ProcessIngestionCounter             |
| Object                    | ProcessIngestionZipLambda            | release-object-ProcessIngestionZip                 |
| Object                    | ProcessIngestionZipObjectLambda      | release-object-ProcessIngestionZipObject           |
| Object                    | ProcessIngestionZipPartLambda        | release-object-ProcessIngestionZipPart             |
| Object                    | ProcessLargeObjectLambda             | release-object-ProcessLargeObject                  |
| Object                    | RemoveObjectCommand                  | release-object-command-RemoveObject                |
| Object                    | RemoveObjectEventHandler             | release-object-event-RemoveObject                  |
| Object                    | RemoveObjectsCommand                 | release-object-command-RemoveObjects               |
| Object                    | RemoveObjectsEventHandler            | release-object-event-RemoveObjects                 |
| Object                    | UpdateFolderUploadCommand            | release-object-command-UpdateFolderUpload          |
| Object                    | UpdateObjectCommand                  | release-object-command-UpdateObject                |
| Object                    | UpdateObjectEventHandler             | release-object-event-UpdateObject                  |
| Object                    | UpdateObjectMultipartUploadCommand   | release-object-command-UpdateObjectMultipartUpload |
| admin                     | CreateOwnerEventHandlerLambda        | release-admin-event-CreateOwner                    |
| admin                     | CreateOwnerLambda                    | release-admin-command-CreateOwner                  |
| admin                     | CreateRelationshipEventHandlerLambda | release-admin-event-CreateRelationship             |
| admin                     | CreateRelationshipLambda             | release-admin-command-CreateRelationship           |
| admin                     | GetAssetLambda                       | release-admin-command-GetAsset                     |
| admin                     | GetOwnerLambda                       | release-admin-command-GetOwner                     |
| admin                     | ListAssetsLambda                     | release-admin-command-ListAssets                   |
| admin                     | ListOwnersLambda                     | release-admin-command-ListOwners                   |
| admin                     | RemoveOwnerEventHandlerLambda        | release-admin-event-RemoveOwner                    |
| admin                     | RemoveOwnerLambda                    | release-admin-command-RemoveOwner                  |
| admin                     | UpdateOwnerEventHandlerLambda        | release-admin-event-UpdateOwner                    |
| admin                     | UpdateOwnerLambda                    | release-admin-command-UpdateOwner                  |
| admin                     | UpdateRelationshipEventHandlerLambda | release-admin-event-UpdateRelationship             |
| admin                     | UpdateRelationshipLambda             | release-admin-command-UpdateRelationship           |
| catalog                   | CreateCatalogCommand                 | release-catalog-command-CreateCatalog              |
| catalog                   | CreateCatalogEventHandler            | release-catalog-event-CreateCatalog                |
| catalog                   | DescribeCatalogsCommand              | release-catalog-command-DescribeCatalogs           |
| catalog                   | GetCatalogCommand                    | release-catalog-command-GetCatalog                 |
| catalog                   | ListCatalogsCommand                  | release-catalog-command-ListCatalogs               |
| catalog                   | RemoveCatalogCommand                 | release-catalog-command-RemoveCatalog              |
| catalog                   | RemoveCatalogEventHandler            | release-catalog-event-RemoveCatalog                |
| catalog                   | UpdateCatalogCommand                 | release-catalog-command-UpdateCatalog              |
| catalog                   | UpdateCatalogEventHandler            | release-catalog-event-UpdateCatalog                |
| Subscription              | CreateRepublishCommand               | release-subscription-command-CreateRepublish       |
| Subscription              | CreateRepublishEventHandler          | release-subscription-event-CreateRepublish         |
| Subscription              | CreateSubscriptionCommand            | release-subscription-command-CreateSubscription    |
| Subscription              | CreateSubscriptionEventHandler       | release-subscription-event-CreateSubscription      |
| Subscription              | GetRepublishCommand                  | release-subscription-command-GetRepublish          |
| Subscription              | GetSubscriptionCommand               | release-subscription-command-GetSubscription       |
| Subscription              | GlueRepublishProcessor               | release-subscription-GlueRepublishProcessor        |
| Subscription              | ListSubscriptionsCommand             | release-subscription-command-ListSubscriptions     |
| Subscription              | RemoveSubscriptionCommand            | release-subscription-command-RemoveSubscription    |
| Subscription              | RemoveSubscriptionEventHandler       | release-subscription-event-RemoveSubscription      |
| Subscription              | RepublishSubscriptionEvents          | release-subscription-GetRepublishStatus            |
| Subscription              | UpdateSubscriptionCommand            | release-subscription-command-UpdateSubscription    |
| Subscription              | UpdateSubscriptionEventHandler       | release-subscription-event-UpdateSubscription      |
| EventDispatcher           | EventDispatcher                      | release-EventDispatcher                            |                     |
| Collection                | CreateCollectionEventHandlerLambda   | release-collection-event-CreateCollection          |
| Collection                | CreateCollectionLambda               | release-collection-command-CreateCollection        |
| Collection                | GetCollectionLambda                  | release-collection-command-GetCollection           |
| Collection                | ListCollectionsLambda                | release-collection-command-ListCollections         |
| Collection                | RemoveCollectionEventHandlerLambda   | release-collection-event-RemoveCollection          |
| Collection                | RemoveCollectionLambda               | release-collection-command-RemoveCollection        |
| Collection                | RemoveCollectionObjects              | release-collection-RemoveCollectionObjects         |
| Collection                | UpdateCollectionEventHandlerLambda   | release-collection-event-UpdateCollection          |
| Collection                | UpdateCollectionLambda               | release-collection-command-UpdateCollection        |
| TrackingTableDeleteLambda | TrackingTableDeleteLambda            | release-TrackingTableDelete                        |

## Consequences
###Option1 :  
 
Pros :
* No changes to the cloudFormation template names.

Cons:
* Old lambdas and logs are removed from AWS cloudFormation's scope. Which means that we have to manually clean
 up of the old lambdas and logs after deployment.
* Many resources will need to be retained in the cloudFormation templates.

###Option2 :  
 
Pros :
* Standardizes the cloudFormation template names to be consistent with the latest template naming standard.
* Nothing needs to be retained from the old cloudFormation templates.

Cons : 
* Old cloudFormation templates and resources will be retained and need to be cleaned up after deployment.
* Event dispatcher will have to be paused during deployment since both the old and the new event handlers will be
active during deployment.
* The old event handler input sources will need to be removed to prevent them from running after release.  
* An input step will be required to pause the deployment just before regression runs so the old event handlers can
be disabled and the event dispatcher enabled. This input step will need to be removed from the deployment pipeline
in a subsequent release.

## Links
* [UpdateReplacePolicy Attribute](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-attribute-updatereplacepolicy.html)
* [AWS Lambda FunctionName](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-lambda-function.html#cfn-lambda-function-functionname)
* [V1 story S-52258](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:524709)