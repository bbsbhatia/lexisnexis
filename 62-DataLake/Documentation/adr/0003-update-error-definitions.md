
# Update error definitions for NotFound

Date: 2019-07-31

## Status: accepted 

* Deciders: Planet X (DataLake) 

## Context
Currently for resources that can't be found within the DL we return a 400, however users have complained because they
are expecting a 404 to be returned.

## Decision
For the following conditions we will return a 404 and the Type will be determined following aws s3 wording
* resource in a path does not exist 
  * NoSuchAsset
    * /assets/<b>{assetId}</b>
    
* required query params for a resource do not exist 
  * NoSuchCollection
    * /objects/{objectId}?<b>collection-id="foo"</b>
* optional query params that are needed to fully qualify a resource
  * NoSuchObjectVersion
    * /objects/{objectId}?collection-id="foo"&<b>version-number=5</b>

"

## Consequences
API Gateway only allows one error pattern mapped to a single HTTP response code.  All our exceptions will need to be 
updated internally to return the HTTP code as the first element, and the API Gateway needs to be updated to use the following
pattern: ^{httpcode}.* to allow multiple Notfound codes to be returned.

## Links
* [S3 error defintions](https://docs.aws.amazon.com/AmazonS3/latest/API/ErrorResponses.html)
* [V1 story S-73369](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:808856)
