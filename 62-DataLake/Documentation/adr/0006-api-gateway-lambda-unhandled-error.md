
# How we are to handle uncaught exceptions from Lambda to API Gateway

Date: 2019-10-08

## Status:  Accepted

* Deciders: Planet X (DataLake)

## Context
Currently if an unhandled exception occurs within our Lambda code (like a package import error) or the Lambda task times
 out we do not return an appropriate response code or error message.  These errors are instead sent
back to the client as a 20X with an empty string for the body portion. Note: This also applies to missing API Gateway 
Integration error codes. We need to have a "fail-safe" to handle any uncaught exceptions.

## Decision
We will apply logic within the VM template to check for the existence of the errorMessage which is how Lambda sends
errors back to API Gateway.  If this exists then we will override the response headers to be a 500 internal error.  This
guarantees that any uncaught or undeclared error from Lambda will return a 500 rather than a 2XX

ie:
```
#set($inputRoot = $input.path('$'))
{
   "context": {
      "request-id" : "$context.requestId",
      "request-time-epoch" : "$context.requestTimeEpoch",
      "api-id" : "$context.apiId",
      "resource-id" : "$context.resourceId",
      "stage" : "$context.stage"
   },
#if($inputRoot.errorMessage != "")
    #set($context.responseOverride.status = 500)
    "error" : {
        "message": "Unhandled Internal Exception",
        "type": "InternalError"
    }
#else
    "objects" : $input.json('$.response.objects'),
    "item-count": $input.json('$.response.item-count')
    #if($inputRoot.response.next-token != "")
       ,"next-token" : $input.json('$.response.next-token')
    #end
#end
}
```

## Consequences
* All success (2xx) API Gateway VM template responses will need to have this change put in.

## Links
* [Version One Story S-926413](https://www4.v1host.com/LexisNexis/story.mvc/Summary?oidToken=Story:926413)
