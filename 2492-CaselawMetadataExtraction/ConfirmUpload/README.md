## ConfirmUpload lambda

### Description
Handles upload confirmation event for s3 PUTs to case law extraction bucket.

### Dependencies
#### Python libraries
```bash
pip install -r requirements.txt
```
### How to run unit tests
#### Step 1: Install dependencies (see above)
#### Step 2: Run the test script
```bash
./run_tests
...
...
----------------------------------------------------------------------
Ran 2 tests in 0.013s

OK
Name                     Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------------
Source/Lambda/index.py      12      0      2      0   100%
```