import boto3
import json
import logging
import os
import time
import sqlalchemy
import re


logger = logging.getLogger()
if len(logger.handlers) > 0:
    for log_handler in logger.handlers:
        logger.removeHandler(log_handler)
logger.setLevel(logging.INFO)

engine = None


def get_secret(secret_id: str, version_id: str = None, version_stage: str = None) -> dict:
    """Get DB creds
    
    Retrieves DB credentials from AWS secrets manager.

    Args:
        (str) secret_id: id of secret to retrieve
        (str) version_id: version id of secret to retrieve
        (str) version_stage: version stage of secret to retrieve

    Returns: 
        (dict) mapping of secret keys to secret values.
    """
    client = boto3.client("secretsmanager")
    params = {"SecretId": secret_id}
    if version_id:
        params["VersionId"] = version_id
    if version_stage:
        params["VersionStage"] = version_stage
    response = client.get_secret_value(**params)
    return json.loads(response["SecretString"])


def mark_as_uploaded(
    engine: sqlalchemy.engine.base.Engine, doc_id: str
) -> sqlalchemy.engine.ResultProxy:
    """Marks document as uploaded 

    Marks a document as uploaded by removing its ttl value and setting the upload 
    confirmed timestamp.
    
    Args:
        (sqlalchemy.engine.base.Engine) engine: SQLAlchemy engine object.
        (str) doc_id: Document identifier.

    Returns:
        (sqlalchemy.engine.ResultProxy) Update result proxy.
    """

    sql = sqlalchemy.text(
        """
        UPDATE 
            caselaw_metadata_extraction.document_tracking 
        SET 
            ttl = NULL, 
            upload_confirmed_timestamp=NOW() 
        WHERE 
            id = :id
        """
    )
    conn = engine.connect()
    result = conn.execute(sql, id=doc_id)
    conn.close()

    return result


def handler(event: dict, context: dict) -> None:
    """Request handler

    Handles s3 object PUT events. Pulls the document ID off of the event and uses it
    to identify and update the corresponding dynamodb record in order to remove the 
    time-to-live attribute.
    
    Args:
        (dict) event: S3 Event object.
        (dict) context: Runtime context metadata.

    Returns:
        None
    """
    global engine
    if not engine:
        creds = get_secret(os.environ["RDS_SECRET_ARN"])
        conn_str = "mysql+pymysql://{username}:{password}@{host}:{port}".format(**creds)
        engine = sqlalchemy.create_engine(conn_str)

    logging.info(event)
    for record in event["Records"]:
        print(record)
        try:
            document_id_key = record["s3"]["object"]["key"]
            # reverse, strip the four ext chars, reverse
            document_id = document_id_key[::-1][4:][::-1]
            mark_as_uploaded(engine, document_id)
        except Exception:
            logging.exception(f"Failed to remove TTL for incoming record: {record}")
            raise
