import unittest

from unittest.mock import patch, Mock
from index import handler, get_secret, mark_as_uploaded
from contextlib import contextmanager


class ConfirmUploadTests(unittest.TestCase):
    @contextmanager
    def assertNotRaises(self, exception):
        try:
            yield None
        except exception:
            raise self.failureException("{0}".format(exception.__name__))

    @patch("index.boto3")
    def test_get_secret(self, boto3):
        client = Mock()
        client.get_secret_value.return_value = {"SecretString": '{"key": "val"}'}
        boto3.client.return_value = client
        self.assertTrue(isinstance(get_secret("test", "1", "1"), dict))

    @patch("index.sqlalchemy")
    @patch.dict("os.environ", {"RDS_SECRET_ARN": "test"})
    def test_mark_as_uploaded(self, sqlalchemy):
        sqlalchemy.text.return_value = str()
        result_proxy = Mock()
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        self.assertEqual(mark_as_uploaded(engine, "1").rowcount, 1)

    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.mark_as_uploaded")
    @patch.dict("os.environ", {"RDS_SECRET_ARN": "test"})
    def test_handler_success(self, mark_as_uploaded, get_secret, sqlalchemy):
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        sqlalchemy.create_engine.return_value = Mock()
        with self.assertNotRaises(Exception):
            handler({"Records": [{"s3": {"object": {"key": "test"}}}]}, {})

    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.mark_as_uploaded")
    @patch.dict("os.environ", {"RDS_SECRET_ARN": "test"})
    def test_handler_failure(self, mark_as_uploaded, get_secret, sqlalchemy):
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        sqlalchemy.create_engine.return_value = Mock()
        mark_as_uploaded.side_effect = Exception()
        with self.assertRaises(Exception):
            handler({"Records": [{"s3": {"object": {"key": "test"}}}]}, {})


if __name__ == "__main__":
    unittest.main()
