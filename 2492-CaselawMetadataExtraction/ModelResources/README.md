## Model Training Resources

### Description
Houses a number of training related resources:
- SagemakerArtifacts - training data and model binary S3 storage area
- TrainingRole - IAM role used at training time for necessary permissions
- ModelHistory - DynamoDB table that stores model metrics from training events.
