## InvokeModel lambda

### Description
Handles extraction requests, orchestrates feature transforms on incoming data, model invocation for identified word chunks, and result normalization.
### Dependencies
#### Python libraries
```bash
pip install -r requirements.txt
```
### How to run unit tests
#### Step 1: Install dependencies (see above)
#### Step 2: Run the test script
```
./run_tests
...
...
----------------------------------------------------------------------
Ran 35 tests in 0.337s

OK
Name                         Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------------------
Source/Lambda/columns.py         1      0      0      0   100%
Source/Lambda/index.py         121      0     30      0   100%
Source/Lambda/normalize.py      28      0     16      0   100%
Source/Lambda/transform.py     101      0     61      0   100%
------------------------------------------------------------------------
TOTAL                          251      0    107      0   100%
```