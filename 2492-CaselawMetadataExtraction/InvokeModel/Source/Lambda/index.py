import boto3
import json
import logging
import os
import pandas as pd
import re
import requests
import concurrent.futures
import time
import sqlalchemy

from typing import Any
from collections import defaultdict
from io import BytesIO
from bs4 import BeautifulSoup
from transform import build_row
from normalize import NormalizerFactory

logger = logging.getLogger()
if logger.handlers:
    for log_handler in logger.handlers:
        logger.removeHandler(log_handler)
logging.basicConfig(level=logging.INFO)

engine = None


def get_secret(secret_id: str, version_id: str = None, version_stage: str = None) -> dict:
    client = boto3.client("secretsmanager")
    params = {"SecretId": secret_id}
    if version_id:
        params["VersionId"] = version_id
    if version_stage:
        params["VersionStage"] = version_stage
    response = client.get_secret_value(**params)
    return json.loads(response["SecretString"])


def get_s3_locators(
    engine: sqlalchemy.engine.base.Engine, doc_id: str
) -> sqlalchemy.engine.ResultProxy:
    sql = sqlalchemy.text(
        """
        SELECT 
            original_s3_uri
        FROM 
            caselaw_metadata_extraction.document_tracking  
        WHERE 
            id = :id
        """
    )
    conn = engine.connect()
    result = conn.execute(sql, id=doc_id).fetchone()
    conn.close()
    bucket, key = re.search(r"(?:s3:\/\/)?(.*)?\/(.*)", result["original_s3_uri"]).groups()
    return bucket, key


def update_record(
    engine: sqlalchemy.engine.base.Engine, doc_id: str, metadata
) -> sqlalchemy.engine.ResultProxy:

    sql_chunks = []
    for key in metadata:
        sql_chunks.append(f"{key} = :{key}")
    sql_chunks.append("metadata_extraction_timestamp = NOW()")

    sql = sqlalchemy.text(
        " ".join(
            [
                "UPDATE caselaw_metadata_extraction.document_tracking SET",
                ", ".join(sql_chunks),
                "WHERE id = :id",
            ]
        )
    )
    print(sql)
    conn = engine.connect()
    result = conn.execute(sql, id=doc_id, **metadata)
    conn.close()
    return True if result.rowcount == 1 else False


def get_file_from_s3(bucket: str, key: str) -> BytesIO:
    s3 = boto3.resource("s3")
    s3_object = s3.Object(bucket, key).get()
    file_obj = BytesIO(s3_object["Body"].read())
    return file_obj


def get_predictions_frame(request_body: BytesIO) -> pd.DataFrame:
    sagemaker_runtime = boto3.client("sagemaker-runtime")
    response = sagemaker_runtime.invoke_endpoint(
        EndpointName=os.environ["MODEL_ENDPOINT_NAME"],
        Body=request_body,
        ContentType="text/csv",
        Accept="text/csv",
    )
    df = pd.read_csv(BytesIO(response["Body"].read()))
    return df


def transform_predictions(df: pd.DataFrame, court_id: str = None) -> dict:
    extracted_rows = defaultdict(list)
    caselaw_metadata = {}
    for _, row in df.iterrows():
        if row["prediction"] == "UNUSED":
            continue
        extracted_rows[row["prediction"]].append(row)
    norm_factory = NormalizerFactory()
    for name, rows in extracted_rows.items():
        logging.info(f"building {name}...")
        normalizer = norm_factory.create(name)
        sorted_rows = sorted(rows, key=lambda x: (x["y-exact"], x["x-exact"]))
        norm_val = normalizer.normalize(sorted_rows, court_id)
        logging.info(norm_val)
        caselaw_metadata.update(norm_val)
    return caselaw_metadata


def filter_response(response: dict) -> dict:
    """Applies business rules around response object key-value pairs"""

    # decided
    if not response.get("decided_date_normalized") and response.get("pacer_date_normalized"):
        response["decided_date_normalized"] = response["pacer_date_normalized"]

    # filed
    if not response.get("filed_date_normalized") and response.get("pacer_date_normalized"):
        response["filed_date_normalized"] = response["pacer_date_normalized"]

    # only populate the response if we get a case name or docket from the body
    if (
        not response.get("docket_number_extracted")
        and not response.get("case_name_extracted")
        and not response.get("court_extracted")
        and not response.get("date_extracted")
    ):
        response = {}
    return response


def get_document_html(bucket: str, key: str) -> BeautifulSoup:
    ecs_cluster_dns = os.environ["ECS_CLUSTER_DNS"]
    conversion_res = requests.post(
        f"http://{ecs_cluster_dns}/pdftotext",
        data=json.dumps({"s3_bucket": bucket, "s3_key": key}),
        headers={"Content-Type": "application/json"},
    )
    conversion_artifact_path = json.loads(conversion_res.content)["output_s3_uri"]
    bucket, key = re.search(r"(?:s3:\/\/)?(.*)?\/(.*)", conversion_artifact_path).groups()
    file_obj = get_file_from_s3(bucket, key)
    soup = BeautifulSoup(file_obj.read().decode("utf-8"), "lxml")
    return soup, conversion_artifact_path


def get_document_features(doc_id: str, bucket: str, key: str) -> str:
    soup, s3_path = get_document_html(bucket, key)
    block_metas = []
    pages = soup.find_all("page")
    for i, page in enumerate(pages):
        # TODO this should move to normalize
        if i != 0 and i != len(pages) - 1:
            continue
        blocks = page.find_all("block")
        for block in blocks:
            block_metas.append((doc_id, block, page, i))
    rows = []
    with concurrent.futures.ThreadPoolExecutor() as executor:
        jobs = [
            executor.submit(build_row, doc_id, block, page, page_idx, len(pages))
            for doc_id, block, page, page_idx in block_metas
        ]
        for completed_job in concurrent.futures.as_completed(jobs):
            try:
                row = completed_job.result()
                rows.append(row)
            except Exception:
                logger.exception("Failed to build feature set row")
    if len(rows) > 0:
        feature_csv = pd.concat(rows, axis=1).T.to_csv(index=False)
    else:
        feature_csv = pd.DataFrame({}).to_csv(index=False)
    document_meta = {"page_count": len(pages), "pdf_transform_s3_uri": s3_path}
    return feature_csv, document_meta


def build_response(
    http_code: int, body: Any, headers: dict = None, base64_encoded: bool = False
) -> dict:
    return {
        "isBase64Encoded": base64_encoded,
        "statusCode": http_code,
        "headers": headers,
        "body": body,
    }


def handler(event: dict, context: dict) -> dict:
    global engine
    logging.info(event)
    try:
        request_body = json.loads(event["body"])
        doc_id = request_body["input_data"]["document_id"]
        court_id = request_body["input_data"].get("court_id")
        requested_fields = request_body["parameters"]["include_fields"]
        logging.info(f"Received request for {doc_id} for {', '.join(requested_fields)}")
    except Exception:
        message = "Error parsing request body."
        logging.exception(message)
        return build_response(400, json.dumps(message))

    if not engine:
        creds = get_secret(os.environ["RDS_SECRET_ARN"])
        conn_str = "mysql+pymysql://{username}:{password}@{host}:{port}".format(**creds)
        engine = sqlalchemy.create_engine(conn_str)
    try:
        bucket, key = get_s3_locators(engine, doc_id)
    except Exception:
        message = f"Error identifying s3 metadata for {doc_id}."
        logging.exception(message)
        return build_response(500, json.dumps(message))

    try:
        feature_csv, doc_meta = get_document_features(doc_id, bucket, key)
    except Exception:
        message = f"Error building feature csv for {doc_id}"
        logging.exception(message)
        return build_response(500, json.dumps(message))

    try:
        if len(feature_csv.strip()) > 0:
            predictions = get_predictions_frame(feature_csv.encode("utf-8"))
        else:
            predictions = pd.DataFrame({})
    except Exception:
        message = f"Error retrieving prediction response from model endpoint {doc_id}"
        logging.exception(message)
        return build_response(500, json.dumps(message))

    try:
        new_key = key + "_dataframe.csv"
        s3 = boto3.resource("s3")
        s3.Bucket(bucket).put_object(Key=new_key, Body=predictions.to_csv(index=False))
        extractions = transform_predictions(predictions, court_id)
    except Exception:
        message = f"Error normalizing extraction results for {doc_id}"
        logging.exception(message)
        return build_response(500, json.dumps(message))

    try:
        s3_locators = {"feature_csv_s3_uri": f"s3://{bucket}/{new_key}"}
        all_meta = {**extractions, **doc_meta, **s3_locators}
        update_record(engine, doc_id, all_meta)
    except Exception:
        message = f"Error updating table record with extracted metadata for {doc_id}"
        logging.exception(message)
        return build_response(500, json.dumps(message))

    response_meta = {}
    # temp debug
    if extractions.get("docket_number_extracted") and not extractions.get(
        "docket_number_normalized"
    ):
        extractions["docket_number_normalized"] = extractions.get("docket_number_extracted")
            
    for k, v in filter_response({**extractions, **doc_meta}).items():
        attr_name = k.replace("_normalized", "")
        if attr_name in requested_fields:
            response_meta[attr_name] = v

    return build_response(200, json.dumps({"document_id": doc_id, "results": response_meta}))


if __name__ == "__main__":
    os.environ[
        "RDS_SECRET_ARN"
    ] = "arn:aws:secretsmanager:us-east-1:284211348336:secret:RDSInstanceSecret-gYnSeK9zXy9z-pFPFCC"
    os.environ[
        "ECS_CLUSTER_DNS"
    ] = "internal-ddc1-LoadB-4DP97LFLZCUT-1303861391.us-east-1.elb.amazonaws.com"
    os.environ["MODEL_ENDPOINT_NAME"] = "CaseLawMetadataExtraction-Classifier"
    print(
        handler(
            {
                "body": '{"input_data": {"document_id": "55b33464-63e3-43e9-917f-651eae24da31"}, "parameters": {"include_fields": ["case_name", "docket_number", "decided_date", "filed_date", "court"]}}'
            },
            {},
        )
    )
