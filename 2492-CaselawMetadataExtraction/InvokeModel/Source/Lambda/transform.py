import math
import json
import re
import requests
import os
import pandas as pd
import logging
from columns import columns
from typing import Union
from bs4 import BeautifulSoup, element

logger = logging.getLogger(__name__)


class UniqueList:
    def __init__(self, values: list) -> None:
        self.values = []
        self.extend(values)

    def append(self, value: list) -> None:
        self.extend([value])

    def extend(self, values: list) -> None:
        for value in values:
            if value not in self.values:
                self.values.append(value)

    def __repr__(self) -> str:
        return ", ".join([str(x) for x in self.values])

    def __str__(self) -> str:
        return ", ".join([str(x) for x in self.values])

    def __eq__(self, x: list) -> bool:
        return self.values == x


def entity_string(entities: list, ignore: tuple = ("O", "MISC"), reduce: bool = False) -> str:
    reduced_entities = []
    add_count = 0
    for entity in entities:
        if (
            len(reduced_entities) == 0
            or entity["ner"] in ignore
            or (not reduce or entity["ner"] != reduced_entities[add_count - 1]["ner"])
        ):
            reduced_entities.append(entity)
            add_count += 1
    return " ".join([x["word"] if x["ner"] in ignore else x["ner"] for x in reduced_entities])


def mode(numbers: list, min_max: str = None) -> Union[int, list]:
    counts = {n: numbers.count(n) for n in set(numbers)}
    modes = sorted(dict(filter(lambda x: x[1] == max(counts.values()), counts.items())).keys())
    if min_max == "min":
        return modes[0]
    elif min_max == "max":
        return modes[-1]
    else:
        return modes


def element_text(elem: BeautifulSoup, normalize: bool = True) -> str:
    words = elem.find_all("word")
    heights = []
    all_len_one = True
    for word in words:
        word_text = word.get_text()
        if len(word_text) > 1:
            heights.append(float(word.attrs["ymax"]) - float(word.attrs["ymin"]))
            all_len_one = False
    norm_word_height = sum(heights) / len(heights) if len(heights) > 0 else 0
    if not all_len_one:
        text = get_joined_word_text(words, norm_word_height)
    else:
        text = " ".join([w.get_text() for w in words])

    if normalize:
        return " ".join(re.sub(r"[^A-Za-z0-9\s]", "", text).split()).strip()
    else:
        return text


def get_joined_word_text(words, norm_word_height):
    words_text = []
    skip = False
    for i, word in enumerate(words):
        if skip:
            skip = False
            continue
        word_text = word.get_text()
        if (
            len(word_text) == 1
            and word_text.isalnum()
            and float(word.attrs["ymax"]) - float(word.attrs["ymin"]) > norm_word_height
            and i < len(words) - 1
            and not word_text.islower()
            and words[i + 1].string is not None
        ):
            words_text.append(word_text + words[i + 1].string)
            skip = True
        else:
            words_text.append(word_text)
    text = " ".join(words_text)
    return text


def nlp_annotate(text: str) -> list:
    annotations = []
    if len(text.strip()) == 0:
        return annotations
    ecs_cluster_dns = os.environ["ECS_CLUSTER_DNS"]
    res = requests.post(f"http://{ecs_cluster_dns}/corenlp", data=text.encode("utf-8"))
    response_text = res.content.decode("utf-8")
    try:
        response_content = json.loads(response_text)
        for sentence in response_content["sentences"]:
            for token in sentence["tokens"]:
                annotations.append(token)
    except Exception:
        logger.exception(f"Bad raw response: {text} | {response_text}")
    return annotations


def build_row(
    fn: str, block: element.Tag, page: element.Tag, page_idx: int, page_len: int
) -> pd.Series:
    xmin, ymin = int(float(block.attrs["xmin"])), int(float(block.attrs["ymin"]))
    xmax, ymax = int(float(block.attrs["xmax"])), int(float(block.attrs["ymax"]))
    page_width, page_height = (int(float(page.attrs["width"])), int(float(page.attrs["height"])))
    word_elems = block.find_all("word")
    word_heights = [int(float(word["ymax"])) - int(float(word["ymin"])) for word in word_elems]
    word_height_mode = mode(word_heights, min_max="max")

    x = xmin
    y = ymin
    width = xmax - xmin
    height = ymax - ymin
    x_rel = x / page_width
    y_rel = y / page_height
    width_rel = width / page_width
    height_rel = height / page_height

    words = [x.strip() for x in block.get_text().split()]
    word_count = len(words)
    text = element_text(block, normalize=False)
    bytes_ = "".join(words)
    characters = "".join(re.split(r"[^A-Za-z0-9]", bytes_))
    digits = "".join(re.split(r"[^0-9]", bytes_))
    punctuation = "".join(re.split(r"[A-Za-z0-9]", bytes_))

    annotations = nlp_annotate(text)

    text_clean = re.sub(r"[^-'/A-Za-z0-9 ]", " ", text).strip()
    cleaned_annotations = nlp_annotate(text_clean)

    entity_count = len([x for x in annotations if x["ner"] != "O"])

    row = pd.Series(
        [
            fn,
            page_idx,
            page_idx / page_len if page_len > 0 else 0,
            word_height_mode,
            round(x_rel, 2),
            round(x_rel / 10, 2),
            math.floor(math.log(x)) if x > 0 else 0,
            x,
            round(x / 10, 0),
            round(y_rel, 2),
            round(y_rel / 10, 2),
            math.floor(math.log(y)) if y > 0 else 0,
            y,
            round(y / 10, 0),
            round(height_rel, 2),
            round(height_rel / 10, 2),
            math.floor(math.log(height)) if height > 0 else 0,
            height,
            round(height / 10, 0),
            round(width_rel, 2),
            round(width_rel / 10, 2),
            math.floor(math.log(width)) if width > 0 else 0,
            width,
            round(width / 10, 0),
            word_count,
            len(bytes_),
            sum([len(word) for word in words]) / word_count if word_count > 0 else 0,
            round(len(characters) / len(bytes_) if len(bytes_) > 0 else 0, 2),
            round(len(digits) / len(bytes_) if len(bytes_) > 0 else 0, 2),
            round(len(punctuation) / len(bytes_) if len(bytes_) > 0 else 0, 2),
            text,
            text_clean,
            entity_string(annotations, ignore=("O", "MISC"), reduce=True),
            entity_string(cleaned_annotations, ignore=("O", "MISC"), reduce=True),
            entity_string(annotations, ignore=("O", "MISC")),
            entity_string(cleaned_annotations, ignore=("O", "MISC")),
            " ".join([x["word"] for x in annotations]),
            UniqueList([x["pos"] for x in annotations]),
            " ".join([x["pos"] for x in annotations]),
            " ".join([x["word"] + "/" + x["pos"] for x in annotations]),
            " ".join([x["word"] + "/" + x["pos"] for x in cleaned_annotations]),
            entity_count,
            entity_count / word_count if word_count > 0 else 0,
            UniqueList([x["ner"] for x in annotations if x["ner"] not in ("O", "MISC")]),
            " ".join([x["ner"] for x in annotations if x["ner"] not in ("O", "MISC")]),
            " ".join(
                [x["word"] + "/" + x["ner"] for x in annotations if x["ner"] not in ("O", "MISC")]
            ),
            " ".join(
                [
                    x["word"] + "/" + x["ner"]
                    for x in cleaned_annotations
                    if x["ner"] not in ("O", "MISC")
                ]
            ),
        ],
        index=columns,
    )
    return row
