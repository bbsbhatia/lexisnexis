import re
import logging
from abc import ABCMeta, abstractmethod

from lng_date_mentions_extractor.extractor import DateMentionsExtractor
from docket_number_patterns import patterns


logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)


class WordChunkNormalizer(metaclass=ABCMeta):
    @abstractmethod
    def normalize(self, target: str, text: str) -> str:
        return text


class CaseNameChunkNormalizer(WordChunkNormalizer):
    def __init__(self) -> None:
        self.punc_pattern = re.compile(r"(?<![a-zA-Z0-9\.])([^a-zA-Z0-9\s])")
        self.phrase_pattern = re.compile(r"(not\s*?for\s*?publication)", flags=re.I)

    def _filter(self, chunks: list) -> list:
        stop_word_idxs = []
        for i, chunk in enumerate(chunks):
            if (
                "defendant" in str(chunk["words-normalized"]).lower()
                or "respondent" in str(chunk["words-normalized"]).lower()
            ):
                stop_word_idxs.append(i)
        if len(stop_word_idxs) > 0:
            chunks = chunks[: max(stop_word_idxs) + 1]
        return chunks

    def normalize(self, chunks: list, court_id: str = None) -> str:
        normed = {}
        text = " ".join([str(r["words-normalized"]).strip() for r in self._filter(chunks)])
        punc_normed = self.punc_pattern.sub("", text)
        phrase_normed = self.phrase_pattern.sub("", punc_normed)
        normed["case_name_normalized"] = (
            " ".join([x.strip() for x in phrase_normed.split()]) if phrase_normed else None
        )
        normed["case_name_extracted"] = " ".join(
            [str(r["words-normalized"]).strip() for r in chunks]
        )
        return normed


class DocketNumberChunkNormalizer(WordChunkNormalizer):
    def normalize(self, chunks: list, court_id: str = None) -> str:
        normed = {}
        try:
            text = " ".join([str(r["words-normalized"]).strip() for r in chunks])
            phrase_normed = None
            if court_id in patterns:
                match = patterns[court_id].search(text)
                if match:
                    try:
                        phrase_normed = match.group(1)
                        logger.info(f"Docket match: {phrase_normed} | {patterns[court_id].pattern}")
                    except Exception:
                        logger.exception("Pattern must define a capture group.")
            normed["docket_number_normalized"] = (
                " ".join([x.strip() for x in phrase_normed.split()]) if phrase_normed else None
            )
            normed["docket_number_extracted"] = text
        except Exception:
            logger.exception("Failed to normalize docket number")
        return normed


class CourtChunkNormalizer(WordChunkNormalizer):
    def normalize(self, chunks: list, court_id: str = None) -> str:
        normed = {}
        try:
            text = " ".join([str(r["words-normalized"]).strip() for r in chunks])
            normed["court_extracted"] = text
            normed["court_normalized"] = text
        except Exception:
            logger.exception("Failed to normalize court.")
        return normed


class PacerHeaderChunkNormalizer(WordChunkNormalizer):
    def normalize(self, chunks: list, court_id: str = None) -> str:
        normed = {}
        try:
            text = " ".join([str(r["words-normalized"]).strip() for r in chunks])
            normed["pacer_header_extracted"] = text
            pacer_date_mentions = DateMentionsExtractor(text).extract()
            if len(pacer_date_mentions) > 0:
                normed["pacer_date_extracted"] = pacer_date_mentions[0].matched_text
                normed["pacer_date_normalized"] = pacer_date_mentions[0].iso_8601_date_str()
        except Exception:
            logger.exception("Failed to normalize pacer header targets")
        return normed


class DateChunkNormalizer(WordChunkNormalizer):
    def normalize(self, chunks: list, court_id: str = None) -> str:
        normed = {}
        try:
            text = " ".join([str(r["words-normalized"]).strip() for r in chunks])
            date_mentions = DateMentionsExtractor(text).extract()
            if len(date_mentions) == 1:
                normed["decided_date_normalized"] = date_mentions[0].iso_8601_date_str()
                normed["decided_date_extracted"] = date_mentions[0].matched_text

            if len(date_mentions) >= 2:
                decided_date = min(date_mentions, key=lambda x: x.date)
                normed["decided_date_normalized"] = decided_date.iso_8601_date_str()
                normed["decided_date_extracted"] = decided_date.matched_text

                remaining_dates = [x for x in date_mentions if x.date != decided_date.date]
                try:
                    filed_date = min(remaining_dates, key=lambda x: x.date)
                    normed["filed_date_normalized"] = filed_date.iso_8601_date_str()
                    normed["filed_date_extracted"] = filed_date.matched_text
                except Exception:
                    logger.warning("No unique filed date, all identified dates are the same.")

                if len(date_mentions) > 2:
                    logger.warning(f"Multiple possibilities for filed date detected.")
            normed["date_extracted"] = " ".join([r["words-normalized"].strip() for r in chunks])
        except Exception:
            logger.exception("failed to normalize document body dates.")
        return normed


class NormalizerFactory:
    def __init__(self) -> None:
        self._dispatch = {
            "Case Name": CaseNameChunkNormalizer,
            "Date": DateChunkNormalizer,
            "Docket Number": DocketNumberChunkNormalizer,
            "Court": CourtChunkNormalizer,
            "Pacer Header": PacerHeaderChunkNormalizer,
        }

    def create(self, normalizer_type: str, *args: list, **kwargs: dict) -> WordChunkNormalizer:
        try:
            normalizer = self._dispatch[normalizer_type]
        except KeyError:
            logger.exception(f"No normalizer class defined for {normalizer_type}!")
            raise
        return normalizer(*args, **kwargs)
