import re

patterns = {
    "aldimd": re.compile(
        r"(?:([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9a-z]+-[a-z]{3}-?[a-z]{2}?|[0-9]{1}:[0-9a-z]+-[a-z]{3}))",
        flags=re.I,
    ),
    "aldind": re.compile(
        r"(?:([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9a-z]+-[a-z]{3}-?[a-z]{2}?|[0-9]{1}:[0-9a-z]+-?[a-z]{3}?))",
        flags=re.I,
    ),
    "aldisd": re.compile(
        r"([0-9]{1,2}:[0-9]{2}-[a-z]{2}-[0-9]+-[a-z]{1,3}-?[a-z]{1}?)", flags=re.I
    ),
    "cadicd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{1,3}-[a-z]{1,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{3}|[0-9]{1}:[0-9a-z]{8})",
        flags=re.I,
    ),
    "cadied": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}\\s?-?[a-z]{1,3}\\s?-?[a-z]{1,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}\\s?-?-?-?[a-z]{3}|[0-9]{1}:[0-9a-z]{8})",
        flags=re.I,
    ),
    "cadind": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2}|[0-9]{1}:[0-9a-z]{8})",
        flags=re.I,
    ),
    "codi00": re.compile(
        r"case\\s*\\d{1}[:|;](?:(\\d{2}-[a-z]{2}-\\d{3,5}-[a-z]{2,3}|\\d{2}-[a-z]{2}-\\d{3,5}-[a-z]{2,3}-[a-z]{3})) ",
        flags=re.I,
    ),
    "dcdi00": re.compile(
        r"case(?:[\\x0d|\\x0a|\\xA0|\\s]+no[\\x0d|\\x0a|\\xA0|\\s|.]+|[\\x0d|\\x0a|\\xA0|\\s]+|[\\x0d|\\x0a|\\xA0|\\s]+no[\\x0d|\\x0a|\\xA0|\\s|.]+\\d{1}:|[\\x0d|\\x0a|\\xA0|\\s]+\\d{1}:)((?:\\d{2}-[a-z]{2}-\\d{3,5}-[a-z]{2,3}|\\d{2}-[a-z]{2}-\\d{3,5}))",
        flags=re.I,
    ),
    "dedi00": re.compile(
        r"case\\s*\\d{1}[:|;](?:(\\d{2}-[a-z]{2}-\\d{3,5}-[a-z]{2,3}|\\d{2}-[a-z]{2}-\\d{3,5}-[a-z]{2,3}-[a-z]{3}))",
        flags=re.I,
    ),
    "fldimd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{2,3}-[0-9a-z]{2,4}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{2,3})",
        flags=re.I,
    ),
    "fldind": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{2,3}-[0-9a-z]{2,4}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{2,3})",
        flags=re.I,
    ),
    "fldisd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2}|[0-9]{1}:[0-9a-z]{8})", flags=re.I
    ),
    "gadimd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}-[a-z]{2,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}|[0-9]{1}:[0-9a-z]{7,8})",
        flags=re.I,
    ),
    "gadind": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}-[a-z]{2,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}|[0-9]{1}:[0-9a-z]{7,8})",
        flags=re.I,
    ),
    "gadisd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3})",
        flags=re.I,
    ),
    "gudi00": re.compile(r"case\\s+\\d+:\\s*(\\d{2}-[c][r|v]-\\d{5})\\s", flags=re.I),
    "iddi00": re.compile(
        r"(?:case\\s+no.\\s+[a-z]{2}|case\\s+no.|case)\\s+\\d{1}[:|;](\\d{2}-[a-z]{2}-\\d{3,5}-[a-z]{3})",
        flags=re.I,
    ),
    "ildicd": re.compile(
        r"[\\x0a|\\x0d|\\xA0|\\s]+(\\d{1}:\\d{2}-[a-z]{2}-\\d{5}(-[a-z]{2,3})*)[\\x0a|\\x0d|\\xA0|\\s]+",
        flags=re.I,
    ),
    "ildind": re.compile(r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5})", flags=re.I),
    "ildisd": re.compile(r"(\\d:\\d{2}-[a-z]{2}-\\d{5}-[a-z]{3}(-[a-z]{3})?)", flags=re.I),
    "indind": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3})",
        flags=re.I,
    ),
    "indisd": re.compile(r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{3}-[a-z]{3})", flags=re.I),
    "ksdi00": re.compile(r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3})", flags=re.I),
    "kydied": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{3}|[0-9]{2}-[a-z]{2}-[0-9]{2}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[0-9]{4}-[a-z]{3}|[0-9]{2}-[0-9]{2,3}-[a-z]{2,3})",
        flags=re.I,
    ),
    "kydiwd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-?[a-z]{2}-[0-9a-z]{3,5}-[a-z]{3})",
        flags=re.I,
    ),
    "ladimd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{2}-[0-9]{3}-[a-z]{3})",
        flags=re.I,
    ),
    "madi00": re.compile(r"(\\d{2}-([a-z]{2}-)?\\d{5}-[a-z]{2,3})", flags=re.I),
    "mddi00": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{2}-[0-9]{4}|[0-9a-z]{2,3}-[0-9a-z]{2}-[0-9]{3,4})",
        flags=re.I,
    ),
    "medi00": re.compile(
        r"(?:case\\s?#?:?|no.)\\s([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]+-[A-Z]+)", flags=re.I
    ),
    "midied": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3})",
        flags=re.I,
    ),
    "midiwd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{2})",
        flags=re.I,
    ),
    "mndi00": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{2,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3})",
        flags=re.I,
    ),
    "modied": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}|[0-9]{1}:[0-9a-z]{6})",
        flags=re.I,
    ),
    "modiwd": re.compile(r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3})", flags=re.I),
    "mtdi00": re.compile(r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3})", flags=re.I),
    "ncdimd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}-[a-z]{2,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}|[0-9]{1}:[0-9a-z]{7,8})",
        flags=re.I,
    ),
    "nhdi00": re.compile(r"(?:case no|civil no|criminal no).?\\s+([0-9a-z\\-]+)\\s", flags=re.I),
    "njdi00": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{2}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{2}-[a-z]{2}-[0-9]{3,4}|[0-9]{2}-[0-9]{3,4})",
        flags=re.I,
    ),
    "nmdi00": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}-[a-z]{2,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}|[0-9]{1}:[0-9a-z]{7,8}|[0-9]{2}-[0-9]{4})",
        flags=re.I,
    ),
    "nydied": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}|[0-9]{1}:[0-9a-z]{8})", flags=re.I
    ),
    "nydind": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{1}:[0-9a-z]{7})",
        flags=re.I,
    ),
    "nydisd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{2,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}|[0-9]{1}:[0-9a-z]{8})",
        flags=re.I,
    ),
    "nydiwd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{1}:[0-9a-z]{5,7})",
        flags=re.I,
    ),
    "ohdind": re.compile(
        r"(?:case:)\\s*\\d*\\:(\\d{1,2}[\\-\\s]?[a-z]{2}[\\-\\s]?\\d{1,5})", flags=re.I
    ),
    "ohdisd": re.compile(r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3})", flags=re.I),
    "okdind": re.compile(
        r"(?:no).?\\s*(\\d{1,2}\\-[a-z]{2}\\-\\d{1,5}\\-([a-z]{3}|\\d{3})([\\-][a-z]{3})?)",
        flags=re.I,
    ),
    "ordi00": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2}|[0-9]{1}:[0-9a-z]{8})", flags=re.I
    ),
    "padied": re.compile(
        r"(?:no.|case).*?([0-9]{2}-[0-9]{4}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,4})",
        flags=re.I,
    ),
    "padimd": re.compile(r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,4}|[0-9]{2}-[0-9]{4})", flags=re.I),
    "padiwd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3,5}-[a-z]{2,3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}|[0-9]{2}-[0-9a-z]{7}|[0-9]{2}-[0-9]{3}-[0-9]{2}|[0-9]{2}-[0-9]{3,4})",
        flags=re.I,
    ),
    "ridi00": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{1,3}-[a-z]{1,3}|[0-9]{2}-[0-9]{3}-[0-9a-z]{3}|[0-9]{2}-[0-9a-z]{3,4})",
        flags=re.I,
    ),
    "scdi00": re.compile(r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3})", flags=re.I),
    "tndied": re.compile(
        r"(?:no.|#:).*?([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{2,5}-[a-z]{3}-[a-z]{3,4}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{3})",
        flags=re.I,
    ),
    "tndimd": re.compile(r"case.*?([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5})", flags=re.I),
    "tndiwd": re.compile(
        r"case.*?([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3})",
        flags=re.I,
    ),
    "txdied": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}-[a-z]{3}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2,3}|[0-9]{1}:[0-9a-z]{6,10})",
        flags=re.I,
    ),
    "txdind": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{1}-[a-z]{2}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{1}|[0-9]{1}:[0-9a-z]{6})",
        flags=re.I,
    ),
    "txdisd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}|[0-9]{1}:[0-9a-z]{6,10})", flags=re.I
    ),
    "txdiwd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{2}|[0-9]{1}:[0-9a-z]{6,8})", flags=re.I
    ),
    "vadied": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[a-z]{3}|[0-9]{1}:[0-9a-z]{7})", flags=re.I
    ),
    "vadiwd": re.compile(
        r"([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}|[0-9]{1}:[0-9a-z]{6,9})", flags=re.I
    ),
    "vidi00": re.compile(r"no..*?([0-9]{2,4}-[0-9]{2,4})", flags=re.I),
    "vtdi00": re.compile(r"(\\d{2}-[a-z]{2}-\\d{3})", flags=re.I),
    "wadiwd": re.compile(r"([0-9a-z]{10}-[0-9]{1,2})", flags=re.I),
    "widiwd": re.compile(r"([0-9a-z]{10}-[0-9]{1,3})", flags=re.I),
    "wvdind": re.compile(
        r"(?:no.)?\\:?\\s*\\d\\:\\s*(\\d{1,2}[\\-–]?[a-z]{2}[\\-–]?\\d{1,5})", flags=re.I
    ),
    "wvdisd": re.compile(
        r"no.\\s+([0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}-[0-9]{2}|[0-9]{1}:[0-9]{2}-[a-z]{2}-[0-9]{5}|[0-9]{1}:[0-9]{2}-[0-9]{5})",
        flags=re.I,
    ),
    "wydi00": re.compile(r"([0-9a-z]{10}-[0-9]{1,3})", flags=re.I),
    "ohdind": re.compile(
        r"(?:CASE)?\s*?(?:NO)?s?\.?\:?\s*?(\d\s*?:\s*?\d{2}\s*?\-?(?:CV|CR|MD|OP)\s*?\-?\d+(?:\-WHB|\-DAP)?)",
        flags=re.I,
    ),
}

_proposed_patterns = {
    "aldimd": re.compile(r"(\d+:[0-9A-Za-z-]*(?:\s[\(|\[](?:WO|wo)[\)|\]])?)", flags=re.I),
    "aldind": re.compile(
        r"((?:\d{1}:)?\d{1,2}-?\s*(?:[a-zA-Z]{2,3})?\s*-\w+-\s*[A-Z]+(?:-[A-Z]+)?)", flags=re.I
    ),
    "aldisd": re.compile(r"((?:\d+:)?\d+-[A-Za-z0-9]+-[A-Za-z0-9]+-\w+(?:-\w+)?)", flags=re.I),
    "cadicd": re.compile(
        r"((?:\d+:|\w+\s+)?\d+-?\w+(?:-|\s+)?\w+(?:-\w+|\s+\(?:\w+\))?)", flags=re.I
    ),
    "cadied": re.compile(r"(\d{1}(?:[-:](?:\d+|[A-Za-z]{2,3}))+)", flags=re.I),
    "cadind": re.compile(r"(\d{1,2}(?:(?::|-)\s*(?:\d+|\w+))+)", flags=re.I),
    "codi00": re.compile(r"(\d*:?\d+-?\w{2}-?\d+-?\w+-?\w*)", flags=re.I),
    "dcdi00": re.compile(r"(\d+[:-][A-Za-z0-9-]+\s+\([A-Z]+\))", flags=re.I),
    "dedi00": re.compile(r"((?:\d+:)?\d+-[A-Za-z0-9]+-[A-Za-z0-9]+-\w+(?:-\w+)?)", flags=re.I),
    "fldimd": re.compile(r"((?:\d:)?\d+-(?:CV|CR|cv|cr)?-\d+-\w+-\w+)", flags=re.I),
    "fldind": re.compile(r"(\d+:?\w+-?\/?\w+-?\/?\w+-?\/?\w*-?\/?\w*)", flags=re.I),
    "fldisd": re.compile(r"(\d*:?\d+\s*-?\w+-?\w+-?\w+\/?\w+-?\w+)", flags=re.I),
    "gadimd": re.compile(
        r"((?:\d*:?\d*-?(?:CV|CR|cv|cr)\s*-?\d+\s*(?:\(.*\))?\w*-?\w*-?\w*)\s)", flags=re.I
    ),
    "gadind": re.compile(r"(\d{1,2}(?:(?:-|:)(?:\d+|\w+))+)", flags=re.I),
    "gadisd": re.compile(r"((?:\d:\d+-)?(?:CV|CR|cv|cr)(?:\s)?[0-9-A-Z]+)", flags=re.I),
    "gudi00": re.compile(r"(\d{2}(?:[-:]?(?:\d+|[A-Za-z]{1,3}))+)", flags=re.I),
    "ildicd": re.compile(r"(\d*:?\d+-?\w{2}-?\d+-?\w+-?\w*-?\w*)", flags=re.I),
    "ildind": re.compile(r"((?:\d:)?\d+(?:\s+|-)(?:C)(?:V|R|Y)?(?:\s+|-)\d*-?\w+)", flags=re.I),
    "ildisd": re.compile(r"(\d+[:-][A-Za-z0-9-]+)", flags=re.I),
    "indind": re.compile(r"(\d*:?\d+\s*-?\w{2}-?\s*\d+\s*-?\w+-?\/?\w*-?\w*)", flags=re.I),
    "indisd": re.compile(r"(\d{1}(?:(?:-|:)(?:\d+|\w+))+)", flags=re.I),
    "ksdi00": re.compile(r"(\d{1,2}(?:(?:-|:)(?:\d+|\w+))+)", flags=re.I),
    "kydied": re.compile(r"(\d{1,2}(?:(?:-|:)(?:\d+|\w+)/?)+)", flags=re.I),
    "kydiwd": re.compile(r"((?:\d*:)?\d{1,2}-?\s*(?:[a-zA-Z]{2,3})?\s*-\w+-\s*[A-Z]+)", flags=re.I),
    "ladimd": re.compile(r"(\d+-?\w+-?\w+-\w+-\w+)", flags=re.I),
    "madi00": re.compile(r"(\d{1,2}(?:(?::|-)\s*(?:\d+|\w+))+)", flags=re.I),
    "mddi00": re.compile(r"([A-Z0-9]{1,3}-[A-Z0-9]{2,4}-[A-Z0-9]{2,4})", flags=re.I),
    "medi00": re.compile(
        r"((?:\d*:)?\d{1,2}-?\s*(?:[a-zA-Z]{2,3})?\s*-?\d+(?:-\s*[A-Z]+)?(?:-\d+)?)", flags=re.I
    ),
    "midied": re.compile(
        r"((?:[0-9]{1}:)?\d{1,2}-?\s*(?:[a-zA-Z]{2,3})?\s*-\s*\d{3,5}(?:\s*-\(?[A-Z]{2,4}\)?)*)",
        flags=re.I,
    ),
    "midiwd": re.compile(
        r"((?:[0-9]{1}:)?\d{1,2}-?\s*[a-zA-Z]{2,3}\s*\.?-?\s*\d{3,5}(?:\s*\([A-Z]{2,4}\))*(?:\s*-\(?[A-Z]{2,4}\)?)*)",
        flags=re.I,
    ),
    "mndi00": re.compile(r"(\d+-?(?:\w+)?(?:d+)?-?\d+\s*\(?.[^\s]*\)?)", flags=re.I),
    "modied": re.compile(r"((?:\d+:)?\d+(?:-|\s)[A-Za-z0-9]+-[A-Za-z0-9]+(?:-|\s)\w+)", flags=re.I),
    "modiwd": re.compile(r"(\w(?:-|:)\d+-\w+(?:-\w+)?)", flags=re.I),
    "mtdi00": re.compile(r"((?:CR|CV)(?:[-:]?(?:\s*?\d+|[A-Z]{1,3}))[^\s]+)", flags=re.I),
    "ncdimd": re.compile(r"(\d{1}\:[A-Za-z0-9-]+)", flags=re.I),
    "nddi00": re.compile(r"(\d{1}(?:[-:](?:\d+|[CVcvr]{1,2}))+)", flags=re.I),
    "njdi00": re.compile(
        r"(\d{1,2}-\d+\s?(?:\(?[A-Z]{2,3}\))?\s?(?:\(?[A-Z]{2,3}\))?)", flags=re.I
    ),
    "nmdi00": re.compile(r"(\d+-\d+\s[A-Z/\\]+)", flags=re.I),
    "nydied": re.compile(r"(\d+[:-][A-Za-z0-9-]+\s+\([A-Z]+\)\s*\([A-Z]+\))", flags=re.I),
    "nydisd": re.compile(
        r"((?:[0-9]{1}:)?\d{1,2}-?\s*[a-zA-Z]{2,3}\s*\.?-?\s*\d{3,5}(?:\s*\([A-Z]{2,4}\))*(?:\s*-\(?[A-Z]{2,4}\)?)*)",
        flags=re.I,
    ),
    "nydiwd": re.compile(r"((?:\d:)?\d+(?:\s+|-)(?:C)(?:V|R|Y)?(?:\s+|-)\d*-?\w+)", flags=re.I),
    "ohdind": re.compile(r"(\d:\s*\d{2}-?\s*\w{2}-?\s*\d+)", flags=re.I),
    "ohdisd": re.compile(r"((?:\d*:)?\d{1,2}(?:~|-)?\s*(?:[a-zA-Z]{2,3})?\s*-?\d+)", flags=re.I),
    "ordi00": re.compile(
        r"((?:\d:)?(?:\w+\s+)?\d+?(?::|-)?(?:CV|CR|cv|cr)?-\d+-?(?:\w+)?(?:-\d)?)", flags=re.I
    ),
    "padied": re.compile(r"(\d{1,2}[:-](?:-?(?:\d+|[A-Z]{2,3}))+)", flags=re.I),
    "padimd": re.compile(r"((?:\d*:)?\d*-?(?:CV|CR)?-?\d+)", flags=re.I),
    "padiwd": re.compile(r"((?:\d+:(?:\s)?)?\d+-[0-9-cvA-Z]+)", flags=re.I),
    "ridi00": re.compile(r"((?:\d+:)?\d+-?\w+(?:\s+|-)?\w+-?\w+-?\w+)", flags=re.I),
    "scdi00": re.compile(r"(\d{1}:\d+(?:-(?:\d+|\w+))+)", flags=re.I),
    "tndied": re.compile(
        r"([0-9]{1}:\d{1,2}-?\s*(?:[a-zA-Z]{2,3})?\s*-\s*\d{1,4}(?:-?\(?[0-9a-zA-Z]{1,6}\)?)*)",
        flags=re.I,
    ),
    "tndimd": re.compile(
        r"((?:\d:)?(?:\w+\s+)?\d+?(?::|-)?(?:CV|CR|cv|cr)?-\d+-?(?:\w+)?(?:-\d)?)", flags=re.I
    ),
    "tndiwd": re.compile(r"((?:\d+:)?\d+-[A-Za-z0-9]+-[A-Za-z0-9]+-\w+(?:-\w+)?)", flags=re.I),
    "txdied": re.compile(r"(\d:\d+-?\w*-?\d+-?\w*-?\w*)", flags=re.I),
    "txdind": re.compile(r"(\d:\d+-?\w*-?\d+-?\w*-?\w*\s?-?\s*(?:\(.*\))?)", flags=re.I),
    "txdisd": re.compile(r"(\d{1,2}(?:(?:-|:)(?:\d+|\w+))+)", flags=re.I),
    "txdiwd": re.compile(
        r"((?:[0-9A-Z]{1,2}:)?[0-9A-Z]{1,2}[-–]\s*[0-9A-Z\s]{2,3}\s*[–-][0-9A-Z\s]{2,3}(?:[–-]?\(?[A-Z0-9]{1,5}\)?)*)",
        flags=re.I,
    ),
    "vadiwd": re.compile(
        r"((?:[0-9]{1}:)?\d{1,2}-?\s*[a-zA-Z\s]{2,3}\s*\.?-?\s*\d{1,5}(?:\s*-\(?\d{1,4}\)?)*)",
        flags=re.I,
    ),
    "vidi00": re.compile(r"(\d{4}(?:[\-/:]\d+)+)", flags=re.I),
    "vtdi00": re.compile(r"(\d{1}(?:[-:](?:\d+|[A-Za-z]{1,3}))+)", flags=re.I),
    "widiwd": re.compile(r"(\d+-[a-z]+-\d+-[a-z]+)", flags=re.I),
    "wvdind": re.compile(
        r"((?:\d*:)?\d*-?(?:CV|CR|cv|cr)-?\d+(?:\s+\(.*\)|-\w+)?(?:-\w+)?)", flags=re.I
    ),
    "akdi00": re.compile(r"(\d+:[0-9A-Za-z-]+)", flags=re.I),
    "okdind": re.compile(r"(?:[0-9]{1,2}(?:-|:)[0-9A-Za-z-]+)", flags=re.I),
    "okdiwd": re.compile(r"((?:CIV|civ|cr|CR)-[0-9A-Za-z-]+)", flags=re.I),
    "prdi00": re.compile(r"(\d+-[0-9A-Z-]+(?:\s?(?:\([A-Z\/]*)\))?)", flags=re.I),
    "sddi00": re.compile(r"(\d+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "vadied": re.compile(r"(\d+:[0-9A-Za-z-]+(?:\s?(?:\([A-Z\/]*)\))?)", flags=re.I),
    "wadiwd": re.compile(r"(?:[0-9A-Z]+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "wvdisd": re.compile(r"([0-9]+:[0-9A-Za-z-]+)", flags=re.I),
    "okdied": re.compile(
        r"([0-9CRCVI]+(?:-|:|\s)[0-9A-Za-z-]+(?:\s?(?:\([A-Z\/]*)\))?)", flags=re.I
    ),
    "utdi00": re.compile(r"([0-9]+(?::|-)[0-9A-Za-z-:]+)", flags=re.I),
    "wadied": re.compile(r"([0-9CVRI]+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "widied": re.compile(r"([0-9]+-[0-9A-Za-z-]+)", flags=re.I),
    "wydi00": re.compile(r"([0-9]+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "ardied": re.compile(r"([0-9]+:[0-9A-Za-z-\/]+)", flags=re.I),
    "ardiwd": re.compile(r"([0-9]+:[0-9A-Za-z-\/]+)", flags=re.I),
    "azdi00": re.compile(r"([0-9CRCVI]+(?:-|:)[0-9A-Za-z-]+(?:\s?(?:\([A-Z\/]*)\))?)", flags=re.I),
    "nvdi00": re.compile(r"([0-9CV]+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "ncdiwd": re.compile(r"([0-9]+:[0-9A-Za-z-]+)", flags=re.I),
    "nydind": re.compile(r"([0-9]+(?:-|:)[0-9A-Za-z-]+(?:\s?(?:\([A-Z\/]*)\))?)", flags=re.I),
    "nhdi00": re.compile(r"([0-9]+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "msdind": re.compile(r"([0-9]+(?::|-)[0-9A-Za-z-\/]+)", flags=re.I),
    "nedi00": re.compile(r"([0-9]+:[0-9A-Za-z-\/]+)", flags=re.I),
    "ncdied": re.compile(r"([0-9S]+(?::|-)[0-9A-Za-z-\(\)]+)", flags=re.I),
    "cadisd": re.compile(r"([0-9A-Z]+(?::|-)[0-9A-Za-z-]+(?:\s?(?:\([A-Z]*)\))?)", flags=re.I),
    "hidi00": re.compile(r"([0-9]{2}-[0-9]+\s[A-Z]{2,3}-[A-Z]{2,3})", flags=re.I),
    "ladied": re.compile(r"([0-9]{2}-[0-9]+)", flags=re.I),
    "msdisd": re.compile(r"([0-9]+:[0-9A-Za-z-]+)", flags=re.I),
    "iadind": re.compile(r"([0-9CR]+(?::|-)[0-9A-Za-z-]+(?:\s?(?:[A-Z]{3}))?)", flags=re.I),
    "ladiwd": re.compile(r"([0-9]+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "iddi00": re.compile(r"([0-9CRVI]+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "iadisd": re.compile(r"([0-9]+(?::|-)[0-9A-Za-z-]+)", flags=re.I),
    "ctdi00": re.compile(r"([0-9]+(?:-|:)[0-9A-Za-z-]+(?:\s?(?:\([A-Z\/]*)\))?)", flags=re.I),
    "cmdi00": re.compile(r"([0-9]+(?:-|:)[0-9A-Za-z:-]+)", flags=re.I),
}

# temporary..
patterns = {**patterns, **_proposed_patterns}
