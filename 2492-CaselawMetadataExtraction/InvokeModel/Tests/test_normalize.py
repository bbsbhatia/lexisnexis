import pandas as pd
import unittest
import re

from contextlib import contextmanager
from unittest.mock import patch
from normalize import (
    CaseNameChunkNormalizer,
    DateChunkNormalizer,
    DocketNumberChunkNormalizer,
    CourtChunkNormalizer,
    PacerHeaderChunkNormalizer,
    NormalizerFactory,
)


class NormalizeTests(unittest.TestCase):
    @contextmanager
    def assertNotRaises(self, exception):
        try:
            yield None
        except exception:
            raise self.failureException("{0}".format(exception.__name__))

    def test_case_name_normalizer(self):
        normalizer = CaseNameChunkNormalizer()
        rows = [
            {"words-normalized": "PartyOne, Plaintiff"},
            {"words-normalized": "v."},
            {"words-normalized": "PartyTwo, Defendant."},
            {"words-normalized": "Judge's Name, U.S.D.J."},
        ]

        self.assertTrue(
            normalizer.normalize(rows)
            == {
                "case_name_extracted": "PartyOne, Plaintiff v. PartyTwo, Defendant. Judge's Name, U.S.D.J.",
                "case_name_normalized": "PartyOne, Plaintiff v. PartyTwo, Defendant.",
            }
        )

    def test_decided_date_normalizer_one_date(self):
        normalizer = DateChunkNormalizer()
        rows = [{"words-normalized": "Decided on April 2nd 2019"}]
        self.assertTrue(
            normalizer.normalize(rows)
            == {
                "date_extracted": "Decided on April 2nd 2019",
                "decided_date_extracted": "April 2nd 2019",
                "decided_date_normalized": "2019-04-02",
            }
        )

    def test_decided_date_normalizer_multiple_dates(self):
        normalizer = DateChunkNormalizer()
        rows = [{"words-normalized": "Decided on April 2nd 2019 and Filed on April 4th 2019."}]
        self.assertEquals(
            normalizer.normalize(rows),
            {
                "date_extracted": "Decided on April 2nd 2019 and Filed on April 4th 2019.",
                "decided_date_extracted": "April 2nd 2019",
                "decided_date_normalized": "2019-04-02",
                "filed_date_extracted": "April 4th 2019",
                "filed_date_normalized": "2019-04-04",
            },
        )

    def test_docket_number_normalizer(self):
        normalizer = DocketNumberChunkNormalizer()
        rows = [{"words-normalized": "Case No. 2:19-cv-01325-GMN-NJK"}]
        self.assertEqual(
            normalizer.normalize(rows, "nvdi00"),
            {
                "docket_number_normalized": "2:19-cv-01325-GMN-NJK",
                "docket_number_extracted": "Case No. 2:19-cv-01325-GMN-NJK",
            },
        )

    def test_court_normalizer(self):
        normalizer = CourtChunkNormalizer()
        rows = [
            {"words-normalized": "IN THE UNITED STATES DISTRICT COURT FOR THE DISTRICT OF ALASKA"}
        ]
        self.assertEqual(
            normalizer.normalize(rows),
            {
                "court_normalized": "IN THE UNITED STATES DISTRICT COURT FOR THE DISTRICT OF ALASKA",
                "court_extracted": "IN THE UNITED STATES DISTRICT COURT FOR THE DISTRICT OF ALASKA",
            },
        )

    def test_pacer_header_normalizer(self):
        normalizer = PacerHeaderChunkNormalizer()
        rows = [
            {"words-normalized": "Case 3:16-cv-00267-SLG Document 302 Filed 08/06/18 Page 1 of 4"}
        ]
        self.assertEqual(
            normalizer.normalize(rows),
            {
                "pacer_header_extracted": "Case 3:16-cv-00267-SLG Document 302 Filed 08/06/18 Page 1 of 4",
                "pacer_date_extracted": "08/06/18",
                "pacer_date_normalized": "2018-08-06",
            },
        )

    def test_pacer_header_normalizer_no_date(self):
        normalizer = PacerHeaderChunkNormalizer()
        rows = [{"words-normalized": "Case 3:16-cv-00267-SLG Document 302 Page 1 of 4"}]
        self.assertEqual(
            normalizer.normalize(rows),
            {"pacer_header_extracted": "Case 3:16-cv-00267-SLG Document 302 Page 1 of 4"},
        )

    def test_normalizer_factory(self):
        factory = NormalizerFactory()
        self.assertTrue(
            isinstance(factory.create("Case Name"), CaseNameChunkNormalizer)
            and isinstance(factory.create("Pacer Header"), PacerHeaderChunkNormalizer)
            and isinstance(factory.create("Date"), DateChunkNormalizer)
            and isinstance(factory.create("Court"), CourtChunkNormalizer)
            and isinstance(factory.create("Docket Number"), DocketNumberChunkNormalizer)
        )

    def test_normalizer_factory_exception(self):
        factory = NormalizerFactory()
        with self.assertRaises(KeyError):
            factory.create("thisisatest")

    @patch.dict("normalize.patterns", {"nvdi00": re.compile(".*")})
    def test_docket_number_normalizer_capture_group_exception(self):
        normalizer = DocketNumberChunkNormalizer()
        rows = [{"words-normalized": "Case No. 2:19-cv-01325-GMN-NJK"}]
        with self.assertNotRaises(Exception):
            normalizer.normalize(rows, "nvdi00")

    def test_docket_number_normalizer_bad_chunk(self):
        normalizer = DocketNumberChunkNormalizer()
        rows = [{}]
        with self.assertNotRaises(Exception):
            normalizer.normalize(rows, "nvdi00")

    def test_court_normalizer_bad_chunk(self):
        normalizer = CourtChunkNormalizer()
        rows = [{}]
        with self.assertNotRaises(Exception):
            normalizer.normalize(rows, "nvdi00")

    def test_pacer_header_normalizer_bad_chunk(self):
        normalizer = PacerHeaderChunkNormalizer()
        rows = [{}]
        with self.assertNotRaises(Exception):
            normalizer.normalize(rows, "nvdi00")

    def test_date_normalizer_bad_chunk(self):
        normalizer = DateChunkNormalizer()
        rows = [{}]
        with self.assertNotRaises(Exception):
            normalizer.normalize(rows, "nvdi00")

    def test_decided_date_normalizer_multiple_dates_warning(self):
        normalizer = DateChunkNormalizer()
        rows = [
            {
                "words-normalized": "Decided on April 2nd 2019 and Filed on April 4th 2019. Refiled on April 5th 2019."
            }
        ]
        with self.assertNotRaises(Exception):
            normalizer.normalize(rows)

    def test_decided_date_normalizer_no_dates(self):
        normalizer = DateChunkNormalizer()
        rows = [{"words-normalized": "This is not a date chunk."}]
        with self.assertNotRaises(Exception):
            normalizer.normalize(rows)


if __name__ == "__main__":
    unittest.main()
