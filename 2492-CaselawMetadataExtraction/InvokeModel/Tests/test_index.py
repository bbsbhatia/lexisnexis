import pandas as pd
import unittest

from unittest.mock import patch, Mock
from index import (
    get_secret,
    get_s3_locators,
    update_record,
    get_file_from_s3,
    get_predictions_frame,
    transform_predictions,
    filter_response,
    get_document_html,
    get_document_features,
    build_response,
    handler,
)


class InvokeModelTests(unittest.TestCase):
    @patch("index.boto3")
    def test_get_secret(self, boto3):
        client = Mock()
        client.get_secret_value.return_value = {"SecretString": '{"key": "val"}'}
        boto3.client.return_value = client
        self.assertTrue(isinstance(get_secret("test", "1", "1"), dict))

    @patch("index.sqlalchemy")
    def test_get_s3_locators(self, sqlalchemy):
        result_proxy = Mock()
        result_proxy.fetchone.return_value = {"original_s3_uri": "s3://bucket/key"}
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        result = get_s3_locators(engine, "this is a test")
        self.assertEqual(result, ("bucket", "key"))

    @patch("index.sqlalchemy")
    def test_update_record(self, sqlalchemy):
        result_proxy = Mock()
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        self.assertTrue(update_record(engine, "1", {"key": "val"}))

    @patch("index.boto3")
    @patch.dict("index.os.environ", {"MODEL_ENDPOINT_NAME": "test"})
    def test_get_predictions_frame(self, boto3):
        import io

        client = Mock()
        client.invoke_endpoint.return_value = {"Body": io.BytesIO(b"1,2,3,target")}
        boto3.client.return_value = client
        self.assertTrue(isinstance(get_predictions_frame("test"), pd.DataFrame))

    def test_transform_predictions(self):
        df = pd.DataFrame(
            [
                {
                    "y-exact": 0,
                    "x-exact": 0,
                    "prediction": "Case Name",
                    "words-normalized": "PartyOne, Plaintiff",
                },
                {"y-exact": 1, "x-exact": 0, "prediction": "UNUSED", "words-normalized": ""},
                {"y-exact": 2, "x-exact": 0, "prediction": "Case Name", "words-normalized": "v."},
                {
                    "y-exact": 3,
                    "x-exact": 0,
                    "prediction": "Case Name",
                    "words-normalized": "PartyTwo, Defendant.",
                },
                {
                    "y-exact": 4,
                    "x-exact": 0,
                    "prediction": "Case Name",
                    "words-normalized": "Judge Name, U.S.D.J.",
                },
            ]
        )
        self.assertEqual(
            transform_predictions(df),
            {
                "case_name_normalized": "PartyOne, Plaintiff v. PartyTwo, Defendant.",
                "case_name_extracted": "PartyOne, Plaintiff v. PartyTwo, Defendant. Judge Name, U.S.D.J.",
            },
        )

    def test_filter_response_without_body_metadata(self):
        self.assertEqual(filter_response({"pacer_date_normalized": "2019-08-06"}), {})

    def test_filter_response_with_body_metadata(self):
        self.assertEqual(
            filter_response(
                {
                    "case_name_extracted": "PartyOne, Plaintiff v. PartyTwo, Defendant",
                    "case_name_normalized": "PartyOne, Plaintiff v. PartyTwo, Defendant",
                }
            ),
            {
                "case_name_extracted": "PartyOne, Plaintiff v. PartyTwo, Defendant",
                "case_name_normalized": "PartyOne, Plaintiff v. PartyTwo, Defendant",
            },
        )

    @patch("index.requests")
    @patch("index.boto3.resource")
    @patch("index.build_row")
    @patch.dict("index.os.environ", {"ECS_CLUSTER_DNS": "test"})
    def test_get_document_features_success(self, build_row, resource, requests):
        response = Mock()
        response.content = '{"output_s3_uri": "s3://test/test"}'
        requests.post.return_value = response
        file_ = Mock()
        file_.read.return_value = b"""
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>                                        
                </body>
            </html>"""
        resource.return_value.Object.return_value.get.return_value = {"Body": file_}
        build_row.return_value = pd.Series(["a", "b", "c", "d"])
        self.assertTrue(
            get_document_features("test", "test", "test")
            == (
                "0,1,2,3\na,b,c,d\na,b,c,d\n",
                {"page_count": 3, "pdf_transform_s3_uri": "s3://test/test"},
            )
        )

    @patch("index.requests")
    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.build_row")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.filter_response")
    @patch.dict(
        "index.os.environ",
        {"ECS_CLUSTER_DNS": "test", "RDS_SECRET_ARN": "test", "MODEL_ENDPOINT_NAME": "test"},
    )
    def test_handler_success(
        self, filter_response, get_secret, sqlalchemy, build_row, client, resource, requests
    ):
        response = Mock()
        response.content = '{"output_s3_uri": "s3://test/test"}'
        requests.post.return_value = response
        file_ = Mock()
        file_.read.return_value = b"""
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>
                </body>
            </html>"""
        prediction = Mock()
        prediction.read.return_value = b"1,2,3,4,5"
        client.return_value.invoke_endpoint.return_value = {"Body": prediction}
        resource.return_value.Object.return_value.get.return_value = {"Body": file_}
        resource.return_value.Bucket.return_value.put.return_value = {
            "ResponseMetadata": {"HTTPStatusCode": 200}
        }

        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        result_proxy = Mock()
        result_proxy.fetchone.return_value = {"original_s3_uri": "s3://test/test"}
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        sqlalchemy.create_engine.return_value = engine
        filter_response.return_value = {
            "case_name_normalized": "PartyOne, Plaintiff v. PartyTwo, Defendant."
        }
        build_row.return_value = pd.Series(["a", "b", "c", "d"])
        self.assertTrue(
            handler(
                {
                    "body": """{
                    "input_data": {
                        "document_id": "test"
                    },
                    "parameters":{
                        "include_fields": []
                    }
                }"""
                },
                {},
            )["statusCode"]
            == 200
        )

    @patch("index.transform_predictions")
    @patch("index.requests")
    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.build_row")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch.dict(
        "index.os.environ",
        {"ECS_CLUSTER_DNS": "test", "RDS_SECRET_ARN": "test", "MODEL_ENDPOINT_NAME": "test"},
    )
    def test_transform_predictions_error(
        self, get_secret, sqlalchemy, build_row, client, resource, requests, transform_predictions
    ):
        response = Mock()
        response.content = '{"output_s3_uri": "s3://test/test"}'
        requests.post.return_value = response
        file_ = Mock()
        file_.read.return_value = b"""
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>
                </body>
            </html>"""
        prediction = Mock()
        prediction.read.return_value = b"1,2,3,4,5"
        client.return_value.invoke_endpoint.return_value = {"Body": prediction}
        resource.return_value.Object.return_value.get.return_value = {"Body": file_}
        resource.return_value.Bucket.return_value.put.return_value = {
            "ResponseMetadata": {"HTTPStatusCode": 200}
        }

        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        result_proxy = Mock()
        result_proxy.fetchone.return_value = {"original_s3_uri": "s3://test/test"}
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        sqlalchemy.create_engine.return_value = engine
        transform_predictions.side_effect = Exception()
        build_row.return_value = pd.Series(["a", "b", "c", "d"])
        self.assertTrue(
            handler(
                {
                    "body": """{
                    "input_data": {
                        "document_id": "test"
                    },
                    "parameters":{
                        "include_fields": []
                    }
                }"""
                },
                {},
            )["statusCode"]
            == 500
        )

    @patch("index.get_secret")
    @patch("index.get_s3_locators")
    def test_handler_s3_locator_error(self, get_s3_locators, get_secret):
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        get_s3_locators.side_effect = Exception()
        self.assertTrue(
            handler(
                {
                    "body": """{
                    "input_data": {
                        "document_id": "test"
                    },
                    "parameters":{
                        "include_fields": []
                    }
                }"""
                },
                {},
            )["statusCode"]
            == 500
        )

    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.get_s3_locators")
    @patch("index.get_document_features")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test"})
    def test_handler_get_document_features_error(
        self, get_document_features, get_s3_locators, get_secret, sqlalchemy
    ):
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        get_s3_locators.return_value = "bucket", "key"
        get_document_features.side_effect = Exception()
        result_proxy = Mock()
        result_proxy.fetchone.return_value = {"original_s3_uri": "s3://test/test"}
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        sqlalchemy.create_engine.return_value = engine
        self.assertTrue(
            handler(
                {
                    "body": """{
                    "input_data": {
                        "document_id": "test"
                    },
                    "parameters":{
                        "include_fields": []
                    }
                }"""
                },
                {},
            )["statusCode"]
            == 500
        )

    def test_handler_request_error(self):
        self.assertTrue(
            handler(
                {
                    "body": """{
                    "parameters":{
                        "include_fields": []
                    }
                }"""
                },
                {},
            )["statusCode"]
            == 400
        )

    @patch("index.requests")
    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.build_row")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch.dict(
        "index.os.environ",
        {"ECS_CLUSTER_DNS": "test", "RDS_SECRET_ARN": "test", "MODEL_ENDPOINT_NAME": "test"},
    )
    def test_handler_invoke_endpoint_error(
        self, get_secret, sqlalchemy, build_row, client, resource, requests
    ):
        response = Mock()
        response.content = '{"output_s3_uri": "s3://test/test"}'
        requests.post.return_value = response
        file_ = Mock()
        file_.read.return_value = b"""
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>
                </body>
            </html>"""
        prediction = Mock()
        prediction.read.return_value = b"1,2,3,4,5"
        client.return_value.invoke_endpoint.side_effect = Exception()
        resource.return_value.Object.return_value.get.return_value = {"Body": file_}
        resource.return_value.Bucket.return_value.put.return_value = {
            "ResponseMetadata": {"HTTPStatusCode": 200}
        }

        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        result_proxy = Mock()
        result_proxy.fetchone.return_value = {"original_s3_uri": "s3://test/test"}
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        sqlalchemy.create_engine.return_value = engine

        build_row.return_value = pd.Series(["a", "b", "c", "d"])
        self.assertTrue(
            handler(
                {
                    "body": """{
                    "input_data": {
                        "document_id": "test"
                    },
                    "parameters":{
                        "include_fields": []
                    }
                }"""
                },
                {},
            )["statusCode"]
            == 500
        )

    @patch("index.requests")
    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.build_row")
    @patch("index.transform_predictions")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.update_record")
    @patch.dict(
        "index.os.environ",
        {"ECS_CLUSTER_DNS": "test", "RDS_SECRET_ARN": "test", "MODEL_ENDPOINT_NAME": "test"},
    )
    def test_handler_update_record_error(
        self,
        update_record,
        get_secret,
        sqlalchemy,
        transform_predictions,
        build_row,
        client,
        resource,
        requests,
    ):
        response = Mock()
        response.content = '{"output_s3_uri": "s3://test/test"}'
        requests.post.return_value = response
        file_ = Mock()
        file_.read.return_value = b"""
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>
                </body>
            </html>"""
        prediction = Mock()
        prediction.read.return_value = b"1,2,3,4,5"
        client.return_value.invoke_endpoint.return_value = {"Body": prediction}
        resource.return_value.Object.return_value.get.return_value = {"Body": file_}
        resource.return_value.Bucket.return_value.put.return_value = {
            "ResponseMetadata": {"HTTPStatusCode": 200}
        }

        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        result_proxy = Mock()
        result_proxy.fetchone.return_value = {"original_s3_uri": "s3://test/test"}
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        sqlalchemy.create_engine.return_value = engine
        update_record.side_effect = Exception()
        build_row.return_value = pd.Series(["a", "b", "c", "d"])
        transform_predictions.return_value = pd.DataFrame(
            [["test v. test", "PERSON", "Case Name", 1, 1], ["test", "", "UNUSED", 1, 1]],
            columns=["words-normalized", "entities-types", "prediction", "y-exact", "x-exact"],
        )
        self.assertTrue(
            handler(
                {
                    "body": """{
                    "input_data": {
                        "document_id": "test"
                    },
                    "parameters":{
                        "include_fields": []
                    }
                }"""
                },
                {},
            )["statusCode"]
            == 500
        )


if __name__ == "__main__":
    unittest.main()
