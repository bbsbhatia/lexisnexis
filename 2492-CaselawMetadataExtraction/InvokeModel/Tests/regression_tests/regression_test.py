import logging
import pickle
from abc import ABC, abstractmethod


class RegressionTest(ABC):
    def __init__(self, results_path, logger, verbose=True):
        self.results_path = results_path
        self.logger = logger
        self.verbose = verbose

    def log(self, message, level=logging.INFO):
        if self.verbose:
            self.logger.log(level, message)

    def evalulate(self):
        tests_passing = False
        prior_results = self.load_prior_results()
        current_results = self.run_test_cases()
        regressed_cases = prior_results.difference(current_results)

        if len(regressed_cases) == 0:
            self.log("No regression")
            tests_passing = True
        else:
            self.log("Regression!")

        if tests_passing and len(current_results) > len(prior_results):
            self.log("Saving new golden set")
            self.save_new_results(current_results)
        return tests_passing

    def save_new_results(self, results):
        with open(self.results_path, "wb") as out_f:
            pickle.dump(results, out_f, pickle.HIGHEST_PROTOCOL)

    def load_prior_results(self):
        prior_results = set()
        try:
            with open(self.results_path, "rb") as in_f:
                prior_results = pickle.load(in_f)
        except Exception:
            self.log("No prior results.")
        return prior_results

    @abstractmethod
    def run_test_cases(self):
        pass
