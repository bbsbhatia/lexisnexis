import logging
import hashlib
import os
from .regression_test import RegressionTest


class DocketNumberRegressionTest(RegressionTest):
    def __init__(self, docket_number_patterns, court_test_cases, logger, verbose=True):
        self.docket_number_patterns = docket_number_patterns
        self.court_test_cases = court_test_cases
        file_path = os.path.dirname(__file__)
        result_spath = os.path.join(file_path, "./results/docket_number_regression_results.pickle")
        super().__init__(result_spath, logger, verbose)

    def run_test_cases(self):
        court_stats = {}
        results = set()
        for court, test_cases in self.court_test_cases.items():
            pattern = self.docket_number_patterns.get(court)
            court_stats[court] = {
                "match_count": 0,
                "error_count": 0,
                "pattern": pattern,
                "errors": [],
            }
            if not pattern:
                continue
            for case in test_cases:
                try:
                    result = None
                    match = pattern.search(case["raw_text"])
                    if match and len(match.groups()) == 1:
                        result = match.group(0)
                    elif match and len(match.groups()) == 2:
                        result = match.group(1)
                    elif match and len(match.groups()) > 2:
                        result = "MULTIPLE CAPTURE GROUPS: " + "|".join(match.groups())
                    assert result and result.lower() == case["expected_result"].lower()
                    court_stats[court]["match_count"] += 1
                    md5 = hashlib.md5()
                    md5.update(
                        f"{court}-{pattern.pattern}-{case['raw_text']}-{case['expected_result']}".encode(
                            "utf-8"
                        )
                    )
                    results.add(md5.digest())
                except AssertionError:
                    court_stats[court]["error_count"] += 1
                    court_stats[court]["errors"].append(
                        f"\t\tInput: {case['raw_text']}\n\t\tExpected: {case['expected_result']}\n\t\tMatched: {result}\n"
                    )
        for court, stats in sorted(court_stats.items()):
            self.log("=" * 80)
            self.log(f"Court: {court}")
            self.log(f"Pattern: {stats['pattern']}")
            self.log(f"Match count: {stats['match_count']}")
            self.log(f"Error count: {stats['error_count']}")
            self.log("Errors:\n")
            for i, error in enumerate(stats["errors"]):
                self.log(f"\t{i+1}.)")
                self.log(error)
        return results


if __name__ == "__main__":

    import sys

    sys.path.append("../../Source/Lambda/")
    from docket_number_patterns import patterns
    from docket_regression_cases import docket_regression_cases

    formatter = logging.Formatter("%(message)s")
    log_handler = logging.StreamHandler()
    log_handler.setFormatter(formatter)
    logger = logging.getLogger(__name__)
    logger.addHandler(log_handler)
    logger.setLevel(logging.INFO)
    docket_regression_test = DocketNumberRegressionTest(
        patterns, docket_regression_cases, logger, verbose=True
    )
    docket_regression_test.evalulate()
