import pandas as pd
import unittest

from bs4 import BeautifulSoup
from unittest.mock import patch, Mock
from transform import UniqueList, entity_string, mode, element_text, nlp_annotate, build_row


class TransformTests(unittest.TestCase):
    def test_unique_list_unique(self):
        unique_list = UniqueList([1, 2])
        unique_list.append(1)
        unique_list.append(2)
        unique_list.append(3)
        self.assertTrue(unique_list == [1, 2, 3])

    def test_unique_list_repr(self):
        unique_list = UniqueList([1, 2])
        self.assertTrue(repr(unique_list) == "1, 2")

    def test_unique_list_str(self):
        unique_list = UniqueList([1, 2])
        self.assertTrue(str(unique_list) == "1, 2")

    def test_entity_string(self):
        annotations = [
            {"word": "test", "ner": "PERSON"},
            {"word": "test", "ner": "PERSON"},
            {"word": "test", "ner": "MISC"},
        ]
        self.assertTrue(entity_string(annotations, reduce=True) == "PERSON test")

    def test_mode(self):
        numbers = [1, 1, 1, 3, 3, 3, 4, 2]
        self.assertTrue(mode(numbers) == [1, 3])

    def test_mode_min(self):
        numbers = [1, 1, 1, 3, 3, 3, 4, 2]
        self.assertTrue(mode(numbers, min_max="min") == 1)

    def test_mode_max(self):
        numbers = [1, 1, 1, 3, 3, 3, 4, 2]
        self.assertTrue(mode(numbers, min_max="max") == 3)

    def test_element_text_not_all_one_len(self):
        soup = BeautifulSoup(
            """
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=12>T</word>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>est</word>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>                                        
                </body>
            </html>""",
            "lxml",
        )
        self.assertTrue(element_text(soup, normalize=False) == "Test test")

    def test_element_text_all_one_len(self):
        soup = BeautifulSoup(
            """
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>0</word>
                        </block>
                    </page>                                        
                </body>
            </html>""",
            "lxml",
        )
        self.assertTrue(element_text(soup, normalize=False) == "0")

    def test_element_text_normalize(self):
        soup = BeautifulSoup(
            """
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>this</word>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>-</word>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>is</word>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>a</word>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>-</word>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>                                        
                </body>
            </html>""",
            "lxml",
        )
        self.assertTrue(element_text(soup, normalize=True) == "this is a test")

    def test_nlp_annotate_no_text(self):
        self.assertTrue(nlp_annotate("") == [])

    @patch("transform.requests")
    @patch.dict("index.os.environ", {"ECS_CLUSTER_DNS": "test"})
    def test_nlp_annotate_success(self, requests):
        requests.post.return_value.content = b'{"sentences": [{"tokens":[{}]}]}'
        self.assertTrue(nlp_annotate("this is a test") == [{}])

    @patch("transform.requests")
    @patch.dict("index.os.environ", {"ECS_CLUSTER_DNS": "test"})
    def test_nlp_annotate_service_error(self, requests):
        requests.post.return_value.content = b""
        self.assertTrue(nlp_annotate("this is a test") == [])

    @patch("transform.requests")
    @patch.dict("index.os.environ", {"ECS_CLUSTER_DNS": "test"})
    def test_build_row(self, requests):
        requests.post.return_value.content = (
            b'{"sentences": [{"tokens":[{"word": "test", "ner": "MISC", "pos": "O"}]}]}'
        )
        soup = BeautifulSoup(
            """
            <html>
                <body>
                    <page width=10 height=10>
                        <block xmin=0 xmax=10 ymin=0 ymax=10>
                            <word xmin=0 xmax=10 ymin=0 ymax=10>test</word>
                        </block>
                    </page>                                        
                </body>
            </html>""",
            "lxml",
        )
        page = soup.find("page")
        block = page.find("block")
        self.assertTrue(isinstance(build_row("test", block, page, 0, 1), pd.Series))


if __name__ == "__main__":
    unittest.main()
