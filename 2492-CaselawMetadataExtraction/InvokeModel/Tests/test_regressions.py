import unittest
import logging
from collections import defaultdict
from docket_number_patterns import patterns
from regression_tests import docket_regression_cases
from regression_tests import DocketNumberRegressionTest


class DocketNumberPatternsTest(unittest.TestCase):
    def test_pattern_matches(self):
        print(type(docket_regression_cases))
        test_runner = DocketNumberRegressionTest(
            patterns, docket_regression_cases, logging.getLogger("dummy"), False
        )
        tests_passing = test_runner.evalulate()
        self.assertTrue(tests_passing)


if __name__ == "__main__":
    unittest.main()
