{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "Resources for Case Law Metadata Extraction ECS cluster.",
    "Parameters": {
        "AssetAreaName": {
            "Description": "LNG Asset Resource Name",
            "Type": "String",
            "Default": "CaseLawMetadataExtraction"
        },
        "AssetGroup": {
            "Description": "LNG Asset Group",
            "Type": "String",
            "AllowedValues": [
                "sb1",
                "ddc1",
                "cdc1",
                "pdc1"
            ]
        },
        "AssetID": {
            "Description": "LNG Asset ID",
            "Type": "String",
            "Default": "2492",
            "AllowedValues": [
                "2492"
            ]
        },
        "AssetName": {
            "Description": "LNG Asset Name",
            "Type": "String",
            "Default": "CaseLawMetadataExtraction",
            "AllowedValues": [
                "CaseLawMetadataExtraction"
            ]
        },
        "Build": {
            "Description": "Jenkins Build Number",
            "Type": "String",
            "Default": "1"
        }
    },
    "Resources": {
        "CloudMetadata": {
            "Type": "Custom::CloudMetadata",
            "Properties": {
                "ServiceToken": {
                    "Fn::Sub": "arn:aws:lambda:us-east-1:807841377931:function:cloud_metadata:STABLE"
                },
                "AssetID": {
                    "Ref": "AssetID"
                },
                "AssetGroup": {
                    "Ref": "AssetGroup"
                },
                "AssetAreaName": {
                    "Ref": "AssetAreaName"
                },
                "Version": "1",
                "LastUpdate": {
                    "Ref": "Build"
                }
            }
        },
        "CWLogGroup": {
            "Type": "AWS::Logs::LogGroup",
            "Properties": {}
        },
        "ECSCluster": {
            "Type": "AWS::ECS::Cluster",
            "Properties": {
                "Tags": [
                    {
                        "Key": "AssetGroup",
                        "Value": {
                            "Ref": "AssetGroup"
                        }
                    },
                    {
                        "Key": "AssetID",
                        "Value": {
                            "Ref": "AssetID"
                        }
                    },
                    {
                        "Key": "AssetAreaName",
                        "Value": {
                            "Ref": "AssetAreaName"
                        }
                    },
                    {
                        "Key": "AssetName",
                        "Value": {
                            "Ref": "AssetName"
                        }
                    }
                ]                
            }            
        },
        "SecurityGroup": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "GroupDescription": "Security group for the case law metadata extraction ECS cluster",
                "SecurityGroupEgress": [
                    {
                        "IpProtocol": "-1",
                        "FromPort": "-1",
                        "ToPort": "-1",
                        "CidrIp": "0.0.0.0/0"
                    }
                ],
                "SecurityGroupIngress": [
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "80",
                        "ToPort": "80",
                        "CidrIp": "10.0.0.0/8"
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "443",
                        "ToPort": "443",
                        "CidrIp": "10.0.0.0/8"
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "80",
                        "ToPort": "80",
                        "CidrIp": "172.29.0.0/16"
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "443",
                        "ToPort": "443",
                        "CidrIp": "172.29.0.0/16"
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "80",
                        "ToPort": "80",
                        "CidrIp": "138.12.0.0/16"
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "443",
                        "ToPort": "443",
                        "CidrIp": "138.12.0.0/16"
                    }
                ],
                "VpcId": {
                    "Fn::GetAtt": [
                        "CloudMetadata",
                        "vpc.Id"
                    ]
                }
            }
        },
        "LoadBalancer": {
            "Type": "AWS::ElasticLoadBalancingV2::LoadBalancer",
            "Properties": {
                "Scheme": "internal",
                "Subnets": {
                    "Fn::GetAtt": [
                        "CloudMetadata",
                        "vpc.PrivateSubnetList"
                    ]
                },
                "LoadBalancerAttributes": [
                    {
                        "Key": "idle_timeout.timeout_seconds",
                        "Value": "50"
                    }
                ],
                "SecurityGroups": [
                    {
                        "Ref": "SecurityGroup"
                    },
                    {
                        "Fn::GetAtt": [
                            "CloudMetadata",
                            "vpc.SecurityGroups.SgInternetOut"
                        ]
                    },
                    {
                        "Fn::GetAtt": [
                            "CloudMetadata",
                            "vpc.SecurityGroups.SgRemoteAccess"
                        ]
                    }
                ]
            }
        },
        "TargetGroup": {
            "Type": "AWS::ElasticLoadBalancingV2::TargetGroup",
            "Properties": {
                "HealthCheckIntervalSeconds": 30,
                "HealthCheckProtocol": "HTTP",
                "HealthCheckTimeoutSeconds": 10,
                "HealthyThresholdCount": 2,
                "Port": 80,
                "Protocol": "HTTP",
                "UnhealthyThresholdCount": 2,
                "VpcId": {
                    "Fn::GetAtt": [
                        "CloudMetadata",
                        "vpc.Id"
                    ]
                }
            }
        },
        "Listener": {
            "Type": "AWS::ElasticLoadBalancingV2::Listener",
            "DependsOn": "LoadBalancer",
            "Properties": {
                "DefaultActions": [
                    {
                        "Type": "forward",
                        "TargetGroupArn": {
                            "Ref": "TargetGroup"
                        }
                    }
                ],
                "LoadBalancerArn": {
                    "Ref": "LoadBalancer"
                },
                "Port": "80",
                "Protocol": "HTTP"
            }
        },
        "ECSRole": {
            "Type": "AWS::IAM::Role",
            "Properties": {
                "AssumeRolePolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {
                                "Service": [
                                    "ecs.amazonaws.com"
                                ]
                            },
                            "Action": [
                                "sts:AssumeRole"
                            ]
                        }
                    ]
                },
                "Path": {
                    "Fn::Sub": "/AssetApplication_${AssetID}/"
                },
                "Policies": [
                    {
                        "PolicyName": "ecs-service",
                        "PolicyDocument": {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "ec2:AttachNetworkInterface",
                                        "ec2:CreateNetworkInterface",
                                        "ec2:CreateNetworkInterfacePermission",
                                        "ec2:DeleteNetworkInterface",
                                        "ec2:DeleteNetworkInterfacePermission",
                                        "ec2:Describe*",
                                        "ec2:DetachNetworkInterface",
                                        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
                                        "elasticloadbalancing:DeregisterTargets",
                                        "elasticloadbalancing:Describe*",
                                        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                                        "elasticloadbalancing:RegisterTargets"
                                    ],
                                    "Resource": "*"
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "ECSTaskExecutionRole": {
            "Type": "AWS::IAM::Role",
            "Properties": {
                "AssumeRolePolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {
                                "Service": [
                                    "ecs-tasks.amazonaws.com"
                                ]
                            },
                            "Action": [
                                "sts:AssumeRole"
                            ]
                        }
                    ]
                },
                "Path": {
                    "Fn::Sub": "/AssetApplication_${AssetID}/"
                },
                "Policies": [
                    {
                        "PolicyName": "AmazonECSTaskExecutionRolePolicy",
                        "PolicyDocument": {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "ecr:GetAuthorizationToken",
                                        "ecr:BatchCheckLayerAvailability",
                                        "ecr:GetDownloadUrlForLayer",
                                        "ecr:BatchGetImage",
                                        "logs:CreateLogStream",
                                        "logs:PutLogEvents"
                                    ],
                                    "Resource": "*"
                                }
                            ]
                        }
                    }
                ]
            }
        }
    },
    "Outputs": {
        "GLOBALCaseLawMetadataExtractionECSClusterName": {
            "Description": "The name of the Case Law Metadata Extraction ECS cluster",
            "Value": {
                "Fn::Join": [
                    "|",
                    [
                        {
                            "Ref": "AssetGroup"
                        },
                        {
                            "Ref": "AssetID"
                        },
                        {
                            "Ref": "AssetAreaName"
                        },
                        {
                            "Ref": "ECSCluster"
                        }
                    ]
                ]
            }
        },
        "GLOBALCaseLawMetadataExtractionECSClusterArn": {
            "Description": "The name of the Case Law Metadata Extraction ECS cluster",
            "Value": {
                "Fn::Join": [
                    "|",
                    [
                        {
                            "Ref": "AssetGroup"
                        },
                        {
                            "Ref": "AssetID"
                        },
                        {
                            "Ref": "AssetAreaName"
                        },
                        {
                            "Fn::GetAtt": [
                                "ECSCluster",
                                "Arn"
                            ]
                        }
                    ]
                ]
            }
        },        
        "GLOBALCaseLawMetadataExtractionCWLogGroup": {
            "Description": "The name of the Case Law Metadata Extraction cloud watch log group",
            "Value": {
                "Fn::Join": [
                    "|",
                    [
                        {
                            "Ref": "AssetGroup"
                        },
                        {
                            "Ref": "AssetID"
                        },
                        {
                            "Ref": "AssetAreaName"
                        },
                        {
                            "Ref": "CWLogGroup"
                        }
                    ]
                ]
            }
        },
        "GLOBALCaseLawMetadataExtractionECSRoleArn": {
            "Description": "The ARN of the Case Law Metadata Extraction ECS role",
            "Value": {
                "Fn::Join": [
                    "|",
                    [
                        {
                            "Ref": "AssetGroup"
                        },
                        {
                            "Ref": "AssetID"
                        },
                        {
                            "Ref": "AssetAreaName"
                        },
                        {
                            "Fn::GetAtt": [
                                "ECSRole",
                                "Arn"
                            ]
                        }
                    ]
                ]
            }
        },
        "GLOBALCaseLawMetadataExtractionECSTaskExecutionRoleArn": {
            "Description": "The ARN of the Case Law Metadata Extraction ECS task execution role",
            "Value": {
                "Fn::Join": [
                    "|",
                    [
                        {
                            "Ref": "AssetGroup"
                        },
                        {
                            "Ref": "AssetID"
                        },
                        {
                            "Ref": "AssetAreaName"
                        },
                        {
                            "Fn::GetAtt": [
                                "ECSTaskExecutionRole",
                                "Arn"
                            ]
                        }
                    ]
                ]
            }
        },
        "GLOBALCaseLawMetadataExtractionListenerArn": {
            "Description": "The ARN of the Case Law Metadata Extraction load balancer's listener",
            "Value": {
                "Fn::Join": [
                    "|",
                    [
                        {
                            "Ref": "AssetGroup"
                        },
                        {
                            "Ref": "AssetID"
                        },
                        {
                            "Ref": "AssetAreaName"
                        },
                        {
                            "Ref": "Listener"
                        }
                    ]
                ]
            }
        },
        "GLOBALCaseLawMetadataExtractionSecurityGroup": {
            "Description": "The name/id of the Case Law Metadata Extraction security group",
            "Value": {
                "Fn::Join": [
                    "|",
                    [
                        {
                            "Ref": "AssetGroup"
                        },
                        {
                            "Ref": "AssetID"
                        },
                        {
                            "Ref": "AssetAreaName"
                        },
                        {
                            "Ref": "SecurityGroup"
                        }
                    ]
                ]
            }
        },
        "GLOBALCaseLawMetadataExtractionLoadBalancerDNSName": {
            "Description": "The DNS name of the Case Law Metadata Extraction ECS cluster load balancer",
            "Value": {
                "Fn::Join": [
                    "|",
                    [
                        {
                            "Ref": "AssetGroup"
                        },
                        {
                            "Ref": "AssetID"
                        },
                        {
                            "Ref": "AssetAreaName"
                        },
                        {
                            "Fn::GetAtt":  [
                                "LoadBalancer",
                                "DNSName"
                            ]
                        }
                    ]
                ]
            }
        }        
    }
}