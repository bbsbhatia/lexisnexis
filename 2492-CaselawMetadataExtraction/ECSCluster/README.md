## ECS Cluster

### Description
Amazon Elastic Container Service Cluster. Provides platform for container based tasks:
- CoreNLP service
- PDFToText service
- PostbackTransform service
