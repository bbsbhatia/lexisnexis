# Introduction 
Team Airtime has been building a caselaw metadata extraction service for the purpose of automating the IRT data entry screen. Our MVP solution was launched for case name extractions on May 8th and an expanded rollout took place on August 4th, which included extraction functionality for docket number, decided date, and filed date, along with case name improvements.

 

## Training Data

Our solution leverages positional/coordinate data associated with logical segments of a caselaw document, as well as a number of natural language features derived from that segment. In order to construct this training data, text-extractable PDF documents are first converted to an HTML format using a service that provides bounding box coordinates around each of the chunks of text identified within the document. Each of these chunks are then passed along to a natural language processing (NLP) service that provides both named entity and part-of-speech tags. At this point, the word chunks and their associated data points are formatted as a row-wise data set and labelled as to whether or not they represent part-of a key caselaw document attribute (there is the possibility for extraction targets to span multiple text chunks).

 

## Model Construction, Training, & Evaluation

Our classifier was written in Python, leveraging the Scikit-Learn and XGBoost libraries for our model pipeline and Flask for a simple webserver for invoking predictions and kicking off training tasks over HTTP. Our model pipeline consists of preprocessing steps for building TFIDF vectors from our natural language features, scaling, selection, and a gridsearch wrapper around our XGBoost classifier for the purpose of hyperparameter optimization over a 3-fold cross-validation strategy. At training time, this model pipeline is evaluated over a 10-fold, outer cross validation strategy (in other words, we're leveraging a nested 10/3 cross validation strategy where HPO occurs over the inner CV folds and evaluation occurs over the outer CV folds). Following evaluation, scores are recorded and a final model is fit, serialized, and stored in AWS S3.

 

## Supporting Services 

Our end-to-end solution leverages a number of AWS cloud resources in order to provide extracted attributes when given a pdf document; API Gateway serves as the interface for our service, Lambdas back all of the routes behind this interface, Simple Storage Service (S3) is used for storage throughout, an Elastic Container Service (ECS) cluster running PDFToText and CoreNLP services is leveraged to transform PDFs into model-invocation ready data, Sagemaker is used to host our classifier and manage training tasks, and an Aurora serverless cluster is used for document tracking.



## Infrastructure, Builds, and Deployments

All supporting infrastructure and code for this project is built and deployed from BCS Jenkins. All cloud resources are described by CloudFormation templates for consistent and reliable deployments across environments. The code backing our model is first baked into a Docker image and pushed to AWS Elastic Container Repository (ECR) from a Jenkins Pipeline. Then, another pipeline kicks off a Sagemaker training job with the ECR image URI and our latest training data. Finally, a third pipeline kicks off the deployment of the Sagemaker invocation endpoint with the trained model binary. This third pipeline implements a score check that will fail the deployment if the model at hand doesn't exceed prior deployed models.



## Workflow Integration

This extraction service is invoked from the Rocket conversion platform during the Metadata Extract stage. Extraction results are then supplied to IRT.



## Feedback Loop

As editors confirm extracted values in the IRT screen, they are posted back to the extraction service. A cronned ECS task, PostbackTransform, runs weekly to detect disagreements between predictions and confirmed results. If a disagreement is detected, then the service tries to locate the best fuzzy match for the correct value within the previously constructed document text chunks. If a match is found with a high degree of confidence, then the previously constructed document text chunks are relabeled, marked as training-eligible, and copied over to the training data location that future training jobs will pull from.



## Metrics

Metrics and reports are being built out in Tableau using IRT document and tracking data.

# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
2.	Software dependencies
3.	Latest releases
4.	API references

# Build and Test
TODO: Describe and show how to build your code and run the tests. 

# Contribute
TODO: Explain how other users and developers can contribute to make your code better. 

If you want to learn more about creating good readme files then refer the following [guidelines](https://www.visualstudio.com/en-us/docs/git/create-a-readme). You can also seek inspiration from the below readme files:
- [ASP.NET Core](https://github.com/aspnet/Home)
- [Visual Studio Code](https://github.com/Microsoft/vscode)
- [Chakra Core](https://github.com/Microsoft/ChakraCore)