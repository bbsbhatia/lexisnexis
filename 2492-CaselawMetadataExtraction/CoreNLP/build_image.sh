dockerfile=${1:-DevOps/Docker/Dockerfile}
image=${2:-corenlpservice}

docker build -f $dockerfile -t $image .
