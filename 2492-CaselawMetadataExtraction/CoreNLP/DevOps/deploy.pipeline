@Library('DevOpsShared@v1') _
pipeline {
    agent any
    parameters {
        string(name: 'releaseUnitBuild', description: 'The build number of the release unit to deploy')
        string(name: 'imageURL', description: 'The URL of the ECR image')
        string(name: 'targetAccount', description: 'The AWS account number to deploy into', defaultValue: PRODUCT_CONTENT_DEV)
        string(name: 'assetGroup', description: 'The name of the asset group to deploy into', defaultValue: 'ddc1')
    }
    environment {
        def ASSET_ID = "2492"
        def ASSET_NAME = "CaseLawMetadataExtraction"
        def PROJECT_PATH = "CoreNLP"
        def ASSET_AREA_NAME = "${ASSET_NAME}/${PROJECT_PATH}"
        def ASSET_GROUP = "${params.assetGroup}"
        def RELEASE_UNIT = "${commons.getBranchName()}/${params.releaseUnitBuild}"     
    }
    stages {
        stage('Stage Release Unit') {
            steps{
                stageReleaseUnit()
            }
        }              
        stage('Deploy Resources') {
            steps{
                cloudformation(
                    command: 'createChangeSet',
                    account: params.targetAccount,
                    capabilities: "CAPABILITY_NAMED_IAM",
                    templateName: 'CoreNLP',
                    assetGroup: ASSET_GROUP,
                    stackParameters: [  'AssetGroup': ASSET_GROUP,
                                        'Build': params.releaseUnitBuild,
                                        'ImageUrl': params.imageURL
                                    ]
                )
                cloudformation(
                    command: 'executeChangeSet',
                    account: params.targetAccount, 
                    templateName: 'CoreNLP'
                )
            }
        }
    }
}
