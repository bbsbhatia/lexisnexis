## CoreNLP service

### Description
Provides a containerized HTTP microservice around the Stanford CoreNLP application.

### Dependencies
#### Docker
See https://docs.docker.com/install/ for platform specific installation instructions, or use the following to install for Ubuntu:
```bash
#Add docker's official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#Set up stable docker repository.
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

#update package index now that we've added the docker repo
sudo apt-get -y update

#install docker community edition
sudo apt-get -y install docker-ce

#create docker group and config permissions
sudo groupadd docker
sudo usermod -aG docker $USER
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "/home/$USER/.docker" -R
```
### How to build application
```bash
docker build -f DevOps/Docker/Dockerfile -t corenlpservice .
```
### How to run application
```bash
docker run -p 80:80 -d --rm corenlpservice
...
...
[main] INFO CoreNLP - Starting server...
[main] INFO CoreNLP - StanfordCoreNLPServer listening at /0.0.0.0:80
```
### How to invoke 
```bash
curl -d "<string to annotate>" 0.0.0.0:80/corenlp
```
### Helper (optional) scripts
#### bootstrap.sh
Installs docker for an Ubuntu OS
#### build_image.sh
Small wrapper around docker build command setup
#### run_container.sh
Small wrapper for docker run command setup