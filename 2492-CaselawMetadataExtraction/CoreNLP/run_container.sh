image=${1:-corenlpservice}
CONTAINER_ID=$(docker run -p 80:80 -d --rm $image)
SHORT_CONTAINER_ID=${CONTAINER_ID:0:12}
echo $CONTAINER_ID
docker container ls | grep $SHORT_CONTAINER_ID