## Document Storage

### Description
Defines an S3 bucket for the storage of uploaded documents and extraction related artifacts.