import re
from dateutil.parser import parse

"""
A simple class/struct describing a mention of a date within the context of a text string.
"""


class DateMention:
    def __init__(self, str_matched, start_pos, end_pos, datetime_obj):
        self.matched_text = str_matched
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.date = datetime_obj

    def __str__(self):
        return '<DateMention matched_text="{}" start_pos="{}" end_pos="{}" />'.format(
            self.matched_text, self.start_pos, self.end_pos
        )

    def iso_8601_date_str(self):
        return "-".join(
            [str(self.date.year), str(self.date.month).zfill(2), str(self.date.day).zfill(2)]
        )


"""
A class to extract mentions of dates within the context of a text string
"""


class DateMentionsExtractor:

    date_re = None

    def __init__(self, context_str):
        self.context_str = context_str
        self.date_mentions = None  # Loaded by the extract() method

    """
    Static/class method to lazy-load a complex regular expression, on-demand.
    """

    @classmethod
    def __compile_re(cls):

        if cls.date_re is not None:
            return

        short_month2num = {
            "jan": 1,
            "feb": 2,
            "mar": 3,
            "apr": 4,
            "may": 5,
            "jun": 6,
            "jul": 7,
            "aug": 8,
            "sep": 9,
            "sept": 9,
            "oct": 10,
            "nov": 11,
            "dec": 12,
        }

        long_month2num = {
            "january": 1,
            "february": 2,
            "march": 3,
            "april": 4,
            "may": 5,
            "june": 6,
            "july": 7,
            "august": 8,
            "september": 9,
            "october": 10,
            "november": 11,
            "december": 12,
        }

        short_weekdays = ["Mon", "Tue", "Tues", "Wed", "Weds", "Thur", "Thurs", "Fri"]

        long_weekdays = [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        ]

        re_short_weekdays = "|".join(short_weekdays)
        re_long_weekdays = "|".join(long_weekdays)
        re_short_or_long_weekdays = "|".join([re_short_weekdays, re_long_weekdays])

        re_year = r"(?:(?:(?:1|2)\d{3})|\d{2})"

        re_days_of_month_simple = "(?:" + "|".join([str(x) for x in range(1, 32)]) + ")"
        re_days_of_month_incl0 = "(?:" + "|".join([str(x).zfill(2) for x in range(1, 32)]) + ")"
        re_days_of_month = "(?:" + "|".join([re_days_of_month_simple, re_days_of_month_incl0]) + ")"

        re_month_long = "(?:" + "|".join(sorted(long_month2num.keys(), key=str.lower)) + ")"
        re_month_short = "(?:" + "|".join(sorted(short_month2num.keys(), key=str.lower)) + ")"
        re_month_short = "(?:" + re_month_short + "\.?)"
        re_month_num_simple = "(?:" + "|".join([str(x) for x in range(1, 13)]) + ")"
        re_month_num_incl0 = "(?:" + "|".join([str(x).zfill(2) for x in range(1, 13)]) + ")"
        re_month_nums = "(?:" + "|".join([re_month_num_simple, re_month_num_incl0]) + ")"
        re_short_or_long_month = "|".join([re_month_short, re_month_long])

        # Format: Tuesday, April 25, 2087
        # Format: Tuesday, Apr. 25, 2087
        # Format: Tuesday, Apr 25, 2087
        # Format: Sunday, December 13th, 2015
        re_long_date_1 = (
            "(?:" + re_short_or_long_weekdays + r")\s*,\s*"
            "(?:" + re_short_or_long_month + r")\s*"
            "(?:" + re_days_of_month + r")(?:st|nd|rd|th)?\s*,\s*"
            "(?:" + re_year + ")"
        )

        # Format: April 25, 2087
        # Format: Apr. 25, 2087
        # Format: Apr 25, 2087
        # Format: Apr 25 2087
        re_long_date_2 = (
            "(?:" + re_short_or_long_month + r")\s+"
            "(?:" + re_days_of_month + r")(?:st|nd|rd|th)?\s*,?\s+"
            "(?:" + re_year + ")"
        )

        # Format: 05-10-05, 05-10-2005
        re_short_date_1a = "-".join([re_month_nums, re_days_of_month, re_year])

        # Format: 05.10.04
        re_short_date_1b = r"\.".join([re_month_nums, re_days_of_month, re_year])

        # Format: 05/10/04
        re_short_date_1c = "/".join([re_month_nums, re_days_of_month, re_year])

        # Format: 25-Jan-2005
        re_short_date_2 = (
            "(?:" + re_days_of_month_incl0 + r")\-"
            "(?:" + re_month_short + r")\-"
            "(?:" + re_year + ")"
        )

        # Format: 17 Jan 2008
        re_short_date_3 = (
            "(?:"
            + re_days_of_month_incl0
            + r")\s+"
            + "(?:"
            + re_month_short
            + r")\s+"
            + "(?:"
            + re_year
            + ")"
        )

        re_iso_8601_date = r"(?:(?:1|2)\d{3}-\d{2}-\d{2})"

        date_regex_str = (
            r"(?:(?:^|\b)(?:"
            + "|".join(
                [
                    "\n\n(?:{})".format(x)
                    for x in [
                        re_short_date_1a,
                        re_short_date_1b,
                        re_short_date_1c,
                        re_iso_8601_date,
                        re_short_date_2,
                        re_short_date_3,
                        re_long_date_1,
                        re_long_date_2,
                    ]
                ]
            )
            + r")(?:\b|$))"
        )

        # print("RE STR: " + date_regex_str)

        cls.date_re = re.compile(date_regex_str, flags=re.I | re.S | re.X)

    def extract(self):

        if self.date_mentions is not None:
            return self.date_mentions

        DateMentionsExtractor.__compile_re()
        self.date_mentions = []

        for match in re.finditer(DateMentionsExtractor.date_re, self.context_str):

            # Skip invalid or unparseable dates
            date_obj = None
            try:
                date_obj = parse(match.group(0))
            except Exception as e:
                # print ("EXCEPTION CAUGHT: " + str(e))
                continue

            # Retain all mentioned dates which appear to be valid according to the Gregorian calendar.
            self.date_mentions.append(
                DateMention(match.group(0), match.start(), match.end(), date_obj)
            )

        return self.date_mentions
