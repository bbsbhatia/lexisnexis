from setuptools import setup, find_packages
from os import getenv

setup(
    name="lng_date_mentions_extractor",
    packages=find_packages(),
    version="1.1.%s" % getenv("BUILD_NUMBER", 0),
    description="Date extraction library.",
    install_requires=["python-dateutil"],
)

