import unittest

from lng_date_mentions_extractor.extractor import DateMentionsExtractor


class DateMentionsExtractorTest(unittest.TestCase):
    def test_true_positives(self):

        date_str = """
            Tuesday, April 25, 2087
            Tuesday, Apr. 25, 2087
            Tuesday, Apr 25, 2087
            Tuesday, December 1st, 2015
            Wednesday, December 2nd, 2015
            Thursday, December 3rd, 2015
            Sunday, December 13th, 2015

            April 25, 2087
            Apr. 25, 2087
            Apr 25, 2087
            Apr 25 2087

            5-10-05
            05.10.04
            05/10/04

            25-Jan-2005
            17 Jan 2008

            2017-01-01
            1125-12-31 # Oldy but a goodie?
        """

        dme = DateMentionsExtractor(date_str)

        # Did we extract the right number of dates?
        mentions = dme.extract()
        self.assertTrue(len(mentions) == 18)

        # Did we extract the correct dates from each mention?
        # print("ISO Date: " + mentions[0].iso_8601_date_str())
        self.assertTrue(mentions[0].iso_8601_date_str() == "2087-04-25")
        self.assertTrue(mentions[1].iso_8601_date_str() == "2087-04-25")
        self.assertTrue(mentions[2].iso_8601_date_str() == "2087-04-25")
        self.assertTrue(mentions[3].iso_8601_date_str() == "2015-12-01")
        self.assertTrue(mentions[4].iso_8601_date_str() == "2015-12-02")
        self.assertTrue(mentions[5].iso_8601_date_str() == "2015-12-03")
        self.assertTrue(mentions[6].iso_8601_date_str() == "2015-12-13")
        self.assertTrue(mentions[7].iso_8601_date_str() == "2087-04-25")
        self.assertTrue(mentions[8].iso_8601_date_str() == "2087-04-25")
        self.assertTrue(mentions[9].iso_8601_date_str() == "2087-04-25")
        self.assertTrue(mentions[10].iso_8601_date_str() == "2087-04-25")
        self.assertTrue(mentions[11].iso_8601_date_str() == "2005-05-10")
        self.assertTrue(mentions[12].iso_8601_date_str() == "2004-05-10")
        self.assertTrue(mentions[13].iso_8601_date_str() == "2004-05-10")
        self.assertTrue(mentions[14].iso_8601_date_str() == "2005-01-25")
        self.assertTrue(mentions[15].iso_8601_date_str() == "2008-01-17")
        self.assertTrue(mentions[16].iso_8601_date_str() == "2017-01-01")

    def test_false_positives(self):

        # Define some garbage dates
        date_str = """
            # Bogus years
            9999-01-01 # Bogus
            3001-01-01 # Bogus

            # Bogus months
            2019-00-25 # Bogus
            2019-13-25 # Bogus

            # Bogus days
            2019-01-32 # Bogus
            2019-02-30 # Bogus
            2019-03-32 # Bogus
            2019-04-31 # Bogus
            2019-05-32 # Bogus
            2019-06-31 # Bogus
            2019-07-32 # Bogus
            2019-08-32 # Bogus
            2019-09-31 # Bogus
            2019-10-32 # Bogus
            2019-11-31 # Bogus
            2019-12-32 # Bogus
        """
        dme = DateMentionsExtractor(date_str)
        mentions = dme.extract()
        self.assertTrue(len(mentions) == 0)

    def test_string_representation(self):
        date_str = "Tuesday, December 1st, 2015"
        dme = DateMentionsExtractor(date_str)
        mentions = dme.extract()
        print(mentions[0])
        self.assertEqual(
            mentions[0].__str__(),
            '<DateMention matched_text="Tuesday, December 1st, 2015" start_pos="0" end_pos="27" />',
        )

    def test_early_return(self):
        date_str = "Tuesday, December 1st, 2015"
        dme = DateMentionsExtractor(date_str)
        mentions_one = dme.extract()
        mentions_two = dme.extract()
        self.assertEqual(mentions_one, mentions_two)


if __name__ == "__main__":
    unittest.main()
