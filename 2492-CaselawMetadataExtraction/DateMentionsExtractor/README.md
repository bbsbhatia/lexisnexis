## Date Mentions Extractor Library
### Description
Sam Archer's date mentions extractor project wrapped as a pypi installable library. The build process stages this to the BCS artifactory PYPI index.
### How to install
```bash
pip install lng_date_mentions_extractor -i https://pypi.content.aws.lexis.com/simple
```

### How to use

```python
from lng_date_mentions_extractor.extractor import DateMentionsExtractor

date_mentions = DateMentionsExtractor("09/28/2019").extract()
```
