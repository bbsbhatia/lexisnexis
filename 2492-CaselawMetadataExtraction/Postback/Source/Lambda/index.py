import boto3
import json
import logging
import os
import time
import sqlalchemy

from typing import Any

logger = logging.getLogger()
if len(logger.handlers) > 0:
    for log_handler in logger.handlers:
        logger.removeHandler(log_handler)
logger.setLevel(logging.INFO)

engine = None


def get_secret(secret_id: str, version_id: str = None, version_stage: str = None) -> dict:
    """Get DB creds
    
    Retrieves DB credentials from AWS secrets manager.

    Args:
        (str) secret_id: id of secret to retrieve
        (str) version_id: version id of secret to retrieve
        (str) version_stage: version stage of secret to retrieve

    Returns: 
        (dict) mapping of secret keys to secret values.
    """
    client = boto3.client("secretsmanager")
    params = {"SecretId": secret_id}
    if version_id:
        params["VersionId"] = version_id
    if version_stage:
        params["VersionStage"] = version_stage
    response = client.get_secret_value(**params)
    return json.loads(response["SecretString"])


def update_record(engine: sqlalchemy.engine.base.Engine, doc_id: str, metadata: dict) -> bool:
    """Updates record
    
    Updates the record for a case law document to set a value for an attribute
    with a "_confirmed" suffix. This is done in order to denote a confirmed/correct
    metadata value, as opposed to a predicted one.

    Args:
        (str) doc_id: ID of the document we're updating.
        (dict) attrs: Attributes to write to the document record as confirmed metadata
            values.
    
    Returns:
        (bool) True if an update occurred, False if an update did not occur.
    """

    sql_chunks = []
    for key in metadata:
        sql_chunks.append(f"{key} = :{key}")
    sql_chunks.append("postback_timestamp = NOW()")

    sql = sqlalchemy.text(
        " ".join(
            [
                "UPDATE caselaw_metadata_extraction.document_tracking SET",
                ", ".join(sql_chunks),
                "WHERE id = :id",
            ]
        )
    )
    conn = engine.connect()
    result = conn.execute(sql, id=doc_id, **metadata)
    conn.close()
    return True if result.rowcount == 1 else False


def build_response(
    http_code: int, body: Any, headers: dict = None, base64_enc: bool = False
) -> dict:
    """builds response
    
    Builds a JSON response object to return to the requester (over HTTP via API Gateway).

    Args:
        (int) http_code: HTTP status code (200, 300, 400, 500, etc).
        (Any) body: Value to supply as response content.
        (dict) headers: Mapping of header keys to header values.
        (bool) base64_enc: Indicates whether or not body is a base 64 encoded string. 
    
    Returns:
        (dict) Response map.
    """
    return {
        "isBase64Encoded": base64_enc,
        "statusCode": http_code,
        "headers": headers,
        "body": body,
    }


def handler(event: dict, context: dict) -> dict:
    """Request handler

    Handles postback requests proxied from API gateway and orchestrates
    record update for supplied (and confirmed) metadata.

    Args:
        (dict) event: Event object, contains HTTP request metadata & content.
        (dict) context: Runtime context metadata.

    Returns:
        (dict) Response object, adhering to API Gateway proxy integration output format.
    """
    global engine
    logger.info(event)
    try:
        request_body = json.loads(event["body"])
        doc_id = request_body["input_data"]["document_id"]
        confirmed_vals = request_body["parameters"]
        logger.info(f"Received postback for {doc_id} with {', '.join(confirmed_vals.keys())}")
    except KeyError:
        message = "Error parsing request body. Required key(s) missing"
        logger.exception(message)
        return build_response(400, json.dumps(message))
    except json.decoder.JSONDecodeError:
        message = "Error parsing request body. Not valid JSON."
        logger.exception(message)
        return build_response(400, json.dumps(message))

    if not engine:
        creds = get_secret(os.environ["RDS_SECRET_ARN"])
        conn_str = "mysql+pymysql://{username}:{password}@{host}:{port}".format(**creds)
        engine = sqlalchemy.create_engine(conn_str)

    try:

        tracked_items = ["case_name", "court_name", "docket_number", "decided_date", "filed_date"]
        tracked_vals = {k: v for k, v in confirmed_vals.items() if k in tracked_items}
        normed_vals = {
            f"{k.lower().replace(' ', '_')}_confirmed": v for k, v in tracked_vals.items()
        }
        update_record(engine, doc_id, normed_vals)
    except:
        message = f"Error record with extracted metadata for {doc_id}"
        logger.exception(message)
        return build_response(500, json.dumps(message))

    response = build_response(200, json.dumps("Success"))
    return response
