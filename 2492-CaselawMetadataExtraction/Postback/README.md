## Postback lambda

### Description
Handles postbacks from Rocket (or others) containing confirmed case law metadata values.

### Dependencies
#### Python libraries
```bash
pip install -r requirements.txt
```
### How to run unit tests
#### Step 1: Install dependencies (see above)
#### Step 2: Run the test script
```bash
./run_tests
...
...
----------------------------------------------------------------------
Ran 8 tests in 0.032s

OK
Name                     Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------------
Source/Lambda/index.py      37      0     10      0   100%
```