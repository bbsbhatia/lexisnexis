import unittest

from unittest.mock import patch, Mock
from index import update_record, build_response, handler, logger, get_secret


class PostbackTests(unittest.TestCase):
    @patch("index.boto3")
    def test_get_secret(self, boto3):
        client = Mock()
        client.get_secret_value.return_value = {"SecretString": '{"key": "val"}'}
        boto3.client.return_value = client
        self.assertTrue(isinstance(get_secret("test", "1", "1"), dict))

    def test_update_record_no_attrs(self):
        result_proxy = Mock()
        result_proxy.rowcount = 0
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        self.assertFalse(update_record(engine, "test", {}))

    def test_update_record_with_attrs_dynamodb_success(self):
        result_proxy = Mock()
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        self.assertTrue(update_record(engine, "test", {"case_name": "test"}))

    def test_build_response(self):
        self.assertEquals(
            build_response(200, "{}", {}, True),
            {"isBase64Encoded": True, "statusCode": 200, "headers": {}, "body": "{}"},
        )

    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test"})
    def test_handler_valid_request(self, get_secret, sqlalchemy):
        event = {
            "body": """{
                    "input_data": {"document_id": "test"},
                    "parameters": {"case_name": "test"}
                }"""
        }
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        result_proxy = Mock()
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        sqlalchemy.create_engine.return_value = engine
        self.assertEquals(handler(event, {})["statusCode"], 200)

    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.update_record")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test"})
    def test_handler_update_record_error(self, update_record, get_secret, sqlalchemy):
        event = {
            "body": """{
                    "input_data": {"document_id": "test"},
                    "parameters": {"case_name": "test"}
                }"""
        }
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        update_record.side_effect = Exception()
        self.assertEquals(handler(event, {})["statusCode"], 500)

    def test_handler_missing_keys(self):
        event = {
            "body": """{
                    "parameters": {"case_name": "test"}
                }"""
        }
        self.assertEquals(handler(event, {})["statusCode"], 400)

    def test_handler_invalid_json(self):
        event = {
            "body": """{
                    "parameters": {"case_name": "test"},,,
                }"""
        }
        self.assertEquals(handler(event, {})["statusCode"], 400)


if __name__ == "__main__":
    unittest.main()
