## Security Groups

### Description
Defines security groups for the ECS Cluster for project-specific networking requirements.