## API Gateway

### Description
HTTP API for AIRTime metadata extraction activities. Routes supported are:
- /get-document-staging-data
  - Backed by GetDocumentStagingData lambda.
  - Resource for retrieving presigned s3 upload URL.
- /get-metadata
  - Backed by InvokeModel lambda.
  - Resource for retrieving a case law document extraction response.
- /postback
  - Backed by Postback lambda
  - Resource for posting editor-confirmed values back to document extraction tracking service. These confirmed values are used as additional training data for future training events.