## Classifier service

### Description
Provides a containerized HTTP microservice around a case law, word-chunk classifier with targets of case name, filed date, decided date, and docket number.

### Dependencies
#### Docker
See https://docs.docker.com/install/ for platform specific installation instructions, or use the following to install for Ubuntu:
```bash
#Add docker's official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#Set up stable docker repository.
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

#update package index now that we've added the docker repo
sudo apt-get -y update

#install docker community edition
sudo apt-get -y install docker-ce

#create docker group and config permissions
sudo groupadd docker
sudo usermod -aG docker $USER
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "/home/$USER/.docker" -R
```
### How to build application
```bash
docker build -f DevOps/Docker/Dockerfile -t classifierservice .
```
### How to run application

##### Train
```bash
AWS_ACCESS_KEY_ID=$(aws --profile default configure get aws_access_key_id)
AWS_SECRET_ACCESS_KEY=$(aws --profile default configure get aws_secret_access_key)
AWS_SESSION_TOKEN=$(aws --profile default configure get aws_session_token)

docker run -p 8080:8080 --rm \
   -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
   -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
   -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
   -v $(pwd)Source/Python/app/local_test/test_dir:/opt/ml \
   classifierservice train
```

##### Serve
```bash
AWS_ACCESS_KEY_ID=$(aws --profile default configure get aws_access_key_id)
AWS_SECRET_ACCESS_KEY=$(aws --profile default configure get aws_secret_access_key)
AWS_SESSION_TOKEN=$(aws --profile default configure get aws_session_token)

docker run -p 8080:8080 --rm \
   -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
   -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
   -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
   -v $(pwd)Source/Python/app/local_test/test_dir:/opt/ml \
   classifierservice serve
```

### How to invoke 
```bash
curl --data-binary "@<path to csv>" -H "Content-Type: text/csv" http://localhost:8080/invocations
```
### How to run unit tests
#### Step 1: Install dependencies
```bash
pip install -r requirements.txt
```
#### Step 2: Run the test script
```bash
./run_tests
...
...
----------------------------------------------------------------------
Ran 21 tests in 0.175s

OK
Name                             Stmts   Miss Branch BrPart  Cover   Missing
----------------------------------------------------------------------------
Source/Python/app/models.py         23      0      0      0   100%
Source/Python/app/predictor.py      44      0      6      0   100%
Source/Python/app/utilities.py      50      0     12      0   100%
Source/Python/app/wsgi.py            1      0      0      0   100%
----------------------------------------------------------------------------
TOTAL                              118      0     18      0   100%
```
### Helper (optional) scripts
#### Source/Python/app/local_test/train_local.sh
Initiates training against csv located under Source/Python/app/local_test/test_dir/input/data/train/
#### Source/Python/app/local_test/serve_local.sh
Small wrapper for standing up inference web server.