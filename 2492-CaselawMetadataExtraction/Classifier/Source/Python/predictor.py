#!/usr/bin/env python3.6

import os
import json
import numpy
import flask
import pandas as pd
import logging

from io import BytesIO, StringIO
from sklearn.externals import joblib
from models import features


prefix = "/opt/ml/"
model_path = os.path.join(prefix, "model")


class ScoringService(object):
    model = None

    @classmethod
    def get_model(cls):
        if cls.model == None:
            with open(os.path.join(model_path, "trained_model.pkl"), "rb") as in_f:
                cls.model = joblib.load(in_f)
        return cls.model

    @classmethod
    def predict(cls, input_):
        model = cls.get_model()
        ret = model.predict(input_)
        return ret


app = flask.Flask(__name__)


@app.route("/ping", methods=["GET"])
def ping():
    health = ScoringService.get_model() is not None

    status = 200 if health else 404
    return flask.Response(response="\n", status=status, mimetype="application/json")


@app.route("/invocations", methods=["POST"])
def transformation():
    data = None
    if flask.request.content_type != "text/csv":
        return flask.Response(
            response="This predictor only supports CSV data", status=415, mimetype="text/plain"
        )

    data = BytesIO(flask.request.data)
    data.seek(0)
    try:
        df = pd.read_csv(data)
    except Exception:
        message = "Error reading input data."
        logging.exception(message)
        return flask.Response(response=message, status=400, mimetype="application/json")

    try:
        predictions = ScoringService.predict(df)
        df["prediction"] = predictions
        f_out = StringIO()
        df.to_csv(f_out)
        f_out.seek(0)
        return flask.Response(response=f_out, status=200, mimetype="text/csv")
    except Exception:
        message = "Error forming prediction on supplied data."
        logging.exception(message)
        return flask.Response(response=message, status=400, mimetype="application/json")
