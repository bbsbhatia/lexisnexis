#!/usr/bin/env python3.6

import numpy as np
import pandas as pd
import traceback


from collections import defaultdict
from hyperopt import hp, fmin, tpe, Trials, space_eval
from functools import partial
from numbers import Number
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import cross_val_score


class HyperoptModel(BaseEstimator, TransformerMixin):
    def __init__(self, estimator, fit_params, hp_space, cv, max_evals, scoring):
        self.fit_params = fit_params
        self.estimator = estimator
        self.hp_space = hp_space
        self.max_evals = max_evals
        self.cv = cv
        self.scoring = scoring
        self.trials = Trials()
        self.best_params_ = None
        self.objective_round = 0

    def transform(self, x):
        return x

    def predict(self, x):
        return self.estimator.predict(x)

    @property
    def param_grid(self):
        merged = defaultdict(list)
        for trial in self.trials.trials:
            for k, v in trial["misc"]["vals"].items():
                merged[k].extend(v)
        [merged[k].sort() for k in merged]
        return merged

    def fit(self, x, y):
        min_obj = partial(self.objective, x=x, y=y)
        best = fmin(
            min_obj, self.hp_space, algo=tpe.suggest, max_evals=self.max_evals, trials=self.trials
        )
        self.best_params_ = space_eval(self.hp_space, best)
        print("best params: ", self.best_params_)
        print("trials meta: ", self.trials.trials)
        self.estimator.set_params(**self.best_params_)
        print("fitting estimator....")
        self.estimator.fit(x, y, **self.fit_params)
        print("fit!")

    def objective(self, params, x, y):
        print(f"Starting objective round #{self.objective_round}...")
        print("params: ", params)
        self.estimator.set_params(**params)
        try:
            score = cross_val_score(
                self.estimator,
                x,
                y,
                fit_params=self.fit_params,
                cv=self.cv,
                scoring=self.scoring,
                n_jobs=-1,
                verbose=True,
            )
        except Exception:
            print(traceback.format_exc())
            score = np.array([0])
        print(f"Objective round #{self.objective_round} complete.")
        self.objective_round += 1
        return 1 - score.mean()


class ColumnSelector(BaseEstimator, TransformerMixin):
    def __init__(self, columns):
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        return x[self.columns]


class FillNATransformer(BaseEstimator, TransformerMixin):
    def __init__(self, value=None, method=None, axis=None, limit=None, downcast=None, columns=None):
        self.value = value
        self.method = method
        self.axis = axis
        self.limit = limit
        self.downcast = downcast
        self.columns = columns

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        if self.columns is not None:
            x = x[self.columns]

        return x.fillna(
            value=self.value,
            method=self.method,
            axis=self.axis,
            limit=self.limit,
            downcast=self.downcast,
        )


class DataFrameRowTextJoiner(BaseEstimator, TransformerMixin):
    def __init__(self, delimiter=" "):
        self.delimiter = delimiter

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        return pd.Series(x.values.tolist()).str.join(self.delimiter).values.tolist()


class ReportScore(Number):
    def __init__(self, report):
        self.report = report
        super().__init__()


def class_report_score(estimator, x, y):
    predictions = estimator.predict(x)
    class_report = classification_report(y, predictions)
    return ReportScore(class_report)


def confusion_matrix_score(estimator, x, y):
    predictions = estimator.predict(x)
    labels = sorted(y.unique().tolist())
    cm = confusion_matrix(y, predictions, labels=labels)
    max_len = len(max([str(j) for i in cm for j in i], key=lambda x: len(x)))
    rows = ["\t".join(str(j).ljust(max_len) for j in i) for i in cm]
    report = "Class map:\n\n\t"
    report += "\n\t".join([str(i) + " -> " + str(x) for i, x in enumerate(labels)])
    report += "\n\nConfusion matrix:\n"
    report += "\n\tPredicted\t" + "\t".join([str(x).ljust(max_len) for x in range(len(labels))])
    report += "\nTrue\t" + "-" * 60
    for label, row in zip(range(len(labels)), rows):
        report += f"\n{label}\t|\t\t{row}"
    return ReportScore(report)
