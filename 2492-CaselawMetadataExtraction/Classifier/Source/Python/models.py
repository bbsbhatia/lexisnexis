from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn import preprocessing
from sklearn.base import clone
from xgboost import XGBClassifier
from sklearn.model_selection import StratifiedKFold
from utilities import ColumnSelector, DataFrameRowTextJoiner, FillNATransformer, HyperoptModel
from hyperopt import hp, fmin, tpe, Trials, space_eval
from imblearn.over_sampling import SMOTE
from imblearn.pipeline import Pipeline as IMBPipeline


_text_features = [
    "words-normalized",
    "words-cleaned",
    "words-cleaned-entities-full",
    "words-cleaned-entities",
    "words-cleaned-entities-full-expanded",
    "words-cleaned-entities-expanded",
    "tokens",
    "pos-types",
    "pos-normalized-tags",
    "pos-normalized",
    "pos-cleaned",
    "entities-types",
    "entities-normalized-tags",
    "entities-normalized",
    "entities-cleaned",
]

_numeric_features = [
    "font-size",
    "page",
    "page-relative",
    "x-relative",
    "x-relative-scale",
    "x-log",
    "x-exact-scale",
    "y-relative",
    "y-relative-scale",
    "y-log",
    "y-exact-scale",
    "h-relative",
    "h-relative-scale",
    "h-log",
    "h-exact-scale",
    "w-relative",
    "w-relative-scale",
    "w-log",
    "w-exact-scale",
    "words-count",
    "words-length",
    "words-average-length",
    "words-percent-characters",
    "words-percent-digits",
    "words-percent-punctuation",
    "entities-count",
    "entities-percentage",
]

# TODO separate out vectorizers for each text column...not sure why they were originally joining these. Seems like it
# would create a weird tfidf space...
_text_transform = Pipeline(
    [
        ("column_selector", ColumnSelector(columns=_text_features)),
        ("fill_na_values", FillNATransformer(value="None")),
        ("text_joiner", DataFrameRowTextJoiner()),
        ("tfidf", TfidfVectorizer()),
    ]
)

_non_text_transform = Pipeline([("column_selector", ColumnSelector(columns=_numeric_features))])

_feature_transformer = Pipeline(
    [
        (
            "union",
            FeatureUnion(
                transformer_list=[("text", _text_transform), ("non_text", _non_text_transform)]
            ),
        ),
        ("scalar_transform", preprocessing.StandardScaler(with_mean=False)),
    ]
)

_base_pipeline = Pipeline(
    [("features", _feature_transformer), ("selection", SelectKBest(score_func=chi2, k=3000))]
)

################################################################################
# XGBoost
################################################################################

hp_space = {}
hp_space["features__union__text__tfidf__sublinear_tf"] = hp.choice("sublinear_tf", [True, False])
hp_space["features__union__text__tfidf__min_df"] = 1 + hp.randint("min_df", 3)
hp_space["features__union__text__tfidf__max_df"] = 1 + hp.uniform("max_df", 0.7, 1.0)
hp_space["features__union__text__tfidf__ngram_range"] = hp.choice(
    "ngram_range", [(1, 1), (2, 2), (3, 3), (1, 2), (2, 3), (1, 3)]
)
hp_space["selection__k"] = hp.choice("k", [10, 100, 250, 500, 1000, 2000, 3000, "all"])
hp_space["classifier__max_depth"] = hp.choice("max_depth", [6, 10, 20, 50, 100])
hp_space["classifier__n_estimators"] = 10 + hp.randint("classifier__n_estimators", 200)
hp_space["classifier__min_child_weight"] = 2 + hp.randint("min_child_weight", 20)
hp_space["classifier__learning_rate"] = hp.quniform("learning_rate", 0.025, 0.5, 0.025)
hp_space["classifier__min_split_loss"] = hp.quniform("min_split_loss", 0.5, 1, 0.05)
hp_space["classifier__subsample"] = hp.quniform("subsample", 0.5, 1, 0.05)
hp_space["classifier__colsample_bytree"] = hp.quniform("colsample_bytree", 0.5, 1, 0.05)
hp_space["classifier__scale_pos_weight"] = hp.quniform("scale_pos_weight", 0.6, 1.5, 0.05)

xgb_pipeline = clone(_base_pipeline)
xgb_pipeline.steps.append(["classifier", XGBClassifier()])

model = HyperoptModel(
    estimator=xgb_pipeline,
    fit_params={},
    hp_space=hp_space,
    cv=StratifiedKFold(n_splits=5),
    max_evals=20,
    scoring="f1_macro",
)

target = "class-type"
features = _text_features + _numeric_features
models = [("xgb", model)]
