import unittest
import os
import sys
import inspect

from models import models, hp_space
from collections import Iterable


class ModelTests(unittest.TestCase):
    def test_models_type(self):
        self.assertTrue(isinstance(models, Iterable))

    def test_hyperparams_set(self):
        self.assertTrue(len(hp_space) > 0)


if __name__ == "__main__":
    unittest.main()
