import unittest
import pandas as pd
import numpy as np

from unittest.mock import Mock
from hyperopt import hp
from contextlib import contextmanager
from sklearn.model_selection import StratifiedKFold
from sklearn.svm import SVC
from utilities import (
    ColumnSelector,
    FillNATransformer,
    DataFrameRowTextJoiner,
    class_report_score,
    ReportScore,
    confusion_matrix_score,
    HyperoptModel,
)


class UtilityTests(unittest.TestCase):
    def test_column_selector_fit(self):
        transformer = ColumnSelector("column0")
        self.assertTrue(transformer.fit(0, 0) is transformer)

    def test_column_selector_transform(self):
        transformer = ColumnSelector("column0")
        x = pd.DataFrame([[0], [1]], columns=["column0"])
        self.assertTrue(transformer.transform(x) is x["column0"])

    def test_fill_na_transformer_fit(self):
        transformer = FillNATransformer()
        self.assertTrue(transformer.fit(0, 0) is transformer)

    def test_fill_na_transformer_transform(self):
        transformer = FillNATransformer(value=0)
        x = pd.DataFrame([[None]], columns=["column0"])
        self.assertEquals(transformer.transform(x).iloc[0, 0], 0)

    def test_fill_na_transformer_transform_with_columns(self):
        transformer = FillNATransformer(value=0, columns="column0")
        x = pd.DataFrame([[None]], columns=["column0"])
        self.assertEquals(transformer.transform(x).iloc[0], 0)

    def test_df_row_text_joiner_fit(self):
        transformer = DataFrameRowTextJoiner()
        self.assertTrue(transformer.fit(0, 0) is transformer)

    def test_df_row_text_joiner_transformer(self):
        transformer = DataFrameRowTextJoiner()
        x = pd.DataFrame([["Hello"], ["World"]], columns=["column0"])
        self.assertEquals(transformer.transform(x), ["Hello", "World"])

    def test_class_report_score(self):
        x = [5, 6, 7, 8]
        y = [1, 2, 3, 4]
        estimator = Mock()
        estimator.predict.return_value = y
        self.assertTrue(isinstance(class_report_score(estimator, x, y), ReportScore))

    def test_confusion_matrix_score(self):
        x = pd.Series([5, 6, 7, 8])
        y = pd.Series([1, 2, 3, 4])
        estimator = Mock()
        estimator.predict.return_value = y
        self.assertTrue(isinstance(confusion_matrix_score(estimator, x, y), ReportScore))

    def test_hyper_opt_model_transform(self):
        model = HyperoptModel(
            estimator=Mock(), fit_params={}, hp_space={}, cv=Mock(), max_evals=1, scoring="f1_macro"
        )
        self.assertTrue(model.transform(True) == True)

    def test_hyper_opt_model_fit(self):

        model = HyperoptModel(
            estimator=SVC(),
            fit_params={},
            hp_space={"gamma": hp.choice("gamma", [.1])},
            cv=StratifiedKFold(),
            max_evals=1,
            scoring="f1_macro",
        )
        model.fit(np.asarray(range(1000)).reshape(-1, 1), np.asarray([i for i in range(2) for _ in range(500)]))
        self.assertTrue(len(model.param_grid) >= 1)

    def test_hyper_opt_model_param_grid(self):
        model = HyperoptModel(
            estimator=Mock(), fit_params={}, hp_space={}, cv=Mock(), max_evals=1, scoring="f1_macro"
        )
        model.trials._trials = [{"misc": {"vals": {"x": [5, 4, 2, 1, 3]}}}]
        self.assertTrue(model.param_grid["x"] == [1, 2, 3, 4, 5])


if __name__ == "__main__":
    unittest.main()
