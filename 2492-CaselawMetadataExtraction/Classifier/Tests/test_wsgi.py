import unittest
import flask

from wsgi import app


class WSGITests(unittest.TestCase):
    def test_app(self):
        self.assertTrue(isinstance(app, flask.app.Flask))


if __name__ == "__main__":
    unittest.main()
