import unittest
import pandas as pd
from unittest.mock import patch, Mock
from predictor import ScoringService, ping, transformation
from models import features


class PredictorTests(unittest.TestCase):
    def setUp(self):
        ScoringService.model = None

    @patch("predictor.open")
    @patch("predictor.joblib")
    def test_scoring_service_get_model(self, joblib, open):
        joblib.load.return_value = "test"
        open.return_value.__enter__.return_value = Mock()
        service = ScoringService()
        self.assertTrue(service.get_model() == "test")

    def test_scoring_service_get_model_already_set(self):
        service = ScoringService()
        ScoringService.model = "test"
        self.assertTrue(service.get_model() == "test")

    def test_scoring_service_predict(self):
        service = ScoringService()
        model = Mock()
        model.predict.return_value = "test"
        ScoringService.model = model
        self.assertTrue(service.predict("test") == "test")

    def test_ping_healthy(self):
        ScoringService.model = Mock()
        self.assertTrue(ping().status_code == 200)

    @patch("predictor.ScoringService.get_model")
    def test_ping_unhealthy(self, get_model):
        get_model.return_value = None
        self.assertTrue(ping().status_code == 404)

    @patch("predictor.flask.request")
    def test_transformation_bad_mimetype(self, request):
        request.content_type = "application/json"
        self.assertTrue(transformation().status_code == 415)

    @patch("predictor.ScoringService.predict")
    @patch("predictor.flask.request")
    def test_transformation_success(self, request, predict):
        predict.return_value = ["case_name"]
        request.content_type = "text/csv"
        request.data = ",".join(features).encode("utf-8")
        self.assertTrue(transformation().status_code == 200)

    @patch("predictor.ScoringService.predict")
    @patch("predictor.flask.request")
    def test_transformation_header_correction_success(self, request, predict):
        predict.return_value = ["case_name"]
        request.content_type = "text/csv"
        request.data = ",".join(sorted(features)).encode("utf-8")
        self.assertTrue(transformation().status_code == 200)

    @patch("predictor.pd.read_csv")
    @patch("predictor.flask.request")
    def test_transformation_bad_shape(self, request, read_csv):
        read_csv.side_effect = Exception()
        request.content_type = "text/csv"
        request.data = ",".join(features).encode("utf-8")
        self.assertTrue(transformation().status_code == 400)

    @patch("predictor.ScoringService.predict")
    @patch("predictor.flask.request")
    def test_transformation_prediction_error(self, request, predict):
        predict.side_effect = Exception()
        request.content_type = "text/csv"
        request.data = ",".join(features).encode("utf-8")
        self.assertTrue(transformation().status_code == 400)


if __name__ == "__main__":
    unittest.main()
