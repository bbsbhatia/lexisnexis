import boto3
import json
import logging
import os
import re
import time
import sqlalchemy


from datetime import datetime
from typing import Any

logger = logging.getLogger()
if len(logger.handlers) > 0:
    for log_handler in logger.handlers:
        logger.removeHandler(log_handler)
logger.setLevel(logging.INFO)

engine = None


def build_response(
    http_code: int, body: Any, headers: dict = None, base64_enc: bool = False
) -> dict:
    """builds response
    
    Builds a JSON response object to return to the requester (over HTTP via API Gateway).

    Args:
        (int) http_code: HTTP status code (200, 300, 400, 500, etc).
        (Any) body: Value to supply as response content.
        (dict) headers: Mapping of header keys to header values.
        (bool) base64_enc: Indicates whether or not body is a base 64 encoded string. 
    
    Returns:
        (dict) Response map.
    """
    return {
        "isBase64Encoded": base64_enc,
        "statusCode": http_code,
        "headers": headers,
        "body": body,
    }


def get_secret(secret_id: str, version_id: str = None, version_stage: str = None) -> dict:
    """Get DB creds
    
    Retrieves DB credentials from AWS secrets manager.

    Args:
        (str) secret_id: id of secret to retrieve
        (str) version_id: version id of secret to retrieve
        (str) version_stage: version stage of secret to retrieve

    Returns: 
        (dict) mapping of secret keys to secret values.
    """
    client = boto3.client("secretsmanager")
    params = {"SecretId": secret_id}
    if version_id:
        params["VersionId"] = version_id
    if version_stage:
        params["VersionStage"] = version_stage
    response = client.get_secret_value(**params)
    return json.loads(response["SecretString"])


def stage_document_to_table(
    engine: sqlalchemy.engine.base.Engine, doc_id: str, s3_uri: str, expires: int
) -> None:
    """Writes a record for a staged document 

    Writes a db record for a document staged for the caselaw metadata extraction process.

    Args:
        (sqlalchemy.engine.base.Engine) engine: SQLAlchemy engine object.
        (str) doc_id: Document identifier.
        (str) s3_uri: S3 URI where document will be written to.
        (int) expires: seconds till expiry time.

    Returns:
        None
    """

    sql = sqlalchemy.text(
        """
        INSERT INTO caselaw_metadata_extraction.document_tracking 
            (id, original_s3_uri, upload_staged_timestamp, ttl) 
        VALUES
            (:id, :original_s3_uri, NOW(), :ttl)
        """
    )
    conn = engine.connect()
    result = conn.execute(sql, id=doc_id, original_s3_uri=s3_uri, ttl=expires)
    conn.close()

    return result


def check_if_document_exists(engine: sqlalchemy.engine.base.Engine, doc_id: str) -> bool:
    """Checks if document exists 

    Checks database to see if we already have started tracking this document id.

    Args:
        (sqlalchemy.engine.base.Engine) engine: SQLAlchemy engine object.
        (str) doc_id: Document identifier.

    Returns:
        (bool) Boolean value indicating whether or not we have the document in our database
    """

    sql = sqlalchemy.text(
        """
        select count(*) from caselaw_metadata_extraction.document_tracking where id = :id
        """
    )
    conn = engine.connect()
    result = conn.execute(sql, id=doc_id).scalar()
    conn.close()

    return True if result == 1 else False


def handler(event: dict, context: dict) -> dict:
    """Request handler
    
    Handles document staging requests proxied from API gateway and orchestrates expirable
    presigned url generation and expirable dynamodb record creation.

    Args:
        (dict) event: Event object, contains HTTP request metadata & content.
        (dict) context: Runtime context metadata.

    Returns:
        (dict) Response object, adhering to API Gateway proxy integration output format.
    """
    global engine
    logger.info(event)
    expires_in = None
    query_params = event.get("queryStringParameters")
    if not query_params or not query_params.get("document_id"):
        message = "Missing query string. Required keys: document_id"
        return build_response(400, json.dumps(message))
    if query_params.get("expires_in") is not None:
        expires_in = int(query_params.get("expires_in"))

    s3 = boto3.client("s3")
    if not engine:
        creds = get_secret(os.environ["RDS_SECRET_ARN"])
        conn_str = "mysql+pymysql://{username}:{password}@{host}:{port}".format(**creds)
        engine = sqlalchemy.create_engine(conn_str)

    doc_id = query_params.get("document_id")
    doc_exists = check_if_document_exists(engine, doc_id)

    if doc_exists:
        message = "Document already exists."
        return build_response(409, json.dumps(message))

    filename = f"{doc_id}.pdf"
    kwargs = {
        "ClientMethod": "put_object",
        "Params": {"Bucket": os.environ["S3_BUCKET"], "Key": filename},
    }

    if expires_in is not None:
        kwargs["ExpiresIn"] = expires_in

    try:
        presigned_url = s3.generate_presigned_url(**kwargs)
    except Exception:
        message = f"Failed to generate presigned url with params: {kwargs}"
        logger.exception(message)
        return build_response(500, json.dumps(message))

    try:
        expires = int(re.search(r"&Expires=([\d]+)", presigned_url).group(1))
        stage_document_to_table(
            engine,
            doc_id,
            f"s3://{os.environ['S3_BUCKET']}/{filename}",
            datetime.utcfromtimestamp(expires),
        )
    except Exception:
        message = f"Failed to create document record for doc id: {doc_id}"
        logger.exception(message)
        return build_response(500, json.dumps(message))

    response = build_response(
        200, json.dumps({"document_id": doc_id, "presigned_url": presigned_url})
    )
    return response
