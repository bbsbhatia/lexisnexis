@Library('DevOpsShared@v1') _
pipeline {
    agent any
    parameters {
        string(name: 'releaseUnitBuild', description: 'The build number to deploy')
        string(name: 'assetGroup', description: 'The asset group to deploy to', defaultValue: 'ddc1')
        string(name: 'targetAccount', description: 'The AWS account number to deploy to', defaultValue: PRODUCT_CONTENT_DEV)
    }
    environment {
        def ASSET_ID = "2492"
        def ASSET_NAME = "CaseLawMetadataExtraction"
        def PROJECT_PATH = "GetDocumentStagingData"
        def ASSET_AREA_NAME = "${ASSET_NAME}/${PROJECT_PATH}"
        def TARGET_ACCOUNT = "${params.targetAccount}"
        def ASSET_GROUP = "${params.assetGroup ?: commons.getBranchName()}"
        def RELEASE_UNIT = [commons.getBranchName(), params.releaseUnitBuild].join('/')        
    }
    stages {
        stage('Stage Release Unit') {
            steps{
                stageReleaseUnit()
            }
        }
        stage('Deploy Lambda') {
            steps {
                cloudformation(
                        command: 'createChangeSet',
                        account: TARGET_ACCOUNT,
                        templateName: 'GetDocumentStagingData',
                        stackParameters: ['LambdaSourceS3Bucket'  : STAGING_BUCKET,
                                          'LambdaSourceS3Key': [ASSET_ID,ASSET_NAME,ASSET_AREA_NAME,RELEASE_UNIT,"${env.ASSET_AREA_NAME.replace("/","-")}.zip"].join('/'),
                                          'Build'   : params.releaseUnitBuild,
                                          'AssetGroup': ASSET_GROUP]
                )
                cloudformation(
                        command: 'executeChangeSet',
                        account: TARGET_ACCOUNT,
                        templateName: 'GetDocumentStagingData'
                )
            }
        }
    }
}