## GetDocumentStagingData lambda

### Description
Handles staging for proposed document upload by allocating an expirable, presigned s3 url and dynamodb tracking record.

### Dependencies
#### Python libraries
```bash
pip install -r requirements.txt
```
### How to run unit tests
#### Step 1: Install dependencies (see above)
#### Step 2: Run the test script
```bash
./run_tests
...
...
----------------------------------------------------------------------
Ran 7 tests in 0.035s

OK
Name                     Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------------
Source/Lambda/index.py      34      0      8      0   100%
```