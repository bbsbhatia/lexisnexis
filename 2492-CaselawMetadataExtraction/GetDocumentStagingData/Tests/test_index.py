import unittest

from unittest.mock import patch, Mock
from index import (
    build_response,
    handler,
    get_secret,
    stage_document_to_table,
    check_if_document_exists,
)


class GetDocumentStagingDataTests(unittest.TestCase):
    @patch("index.boto3")
    def test_get_secret(self, boto3):
        client = Mock()
        client.get_secret_value.return_value = {"SecretString": '{"key": "val"}'}
        boto3.client.return_value = client
        self.assertTrue(isinstance(get_secret("test", "1", "1"), dict))

    def test_build_response(self):
        self.assertEquals(
            build_response(200, "{}", {}, True),
            {"isBase64Encoded": True, "statusCode": 200, "headers": {}, "body": "{}"},
        )

    @patch("index.sqlalchemy")
    def test_stage_document_to_table(self, sqlalchemy):
        sqlalchemy.text.return_value = str()
        result_proxy = Mock()
        result_proxy.rowcount = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        self.assertEqual(stage_document_to_table(engine, 1, "test", 1).rowcount, 1)

    @patch("index.sqlalchemy")
    def test_check_if_document_exists(self, sqlalchemy):
        sqlalchemy.text.return_value = str()
        result_proxy = Mock()
        result_proxy.scalar.return_value = 1
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        self.assertTrue(check_if_document_exists(engine, 1))

    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.check_if_document_exists")
    @patch("index.stage_document_to_table")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test", "S3_BUCKET": "test"})
    def test_handler_success(
        self,
        stage_document_to_table,
        check_if_document_exists,
        get_secret,
        sqlalchemy,
        client,
        resource,
    ):
        stage_document_to_table.return_value = None
        check_if_document_exists.return_value = False
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        sqlalchemy.create_engine.return_value = Mock()
        client.return_value.generate_presigned_url.return_value = "&Expires=10"
        self.assertEquals(
            handler({"queryStringParameters": {"document_id": "test"}}, {})["statusCode"], 200
        )

    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.check_if_document_exists")
    @patch("index.stage_document_to_table")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test", "S3_BUCKET": "test"})
    def test_handler_expires_in_success(
        self,
        stage_document_to_table,
        check_if_document_exists,
        get_secret,
        sqlalchemy,
        client,
        resource,
    ):
        stage_document_to_table.return_value = None
        check_if_document_exists.return_value = False
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        sqlalchemy.create_engine.return_value = Mock()
        client.return_value.generate_presigned_url.return_value = "&Expires=10"
        self.assertEquals(
            handler({"queryStringParameters": {"document_id": "test", "expires_in": 10}}, {})[
                "statusCode"
            ],
            200,
        )

    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.check_if_document_exists")
    @patch("index.stage_document_to_table")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test", "S3_BUCKET": "test"})
    def test_handler_md5_exists(
        self,
        stage_document_to_table,
        check_if_document_exists,
        get_secret,
        sqlalchemy,
        client,
        resource,
    ):
        stage_document_to_table.return_value = None
        check_if_document_exists.return_value = True
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        sqlalchemy.create_engine.return_value = Mock()
        self.assertEquals(
            handler({"queryStringParameters": {"document_id": "test"}}, {})["statusCode"], 409
        )

    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.check_if_document_exists")
    @patch("index.stage_document_to_table")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test", "S3_BUCKET": "test"})
    def test_handler_s3_presigned_url_error(
        self,
        stage_document_to_table,
        check_if_document_exists,
        get_secret,
        sqlalchemy,
        client,
        resource,
    ):
        stage_document_to_table.return_value = None
        check_if_document_exists.return_value = False
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        sqlalchemy.create_engine.return_value = Mock()
        client.return_value.generate_presigned_url.side_effect = Exception()
        self.assertEquals(
            handler({"queryStringParameters": {"document_id": "test"}}, {})["statusCode"], 500
        )

    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch("index.sqlalchemy")
    @patch("index.get_secret")
    @patch("index.check_if_document_exists")
    @patch("index.stage_document_to_table")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test", "S3_BUCKET": "test"})
    def test_handler_dynamodb_put_item_error(
        self,
        stage_document_to_table,
        check_if_document_exists,
        get_secret,
        sqlalchemy,
        client,
        resource,
    ):
        stage_document_to_table.side_effect = Exception()
        check_if_document_exists.return_value = False
        get_secret.return_value = {
            "username": "test",
            "password": "test",
            "host": "test",
            "port": "test",
        }
        sqlalchemy.create_engine.return_value = Mock()
        client.return_value.generate_presigned_url.return_value = "&Expires=10"
        self.assertEquals(
            handler({"queryStringParameters": {"document_id": "test"}}, {})["statusCode"], 500
        )

    @patch("index.boto3.resource")
    @patch("index.boto3.client")
    @patch.dict("index.os.environ", {"RDS_SECRET_ARN": "test", "S3_BUCKET": "test"})
    def test_handler_missing_params(self, client, resource):
        self.assertEquals(handler({"queryStringParameters": {}}, {})["statusCode"], 400)


if __name__ == "__main__":
    unittest.main()
