## PostbackTransform Task

### Description
Provides a containerized task that transforms posted back values into training data files.

### Dependencies
#### Docker
See https://docs.docker.com/install/ for platform specific installation instructions, or use the following to install for Ubuntu:
```bash
#Add docker's official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#Set up stable docker repository.
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

#update package index now that we've added the docker repo
sudo apt-get -y update

#install docker community edition
sudo apt-get -y install docker-ce

#create docker group and config permissions
sudo groupadd docker
sudo usermod -aG docker $USER
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "/home/$USER/.docker" -R
```
### How to build application
```bash
docker build -f DevOps/Docker/Dockerfile -t postbacktransform .
```
### How to run application
```bash
AWS_ACCESS_KEY_ID=$(aws --profile default configure get aws_access_key_id)
AWS_SECRET_ACCESS_KEY=$(aws --profile default configure get aws_secret_access_key)
AWS_SESSION_TOKEN=$(aws --profile default configure get aws_session_token)

docker run --rm \
   -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
   -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
   -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
   postbacktransform
```
### How to run unit tests
#### Step 1: Install dependencies
```bash
pip install -r requirements.txt
```
#### Step 2: Run the test script
```bash
./run_tests
...
...
----------------------------------------------------------------------
Ran 16 tests in 0.198s

OK
Name                   Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------------
Source/Python/app.py     153      7     82      7    94%   229, 291-296, 303-305, 200->204, 204->212, 212->219, 226->229, 260->exit, 263->267, 290->291
```
### Helper (optional) scripts
#### bootstrap.sh
Installs docker for an Ubuntu OS
#### build_image.sh
Small wrapper around docker build command setup
#### run_container.sh
Small wrapper for docker run command setup