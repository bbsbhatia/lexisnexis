dockerfile=${1:-DevOps/Docker/Dockerfile}
image=${2:-postbacktransform}

docker build -f $dockerfile -t $image .
