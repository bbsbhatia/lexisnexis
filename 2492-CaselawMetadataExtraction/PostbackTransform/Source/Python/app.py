import boto3
import concurrent.futures
import json
import logging
import os
import pandas as pd
import re
import sqlalchemy as sa
import numpy as np
import math
import os
import sys

from fuzzywuzzy import fuzz
from datetime import datetime
from collections import Counter
from lng_date_mentions_extractor.extractor import DateMentionsExtractor

logger = logging.getLogger()
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

def get_secret(secret_id: str, version_id: str = None, version_stage: str = None) -> dict:
    client = boto3.Session().client("secretsmanager")
    params = {"SecretId": secret_id}
    if version_id:
        params["VersionId"] = version_id
    if version_stage:
        params["VersionStage"] = version_stage
    response = client.get_secret_value(**params)
    return json.loads(response["SecretString"])


def set_training_set_status(
    engine: sa.engine.base.Engine, id_: str, status: int, s3_uri: str = None
) -> sa.engine.ResultProxy:
    sql = sa.text(
        """
        UPDATE
            caselaw_metadata_extraction.document_tracking
        SET
            training_set_status = :status,
            confirmed_csv_s3_uri = :s3_uri
        WHERE
            id = :id
        """
    )
    conn = engine.connect()
    result = conn.execute(sql, id=id_, status=status, s3_uri=s3_uri)
    conn.close()
    return result


def get_documents(engine: sa.engine.base.Engine) -> sa.engine.ResultProxy:
    sql = sa.text(
        """
            SELECT 
                id,
                feature_csv_s3_uri,
                postback_timestamp,
                CASE
                    WHEN case_name_confirmed IS NULL THEN 1
                    ELSE 0
                END AS case_name_match,
                CASE
                    WHEN docket_number_confirmed IS NULL THEN 1
                    ELSE 0
                END AS docket_number_match,
                CASE
                    WHEN decided_date_confirmed IS NULL THEN 1
                    ELSE 0
                END AS decided_date_match,
                CASE
                    WHEN filed_date_confirmed IS NULL THEN 1
                    ELSE 0
                END AS filed_date_match,
                case_name_confirmed,
                docket_number_confirmed,
                decided_date_confirmed,
                filed_date_confirmed
            FROM
                caselaw_metadata_extraction.document_tracking
            WHERE
                postback_timestamp IS NOT NULL
                    AND training_set_status IS NULL
                    AND ((case_name_normalized != case_name_confirmed
                    AND case_name_confirmed IS NOT NULL)
                    OR (docket_number_normalized != docket_number_confirmed
                    AND docket_number_confirmed IS NOT NULL)
                    OR (decided_date_normalized != decided_date_confirmed
                    AND decided_date_confirmed IS NOT NULL)
                    OR (filed_date_normalized != filed_date_confirmed
                    AND filed_date_confirmed IS NOT NULL));
        """
    )
    conn = engine.connect()
    result = conn.execute(sql).fetchall()
    conn.close()
    return result


def find_case_name_indices(correct_case_name, document_frame):
    top_scores = {}
    split_mods = [2, 2.5, 3, 3.5]
    for split_mod in split_mods:
        row_text_indices = {}
        joined_text = ""
        curr_text_idx = 0

        df_sort = document_frame.copy()
        df_sort = df_sort[
            df_sort["x-exact"]
            <= (
                (df_sort["w-exact"] / df_sort["w-relative"])
                - (split_mod * df_sort["x-exact"].min())
            )
            / 2
        ]

        df_sort["original_index"] = df_sort.index
        df_sort = df_sort.sort_values(["page", "y-exact"], ascending=[True, True]).reset_index()

        for idx, row in df_sort.iterrows():
            joined_text += str(row["words-normalized"]) + " "

            row_text_indices[curr_text_idx] = idx
            curr_text_idx = len(joined_text) - 1

        ngrams = []
        target_word_count = len(correct_case_name.split())
        for ngram_size in range(target_word_count - 3, target_word_count + 3):
            ngrams.extend(get_ngrams(joined_text.split(), ngram_size))

        scores = {}
        for ngram in ngrams:
            similarity = fuzz.ratio(" ".join(ngram), correct_case_name)
            scores[ngram] = similarity

        max_ = max(scores.items(), key=lambda x: x[1])
        match_text = " ".join(max_[0])
        match_start_index = joined_text.index(match_text)
        match_end_index = match_start_index + len(match_text)

        closest_start_row = find_closest_element(
            match_start_index, row_text_indices, method="floor"
        )
        closest_end_row = find_closest_element(match_end_index, row_text_indices, method="ceiling")

        original_indices = []
        slice_ = slice(
            row_text_indices.get(closest_start_row), row_text_indices.get(closest_end_row)
        )
        for idx, row in df_sort.iloc[slice_].iterrows():
            original_indices.append(row["original_index"])

        top_scores[max_[1]] = original_indices
    max_max = max(top_scores.items(), key=lambda x: x[0])
    return max_max


def find_docket_number_indices(correct_docket_number, document_frame):
    scores = {}
    for idx, row in document_frame.iterrows():
        scores[idx] = fuzz.ratio(str(row["words-normalized"]), correct_docket_number)
    best_match = max(scores.items(), key=lambda x: x[1])
    best_matches = [x[0] for x in scores.items() if x[1] == best_match[1]]
    return best_match[1], best_matches


def find_date_indices(correct_date, document_frame):
    scores = {}
    for idx, row in document_frame.iterrows():
        date_mentions = DateMentionsExtractor(str(row["words-normalized"])).extract()
        if len(date_mentions) > 0:
            for date_mention in date_mentions:
                if date_mention.iso_8601_date_str() == correct_date:
                    scores[idx] = fuzz.ratio(date_mention.matched_text, str(row["words-normalized"]))
    best_match = max(scores.items(), key=lambda x: x[1]) if len(scores) else (None, -1)
    best_matches = [x[0] for x in scores.items() if x[1] == best_match[1]]
    return best_match[1], best_matches


def build_corrected_rows(df, label, correct_value, threshold, index_func):
    high_confidence = False
    for idx, _ in df[df.prediction == label].iterrows():
        df.loc[idx, "prediction"] = "UNUSED"
    sim, idxs = index_func(correct_value, df)
    if sim >= threshold:
        for idx in idxs:
            df.loc[idx, "prediction"] = label
        high_confidence = True
    return df, high_confidence


def build_corrected_frame(document, frame):
    relabeled_df = frame.copy()
    high_confidence = True
    if document["case_name_match"] == 0:
        relabeled_df, high_confidence = build_corrected_rows(
            relabeled_df, "Case Name", document["case_name_confirmed"], 90, find_case_name_indices
        )
    if document["docket_number_match"] == 0:
        relabeled_df, high_confidence = build_corrected_rows(
            relabeled_df,
            "Docket Number",
            document["docket_number_confirmed"],
            90,
            find_docket_number_indices,
        )
    if document["decided_date_match"] == 0 or document["filed_date_match"] == 0:
        relabeled_df, high_confidence = build_corrected_rows(
            relabeled_df, "Date", document["decided_date_confirmed"], 80, find_date_indices
        )
        relabeled_df, high_confidence = build_corrected_rows(
            relabeled_df, "Date", document["filed_date_confirmed"], 80, find_date_indices
        )
    return relabeled_df, high_confidence


def find_closest_row(row, frame):
    row_dists = {}
    row_coords = np.asarray([row["y-exact"], row["x-exact"]])
    for idx, other_row in frame.iterrows():
        if other_row["page"] != row["page"] or (
            other_row["y-exact"] > row["y-exact"] or other_row["x-exact"] > row["x-exact"]
        ):
            continue
        other_row_coords = np.asarray([other_row["y-exact"], other_row["x-exact"]])
        dist = np.linalg.norm(row_coords - other_row_coords)
        row_dists[idx] = dist
    non_zero_dists = {k: v for k, v in row_dists.items() if v != 0}
    try:
        closest_row_idx = min(non_zero_dists.items(), key=lambda x: x[1])[0]
    except Exception as e:
        print("found top row: ", str(row["words-normalized"]), e)
        closest_row_idx = None

    return closest_row_idx


def get_ngrams(content, n):
    return list(zip(*[content[i:] for i in range(n)]))


def find_closest_element(elem, lst, method=None):
    # handle some edge cases
    if len(lst) == 0:
        return -1
    elif elem in lst:
        return elem

    # find absolute distances between provided index and list of indices
    elem_diffs = {}
    for lst_elem in lst:
        elem_diffs[lst_elem] = abs(lst_elem - elem)

    # establish the sort of filter we want to use, default is no active filtering
    filter_ = lambda x: True
    if method == "floor":
        filter_ = lambda x: x < elem
    elif method == "ceiling":
        filter_ = lambda x: x > elem

    # yield post-filtered index elem with minimum distance to provided index
    eligible_elems = {k: v for k, v in elem_diffs.items() if filter_(k)}
    if len(eligible_elems) > 0:
        return min(eligible_elems.items(), key=lambda x: x[1])[0]
    else:
        return None


def main():
    logger.info("starting run...")
    creds = get_secret(os.environ["RDS_SECRET_ARN"])
    conn_str = "mysql+pymysql://{username}:{password}@{host}:{port}".format(**creds)
    logger.info("got connection string...")
    engine = sa.create_engine(conn_str)
    s3 = boto3.Session().resource("s3")
    logger.info("geting documents...")
    docs = get_documents(engine)
    logger.info(f"found {len(docs)} documents. Working...")
    for doc in docs:
        try:
            bucket, key = re.search(r"(?:s3:\/\/)?(.*)?\/(.*)", doc["feature_csv_s3_uri"]).groups()
            s3_obj = s3.Object(bucket, key).get()
            frame = pd.read_csv(s3_obj["Body"])
            corrected_frame, high_confidence = build_corrected_frame(doc, frame)
            if high_confidence:
                filename = key.replace(".csv", "").replace(".pdf", "") + "_confirmed.csv"
                new_key = f"CaseLawMetadataExtraction/input/{filename}"
                s3.Bucket(os.environ["ARTIFACTS_BUCKET"]).put_object(
                    Key=new_key, Body=corrected_frame.to_csv(index=False)
                )
                set_training_set_status(
                    engine, doc["id"], 1, f"s3://{os.environ['ARTIFACTS_BUCKET']}/{new_key}"
                )
            else:
                # mark with separate status, i.e. needs human review for training set addition
                set_training_set_status(engine, doc["id"], -1)
            logger.info(f"high confidence for {doc['id']}? {high_confidence}")
        except Exception as e:
            logger.error(f"Error: {e}", exc_info=True)
            set_training_set_status(engine, doc["id"], -1)
    logger.info("Done.")


if __name__ == "__main__":
    main()
