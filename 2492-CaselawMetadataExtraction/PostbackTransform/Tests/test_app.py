import pandas as pd
import unittest
import io

from contextlib import contextmanager
from unittest.mock import patch, Mock
from app import (
    get_secret,
    set_training_set_status,
    get_documents,
    find_case_name_indices,
    find_docket_number_indices,
    find_date_indices,
    build_corrected_frame,
    find_closest_row,
    get_ngrams,
    find_closest_element,
    main,
)


class AppTests(unittest.TestCase):
    @contextmanager
    def assertNotRaises(self, exception):
        try:
            yield None
        except exception:
            raise self.failureException("{0}".format(exception.__name__))

    @patch("app.boto3")
    def test_get_secret(self, boto3):
        client = Mock()
        client.get_secret_value.return_value = {"SecretString": '{"key": "val"}'}
        session = Mock()
        session.client.return_value = client
        boto3.Session.return_value = session
        self.assertTrue(isinstance(get_secret("test", "1", "1"), dict))

    @patch("app.sa")
    def test_set_training_set_status(self, sqlalchemy):
        connection = Mock()
        connection.execute.return_value = {"affected_row_count": 1}
        engine = Mock()
        engine.connect.return_value = connection
        result = set_training_set_status(engine, 1, 1, "s3://testpath")
        self.assertTrue(result == {"affected_row_count": 1})

    def test_get_documents(self):
        result_proxy = Mock()
        result_proxy.fetchall.return_value = [1, 2, 3]
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        result = get_documents(engine)
        self.assertTrue(result == [1, 2, 3])

    def test_find_case_name_indices(self):
        pass

    def test_find_docket_number_indices(self):
        pass

    def test_find_date_indices(self):
        pass

    def test_build_corrected_frame(self):
        df = pd.DataFrame(
            {
                "prediction": ["Case Name", "Pacer Header", "Docket Number", "Date", "Date"],
                "x-exact": [1, 2, 3, 4, 5],
                "w-exact": [10, 2, 3, 4, 5],
                "w-relative": [1, 2, 3, 4, 5],
                "page": [1, 2, 3, 4, 5],
                "y-exact": [1, 2, 3, 4, 5],
                "words-normalized": [
                    "PersonL vs Person2",
                    "Filed 09/06/2019 - Court of Jurisdiction - Page 1 of 2",
                    "1111-CV-11",
                    "08/05/2019",
                    "09/06/2019",
                ],
            }
        )

        doc = {
            "case_name_match": 0,
            "case_name_confirmed": "Person1 vs Person2",
            "docket_number_match": 0,
            "docket_number_confirmed": "1112-CV-11",
            "decided_date_match": 0,
            "decided_date_confirmed": "2019-08-05",
            "filed_date_confirmed": "2019-09-06",
        }
        self.assertTrue(isinstance(build_corrected_frame(doc, df), tuple))

    def test_build_corrected_frame_low_conf(self):
        df = pd.DataFrame(
            {
                "prediction": ["Case Name", "Pacer Header", "Docket Number", "Date"],
                "x-exact": [1, 2, 3, 4],
                "w-exact": [10, 2, 3, 4],
                "w-relative": [1, 2, 3, 4],
                "page": [1, 2, 3, 4],
                "y-exact": [1, 2, 3, 4],
                "words-normalized": [
                    "PersonL vs Person2",
                    "Filed 09/06/2019 - Court of Jurisdiction - Page 1 of 2",
                    "1111-CV-11",
                    "08/05/2019",
                ],
            }
        )

        doc = {
            "case_name_match": 0,
            "case_name_confirmed": "Person1",
            "docket_number_match": 0,
            "docket_number_confirmed": "1112",
            "decided_date_match": 0,
            "decided_date_confirmed": "2018-03-06",
            "filed_date_confirmed": "2018-12-01",
        }

        self.assertTrue(build_corrected_frame(doc, df)[1] == False)

    def test_find_closest_row(self):
        df = pd.DataFrame(
            {
                "y-exact": [1, 2, 3],
                "x-exact": [1, 2, 3],
                "page": [1, 2, 3],
                "words-normalized": [1, 2, 3],
            }
        )

        row = {"y-exact": 1.2, "x-exact": 1.2, "page": 1, "words-normalized": "test"}
        self.assertTrue(find_closest_row(row, df) == 0)

    def test_get_ngrams(self):
        lst = "This should be transformed into a list of overlapping 2-tuples".split()
        self.assertTrue(
            get_ngrams(lst, 2)
            == [
                ("This", "should"),
                ("should", "be"),
                ("be", "transformed"),
                ("transformed", "into"),
                ("into", "a"),
                ("a", "list"),
                ("list", "of"),
                ("of", "overlapping"),
                ("overlapping", "2-tuples"),
            ]
        )

    def test_find_closest_element_no_elems(self):
        self.assertTrue(find_closest_element(1, [], None) == -1)

    def test_find_closest_element_elem_present(self):
        self.assertTrue(find_closest_element(1, [1], None) == 1)

    def test_find_closest_element_elem_ceiling(self):
        self.assertTrue(find_closest_element(2, [0, 1, 3, 4], "ceiling") == 3)

    def test_find_closest_element_elem_floor(self):
        self.assertTrue(find_closest_element(2, [0, 1, 3, 4], "floor") == 1)

    def test_find_closest_row_top_row(self):
        df = pd.DataFrame(
            {
                "y-exact": [1, 2, 3],
                "x-exact": [1, 2, 3],
                "page": [1, 2, 3],
                "words-normalized": [1, 2, 3],
            }
        )

        row = {"y-exact": 0, "x-exact": 0, "page": 1, "words-normalized": "test"}
        self.assertTrue(find_closest_row(row, df) == None)

    @patch("app.sa")
    @patch("app.boto3")
    @patch.dict("app.os.environ", {"RDS_SECRET_ARN": "test"})
    def test_main(self, boto3, sqlalchemy):
        body = io.BytesIO(
            b"prediction,x-exact,w-exact,w-relative,page,y-exact,words-normalized\n"
            + b"Case Name,1, 10, 1, 1, 1, PersonL vs Person2\n"
            + b"Pacer Header, 2, 2, 2, 2, 2, Filed 09/06/2019 - Court of Jurisdiction - Page 1 of 2\n"
            b"Docket Number, 3, 3, 3, 3, 3, 1111-CV-11\n" + b"Date, 4, 4, 4, 4, 4, 08/05/2019\n"
        )

        s3_res = {"Body": body}

        client = Mock()
        resource = Mock()
        client.get_secret_value.return_value = {
            "SecretString": '{"username": "val", "password": "val", "host": "val", "port": 10}'
        }
        resource.Object.return_value.get.return_value = s3_res

        session = Mock()
        session.client.return_value = client
        session.resource.return_value = resource
        boto3.Session.return_value = session

        doc = {
            "case_name_match": 0,
            "case_name_confirmed": "Person1 vs Person2",
            "docket_number_match": 0,
            "docket_number_confirmed": "1112-CV-11",
            "decided_date_match": 0,
            "decided_date_confirmed": "2019-08-05",
            "filed_date_confirmed": "2019-09-06",
            "feature_csv_s3_uri": "s3://bucket/key",
            "id": "test",
        }

        result_proxy = Mock()
        result_proxy.fetchall.return_value = [doc]
        connection = Mock()
        connection.execute.return_value = result_proxy
        engine = Mock()
        engine.connect.return_value = connection
        sqlalchemy.create_engine.return_value = engine
        with self.assertNotRaises(Exception):
            main()


if __name__ == "__main__":
    unittest.main()
