import unittest

from unittest.mock import patch, Mock
from wsgi import app as app1
from app import app as app2


class WSGITests(unittest.TestCase):
    def test_app_identity(self):
        self.assertTrue(app1 is app2)
