import unittest

from unittest.mock import patch, Mock
from app import alive, pdftotext_s3, pdftotext, pdftotext_pdf


class PDFToTextServiceTests(unittest.TestCase):
    def test_alive(self):
        self.assertEquals(alive(), "pdftotext service is alive")

    @patch("app.s3")
    @patch("app.Popen")
    def test_pdftotext_s3_success(self, popen, s3):
        s3.get_object.return_value = {"Body": Mock()}
        proc = Mock()
        proc.communicate.return_value = ("pdfcontent", "")
        popen.return_value = proc
        request = Mock()
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        self.assertEqual(pdftotext_s3(request).status_code, 200)

    @patch("app.s3")
    @patch("app.Popen")
    def test_pdftotext_s3_bad_request_json(self, popen, s3):
        s3.get_object.return_value = {"Body": Mock()}
        proc = Mock()
        proc.communicate.return_value = ("pdfcontent", "")
        popen.return_value = proc
        request = Mock()
        request.data = b"test"
        self.assertEqual(pdftotext_s3(request).status_code, 400)

    @patch("app.s3")
    @patch("app.Popen")
    def test_pdftotext_s3_required_keys_missing(self, popen, s3):
        s3.get_object.return_value = {"Body": Mock()}
        proc = Mock()
        proc.communicate.return_value = ("pdfcontent", "")
        popen.return_value = proc
        request = Mock()
        request.data = b'{"key":"val"}'
        self.assertEqual(pdftotext_s3(request).status_code, 400)

    @patch("app.s3")
    @patch("app.Popen")
    def test_pdftotext_s3_object_retrieval_error(self, popen, s3):
        s3.get_object.side_effect = Exception()
        proc = Mock()
        proc.communicate.return_value = ("pdfcontent", "")
        popen.return_value = proc
        request = Mock()
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        self.assertEqual(pdftotext_s3(request).status_code, 500)

    @patch("app.s3")
    @patch("app.Popen")
    def test_pdftotext_s3_conversion_error(self, popen, s3):
        s3.get_object.return_value = {"Body": Mock()}
        proc = Mock()
        proc.communicate.side_effect = Exception()
        popen.return_value = proc
        request = Mock()
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        self.assertEqual(pdftotext_s3(request).status_code, 500)

    @patch("app.s3")
    @patch("app.Popen")
    def test_pdftotext_s3_upload_error(self, popen, s3):
        s3.get_object.return_value = {"Body": Mock()}
        s3.put_object.side_effect = Exception()
        proc = Mock()
        proc.communicate.return_value = ("pdfcontent", "")
        popen.return_value = proc
        request = Mock()
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        self.assertEqual(pdftotext_s3(request).status_code, 500)

    @patch("app.s3")
    @patch("app.Popen")
    def test_pdftotext_s3_std_err_and_std_in_exist(self, popen, s3):
        s3.get_object.return_value = {"Body": Mock()}
        proc = Mock()
        proc.communicate.return_value = ("content converted", "error encountered")
        popen.return_value = proc
        request = Mock()
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        self.assertEqual(pdftotext_s3(request).status_code, 200)

    @patch("app.s3")
    @patch("app.Popen")
    @patch("app.request")
    def test_pdftotext_jsontype_request_success(self, request, popen, s3):
        s3.get_object.return_value = {"Body": Mock()}
        proc = Mock()
        proc.communicate.return_value = ("pdfcontent", "")
        popen.return_value = proc
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        request.content_type = "application/json"
        self.assertEqual(pdftotext().status_code, 200)

    @patch("app.Popen")
    @patch("app.request")
    def test_pdftotext_pdftype_request_success(self, request, popen):
        proc = Mock()
        proc.communicate.return_value = ("pdfcontent", "")
        popen.return_value = proc
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        request.content_type = "application/pdf"
        self.assertEqual(pdftotext().status_code, 200)

    @patch("app.Popen")
    @patch("app.request")
    def test_pdftotext_no_contenttype_specified(self, request, popen):
        proc = Mock()
        proc.communicate.return_value = ("pdfcontent", "")
        popen.return_value = proc
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        self.assertEqual(pdftotext().status_code, 400)

    @patch("app.request")
    def test_pdftotext_no_request_data(self, request):
        request.data = None
        self.assertEqual(pdftotext().status_code, 400)

    @patch("app.Popen")
    def test_pdftotext_pdf_conversion_error(self, popen):
        proc = Mock()
        proc.communicate.side_effect = Exception()
        popen.return_value = proc
        request = Mock()
        request.data = b"test"
        self.assertEqual(pdftotext_pdf(request).status_code, 500)

    @patch("app.Popen")
    @patch("app.request")
    def test_pdftotext_pdf_std_err_exists(self, request, popen):
        proc = Mock()
        proc.communicate.return_value = ("", "error encountered")
        popen.return_value = proc
        request.data = b'{"s3_bucket": "test", "s3_key": "test"}'
        request.content_type = "application/pdf"
        self.assertEqual(pdftotext().status_code, 500)


if __name__ == "__main__":
    unittest.main()
