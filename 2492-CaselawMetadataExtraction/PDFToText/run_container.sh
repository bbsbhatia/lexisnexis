image=${1:-pdftotextservice}
profile=${2:-default}

AWS_ACCESS_KEY_ID=$(aws --profile $profile configure get aws_access_key_id)
AWS_SECRET_ACCESS_KEY=$(aws --profile $profile configure get aws_secret_access_key)
AWS_SESSION_TOKEN=$(aws --profile $profile configure get aws_session_token)

CONTAINER_ID=$(docker run -p 80:80 -d --rm \
   -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
   -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
   -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
   $image)

SHORT_CONTAINER_ID=${CONTAINER_ID:0:12}
echo $CONTAINER_ID
docker container ls | grep $SHORT_CONTAINER_ID