import boto3
import json
import logging

from flask import Flask, request, Response
from subprocess import Popen, PIPE
from werkzeug.local import LocalProxy

app = Flask(__name__)
s3 = boto3.client("s3")


@app.route("/", methods=["GET"])
def alive():
    return "pdftotext service is alive"


def pdftotext_s3(request: LocalProxy) -> Response:
    # verify well-formed JSON
    try:
        request_data = json.loads(request.data.decode("utf-8"))
    except:
        return Response("Invalid JSON on request", status=400)

    # verify required keys
    if not request_data.get("s3_bucket") or not request_data.get("s3_key"):
        return Response("Request JSON must contain s3_bucket and s3_key", status=400)

    bucket, key = request_data["s3_bucket"], request_data["s3_key"]

    # pull the file to convert
    try:
        app.logger.info(f"Pulling s3://{bucket}/{key}...")
        get_object_response = s3.get_object(Bucket=bucket, Key=key)
        print("res: ", get_object_response)
    except Exception:
        app.logger.exception("Error retrieving object from s3")
        return Response("Internal server error", status=500)

    # convert file with pdftotext
    try:
        app.logger.info(f"Converting s3://{bucket}/{key} to text...")
        proc = Popen(["pdftotext", "-bbox-layout", "-", "-"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        std_out, std_err = proc.communicate(input=get_object_response["Body"].read())
    except Exception:
        app.logger.exception("Error converting document")
        return Response("Internal server error", status=500)

    # store conversion output back in s3
    try:
        new_key = key + "_pdftotext.html"
        app.logger.info(f"Writing output to s3://{bucket}/{new_key}...")
        s3.put_object(Bucket=bucket, Key=new_key, Body=std_out)
    except Exception:
        app.logger.exception("Error putting results to s3")
        return Response("Internal server error", status=500)

    if std_err:
        app.logger.error("Non-fatal error encounterd while converting document:", std_err)
        # log it, but return whatever we got on std_out
        # return Response("Internal server error", status=500)

    app.logger.info(f"Conversion complete, output written to s3://{bucket}/{new_key}")
    response_body = json.dumps({"output_s3_uri": f"s3://{bucket}/{new_key}"})
    return Response(response_body, content_type="application/json", status=200)


@app.route("/pdftotext", methods=["POST"])
def pdftotext() -> Response:

    if not request.data:
        return Response("No data supplied on request", status=400)

    if request.content_type == "application/json":
        return pdftotext_s3(request)
    elif request.content_type == "application/pdf":
        return pdftotext_pdf(request)
    else:
        return Response("Content type must be application/json or application/pdf", status=400)


def pdftotext_pdf(request: LocalProxy) -> Response:

    try:
        app.logger.info("Converting pdf file bytes...")
        proc = Popen(["pdftotext", "-bbox-layout", "-", "-"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        std_out, std_err = proc.communicate(input=request.data)
    except Exception:
        app.logger.exception("Error converting document")
        return Response("Internal server error", status=500)

    if std_err:
        app.logger.error(std_err)
        return Response("Internal server error", status=500)
    app.logger.info("Conversion successful, supplying output on response")
    return Response(std_out, status=200, direct_passthrough=True)


if __name__ == "__main__":
    app.run(debug=True)
else:
    gunicorn_logger = logging.getLogger("gunicorn.error")
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
