dockerfile=${1:-DevOps/Docker/Dockerfile}
image=${2:-pdftotextservice}

docker build -f $dockerfile -t $image .
