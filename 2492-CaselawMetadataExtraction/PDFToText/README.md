## PDFToText service

### Description
Provides a containerized HTTP microservice around the poppler-utils application, pdftotext.

### Dependencies
#### Docker
See https://docs.docker.com/install/ for platform specific installation instructions, or use the following to install for Ubuntu:
```bash
#Add docker's official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#Set up stable docker repository.
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

#update package index now that we've added the docker repo
sudo apt-get -y update

#install docker community edition
sudo apt-get -y install docker-ce

#create docker group and config permissions
sudo groupadd docker
sudo usermod -aG docker $USER
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "/home/$USER/.docker" -R
```
### How to build application
```bash
docker build -f DevOps/Docker/Dockerfile -t pdftotextservice .
```
### How to run application
```bash
AWS_ACCESS_KEY_ID=$(aws --profile default configure get aws_access_key_id)
AWS_SECRET_ACCESS_KEY=$(aws --profile default configure get aws_secret_access_key)
AWS_SESSION_TOKEN=$(aws --profile default configure get aws_session_token)

docker run -p 80:80 --rm \
   -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
   -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
   -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN \
   pdftotextservice

[2019-05-06 20:23:52 +0000] [1] [INFO] Starting gunicorn 19.9.0
[2019-05-06 20:23:52 +0000] [1] [INFO] Listening at: http://0.0.0.0:80 (1)
[2019-05-06 20:23:52 +0000] [1] [INFO] Using worker: sync
[2019-05-06 20:23:52 +0000] [8] [INFO] Booting worker with pid: 8
```
### How to invoke 
#### Invoke directly for PDF on POST
```bash
curl -H "Content-Type:application/pdf" --data-binary "@<path to pdf>" 0.0.0.0:80/pdftotext
```
#### Invoke for S3 reference
 ```bash
curl -H "Content-Type:application/json" --data '{"s3_bucket": "<source bucket name>", "s3_key": "<source object key>"}' 0.0.0.0:80/pdftotext
 ```
### How to run unit tests
#### Step 1: Install dependencies
```bash
pip install -r requirements.txt
```
#### Step 2: Run the test script
```bash
./run_tests
...
...
----------------------------------------------------------------------
Ran 8 tests in 0.032s

OK
Name                     Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------------
Source/Lambda/index.py      37      0     10      0   100%
```
### Helper (optional) scripts
#### bootstrap.sh
Installs docker for an Ubuntu OS
#### build_image.sh
Small wrapper around docker build command setup
#### run_container.sh
Small wrapper for docker run command setup