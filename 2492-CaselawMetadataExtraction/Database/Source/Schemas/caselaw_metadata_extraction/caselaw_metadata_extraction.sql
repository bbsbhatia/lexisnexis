CREATE DATABASE IF NOT EXISTS `caselaw_metadata_extraction`;

USE `caselaw_metadata_extraction`;

CREATE TABLE `document_tracking` (
  # workflow tracking columns
  `id` VARCHAR(255) NOT NULL,
  `original_s3_uri` VARCHAR(1024) NOT NULL,
  `pdf_transform_s3_uri` VARCHAR(1024),
  `feature_csv_s3_uri` VARCHAR(1024),
  `court_extracted` TEXT,
  `court_normalized` TEXT,
  `court_confirmed` TEXT,
  `case_name_extracted` TEXT,
  `case_name_normalized` TEXT,
  `case_name_confirmed` TEXT,
  `docket_number_extracted` TEXT,
  `docket_number_normalized` TEXT,
  `docket_number_confirmed` TEXT,
  `date_extracted` TEXT,
  `decided_date_extracted` TEXT, # these should be dates
  `decided_date_normalized` TEXT,
  `decided_date_confirmed` TEXT,
  `filed_date_extracted` TEXT, # these should be dates
  `filed_date_normalized` TEXT, # these should be dates
  `filed_date_confirmed` TEXT, # these should be dates
  `page_count` INT,
  `in_traning_set` TINYINT DEFAULT 0,
  `postback_timestamp` DATETIME(3),
  `upload_confirmed_timestamp` DATETIME(3),
  `upload_staged_timestamp` DATETIME(3),
  `metadata_extraction_timestamp` DATETIME(3),
  `ttl` DATETIME(3),
  PRIMARY KEY (`id`)
);

CREATE TABLE `training_set_updates` (
  `id` VARCHAR(255) NOT NULL,
  `s3_uri` VARCHAR(1024) NOT NULL,
  `postback_timestamp_range_start` DATETIME(3),
  `postback_timestamp_range_end` DATETIME(3),
  PRIMARY KEY (`id`)
);