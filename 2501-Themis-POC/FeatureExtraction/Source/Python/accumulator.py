"""
Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.

This module provides functions to accumulate features so that these are batched up 
and written to a csv file.

"""

import csv
import os


class FeatureAccumulator:
    """
    FeatureAccumulator provides mechanism to accumulate features so that these are 
    batched up and written to a csv file.
    """
    def __init__(self, feature_directory, feature_file, batch_size=1000):
        """
        Create a new instance.

        :type feature_directory: string
        :param feature_directory: absolute path for feature file directory
        :type feature_file: string
        :param feature_file: feature file name
        :type batch_size: int
        :param batch_size: batch size (number of rows)
        """
        self.feature_directory = feature_directory
        self.feature_file = feature_file
        self.file_created = False
        self.batch_size = batch_size
        self.features_list = []
        self.keys = []

    def add_features(self, features):
        """
        Add features to internal list and flush to csv file.

        :param features: dict
        :param features: features dictionary
        """
        self.features_list.append(features)
        if len(self.features_list) >= self.batch_size:
            self.flush()

    def flush(self):
        """
        Add features to internal list and flush to csv file.

        :param features: dict
        :param features: features dictionary
        """
        print('\nWriting batch of {} features to {}'.format(len(self.features_list), self.feature_file));
        mode = 'a' if self.file_created else 'w'
        self.keys = self.features_list[0] if len(self.features_list) > 0 else []
        with open(os.path.join(self.feature_directory, self.feature_file), mode) as f:
            writer = csv.DictWriter(f, fieldnames=self.keys)
            if mode == 'w':
                writer.writeheader()
            writer.writerows(self.features_list)
            self.file_created = True
        self.features_list.clear()

    def read_csv(self, file):
        with open(file, mode='r') as f:
            csv_reader = csv.DictReader(f)
            line_count = 0
            for row in csv_reader:
                self.features_list.append(row)
            print(f'Processed {line_count} lines.')
        return self.get_feature_list()

    def get_feature_list(self):
        return self.features_list
