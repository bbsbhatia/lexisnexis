import csv
import os

class FeatureExtractorBuildCsv:

    def build_csv(self, feature_directory, generated_list, feature_version, file_name='holdoutdata.csv'):
        keys = generated_list[0].keys()
        path_to_dir = os.path.join(feature_directory, file_name)
        with open(path_to_dir, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=keys)
            writer.writeheader()
            writer.writerows(generated_list)
        self.upload_file(feature_directory, file_name, feature_version)
        generated_list.clear()

    def upload_file(self, session, s3bucket_name, feature_dir, key_prefix, file_name):
        s3_feature = session.upload_data(feature_dir, bucket=s3bucket_name, key_prefix=key_prefix)
        print("Uploaded %s CSV -> %s" % (file_name, s3_feature))