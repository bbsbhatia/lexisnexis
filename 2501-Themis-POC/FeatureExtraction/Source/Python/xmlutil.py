"""
    Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
    
    FeatureExtractorXmlUtil builds a xml tree from fragments of the lambda response that we pass it.
    It also finds certain tags within a xml fragment that we need to train the model.
"""
from lxml import etree as et


class FeatureExtractorXmlUtil:

    def __init__(self):
        """
        Create a new instance.
        """

    def xml_to_tree(self, fragment):
        """
        Return ElementTree for given xml text. This builds the namespaces
        and creates a root <doc> for the fragment that is passed.
        :param fragment: XML response/body that needs to be updated
        :return tree and naemspaces for the xml fragrment we pass
        """

        namespaces = {'laaa': 'http://www.w3.org/laaa',
                      'lnci': 'http://www.w3.org/lnci', 'lnvxe': 'http://www.w3.org/lnvxe'}
        ns = ' '.join(('xmlns:{}="{}"'.format(*i) for i in namespaces.items()))
        xml = '<doc {}>{}</doc>'.format(ns, fragment)
        tree = et.fromstring(xml)
        return tree, namespaces

    def find_element_attributes(self, tag, fragment, searchchildren):
        """
        Return attribute list of the first element or children based on tag and attribute value.
        :param tag: XML tag to search for
        :param fragment: XML response/body that is being search
        :param searchchildren: True/False, determines if needed to search within the XML tag
        :return attributes
        """

        tree, namespaces = self.xml_to_tree(fragment)
        elements = tree.xpath(".//{}".format(tag), namespaces=namespaces)

        if searchchildren:
            attributes = []
            for el in elements:
                list_of_elements = list(el)
                dictionary_of_elements = {}
                for feature in list_of_elements:
                    dictionary_of_elements.update(feature.attrib)
                attributes.append(dictionary_of_elements)
        else:
            attributes = [el.attrib for el in elements]
        return attributes
