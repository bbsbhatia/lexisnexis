"""
    Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.

    FeatureExtractorBuildCsv is used to build the feature dictionary list from the AWS lambda response
    This also uses the FeatureExtractorXmlUtil in order to parse the response with the features we want in the individual
    paragraphs.
"""
from buildcsv import *
from accumulator import *


def register_functions():
    """
    A delegate function that handles the functions that are inside the class.
    Anything with the attribute @builder will be have their name and location
    in memory tied to a dictionary registry.

    registrar.all gets all the items in registry
    :return: registrar
    """
    registry = {}

    def registrar(func):
        registry[func.__name__] = func
        return func

    registrar.all = registry
    return registrar


builder = register_functions()


class BuildListDicts:
    """
    This is a Util class to create the individual feature dictionaries in order to
    filter out each list dictionary for file conversion.
    """

    def __init__(self):
        """
        Creates a new instance.
        """
        self.master_csv_list = []

    @builder
    def build_fonly_features_all_nos(self, list_of_dicts):
        """
        Treated only with 'f', had cites recognized prospectively
        :param list_of_dicts:
        :returns list of dictionaries with only 'f' and cites recognized
        """
        # followedbyct > 0, adj_casecct > 0, letters is not blank and only f
        fonly_list = []
        for dictionary in list_of_dicts:
            keys = ['followedbyct', 'adj_casecct', 'letters']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['followedbyct']) > 0 and int(dict_values['adj_casecct']) > 0 and dict_values[
                        'letters'] is not '':
                    fonly_list.append(dictionary)

        return fonly_list

    @builder
    def build_fonly_nos_features_all_nos(self, list_of_dicts):
        """
        Treated only with ‘f’, no cites recognized prospectively – manually added anaphoric
        :param list_of_dicts:
        :returns list of dictionaries
        """
        # letters == f only, adj_casecct == 0, mananaphcasect > 0
        fonly_manual_anaphorics_list = []
        for dictionary in list_of_dicts:
            keys = ['followedbyct', 'mananaphcasect', 'letters']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['followedbyct']) > 0 and int(dict_values['mananaphcasect']) > 0 and dict_values[
                        'letters'] is not '':
                    fonly_manual_anaphorics_list.append(dictionary)

        return fonly_manual_anaphorics_list

    @builder
    def build_nocite_features_all(self, list_of_dicts):
        """
        Untreated, no cites
        :param list_of_dicts:
        :returns list of dictionaries
        """
        # trts are empty and retrocct = 0
        untreated_list = []
        for dictionary in list_of_dicts:
            keys = ['retrocct', 'letters']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['retrocct']) == 0 and dict_values['letters'] is '':
                    untreated_list.append(dictionary)

        return untreated_list

    @builder
    def build_notreat_features_all_nos(self, list_of_dicts):
        """
        Untreated, contains citations
        :param list_of_dicts:
        :returns list of dictionaries
        """
        # trts are empty but retrocct has counts
        untreated_cites_list = []
        for dictionary in list_of_dicts:
            keys = ['letters', 'retrocct']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['retrocct']) > 0 and dict_values['letters'] is '':
                    untreated_cites_list.append(dictionary)

        return untreated_cites_list

    @builder
    def build_treated_nocase_features_all_nos(self, list_of_dicts):
        """
        Treated, non-case citations only
        :param list_of_dicts:
        :returns list of dictionaries
        """
        # trts have values, but casecct = 0
        treated_noncase_list = []
        for dictionary in list_of_dicts:
            keys = ['letters', 'casecct']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['casecct']) == 0 and dict_values['letters'] is not '':
                    treated_noncase_list.append(dictionary)

        return treated_noncase_list

    @builder
    def build_treatedandf_features_all_nos(self, list_of_dicts):
        """
        Treated with ‘f’ and another letter, had cites recognized prospectively
        :param list_of_dicts:
        :returns list of dictionaries
        """
        # trts is not empty, adj_casecct > 0
        treatedandf_list = []
        for dictionary in list_of_dicts:
            keys = ['letters', 'adj_casecct']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['adj_casecct']) > 0 and dict_values['letters'] is not '':
                    treatedandf_list.append(dictionary)

        return treatedandf_list

    @builder
    def build_treatedandf_nos_features_all_nos(self, list_of_dicts):
        """
        Treated with ‘f’ and another letter, no cites recognized prospectively
         – manually added anaphoric
        :param list_of_dicts:
        :returns list of dictionaries
        """
        # trts is not empty, adj_casecct == 0, mananaphcasect > 0
        treatedandf_nos_list = []
        for dictionary in list_of_dicts:
            keys = ['letters', 'adj_casecct', 'mananaphcasect']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['adj_casecct']) == 0 and int(dict_values['mananaphcasect']) > 0 and dict_values[
                        'letters'] is not '':
                    treatedandf_nos_list.append(dictionary)

        return treatedandf_nos_list

    @builder
    def build_treatednof_features_all_nos(self, list_of_dicts):
        """
        Treated but NOT with ‘f’, had cites recognized prospectively
        :param list_of_dicts:
        :returns list of dictionaries
        """
        # followedbyct == 0, adj_casecct > 0
        nof_cites_list = []
        for dictionary in list_of_dicts:
            keys = ['followedbyct', 'adj_casecct']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['adj_casecct']) > 0 and int(dict_values['followedbyct']) == 0:
                    nof_cites_list.append(dictionary)

        return nof_cites_list

    @builder
    def build_treatednof_nos_features_all_nos(self, list_of_dicts):
        """
        Treated but NOT with ‘f’, no cites recognized prospectively – manually added anaphoric
        :param list_of_dicts:
        :returns list of dictionaries
        """
        # followedbyct == 0, adj_casecct == 0, mananaphcasect > 0
        nof_manual_list = []
        for dictionary in list_of_dicts:
            keys = ['followedbyct', 'adj_casecct', 'mananaphcasect']
            dict_values = self.determine_key_values(keys, dictionary)
            for key in dict_values:
                if int(dict_values['followedbyct']) == 0 and int(dict_values['adj_casecct']) == 0 and int(
                        dict_values['mananaphcasect']):
                    nof_manual_list.append(dictionary)

        return nof_manual_list

    def determine_key_values(self, keys, dictionary):
        """
        This gives me the needed key value pairs in a dictionary format for each of the builder functions
        :param keys: The list of keys we want to search for
        :param dictionary: The dictionary we are searching in
        :return: key value pairs in a dictionary format
        """
        dictionary_values = {}
        for item in dictionary:
            for key in keys:
                value = (dictionary.get(key))
                dictionary_values.update({key: value})
            return dictionary_values

    def call(self, registered_functions, function_name):
        """
        Helper function that delegates each function in their spot in memory.
        :param registered_functions: The dictionary of functions that are needing to be delegated
        :param function_name: The name of the function being called
        :return: List dictionary of features for that feature file
        """
        # dictionary_name['build_treatedandf_features_all_nos'](self, convert_to_csv_list)
        return registered_functions[function_name](self, self.master_csv_list)

    def create_all_feature_files(self, feature_directory, files_to_build, feature_version):
        """
        Calls all the builder functions and delegates them to create a csv file with the names passed in a list
        :param feature_directory: The feature directory (S3 bucket) we want to store the files
        :param files_to_build: A list of file names to build
        :param feature_version: The version number we pass
        """
        builder_functions = ["build_" + file_name for file_name in files_to_build]
        all_builder_functions = builder.all
        for function, file_name in zip(builder_functions, files_to_build):
            if function in builder_functions:
                generated_list = self.call(all_builder_functions, function)
                if generated_list:
                    csv_builder = FeatureExtractorBuildCsv()
                    csv_builder.build_csv(feature_directory, generated_list, feature_version, file_name=file_name)
            else:
                print('%s is not a function that is built into the class. Please add the function along with the '
                      'appropriate checkers for the feature file.' % function)
