"""
Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.

This module provides an API to invoke feature extractor service.

For more information refer to https://wiki.regn.net/wiki/DataLake_Hello_World
"""

from requests import request
from requests.exceptions import RequestException
from buildmasterlist import *


class FeatureExtractor:
    """
    FeatureExtractor provides mechanism to invoke feature extractor service
    so that features from a JCD document can be returned to the client
    """
    def __init__(self, api_url):
        """
        Create a new instance.

        :type api_url: string
        :param api_url: feature extractor service end point (API Gateway Url)
        """
        self.api_key = api_url

    def invoke_lambda(self, payload, uri):
        headers = {
            'Accept': "application/xml",
            'Content-type': "application/xml",
            'accept-encoding': "gzip, deflate"
        }

        print('Invoking lambda function...')

        try:
            response = request('POST', uri, headers=headers, data=payload.encode('utf-8'))

            if response.ok:
                print('Successful POST request..')
                return response.text
            else:
                print('Unable to get resources, Status Code: %s' % response.status_code)

        except RequestException as e:
            print('Error: {}'.format(e))

    def get_api_url(self):
        return self.api_key

    def get_features(self, document):
        """
        Get list of feature dictionary for a document.

        :type document: string
        :param document: JCD document string representation

        :return: feature dictionary list
        """
        response = self.invoke_lambda(document, self.get_api_url())
        master_list_builder = FeatureExtractorBuildMasterList()
        master_list_builder.build_feature_list_dictionaries(response)

        features = master_list_builder.get_feature_list()
        return features
