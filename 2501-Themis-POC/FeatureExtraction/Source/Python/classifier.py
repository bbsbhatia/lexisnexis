# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This module provides methods to classify a given feature set.
#

def classify_treatment(features):
    """
    Determine if given set of features would classify a paragraph as having treatment or not.
    
    Currently we test for treatment if the trts feature is not blank. Which means the DPF found
    treatment in the paragraph. This can be a little more sophisticated in the future when we add negative
    treatment into the mix.

    :type features: dict
    :param features: dictionary of paragraph features

    :return: 1 if treatment eligible, 0 if not
    """
    for feature in features:
        trts = features.get('trts')
        if trts is not '':
            return 1
    return 0
