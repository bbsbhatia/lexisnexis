var DocumentDictionary = ( function () {

    var citeDictionary = {};
    //cite1 = {id: 'I5VXB', 'letter': '', 'anaphoric_id': '', 'para_id':'3', 'descr':'x vs. y'};
    //citeDictionary['I5VXB'] = cite1;

    var paraDictionary = {};
    //para1 = {id: '1', cites:['I5VXB'], content: 'abcde', trigger: true};
    //paraDictionary['1'] = para1

    var triggerDictionary = {};
    //trigger1 = {id: 'I4M9S4V', fragments: [{'para_id':'3', content: 'bcd' }] 
    //triggerDictionary['I5VXB'] = trigger1

    var buildDictionary = function() {
        for (var key in data) {
            ids = key.split(':'), paraId = ids[0], groupId = ids[1];
            cites = data[key].cites;
            updateCiteDictionary(cites, paraId)
            updateParaDictionary(paraId, data[key].para, cites);
            if (groupId != '') updateTriggerDictionary(groupId, data[key]['cite_ref'], paraId, data[key].lang)
        }
        for (var key in triggerDictionary) {
            triggerDictionary[key].fragments.forEach(function(fragment) {
                paraDictionary[fragment['para_id']].trigger = true;
            });
        }

    }

    var updateTriggerDictionary = function(groupId, citeRef, paraId, lang) {
        citeIds = citeRef.split(':');
        citeIds.forEach(function(citeId){
            trigger = triggerDictionary[citeId];
            if (trigger == null) trigger = {id:groupId, fragments:[]};
            trigger.fragments.push({'para_id':paraId, content:lang});
            triggerDictionary[citeId] = trigger;
        });
    }

    var updateParaDictionary = function(paraId, content, cites) {
        para = {id:paraId, content:content, cites:toCiteIds(cites), trigger:false};
        paraDictionary[paraId] = para;
    }

    var updateCiteDictionary = function(cites, paraId) {
        cites.forEach(function(cite, index){
            cite['para_id'] = paraId
            citeDictionary[cite.id] = cite 
        });        
    }

    var toCiteIds = function(cites) {
        ids = [];
        cites.forEach(function(cite, index){
            ids.push(cite.id) 
        });
        return ids;       
    }

    var getPara = function getPara(id) {
        return paraDictionary[id];    
    }

    var getParaIds = function(trigger=false) {
        ids = [];
        for (var key in paraDictionary) {
            ids.push(paraDictionary[key].id);
        }
        return ids;
    }

    var getSections = function(paraId, citeId=null) {
        para = getPara(paraId);
        content = para.content;
        citeId = `${citeId}`;
        cites = [];
        para.cites.forEach(function(id){
            cites.push(getCite(id).descr);
        });
        trigger = getCiteTrigger(citeId);
        if (trigger == null) {
            return [{text: content, highlight: false, cites: cites}];
        }
        highlights = [];
        trigger.fragments.forEach(function(fragment){
            if (fragment['para_id'] == paraId) {
                highlights.push(fragment.content);
            }
        });
        sections = makeSections(content, highlights, cites);
        return sections;
    }

    var makeSections = function(content, highlights, cites) {
        str = content;
        highlights.forEach(function(text){
            str = str.replace(text, `<m>${text}<m>`);
        });
        sections = []
        str.split('<m>').forEach(function(text){
            if (text == "") return;
            highlight = highlights.includes(text);
            cref = [];
            cites.forEach(function(cite){
                if(text.includes(cite)) cref.push(cite);
            });
            sections.push({text: text, highlight: highlight, cites: cref});
        });
        return sections;
    }

    var getSectionlets = function(text, cites) {
        str = text;
        cites.forEach(function(cite){
            str = str.replace(cite, `<m>${cite}<m>`);
        });
        sectionlets = [];
        str.split('<m>').forEach(function(s){
            if (s == "") return;
            annotation = cites.includes(s) ? 'cite': '';
            sectionlets.push({text: s, annotation: annotation});
        });
        return sectionlets;
    }

    var getCite = function(id) {
        return citeDictionary[id];
    }

    var getCiteIds = function() {
        ids = [];
        for (var key in citeDictionary) {
            ids.push(citeDictionary[key].id);
        }
        return ids;
    }

    followedLetters = ['#&ff', '#ff', '&ff', 'ff', '#&f', '#f', '&f', 'f'];

    var getFollowedCiteIds = function() {
        ids = [];
        for (var key in citeDictionary) {
            cite = citeDictionary[key];
            if (followedLetters.includes(cite.letter)) ids.push(cite.id);
        }
        return ids;
    }

    var getCiteTrigger = function(citeId) {
        return triggerDictionary[citeId];
    }

    var getTriggerIds = function() {
        ids = [];
        for (var key in triggerDictionary) {
            ids.push(triggerDictionary[key].id);
        }
        return ids;    
    }
    
    buildDictionary();
    
    dictionary = {
        getTriggerIds: getTriggerIds, 
        getCiteTrigger: getCiteTrigger,
        getFollowedCiteIds: getFollowedCiteIds,
        getCiteIds: getCiteIds,
        getCite: getCite,
        getSectionlets: getSectionlets,
        makeSections: makeSections,
        getSections: getSections,
        getPara: getPara,
        getParaIds: getParaIds        
    }
    
    return dictionary;
    
}) ();
