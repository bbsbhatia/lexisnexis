class Document extends React.Component { 
    
    state = {
        paragraphIds: getParaIds(),
        followedCiteIds: getFollowedCiteIds(),
        selectedCiteId: null
    };

    highlightTriggerLanguage = (citeId) => {
        this.setState(cState =>({paragraphIds: cState.paragraphIds, followedCiteIds: cState.followedCiteIds, selectedCiteId:citeId}));
    }

    render() { return(
        <div class="row">
            <div class="col-md-3">
                <div class="sticky-top">
                    <Navigator ids={this.state.followedCiteIds} />
                </div>
            </div>
            <main class="col-md-9" role="main">
                <ParagraphList ids={this.state.paragraphIds} ref="ids" />
            </main>
        </div>        
    ); }
};

class Navigator extends React.Component { 
    render() { 
        return(
            <CiteList ids={this.props.ids} action="true"/>
        ); 
    }
};

class ParagraphList extends React.Component { 
    render() { 
        return(
            this.props.ids.map(id => <Paragraph para={getPara(id)} />)
        ); 
    }
};


const Paragraph = (props) => {
    const sections = getSections(props.para.id, 'I5VXB2RB2D6MS601B0000400');
    return (
        <div class="row">
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-header">
                        <h3 class="panel-title">Paragraph {props.para.id}</h3>
                    </div>
                    <div class="card-body">
                    {
                        sections.map(section => <Section highlight={section.highlight} text={section.text} />)
                    }
                    </div>
                    <div class="card-footer">
                        <CiteList ids={props.para.cites} />
                    </div>
                </div>
            </div>
        </div>  
    );
};

class Section extends React.Component { 
    
    render() { 
        return(
            <span className={this.props.highlight ? 'text-warning' : 'text-normal'}>{this.props.text}</span>
        ); 
    }
};

class CiteList extends React.Component { 
    render() { 
        return(
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-group">
                    {
                        this.props.ids.map(id => <Cite action={this.props.action} cite={getCite(id)} />)
                    }
                    </ul>
                </div>
            </div>
        ); 
    }
};

const Cite = (props) => {
    const action = props.action;
    if ("true" == props.action) {
        return <CiteLink cite={props.cite} />;
    } else {
        return <CiteAnchor cite={props.cite} />
    }
};

const CiteLink = (props) => {
    return (
        <a href={`#${props.cite.id}`} class="list-group-item list-group-item-action">
            <span>{props.cite.id}</span>
            <span class="badge badge-pill badge-success ml-2">{props.cite.letter}</span>
        </a>
    );
};

const CiteAnchor = (props) => {
    return (
        <li id={props.cite.id} class="list-group-item  mb-2">
            <span>{props.cite.id}</span>
            <span class="badge badge-pill badge-success ml-2">{props.cite.letter}</span>
        </li>
    );
};