const initState = {
    paragraphIds: DocumentDictionary.getParaIds(),
    followedCiteIds: DocumentDictionary.getFollowedCiteIds(),
    selectedCiteId: null,
    collapse: false,
    viewMode: 'text'
}

function reducer(state = initState, action) {
    if (action.type == 'HILITE_LANGUAGE') {
        const collapse = state.selectedCiteId === action.cite ? !state.collapse : false;
        return {
            paragraphIds: state.paragraphIds,
            followedCiteIds: state.followedCiteIds,
            selectedCiteId: action.cite,
            collapse: collapse,
            viewMode: state.viewMode
        }
    };
    if (action.type == 'REVEAL_ANAPHORICREF') {
        const collapse = false;
        return {
            paragraphIds: state.paragraphIds,
            followedCiteIds: state.followedCiteIds,
            selectedCiteId: state.selectedCiteId,
            collapse: collapse,
            viewMode: state.viewMode
        }
    };    
    if (action.type == 'SWITCH_VIEW') {
        const collapse = false;
        return {
            paragraphIds: state.paragraphIds,
            followedCiteIds: state.followedCiteIds,
            selectedCiteId: state.selectedCiteId,
            collapse: collapse,
            viewMode: action.mode
        }
    };    
    return state;
}

const store = Redux.createStore(reducer);

store.subscribe(() => {
  console.log(store.getState());
});

const Provider = ReactRedux.Provider;

const mapStateToProps = state => ({ 
    paragraphIds: state == null ? [] : state.paragraphIds, 
    followedCiteIds: state == null ? [] : state.followedCiteIds,
    selectedCiteId: state == null ? null : state.selectedCiteId,
    collapse: state == null ? false : state.collapse,
    viewMode: state == null ? '' : state.viewMode
});

class Document extends React.Component { 
    
    render() { 
        return(
            <React.Fragment>
                <NavBar />
                <GraphView />
                <TextView />
            </React.Fragment>
        ); 
    }
};

class _NavBar_ extends React.Component { 

    constructor(props) {
        super(props);
        this.state = {mode:'text', network:null};
    }

    delayedVisualization = () => {
        this.setState((cState) => ({mode: cState.mode, network: renderVisualization()}));
    }

    handleClick = () => {
        document.getElementById('document-text').classList.toggle("show");
        document.getElementById('network').classList.toggle("show");
        if (this.state.network) this.state.network.destroy();
        setTimeout(this.delayedVisualization, 500);
    }

    handleChange = (e) => {
        const mode = e.target.value;
        this.setState((cState) => ({mode: mode, network: cState.network}));
        document.getElementById('mode-text').classList.toggle("active");
        document.getElementById('mode-graph').classList.toggle("active");
        store.dispatch({ type: 'SWITCH_VIEW', mode: mode });
    }
    
    render() { 
        return(
            <nav class="navbar navbar-light bg-light fixed-top">
                <div class="btn-group btn-group-toggle">
                    <label id="mode-text" class="btn btn-primary btn-sm active">
                        <input type="radio" value="text" checked={this.state.mode === 'text'} onChange={this.handleChange}/>
                        Text
                        <i class='ml-2 fas fa-file-alt'></i>
                    </label>
                    <label id="mode-graph" class="btn btn-primary btn-sm">
                        <input type="radio" value="graph" checked={this.state.mode === 'graph'} onChange={this.handleChange}/>
                        Graph
                        <i class='ml-1 fas fa-bezier-curve'></i>
                    </label>
                </div>
            </nav>
        ); 
    }
};

const NavBar = ReactRedux.connect(mapStateToProps)(_NavBar_)

class _TextView_ extends React.Component { 
   
    render() { 
        const mode = this.props.viewMode;
        const show = mode === 'text' ? 'show' : '';
        return(
            <React.Fragment>
                <div id="document-text" class={`container-fluid collapse ${show}`}>       
                    <div class="row">
                        <div class="col-md-3">
                            <div class="sticky-top bd-sidebar">
                                <Navigator />
                            </div>
                        </div>
                        <main class="col-md-9 bd-main" role="main">
                            <ParagraphList />
                        </main>
                    </div>        
                </div>
            </React.Fragment>
        ); 
    }
    
}

const TextView = ReactRedux.connect(mapStateToProps)(_TextView_)

class _GraphView_ extends React.Component { 

    componentDidUpdate(prevProps, prevState) {
        if (this.props.viewMode === 'graph') DocumentVisualizer.render('visualizer');
    }

    render() { 
        const show = this.props.viewMode === 'graph' ? 'show' : '';
        return(
            <div id="visualizer" class={`collapse ${show} sticky-top bd-visualizer`}>
            </div>     
        ); 
    }
};

const GraphView = ReactRedux.connect(mapStateToProps)(_GraphView_)

class _Navigator_ extends React.Component { 
    render() { 
        return(
            <React.Fragment>
                <CiteList ids={this.props.followedCiteIds} action="true"/>
            </React.Fragment>
        ); 
    }
};

const Navigator = ReactRedux.connect(mapStateToProps)(_Navigator_)

class _ParagraphList_ extends React.Component { 
    render() { 
        const selCiteId = this.props.selectedCiteId;
        const collapse = this.props.collapse;
        return(
            <React.Fragment>
                <h6>Document Content</h6>
                {
                this.props.paragraphIds.map(id => <Paragraph para={DocumentDictionary.getPara(id)} selCiteId={selCiteId} collapse={collapse} />)
                }
            </React.Fragment>
        ); 
    }
};

const ParagraphList = ReactRedux.connect(mapStateToProps)(_ParagraphList_)

const Paragraph = (props) => {
    const sections = DocumentDictionary.getSections(props.para.id, props.selCiteId);
    const hilited = sections.filter(s => s.highlight == true).length > 0 || props.para.cites.includes(props.selCiteId);
    const show = !props.collapse || hilited ? 'show' : '';
    const citeCount = props.para.cites.length;
    return (
        <div class={`row collapse ${show}`}>
            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-header">
                        <h6 class="panel-title">Paragraph {props.para.id}</h6>
                    </div>
                    <div class="card-body">
                    {
                        sections.map(s => <Section highlight={s.highlight} type={s.type} text={s.text} cites={s.cites} />)
                    }
                    </div>
                    <div class={citeCount  != 0 ? 'card-footer' : 'd-none'}>
                        <CiteList ids={props.para.cites} />
                    </div>
                </div>
            </div>
        </div>  
    );
};

class Section extends React.Component {     
    render() { 
        const highlight = this.props.highlight;
        const sectionlets = DocumentDictionary.getSectionlets(this.props.text, this.props.cites);
        return sectionlets.map(s => <Sectionlet text={s.text} annotation={s.annotation} highlight={highlight} /> );
    }
};

class Sectionlet extends React.Component {     
    render() { 
        const text = this.props.text;
        const highlight = this.props.highlight;
        const annotation = this.props.annotation;
        const citeClass = annotation == 'cite' ? 'bg-opacity5' : '';
        const cssClass = this.props.highlight ? `bg-warning ${citeClass}` : `text-muted ${citeClass}`;        
        return <span class={cssClass}>{this.props.text}</span>
    }
};

class CiteList extends React.Component { 
    render() { 
        const action = this.props.action;
        const cssClass = action == "true" ? 'col-md-12': 'col-md-10';
        return(
             <React.Fragment>
                <h6 class="panel-title">Cites {action == "true" && "Followed"}</h6>
                <div class="row">
                    <div class={cssClass}>
                        <ul class="list-group">
                        {
                            this.props.ids.map(id => <Cite action={action} cite={DocumentDictionary.getCite(id)} />)
                        }
                        </ul>
                    </div>
                </div>
             </React.Fragment>
        ); 
    }
};

const Cite = (props) => {
    const action = props.action;
    if ("true" == props.action) {
        return <CiteLink cite={props.cite} />;
    } else {
        return <CiteAnchor cite={props.cite} />
    }
};

class _CiteAnchor_ extends React.Component  {

    scrollToAnchor = () => {
        let navbar = document.querySelector(".navbar");
        let navbarheight = parseInt(window.getComputedStyle(navbar).height,10);
        window.scrollBy({top: (navbarheight + 5) * -1, left: 0, behavior: 'smooth' });
    }

    handleClick = (e) => {
        setTimeout(() => {this.scrollToAnchor();}, 100);
        const citeAction = { type: 'REVEAL_ANAPHORICREF', cite: this.props.cite.anaphoric_id };
        store.dispatch(citeAction);
    }

    render() {
        const hilited = this.props.cite.id == this.props.selectedCiteId ? 'bg-warning': '';
        const anaphoricRef = this.props.cite.anaphoric_id != '' ? DocumentDictionary.getCite(this.props.cite.anaphoric_id).descr : '';
        return (
            <li id={this.props.cite.id} class={`list-group-item ${hilited} mb-2`}>
                <span>
                    <small>{this.props.cite.descr}</small>
                    <span class="badge badge-pill badge-success ml-2">{this.props.cite.letter}</span>
                </span>
                <hr class={anaphoricRef  != '' ? '' : 'd-none'}/>
                <a href={`#${this.props.cite.anaphoric_id}`} onClick={this.handleClick}>
                    <span class={anaphoricRef != '' ? '' : 'd-none'}><small>{anaphoricRef}</small></span>
                </a>
            </li>
        );
    }
};

const CiteAnchor = ReactRedux.connect(mapStateToProps)(_CiteAnchor_)
    
class _CiteLink_ extends React.Component {
    
    scrollToAnchor = () => {
        let navbar = document.querySelector(".navbar");
        let navbarheight = parseInt(window.getComputedStyle(navbar).height,10);
        window.scrollBy({top: (navbarheight + 5) * -1, left: 0, behavior: 'smooth' });
    }
    
    handleClick = () => {
        setTimeout(() => {this.scrollToAnchor();}, 100);
        const citeAction = { type: 'HILITE_LANGUAGE', cite: this.props.cite.id };
        store.dispatch(citeAction);
    }
    
    render() {
        const active = this.props.cite.id == this.props.selectedCiteId ? 'active': '';
        return (
            <a href={`#${this.props.cite.id}`} class={`list-group-item list-group-item-action ${active}`} onClick={this.handleClick}>
                <span><small>{this.props.cite.descr}</small></span>
                <span class="badge badge-pill badge-success ml-2">{this.props.cite.letter}</span>
            </a>
        );
    }
};

const CiteLink = ReactRedux.connect(mapStateToProps)(_CiteLink_)

//store.dispatch({ type: 'HILITE_LANGUAGE', cite: 'I5VXB2RB2D6MS601F0000400' });


