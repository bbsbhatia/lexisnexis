import os
from laaa_classifier_core.classifier_training import classifier_training


if __name__ == "__main__":
    classifier_training.train_classifier(os.path.dirname(__file__))
