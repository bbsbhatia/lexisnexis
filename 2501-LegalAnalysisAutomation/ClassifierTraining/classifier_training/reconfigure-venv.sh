JSON_FILE=/home/ec2-user/classifier-training/src/python/config.json
echo "configuration file: $JSON_FILE"
CLASSIFIER_CORE_VERSION=`cat $JSON_FILE | python3 -c "import sys, json; print(json.load(sys.stdin).get('classifier_core_version', 'NONE'))"`
echo "classifier core version: $CLASSIFIER_CORE_VERSION"
if [ "$CLASSIFIER_CORE_VERSION" != 'NONE' ]; then
  echo override venv classifier-core with package $CLASSIFIER_CORE_VERSION
  source /home/ec2-user/venv/classifier-training/bin/activate
  pip install laaa-classifier-core==$CLASSIFIER_CORE_VERSION
  deactivate
fi