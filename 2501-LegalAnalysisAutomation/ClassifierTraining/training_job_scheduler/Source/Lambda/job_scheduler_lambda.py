import json
import jenkins
import boto3

JENKINS_URL = 'http://jenkins.content.aws.lexis.com'
JENKINS_USER = 'bhatias1'
USER_TOKEN = 'lambda-token'
USER_PASSWORD = '11ead797fd4d0fd1e31cd76fe9dc71e5c6'
TRIGGER_JOB_NAME = 'Themis/AutomatedClassifierTraining/DeploymentJobTrigger'
TARGET_JOB_BUILD_ID = '8'  # release build unit for classifier_training job


def lambda_handler(event, context):
    ''' Entry point for lambda execution '''
    print('event:', '\n', event)
    s3_bucket, s3_key, version_id = _training_config_metadata(event)
    print('bucket:', s3_bucket, 'key:', s3_key, ' version:', version_id)
    instance_type = _get_ec2_instance_type(s3_bucket, s3_key, version_id)
    print('ec2 instance type:', instance_type)
    try:
        queue_id = _schedule_job(s3_bucket, s3_key, version_id, instance_type)
        return {
            'statusCode': 200,
            'body': json.dumps(f'job dispatched with queue id:{queue_id}')
        }
    except Exception as e:
        print(f'exception : {e}')
        return {
            'statusCode': 500,
            'body': json.dumps(f'job dispatch error:{getattr(e, "message", repr(e))}')
        }


def _training_config_metadata(event):
    ''' Get s3 object details from lambda event '''
    s3_object = event['Records'][0]['s3']['object']
    key, version_id = s3_object['key'], s3_object['versionId']
    s3_bucket = event['Records'][0]['s3']['bucket']['name']
    print('bucket:', s3_bucket, 'key:', key, ' version:', version_id)
    return s3_bucket, key, version_id


def _get_ec2_instance_type(s3_bucket, s3_key, version_id):
    instance_type = 'c5.9xlarge'
    try:
        config_path = '/tmp/config.json'
        s3 = boto3.client('s3')
        s3.download_file(s3_bucket, s3_key, config_path, ExtraArgs={'VersionId': version_id})
        with open(config_path, mode='r') as conf_file:
            config = json.load(conf_file)
            instance_type = config.get('ec2_instance_type', instance_type)
    except Exception as e:
        print(f'Exception on config.json download: {e}')
    return instance_type


def _schedule_job(s3_bucket, s3_key, version_id, ec2_instance_type):
    ''' Schedule jenkins trigger job '''
    jenkins_url = JENKINS_URL
    jenkins_user = JENKINS_USER
    user_token = USER_TOKEN
    user_password = USER_PASSWORD
    trigger_job_name = TRIGGER_JOB_NAME
    target_job_build_id = TARGET_JOB_BUILD_ID

    job_parameters = {'releaseUnitBuild': target_job_build_id,
                      'ec2InstanceType': ec2_instance_type,
                      's3Bucket': s3_bucket,
                      's3ObjKey': s3_key,
                      's3ObjVer': version_id}

    server = jenkins.Jenkins(jenkins_url, username=jenkins_user, password=user_password)
    print(f'{server.get_whoami()}')
    queue_id = server.build_job(trigger_job_name, parameters=job_parameters, token=user_token)
    print(f'job dispatched with queue id:{queue_id}')
    print(f'{server.get_queue_info()}')
    return queue_id

