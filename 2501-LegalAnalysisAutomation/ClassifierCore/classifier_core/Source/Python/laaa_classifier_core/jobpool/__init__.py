# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This package provides an API for multiprocessing so that a job can
# be divided amongst workers to take advantage of multiple cores available
# in the system.
#
# For more information refer to http://docs.dask.org/en/latest/scheduling.html

from multiprocessing import Pool, cpu_count

_pool = None


def init_pool():
    global _pool
    _pool = Pool(processes=cpu_count())


def get_pool():
    global _pool
    if not _pool:
        init_pool()
    return _pool
