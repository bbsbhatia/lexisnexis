# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This package provides an API to store instrumentation data related to
# execution of a method call. The implementation uses aws x-ray sdk to inject
# the data into aws x-ray service.
#
# For more information refer to https://github.com/aws/aws-xray-sdk-python

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.ext.flask.middleware import XRayMiddleware
from collections import namedtuple
import os

# Named Tuple to store instrumentation data.
#
# Searchable data is dictionary of key, value pairs and key is used to search
# the instrumentation records in destination repository of such records. These
# records are stored as annotations in x-ray service.
#
# Non-searchable data stores additional instrumentation data as a dictionary
# where key is used to identify the named metadata and value is a dictionary
# of metadata key, value pairs. These records are stored as metadata
# in x-ray service
Record = namedtuple('InstrumentationRecord', ['searchable', 'non_searchable'], defaults=(None, None))


# Class to dump xray records to stdout when environment variable 
# CLASSIFIER_STDOUT is defined as TRUE.
class _StdoutRecorder:

    def put_annotation(self, key, value):
        print(key, value, sep=':')
        
    def put_metadata(self, key, value):
        print(key, value, sep=':')
        
    def current_segment(self):
        return self
    
    def current_subsegment(self):
        return self

    def begin_segment(self, s):
        pass

    def end_segment(self):
        pass

    def begin_subsegment(self, s):
        pass

    def end_subsegment(self):
        pass


_stdout_recorder = _StdoutRecorder()


def configure(app, service_name):
    """
    Configure XRay Middleware for the Flask Service.

    :type app: flask.Flask
    :param app: Flask app
    :type service_name: string
    :param service_name: Service name identifies the originator of
    instrumentation data in aws x-ray
    """
    xray_recorder.configure(sampling=False,
                            service=service_name,
                            plugins=('EC2Plugin',))
    XRayMiddleware(app, xray_recorder)


def record(recorder=None, subsegment=None, segment=None):
    """
    This is intended to be used as a decorator for the method call that
    is being instrumented. This decorator allows for instrumentation data
    handling in one centralized place so that multiple calls to collect
    instrumentation data is avoided. This way, the cross cutting concern of
    instrumenting the method call is delegated to a recorder function rather
    than separate calls to record data proliferated throughout the method.

    :type recorder function(response, options)
    :param recorder: A function that is passed the response object from
    method call and options dictionary that contains the method arguments.
    The function must return an instance of InstrumentationRecord. 
    :type subsegment string
    :param subsegment optional - start a new subsegment to record
    instrumentation data. See xray_recorder.begin_subsegment(...) for
    more details.
    :type segment string
    :param segment optional - start a new segment to record.  This really
    should only be used in initialization functions as it gets populated automatically
    in other scenarios. See xray_recorder.begin_segment(...) for
    more details.
    :return: decorator
    """
    def wrapper_outer(f):

        def wrapper(*arg, **kwargs):
            function_exception = None
            try:
                _begin(segment, subsegment)
                try:
                    result = f(*arg, **kwargs)
                except Exception as error:
                    function_exception = error
                    raise
                if recorder:
                    data = recorder(result, kwargs)
                    if isinstance(data, Record):
                        _record_data(data.searchable, True)
                        _record_data(data.non_searchable, False)
                return result
            except Exception as error:
                _record_error(error)
                if function_exception:
                    raise
            finally:
                _end(segment, subsegment)
        return wrapper

    return wrapper_outer


def record_data(key, value, searchable):
    """
    This method is used to collect instrumentation data. Data is stored in
    current subsegment if there is one activae, otherwise it is stored in
    the segment.

    :type key: string
    :param key: identifier for instrumentation data
    :type value: str, bool or number when searchable is True. When searcable
    is False, an instance of dict
    :param value: instrumentation data
    :type searchable: bool
    :param searchable: whether key for instrumentation data can be
    used to search in aws x-ray
    """
    _assert_type(key, value, searchable)
    dump_stdout = os.environ.get('CLASSIFIER_STDOUT', 'FALSE').upper()
    recorder = _stdout_recorder if dump_stdout == 'TRUE' else xray_recorder
    segment = recorder.current_segment()
    subsegment = recorder.current_subsegment()
    span = subsegment if subsegment is not None else segment
    if searchable:
        span.put_annotation(key, value)
    else:
        span.put_metadata(key, value)


SEARCHABLE_TYPE_MSG = 'searchable value must be a string, number or bool'
NON_SEARCHABLE_TYPE_MSG = 'non-searchable value must be a dictionary'


def _assert_type(key, value, searchable):
    if type(key) != str:
        raise ValueError('key must be a string')
    if searchable:
        if type(value) not in [str, bool, int, float]:
            raise ValueError(SEARCHABLE_TYPE_MSG)
    else:
        if type(value) != dict:
            raise ValueError(NON_SEARCHABLE_TYPE_MSG)


def _record_data(data, searchable):
    if data is None:
        return
    for key, value in data.items():
        record_data(key, value, searchable)


def _record_error(error):
    record_data('instrumentation_error', True, True)
    detail = {'message': error.__repr__()}
    record_data('instrumentation_error_detail', detail, False)


def _begin(segment, subsegment):
    dump_stdout = os.environ.get('CLASSIFIER_STDOUT', 'FALSE').upper()
    recorder = _stdout_recorder if dump_stdout == 'TRUE' else xray_recorder
    if segment is not None:
        recorder.begin_segment(segment)
    if subsegment is not None:
        recorder.begin_subsegment(subsegment)


def _end(segment, subsegment):
    dump_stdout = os.environ.get('CLASSIFIER_STDOUT', 'FALSE').upper()
    recorder = _stdout_recorder if dump_stdout == 'TRUE' else xray_recorder
    if subsegment is not None:
        recorder.end_subsegment()
    if segment is not None:
        recorder.end_segment()
