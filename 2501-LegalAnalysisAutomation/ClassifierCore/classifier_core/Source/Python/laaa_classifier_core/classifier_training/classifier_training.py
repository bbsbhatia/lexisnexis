# Modified by LexisNexis Themis team 2019
# -----------------------------------------------------------------------------
import os
import logging.config
import json
import boto3
import botocore.exceptions
import shutil
import urllib.request
import smtplib
import traceback
import re
import platform
import numpy as np
import pandas as pd
from email.message import EmailMessage
from datetime import datetime
from pkg_resources import get_distribution
from prettytable import PrettyTable

from laaa_classifier_core.classifier_training.ShepardsClassifier import train
from laaa_classifier_core.classifier_training.test_holdout_data import run_classifier_test

logger = logging.getLogger()
model_name = None
config = None
log_dir = None
output_dir = None
start_date = datetime.now()
holdout_results = None


def main(config_path):
    global logger
    global model_name
    global config
    global log_dir
    global output_dir

    if not config_path:
        raise Exception('config_path must be specified')

    config_file = os.path.join(config_path, 'config.json')
    with open(config_file, mode='r') as conf_json:
        config = json.load(conf_json)

    log_dir = os.path.dirname(config['logging']['handlers']['file']['filename'])
    if log_dir and not os.path.exists(log_dir):
        os.makedirs(log_dir)

    logging.config.dictConfig(config['logging'])
    logger = logging.getLogger(__name__)

    # The description to be used in the model folder when uploaded to S3 in the format:
    # YYYYMMDD-<s3_model_description>[-#]
    s3_model_description = config.get('s3_model_description')
    model_name = start_date.strftime('%Y%m%d')
    if s3_model_description:
        s3_model_description = re.sub(r'[<>[\]:"/\\|?*]', '_', s3_model_description)
        model_name = f'{model_name}-{s3_model_description}'

    treatment_phase = config['treatment_phase']
    categories = config['categories']

    # Create a timestamped subdirectory for current session output
    output_dir = os.path.join(os.environ.get('CLASSIFIER_TRAINING_OUTPUT', output_dir),
                              f'{model_name}-{start_date.strftime("%H-%M-%S.%f")}')
    os.makedirs(output_dir)

    # copy the config to the output dir so it will be picked up into S3
    shutil.copy2(src=config_file, dst=output_dir)

    # This is where you put the 9 training CSV files, or they will be downloaded to from S3
    data_dir = os.environ['CLASSIFIER_TRAINING_DATA']

    # CLASSIFIER_MODEL_PATH is the directory containing the lexicon file, whose filename is given as lexicon_file
    # in config.json. This approach is for consistency with the treatment_classifier_service to use the same env
    lexicon_file = os.path.join(os.environ['CLASSIFIER_MODEL_PATH'], config['lexicon_file'])

    # A list of sample count settings to try. Each item in the try list should contain a list of 9 integers, indicating
    # the number of samples to take from each of the 9 training data CSVs, e.g.:
    # [[ 10, 20, 30, 40, 50, 60, 70, 80, 90],
    #  [ 99, 88, 77, 66, 55, 44, 33, 22, 11]]
    try_sample_counts = config['try_sample_counts']

    # The S3 bucket used for feature CSV download and model upload
    s3_bucket = os.environ.get('S3_BUCKET')

    # Determines whether to upload the models to S3 after training
    upload_to_s3 = config['upload_to_s3']

    # Determines whether to download training data feature CSVs from the S3 bucket
    download_s3_features = config['download_s3_features']

    # The name of the feature set to download from <s3_bucket>/training/features/<s3_features>
    s3_features = config.get('s3_features', 'current')

    # Determines whether to overwrite existing feature CSVs with those downloaded from S3
    overwrite_features = config.get('overwrite_features', False)

    # The filename of the CSV file containing holdout data in the CLASSIFIER_TRAINING_DATA directory
    # If not set, holdout tests will not be run
    holdout_data_filename = config.get('holdout_data_filename')

    logger.info('Starting training from:')
    logger.info(f'{__file__}')
    logger.info(f'Core: laaa-classifier-core=={get_distribution("laaa-classifier-core").version}')
    logger.info('sample counts:')
    for try_sample_count in try_sample_counts:
        logger.info(try_sample_count)
    logger.info(f'treatment_phase={treatment_phase}')
    logger.info(f'categories={categories}')
    logger.info(f'pos_label={config["pos_label"]}')
    logger.info(f'vectorizer={config["vectorizer"]}')
    logger.info(f'feature_selector={config["feature_selector"]}')
    logger.info(f'scaler={config["scaler"]}')
    logger.info(f'num_folds={config["num_folds"]}')
    logger.info(f'num_iterations={config["num_iterations"]}')
    logger.info(f'threshold_score={config["threshold_score"]}')
    logger.info(f'threshold_train_score={config["threshold_train_score"]}')
    logger.info(f'threshold_f1_macro={config["threshold_f1_macro"]}')
    logger.info(f'xgb_param_dist={config["xgb_param_dist"]}')
    logger.info(f'scorer={config["scorer"]}')
    logger.info(f'lexicon_file={lexicon_file}')
    logger.info(f'output_dir={output_dir}')
    logger.info(f'data_dir={data_dir}')
    logger.info(f'upload_to_s3={upload_to_s3}')
    logger.info(f'download_s3_features={download_s3_features}')
    logger.info(f's3_features={s3_features}')
    datafiles = config['datafiles']
    logger.info(f'sampled feature CSV file names =datafiles={datafiles}')
    feature_names = config['feature_names']
    logger.info(f'feature names={feature_names}')
    text_columns = config['text_columns']
    logger.info(f'text columns={text_columns}')
    key_column = config['key_column']
    logger.info(f'key_column={key_column}')
    logger.info(f'Python Version:{platform.python_version()}')
    logger.info(f'Pandas Version:{pd.__version__}')
    logger.info(f'Numpy Version:{np.__version__}')
    logger.info(f'Notification Email={config["notification_email"]}')

    if download_s3_features:
        download_features_from_s3(bucket=s3_bucket,
                                  feature_set=s3_features,
                                  path=data_dir,
                                  overwrite=overwrite_features)
    output_row_count_for_datafiles(data_dir, datafiles)
    # copy the lexicon to the output dir, it will also be picked up into S3
    shutil.copy2(src=lexicon_file, dst=output_dir)

    output_lexicon_tracing(lexicon_file)
    if holdout_data_filename:
        output_holdout_tracing(os.path.join(data_dir, holdout_data_filename), key_column)

    for try_sample_count in try_sample_counts:
        logger.info(f'###### Starting Training for sample counts: {try_sample_count}  ######')
        train(lexicon_file=lexicon_file,
              sample_counts=try_sample_count,
              models_dir=output_dir,
              data_dir=data_dir,
              config=config)

    if holdout_data_filename:
        holdout_data_path = os.path.join(data_dir, holdout_data_filename)
        global holdout_results
        holdout_results = run_classifier_test(output_dir, holdout_data_path, lexicon_file, config=config)

    logger.info('Training complete')

def output_row_count_for_datafiles(data_dir, datafiles):
    table = PrettyTable(['Data File', 'Row Count'])
    for file in datafiles:
        df = pd.read_csv(os.path.join(data_dir, file))
        row_count = len(df.index)
        table.add_row([file, row_count])
    logger.info(f'\nData Files and their counts:\n{table}')


def output_lexicon_tracing(lexicon_file):
    logger.info(f'Lexicon file: {lexicon_file}')
    formatted_date = datetime.fromtimestamp(os.path.getmtime(lexicon_file)).strftime('%m/%d/%Y %H:%M:%S')
    logger.info(f'Lexicon date/time: {formatted_date}')
    if lexicon_file.lower().endswith("xls") or lexicon_file.lower().endswith("xlsx"):
        df = pd.read_excel(lexicon_file)
    else:
        df = pd.read_csv(lexicon_file)
    row_count = len(df.index)
    logger.info(f'Lexicon row count: {row_count}')
    logger.info('Lexicon Data first 100 rows \n{}'.format(df.head(100)))

def output_holdout_tracing(holdout_data_path, key_column):
    logger.info(f'Holdout/Validation file: {holdout_data_path}')
    formatted_date = datetime.fromtimestamp(os.path.getmtime(holdout_data_path)).strftime('%m/%d/%Y %H:%M:%S')
    logger.info(f'Holdout/Validation date/time: {formatted_date}')
    df = pd.read_csv(holdout_data_path)
    logger.info(f'Holdout/Validation columns: {df.columns}')
    row_count = len(df.index)
    logger.info(f'Holdout/Validation row count: {row_count}')
    in_class = len(df.loc[df[key_column] == 0])
    logger.info(f'Holdout/Validation In class Row Count: {in_class}')

def download_features_from_s3(bucket, feature_set, path, overwrite=False):
    """
    Downloads feature CSV training data from the given bucket into the given path
    :param bucket: S3 bucket name
    :param feature_set: name of feature set
    :param path: location to save files
    :param overwrite: option to overwrite existing files, otherwise download is skipped
    """
    logger.info('Downloading features from S3')
    logger.info(f'feature_set={feature_set} path={path}')
    try:
        if not os.path.exists(path):
            os.makedirs(path)

        session = boto3.session.Session()
        s3 = session.resource('s3')
        bucket = s3.Bucket(bucket)

        for obj in bucket.objects.filter(Prefix=f'training/features/{feature_set}'):
            if not obj.key.endswith('/'):
                filename = os.path.join(path, os.path.basename(obj.key))
                logger.debug(filename)
                if os.path.exists(filename) and not overwrite:
                    logger.warning(
                        f'{filename} already exists. Skipping. Use "overwrite_features": true to force download')
                else:
                    bucket.download_file(Key=obj.key, Filename=filename)

    except botocore.exceptions.ClientError:
        logger.exception('Error downloading features from S3:')
    except IOError:
        logger.exception('IO error while saving downloaded features from S3:')


def upload_output_to_s3(src, bucket):
    """
    Uploads all the files from the src directory, then moves them to dst
    Format will be <model_name>[-<s3_model_description>][-#]
    -2, -3 etc is automatically appended if the model name and description already exists in S3
    :param src: location of files to be uploaded
    :param bucket: S3 bucket to upload to
    :returns URL to the S3 bucket in AWS Console
    """
    logger.info('Copying output to S3')
    try:
        session = boto3.session.Session()
        client = session.client('s3')
        model_number = 1
        s3_model_name = model_name
        response = client.list_objects(Bucket=bucket, Prefix=f'model/{model_name}', Delimiter='/')
        existing_models = [prefix['Prefix'] for prefix in
                           response.get('CommonPrefixes', [])]
        while f'model/{s3_model_name}/' in existing_models:
            logger.debug(f'Found existing model {s3_model_name}')
            model_number += 1
            s3_model_name = f'{model_name}-{model_number}'

        for root, dirs, files in os.walk(src):
            for file in files:
                path = os.path.join(root, file)
                logger.debug(f'path={path}')
                key = os.path.relpath(path, start=src).replace('\\', '/')
                key = f'model/{s3_model_name}/{key}'
                logger.debug(f'key={key}')
                client.upload_file(Filename=path, Bucket=bucket, Key=key)

        return f'https://s3.console.aws.amazon.com/s3/buckets/{bucket}/model/{s3_model_name}/'

    except Exception as e:
        logger.exception('Error copying output to S3:')
        raise e


def get_ec2_instance_identity():
    """
    Returns the AWS EC2 instance identity document
    See https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-identity-documents.html
    :return: the EC2 instance identity document if we are on EC2, otherwise None
    """
    try:
        with urllib.request.urlopen('http://169.254.169.254/latest/dynamic/instance-identity/document',
                                    timeout=10) as r:
            return json.load(r)
    except Exception as e:
        logger.warning(f'Could not get EC2 Instance Identity Document - may not be running on EC2 - {e}')
        return None


def get_cloud_formation_stack_name(instance_identity):
    """
    Gets the stack name our EC2 instance is part of
    """
    region = instance_identity['region']
    instance_id = instance_identity['instanceId']
    instance = boto3.resource('ec2', region_name=region).Instance(instance_id)
    for tag in instance.tags:
        if tag["Key"] == 'aws:cloudformation:stack-name':
            return tag["Value"]
    return None


def delete_stack(instance_identity):
    """
    Deletes the CloudFormation stack our EC2 instance is part of
    """
    region = instance_identity['region']
    stack_name = get_cloud_formation_stack_name(instance_identity)
    if stack_name:
        boto3.resource('cloudformation', region_name=region).Stack(stack_name).delete()


def send_success_notification(training_time, bucket_url):
    message = f'Training completed in {training_time}'

    if bucket_url:
        message += f'\n\n<a href="{bucket_url}">S3 Bucket</a>' \
            f'\nYou must be <a href="{config.get("aws_login_url")}">signed in to AWS</a> first'

    if holdout_results:
        for model, metrics_df, _ in holdout_results:
            message += f'\n\n<a href="{bucket_url}{model}/">{model}</a>' if bucket_url else f'\n\n{model}'
            for metric in metrics_df:
                message += f'\n\t{metric}: {metrics_df[metric].iloc[0]}'

    send_notification(to=config.get('notification_email'),
                      status='Success',
                      message=message)


def send_notification(to, status, message=''):
    hosts = config['smtp']['hosts']
    attempts = config['smtp'].get('attempts_per_host', 3)
    timeout = config['smtp'].get('timeout', 10)
    sender = config['smtp']['sender']
    content = f'Training {model_name} completed with status: {status}'

    ec2_instance_identity = get_ec2_instance_identity()
    if ec2_instance_identity:
        stack_name = get_cloud_formation_stack_name(ec2_instance_identity)
        cf_url = f'https://console.aws.amazon.com/cloudformation/home?#/stack/detail?stackId={stack_name}'
        instance_id = ec2_instance_identity["instanceId"]
        ec2_url = f'https://console.aws.amazon.com/ec2/v2/home?#Instances:instanceId={instance_id}'
        content += f' on instance type {ec2_instance_identity["instanceType"]}' \
                   f' @ {ec2_instance_identity["privateIp"]}' \
                   f'\n\nStack: <a href="{cf_url}">{stack_name}</a> ' \
                   f'(Delete the stack rather than terminating the instance)' \
                   f'\nInstance: <a href="{ec2_url}">{instance_id}</a>'

    if message:
        content += f'\n\n{message}'
    msg = EmailMessage()
    content = content.replace('\n', '\n<br/>').replace('\t', '&emsp;')
    msg.add_alternative(content, subtype='html')
    msg['From'] = sender
    msg['To'] = to
    msg['Subject'] = f'{model_name}: {status}'
    for attempt in range(1, attempts + 1):
        for host in hosts:
            try:
                with smtplib.SMTP(host=host, timeout=timeout) as smtp:
                    smtp.send_message(msg)
                return
            except Exception as e:
                logger.exception(f'Sending email failed - {e} - (attempt {attempt} of {attempts} on {host}):')


def train_classifier(config_path_override):
    try:
        main(config_path_override)
    except Exception as e:
        logger.exception(f'Unhandled exception {e} during training:')
        notification_email = config.get('notification_email') if config else None
        if notification_email:
            send_notification(to=notification_email,
                              status='Failure',
                              message=''.join(traceback.format_exception(etype=None, value=e, tb=e.__traceback__)))
        exit(1)
    finally:
        if log_dir and output_dir:
            for log in os.listdir(log_dir):
                shutil.copy2(src=os.path.join(log_dir, log), dst=output_dir)

        bucket_url = None
        if config and config.get('upload_to_s3') and output_dir:
            bucket_url = upload_output_to_s3(src=output_dir,
                                             bucket=os.environ.get('S3_BUCKET'))

        if config and config.get('terminate_ec2_on_finish'):
            ec2_instance_identity = get_ec2_instance_identity()
            if ec2_instance_identity:
                delete_stack(ec2_instance_identity)

    if config.get('notification_email'):
        training_time = datetime.now() - start_date
        send_success_notification(training_time, bucket_url)


if __name__ == "__main__":
    train_classifier(os.path.dirname(__file__))
