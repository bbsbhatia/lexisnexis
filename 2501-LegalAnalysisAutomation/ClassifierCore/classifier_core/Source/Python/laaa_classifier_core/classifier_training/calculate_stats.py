import numpy as np
import pandas as pd

def calculate_stats(df, key_column, sort = list()):
    pd.options.mode.use_inf_as_na = True #Set this true to calculate empty strings as null in the isnull check
    row_count = len(df.index)
    lni_count = len(df['lni'].unique())
    key_column_percentage = ((row_count - len(df.loc[df[key_column] == 0]))/row_count) * 100
    df_file_stats = pd.DataFrame(columns=('Row Count', 'LNI Count', 'Key Column', 'Key Column Percentage'))
    df_file_stats.loc[0] = [row_count, lni_count, key_column, str(key_column_percentage) + '%']
    df_numeric_stats = pd.DataFrame(columns=('Column', 'Count of Zero Values', 'Count of Non-Zero Values', 'Max Value', 'Min Value', 'Avg Value', 'Sum of Values'))
    df_string_stats = pd.DataFrame(columns=('Column', 'Count of Empty Strings', 'Count of Non-Empty Strings', 'Max Length', 'Min Length', 'Avg Len'))
    for col in sort_list(df.columns.tolist(), sort):
        if (df[col].dtype == np.float64 or df[col].dtype == np.int64):
            count_zeros = len(df.loc[df[col] == 0])
            count_non_zeros = row_count - count_zeros
            avg = df[col].mean()
            max = df[col].max()
            min = df[col].min()
            sum = df[col].sum()
            df_numeric_stats.loc[len(df_numeric_stats)]=[col, count_zeros, count_non_zeros, max, min, avg, sum]
        else:
            count_zeros = len(df.loc[df[col] == 0]) + len(df.loc[df[col] == '0']) #Adding for when the trainer changes empty strings to 0
            count_empty_strings = df[col].isnull().sum() + count_zeros
            count_non_empty = row_count - count_empty_strings
            col_len = col+'len'
            df[col_len] = df[col].astype(str).map(len)
            max_length = df[col_len].max()
            min_length = df[col_len].min()
            avg_length = df[col_len].mean()
            df_string_stats.loc[len(df_string_stats)]=[col, count_empty_strings, count_non_empty, max_length, min_length, avg_length]
    return df_file_stats, df_numeric_stats, df_string_stats
"""
This sorts a list by a list of preferred sort list.  
Not all of the values in the list you are sorting have to appear in the sort list. Allowing you to have your most desired items show first without specifying the others.
Any values in the sort that don't appear in the list are ignored.
"""
def sort_list(list_to_sort, sort):
    intersection = sorted(set(sort).intersection(set(list_to_sort)), key = sort.index)
    remaining_values = list(set(list_to_sort).difference(set(sort)))
    return intersection + remaining_values

def write_stats_to_xlsx(file_location, df_file_stats, df_numeric_stats, df_string_stats):
    with pd.ExcelWriter(file_location) as writer:
        df_file_stats.to_excel(writer, sheet_name='File Statistics', index = None, header = True)
        df_numeric_stats.to_excel(writer, sheet_name='Numeric Statistics', index = None, header = True)
        df_string_stats.to_excel(writer, sheet_name='String Statistics', index = None, header = True)

def read_csv_and_write_stats_to_xlsx(input_csv, output_xlsx, nrows, key_column, sort):
    df = pd.read_csv(input_csv, sep=',', nrows=nrows, index_col=None)
    read_df_and_write_stats_to_xlsx(df, output_xlsx, key_column, sort)

def read_df_and_write_stats_to_xlsx(df, output_xlsx, key_column, sort):
    df_file_stats, df_numeric_stats, df_string_stats = calculate_stats(df, key_column, sort)
    write_stats_to_xlsx(output_xlsx, df_file_stats, df_numeric_stats, df_string_stats)

if __name__ == "__main__":
    sort = list(['lni','pnum','ptot', 'courtlevel','year', 'negct', 'len', 'textlen', 'last-head','encparty1', 'encparty2', 'text', 'ctext', 'letters'])
    read_csv_and_write_stats_to_xlsx('C:/Themis/NegativeTreatment/Holdout/validation_small.csv', 'C:/Themis/NegativeTreatment/Holdout/statistics.xlsx', None, 'negct', sort)
