# Modified by LexisNexis Themis team 2019
# -----------------------------------------------------------------------------
import os
import re
from traceback import format_exception

import psutil
from functools import partial

import numpy as np
import pandas as pd
from sklearn import metrics
from multiprocessing import cpu_count, Pool
import logging
#from pandas_ml import ConfusionMatrix

from laaa_classifier_core.classifier_training import training_data_reader
from laaa_classifier_core.classifier import GenFeatures, ClassifierHelper


# Function for string concatenation on dataframe column
def my_concat(values):
    values_str = [str(i) for i in values]
    values_str_x = []
    for i in values_str:
        if len(i) > 0 and i != 'nan':
            values_str_x.append(i)
    return '|'.join(values_str_x)


def get_doc_level_df(df):
    lni_df = df.groupby(['lni']).size().reset_index(name='parasct')

    shep_letters_df = df.groupby(['lni'])['letters'].agg(my_concat).reset_index(name='ShepLetters')
    lni_df['ShepLetters'] = shep_letters_df['ShepLetters']

    # Has Treatment Doc Rollup
    df['IsShepTreat'] = np.where((df['key_treated'] == 0), 1, 0)
    df['IsShepNoTreat'] = np.where((df['key_treated'] == 0), 0, 1)
    df['IsClassTreat'] = np.where((df['outcome_treated'] == 0), 1, 0)
    df['IsClassNoTreat'] = np.where((df['outcome_treated'] == 0), 0, 1)

    shep_treat_df = df.groupby(['lni'])['IsShepTreat'].sum().reset_index(name='ShepTreatCt')
    class_treat_df = df.groupby(['lni'])['IsClassTreat'].sum().reset_index(name='ClassTreatCt')

    lni_df['ShepTreatCt'] = shep_treat_df['ShepTreatCt']
    lni_df['ClassTreatCt'] = class_treat_df['ClassTreatCt']
    lni_df['ShepTreatDoc'] = np.where((lni_df['ShepTreatCt'] > 0), 1, 0)
    lni_df['ShepNoTreatDoc'] = np.where((lni_df['ShepTreatCt'] == 0), 1, 0)
    lni_df['ClassTreatDoc'] = np.where((lni_df['ClassTreatCt'] > 0), 1, 0)
    lni_df['ClassNoTreatDoc'] = np.where((lni_df['ClassTreatCt'] == 0), 1, 0)
    lni_df['true_pos_no_treat'] = np.where(((lni_df['ClassTreatDoc'] == 0) & (lni_df['ShepTreatDoc'] == 0)), 1, 0)

    # Check that the LNIs are in the same order - I've never seen this fail
    _lni_lni = lni_df['lni']
    _shep_treat_lni = shep_treat_df['lni']
    _class_treat_lni = class_treat_df['lni']

    logger = logging.getLogger(__name__)
    for i in range(len(_lni_lni)):
        if _lni_lni[i] != _shep_treat_lni[i]:
            logger.error('ERROR: _lni_lni[i] != _shep_treat_lni[i]')
        if _lni_lni[i] != _class_treat_lni[i]:
            logger.error('ERROR: _lni_lni[i] != _class_treat_lni[i]')
        if _shep_treat_lni[i] != _class_treat_lni[i]:
            logger.error('ERROR: _shep_treat_lni[i] != _class_treat_lni[i]')
    return lni_df


def doc_level_status_treated(df, doc_validation_file, doc_p_error_file, doc_r_error_file):
    report = ['::::::::::::::::::::::doc_level_error_reports::::::::::::::::::::::::::', f'Doc validation_file:{doc_validation_file}',
              f'Doc p_error_file:{doc_p_error_file}', f'Doc r_error_file:{doc_r_error_file}']

    lni_df = get_doc_level_df(df)

    num_docs = lni_df.shape[0]
    true_pos_no_treat = lni_df['true_pos_no_treat'].sum()
    tagged_pos_no_treat = lni_df['ClassNoTreatDoc'].sum()
    shep_pos_no_treat = lni_df['ShepNoTreatDoc'].sum()
    doc_precision_no_treat = true_pos_no_treat / tagged_pos_no_treat
    doc_recall_no_treat = true_pos_no_treat / shep_pos_no_treat

    report.append('::::::::::::::::::::::doc_level_stats::::::::::::::::::::::::::')
    report.append(
        f'  num_docs={num_docs} ; true_pos_no_treat={true_pos_no_treat} ; tagged_pos_no_treat={tagged_pos_no_treat} ; '
        f'shep_pos_no_treat={shep_pos_no_treat}')
    report.append(f'doc_precision_no_treat={doc_precision_no_treat}')
    report.append(f'doc_recall_no_treat={doc_recall_no_treat}')

    if len(doc_validation_file) > 0:
        lni_df.to_csv(doc_validation_file, sep=',', index=False)

    lni_df['e_type'] = lni_df['ShepNoTreatDoc'] - lni_df['ClassNoTreatDoc']
    df_r_error = lni_df[lni_df['e_type'] == 1]
    df_p_error = lni_df[lni_df['e_type'] == -1]
    df_r_error = df_r_error.loc[:, ['lni', 'ShepLetters', 'ShepNoTreatDoc', 'ClassNoTreatDoc']]
    df_p_error = df_p_error.loc[:, ['lni', 'ShepLetters', 'ShepNoTreatDoc', 'ClassNoTreatDoc']]

    if len(doc_p_error_file) > 0:
        df_p_error.to_csv(doc_p_error_file, sep=',', index=False)
    if len(doc_r_error_file) > 0:
        df_r_error.to_csv(doc_r_error_file, sep=',', index=False)

    return report


def gen_model_parts(path, model, config):
    logger = logging.getLogger(__name__)
    logger.debug(':::::::::::::::::::::::::::gen_model_parts::::::::::::::::::::::::::::')
    logger.debug('path=' + path)
    logger.debug('model=' + model)
    sc_pattern = r'^.+?_([\d_]+)_.+$'
    sc_compiled = re.compile(sc_pattern, re.M)
    sc_replace = r'\1'
    sc_str = re.sub(sc_compiled, sc_replace, model)
    logger.debug('sc_str=' + sc_str)
    model_dir = os.path.join(path, model)
    class_pickle = os.path.join(model_dir, 'model.pkl')
    treatment_phase = config['treatment_phase']
    vect_pickle = os.path.join(path, 'vectorizer_' + treatment_phase + '_' + sc_str + '.pickle')
    feat_sel_pickle = os.path.join(path, 'feature_selector_' + treatment_phase + '_' + sc_str + '.pickle')
    if not re.match(sc_compiled, model) or not os.path.isfile(class_pickle):
        return None
    logger.debug('class_pickle=' + class_pickle)
    logger.debug('vect_pickle=' + vect_pickle)
    logger.debug('feat_sel_pickle=' + feat_sel_pickle)

    return [model_dir, class_pickle, vect_pickle, feat_sel_pickle]


def get_models_list(models_dir, config):
    models_list = []

    names_list = [child for child in os.listdir(models_dir) if
                  os.path.isdir(os.path.join(models_dir, child))]

    for name in names_list:
        parts = gen_model_parts(models_dir, name, config)
        if parts:
            models_list.append(parts)

    return models_list


def go_report_treated_loop(df, config, text_columns_tr, other_feature_names_tr, holdout_data_path, model_dir,
                           classifier_helper, para_validation_and_error_reports):
    model_name = os.path.basename(model_dir)
    report_path = os.path.join(model_dir, 'report.txt')
    report = [model_name]
    metrics_df = pd.DataFrame()
    try:
        holdout_data_filename = os.path.basename(holdout_data_path)
        validation_file = os.path.join(model_dir,
                                       'para_validation_' + holdout_data_filename)
        p_error_file = os.path.join(model_dir, 'para_p_error_' + holdout_data_filename)
        r_error_file = os.path.join(model_dir, 'para_r_error_' + holdout_data_filename)
        doc_validation_file = os.path.join(model_dir,
                                           'doc_validation_' + holdout_data_filename)
        doc_p_error_file = os.path.join(model_dir, 'doc_p_error_' + holdout_data_filename)
        doc_r_error_file = os.path.join(model_dir, 'doc_r_error_' + holdout_data_filename)
        metrics_file = os.path.join(model_dir, 'metrics.csv')

        # Run Treated
        outcome_treated = classifier_helper.classify_treated(df, text_columns_tr, other_feature_names_tr)

        key_treated = np.where((df[config['key_column']] > 0), 0, 1)

        report.append('Classifier Metrics Reports:\n')
        report.append('Paragraph Level Metrics Report:')
        report.append(str(metrics.confusion_matrix(key_treated, outcome_treated)))
        #report.append(ConfusionMatrix(key_treated, outcome_treated))
        report.append(str(metrics.classification_report(key_treated, outcome_treated, digits=4)))
        df['key_treated'] = key_treated
        df['outcome_treated'] = outcome_treated
        lni_df = get_doc_level_df(df)
        
        report.append('Document Level In Class Report:')
        report.append(str(metrics.confusion_matrix(lni_df['ShepTreatDoc'], lni_df['ClassTreatDoc'])))
        #report.append(ConfusionMatrix(lni_df['ShepTreatDoc'], lni_df['ClassTreatDoc']))
        report.append(str(metrics.classification_report(lni_df['ShepTreatDoc'], lni_df['ClassTreatDoc'], digits=4)))
        actual_in_doc_percentage = (lni_df['ShepTreatDoc'].sum() / lni_df['ShepTreatDoc'].size) * 100
        classifier_in_doc_percentage = (lni_df['ClassTreatDoc'].sum() / lni_df['ClassTreatDoc'].size) * 100
        report.append(f'Actual in class doc percentage: {actual_in_doc_percentage}%')
        report.append(f'Classifier in class doc percentage: {classifier_in_doc_percentage}%')

        report.append('\nDocument Level Out Class Report:')
        report.append(str(metrics.confusion_matrix(lni_df['ShepNoTreatDoc'], lni_df['ClassTreatDoc'])))
        #report.append(ConfusionMatrix(lni_df['ShepNoTreatDoc'], lni_df['ClassTreatDoc']))
        report.append(str(metrics.classification_report(lni_df['ClassNoTreatDoc'], lni_df['ClassNoTreatDoc'], digits=4)))
        actual_out_doc_percentage = (lni_df['ShepNoTreatDoc'].sum() / lni_df['ShepNoTreatDoc'].size) * 100
        classifier_out_doc_percentage = (lni_df['ClassNoTreatDoc'].sum() / lni_df['ClassNoTreatDoc'].size) * 100
        report.append(f'Actual out class doc percentage: {actual_out_doc_percentage}%')
        report.append(f'Classifier out class doc percentage: {classifier_out_doc_percentage}%')


        # Doc Level Stats
        df_roll_treated = df.loc[:, ['lni', 'letters']]
        df_roll_treated['key_treated'] = key_treated
        df_roll_treated['outcome_treated'] = outcome_treated
        report.extend(
            doc_level_status_treated(df_roll_treated, doc_validation_file, doc_p_error_file, doc_r_error_file)
        )

        # Write out validation files and error reports
        treated_etype = key_treated - outcome_treated

        if para_validation_and_error_reports:
            df_r_error = df[treated_etype == -1]
            df_p_error = df[treated_etype == 1]
            df_r_error = df_r_error.loc[:, ['text', 'letters', 'key_treated', 'outcome_treated']]
            df_p_error = df_p_error.loc[:, ['text', 'letters', 'key_treated', 'outcome_treated']]

            df.to_csv(validation_file, sep=',', index=False)
            df_r_error.to_csv(r_error_file, sep=',', index=False)
            df_p_error.to_csv(p_error_file, sep=',', index=False)

        # Count the number and percentage of paragraphs highlighted
        count_highlighted = np.where((outcome_treated == 0), 1, 0)
        count_paras = np.where((outcome_treated == 0), 1, 1)
        num_paras = count_paras.sum()
        num_highlighted = count_highlighted.sum()
        pct_highlighted = num_highlighted / num_paras * 100

        report.append('::::::::::::::::::::::para_level_stats::::::::::::::::::::::::::')
        report.append('num_paras = ' + str(num_paras))
        report.append('para_num_highlighted = ' + str(num_highlighted))
        report.append('para_percent_highlighted = ' + str(pct_highlighted))

        pos_label = config['pos_label']
        metrics_df['para precision'] = [metrics.precision_score(key_treated, outcome_treated, pos_label=pos_label)]
        metrics_df['para recall'] = [metrics.recall_score(key_treated, outcome_treated, pos_label=pos_label)]
        metrics_df['para accuracy'] = [metrics.accuracy_score(key_treated, outcome_treated)]
        metrics_df['para f1-score'] = [metrics.f1_score(key_treated, outcome_treated, pos_label=pos_label)]
        metrics_df['para percent_highlighted'] = [pct_highlighted]
        metrics_df['in class doc precision'] = [metrics.precision_score(lni_df['ShepTreatDoc'], lni_df['ClassTreatDoc'])]
        metrics_df['in class doc recall'] = [metrics.recall_score(lni_df['ShepTreatDoc'], lni_df['ClassTreatDoc'])]
        metrics_df['Actual in class doc percentage'] = [actual_in_doc_percentage]
        metrics_df['Classifier in class doc percentage'] = [classifier_in_doc_percentage]
        metrics_df['out class doc precision'] = [metrics.precision_score(lni_df['ShepNoTreatDoc'], lni_df['ClassNoTreatDoc'])]
        metrics_df['out class doc recall'] = [metrics.recall_score(lni_df['ShepNoTreatDoc'], lni_df['ClassNoTreatDoc'])]
        metrics_df['Actual out class doc percentage'] = [actual_out_doc_percentage]
        metrics_df['Classifier out class doc percentage'] = [classifier_out_doc_percentage]
        metrics_df.to_csv(metrics_file, sep=',', index=False)

    except Exception as e:
        report.append(f'Exception {e} while running result report for {model_name}')
        report.append(''.join(format_exception(etype=None, value=e, tb=e.__traceback__)))

    report_text = '\n################### end report ##############################\n\n'.join(report)
    try:
        with open(report_path, mode='w', encoding='utf-8') as report_file:
            report_file.write(report_text)
    except IOError:
        pass

    return model_name, metrics_df, report_text


def go_report_treated_feature(df, validation_file, p_error_file, r_error_file, feat_name, config):
    logger = logging.getLogger(__name__)
    logger.info('\n\:::::::::::::::::::::::ngo_report_treated_feature:::::::::::::::::::::::')
    logger.info('feat_name:' + feat_name)
    logger.debug('validation_file:' + validation_file)
    logger.debug('p_error_file:' + p_error_file)
    logger.debug('r_error_file:' + r_error_file)

    # Get test data with generated features
    # df, text_columns, other_feature_names = shep_data_reader.read_testdata_treated(csv_file)

    lexindex_name = 'lex_outcome_' + feat_name
    df['key_treated'] = np.where((df[config['key_column']] > 0), 0, 1)
    df[lexindex_name] = np.where((df[feat_name] > 0), 0, 1)

    key_treated = df['key_treated']
    outcome_treated = df[lexindex_name]

    logger.info('\nClassifier Metrics Report: Feature Name (' + feat_name + ')')
    logger.info('\nConfusion matrix:\n' + metrics.confusion_matrix(key_treated, outcome_treated))
    logger.info('\nMetrics table:\n' + metrics.classification_report(key_treated, outcome_treated, digits=4))

    # Write out validation files and error reports
    df['Treated_etype'] = df['key_treated'] - df[lexindex_name]

    df_r_error = df[df['Treated_etype'] == -1]
    df_p_error = df[df['Treated_etype'] == 1]
    df_r_error = df_r_error.ix[:, ['ctext', 'letters', 'key_treated', lexindex_name, feat_name]]
    df_p_error = df_p_error.ix[:, ['ctext', 'letters', 'key_treated', lexindex_name, feat_name]]

    df.to_csv(validation_file, sep=',', index=False)
    df_r_error.to_csv(r_error_file, sep=',', index=False)
    df_p_error.to_csv(p_error_file, sep=',', index=False)

    # Count the number and percentage of paragraphs highlighted
    df['countHighlighted'] = np.where((df[lexindex_name] == 0), 1, 0)
    df['countParas'] = np.where((df[lexindex_name] == 0), 1, 1)
    num_paras = df['countParas'].sum()
    num_highlighted = df['countHighlighted'].sum()
    pct_highlighted = num_highlighted / num_paras * 100

    logger.info('num_paras = ' + str(num_paras))
    logger.info('num_highlightedparas = ' + str(num_highlighted))
    logger.info('percent_highlighted = ' + str(pct_highlighted))


def run_feature_test(holdout_data_path, lexicon_file, config):
    holdout_data_filename = os.path.basename(holdout_data_path)
    feat_names = ['lextrtx_cnt',
                  'lexfollow_cnt',
                  'lexbbs_cnt',
                  'lextrt_cnt',
                  'lexlistlong_cnt',
                  'lexlist_cnt',
                  'lexneg_cnt',
                  'lexmisc_factor_cnt',
                  'lexcoref_cnt',
                  'lexd_cnt',
                  'lex_cite_struct_cnt']
    df, text_columns, other_feature_names = \
        training_data_reader.read_testdata_treated(holdout_data_path,
                                                   apply_lexicon_treated=True,
                                                   output_lexicon_treated_matches=True,
                                                   config=config,
                                                   treatment_language_file=lexicon_file)

    for feat_name in feat_names:
        validation_file = os.path.join(v_path, 'feat_' + feat_name + '_validation_' + holdout_data_filename)
        p_error_file = os.path.join(v_path, 'feat_' + feat_name + '_p_error_' + holdout_data_filename)
        r_error_file = os.path.join(v_path, 'feat_' + feat_name + '_r_error_' + holdout_data_filename)

        go_report_treated_feature(df, validation_file, p_error_file, r_error_file, feat_name, config=config)


holdout_data = None


def initializer(data):
    global holdout_data
    holdout_data = data


def run_classifier_test(models_dir, holdout_data_path, lexicon_file, config, para_validation_and_error_reports=False):
    logger = logging.getLogger(__name__)

    # each process may use up to 28GB
    max_processes = psutil.virtual_memory().available // (28 * 1024 ** 3)
    processes = min(max_processes, cpu_count())
    if processes < 1:
        processes = 1

    df, text_columns_tr, other_feature_names_tr = \
        training_data_reader.read_testdata_treated(holdout_data_path, treatment_language_file=lexicon_file,config=config)
    df = df[df['courtlevel'].notna()]

    models_list = get_models_list(models_dir, config)
    logger.debug(f'models_list={models_list}')
    func = partial(run_classifier_test_process,
                   holdout_data_path, lexicon_file, text_columns_tr, other_feature_names_tr,
                   para_validation_and_error_reports, config=config)
    result = Pool(processes=processes, initializer=initializer, initargs=(df,)).map(func, models_list)
    for _, _, report in result:
        logger.info(report)
    return result


def run_classifier_test_process(holdout_data_path, lexicon_file, text_columns_tr, other_feature_names_tr,
                                para_validation_and_error_reports, model_info, config):
    model_dir, model_pickle_path, vect_pickle_path, feat_sel_pickle_path = model_info
    print('\nInitialize classifier service Treated:')
    print('Class Pickle Treated: ' + model_pickle_path)
    print('Vectorizer Pickle Treated: ' + vect_pickle_path)
    print('Feature Selection Pickle Treated: ' + feat_sel_pickle_path)
    model_pickle = ClassifierHelper.load_pickle_file(model_pickle_path, use_job_lib=True)
    vect_pickle = ClassifierHelper.load_pickle_file(vect_pickle_path)
    feat_sel_pickle = ClassifierHelper.load_pickle_file(feat_sel_pickle_path)
    my_gf = GenFeatures(treatment_language_file=lexicon_file,
                        DebugLexicon=True)
    classifier_helper = ClassifierHelper(myGF=my_gf,
                                         classifier_pickle=model_pickle,
                                         feature_vectorizer_pickle=vect_pickle,
                                         feature_selector_pickle=feat_sel_pickle)
    return go_report_treated_loop(df=holdout_data,
                                  text_columns_tr=text_columns_tr,
                                  other_feature_names_tr=other_feature_names_tr,
                                  holdout_data_path=holdout_data_path,
                                  classifier_helper=classifier_helper,
                                  model_dir=model_dir,
                                  para_validation_and_error_reports=para_validation_and_error_reports,
                                  config=config)


if __name__ == "__main__":

    models_dir = os.environ['CLASSIFIER_TRAINING_OUTPUT']
    Holdout_data_csv = os.environ['CLASSIFIER_HOLDOUT_FILE']
    Holdout_data_fname = os.path.basename(Holdout_data_csv)
    v_path = os.environ['CLASSIFIER_TRAINING_OUTPUT']
    lexicon_file = os.path.join(os.environ['CLASSIFIER_MODEL_PATH'], 'treatment_features_lexicon.xlsx')

    logging.basicConfig(level=logging.DEBUG)

    RunClassifierTest = True
    RunFeatureTest = True

    if RunFeatureTest:
        run_feature_test(Holdout_data_csv, lexicon_file)

    if RunClassifierTest:
        run_classifier_test(models_dir, Holdout_data_csv, lexicon_file, para_validation_and_error_reports=True)
