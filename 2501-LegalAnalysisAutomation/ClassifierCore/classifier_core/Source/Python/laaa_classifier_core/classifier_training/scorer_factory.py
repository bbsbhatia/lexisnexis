from sklearn.metrics import *
from sklearn.metrics.cluster import *
import inspect
import re


def get_scorer(scorer, config_pos_label=1, **kwargs):
    """Get a scorer from string
    :param scorer: The name of the scorer per
        https://scikit-learn.org/stable/modules/model_evaluation.html#common-cases-predefined-values
        or 'custom_{precision|recall|f1}_score, fbeta_score
        or a function definition as a string: def custom_score_func(y_true, y_pred, ...): ... return score
    :param config_pos_label: which class to return the score for
    :param kwargs: any arguments to the scorer function. If pos_label is included, it will override config_pos_label
    :return: the scorer
    """
    result = re.search(r'^def\s+(.+)\s*\(', scorer)
    if result:
        exec(scorer)
        scorer_params = dict(score_func=locals()[result.group(1)])
    else:
        try:
            scorer_params = SCORERS[scorer]
        except KeyError:
            raise ValueError('%r is not a valid scoring value. '
                             'Use sorted(scorer_factory.SCORERS.keys()) '
                             'to get valid options, or pass the scoring function '
                             'definition as a string: "def custom_score_func(y_true, y_pred, ...): ..."' % scorer)

    make_scorer_args = {**scorer_params, **kwargs}

    score_func = scorer_params['score_func']
    if 'pos_label' in inspect.signature(score_func).parameters and 'pos_label' not in kwargs:
        make_scorer_args['pos_label'] = config_pos_label

    return make_scorer(**make_scorer_args)


def custom_precision_score(y_true, y_pred, precision_min=0, recall_min=0, f1_min=0, threshold_fail_score=0,
                           labels=None, pos_label=1, average='binary', sample_weight=None):
    p, r, f, _ = precision_recall_fscore_support(y_true, y_pred,
                                                 labels=labels,
                                                 pos_label=pos_label,
                                                 average=average,
                                                 warn_for=('precision',),
                                                 sample_weight=sample_weight)
    if r < recall_min or p < precision_min or f < f1_min:
        return threshold_fail_score
    else:
        return p


def custom_recall_score(y_true, y_pred, precision_min=0, recall_min=0, f1_min=0, threshold_fail_score=0,
                        labels=None, pos_label=1, average='binary', sample_weight=None):
    p, r, f, _ = precision_recall_fscore_support(y_true, y_pred,
                                                 labels=labels,
                                                 pos_label=pos_label,
                                                 average=average,
                                                 warn_for=('recall',),
                                                 sample_weight=sample_weight)
    if r < recall_min or p < precision_min or f < f1_min:
        return threshold_fail_score
    else:
        return r


def custom_f1_score(y_true, y_pred, precision_min=0, recall_min=0, f1_min=0, threshold_fail_score=0,
                    labels=None, pos_label=1, average='binary', sample_weight=None):
    p, r, f, _ = precision_recall_fscore_support(y_true, y_pred,
                                                 labels=labels,
                                                 pos_label=pos_label,
                                                 average=average,
                                                 warn_for=('f-score',),
                                                 sample_weight=sample_weight)
    if r < recall_min or p < precision_min or f < f1_min:
        return threshold_fail_score
    else:
        return f


# Standard regression scores
explained_variance_scorer = dict(score_func=explained_variance_score)
r2_scorer = dict(score_func=r2_score)
neg_mean_squared_error_scorer = dict(score_func=mean_squared_error,
                                     greater_is_better=False)
neg_mean_squared_log_error_scorer = dict(score_func=mean_squared_log_error,
                                         greater_is_better=False)
neg_mean_absolute_error_scorer = dict(score_func=mean_absolute_error,
                                      greater_is_better=False)

neg_median_absolute_error_scorer = dict(score_func=median_absolute_error,
                                        greater_is_better=False)

# Standard Classification Scores
accuracy_scorer = dict(score_func=accuracy_score)
balanced_accuracy_scorer = dict(score_func=balanced_accuracy_score)

# Score functions that need decision values
roc_auc_scorer = dict(score_func=roc_auc_score, greater_is_better=True,
                      needs_threshold=True)
average_precision_scorer = dict(score_func=average_precision_score,
                                needs_threshold=True)

# Score function for probabilistic classification
neg_log_loss_scorer = dict(score_func=log_loss, greater_is_better=False,
                           needs_proba=True)
brier_score_loss_scorer = dict(score_func=brier_score_loss,
                               greater_is_better=False,
                               needs_proba=True)
fbeta_scorer = dict(score_func=fbeta_score)

# Clustering scores
adjusted_rand_scorer = dict(score_func=adjusted_rand_score)
homogeneity_scorer = dict(score_func=homogeneity_score)
completeness_scorer = dict(score_func=completeness_score)
v_measure_scorer = dict(score_func=v_measure_score)
mutual_info_scorer = dict(score_func=mutual_info_score)
adjusted_mutual_info_scorer = dict(score_func=adjusted_mutual_info_score)
normalized_mutual_info_scorer = dict(score_func=normalized_mutual_info_score)
fowlkes_mallows_scorer = dict(score_func=fowlkes_mallows_score)

# Custom scorers
custom_precision_scorer = dict(score_func=custom_precision_score)
custom_recall_scorer = dict(score_func=custom_recall_score)
custom_f1_scorer = dict(score_func=custom_f1_score)

SCORERS = dict(explained_variance=explained_variance_scorer,
               r2=r2_scorer,
               neg_median_absolute_error=neg_median_absolute_error_scorer,
               neg_mean_absolute_error=neg_mean_absolute_error_scorer,
               neg_mean_squared_error=neg_mean_squared_error_scorer,
               neg_mean_squared_log_error=neg_mean_squared_log_error_scorer,
               accuracy=accuracy_scorer,
               roc_auc=roc_auc_scorer,
               balanced_accuracy=balanced_accuracy_scorer,
               average_precision=average_precision_scorer,
               neg_log_loss=neg_log_loss_scorer,
               brier_score_loss=brier_score_loss_scorer,
               # Cluster metrics that use supervised evaluation
               adjusted_rand_score=adjusted_rand_scorer,
               homogeneity_score=homogeneity_scorer,
               completeness_score=completeness_scorer,
               v_measure_score=v_measure_scorer,
               mutual_info_score=mutual_info_scorer,
               adjusted_mutual_info_score=adjusted_mutual_info_scorer,
               normalized_mutual_info_score=normalized_mutual_info_scorer,
               fowlkes_mallows_score=fowlkes_mallows_scorer,
               fbeta_score=fbeta_scorer,
               custom_precision_score=custom_precision_scorer,
               custom_recall_score=custom_recall_scorer,
               custom_f1_score=custom_f1_scorer)

for name, metric in [('precision', precision_score),
                     ('recall', recall_score), ('f1', f1_score)]:
    SCORERS[name] = dict(score_func=metric)
    for average in ['macro', 'micro', 'samples', 'weighted']:
        qualified_name = '{0}_{1}'.format(name, average)
        SCORERS[qualified_name] = dict(score_func=metric, pos_label=None,
                                       average=average)
