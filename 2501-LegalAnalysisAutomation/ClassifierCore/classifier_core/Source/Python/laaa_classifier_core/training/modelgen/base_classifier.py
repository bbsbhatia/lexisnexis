# -----------------------------------------------------------------------------
# Shepards Treatment classifiers
# This work is still in progress.
#
# Author: Sanjay Sharma, iLabs
# Modified by: Mahesh Pendyala, iLabs
# Revision: 1.0 Date: 4/18/2018
# Modified by Mark Shewhart: November 2018
# Modified by LexisNexis Themis team 2019
# -----------------------------------------------------------------------------
import datetime
import io
import logging
import os
from abc import abstractmethod
from time import time
from operator import attrgetter

import numpy as np
import pandas as pd
# hyperparameter optimizations to try to find best classifier/ensemble
from sklearn import metrics
# extract and normalize features from input data
# print the classifier data
from sklearn import tree
from sklearn.externals import joblib
# select best features using feature selection algorithms
import sklearn.feature_selection
import sklearn.preprocessing
import sklearn.feature_extraction.text
import scipy.stats

from laaa_classifier_core.classifier_training import scorer_factory


class BaseClassifier:
    def __init__(self, pos_label, name='base', categories=('positive', 'negative')):

        self.name = name
        self.categories = categories

        self.logger = logging.getLogger(__name__)

        # Added initialization of member variables
        self.results = []
        self.feature_data_formatted = None
        self.target_data_formatted = None
        self.feature_names = None
        self.feature_selector = None
        self.train_data_strs = []
        self.test_data_strs = []
        self.num_docs_in_data = None
        self.num_docs_in_data_str = None
        self.num_folds = None
        self.num_iterations = None
        self.threshold_score = None
        self.threshold_train_score = None
        self.threshold_f1_macro = None
        self.num_clfs = None
        self.models_dir = None
        self.sample_counts_label = None
        self.pos_label = pos_label
        self.start_date = datetime.datetime.now()

    @abstractmethod
    def extract_features(self, config):
        pass

    @abstractmethod
    def select_features(self):
        pass

    @abstractmethod
    def classify(self):
        pass

    @staticmethod
    def load_scorer(scorer_config, pos_label=1):
        return scorer_factory.get_scorer(scorer_config['func'], pos_label, **scorer_config.get('params', {}))

    @staticmethod
    def load_param_distributions(param_config):
        param_dist = {}
        for key, value in param_config.items():
            if isinstance(value, dict):
                func_name = [*value][0]

                if func_name.startswith('scipy.stats.'):
                    func = attrgetter(func_name.split('scipy.stats.', maxsplit=1)[1])(scipy.stats)
                    func_args = value[func_name]
                    if isinstance(func_args, dict):
                        value = func(**func_args)
                    elif isinstance(func_args, list):
                        value = func(*func_args)
                    else:
                        value = func(func_args)

            param_dist[key] = value
        return param_dist

    @staticmethod
    def load_vectorizer(v_config):
        v_type = attrgetter(v_config['type'])(sklearn.feature_extraction.text)
        params = v_config.get('params', {})
        return v_type(**params)

    @staticmethod
    def load_feature_selector(fs_config):
        fs_type = attrgetter(fs_config['type'])(sklearn.feature_selection)
        params = fs_config.get('params', {}).copy()
        if 'score_func' in params:
            params['score_func'] = attrgetter(params['score_func'])(sklearn.feature_selection)

        return fs_type(**params)

    @staticmethod
    def load_scaler(scaler_config):
        scaler_type = attrgetter(scaler_config['type'])(sklearn.preprocessing)
        params = scaler_config.get('params', {})
        return scaler_type(**params)

    @staticmethod
    def balanced_accuracy(y_true, y_pred):
        """Default scoring function: balanced accuracy

        Balanced accuracy computes each class' accuracy on a per-class basis using a
        one-vs-rest encoding, then computes an unweighted average of the class accuracies.

        Parameters
        ----------
        y_true: numpy.ndarray {n_samples}
            True class labels
        y_pred: numpy.ndarray {n_samples}
            Predicted class labels by the estimator

        Returns
        -------
        fitness: float
            Returns a float value indicating the `individual`'s balanced accuracy
            0.5 is as good as chance, and 1.0 is perfect predictive accuracy
        """
        all_classes = list(set(np.append(y_true, y_pred)))
        all_class_accuracies = []
        for this_class in all_classes:
            this_class_sensitivity = \
                float(sum((y_pred == this_class) & (y_true == this_class))) / \
                float(sum((y_true == this_class)))

            this_class_specificity = \
                float(sum((y_pred != this_class) & (y_true != this_class))) / \
                float(sum((y_true != this_class)))

            this_class_accuracy = (this_class_sensitivity + this_class_specificity) / 2.
            all_class_accuracies.append(this_class_accuracy)

        return np.mean(all_class_accuracies)

    # Benchmark classifiers
    def benchmark(self, categories, x_train, x_test, y_train, y_test, clf, name="", feature_names=''):

        train_time = 0
        test_time = 0
        score = 0
        clf_descr = name
        clf_data = {"name": "Failed_" + str(name), "status": "Init", "score": score}
        binary_classification = y_train.max() == y_train.min() + 1

        try:
            clf_data["train_in_class"] = list(y_train).count(self.pos_label) / len(y_train)
            clf_data["test_in_class"] = list(y_test).count(self.pos_label) / len(y_test)
            clf_data["fold_row_count"] = y_train.shape[0]
            t0 = time()
            clf.fit(x_train, y_train)
            train_time = time() - t0
            self.logger.debug(f'{name}: #######  Benchmark ############################')
            self.logger.debug(f'{name}: fit time for train data:  {train_time:{5}.{4}}')

            # Predict training data
            t0 = time()
            pred = clf.predict(x_train)
            test_time = time() - t0
            train_score = metrics.accuracy_score(y_train, pred)
            self.logger.debug(f'{name}: predict time for train data:  {test_time:{5}.{4}}')
            train_s = list(pred).count(0)
            train_f = list(pred).count(1)

            # Predict test data
            t0 = time()
            pred = clf.predict(x_test)
            test_time = time() - t0
            self.logger.debug(f'{name}: predict time for test data:  {test_time:{5}.{4}}')
            test_s = list(pred).count(0)
            test_f = list(pred).count(1)

            train_data = None
            test_data = None

            if binary_classification:
                train_data = "Train: Real S, Real F, Pred S, Pred F {0} {1} {2} {3}".format(list(y_train).count(0),
                                                                                            list(y_train).count(1),
                                                                                            train_s,
                                                                                            train_f)
                test_data = "Test: Real S, Real F, Pred S, Pred F {0} {1} {2} {3}".format(list(y_test).count(0),
                                                                                          list(y_test).count(1), test_s,
                                                                                          test_f)

            # -- Score on test data ---
            score = metrics.accuracy_score(y_test, pred)
            self.logger.debug(f'{name}: = accuracy score = score = {score:{5}.{4}}')

            clf_data["score"] = score
            clf_data["train_score"] = train_score
            if binary_classification:
                clf_data["train_data"] = train_data
                clf_data["test_data"] = test_data

            # This is F-1 measure for Not Success ONLY
            binary_classification = y_train.max() == y_train.min() + 1
            if binary_classification:
                f1_binary = metrics.f1_score(y_test, pred)
                clf_data["f1_binary"] = f1_binary
            f1_binary = metrics.f1_score(y_test, pred, average='micro')
            clf_data["f1_micro"] = f1_binary
            f1_binary = metrics.f1_score(y_test, pred, average='macro')
            clf_data["f1_macro"] = f1_binary
            f1_binary = metrics.f1_score(y_test, pred, average='weighted')
            clf_data["f1_weighted"] = f1_binary
            if binary_classification:
                recall_l0 = metrics.recall_score(y_test, pred, pos_label=self.pos_label)
                precsion_l0 = metrics.precision_score(y_test, pred, pos_label=self.pos_label)
                clf_data["recall_l0"] = recall_l0
                clf_data["precsion_l0"] = precsion_l0
                self.logger.debug(f'{name}: = recall(0) score = {recall_l0:{5}.{4}}')
                self.logger.debug(f'{name}: = precision(0) score = {precsion_l0:{5}.{4}}')

            self.logger.debug(f'{name}: = f1_macro score = {clf_data["f1_macro"]:{5}.{4}}')

            clf_data["roc_auc"] = 0
            if hasattr(clf, 'predict_proba') and "Voting" not in name and binary_classification:
                predprob = clf.predict_proba(x_test)[:, 1]
                clf_data["roc_auc"] = metrics.roc_auc_score(y_test, predprob)

            # p, r, f1, s = metrics.precision_recall_fscore_support(y_test, pred)
            # Measured by Macro - clf_data["f1_mean"] = np.average(f1)
            # if (score > 0.60 and f1_ns > .50 and f1[1] > 0.30 and r[1] > 0.30):

            clf_data['clf'] = clf

            if hasattr(clf, 'cv_results_'):
                best_parameters = clf.best_params_

                clf_data['best_grid_score'] = clf.best_score_
                clf_best_params = []
                self.logger.debug(f'Best Hyperparameters:')
                for param_name in sorted(best_parameters.keys()):
                    self.logger.debug("%s: %r" % (param_name, best_parameters[param_name]))
                    clf_best_params.append("{0}:{1}".format(param_name, best_parameters[param_name]))
                clf_data['best_params'] = clf_best_params
                self.logger.debug(f'{name}:  best_params = {clf_best_params}, best score = {clf.best_score_:{5}.{4}}')
            clf_data['train_test_data'] = '{0:20} [{1:5d},{2:5d}] accuracy: {3:25.3f}'.format(name, len(y_train),
                                                                                              len(y_test), score)
            best_clf = clf
            if hasattr(clf, 'best_estimator_'):
                best_clf = clf.best_estimator_

            clf_data["best_clf"] = best_clf

            clf_data['classification report'] = metrics.classification_report(y_test, pred,
                                                                              target_names=categories)
            class_str = ""
            p, r, f1, s = metrics.precision_recall_fscore_support(y_test, pred)

            for i in range(len(p)):
                class_str = class_str + str(p[i]) + ","
                class_str = class_str + str(r[i]) + ","
                class_str = class_str + str(f1[i]) + ","
                class_str = class_str + str(s[i]) + ","
            class_str += str(np.average(p))
            class_str += ','
            class_str += str(np.average(s))
            class_str += ','
            class_str += str(np.average(f1))
            class_str += ','
            class_str += str(np.sum(s))

            cf = metrics.confusion_matrix(y_test, pred)

            clf_data['confusion matrix'] = cf
            clf_data['key_data'] = '{0},{1},{2},{3:.3f},{4},{5},{6},{7}'.format(name, len(y_train), len(y_test),
                                                                                score,
                                                                                ','.join(str(x) for x in
                                                                                         np.array(cf).reshape(
                                                                                             -1, ).tolist()),
                                                                                class_str,
                                                                                datetime.datetime.now().strftime(
                                                                                    "%d-%b-%I-%M-%S-%f"),
                                                                                clf_data["roc_auc"])
            # ---- Feature names
            if hasattr(clf, 'feature_importances_') and len(feature_names) > 0:
                imp_feature_index = clf.feature_importances_.argsort()[::-1]
                sorted_imp = clf.feature_importances_[imp_feature_index]
                sorted_names = feature_names[imp_feature_index]
                last_index = len(sorted_names)
                feat_str = "\nImportant features:\n" + "\n".join(
                    [sorted_names[i] + ":" + str(sorted_imp[i]) for i in range(last_index - 1)]) + "\n\n"
                clf_data['feature_importances_'] = feat_str
                clf_data['sorted_imp_feat_score'] =  sorted_imp
                clf_data['sorted_imp_feat_names'] =  sorted_names
                self.logger.debug(f'{name}: {feat_str}')
            if hasattr(clf, 'coef_') and len(feature_names) > 0:
                topones = np.argsort(clf.coef_[0])[::-1]
                feat_str = "Important features:\n" + "\n".join(np.array(feature_names)[topones])
                self.logger.debug(f'{name}: {feat_str}')
                clf_data['feature_importances_'] = feat_str

            clf_descr = str(clf).split('(')[0]

            self.logger.info(f'Classifier Settings:\n{clf}')
            self.logger.debug("F1 scores\n{0}\n{1}".format(f1, f1))
            self.logger.debug("Balanced accuracy = {0}".format(self.balanced_accuracy(y_test, pred)))
            self.logger.debug("AUC Score (Train): %f" % metrics.roc_auc_score(y_test, predprob))
            self.logger.debug("confusion matrix:\n{0}".format(cf))
            self.logger.info(
                "classification report:\n" + metrics.classification_report(y_test, pred, target_names=categories))

            self.logger.debug(
                '{0} [{1:5d},{2:5d}] accuracy: {3:.3f}'.format(name, len(y_train), len(y_test), score))
            clf_data["name"] = name
            clf_data["status"] = "Done"

            self.logger.debug(
                f'score:{clf_data["score"]}, precision:{clf_data["precsion_l0"]}, f1_macro:{clf_data["f1_macro"]}')
            self.logger.debug(
                f'threshold_score:{self.threshold_score}, threshold_train_score:{self.threshold_train_score}, '
                f'threshold_f1_macro:{self.threshold_f1_macro}')

            if clf_data["score"] > self.threshold_score and clf_data["train_score"] > self.threshold_train_score and \
                    clf_data["f1_macro"] > self.threshold_f1_macro:
                self.pickle_classifier(clf_data)
            else:
                self.logger.info('Not Writing below threshold classifier:' + clf_descr)
            self.logger.debug('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n')

        except Exception:
            self.logger.exception("Classifier failed")
        return clf_descr, score, train_time, test_time, clf_data

    # save the classifier
    def pickle_classifier(self, clf_item):

        name = clf_item['name']
        score = clf_item['score']
        f1_macro = clf_item['f1_macro']
        if 'recall_l0' in clf_item:
            recall_score = clf_item['recall_l0']
        else:
            recall_score = 0.0001

        write_path = os.path.join(self.models_dir,
                                  "{0}_{1:3.3f}_{2:3.3f}_{3:3.3f}_model".format(name, score, f1_macro, recall_score))
        dt_string = self.start_date.strftime("%d-%b-%I-%M-%S-%f")
        pickle_dir = write_path + "_" + self.sample_counts_label + "_" + dt_string

        best_clf = clf_item["best_clf"]

        self.logger.info('Writing: Classifier pickle dir is ' + pickle_dir)

        if not os.path.exists(pickle_dir):
            os.makedirs(pickle_dir)
        joblib.dump(best_clf, os.path.join(pickle_dir, "model.pkl"))

        if 'Decision Tree' in name and len(self.feature_names) > 0:
            tree_clf = best_clf
            if 'L1' in name:
                tree_clf = best_clf._final_estimator
            file_name = write_path + dt_string + ".dot"
            self.logger.info(pickle_dir + file_name)
            with open(file_name, 'w') as f:
                tree.export_graphviz(tree_clf, out_file=f,
                                     feature_names=self.feature_names,
                                     filled=True, rounded=True,
                                     special_characters=True)
                # ['Succesfull', 'Not Successful']
            file_name = write_path + dt_string + ".py"
            with open(file_name, 'w') as f:
                f.write(self.get_codea(tree_clf, self.feature_names, self.categories))

    # Produce psuedo-code for decision tree.
    # call get_codea(dt, features, targets)
    @staticmethod
    def get_codea(tree, feature_names, spacer_base="    "):
        """
        Args
        ----
        tree -- scikit-leant DescisionTree.
        feature_names -- list of feature names.
        target_names -- list of target (class) names.
        spacer_base -- used for spacing code (default: "    ").

        Notes
        -----
        based on http://stackoverflow.com/a/30104792.
        """

        buff = io.StringIO()
        left = tree.tree_.children_left
        right = tree.tree_.children_right
        threshold = tree.tree_.threshold
        features = [feature_names[i] for i in tree.tree_.feature]
        value = tree.tree_.value

        def recurse(left, right, threshold, features, node, depth, buff):
            spacer = spacer_base * depth
            if threshold[node] != -2:
                buff.write(spacer + "if row['" + features[node] + "'] <= " + str(threshold[node]) + ":\n")
                if left[node] != -1:
                    recurse(left, right, threshold, features,
                            left[node], depth + 1, buff)
                    buff.write(spacer + "else:\n")
                if right[node] != -1:
                    recurse(left, right, threshold, features,
                            right[node], depth + 1, buff)
                    # print(spacer + "}")
            else:
                target = value[node]

                buff.write(
                    spacer + 'return predict(' + str(int(target[0][0])) + ',' + str(int(target[0][1])) + ')' + '\n')

        recurse(left, right, threshold, features, 0, 0, buff)
        return buff.getvalue()

    # This function prints the results of classifiers.
    def report_results(self):
        """All classifiers are done, print the results of classifiers.
        Parameters
        ----------
        results : List
            List contains the results of classifiers
        num_folds : int
            Number of folds used in cross validation
            out of a classifier.
        self.num_clfs : int, default=False
            Number of classifiers
        """

        try:
            if not os.path.exists(self.models_dir):
                os.makedirs(self.models_dir)

            date_str = datetime.datetime.now().strftime("%d-%b-%I-%M-%S")
            file_name = os.path.join(self.models_dir, "results_" + date_str + '.txt')

            with open(file_name, 'w') as f:

                num_clfs_in_results = int(len(self.results) / self.num_folds)

                f.write("Number of classifiers  = {0}\n".format(self.num_clfs))
                f.write("Number of classifiers in results = {0}\n".format(num_clfs_in_results))

                verbose = 5
                all_clfs = []
                for i in range(num_clfs_in_results):
                    clf = []
                    all_clfs.append(clf)

                for index, result in enumerate(self.results):
                    all_clfs[index % num_clfs_in_results].append(result[4])

                df_list = []
                for clf in all_clfs:
                    if clf[0]['status'] != 'Done':
                        self.logger.error("Classifier {} failed".format(clf[0]['name']))
                        continue
                    scores = []
                    f1_binarys = []
                    f1_micros = []
                    f1_macros = []
                    f1_weighted = []
                    roc_auc_scores = []
                    train_scores = []
                    recall_l0 = []
                    precision_l0 = []
                    # f.write('_' * 80)
                    f.write(clf[0]['name'])
                    for clf_item in clf:
                        scores.append(clf_item["score"])
                        if 'f1_binary' in clf_item:
                            f1_binarys.append(clf_item["f1_binary"])
                        if 'f1_micro' in clf_item:
                            f1_micros.append(clf_item["f1_micro"])
                        if 'f1_macro' in clf_item:
                            f1_macros.append(clf_item["f1_macro"])
                        if 'f1_weighted' in clf_item:
                            f1_weighted.append(clf_item["f1_weighted"])
                        if 'roc_auc' in clf_item:
                            roc_auc_scores.append(clf_item["roc_auc"])
                        if 'recall_l0' in clf_item:
                            recall_l0.append(clf_item["recall_l0"])
                        if 'precision_l0' in clf_item:
                            precision_l0.append(clf_item["precision_l0"])
                        train_scores.append(clf_item["train_score"])

                        if verbose > 1 and clf_item.get('classification report', ""):
                            f.write("\n")
                            f.write('*' * 80)
                            f.write("\ntest_score = {0}\n".format(clf_item.get('score', "")))
                            f.write("train_score = {0}\n".format(clf_item.get('train_score', "")))
                            f.write("f1_macro = {0}\n".format(clf_item.get('f1_macro', "")))
                            f.write('train_data= {0}\n'.format(clf_item.get('train_data', "NONE")))
                            f.write('test_data= {0}\n'.format(clf_item.get('test_data', "NONE")))
                            f.write(clf[0]['name'] + " - best_params : " + ','.join(
                                map(str, clf_item.get('best_params', ""))))
                            f.write("\n")
                            f.write(clf[0]['name'] + " - best_grid_score : " + str(clf_item.get('best_grid_score', "")))
                            f.write("\n")
                            # @todo
                            f.write(clf[0]['name'] + ": " + clf_item.get('feature_importances_', ""))
                            f.write("\n")
                            f.write(np.array2string(clf_item.get('confusion matrix', "")))
                            f.write("\n")
                            f.write(clf_item.get('classification report', ""))
                            f.write("\n")
                            f.write(clf_item.get('key_data', ""))
                            f.write("\n")

                    if scores:
                        f.write("Average CSV Test Accuracy: {0:3.3f}, {1:3.3f}, {2},{3}\n".format(np.asarray(scores).mean(),
                                                                                                  np.asarray(
                                                                                                      scores).std() * 2,
                                                                                                  scores,
                                                                                                  clf[0]['name']))
                        f.write(
                            "Average CSV Train Accuracy: {0:3.3f}, {1:3.3f}, {2},{3}\n".format(
                                np.asarray(train_scores).mean(),
                                np.asarray(
                                    train_scores).std() * 2,
                                train_scores, clf[0]['name']))
                        f.write(
                            "Average F1 for Non Success: {0:3.3f}, {1:3.3f}, {2},{3}\n".format(
                                np.asarray(f1_binarys).mean(),
                                np.asarray(
                                    f1_binarys).std() * 2,
                                f1_binarys, clf[0]['name']))
                        f.write("Average F1-Micro: {0:3.3f}, {1:3.3f}, {2},{3}\n".format(np.asarray(f1_micros).mean(),
                                                                                         np.asarray(
                                                                                             f1_micros).std() * 2,
                                                                                         f1_micros, clf[0]['name']))
                        f.write("Average F1-Macro: {0:3.3f}, {1:3.3f}, {2},{3}\n".format(np.asarray(f1_macros).mean(),
                                                                                         np.asarray(
                                                                                             f1_macros).std() * 2,
                                                                                         f1_macros, clf[0]['name']))
                        f.write("Average F1-Weighted: {0:3.3f}, {1:3.3f}, {2},{3}\n".format(np.asarray(f1_weighted).mean(),
                                                                                            np.asarray(
                                                                                                f1_weighted).std() * 2,
                                                                                            f1_weighted, clf[0]['name']))
                        f.write("Average roc_auc_scores : {0:3.3f}, {1:3.3f}, {2},{3}\n".format(
                            np.asarray(roc_auc_scores).mean(),
                            np.asarray(
                                roc_auc_scores).std() * 2,
                            roc_auc_scores, clf[0]['name']))
                        f.write("Average recall_l0 : {0:3.3f}, {1:3.3f}, {2},{3}\n".format(np.asarray(recall_l0).mean(),
                                                                                           np.asarray(
                                                                                               recall_l0).std() * 2,
                                                                                           recall_l0, clf[0]['name']))

                        f.write(
                            "Average precision_l0 : {0:3.3f}, {1:3.3f}, {2},{3}\n".format(np.asarray(precision_l0).mean(),
                                                                                          np.asarray(
                                                                                              precision_l0).std() * 2,
                                                                                          precision_l0, clf[0]['name']))
                        f.write('-' * 80)
                        f.write("\n")

                        clf_report = {'classifier_name': clf[0]['name'],
                                      'mean_accuracy': np.asarray(scores).mean(),
                                      'mean_f1_macro': np.asarray(f1_macros).mean(),
                                      'mean_auc': np.asarray(roc_auc_scores).mean(),
                                      'mean_train_accuracy': np.asarray(train_scores).mean(),
                                      'mean_recall_label0': np.asarray(recall_l0).mean(),
                                      'mean_precision_label0': np.asarray(precision_l0).mean(),
                                      }
                        df_list.append(clf_report)
                pd.DataFrame(df_list).to_csv(os.path.join(self.models_dir, "results_" + date_str + '.csv'))
        except IOError:
            self.logger.exception('IO error saving classifier results report:')
        except Exception:
            self.logger.exception('Error saving classifier results report:')


if __name__ == "__main__":
    base_cls = BaseClassifier(name='shep', categories=['Treatment', 'No Treatment'])
