# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This package provides common utility methods to be used during model
# generation.

from . import shepards_classifier
from . import calculate_stats

train_model = shepards_classifier.train
calculate_stats = calculate_stats.calculate_stats
