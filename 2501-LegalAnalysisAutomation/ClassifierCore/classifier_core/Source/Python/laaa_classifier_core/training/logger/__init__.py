# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This package provides functions to configure logging.

import logging.config
import os


def init(config):
    global log_dir

    log_dir = os.path.dirname(config['handlers']['file']['filename'])
    if log_dir:
        os.makedirs(log_dir, exist_ok=True)

    logging.config.dictConfig(config)

