# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# driver.py provides main entry method for model training and validation

import os
import shutil
import logging
import traceback
import pandas as pd
from datetime import datetime
from prettytable import PrettyTable

from laaa_classifier_core.training.validation import validate_classifier_with_holdout_data
from laaa_classifier_core.training.logger import init as init_logging
from laaa_classifier_core.training.utils import ClassifierConfig
from laaa_classifier_core.training.utils import copy_files
from laaa_classifier_core.training.utils import download_s3_models
from laaa_classifier_core.training.utils import download_s3_holdout_data
from laaa_classifier_core.training.utils import is_s3_resource
from laaa_classifier_core.training.utils import download_features_from_s3
from laaa_classifier_core.training.utils import read_config as read_classifier_config
from laaa_classifier_core.training.utils import dump_config as dump_classifier_config
from laaa_classifier_core.training.utils import get_models_list
from laaa_classifier_core.training.utils import send_notification_email
from laaa_classifier_core.training.utils import send_success_notification_email
from laaa_classifier_core.training.utils import upload_output_to_s3
from laaa_classifier_core.training.utils import get_ec2_instance_identity
from laaa_classifier_core.training.utils import delete_cloud_formation_stack
from laaa_classifier_core.training.modelgen import train_model


def _execute_training(config):
    config = config  # type: ClassifierConfig
    logger = logging.getLogger(__name__)

    holdout_results, bucket_url = None, None
    try:
        holdout_results = _do_train_model(config)
    except Exception as e:
        logger.exception(f'Unhandled exception {e} during training:')
        _handle_train_error(e, config.smtp, config.notification_email)
        exit(1)
    finally:
        bucket_url = _finalize_train_model(config)

    if config.notification_email:
        training_time = datetime.now() - config.start_date
        send_success_notification_email(training_time, bucket_url, config.aws_login_url,
            config.smtp, config.notification_email, holdout_results)


def _execute_validation(config):
    logger = logging.getLogger(__name__)
    try:
        artifacts_location = config.validation_artifacts['destination']
        overwrite_artifacts = config.validation_artifacts['overwrite']
        models_location = config.validation_artifacts['models']['location']
        models_filter = config.validation_artifacts['models']['filters']
        _copy_models(models_location, models_filter, artifacts_location, overwrite_artifacts)
        holdout_data_path = config.validation_artifacts['validation_file']
        _copy_holdout_data(holdout_data_path, artifacts_location, overwrite_artifacts)
        holdout_data_path = os.path.join(artifacts_location, os.path.basename(holdout_data_path))
        _output_holdout_tracing(holdout_data_path, config.key_column)
        lexicon_file = config.lexicon_file
        lexicon_file_path = os.path.join(artifacts_location, lexicon_file)
        _output_lexicon_tracing(lexicon_file_path)
        logger.info(f'Starting Validation - Artifacts Location: {artifacts_location} ')
        models_list = get_models_list(artifacts_location, models_filter)
        logger.info(f'Num Models={len(models_list)}')
        validate_classifier_with_holdout_data(artifacts_location, holdout_data_path, lexicon_file_path, config=config)
    except Exception as e:
        logger.exception(f'Unhandled exception {e} during validation:')
        _handle_train_error(e, config.smtp, config.notification_email)
        exit(1)

    if config.notification_email:
        send_notification_email(config.notification_email, config.smtp, 'Validation Complete')


def _do_train_model(config):
    logger = logging.getLogger(__name__)

    holdout_results = None

    if config.download_s3_features:
        download_features_from_s3(bucket=config.s3_bucket,
                                  feature_set=config.s3_features,
                                  path=config.data_dir,
                                  overwrite=config.overwrite_features)

    _output_row_count_for_datafiles(config.data_dir, config.datafiles)

    os.makedirs(config.output_dir)

    lexicon_file_path = os.path.join(config.classifier_model_path, config.lexicon_file)

    shutil.copy2(src=lexicon_file_path, dst=config.output_dir)

    _output_lexicon_tracing(lexicon_file_path)

    for try_sample_count in config.try_sample_counts:
        logger.info(f'###### Starting Training for sample counts: {try_sample_count}  ######')
        train_model(lexicon_file=lexicon_file_path,
                    sample_counts=try_sample_count,
                    models_dir=config.output_dir,
                    data_dir=config.data_dir,
                    config=config)

    if config.holdout_data_filename:
        holdout_data_path = os.path.join(config.data_dir, config.holdout_data_filename)
        _output_holdout_tracing(holdout_data_path, config.key_column)
        holdout_results = validate_classifier_with_holdout_data(config.output_dir, holdout_data_path, lexicon_file_path, config=config)

    return holdout_results


def _handle_train_error(e, smtp_config, notification_email):
    if notification_email:
        message = ''.join(traceback.format_exception(etype=None, value=e, tb=e.__traceback__))
        send_notification_email(recipient=notification_email, smtp_config=smtp_config, status='Failure', message=message)


def _finalize_train_model(config):
    if config.log_dir and config.output_dir:
        for log in os.listdir(config.log_dir):
            shutil.copy2(src=os.path.join(config.log_dir, log), dst=config.output_dir)

    bucket_url = None
    if config and config.upload_to_s3 and config.output_dir:
        bucket_url = upload_output_to_s3(src=config.output_dir,
                                         bucket=config.s3_bucket,
                                         model_name=config.model_name)

    if config and config.terminate_ec2_on_finish:
        ec2_instance_identity = get_ec2_instance_identity()
        if ec2_instance_identity:
            delete_cloud_formation_stack(ec2_instance_identity)

    return bucket_url


def _copy_models(models_location, models_filter, dest_location, overwrite):
    if is_s3_resource(models_location):
        download_s3_models(models_location, dest_location, models_filter, overwrite=overwrite)
    else:
        copy_files(models_location, dest_location, filters=models_filter, overwrite=overwrite)


def _copy_holdout_data(holdout_data_path, dest_location, overwrite):
    if is_s3_resource(holdout_data_path):
        download_s3_holdout_data(holdout_data_path, dest_location, overwrite)
    else:
        shutil.copy(holdout_data_path, dest_location)


def _output_row_count_for_datafiles(data_dir, datafiles):
    logger = logging.getLogger(__name__)
    table = PrettyTable(['Data File', 'Row Count'])
    for file in datafiles:
        df = pd.read_csv(os.path.join(data_dir, file))
        row_count = len(df.index)
        table.add_row([file, row_count])
    logger.info(f'\nData Files and their counts:\n{table}')


def _output_lexicon_tracing(lexicon_file):
    logger = logging.getLogger(__name__)
    logger.info(f'Lexicon file: {lexicon_file}')
    formatted_date = datetime.fromtimestamp(os.path.getmtime(lexicon_file)).strftime('%m/%d/%Y %H:%M:%S')
    logger.info(f'Lexicon date/time: {formatted_date}')
    if lexicon_file.lower().endswith("xls") or lexicon_file.lower().endswith("xlsx"):
        df = pd.read_excel(lexicon_file)
    else:
        df = pd.read_csv(lexicon_file)
    row_count = len(df.index)
    logger.info(f'Lexicon row count: {row_count}')
    logger.info('Lexicon Data first 100 rows \n{}'.format(df.head(100)))


def _output_holdout_tracing(holdout_data_path, key_column):
    logger = logging.getLogger(__name__)
    logger.info(f'Holdout/Validation file: {holdout_data_path}')
    formatted_date = datetime.fromtimestamp(os.path.getmtime(holdout_data_path)).strftime('%m/%d/%Y %H:%M:%S')
    logger.info(f'Holdout/Validation date/time: {formatted_date}')
    df = pd.read_csv(holdout_data_path)
    logger.info(f'Holdout/Validation columns: {df.columns}')
    row_count = len(df.index)
    logger.info(f'Holdout/Validation row count: {row_count}')
    in_class = len(df.loc[df[key_column] == 0])
    logger.info(f'Holdout/Validation In class Row Count: {in_class}')


execution_functions = {
    'validation': _execute_validation,
    'training': _execute_training
}


def execute(config_path_override):
    config = read_classifier_config(config_path_override)  # type: ClassifierConfig
    init_logging(config.logging)
    logger = logging.getLogger(__name__)
    dump_classifier_config(logger)
    run_mode = config.run_mode
    execution_functions[run_mode](config)


if __name__ == "__main__":
    execute(os.path.dirname(__file__))
