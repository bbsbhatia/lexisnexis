# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This module provides utility functions to read training and validation data

import numpy as np
import pandas as pd
import os
import logging

from laaa_classifier_core.classifier import GenFeatures


def list_para_files(data_dir, config):
    # Returns a list of training data filenames
    # The order is important for selecting specific proportions of different types of paragraphs
    datafiles = []
    for file in config.datafiles:
        datafiles.append(os.path.join(data_dir, file))
    return datafiles


"""
     430,676 fonly_features_all_nos.csv
      25,091 fonly_nos_features_all_nos.csv
    4,813,587 nocite_features_all_nos.csv
    3,056,550 notreat_features_all_nos.csv
      26,644 treatedandf_features_all_nos.csv
       1,980 treatedandf_nos_features_all_nos.csv
     113,049 treatednof_features_all_nos.csv
     13,530 treatednof_nos_features_all_nos.csv
     25,832 treated_nocase_features_all_nos.csv

"""


def read_shep_para_data(datafiles, sample_counts, config):
    logger = logging.getLogger(__name__)
    logger.debug(read_shep_para_data.__name__)
    logger.debug(f'sample_counts={sample_counts}')
    # Returns a dataframe of paragraph CSV rows.
    # sample_counts is a list of #rows to get from each input file
    df = [None] * len(datafiles)
    index_col = config.feature_index_column
    for index, filename in enumerate(datafiles):
        rows2read = int(sample_counts[index])
        tmp_df = pd.read_csv(filename, sep=',', nrows=rows2read, index_col=index_col)
        tmp_df.fillna(0, inplace=True)
        df[index] = tmp_df

    df = pd.concat(df)
    return df


def get_sample_counts_label(sample_counts, phase):
    logger = logging.getLogger(__name__)
    logger.debug(get_sample_counts_label.__name__)
    # sample_counts is a list of #rows to get from each input file
    # phase is a label such as 'Treated' or 'TreatedOTF'
    # This outputs a string label for use in output file names
    #
    # Example:
    #     sample_counts=[1,2,3,4,5,6,7,8,9]
    #     phase = 'Treated'
    #     returns: 'Treated_1_2_3_4_5_6_7_8_9'
    sample_counts_str = [str(i) for i in sample_counts]
    sample_counts_label = str(phase) + '_' + '_'.join(sample_counts_str)
    return sample_counts_label


"""
     430,676 fonly_features_all_nos.csv
      25,091 fonly_nos_features_all_nos.csv
    4,813,587 nocite_features_all_nos.csv
    3,056,550 notreat_features_all_nos.csv
      26,644 treatedandf_features_all_nos.csv
       1,980 treatedandf_nos_features_all_nos.csv
     113,049 treatednof_features_all_nos.csv
     13,530 treatednof_nos_features_all_nos.csv
     25,832 treated_nocase_features_all_nos.csv
"""


# This is for the production "Has treatment" classifier
# Read data from the CSV files generated by xQuery
def read_data_treated(sample_counts, data_dir, treatment_language_file, config, apply_lexicon_treated=True,
                      output_lexicon_treated_matches=False,
                      apply_pos_treated=True):
    logger = logging.getLogger(__name__)
    logger.debug(read_data_treated.__name__)
    logger.debug(f'data_dir={data_dir}')
    datafiles = list_para_files(data_dir, config)
    sample_counts_label = get_sample_counts_label(sample_counts, 'Treated')

    df = read_shep_para_data(datafiles, sample_counts, config)
    key_column = config.key_column
    if key_column in df.columns:
        df['class'] = np.where((df[key_column] > 0), 0, 1)

    gf = GenFeatures(ApplyLexiconTreated=apply_lexicon_treated,
                     OutputLexiconTreatedMatches=output_lexicon_treated_matches,
                     ApplyPosTreated=apply_pos_treated,
                     treatment_language_file=treatment_language_file,
                     DebugLexicon=True)

    df, generated_feature_names = gf.add_features(df, max_partitions=None)

    other_feature_names = config.feature_names + generated_feature_names

    return df, config.text_columns, other_feature_names, sample_counts_label


# This is for the production "Has treatment" classifier
# Read data from test files
def read_testdata_treated(csv_file, treatment_language_file, config, apply_lexicon_treated=True,
                          output_lexicon_treated_matches=False,
                          apply_pos_treated=True):
    logger = logging.getLogger(__name__)
    logger.debug(read_testdata_treated.__name__)
    logger.debug(f'csv_file={csv_file}')

    gf = GenFeatures(ApplyLexiconTreated=apply_lexicon_treated,
                     OutputLexiconTreatedMatches=output_lexicon_treated_matches,
                     ApplyPosTreated=apply_pos_treated,
                     treatment_language_file=treatment_language_file,
                     DebugLexicon=True)

    genfeatures_file = os.path.splitext(csv_file)[0] + '.genfeatures.csv'

    index_col = config.feature_index_column

    if os.path.isfile(genfeatures_file):
        df = pd.read_csv(genfeatures_file, sep=',', index_col=0, na_filter=False)
        generated_feature_names = gf.GetGeneratedFeatures()
    else:
        df = pd.read_csv(csv_file, sep=',', index_col=index_col)
        df, generated_feature_names = gf.add_features(df, max_partitions=None)
        df.to_csv(genfeatures_file)

    other_feature_names = config.feature_names + generated_feature_names

    return df, config.text_columns, other_feature_names
