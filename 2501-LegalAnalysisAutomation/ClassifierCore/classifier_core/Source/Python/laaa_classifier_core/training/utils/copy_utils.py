# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This module provides utility methods to copy files.

import os
import fnmatch
import glob
import shutil


def copy_files(src_path, dest_path, filters=None, overwrite=True):
    """
    copy files from source location to destination
    :param src_path: source directory
    :param dest_path: destination directory
    :param overwrite: when True, overwrite model file if it exists, skip otherwise
    :param filters: copy files that match one of the given glob filters in the list
    """
    os.makedirs(dest_path, exist_ok=True)
    for file in glob.glob(src_path+'/**', recursive=True):
        if _filter_match(file, filters):
            if os.path.isdir(file):
                continue
            relative_path = file[len(src_path)+1:]
            folder = os.path.join(dest_path, os.path.dirname(relative_path))
            os.makedirs(folder, exist_ok=True)
            dst_file = os.path.join(folder, os.path.basename(file))
            if overwrite or not os.path.isfile(dst_file):
                shutil.copy(file, folder)


def _filter_match(file, filters):
    """
    return true if file name matches glob expression in list of expressions
    :param file: file name
    :param filters: list of glob expressions
    :returns True if file name matches glob expression, False otherwise
    """
    if not filters:
        return True
    for fltr in filters:
        if fnmatch.fnmatch(file, fltr):
            return True
    return False
