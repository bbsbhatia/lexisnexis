# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This package provides common utility methods to be used during model
# training and validation.

import os
import re
import fnmatch
import glob
import logging


def get_models_list(models_dir, filters=None):
    """
    Return a list of models.
    Each element in the list is a list [model_dir, class_pickle, vect_pickle, feat_sel_pickle]
    :param models_dir: location where models are located
    :param filters: used to select models that match one of the given glob filters in the list
    """
    def filter_match(file):
        if not filters:
            return True
        for fltr in filters:
            if fnmatch.fnmatch(file, fltr):
                return True
        return False

    models_list = []

    names_list = [child for child in os.listdir(models_dir) if
                  os.path.isdir(os.path.join(models_dir, child)) and filter_match(child)]

    for name in names_list:
        parts = _gen_model_parts(models_dir, name)
        if parts:
            models_list.append(parts)

    return models_list


def _gen_model_parts(path, model):

    logger = logging.getLogger(__name__)

    logger.debug(':::::::::::::::::::::::::::gen_model_parts::::::::::::::::::::::::::::')
    logger.debug('path=' + path)
    logger.debug('model=' + model)

    sc_pattern = r'^.+?_([\d_]+)_.+$'
    sc_compiled = re.compile(sc_pattern, re.M)
    sc_replace = r'\1'
    sc_str = re.sub(sc_compiled, sc_replace, model)

    logger.debug('sc_str=' + sc_str)

    model_dir = os.path.join(path, model)
    class_pickle = os.path.join(model_dir, 'model.pkl')
    vect_pickle_pattern = os.path.join(path, 'vectorizer_' + '*' + '_' + sc_str + '.pickle')
    feat_sel_pickle_pattern = os.path.join(path, 'feature_selector_' + '*' + '_' + sc_str + '.pickle')
    vect_pickle = glob.glob(vect_pickle_pattern)[0]
    feat_sel_pickle = glob.glob(feat_sel_pickle_pattern)[0]

    if not re.match(sc_compiled, model) or not os.path.isfile(class_pickle):
        return None

    logger.debug('class_pickle=' + class_pickle)
    logger.debug('vect_pickle=' + vect_pickle)
    logger.debug('feat_sel_pickle=' + feat_sel_pickle)

    return [model_dir, class_pickle, vect_pickle, feat_sel_pickle]


