# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This module provides utility methods for aws ec2 services.

import json
import boto3
import logging
import smtplib
import urllib.request
from email.message import EmailMessage


def get_ec2_instance_identity():
    """
    Returns the AWS EC2 instance identity document
    See https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instance-identity-documents.html
    :return: the EC2 instance identity document if we are on EC2, otherwise None
    """
    try:
        with urllib.request.urlopen('http://169.254.169.254/latest/dynamic/instance-identity/document',
                                    timeout=10) as r:
            return json.load(r)
    except Exception as e:
        logger = logging.getLogger(__name__)
        logger.warning(f'Could not get EC2 Instance Identity Document - may not be running on EC2 - {e}')
        return None


def get_cloud_formation_stack_name(instance_identity):
    """
    Gets the cloud formation stack name EC2 instance is part of
    :param instance_identity: AWS EC2 instance identity document
    :returns stack name
    """
    region = instance_identity['region']
    instance_id = instance_identity['instanceId']
    instance = boto3.resource('ec2', region_name=region).Instance(instance_id)
    for tag in instance.tags:
        if tag["Key"] == 'aws:cloudformation:stack-name':
            return tag["Value"]
    return None


def delete_stack(instance_identity):
    """
    Deletes the CloudFormation stack  EC2 instance is part of
    :param instance_identity: AWS EC2 instance identity document
    """
    region = instance_identity['region']
    stack_name = get_cloud_formation_stack_name(instance_identity)
    if stack_name:
        boto3.resource('cloudformation', region_name=region).Stack(stack_name).delete()


def send_success_notification(training_time, bucket_url, aws_login_url, smtp_config, notification_email, holdout_results=None):
    """
    Sends a notification email on successful completion of training job.
    :param training_time: Total time taken for training job
    :param bucket_url: AWS S3 bucket where trained models are uploaded
    :param aws_login_url: Url for aws console login
    :param smtp_config: SMTP configuration for sending email
    :param notification_email: Recipient email address(s) to send notification
    :param holdout_results: If specified, model metrics from holdout data validation
           are included in message body
    """
    message = f'Training completed in {training_time}'

    if bucket_url:
        message += f'\n\n<a href="{bucket_url}">S3 Bucket</a>' \
            f'\nYou must be <a href="{aws_login_url}">signed in to AWS</a> first'

    if holdout_results:
        for model, metrics_df, _ in holdout_results:
            message += f'\n\n<a href="{bucket_url}{model}/">{model}</a>' if bucket_url else f'\n\n{model}'
            for metric in metrics_df:
                message += f'\n\t{metric}: {metrics_df[metric].iloc[0]}'

    send_notification(recipient=notification_email,
                      smtp_config=smtp_config,
                      status='Success',
                      message=message)


def send_notification(recipient, smtp_config, status, model_name='', message=''):
    """
    Sends a notification email.
    :param recipient: Recipient email address(s) to send notification
    :param smtp_config: SMTP configuration for sending email
    :param status: completion status of training job
    :param model_name: trained model name
    :param message: notification message
    """
    logger = logging.getLogger(__name__)
    hosts = smtp_config['hosts']
    attempts = smtp_config.get('attempts_per_host', 3)
    timeout = smtp_config.get('timeout', 10)
    sender = smtp_config['sender']
    content = f'Training/Validation {model_name} completed with status: {status}'

    ec2_instance_identity = get_ec2_instance_identity()
    if ec2_instance_identity:
        stack_name = get_cloud_formation_stack_name(ec2_instance_identity)
        cf_url = f'https://console.aws.amazon.com/cloudformation/home?#/stack/detail?stackId={stack_name}'
        instance_id = ec2_instance_identity["instanceId"]
        ec2_url = f'https://console.aws.amazon.com/ec2/v2/home?#Instances:instanceId={instance_id}'
        content += f' on instance type {ec2_instance_identity["instanceType"]}' \
                   f' @ {ec2_instance_identity["privateIp"]}' \
                   f'\n\nStack: <a href="{cf_url}">{stack_name}</a> ' \
                   f'(Delete the stack rather than terminating the instance)' \
                   f'\nInstance: <a href="{ec2_url}">{instance_id}</a>'

    if message:
        content += f'\n\n{message}'
    msg = EmailMessage()
    content = content.replace('\n', '\n<br/>').replace('\t', '&emsp;')
    msg.add_alternative(content, subtype='html')
    msg['From'] = sender
    msg['To'] = recipient
    msg['Subject'] = f'{model_name}: {status}'
    for attempt in range(1, attempts + 1):
        for host in hosts:
            try:
                with smtplib.SMTP(host=host, timeout=timeout) as smtp:
                    smtp.send_message(msg)
                return
            except Exception as e:
                logger.exception(f'Sending email failed - {e} - (attempt {attempt} of {attempts} on {host}):')

