# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This module provides utility methods to copy files from/to aws s3 bucket.

import boto3
import botocore.exceptions
import os
import fnmatch
import logging
from urllib.parse import urlparse, ParseResult


def download_holdout_data(s3_url, local_path, overwrite=False):
    """
    Downloads holdout data file from the given bucket into the given path
    :param s3_url: s3 url in format s3://bucket/path
    :param local_path: location to save holdout data file
    :param overwrite: when True, overwrite file if it exists, skip otherwise
    """
    logger = logging.getLogger(__name__)
    logger.info('Downloading holdout data from S3')
    logger.info(f's3_url={s3_url} path={local_path}')
    try:
        os.makedirs(local_path, exist_ok=True)

        url = urlparse(s3_url, allow_fragments=False)  # type: ParseResult
        bucket_name = url.netloc
        object_key = url.path.lstrip('/')

        s3 = boto3.client('s3')

        file_path = os.path.join(local_path, os.path.basename(object_key))
        logger.info(f'Downloading {file_path}')
        if overwrite or not os.path.isfile(file_path):
            s3.download_file(bucket_name, object_key, file_path)
        else:
            logger.info(f'{file_path} exists. Skipping download')
    except botocore.exceptions.ClientError:
        logger.exception('Error downloading holdout data from S3:')
    except IOError:
        logger.exception('IO error while saving downloaded file from S3:')


def download_model_objects(s3_url, local_path, filters, overwrite=False):
    """
    Downloads model objects from the given bucket into the given path
    :param s3_url: s3 url in format s3://bucket/path
    :param local_path: location to save objects
    :param filters: download objects that match one of the given filters in the list
    :param overwrite: when True, overwrite model file if it exists, skip otherwise
    """
    logger = logging.getLogger(__name__)
    logger.info('Downloading objects from S3')
    logger.info(f's3_url={s3_url} filters={filters} path={local_path}')
    try:
        os.makedirs(local_path, exist_ok=True)

        url = urlparse(s3_url, allow_fragments=False)  # type: ParseResult
        bucket_name = url.netloc
        prefix = url.path.lstrip('/') + '/'

        session = boto3.session.Session()
        s3 = session.resource('s3')
        bucket = s3.Bucket(bucket_name)

        for obj in bucket.objects.filter(Prefix=prefix):
            file_key = obj.key[len(prefix):]
            folder = os.path.dirname(file_key)
            subpath = file_key if folder == '' else folder
            if not _filter_match(subpath, filters):
                continue
            filename = os.path.join(local_path, file_key)
            logger.info(f'Downloading {filename}')
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            if overwrite or not os.path.isfile(filename):
                bucket.download_file(Key=obj.key, Filename=filename)
            else:
                logger.info(f'{filename} exists. Skipping download')
    except botocore.exceptions.ClientError:
        logger.exception('Error downloading objects from S3:')
    except IOError:
        logger.exception('IO error while saving downloaded objects from S3:')


def download_features_from_s3(bucket, feature_set, path, overwrite=False):
    """
    Downloads feature CSV training data from the given bucket into the given path
    :param bucket: S3 bucket name
    :param feature_set: name of feature set
    :param path: location to save files
    :param overwrite: option to overwrite existing files, otherwise download is skipped
    """
    logger = logging.getLogger(__name__)
    logger.info('Downloading features from S3')
    logger.info(f'bucket={bucket} feature_set={feature_set} destination path={path}')
    try:
        if not os.path.exists(path):
            os.makedirs(path)

        session = boto3.session.Session()
        s3 = session.resource('s3')
        bucket = s3.Bucket(bucket)
        prefix = f'training/features/{feature_set}'

        logger.info(f'Filtering S3 objects with prefix {prefix}')

        for obj in bucket.objects.filter(Prefix=prefix):
            if not obj.key.endswith('/'):
                filename = os.path.join(path, os.path.basename(obj.key))
                logger.debug(f'Preparing to download {obj.key}')
                if os.path.exists(filename) and not overwrite:
                    logger.warning(f'{filename} exists. Skipping. Use "overwrite_features": true to force download')
                else:
                    bucket.download_file(Key=obj.key, Filename=filename)
                    logger.info(f'Downloaded {filename}')

        logger.info(f'Download from S3 complete')

    except botocore.exceptions.ClientError:
        logger.exception('Error downloading features from S3:')
    except IOError:
        logger.exception('IO error while saving downloaded features from S3:')


def upload_output_to_s3(src, bucket, model_name):
    """
    Uploads all the files from the src directory, then moves them to dst
    Format will be <model_name>[-<s3_model_description>][-#]
    -2, -3 etc is automatically appended if the model name and description already exists in S3
    :param src: location of files to be uploaded
    :param bucket: S3 bucket to upload to
    :param model_name: model name
    :returns URL to the S3 bucket in AWS Console
    """
    logger = logging.getLogger(__name__)
    logger.info('Copying output to S3')
    try:
        session = boto3.session.Session()
        client = session.client('s3')
        model_number = 1
        s3_model_name = model_name
        response = client.list_objects(Bucket=bucket, Prefix=f'model/{model_name}', Delimiter='/')
        existing_models = [prefix['Prefix'] for prefix in
                           response.get('CommonPrefixes', [])]
        while f'model/{s3_model_name}/' in existing_models:
            logger.debug(f'Found existing model {s3_model_name}')
            model_number += 1
            s3_model_name = f'{model_name}-{model_number}'

        for root, dirs, files in os.walk(src):
            for file in files:
                path = os.path.join(root, file)
                logger.debug(f'path={path}')
                key = os.path.relpath(path, start=src).replace('\\', '/')
                key = f'model/{s3_model_name}/{key}'
                logger.debug(f'key={key}')
                client.upload_file(Filename=path, Bucket=bucket, Key=key)

        return f'https://s3.console.aws.amazon.com/s3/buckets/{bucket}/model/{s3_model_name}/'

    except Exception as e:
        logger.exception('Error copying output to S3:')
        raise e


def is_s3_resource(resource):
    """
    check whether given resource url represents a s3 resource e.g. s3://<bucket>/<object-key>
    :param resource: resource url
    :returns True if s3 resource, False otherwise
    """
    url = urlparse(resource, allow_fragments=False)
    return url.scheme == 's3'


def _filter_match(file, filters):
    """
    return true if file name matches glob expression in list of expressions
    :param file: file name
    :param filters: list of glob expressions
    :returns True if file name matches glob expression, False otherwise
    """
    if not filters:
        return True
    for fltr in filters:
        if fnmatch.fnmatch(file, fltr):
            return True
    return False
