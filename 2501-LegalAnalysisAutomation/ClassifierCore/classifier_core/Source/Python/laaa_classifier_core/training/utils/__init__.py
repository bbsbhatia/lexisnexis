# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This package provides common utility methods to be used during model
# training and validation.

from . import s3_utils
from . import ec2_utils
from . import copy_utils
from . import model_utils
from . import config_utils
from . import training_data_reader_utils

copy_files = copy_utils.copy_files
download_s3_models = s3_utils.download_model_objects
download_s3_holdout_data = s3_utils.download_holdout_data
download_features_from_s3 = s3_utils.download_features_from_s3
upload_output_to_s3 = s3_utils.upload_output_to_s3
is_s3_resource = s3_utils.is_s3_resource
get_ec2_instance_identity = ec2_utils.get_ec2_instance_identity
get_cloud_formation_stack_name = ec2_utils.get_cloud_formation_stack_name
delete_cloud_formation_stack = ec2_utils.delete_stack
send_notification_email = ec2_utils.send_notification
send_success_notification_email = ec2_utils.send_success_notification
get_models_list = model_utils.get_models_list
read_config = config_utils.read_config
dump_config = config_utils.dump_config
read_testdata_treated = training_data_reader_utils.read_testdata_treated
read_data_treated = training_data_reader_utils.read_data_treated
get_sample_counts_label = training_data_reader_utils.get_sample_counts_label

ClassifierConfig = config_utils.ClassifierConfig

