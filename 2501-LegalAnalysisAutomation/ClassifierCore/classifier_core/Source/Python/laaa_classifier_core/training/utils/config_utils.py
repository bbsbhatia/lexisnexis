# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This module provides functions to access model training and
# validation configuration. Most of the configuration is
# specified in config.json and some config items are sourced
# from environment variables as well.

import os
import json
import re
import platform
import numpy as np
import pandas as pd
from datetime import datetime
from collections import namedtuple


config_atts = [
    #
    # Description to be used for  model folder when uploaded to S3 in the format:
    # YYYYMMDD-<s3_model_description>[-#]
    's3_model_description',
    #
    # S3 bucket where models are uploaded.
    # This is defined using env:S3_BUCKET
    's3_bucket',
    #
    # Set to true to upload the models to S3 after training
    'upload_to_s3',
    #
    # Set to true to download training data feature CSVs from the S3 bucket.
    # If not specified, existing training data is used.
    # Training data location is determined by env:CLASSIFIER_TRAINING_DATA
    'download_s3_features',
    #
    # Url for Federated Login to AWS Console.
    # This is currently used in notification email that is
    # sent to inform the user that model training is complete.
    'aws_login_url',
    #
    # SMTP configuration for sending notification email.
    # This is a json structure with following fields:
    # hosts: List of SMTP mail servers
    # sender: from email adress
    # attempts_per_host: number of times to try connect to each mail server
    # timeout: mail server connect timeout
    'smtp',
    #
    # Comma separated list of email addresses of recipients for training
    # completion or failure message.
    'notification_email',
    #
    # This property is set to true if AWS Cloud Formation Stack is to be
    # deleted when training is complete, false otherwise. This
    # property is set to false during experimentation when it is
    # desired to SSH into EC2 instance to validate results and
    # change configuration parameters for retraining.
    'terminate_ec2_on_finish',
    #
    # This property specifies which label (0 or 1) is the positive
    # class, e.g. HasTreatment and is used for calculating metrics
    # like precision, recall, f1 etc.
    'pos_label',
    #
    # Type of vectorizer to use from sklearn.feature_extraction.text
    # e.g. TfIdfVectorizer, HashingVectorizer
    #
    # Example:
    #
    # "vectorizer": {
    #   "type": "TfidfVectorizer",
    #   "params": {
    #     "sublinear_tf": true,
    #     "max_df": 0.85,
    #     "ngram_range": [1, 5],
    #     "max_features": 4000,
    #     "min_df": 25
    #   }
    # }
    'vectorizer',
    #
    # Type of feature selector to use from sklearn.feature_selection
    # e.g. SelectKbest, SelectPercentile.
    #
    # score_func is name of function from sklearn.feature_selection
    # e.g. chi2, f_classif
    #
    # Example:
    #
    # "feature_selector": {
    #   "type": "SelectKBest",
    #   "params": {
    #     "score_func": "chi2",
    #     "k": 3000
    #   }
    # }
    'feature_selector',
    #
    # Type of scaler to use from sklearn.preprocessing e.g.
    # StandardScaler, MinMaxScaler
    #
    # Example:
    #
    # "scaler": {
    #   "type": "StandardScaler",
    #   "params": {
    #     "with_mean": false
    #   }
    # }
    'scaler',
    #
    # Number of CV folds to use for training
    'num_folds',
    #
    # Number of training iterations
    'num_iterations',
    #
    # Threshold of accuracy score on test data to determine
    # whether to save model to disk
    'threshold_score',
    #
    # Threshold of accuracy score on train data to determine
    # whether to save model to disk
    'threshold_train_score',
    #
    # Threshold of f1 macro on test data to determinr
    # whether to save model to disk
    'threshold_f1_macro',
    #
    # Dictionary defining a param_distribution for RandomizedSearchCV
    # or GridSearchCV with an XGBoost classifier.
    #
    # Values can be lists or statistical functions from scipy.stats that
    # return a distribution(implementing an rvs method) or an array.
    #
    # Statistical functions are to be given as a dictionary with a single
    # key as the name of the function e.g. scipy.stats.randint,
    # and the value as the parameters to the function.
    # The parameters can be a value, a list, or a dictionary of parameters.
    # If the function does not take any parameters, use an empty list []
    #
    # No parameters: {"scipy.stats.norm": []}
    # Value parameter: {"scipy.stats.norm.rvs": 0}
    # Positional parameters: {"scipy.stats.randint.rvs": [0, 1, 2, 3]}
    # Keyword parameters: {"scipy.stats.expon": {"scale": 0.1}}
    #
    # Example:
    #
    # "xgb_param_dist": {
    #   "max_depth": [6, 10, 20, 50, 100, 150],
    #   "n_estimators": {"scipy.stats.randint": [10, 200]},
    #   "min_child_weight": {"scipy.stats.randint": [2, 20]},
    #   "learning_rate": {
    #     "scipy.stats.expon": {"scale": 0.1}
    #   },
    #   "subsample": [0.5, 0.75, 1.0],
    #   "colsample_bytree": [0.5, 0.75, 1.0],
    #   "scale_pos_weight": [0.67, 1, 1.5]
    # }
    'xgb_param_dist',
    #
    # Scorer to use during training for determining optimal hyperparameters.
    #
    # func: The scoring function.
    #
    # Accepts Any of the predefined values for scoring function as listed in table
    # table 3.3.1.1 of scikit-learn model evaluation documentation
    # https://scikit-learn.org/stable/modules/model_evaluation.html#common-cases-predefined-values
    #
    # custom_precision_score accepts as parameters thresholds and a threshold
    # failure score. If all the thresholds are met, returns the precision value.
    # If the thresholds are not met, returns the threshold failure score
    #
    # precision_min=0
    # recall_min=0
    # f1_min=0
    # threshold_fail_score=0
    # labels=None
    # average='binary'
    # sample_weight=None
    #
    # custom_recall_score same as custom_precision_score except returns
    # the recall score if the thresholds are met
    #
    # custom_f1_score same as custom_precision_score except returns
    # the f1 score if the thresholds are met
    #
    # fbeta_score
    #
    # params: a dictionary of parameters to the scoring function
    #
    # If the scoring function takes a pos_label parameter but it
    # is not set in the scorer.params configuration item, this will
    # be set by the pos_label top-level configuration item above.
    # If pos_label is also set in scorer.params, this setting will take
    # precedence for the scorer only.
    #
    # Example:
    # "scorer": {
    #   "func": "custom_precision_score",
    #   "params": {
    #     "recall_min": 0.5
    #   }
    # }
    'scorer',
    #
    # This is derived using current date and s3_model_description
    # and not specified in config.json
    'model_name',
    #
    # The phase of treatment classifier model
    'treatment_phase',
    #
    # Allowed categories used by the classifier
    'categories',
    #
    # Directory which contains the trained model, vectorizer, and feature selector pickle files.
    # This is determined using env:CLASSIFIER_TRAINING_OUTPUT, s3 model name and start datetime.
    'output_dir',
    #
    # Directory which either already contains the 9 training CSV files,
    # or the directory to store them locally when downloading them from S3
    # This is defined using env:CLASSIFIER_TRAINING_DATA
    'data_dir',
    #
    # classifier_model_path is the directory containing the lexicon file, whose filename is given as lexicon_file
    # in config.json. This approach is for consistency with the treatment_classifier_service to use the same env
    # This is defined using env:CLASSIFIER_MODEL_PATH
    'classifier_model_path',
    #
    # The filename of lexicon file in directory defined using
    # env:CLASSIFIER_MODEL_PATH
    'lexicon_file',
    #
    # A list of sample count settings to try.
    # Each item in the try list should contain a list of 9 integers,
    # indicating the number of samples to take from each of the 9
    # training data CSVs, e.g
    # [
    #   [ 10, 20, 30, 40, 50, 60, 70, 80, 90 ],
    #   [ 99, 88, 77, 66, 55, 44, 33, 22, 11 ]
    # ]
    'try_sample_counts',
    #
    # The version of training feature set to download from
    # location <s3_bucket>/training/features/<s3_features>
    's3_features',
    #
    # Set to true to overwrite exisiting feature CSVs with
    # those downloaded from S3
    'overwrite_features',
    #
    # Filename of CSV containing holdout data
    'holdout_data_filename',
    #
    # list of feature file names
    'datafiles',
    #
    # list of feature names
    'feature_names',
    #
    # feature index column
    'feature_index_column',
    #
    # list of text columns
    'text_columns',
    #
    # key column
    'key_column',
    #
    # This value can either be 'training' or 'validation'.
    # In training mode, models are generated and then validated
    # against holdout data. In validation mode, exisiting models
    # are validated with given validation file.
    'run_mode',
    #
    # When the run_mode value is 'validation', following configuration is
    # used to determine how to source existing models and where
    # to copy them.
    #
    # Example: Models in network / local drive
    #
    # A model is currently a set of 4 files:
    # model pickle, feature selector pickle, vectorizer pickle, lexicon csv
    # These files are structured as follows:
    # model-root:
    #   lexicon
    #   feature selector pickle
    #   vectorizer pickle
    #   /model-1:
    #     model pickle
    #   /model-n:
    #     model pickle
    # For optimization, one feature selector and vectorizer is
    # created for multiple model files. The directory containing model
    # file uses the same pattern that is used to name the
    # feature selector and vectorizer. The pattern is derived from
    # try sample counts.
    #
    # User can select models for validation specifying glob expressions.
    # Configuration below uses location to determine model root.
    # *600_100_100_700* matches vectorizer and feature selector files
    # in model root as well as model directories named using the
    # same pattern. To complete the model definition, lexicon glob
    # expression is explicity defined as well.
    #
    # target_location along with target_prefix determines where
    # the model files and validation file are copied to.
    #
    # validation_file determines location of validation data file.
    #
    # Set overwrite to true is the existing files in target_location
    # are to be erased.
    #
    # "validation_artifacts: {
    #   "models": {
    #      "location": "/model_data/20191121-10-17-07.033077",
    #      "filters": [
    #          "*600_100_100_700*",
    #          "*lexicon*"
    #      ]
    #  },
    #  "holdout_data_file": "C:/config/classifier/training_data_backup/holdout_features_all_nos.csv",
    #  "validation_file": "s3://<bucket-name>/training/features/small/holdout_features_all_nos.csv",
    #  "overwrite": true,
    #  "destination": "/models/anna-run1"
    # }
    #
    # Example: Models in S3
    #
    # "validation_artifacts": {
    #   "models": {
    #     "location": "s3://2501-treatment-classifier-training-284211348336/model/20191122",
    #     "filters": [
    #       "*500_100_100_500_50_50_600_200_100*",
    #       "*lexicon*"
    #     ]
    #   },
    #   "validation_file" : "s3://2501-treatment-classifier-training-284211348336/training/features/small/holdout_features_all_nos.csv",
    #   "overwrite": false,
    #   "destination": "C:/models/anna-run2"
    # }
    #
    # When the run_mode value is 'training', use the following configuration
    #
    # "validation_artifacts": {
    #   "models": {
    #     "location": "",
    #     "filters": []
    #   }
    # }
    'validation_artifacts',
    #
    # Logging configuration
    'logging',
    #
    # This is derived from logging configuration
    'log_dir',
    #
    # Training execution start time and is derived
    # from system date at begining.
    'start_date'
]

ClassifierConfig = namedtuple('ClassifierConfig', config_atts)

_classifier_config = None


def read_config(config_path):
    global _classifier_config
    if _classifier_config:
        return _classifier_config
    config_file = os.path.join(config_path, 'config.json')
    with open(config_file, mode='r') as conf_json:
        config = json.load(conf_json)
        _build_config(config)
    return _classifier_config


def dump_config(logger):
    global _classifier_config
    logger.info('sample counts:')
    for try_sample_count in _classifier_config.try_sample_counts:
        logger.info(try_sample_count)
    logger.info(f'treatment_phase={_classifier_config.treatment_phase}')
    logger.info(f'categories={_classifier_config.categories}')
    logger.info(f'pos_label={_classifier_config.pos_label}')
    logger.info(f'vectorizer={_classifier_config.vectorizer}')
    logger.info(f'feature_selector={_classifier_config.feature_selector}')
    logger.info(f'scaler={_classifier_config.scaler}')
    logger.info(f'num_folds={_classifier_config.num_folds}')
    logger.info(f'num_iterations={_classifier_config.num_iterations}')
    logger.info(f'threshold_score={_classifier_config.threshold_score}')
    logger.info(f'threshold_train_score={_classifier_config.threshold_train_score}')
    logger.info(f'threshold_f1_macro={_classifier_config.threshold_f1_macro}')
    logger.info(f'xgb_param_dist={_classifier_config.xgb_param_dist}')
    logger.info(f'scorer={_classifier_config.scorer}')
    logger.info(f'lexicon_file={_classifier_config.lexicon_file}')
    logger.info(f'classifier_model_path={_classifier_config.classifier_model_path}')
    logger.info(f'output_dir={_classifier_config.output_dir}')
    logger.info(f'data_dir={_classifier_config.data_dir}')
    logger.info(f'upload_to_s3={_classifier_config.upload_to_s3}')
    logger.info(f'download_s3_features={_classifier_config.download_s3_features}')
    logger.info(f's3_features={_classifier_config.s3_features}')
    logger.info(f'aws_login_url={_classifier_config.aws_login_url}')
    logger.info(f'smtp={_classifier_config.smtp}')
    logger.info(f'notification_email={_classifier_config.notification_email}')
    logger.info(f'terminate_ec2_on_finish={_classifier_config.terminate_ec2_on_finish}')
    logger.info(f'sampled feature CSV file names =datafiles={_classifier_config.datafiles}')
    logger.info(f'feature names={_classifier_config.feature_names}')
    logger.info(f'feature index column={_classifier_config.feature_index_column}')
    logger.info(f'text columns={_classifier_config.text_columns}')
    logger.info(f'key_column={_classifier_config.key_column}')
    logger.info(f'run_mode={_classifier_config.run_mode}')
    logger.info(f'validation_artifacts={_classifier_config.validation_artifacts}')
    logger.info(f'log_dir={_classifier_config.log_dir}')
    logger.info(f'Python Version:{platform.python_version()}')
    logger.info(f'Pandas Version:{pd.__version__}')
    logger.info(f'Numpy Version:{np.__version__}')


def _build_config(config):
    global _classifier_config

    s3_model_description = config.get('s3_model_description')

    start_date = datetime.now()

    model_name = start_date.strftime('%Y%m%d')
    if s3_model_description:
        s3_model_description = re.sub(r'[<>[\]:"/\\|?*]', '_', s3_model_description)
        model_name = f'{model_name}-{s3_model_description}'

    treatment_phase = config['treatment_phase']

    categories = config['categories']

    output_dir = os.path.join(os.environ.get('CLASSIFIER_TRAINING_OUTPUT', None),
                              f'{model_name}-{start_date.strftime("%H-%M-%S.%f")}')

    data_dir = os.environ['CLASSIFIER_TRAINING_DATA']

    classifier_model_path = os.environ['CLASSIFIER_MODEL_PATH']

    lexicon_file = config['lexicon_file']

    try_sample_counts = config['try_sample_counts']

    s3_bucket = os.environ.get('S3_BUCKET')

    upload_to_s3 = config['upload_to_s3']

    download_s3_features = config['download_s3_features']

    s3_features = config.get('s3_features', 'current')

    aws_login_url = config.get('aws_login_url')

    smtp = config.get('smtp')

    notification_email = config.get('notification_email')

    terminate_ec2_on_finish = config.get('terminate_ec2_on_finish')

    pos_label = config.get('pos_label')

    vectorizer = config.get('vectorizer')

    feature_selector = config.get('feature_selector')

    scaler = config.get('scaler')

    num_folds = config.get('num_folds')

    num_iterations = config.get('num_iterations')

    threshold_train_score = config.get('threshold_train_score')

    threshold_score = config.get('threshold_score')

    threshold_f1_macro = config.get('threshold_f1_macro')

    xgb_param_dist = config.get('xgb_param_dist')

    scorer = config.get('scorer')

    overwrite_features = config.get('overwrite_features', False)

    holdout_data_filename = config.get('holdout_data_filename')

    datafiles = config['datafiles']

    feature_names = config['feature_names']

    text_columns = config['text_columns']

    feature_index_column = config['feature_index_column']

    key_column = config['key_column']

    run_mode = config['run_mode']

    validation_artifacts = config.get('validation_artifacts')

    logging_config = config['logging']

    log_dir = os.path.dirname(logging_config['handlers']['file']['filename'])

    _classifier_config = ClassifierConfig(s3_model_description=s3_model_description,
                                          s3_bucket=s3_bucket,
                                          upload_to_s3=upload_to_s3,
                                          download_s3_features=download_s3_features,
                                          pos_label=pos_label,
                                          vectorizer=vectorizer,
                                          feature_selector=feature_selector,
                                          scaler=scaler,
                                          num_folds=num_folds,
                                          num_iterations=num_iterations,
                                          threshold_score=threshold_score,
                                          threshold_train_score=threshold_train_score,
                                          threshold_f1_macro=threshold_f1_macro,
                                          xgb_param_dist=xgb_param_dist,
                                          scorer=scorer,
                                          model_name=model_name,
                                          treatment_phase=treatment_phase,
                                          categories=categories,
                                          output_dir=output_dir,
                                          data_dir=data_dir,
                                          classifier_model_path=classifier_model_path,
                                          lexicon_file=lexicon_file,
                                          try_sample_counts=try_sample_counts,
                                          s3_features=s3_features,
                                          aws_login_url=aws_login_url,
                                          smtp=smtp,
                                          notification_email=notification_email,
                                          terminate_ec2_on_finish=terminate_ec2_on_finish,
                                          overwrite_features=overwrite_features,
                                          holdout_data_filename=holdout_data_filename,
                                          datafiles=datafiles,
                                          feature_names=feature_names,
                                          feature_index_column=feature_index_column,
                                          text_columns=text_columns,
                                          key_column=key_column,
                                          run_mode=run_mode,
                                          validation_artifacts=validation_artifacts,
                                          logging=logging_config,
                                          log_dir=log_dir,
                                          start_date=start_date)

    return _classifier_config
