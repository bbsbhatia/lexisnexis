# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This package provides common utility methods to be used during model
# validation.

from . import holdout_data_validator

validate_classifier_with_holdout_data = holdout_data_validator.run_classifier_test
