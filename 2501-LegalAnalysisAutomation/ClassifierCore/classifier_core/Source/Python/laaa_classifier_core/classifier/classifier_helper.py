import logging
import pickle

import numpy as np
import pandas as pd
import scipy
import sklearn
from sklearn.externals import joblib
import laaa_classifier_core.instrumentation as instrumentation

import threading

class ClassifierHelper:
    """ Class to call a trained classifier

     Parameters
     ----------
     classifier_pickle : str
         The path to pickle file that contains the classifier

     feature_vectorizer_pickle : str
         The path to pickle file that contains the vectorizer for converting text features into vectors

     feature_selector_pickle : str
         The path to pickle file that contains the pickled feature selector

    """

    __lock__ = threading.Lock()

    def __init__(self,
                 myGF,
                 classifier_pickle=None,
                 feature_vectorizer_pickle=None,
                 feature_selector_pickle=None,
                 ApplyWhiteListTreated=False,
                 ApplyBlackListTreated=False
                 ):
        self.myGF = myGF
        self.feature_names = []
        self.feature_data_formatted = []
        self.logger = logging.getLogger('Classifier')
        self.logger.setLevel(logging.INFO)
        self.classifier = classifier_pickle
        self.vectorizer = feature_vectorizer_pickle
        self.feature_selector = feature_selector_pickle
        self.ApplyWhiteListTreated = ApplyWhiteListTreated
        self.ApplyBlackListTreated = ApplyBlackListTreated


    @staticmethod
    def load_pickle_file(file_name, use_job_lib=False):
        with open(file_name, 'rb') as f:
            if use_job_lib:
                return joblib.load(f)
            else:
                return pickle.load(f)

    def generate_TreatedOTF_features(self, input_df, text_columns, other_feature_names):
        """
        Take dataframe containing text of paragraphs along with pre-generated features as input and
        generate and save features to be used for classification.

        """

        self.logger.info("Extracting features from the text training data using a sparse vectorizer")

        text_data = pd.Series(input_df[text_columns].values.tolist()).str.join(' ').values.tolist()

        feature_data_formatted = self.vectorizer.transform(text_data)
        feature_names = self.vectorizer.get_feature_names()

        other_features = scipy.sparse.csr_matrix(input_df[other_feature_names].values)
        feature_data_formatted = scipy.sparse.hstack([feature_data_formatted, other_features])
        feature_names.extend(other_feature_names)
        self.feature_data_formatted = feature_data_formatted
        self.feature_names = feature_names

        scaler = sklearn.preprocessing.StandardScaler(with_mean=False)
        self.feature_data_formatted = scaler.fit_transform(self.feature_data_formatted)
        self.feature_data_formatted = self.feature_selector.transform(self.feature_data_formatted)

        if self.feature_names and len(self.feature_names):
            # keep selected feature names
            self.feature_names = [self.feature_names[i] for i
                                  in self.feature_selector.get_support(indices=True)]
            self.feature_names = np.asarray(self.feature_names)
            self.logger.info(self.feature_names)

    def generate_Treated_features(self, input_df, text_columns, other_feature_names):
        self.generate_TreatedOTF_features(input_df, text_columns, other_feature_names)

    def classify_paragraphs(self, df):
        updated_df, text_columns, other_feature_names = self.myGF.add_features_and_return_all_feature_names(df)
        outcome = self.classify_treated(updated_df, text_columns, other_feature_names)
        updated_df['outcome_Treated'] = outcome
        return updated_df

    @instrumentation.record(subsegment='classifier_helper.classify_treated')
    def classify_treated(self, input_df, text_columns, other_feature_names):
        """
        Take dataframe containing text of paragraphs along with pre-generated features as input and
        generate the classifier output.
        """
        with self.__lock__:
            self.generate_Treated_features(input_df, text_columns, other_feature_names)
            outcome = self.classifier.predict(self.feature_data_formatted)

        # Apply whitelist and blacklist logic
        # Do not change the classifier outcome if (1) both match or (2) neither match
        if (self.ApplyWhiteListTreated or self.ApplyBlackListTreated):
            input_df['_outcome'] = outcome
            input_df['whitelist_treated_outcome'] = self.classify_Treated_whitelist(input_df, text_columns, other_feature_names)
            input_df['blacklist_treated_outcome'] = self.classify_Treated_blacklist(input_df, text_columns, other_feature_names)
            if (self.ApplyWhiteListTreated):
                input_df['_outcome'] = np.where((input_df['whitelist_treated_outcome'] == 0 and input_df['blacklist_treated_outcome'] == 0), 0, input_df['_outcome'])
            if (self.ApplyBlackListTreated):
                input_df['_outcome'] = np.where((input_df['whitelist_treated_outcome'] == 1 and input_df['blacklist_treated_outcome'] == 1), 1, input_df['_outcome'])
            outcome = input_df['_outcome']

        return outcome
