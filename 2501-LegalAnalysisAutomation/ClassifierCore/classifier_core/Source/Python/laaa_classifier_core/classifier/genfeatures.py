# -----------------------------------------------------------------------------
# Preprocess the input data and generate features for machine learning
# The functions in this module are generic and can be used for processing
# text from US CaseLaw documents extracted from MarkLogic via xQuery.
# Features that can be extracted given a paragraph (and its heading, if available)
# Paragraphs are typed as Fact/Discussion/Outcome using heading/classifier
# Does the paragraph have list items like 1., a), (1), 1), etc.
#
# Derived from POC code by Mahesh Pendyala
# Mark Shewhart 26 November 2018
# Revision: 3 December 2018
#   - Added threading on transform function
######################################################
# Here were are making changes outside of source code control
# We will need to incorporate the changes into production code
# The production code is being refactored and cleaned up
# ************
# Revision: 25 February 2019
#   - party1/2 county logic
# -----------------------------------------------------------------------------
import sys
import re
import regex
import numpy as np
import pandas as pd
from multiprocessing import cpu_count, Pool
import nltk
from configparser import ConfigParser
import os
import laaa_classifier_core.instrumentation as instrumentation
from laaa_classifier_core.jobpool import get_pool
import logging


class GenFeatures:
    # precompile any regexes here itself
    def __init__(self,
                 treatment_language_file="TreatmentFeaturesLexicon_XLSX.xlsx",
                 ApplyLexiconTreated=True,
                 OutputLexiconTreatedMatches=False,
                 CountEncounteredPartyMatches=True,
                 OutputEncounteredPartyMatches=False,
                 DebugLexicon=False,
                 ApplyPosTreated=True):
        """
        Usage: Preprocessor(treatment_language_file="TreatmentFeaturesLexicon_XLSX.xlsx",
                            ApplyLexiconTreated=True,
                            OutputLexiconTreatedMatches=False,
                            DebugLexicon=False,
                            ApplyPosTreated=True):

        Preprocessor has the following preprocessing functions:
        clean(paratext)
             removes junk as much as possible. Please see source code
        addMoney(paratext)
            Substitute monies with MONEY token
        addDateTime(paratext)
            Substitute dates with DATE token
            Substitute times with TIME token
        getParaTypeFeature(parahdg, paratext):
            Map paragraph to one of Fact/Discussion/Outcome types
        """

        self.logger = logging.getLogger(__name__)
        self.logger.debug('Initing GenFeatures')
        self.GenFeaturesVersion = 3.0;
        self.GeneratedFeaturesList = [];
        self.ApplyLexiconTreated = ApplyLexiconTreated;
        self.ApplyPosTreated = ApplyPosTreated;
        self.OutputLexiconTreatedMatches = OutputLexiconTreatedMatches;
        self.OutputEncounteredPartyMatches = OutputEncounteredPartyMatches;
        self.CountEncounteredPartyMatches = CountEncounteredPartyMatches;
        self._para_headings();
        self._set_POS_tags();
        self._set_misc_feature_names();
        self._treatment_language(treatment_language_file, DebugLexicon=DebugLexicon);

        ini_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.ini')
        config = ConfigParser()
        config.read(ini_file)
        config.get('anaphorics', 'party_excludes')
        # Need to replace this with a configuration read
        self.party_excludes = config.get('anaphorics', 'party_excludes')
        self.party_exclude_list = self.party_excludes.split('|')

        return

    # Generic parallelize any finction for dataFrame.apply()
    def parallelize(self, data, func, partitions=cpu_count(), n_jobs=cpu_count()):
        self.logger.debug(self.parallelize.__name__)
        self.logger.debug(f'data={data} func={func} partitions={partitions} n_jobs={n_jobs}')
        num_rows = data.shape[0]
        if (partitions > num_rows):
            use_partitions = num_rows
        else:
            use_partitions = partitions
        data_split = np.array_split(data, use_partitions)
        pool = Pool(n_jobs)
        data = pd.concat(pool.map(func, data_split))
        pool.close()
        pool.join()
        return data

    # Load treatment language regex CSV file
    def _treatment_language(self, treatment_language_file, DebugLexicon=False):
        """
        Load treatment language regex CSV file
        """
        if (not self.ApplyLexiconTreated):
            return
        self.logger.debug(f'Load treatment language regex CSV file: {treatment_language_file}')
        self._TreatmentRegexList = []
        self._TreatmentRegexFeatureNameList = []
        self._RegexErrors = []
        self._TreatmentRegexList, self._TreatmentRegexFeatureNameList, self._RegexErrors = self._ReadRegexCsvXls(
            treatment_language_file, DebugLexicon=DebugLexicon)

        lex_dict = {}
        lex_dict_compiled = {}
        for i in range(len(self._TreatmentRegexList)):
            the_regex = self._TreatmentRegexList[i]
            the_feature_names = self._TreatmentRegexFeatureNameList[i]

            feature_list = the_feature_names.split('|')

            for the_feature_name in feature_list:
                if the_feature_name in lex_dict.keys():
                    _lst = []
                    _lst = lex_dict[the_feature_name]
                    _lst.append(the_regex)
                    lex_dict[the_feature_name] = _lst
                else:
                    lex_dict[the_feature_name] = [the_regex]
        other_feature_names = []
        for feat, regexlist in lex_dict.items():
            ComboRegex = self._GenUnionRegex(regexlist)
            lex_dict_compiled[feat] = re.compile(ComboRegex, re.I)
            self.GeneratedFeaturesList.append(feat + '_cnt')
            self.GeneratedFeaturesList.append(feat + '_pct')

        self._CompiledRegex_dict = lex_dict_compiled

    # Define headings map patterns to find para type from heading
    def _para_headings(self):
        """
        Setup heading regex to find para type from heading
        We will use a classifier to classify the type later.
        """
        AND = r'\s+(and|\&)'

        FACT_REGEX = [
            r'findings\s+of\s+fact',
            r'(basic|background)' + AND + r'?(\s+)(procedural\s+history|facts)',
            r'facts(' + AND + r'\s+(background\s+information|arguments))',
            r'facts' + AND + r'\s+procedural\s+(background|history)',
            r'facts' + AND + r'\s+(procedural\s+background|procedure|proceedings(\s+below)?)',
            r'factual\s+(background|sufficiency|summary|legal\s+sufficiency)',
            r'factual(\s+background)?' + AND + r'\s+procedural\s+history',
            r'nature\s+of\s+the\s+case\s+(' + AND + r'\s+background)?',
            r'procedural\s+(history|summary|background)',
            r'relevant\s+(procedural\s+history|uncontested\s+facts|factual' + AND + r'\s+procedural\s+background)',
            r'statement\s+of(\s+relevant)?\s+facts(' + AND + r'\s+proceedings|' + AND + r'\s+procedural\s+history)?',
            r'underlying\s+facts\s+(' + AND + r'(\s+procedural\s+history|\s+proceedings))?',
        ]

        DISCUSSION_REGEX = [
            r'discussion(' + AND + r'\s+decision)?',
            r'(rule|issues|analysis)',
            r'(decree|conclusion)(' + AND + r'\s+order)?',
        ]

        OUTCOME_REGEX = [
            r'conclusions\s+of\s+law',
            r'conclusions\/ultimate\s+facts',
            r'summary\s+of\s+(\w+\s+){0,5}conclusions',
        ]

        self.FACT_REGEX = []
        for reg in FACT_REGEX:
            self.FACT_REGEX.append(regex.compile(reg, flags=regex.I))

        self.DISCUSSION_REGEX = []
        for reg in DISCUSSION_REGEX:
            self.DISCUSSION_REGEX.append(regex.compile(reg, flags=regex.I))

        self.OUTCOME_REGEX = []
        for reg in OUTCOME_REGEX:
            self.OUTCOME_REGEX.append(regex.compile(reg, flags=regex.I))

        self.PTYPE_MAP = {'fact': 1, 'discussion': 2, 'outcome': 3, 'unknown': 0}

    # Read in a CSV of regex's and feature names
    # Return 2 lists
    def _ReadRegexCsvXls(self, fname, DebugLexicon=False):
        # Read the lexicon file into a dataframe
        self.logger.info("ReadRegex: fname=" + fname)

        if fname.lower().endswith("xls") or fname.lower().endswith("xlsx"):
            df = pd.read_excel(fname)
        else:
            df = pd.read_csv(fname)

        # Clean up leading and trailing spaces
        df["Regex"] = df["Regex"].str.strip()
        df["Feature Names (pipe separated)"] = df["Feature Names (pipe separated)"].str.strip()

        # Eliminate rows that are not to be used:
        # commented out, empty regex, empty feature name
        # The regex column is labeled "Regex"
        # the feature name column is labeled "Feature Names (pipe separated)"

        # Filter out rows with no regex
        df = df[df["Regex"].str.len() > 0]

        # Filter out rows with commented out regex
        df = df[df["Regex"].str[0] != '#']

        # Filter out rows with no feature name(s)
        df = df[df["Feature Names (pipe separated)"].str.len() > 0]

        error_regex = []
        if DebugLexicon:
            for index, row in df.iterrows():
                rex = row["Regex"]
                feat = row["Feature Names (pipe separated)"]
                if GenFeatures.regex_contains_capturing_group(rex):
                    error_regex.append(rex)
                    self.logger.error('Regex error - capturing groups are not allowed: ' + str(rex))
                    self.logger.error('For feature(s): ' + str(feat))
                try:
                    re.compile(rex)
                except re.error:
                    error_regex.append(rex)
                    self.logger.error('Regex error - failed to compile: ' + str(rex))
                    self.logger.error('For feature(s): ' + str(feat))

            # Return the dataframe columns as lists

        return (df["Regex"].tolist(), df["Feature Names (pipe separated)"].tolist(), error_regex)

    @staticmethod
    def regex_contains_capturing_group(r):
        escaped = False
        in_set = False
        found_group = False

        for i in range(len(r)):
            char = r[i]
            if escaped:
                escaped = False
            elif char == '\\':
                escaped = True
            elif char == '[':
                in_set = True
            elif char == ']':
                in_set = False
            elif char == '(' and not in_set:
                found_group = True
            elif found_group and char == '?':
                if r[i:i+3] == '?P<':
                    # in a named capturing group
                    return True
                else:
                    # in a non-capturing or other extension group
                    found_group = False
            elif found_group:
                # in a capturing group
                return True

        return found_group

    # Take a list of regular expressions, sort by length, "OR" them together with '|'
    # Return a string that is the union of all regular expressions in the list
    def _GenUnionRegex(self, regex_list):
        sorted_regex_list = regex_list
        sorted_regex_list.sort(key=len, reverse=True)
        combo = ''
        for i in range(len(sorted_regex_list)):
            if (len(combo) > 0):
                combo = combo + '|' + sorted_regex_list[i]
            else:
                combo = sorted_regex_list[i]
        return (combo)

    # Inputs: compiled regular expression and text to match
    # 3 Outputs:
    #    * count of the number of pattern matches in text
    #    * list of strings in text that match the pattern
    #    * string that is a pipe-seperated list representing the list of matches
    def _FindRegexMatches(self, compiled_regex, the_text):
        the_matches = re.findall(compiled_regex, the_text)
        num_matches = len(the_matches)
        matches_str = '|'.join(the_matches)
        return (num_matches, the_matches, matches_str)

    # This may be used in the future to count unique values in a list of lexicon matches
    def _MyUniqueValues(self, l):
        ul = []
        for i in range(len(l)):
            if (not (l[i] in ul)):
                ul.append(l[i])
        return (ul)

    # simple preprocessing function
    # remove useless puntuation
    # fold case, etc.
    def clean(self, text):
        """
        Clean the text and get it ready for further processing.
        """

        # Setup substitution/removal of junk regex in input to make it
        # parser/classifier friendly
        def _clean_regex():
            """
            Setup substitution/removal of junk regex in input to make it
            parser/classifier friendly
            """
            CLEAN_REGEX = {  ## local variable
                r'\s\s+': r' ',  # squeeze white space
                r'\[([0-9])\]': r'(\g<1>)',  # [4] => (4)
                r'\[(.)\]': r'\g<1>',  # [a] => a
                r'\[([a-z]{2,5})\]': r'\g<1>',  # [too] => too
                r"\\'": r"'",  # \' => '
                r'\&#[0-9]+;': r'',  # &#184; =>
                r'\s+\.\s+\.\s+\.': r'...',  # . . . => ...
                r'([.;:,])(\w)': r'\g<1> \g<2>',  # put a space after any of .;:,
            }
            self.CLEAN_REGEX = {}
            for reg, sub in CLEAN_REGEX.items():
                self.CLEAN_REGEX[regex.compile(reg)] = sub

        if (not hasattr(self, 'CLEAN_REGEX')):
            _clean_regex();

        matches = [];
        for cregex, subst in self.CLEAN_REGEX.items():
            matches.extend([m for m in cregex.finditer(text)]);
            text = cregex.sub(subst, text);
        return (text, matches);

    # substitute money values with MONEY
    def addMoney(self, text):
        """
        Substitute money values with MONEY
        E.g., "cost me $800.00 " with "cost me MONEY "
        """

        def _money():
            """
            Set up regexes and other steps
            """
            _SYM = r'\$\s*';
            _DOLLARS = r'[0-9]+(\s*,\s*[0-9]{3})*\s*';
            _PENNYS = r'\.\s*[0-9]*\s*';
            _WRDS = r'(m|M|b|B|tr|Tr)illion\s*'
            MONEY = {  ## local variable
                _SYM + _DOLLARS + _PENNYS + _WRDS: ' MONEYHERE ',
                _SYM + _DOLLARS + _WRDS: ' MONEYHERE ',
                _SYM + _DOLLARS + _PENNYS: ' MONEYHERE ',
                _SYM + _DOLLARS: ' MONEYHERE ',
                _SYM + _PENNYS: ' MONEYHERE ',
            }
            self.MONEY = {}
            for reg, sub in MONEY.items():
                self.MONEY[regex.compile(reg)] = sub

        if (not hasattr(self, 'MONEY')):
            _money();

        matches = []
        for cregex, subst in self.MONEY.items():
            matches.extend([m for m in cregex.finditer(text)]);
            text = cregex.sub(subst, text);
        return (text, matches)

    # substitute dates with DATE and timestamps with TIME
    def addDateTime(self, text):
        """
        Substitute dates with DATE and timestamps with TIME
        E.g., "on 4/4/2018 at 8:00pm" with "on DATE at TIME"
        """

        def _datetime():
            """
            Set up regexes and other steps
            """
            AMPM = r'\s*[ap]\.?m\.?';
            SEP = r'[./-]';
            DAY = r'\b\d{1,2}\b';
            MON = r'\b\d{1,2}\b';
            YEAR = r'\b(\d{2}|\d{4})\b';
            MONTH = r'\b(Jan|January|Feb|February|Mar|March|Apr|April|May|' + \
                    r'Jun|June|Jul|July|Aug|August|Sep|September' + \
                    r'Oct|October|Nov|November|Dec|December)\b';
            WS = r'\s*(,)?\s*'
            DATETIME = {
                r'\b(\d{1,2}\s*:\s+\d{2}' + AMPM + r')\b': r' TIMEHERE ',
                MON + SEP + DAY + SEP + YEAR: r' DATE ',
                MONTH + WS + r'(' + DAY + ')?' + WS + YEAR: r' DATEHERE ',
            }
            self.DATETIME = {}
            for reg, sub in DATETIME.items():
                self.DATETIME[regex.compile(reg, flags=regex.I)] = sub

        if (not hasattr(self, 'DATETIME')):
            _datetime();

        matches = []
        for cregex, subst in self.DATETIME.items():
            matches.extend([m for m in cregex.finditer(text)]);
            text = cregex.sub(subst, text);
        return (text, matches)

    # generate paratype feature: one of FACT/DISCUSSION/OUTCOME
    def getParaTypeFeature(self, parahdg, paratext):
        """
        See if para heading matches know para headings. If yes, return the type
        Returns one of ["fact", "discussion", "outcome", "unknown"]
        """
        # if parahdg is empty, return 'unknown' for now.
        # if parahdg is present and is of known type return its type
        # else use a prebuilt classfier to find type of paratext
        if (isinstance(parahdg, str) and len(parahdg) > 0):
            # matches = self.HeadingRecognizer.find(parahdg.lower())
            # return (matches[0][3] if (len(matches) > 0) else "unknown")
            matches = []
            for cregex in self.FACT_REGEX:
                if (cregex.search(parahdg) is not None):
                    return self.PTYPE_MAP["fact"]
            for cregex in self.DISCUSSION_REGEX:
                if (cregex.search(parahdg) is not None):
                    return self.PTYPE_MAP["discussion"]
            for cregex in self.OUTCOME_REGEX:
                if (cregex.search(parahdg) is not None):
                    return self.PTYPE_MAP["outcome"]
        return self.PTYPE_MAP["unknown"]

    # This is where we select the POS tags from NLTK that we want to count
    # It is a dictionary  where key=NLTK POS tag; value=True/False
    def _set_POS_tags(self):
        use_pos_dict = {}
        if (self.ApplyPosTreated):
            use_pos_dict['CC'] = True  # coordinating conjunction
            use_pos_dict['CD'] = True  # cardinal digit
            use_pos_dict['DT'] = True  # determiner
            use_pos_dict['EX'] = True  # existential there (like: “there is” … think of it like “there exists”)
            use_pos_dict['FW'] = True  # foreign word
            use_pos_dict['IN'] = True  # preposition/subordinating conjunction
            use_pos_dict['JJ'] = True  # adjective ‘big’
            use_pos_dict['JJR'] = True  # adjective, comparative ‘bigger’
            use_pos_dict['JJS'] = True  # adjective, superlative ‘biggest’
            use_pos_dict['LS'] = True  # list marker 1)
            use_pos_dict['MD'] = True  # modal could, will
            use_pos_dict['NN'] = True  # noun, singular ‘desk’
            use_pos_dict['NNS'] = True  # noun plural ‘desks’
            use_pos_dict['NNP'] = True  # proper noun, singular ‘Harrison’
            use_pos_dict['NNPS'] = True  # proper noun, plural ‘Americans’
            use_pos_dict['PDT'] = True  # predeterminer ‘all the kids’
            use_pos_dict['POS'] = True  # possessive ending parent’s
            use_pos_dict['PRP'] = True  # personal pronoun I, he, she
            use_pos_dict['PRP$'] = True  # possessive pronoun my, his, hers
            use_pos_dict['RB'] = True  # adverb very, silently,
            use_pos_dict['RBR'] = True  # adverb, comparative better
            use_pos_dict['RBS'] = True  # adverb, superlative best
            use_pos_dict['RP'] = True  # particle give up
            use_pos_dict['TO,'] = True  # to go ‘to’ the store.
            use_pos_dict['UH'] = True  # interjection, errrrrrrrm
            use_pos_dict['VB'] = True  # verb, base form take
            use_pos_dict['VBD'] = True  # verb, past tense took
            use_pos_dict['VBG'] = True  # verb, gerund/present participle taking
            use_pos_dict['VBN'] = True  # verb, past participle taken
            use_pos_dict['VBP'] = True  # verb, sing. present, non-3d take
            use_pos_dict['VBZ'] = True  # verb, 3rd person sing. present takes
            use_pos_dict['WDT'] = True  # wh-determiner which
            use_pos_dict['WP'] = True  # wh-pronoun who, what
            use_pos_dict['WP$'] = True  # possessive wh-pronoun whose
            use_pos_dict['WRB'] = True  # wh-abverb where, when

        self.Use_POS_dict = use_pos_dict

        for pos, use in self.Use_POS_dict.items():
            if (use and self.ApplyPosTreated):
                key_ct = 'pos_ct_' + pos
                key_pct = 'pos_pct_' + pos
                self.GeneratedFeaturesList.append(key_ct)
                self.GeneratedFeaturesList.append(key_pct)

        # Now combine some parts of speech
        if (self.ApplyPosTreated):
            self.GeneratedFeaturesList.append('grp_pos_ADJ_ct')
            self.GeneratedFeaturesList.append('grp_pos_ADJ_pct')

            self.GeneratedFeaturesList.append('grp_pos_NOUN_ct')
            self.GeneratedFeaturesList.append('grp_pos_NOUN_pct')

            self.GeneratedFeaturesList.append('grp_pos_PP_ct')
            self.GeneratedFeaturesList.append('grp_pos_PP_pct')

            self.GeneratedFeaturesList.append('grp_pos_ADV_ct')
            self.GeneratedFeaturesList.append('grp_pos_ADV_pct')

            self.GeneratedFeaturesList.append('grp_pos_ADVCOMP_ct')
            self.GeneratedFeaturesList.append('grp_pos_ADVCOMP_pct')

            self.GeneratedFeaturesList.append('grp_pos_V_ct')
            self.GeneratedFeaturesList.append('grp_pos_V_pct')

            self.GeneratedFeaturesList.append('grp_pos_VPRES_ct')
            self.GeneratedFeaturesList.append('grp_pos_VPRES_pct')

            self.GeneratedFeaturesList.append('grp_pos_VPAST_ct')
            self.GeneratedFeaturesList.append('grp_pos_VPAST_pct')

    def _set_misc_feature_names(self):
        # Sentence and token counts
        self.GeneratedFeaturesList.append('num_sents')
        self.GeneratedFeaturesList.append('num_tokens')
        self.GeneratedFeaturesList.append('avg_sent_tokens')

        # Misc features
        self.GeneratedFeaturesList.append('perc_p')
        self.GeneratedFeaturesList.append('ptype')
        self.GeneratedFeaturesList.append('num_money')
        self.GeneratedFeaturesList.append('num_dates')

        if (self.CountEncounteredPartyMatches):
            self.GeneratedFeaturesList.append('encparty1ct')
            self.GeneratedFeaturesList.append('encparty1ct_pct')
            self.GeneratedFeaturesList.append('encparty2ct')
            self.GeneratedFeaturesList.append('encparty2ct_pct')

    def sum_pos(self, row, pos_list):
        ct = 0
        for pos in pos_list:
            if ((pos in self.Use_POS_dict.keys()) and (self.Use_POS_dict[pos])):
                key_ct = 'pos_ct_' + pos
                ct = ct + row[key_ct]
        return (ct)

    def safe_pct(self, num, den, default_pct=0.0, max_pct=1.0, min_pct=0.0):
        pct = default_pct
        if (den != 0):
            pct = num / den
        if (pct > max_pct):
            pct = max_pct
        if (pct < min_pct):
            pct = min_pct
        return (pct)

    def normText(self, text):
        pat1 = '\\W+'
        pat1_compiled = re.compile(pat1, re.M)
        pat1_replace = '_'
        norm = re.sub(pat1_compiled, pat1_replace, text)
        return (norm)

    def normParty(self, text):
        pat1 = '\\W+'
        pat1_compiled = re.compile(pat1, re.M)
        pat1_replace_space = ' '
        pat1_replace_underscore = '_'
        norm = re.sub(pat1_compiled, pat1_replace_space, text)
        norm = norm.strip()
        norm = re.sub(pat1_compiled, pat1_replace_underscore, norm)
        return (norm)

    def countEncparty(self, encparty, norm_text):
        encparty_ct = 0
        matches = []
        matches_str = ''
        if (len(encparty) == 0):
            return (encparty_ct, matches, matches_str)
        encpartyList = encparty.split('|')
        norm_encpartyList = []
        for party in encpartyList:
            if (not (party in self.party_exclude_list)):
                norm = party.strip()
                norm = '_' + self.normParty(norm) + '_'
                norm_encpartyList.append(norm)
        if (len(norm_encpartyList) == 0):
            return (encparty_ct, matches, matches_str)

        encparty_regex = '|'.join(norm_encpartyList)
        bad_regex = False
        try:
            encparty_regex_compiled = re.compile(encparty_regex, re.M)
        except re.error:
            bad_regex = True
            print('Encountered party regex error:' + str(encparty_regex))
        except:
            bad_regex = True
            print('Encountered unexpected error:' + str(encparty_regex))

        if (bad_regex):
            return (encparty_ct, matches, matches_str)

        encparty_ct, matches, matches_str = self._FindRegexMatches(encparty_regex_compiled, norm_text)

        return (encparty_ct, matches, matches_str)

    # Generate the features by adding then to a single row
    def transform(self, row):
        # This is how we allow input text that has already had features generated
        # to be used as input for testing and training

        row['GenFeaturesVersion'] = self.GenFeaturesVersion

        hdg = row['last-head'];
        para = row['ctext']

        perc_p = self.safe_pct(int(row['pnum']), int(row['ptot']))
        row['perc_p'] = perc_p;

        para, matches = self.clean(para);

        ptype = self.getParaTypeFeature(hdg, para);
        row['ptype'] = ptype;

        para, matches = self.addMoney(para)
        row['num_money'] = len(matches);

        para, matches = self.addDateTime(para)
        row['num_dates'] = len(matches);

        sentence_list = nltk.tokenize.sent_tokenize(para)
        sentence_ct = len(sentence_list)
        num_tokens = 0
        POS_dict = {}
        for sent in sentence_list:
            tokens = nltk.word_tokenize(sent)
            num_tokens = num_tokens + len(tokens)
            if (self.ApplyPosTreated):
                POS_list = nltk.pos_tag(tokens)
                for (word, pos) in POS_list:
                    if ((pos in self.Use_POS_dict.keys()) and (self.Use_POS_dict[pos])):
                        if pos in POS_dict.keys():
                            POS_dict[pos] = POS_dict[pos] + 1
                        else:
                            POS_dict[pos] = 1
        if (self.ApplyPosTreated):
            for pos, use in self.Use_POS_dict.items():
                if use:
                    hold_ct = 0
                    if (pos in POS_dict.keys()):
                        hold_ct = POS_dict[pos]
                    hold_pct = self.safe_pct(hold_ct, num_tokens)
                    key_ct = 'pos_ct_' + pos
                    key_pct = 'pos_pct_' + pos
                    row[key_ct] = hold_ct
                    row[key_pct] = hold_pct

        # Now combine some parts of speech
        if (self.ApplyPosTreated):
            row['grp_pos_ADJ_ct'] = self.sum_pos(row, ['JJ', 'JJR', 'JJS'])
            row['grp_pos_ADJ_pct'] = self.safe_pct(row['grp_pos_ADJ_ct'], num_tokens)

            row['grp_pos_NOUN_ct'] = self.sum_pos(row, ['NN', 'NNS', 'NNP', 'NNPS'])
            row['grp_pos_NOUN_pct'] = self.safe_pct(row['grp_pos_NOUN_ct'], num_tokens)

            row['grp_pos_PP_ct'] = self.sum_pos(row, ['PRP', 'PRP$'])
            row['grp_pos_PP_pct'] = self.safe_pct(row['grp_pos_PP_ct'], num_tokens)

            row['grp_pos_ADV_ct'] = self.sum_pos(row, ['RB', 'RBR', 'RBS'])
            row['grp_pos_ADV_pct'] = self.safe_pct(row['grp_pos_ADV_ct'], num_tokens)

            row['grp_pos_ADVCOMP_ct'] = self.sum_pos(row, ['RBR', 'RBS'])
            row['grp_pos_ADVCOMP_pct'] = self.safe_pct(row['grp_pos_ADVCOMP_ct'], num_tokens)

            row['grp_pos_V_ct'] = self.sum_pos(row, ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ'])
            row['grp_pos_V_pct'] = self.safe_pct(row['grp_pos_V_ct'], num_tokens)

            row['grp_pos_VPRES_ct'] = self.sum_pos(row, ['VBG', 'VBP', 'VBZ'])
            row['grp_pos_VPRES_pct'] = self.safe_pct(row['grp_pos_VPRES_ct'], num_tokens)

            row['grp_pos_VPAST_ct'] = self.sum_pos(row, ['VB', 'VBD', 'VBN'])
            row['grp_pos_VPAST_pct'] = self.safe_pct(row['grp_pos_VPAST_ct'], num_tokens)

        # Sentence and token counts
        row['num_sents'] = sentence_ct

        row['num_tokens'] = num_tokens

        if (self.CountEncounteredPartyMatches):
            norm_text = '_' + self.normParty(para) + '_'
            row['encparty1ct'], matches, matches_str = self.countEncparty(row['encparty1'], norm_text)
            row['encparty1ct_pct'] = self.safe_pct(row['encparty1ct'], num_tokens)
            if (self.OutputEncounteredPartyMatches):
                row['encparty1_str'] = matches_str
            row['encparty2ct'], matches, matches_str = self.countEncparty(row['encparty2'], norm_text)
            row['encparty2ct_pct'] = self.safe_pct(row['encparty2ct'], num_tokens)
            if (self.OutputEncounteredPartyMatches):
                row['encparty2_str'] = matches_str

        # Error - changed row['avg_sent_tokens'] = self.safe_pct(num_tokens , sentence_ct)
        if (sentence_ct > 0):
            row['avg_sent_tokens'] = num_tokens / sentence_ct
        else:
            row['avg_sent_tokens'] = 0

        # Apply lexicons
        # This generates the features defined in the treatment lexicon CSV file
        if (self.ApplyLexiconTreated):
            for feat, compiled_regex in self._CompiledRegex_dict.items():
                num_trt_matches, trt_matches, trt_matches_str = self._FindRegexMatches(compiled_regex, para)
                row[feat + '_cnt'] = num_trt_matches
                pct_matches = self.safe_pct(num_trt_matches, num_tokens)
                row[feat + '_pct'] = pct_matches
                if (self.OutputLexiconTreatedMatches):
                    row[feat + '_matches'] = trt_matches_str

        row['ctext'] = para
        return row;

    # transform DataFrame
    # takes a pandas.DataFrame and applies transform on each row.
    # returns transformed DataFrame.
    def transform_df_base(self, data_frame):
        self.logger.debug(self.transform_df_base.__name__)
        data_frame = data_frame.apply(self.transform, axis=1)
        return data_frame

    def transform_df_multiprocess(self, data_frame, max_partitions):
        self.logger.debug(self.transform_df_multiprocess.__name__)
        partitions = cpu_count()
        if max_partitions is not None and partitions > max_partitions:
            partitions = max_partitions
        num_rows = data_frame.shape[0]
        if partitions > num_rows:
            partitions = num_rows
        data_split = np.array_split(data_frame, partitions)
        return pd.concat(get_pool().map(self.transform_df_base, data_split))

    # This is the method called by the code used to train and test classifiers
    # Thread the transform_df_base() function
    def transform_df(self, data_frame, max_partitions=4):
        self.logger.debug(self.transform_df.__name__)
        data_frame = self.transform_df_multiprocess(data_frame, max_partitions)
        return data_frame

    # The function is to unit test this class
    # It can also be used to call to convert an input_csv to output_csv
    def csv_transform_df(self, input_csv_filename, output_csv_filename):
        import pandas as pd
        input_df = pd.read_csv(input_csv_filename, sep=',', encoding='utf-8')
        input_df['encparty1'] = input_df['encparty1'].astype(str)
        input_df['encparty2'] = input_df['encparty2'].astype(str)
        input_df['encparty1'] = np.where((input_df['encparty1'] == 'nan'), '', input_df['encparty1'])
        input_df['encparty2'] = np.where((input_df['encparty2'] == 'nan'), '', input_df['encparty2'])
        output_df = self.transform_df(input_df)
        output_df.to_csv(output_csv_filename, sep=',', index=False, encoding='utf-8')

    # The function is to return a list of Generated features
    # It will be used by the data reader for adding to the "other_features" output list
    # of features to use by classifier
    def GetGeneratedFeatures(self):
        return self.GeneratedFeaturesList

    @instrumentation.record(subsegment='GenFeatures.add_features')
    def add_features(self, df, max_partitions=4):
        # Adds columns to the input dataframe
        # These are additional features used by various classifiers
        # For now, we will generate these during read in of input CSV
        # Returns a new dataframe
        self.logger.debug(self.add_features.__name__)
        df_enh = df

        # To avoid errors, let's explicitely caste the text input as string
        df_enh['ctext'] = df_enh['ctext'].astype(str)

        df_enh['encparty1'] = df_enh['encparty1'].astype(str)
        df_enh['encparty2'] = df_enh['encparty2'].astype(str)
        df_enh['encparty1'] = np.where((df_enh['encparty1'] == 'nan'), '', df_enh['encparty1'])
        df_enh['encparty2'] = np.where((df_enh['encparty2'] == 'nan'), '', df_enh['encparty2'])

        df_enh = self.transform_df(df_enh, max_partitions)

        generated_feature_names = self.GetGeneratedFeatures()

        return df_enh, generated_feature_names

    def add_features_and_return_all_feature_names(self, df):
        df, generated_feature_names = self.add_features(df)

        # return the list of text columns we want to vectorize in this phase
        text_columns = ['ctext']

        orig_feature_names = ['courtlevel', 'casecct', 'statcct', 'crcct', 'regcct', 'adcct', 'constcct',
                              'juryinstrcct', 'sesslawcct', 'patentcct', 'tmarkcct', 'copyrightcct',
                              'lawrevcct', 'periodicalcct', 'bookcct', 'oagcct', 'annotcct', 'execdoccct',
                              'misccct', 'formcct', 'cct', 'len', 'ptot', 'pnum']

        other_feature_names = orig_feature_names + generated_feature_names

        return df, text_columns, other_feature_names


if (__name__ == "__main__"):
    infname = 'First1000.csv'
    inpath = '/data/home/ilabs/ShepardsPOC2018/project/FOnly/Holdout2Top10/'
    outfname = 'GenFeat_' + infname
    outpath = '/data/home/ilabs/ShepardsPOC2018/project/FOnly/output/debug/'

    redo_infname = outfname
    redo_inpath = outpath
    redo_outfname = 'GenFeat_' + infname
    redo_outpath = '/data/home/ilabs/ShepardsPOC2018/project/FOnly/output/debug/'
    redo_input_csv = inpath + infname
    redo_output_csv = outpath + outfname

    if (len(sys.argv) > 1):
        input_csv = sys.argv[1]
    else:
        input_csv = inpath + infname
    if (len(sys.argv) > 2):
        output_csv = sys.argv[2]
    else:
        output_csv = outpath + outfname
    print('input_csv:', input_csv)
    print('output_csv:', output_csv)

    # Initiate the GenFeatures class
    myGF = GenFeatures(treatment_language_file="BadRegexTreatmentFeaturesLexicon_XLSX.xlsx",
                       ApplyLexiconTreated=True,
                       DebugLexicon=True,
                       OutputLexiconTreatedMatches=True,
                       ApplyPosTreated=False)
    print('GENERATED FEATURES LIST:')
    print(myGF.GetGeneratedFeatures())
    # Call the debug methos that takes input CSV file and outputs a CSV file with generated features
    myGF.csv_transform_df(input_csv, output_csv)

    # Test applying to already generated input
    # myGF.csv_transform_df(redo_input_csv, redo_output_csv)
