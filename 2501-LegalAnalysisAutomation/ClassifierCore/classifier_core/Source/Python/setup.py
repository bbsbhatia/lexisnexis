import re
from os import getenv

from setuptools import setup, find_packages

install_requirements = [
    'Flask==1.0.2',
    'aws_xray_sdk==2.4.2',
    'nltk==3.4',
    'numpy==1.16.2',
    'pandas==0.24.2',
    'regex==2019.03.12',
    'scikit_learn==0.20.3',
    'scipy==1.2.1',
    'xlrd>0.9.0',
    'xgboost==0.81',
    'boto3',
    'psutil==5.6.3',
    'botocore',
    'openpyxl==3.0',
    'PrettyTable'
]


test_requirements = [
    'pytest']


def build_version():
    major = '2'
    minor = '2'
    rev = getenv('BUILD_NUMBER', '0')  # This is provided as a Jenkins Global Variable in the build system
    branch = getenv('GIT_BRANCH', 'local')  # This is provided as a Jenkins Global Variable when using Pipeline SCM

    base_version = '.'.join([major, minor])

    if branch == 'master':
        return f'{base_version}.{rev}'
    else:
        sanitized_branch = re.sub(r'[^a-zA-Z0-9]+', '.', branch)
        return f'{base_version}.dev{rev}+{sanitized_branch}'


setup(
    name='laaa_classifier_core',
    packages=find_packages(),
    package_data={'':['*.ini']},
    version=build_version(),
    description='Packages that are common to other 2501 Projects',
    author='Sameer Bhatia',
    author_email='sameer.bhatia@lexisnexis.com',
    url='https://tfs-glo-lexisadvance.visualstudio.com/DefaultCollection/Content/_git/2501-LegalAnalysisAutomatedAssist',
    keywords=['laaa_classifier_core'],  # arbitrary keywords
    extras_require={'test': test_requirements},
    install_requires=install_requirements
)
