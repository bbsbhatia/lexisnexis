import scipy.stats as stats
from numpy import ndarray
from sklearn.feature_extraction.text import HashingVectorizer, TfidfVectorizer
from sklearn.feature_selection import SelectKBest, chi2, SelectPercentile, f_classif, VarianceThreshold
from sklearn.preprocessing import StandardScaler, RobustScaler
from sklearn.metrics import make_scorer

from laaa_classifier_core.training.modelgen.base_classifier import BaseClassifier
from laaa_classifier_core.classifier_training.scorer_factory import custom_recall_score


def test_load_param_distributions():
    param_config = {
        "value": 0,
        "dict": {'one': 1, 'two': 2},
        "list": [3, 4],
        "distribution": {
            "scipy.stats.norm": []
        },
        "distribution_args": {
            "scipy.stats.randint": [2, 20]
        },
        "distribution_kwargs": {
            "scipy.stats.expon": {"scale": 0.1}
        },
        "rvs_value": {
            "scipy.stats.norm.rvs": 10
        },
        "rvs_args": {
            "scipy.stats.randint.rvs": [1, 2, 200, 100]
        },
        "rvs_kwargs": {
            "scipy.stats.randint.rvs": {
                "low": 3,
                "high": 4,
                "size": 200
            }
        }
    }
    param_dist = BaseClassifier.load_param_distributions(param_config)
    assert param_dist['value'] == 0
    assert param_dist['dict']['one'] == 1
    assert param_dist['list'][0] == 3
    assert isinstance(param_dist['distribution'].dist, type(stats.norm))
    assert isinstance(param_dist['distribution_args'].dist, type(stats.randint))
    assert param_dist['distribution_args'].args == (2, 20)
    assert isinstance(param_dist['distribution_kwargs'].dist, type(stats.expon))
    assert param_dist['distribution_kwargs'].kwds == {"scale": 0.1}
    assert isinstance(param_dist['rvs_value'], float)
    assert isinstance(param_dist['rvs_args'], ndarray)
    assert param_dist['rvs_args'].size == 100
    assert param_dist['rvs_args'].min() == 201
    assert param_dist['rvs_args'].max() == 201
    assert isinstance(param_dist['rvs_kwargs'], ndarray)
    assert param_dist['rvs_kwargs'].size == 200
    assert param_dist['rvs_kwargs'].min() == 3
    assert param_dist['rvs_kwargs'].max() == 3


def test_load_vectorizer():
    v_config_tfidf = {
        "type": "TfidfVectorizer",
        "params": {
            "sublinear_tf": True,
            "max_df": 0.86,
            "ngram_range": [1, 5],
            "max_features": 4000,
            "min_df": 25
        }
    }
    v_tfidf = BaseClassifier.load_vectorizer(v_config_tfidf)
    assert isinstance(v_tfidf, TfidfVectorizer)
    assert v_tfidf.max_df == 0.86

    v_config_hashing = {
        "type": "HashingVectorizer",
        "params": {
            "ngram_range": [1, 5],
            "n_features": 4000,
        }
    }
    v_hashing = BaseClassifier.load_vectorizer(v_config_hashing)
    assert isinstance(v_hashing, HashingVectorizer)
    assert v_hashing.ngram_range == [1, 5]


def test_load_feature_selector():
    fs_config = {
        "type": "SelectKBest",
        "params": {
            "score_func": "chi2",
            "k": 3000
        }
    }
    fs = BaseClassifier.load_feature_selector(fs_config)
    assert isinstance(fs, SelectKBest)
    assert fs.score_func == chi2
    assert fs.k == 3000

    fs_config = {
        "type": "SelectPercentile",
        "params": {
            "score_func": "f_classif",
            "percentile": .6
        }
    }
    fs = BaseClassifier.load_feature_selector(fs_config)
    assert isinstance(fs, SelectPercentile)
    assert fs.score_func == f_classif

    fs_config = {
        "type": "VarianceThreshold",
        "params": {
            "threshold": .7
        }
    }
    fs = BaseClassifier.load_feature_selector(fs_config)
    assert isinstance(fs, VarianceThreshold)
    assert fs.threshold == .7


def test_load_scaler():
    config = {
        "type": "StandardScaler",
        "params": {
            "with_mean": False
        }
    }
    scaler = BaseClassifier.load_scaler(config)
    assert isinstance(scaler, StandardScaler)
    assert scaler.with_mean is False

    config = {
        "type": "RobustScaler",
        "params": {
            "quantile_range": [.5, 95.2]
        }
    }
    scaler = BaseClassifier.load_scaler(config)
    assert isinstance(scaler, RobustScaler)
    assert scaler.quantile_range == [.5, 95.2]


def test_load_scorer():
    config = {
        'func': 'custom_recall_score'
    }
    scorer = BaseClassifier.load_scorer(config)
    assert repr(make_scorer(custom_recall_score, pos_label=1)) == repr(scorer)

    scorer = BaseClassifier.load_scorer(config, pos_label=2)
    assert repr(make_scorer(custom_recall_score, pos_label=2)) == repr(scorer)

    config = {
        'func': 'custom_recall_score',
        'params': {
            'pos_label': 3
        }
    }
    scorer = BaseClassifier.load_scorer(config)
    assert repr(make_scorer(custom_recall_score, pos_label=3)) == repr(scorer)

    scorer = BaseClassifier.load_scorer(config, pos_label=4)
    assert repr(make_scorer(custom_recall_score, pos_label=3)) == repr(scorer)
