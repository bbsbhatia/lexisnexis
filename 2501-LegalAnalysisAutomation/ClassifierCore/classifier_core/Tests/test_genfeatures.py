import os
import pandas as pd
import pytest
import subprocess
import shutil
from laaa_classifier_core.classifier import GenFeatures


@pytest.fixture(scope="session")
def nltk():
    subprocess.call(['python', '-m', 'nltk.downloader', '-d', os.environ['NLTK_DATA'], 'averaged_perceptron_tagger'])
    subprocess.call(['python', '-m', 'nltk.downloader', '-d', os.environ['NLTK_DATA'], 'punkt'])
    yield
    shutil.rmtree(os.environ['NLTK_DATA'])


def get_gen_features(ApplyLexiconTreated=True,CountEncounteredPartyMatches=True,ApplyPosTreated=True):
    return GenFeatures(treatment_language_file=os.environ['CLASSIFIER_MODEL_PATH'] + "/treatment_features_lexicon.xlsx", ApplyLexiconTreated=ApplyLexiconTreated,
                       CountEncounteredPartyMatches = CountEncounteredPartyMatches, ApplyPosTreated=ApplyPosTreated)


def test_norm_text():
    assert 'a_s_df_l_kj_asdf' == get_gen_features().normText('a.s/df;l[]kj];[][;/.asdf')


def test_norm_party():
    assert 'Rafizadeh_In_re_Cyrus_II_Pship' == get_gen_features().normParty('Rafizadeh (In re Cyrus II P''ship)')


def test_safe_pct():
    gf = get_gen_features()
    assert 0 == gf.safe_pct(100, 0)
    assert .25 == gf.safe_pct(1, 4)
    assert .22 == gf.safe_pct(1, 4, max_pct=.22)
    assert .23 == gf.safe_pct(1, 5, min_pct=.23)


def test_sum_pos():
    row = pd.Series()
    row['pos_ct_JJ'] = 3
    row['pos_ct_JJR'] = 1
    row['pos_ct_JJS'] = 0
    row['pos_ct_JJX'] = 5
    assert 4 == get_gen_features().sum_pos(row, ['JJ', 'JJR', 'JJS','xsdy'])


def test_transform(nltk):
    def create_test_row():
        row = pd.Series()
        row['last-head'] = 'I. PROCEDURAL HISTORY'
        row['ctext'] = 'Fred Spicer was found guilty of    capital murder and sentenced to death in the Circuit Court of George County for killing Edmund Hebert. Spicer appealed his verdict and sentence, both of which were affirmed by the Mississippi Supreme Court. CASECITEHERE. Spicer then filed for post-conviction relief, and the Mississippi Supreme Court granted his petition, in part. CASECITEHERE. Specifically, the court found that Spicer had made a sufficient showing that his trial counsel was ineffective at the sentencing phase of his trial to be entitled to an evidentiary hearing on that issue in the trial court. Relief was denied on all of Spicer''s other claims, including his claim that his trial attorneys were ineffective during the guilt phase of his trial.'
        row['pnum'] = 2
        row['ptot'] = 65
        row['encparty1'] = '|Spicer|Spicer'
        row['encparty2'] = '|State|State'
        return row;

    gf = get_gen_features()
    row = create_test_row()
    gf.transform(row)
    assert 'Fred Spicer was found guilty of capital murder and sentenced to death in the Circuit Court of George County for killing Edmund Hebert. Spicer appealed his verdict and sentence, both of which were affirmed by the Mississippi Supreme Court. CASECITEHERE. Spicer then filed for post-conviction relief, and the Mississippi Supreme Court granted his petition, in part. CASECITEHERE. Specifically, the court found that Spicer had made a sufficient showing that his trial counsel was ineffective at the sentencing phase of his trial to be entitled to an evidentiary hearing on that issue in the trial court. Relief was denied on all of Spicers other claims, including his claim that his trial attorneys were ineffective during the guilt phase of his trial.' == row['ctext']
    assert 4 == row['encparty1ct']
    assert 0.030303030303030304 == row['encparty1ct_pct']
    assert 0 == row['encparty2ct']
    assert 0 == row['encparty2ct_pct']
    assert 0.03076923076923077 == row['perc_p']
    assert 1 == row['ptype']
    assert 0 == row['num_money']
    assert 0 == row['num_dates']
    assert 7 == row['num_sents']
    assert 132 == row['num_tokens']
    assert 18.857142857142858 == row['avg_sent_tokens']
    assert 7 == row['grp_pos_ADJ_ct']
    assert 0.05303030303030303 == row['grp_pos_ADJ_pct']
    assert 46 == row['grp_pos_NOUN_ct']
    assert 0.3484848484848485 == row['grp_pos_NOUN_pct']
    assert 7 == row['grp_pos_PP_ct']
    assert 0.05303030303030303 == row['grp_pos_PP_pct']
    assert 2 == row['grp_pos_ADV_ct']
    assert 0.015151515151515152 == row['grp_pos_ADV_pct']
    assert 0 == row['grp_pos_ADVCOMP_ct']
    assert 0 == row['grp_pos_ADVCOMP_pct']
    assert 20 == row['grp_pos_V_ct']
    assert 0.15151515151515152 == row['grp_pos_V_pct']
    assert 3 == row['grp_pos_VPRES_ct']
    assert 0.022727272727272728 == row['grp_pos_VPRES_pct']
    assert 17 == row['grp_pos_VPAST_ct']
    assert 0.12878787878787878 == row['grp_pos_VPAST_pct']
    # Now test with some of the calculations turned off.
    gf = get_gen_features(ApplyPosTreated=False, CountEncounteredPartyMatches=False,ApplyLexiconTreated=False )
    row = create_test_row()
    gf.transform(row)

    assert 'Fred Spicer was found guilty of capital murder and sentenced to death in the Circuit Court of George County for killing Edmund Hebert. Spicer appealed his verdict and sentence, both of which were affirmed by the Mississippi Supreme Court. CASECITEHERE. Spicer then filed for post-conviction relief, and the Mississippi Supreme Court granted his petition, in part. CASECITEHERE. Specifically, the court found that Spicer had made a sufficient showing that his trial counsel was ineffective at the sentencing phase of his trial to be entitled to an evidentiary hearing on that issue in the trial court. Relief was denied on all of Spicers other claims, including his claim that his trial attorneys were ineffective during the guilt phase of his trial.' == row['ctext']
    assert 'encparty1ct' not in row.keys()
    assert 'encparty1ct_pct' not in row.keys()
    assert 'encparty2ct' not in row.keys()
    assert 'encparty2ct_pct' not in row.keys()
    assert 0.03076923076923077 == row['perc_p']
    assert 1 == row['ptype']
    assert 0 == row['num_money']
    assert 0 == row['num_dates']
    assert 7 == row['num_sents']
    assert 132 == row['num_tokens']
    assert 18.857142857142858 == row['avg_sent_tokens']
    assert 'grp_pos_ADJ_ct' not in row.keys()
    assert 'grp_pos_ADJ_pct' not in row.keys()
    assert 'grp_pos_NOUN_ct' not in row.keys()
    assert 'grp_pos_NOUN_pct' not in row.keys()
    assert 'grp_pos_PP_ct' not in row.keys()
    assert 'grp_pos_PP_pct' not in row.keys()
    assert 'grp_pos_ADV_ct' not in row.keys()
    assert 'grp_pos_ADV_pct' not in row.keys()
    assert 'grp_pos_ADVCOMP_pct' not in row.keys()
    assert 'grp_pos_ADVCOMP_pct' not in row.keys()
    assert 'grp_pos_V_ct' not in row.keys()
    assert 'grp_pos_V_pct' not in row.keys()
    assert 'grp_pos_VPRES_ct' not in row.keys()
    assert 'grp_pos_VPRES_pct' not in row.keys()
    assert 'grp_pos_VPAST_ct' not in row.keys()
    assert 'grp_pos_VPAST_pct' not in row.keys()


party_test_results = [
    ('|Spicer|Spicer', 4, 'Fred Spicer was found guilty of capital murder and sentenced to death in the Circuit Court of George County for killing Edmund Hebert. Spicer appealed his verdict and sentence, both of which were affirmed by the Mississippi Supreme Court. CASECITEHERE. Spicer then filed for post-conviction relief, and the Mississippi Supreme Court granted his petition, in part. CASECITEHERE. Specifically, the court found that Spicer had made a sufficient showing that his trial counsel was ineffective at the sentencing phase of his trial to be entitled to an evidentiary hearing on that issue in the trial court. Relief was denied on all of Spicer''s other claims, including his claim that his trial attorneys were ineffective during the guilt phase of his trial.'),
    ('|', 0, 'Paragraph Text'),
    ('', 0, 'Paragraph Text'),
    ('State', 0, 'State vs Jones')
]


@pytest.mark.parametrize('parties, expected_counts, text', party_test_results)
def test_count_enc_party(parties, expected_counts, text):
    gf = get_gen_features()
    normalized_text = gf.normParty(text)
    encparty_ct,matches,matches_str = gf.countEncparty(parties, normalized_text)
    assert expected_counts == encparty_ct


def test_get_para_type_feature():
    gf = get_gen_features()
    assert gf.PTYPE_MAP['fact'] == gf.getParaTypeFeature('I. PROCEDURAL HISTORY','Text')
    assert gf.PTYPE_MAP['discussion'] == gf.getParaTypeFeature('II. ISSUES','Text')
    assert gf.PTYPE_MAP['unknown'] == gf.getParaTypeFeature('III. Stuff','Text')


def test_clean():
    gf = get_gen_features()
    text, matches = gf.clean('[a][1] This   will  be  cleaned.')
    assert 'a(1) This will be cleaned.' == text
    assert 5 == len(matches)
    text, matches = gf.clean('[c][5] This will  be  cleaned;and this.')
    assert 'c(5) This will be cleaned; and this.' == text
    assert 5 == len(matches)


def test_add_money():
    gf = get_gen_features()
    text, matches = gf.addMoney('Defendant was charged $1,000,000.')
    assert 'Defendant was charged  MONEYHERE ' == text
    assert 1 == len(matches)
    text, matches = gf.addMoney('Plaintiff was awarded $5,030,000 for damages.')
    assert 'Plaintiff was awarded  MONEYHERE for damages.' == text
    assert 1 == len(matches)


def test_add_time():
    gf = get_gen_features()
    text, matches = gf.addDateTime('on 4/4/2018 at 8: 00pm')
    assert 'on  DATE  at  TIMEHERE ' == text
    assert 2 == len(matches)
    text, matches = gf.addDateTime('on April 5, 2018')
    assert 'on  DATEHERE ' == text
    assert 1 == len(matches)


def test_regex_contains_capturing_group():
    capturing_group_tests = [
        # True - contains capturing groups
        (r'\W(court|circuit) (defines|defined)', True),
        (r'priority of patents.{1,500}(PATENTCITEHERE)', True),
        (r'(capturing|group)', True),
        (r'(?P<named>group)', True),
        (r'\\(literal|backslash)', True),
        (r'\\\\(literal backslashes)', True),
        (r'\[(capturing after escaped [', True),
        (r'\[capturing(after escaped]', True),
        (r'[set](capturing after set', True),
        (r'open paren(', True),
        (r'[(](capturing after paren in set', True),
        (r'not at beginning(capturing|group)', True),
        (r'not at beginning(?P<named>group)', True),
        (r'not at beginning\\(literal|backslash)', True),
        (r'not at beginning\\\\(literal backslashes)', True),
        (r'not at beginning\[(capturing after escaped [', True),
        (r'not at beginning\[capturing(after escaped]', True),
        (r'not at beginning[set](capturing after set', True),
        (r'not at beginning[(](capturing after paren in set', True),

        # False - does not contain capturing groups
        (r'\W(?:court|circuit) (?:defines|defined)', False),
        (r'priority of patents.{1,500}PATENTCITEHERE,', False),
        (r'priority of patents.{1,500}(?:PATENTCITEHERE)', False),
        (r'(?:non|capturing|group)', False),
        (r'\(escaped paren\)', False),
        (r'\\\(literal \ then escaped paren\)', False),
        (r'[(]paren in set', False),
        (r'no paren', False),
        (r'[paren(not at beginning of set]', False),
        (r'\\[literal \ before set with ( in it]', False),
        (r'incomplete extension group(?',  False),
        (r'not at beginning(?:non|capturing|group)', False),
        (r'not at beginning\(escaped paren\)', False),
        (r'not at beginning\\\(literal \ then escaped paren\)', False),
        (r'not at beginning[(]paren in set', False),
        (r'not at beginning[paren(not at beginning of set]', False),
        (r'not at beginning\\[literal \ before set with ( in it]', False),
    ]

    for test, result in capturing_group_tests:
        assert result == GenFeatures.regex_contains_capturing_group(test)
