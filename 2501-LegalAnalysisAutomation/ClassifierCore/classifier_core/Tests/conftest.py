from pathlib import Path
import sys
import os

if os.environ.get('JENKINS_URL'):
    collect_ignore_glob = ["test_*.py"]

sys.path.append(os.path.join(Path(__file__).parent.parent,'Source','Python'))

if os.environ.get('CLASSIFIER_STDOUT'):
    del os.environ['CLASSIFIER_STDOUT']

os.environ['NLTK_DATA'] = os.path.join(Path(__file__).parent,'NLTKData')
os.environ['TEST_DATA_PATH'] = os.path.join(Path(__file__).parent,'Data')
