import pytest
from unittest.mock import patch, Mock
from laaa_classifier_core.classifier_training import scorer_factory as sf
from sklearn.metrics import get_scorer, make_scorer, recall_score, fbeta_score


def test_scorer_factory():
    assert repr(make_scorer(recall_score, pos_label=0)) == repr(sf.get_scorer('recall', pos_label=0))
    assert repr(get_scorer('neg_log_loss')) == repr(sf.get_scorer('neg_log_loss'))
    assert repr(make_scorer(fbeta_score, beta=0.5, pos_label=1)) == repr(sf.get_scorer('fbeta_score', beta=0.5))
    assert repr(make_scorer(sf.custom_precision_score, pos_label=1)) == repr(sf.get_scorer('custom_precision_score'))
    assert repr(make_scorer(sf.custom_recall_score, pos_label=0)) == repr(
        sf.get_scorer('custom_recall_score', config_pos_label=0))
    assert repr(make_scorer(sf.custom_f1_score, pos_label=0)) == repr(
        sf.get_scorer('custom_f1_score', config_pos_label=2, **{'pos_label': 0}))

    with pytest.raises(ValueError) as excinfo:
        sf.get_scorer('BAD_SCORER')
    assert "'BAD_SCORER' is not a valid scoring value." in str(excinfo.value)


def test_scorer_factory_def():
    mock_estimator = Mock()
    func_str = ('def a_custom_score_func(y_true, y_pred, val, exp):\n'
                '   return val ** exp')
    scorer = sf.get_scorer(func_str, exp=3, val=2)
    assert 'make_scorer(a_custom_score_func, exp=3, val=2)' == repr(scorer)
    assert 8 == scorer(mock_estimator, None, None)
    mock_estimator.predict.assert_called()


def test_custom_precision_score():
    assert 1.0 == sf.custom_precision_score([1, 1, 1], [1, 0, 0])
    assert 0.5 == sf.custom_precision_score([1, 0, 1], [1, 1, 0])
    assert 0.0 == sf.custom_precision_score([0, 0, 0], [1, 1, 1])


def test_custom_recall_score():
    assert 1.0 == sf.custom_recall_score([1, 1, 0], [1, 1, 1])
    assert 0.5 == sf.custom_recall_score([1, 0, 1], [1, 1, 0])
    assert 0.0 == sf.custom_recall_score([1, 1, 0], [0, 0, 0])


def test_custom_f1_score():
    assert 1.0 == sf.custom_f1_score([1, 1, 1], [1, 1, 1])
    assert 0.5 == sf.custom_f1_score([1, 1, 1], [1, 0, 0])
    assert 0.0 == sf.custom_f1_score([1, 1, 0], [0, 0, 1])


@patch('.'.join([sf.__name__, sf.precision_recall_fscore_support.__name__]))
def test_custom_precision_score_thresholds(mock_func):
    mock_func.return_value = (0.1, 0.2, 0.3, 0)
    assert 0.1 == sf.custom_precision_score(None, None)
    assert 0.1 == sf.custom_precision_score(None, None, threshold_fail_score=0.4, precision_min=0.1)
    assert 0.4 == sf.custom_precision_score(None, None, threshold_fail_score=0.4, precision_min=0.5)
    assert 0.1 == sf.custom_precision_score(None, None, threshold_fail_score=0.4, recall_min=0.2)
    assert 0.4 == sf.custom_precision_score(None, None, threshold_fail_score=0.4, recall_min=0.5)
    assert 0.1 == sf.custom_precision_score(None, None, threshold_fail_score=0.4, f1_min=0.3)
    assert 0.4 == sf.custom_precision_score(None, None, threshold_fail_score=0.4, f1_min=0.5)


@patch('.'.join([sf.__name__, sf.precision_recall_fscore_support.__name__]))
def test_custom_recall_score_thresholds(mock_func):
    mock_func.return_value = (0.1, 0.2, 0.3, 0)
    assert 0.2 == sf.custom_recall_score(None, None)
    assert 0.2 == sf.custom_recall_score(None, None, threshold_fail_score=0.4, precision_min=0.1)
    assert 0.4 == sf.custom_recall_score(None, None, threshold_fail_score=0.4, precision_min=0.5)
    assert 0.2 == sf.custom_recall_score(None, None, threshold_fail_score=0.4, recall_min=0.2)
    assert 0.4 == sf.custom_recall_score(None, None, threshold_fail_score=0.4, recall_min=0.5)
    assert 0.2 == sf.custom_recall_score(None, None, threshold_fail_score=0.4, f1_min=0.3)
    assert 0.4 == sf.custom_recall_score(None, None, threshold_fail_score=0.4, f1_min=0.5)


@patch('.'.join([sf.__name__, sf.precision_recall_fscore_support.__name__]))
def test_custom_f1_score_thresholds(mock_func):
    mock_func.return_value = (0.1, 0.2, 0.3, 0)
    assert 0.3 == sf.custom_f1_score(None, None)
    assert 0.3 == sf.custom_f1_score(None, None, threshold_fail_score=0.4, precision_min=0.1)
    assert 0.4 == sf.custom_f1_score(None, None, threshold_fail_score=0.4, precision_min=0.5)
    assert 0.3 == sf.custom_f1_score(None, None, threshold_fail_score=0.4, recall_min=0.2)
    assert 0.4 == sf.custom_f1_score(None, None, threshold_fail_score=0.4, recall_min=0.5)
    assert 0.3 == sf.custom_f1_score(None, None, threshold_fail_score=0.4, f1_min=0.3)
    assert 0.4 == sf.custom_f1_score(None, None, threshold_fail_score=0.4, f1_min=0.5)
