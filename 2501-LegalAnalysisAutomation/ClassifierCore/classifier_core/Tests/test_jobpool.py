from laaa_classifier_core.jobpool import init_pool, get_pool
import multiprocessing
from unittest.mock import patch


def test_pool():
    init_pool()
    pool = get_pool()
    assert pool is not None
    assert pool._processes is multiprocessing.cpu_count()
