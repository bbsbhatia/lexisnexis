export EC2_HOME=/home/ec2-user
export NLTK_HOME=${EC2_HOME}/nltk_data
export VERSION_FILE=${EC2_HOME}/classifier/src/python/static/version.txt
export CLASSIFIER_MODEL_PATH=${EC2_HOME}/classifiermodel
/home/ec2-user/venv/py37classifier/bin/python3.7 /home/ec2-user/classifier/src/python/treatment_classifier_service.py
exit 1