export EC2_HOME=/home/ec2-user
export NLTK_HOME=${EC2_HOME}/nltk_data
export VERSION_FILE=${EC2_HOME}/classifier/src/python/static/version.txt
export CLASSIFIER_MODEL_PATH=${EC2_HOME}/classifiermodel
source /home/ec2-user/venv/py37classifier/bin/activate
NLTK_HOME=/home/ec2-user/nltk_data  CLASSIFIER_MODEL_PATH=/home/ec2-user/classifiermodel uwsgi --master --socket 0.0.0.0:8080 --processes 2 --protocol=http --chdir /home/ec2-user/classifier/src/python --wsgi-file /home/ec2-user/classifier/src/python/treatment_classifier_service.py
exit 1