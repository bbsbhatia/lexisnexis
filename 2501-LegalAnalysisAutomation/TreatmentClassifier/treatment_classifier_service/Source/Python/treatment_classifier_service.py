from flask import Flask, request, json, Response
from pandas.io.json import json_normalize
from configparser import ConfigParser
from pathlib import Path
import logging
import os
import laaa_classifier_core.instrumentation as instrumentation
from http import HTTPStatus
from laaa_classifier_core.classifier import GenFeatures, ClassifierHelper
from laaa_classifier_core.jobpool import init_pool
import psutil

"""treatment_classifier_service is a REST API running in Flask that receives paragraph data and makes a
call to the classifier and returns whether or not the paragraph might have treatment in it.
"""

app = Flask(__name__)
instrumentation.configure(app, 'treatment-classifier-service')

EXPECTED_PARAGRAPH_PROPERTIES = ["para-id", "pnum", "cct", "casecct", "statcct", "crcct", "regcct", "adcct",
                                 "constcct", "juryinstrcct", "sesslawcct", "patentcct", "tmarkcct", "copyrightcct",
                                 "lawrevcct", "periodicalcct", "bookcct", "oagcct", "annotcct", "execdoccct", "misccct",
                                 "formcct", "textlen"]
PROPERTIES_THAT_CAN_BE_NULL = ["encparty1", "encparty2", "last-head", "ctext"]
COLUMNS_TO_CAST_TO_INTS = ["cct", "casecct", "statcct", "crcct", "regcct", "adcct", "constcct", "juryinstrcct",
                           "sesslawcct", "patentcct", "tmarkcct", "copyrightcct", "lawrevcct", "periodicalcct",
                           "bookcct", "oagcct", "annotcct", "execdoccct", "misccct", "formcct", "ptot", "pnum", "len",
                           "courtlevel"]
TOP_LEVEL_ATTRIBUTES_TO_COPY = ["courtlevel", "ptot"]
TOP_LEVEL_ATTRIBUTES = ["courtlevel", "ptot", "paragraphs","lni"]
CLASSIFIER_VERSION = "1.0"


def record_startup(response, options):
    metadata = dict()
    metadata['model_directory'] = os.environ['CLASSIFIER_MODEL_PATH']
    metadata['model_file'] = config.get('classifier', 'model_file')
    metadata['vectorized_pickle_file'] = config.get('classifier', 'vectorized_pickle_file')
    metadata['feature_selector_pickle_file'] = config.get('classifier', 'feature_selector_pickle_file')
    metadata['lexicon_file'] = config.get('classifier', 'lexicon_file')
    non_searchable_data = {'metadata': metadata}
    return instrumentation.Record(non_searchable=non_searchable_data)


@instrumentation.record(record_startup, segment='Startup')
def singleton():
    init_pool()
    read_configuration()
    load_pickle_files()


@instrumentation.record(subsegment='read_configuration')
def read_configuration():
    """Reads the configuration file into memory"""
    global config
    logger = logging.getLogger('Classifier')
    try:
        # Read configuration from the config file.
        ini_file = os.path.join(os.path.dirname(Path(__file__)), 'config.ini')
        config = ConfigParser()
        config.read(ini_file)
    except Exception as error:
        logger.exception("Unexpected error reading config.ini", error)
        raise

    try:
        # Pull in Configuration from classifiermodel.txt
        model_config_file = os.path.join(os.path.dirname(Path(__file__).parent.parent), 'classifiermodel.txt')
        config.add_section('classifier')
        with open(model_config_file) as file:
            for line in file:
                key_value = line.strip().split('=', 1)
                config.set('classifier', key_value[0], key_value[1])
    except Exception as error:
        logger.exception("Unexpected error reading classifiermodel.txt", error)
        raise


@instrumentation.record(subsegment='load_pickle_files')
def load_pickle_files():
    """Loads the pickle file into memory so that it isn't read on each invocation."""

    global class_model_dir, treated_class_pickle, treated_vect_pickle, treated_feat_sel_pickle
    try:
        class_model_dir = os.environ['CLASSIFIER_MODEL_PATH'] + "/"
        treated_class_pickle = ClassifierHelper.load_pickle_file(
            class_model_dir + config.get('classifier', 'model_file'),
            use_job_lib=True)
        treated_vect_pickle = ClassifierHelper.load_pickle_file(
            class_model_dir + config.get('classifier', 'vectorized_pickle_file'))
        treated_feat_sel_pickle = ClassifierHelper.load_pickle_file(
            class_model_dir + config.get('classifier', 'feature_selector_pickle_file'))
    except Exception as error:
        logger = logging.getLogger('Classifier')
        logger.exception("Unexpected error loading Pickle Files", error)
        raise


def get_classifier_helper(ApplyLexiconTreated=True, OutputLexiconTreatedMatches=False, ApplyPosTreated=True):
    myGF = GenFeatures(
        ApplyLexiconTreated=ApplyLexiconTreated,
        OutputLexiconTreatedMatches=OutputLexiconTreatedMatches,
        ApplyPosTreated=ApplyPosTreated,
        treatment_language_file=class_model_dir + config.get('classifier', 'lexicon_file'))

    return ClassifierHelper(myGF, treated_class_pickle, treated_vect_pickle, treated_feat_sel_pickle)


def check_invalid_fields(list_of_invalid_fields, return_dict):
    """Checks if the passed in list of invalid fields is greater than zero and updates the return dictionary
    to include the list of fields that were required.
    """
    if len(list_of_invalid_fields) > 0:
        return_dict["error"] = "MISSING_REQUIRED_FIELDS"
        return_dict["fields"] = list_of_invalid_fields
        return False;
    return True;


def cast_to_int(paragraph_dict, paragraph_result_dict):
    """Casts the columns to ints that are numbers.  Updates any fields that are in error."""
    paragraph_data_valid = True
    list_of_invalid_fields = []
    # Cast Columns to Ints
    for column in COLUMNS_TO_CAST_TO_INTS:
        try:
            paragraph_dict[column] = int(paragraph_dict[column])
        except ValueError as e:
            paragraph_data_valid = False
            list_of_invalid_fields.append(column)

    if len(list_of_invalid_fields) > 0:
        paragraph_result_dict["error"] = "NUMBER_EXPECTED_FOR_FIELD"
        paragraph_result_dict["fields"] = list_of_invalid_fields
    return paragraph_data_valid


def validate_properties_exist(collection_to_validate, keys_to_validate):
    """Looks to verify a property was passed in.  These values are allowed to be null which is the
    distinction for the validate_properties_are_not_null method that requires the properties to be not null."""
    invalid_fields = []
    for key in keys_to_validate:
        if key not in collection_to_validate:
            invalid_fields.append(key)
    return invalid_fields


def validate_properties_are_not_null(collection_to_validate, fields_to_validate):
    """Determines if a field exists in the input JSON"""
    invalid_fields = []
    for field in fields_to_validate:
        v = collection_to_validate.get(field)
        if not isinstance(v, (list, tuple)):
            if not (v and str(v).strip()):
                invalid_fields.append(field)
        elif len(v) == 0:
            invalid_fields.append(field)
    return invalid_fields


def validate_paragraph_properties(paragraph_dict, paragraph_result_dict):
    """Validates both properties that can be null and ones that must not be null and will return both to the caller
    if they both exist"""
    invalid_fields = []
    invalid_fields.extend(validate_properties_are_not_null(paragraph_dict, EXPECTED_PARAGRAPH_PROPERTIES))
    invalid_fields.extend(validate_properties_exist(paragraph_dict, PROPERTIES_THAT_CAN_BE_NULL))
    return check_invalid_fields(invalid_fields, paragraph_result_dict)


@instrumentation.record(subsegment='call_classifier')
def call_classifier(valid_paragraphs):
    """Calls the classifier with paragraphs that have valid data"""
    # Code to convert JSON Paragraphs into a dataframe
    df = json_normalize(valid_paragraphs)
    return get_classifier_helper().classify_paragraphs(df)


def update_classifier_results(row, result):
    """Updates the return JSON to include the results from the classifier."""
    result["para-id"] = row["para-id"]
    if "outcome_Treated" not in row:
        result["error"] = "CLASSIFIER_DID_NOT_RETURN_TREATMENT_INFORMATION"
        return False
    elif row["outcome_Treated"]:
        result["hasTreatment"] = "N"
    else:
        result["hasTreatment"] = "Y"

    return True


def get_memory():
    process = psutil.Process(os.getpid())
    return process.memory_info().rss


def recorder_classify_treatment_segment(response, options):
    """Creates XRay Instrumentation data."""
    searchable_data = {'lni': request.json.get('lni','Missing'), 'status': response.status}
    metadata = {'version': CLASSIFIER_VERSION}
    if response.status_code == HTTPStatus.OK:
        results = response.json["results"]
        metadata['number_of_paragraphs'] = len(results)
        treatment_count = 0
        for result in results:
            if result["hasTreatment"] == 'Y':
                treatment_count += 1
        metadata['number_of_paragraphs_with_treatment'] = treatment_count
    else:
        metadata['request'] = request.json
        metadata['response'] = response.json

    metadata['current_memory'] = get_memory()
    non_searchable_data = {'metadata': metadata}
    return instrumentation.Record(searchable=searchable_data,
                                  non_searchable=non_searchable_data)


@app.route("/citations/classifier/treatment", methods=['POST'])
@instrumentation.record(recorder_classify_treatment_segment)
def classify_treatment():
    """Endpoint to invoke the treatment classifier"""
    return_dictionary = dict()
    try:
        status = HTTPStatus.OK
        results = []
        invalid_top_level_fields = validate_properties_are_not_null(request.json, TOP_LEVEL_ATTRIBUTES)

        valid_paragraphs = []

        if check_invalid_fields(invalid_top_level_fields, return_dictionary):
            # Copy Data from root element to child elements and verify paragraph data.
            for paragraph_dict in request.json["paragraphs"]:
                for field in TOP_LEVEL_ATTRIBUTES_TO_COPY:
                    paragraph_dict[field] = request.json[field]

                # Verify Paragraph Data.  Create a list with good paragraphs.  Add bad Paragraph Data to the result.
                paragraph_result_dict = {}
                paragraph_data_valid = False
                if validate_paragraph_properties(paragraph_dict, paragraph_result_dict):
                    paragraph_dict["len"] = paragraph_dict["textlen"]
                    paragraph_data_valid = cast_to_int(paragraph_dict, paragraph_result_dict)

                if paragraph_data_valid:
                    valid_paragraphs.append(paragraph_dict)
                else:
                    status = HTTPStatus.BAD_REQUEST
                    if "para-id" in paragraph_dict:
                        paragraph_result_dict["para-id"] = paragraph_dict["para-id"]
                    results.append(paragraph_result_dict)

            if valid_paragraphs:
                df = call_classifier(valid_paragraphs)
                for index, row in df.iterrows():
                    result = {}
                    if not update_classifier_results(row, result):
                        status = HTTPStatus.BAD_REQUEST
                    results.append(result)
            return_dictionary["results"] = results
        else:
            status = HTTPStatus.BAD_REQUEST
    except Exception as e:
        return_dictionary["error"] = repr(e);
        status = HTTPStatus.INTERNAL_SERVER_ERROR
    return_dictionary["version"] = CLASSIFIER_VERSION
    return Response(response=json.dumps(return_dictionary),
                    status=status,
                    mimetype='application/json')


@app.route('/health')
def health_check():
    """Health Check endpoint
    """
    return Response(response='Application is Healthy!', status=200, mimetype='text/plain')


@app.route('/version')
def version():
    """
    Provide version information for the build.
    File version.txt is created during Jenkins build execution
    and made available in static folder under the current path.
    """
    return app.send_static_file('version.txt')


if __name__ == "__main__":
    singleton()
    app.run(host=config.get('service', 'host'), port=config.get('service', 'port'))
