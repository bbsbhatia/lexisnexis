This is the Shepards Treatment Classifier Service.  It is invoked using a POST request to the resource "/citations/classifier/treatment".  Port to use is 8000.

Design Doc is found here...
https://reedelsevier.sharepoint.com/:w:/r/sites/LNGOTeamDept/CPS/contentfamilies/Shared%20Documents/Cases/NARS/Automated%20Identification%20of%20Shepards%20Treatment%20Production/Classifier/Treatment%20Classifier%20Rest%20API.docx?d=wbcf723a57e1e4aacbf6bc82ae8623ef0&csf=1&e=nH29WX

The Themis wiki is also found here:
https://wiki.regn.net/wiki/Themis_-_EPIC_E-06644_-_Automated_Identification_of_Shepard%27s_Treatment_%26_Workflow_-_Production_Phase_1
