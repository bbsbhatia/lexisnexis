import treatment_classifier_service
import unittest
import json
import os
import shutil
from pandas.io.json import json_normalize
from unittest.mock import patch, call
from http import HTTPStatus
import subprocess
import laaa_classifier_core.instrumentation as instrumentation


class TestTreatmentClassifierService(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        treatment_classifier_service.singleton()
        subprocess.call(['python', '-m', 'nltk.downloader', '-d', os.environ['NLTK_DATA'], 'averaged_perceptron_tagger'])
        subprocess.call(['python', '-m', 'nltk.downloader', '-d', os.environ['NLTK_DATA'], 'punkt'])

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(os.environ['NLTK_DATA'])

    def setUp(self):
        self.client = treatment_classifier_service.app.test_client()

    def tearDown(self):
        pass

    @staticmethod
    def readJsonData(filename):
        """Method to read in a JSON file and parse it"""
        with open(os.path.join(os.environ['TEST_DATA_PATH'], filename)) as f:
            data = json.load(f)
        return json.dumps(data)

    def test_version(self):
        """Tests the version endpoint."""
        response = self.client.get('/version')
        assert HTTPStatus.NOT_FOUND == response.status_code
    
    def test_health(self):
        """Tests the Health check endpoint."""
        response = self.client.get('/health')
        assert b'Application is Healthy!' == response.data
        assert HTTPStatus.OK == response.status_code

    @patch.object(instrumentation, 'record_data')
    @patch('treatment_classifier_service.get_memory')
    def test_classify_treatment_missing_top_level_data(self, mock_get_memory, mock_instrumentation):
        """Testing with only a small amount of the JSON that is required at the top level of the JSON structure.
           Also testing the XRay calls here for a bad request."""
        mock_get_memory.return_value = 1
        self.call_treatment_service_with_input_and_output_files('input_missing_top_level_fields.json',
                                                                'output_missing_top_level_fields.json', HTTPStatus.BAD_REQUEST)
        expected_calls = [call('lni', 'Missing', True),
                          call('status', '400 BAD REQUEST', True),
                          call('metadata', {'version': '1.0', 'request': {'courtlevel': '3'},
                                            'response': {'error': 'MISSING_REQUIRED_FIELDS', 'fields': ['ptot', 'paragraphs', 'lni'], 'version': '1.0'}, 'current_memory': 1}, False)]
        mock_instrumentation.assert_has_calls(expected_calls)

    @patch.object(instrumentation, 'record_data')
    @patch('treatment_classifier_service.get_memory')
    def test_full_input_happy_path(self, mock_get_memory, mock_instrumentation):
        """Test with good JSON input that yields proper output"""
        mock_get_memory.return_value = 1
        self.call_treatment_service_with_input_and_output_files('input_full_65_paragraphs.json', 'output_full_65_paragraphs.json', HTTPStatus.OK)
        expected_calls = [call('lni', '5TGW-4821-DXHD-G2KC-00000-00', True),
                          call('status', '200 OK', True),
                          call('metadata', {'version': '1.0', 'number_of_paragraphs': 65, 'number_of_paragraphs_with_treatment': 48, 'current_memory': 1}, False)]
        mock_instrumentation.assert_has_calls(expected_calls)

    @patch.object(instrumentation, 'record_data')
    @patch('treatment_classifier_service.get_memory')
    def test_happy_path_1_paragraph(self, mock_get_memory, mock_instrumentation):
        """Test with good JSON input that yields proper output.  Also verifying XRay"""
        mock_get_memory.return_value = 1
        self.call_treatment_service_with_input_and_output_files('input_1_paragraph.json', 'output_1_paragraph.json', HTTPStatus.OK)
        expected_calls = [call('lni', '5TGW-4821-DXHD-G2KC-00000-00', True),
                          call('status', '200 OK', True),
                          call('metadata', {'version': '1.0', 'number_of_paragraphs': 1, 'number_of_paragraphs_with_treatment': 1, 'current_memory': 1}, False)]
        mock_instrumentation.assert_has_calls(expected_calls)

    def test_paragraphs_missing_data(self):
        """Testing with paragraphs that are missing data"""
        self.call_treatment_service_with_input_and_output_files('input_missing_required_data_2_paragraphs.json', 'output_missing_requried_data_2_paragraphs.json',HTTPStatus.BAD_REQUEST)

    def test_no_paragraphs(self):
        """Testing with no paragraph data"""
        self.call_treatment_service_with_input_and_output_files('input_no_paragraphs.json', 'output_no_paragraphs.json', HTTPStatus.BAD_REQUEST)

    def test_cast_exception(self):
        """Testing a bad input scenario of fields that should be numbers but are alpha."""
        self.call_treatment_service_with_input_and_output_files('input_cast_exception.json', 'output_cast_exception.json', HTTPStatus.BAD_REQUEST)

    def test_empty_ctext(self):
        """CText (Paragraph text) can be empty in some documents. Valid Scenario"""
        self.call_treatment_service_with_input_and_output_files('input_empty_ctext.json',
                                                                'output_empty_ctext.json', HTTPStatus.OK)

    def test_missing_ctext(self):
        """CText attribute can be empty, but is required."""
        self.call_treatment_service_with_input_and_output_files('input_missing_ctext.json',
                                                                'output_missing_ctext.json', HTTPStatus.BAD_REQUEST)


    @patch('laaa_classifier_core.classifier.ClassifierHelper.classify_paragraphs')
    def test_exception(self, mock_classifier_helper):
        """Mocking an exception to get code coverage on the catch all"""
        mock_classifier_helper.side_effect = KeyError('foo')
        self.call_treatment_service_with_input_and_output_files('input_full_65_paragraphs.json',
                                                                'output_mock_exception.json', HTTPStatus.INTERNAL_SERVER_ERROR)
        assert mock_classifier_helper.called

    @patch('laaa_classifier_core.classifier.ClassifierHelper.classify_paragraphs')
    def test_bad_classifier_data(self, mock_shep_service):
        """Mocking a case where the classifier does not return the outcome_treated field.
        Not expected to actuall happen."""
        mocked_df = json_normalize(json.loads(TestTreatmentClassifierService.readJsonData('mocked_classifier_return.json')))
        mock_shep_service.return_value = mocked_df

        self.call_treatment_service_with_input_and_output_files('input_full_65_paragraphs.json',
                                                                'output_missing_classifier_treatment_info.json', HTTPStatus.BAD_REQUEST)
        assert mock_shep_service.called

    def call_treatment_service_with_input_and_output_files(self, input_file, output_file, expected_status):
        """Method to invoke the test client with JSON Input and the expected output and status code.
        We are asserting that the dictionaries of the JSON are equal so that we are not comparing string literals and
        so that if there are issues, the error printed will give us feedback on what exactly is wrong."""
        response = self.client.post('/citations/classifier/treatment', data=TestTreatmentClassifierService.readJsonData(input_file),
                                    content_type='application/json')
        expected_response_data = json.loads(TestTreatmentClassifierService.readJsonData(output_file))
        response_data_as_dict = json.loads(response.data)
        self.assertDictEqual(expected_response_data, response_data_as_dict)
        assert expected_status == response.status_code


if __name__ == "__main__":
    unittest.main()
