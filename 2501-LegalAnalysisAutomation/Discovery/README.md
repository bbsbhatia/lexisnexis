# Introduction 
This repository contains scripts / utilities / notebooks for Themis related POC work.

# Getting Started with Jupyter Notebooks
## Activate Python Virtual Env
C:\>C:\COTS\anaconda3\Scripts\activate.bat C:\COTS\anaconda3\envs\py37

## Launch Notebook
(py37) C:\>jupyter notebook --notebook-dir=c:\dev\work\git\2501-Themis-POC\TriggerLanguageAnalysis
