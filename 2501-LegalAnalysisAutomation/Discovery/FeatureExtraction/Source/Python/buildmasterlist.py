"""
    Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
    
    FeatureExtractorBuildCsv is used to build the feature dictionary list from the AWS lambda response
    This also uses the FeatureExtractorXmlUtil in order to parse the response with the features we want in the individual
    paragraphs.
"""
from xmlutil import *


class FeatureExtractorBuildMasterList:

    def __init__(self):
        """
        Creates a new instance.
        """
        self._feature_list = []

    def get_feature_list(self):
        return self._feature_list

    def set_feature_list(self, feature_list):
        self._feature_list = feature_list

    def convert_to_dictionary(self, list_of_dictionaries):
        """
        Returns a single dictionary from a list of dictionaries
        :param list_of_dictionaries: A list of dictionaries that need to be convert to a single dictionary
        :return: A single dictionary
        """
        return dict((key, d[key]) for d in list_of_dictionaries for key in d)

    def build_feature_list_dictionaries(self, response):
        """
        Builds the feature attributes dictionaries and sets the instance variable based on the XML string passed
        :param response: XML String that was received from the lambda function
        """
        xml_util = FeatureExtractorXmlUtil()
        list_of_feature_dictionaries = xml_util.find_element_attributes(
            'paragraph', response, searchchildren=True)
        list_of_document_dictionary = xml_util.find_element_attributes(
            'document', response, searchchildren=False)
        document_attrib_dictionary = self.convert_to_dictionary(
            list_of_document_dictionary)

        for d in list_of_feature_dictionaries:
            d.update(document_attrib_dictionary)

        self.set_feature_list(list_of_feature_dictionaries)
