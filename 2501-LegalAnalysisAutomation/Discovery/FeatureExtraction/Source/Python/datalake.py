""""
Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.

This module provides an API to get objects from data lake.

For more information refer to https://wiki.regn.net/wiki/DataLake_Hello_World
"""

import requests
import json


def get_objects(): 
    headers = {
        'Accept': 'application/json',
        'x-api-key': 'xsaXQ5CAlG7MdygCF963k7YuRi4pFFA6aILxqqzl'
    }
    api_url = 'https://datalake-staging.content.aws.lexis.com/objects/v1?collection-id=M2019060801'
    response = requests.request('GET', api_url, headers=headers)
    return json.loads(response.text)


class DataLakeCollection:
    """
        DataLakeCollection provides mechanism to get objects
        from a specified collection in a given region.
    """
    def __init__(self, api_key, collection, region='prod'):
        """
        Create a new instance.

        :type api_key: string
        :param api_key: key for accessing data lake
        :type collection: string
        :param collection: collection name 
        :type region: string
        :param region: region selector: ['prod', 'staging']
        """        
        self.api_key = api_key
        self.collection = collection
        self.region = region
        self.objects = []
        self.state = 'INIT'
        self.next_token = None
        
    def _get_object_batch(self):

        headers = {
            'Accept': 'application/json',
            'x-api-key': self.api_key
        }
        
        params = {
            'collection-id': self.collection,
            'state': 'Created',
            'max-items': 10
        }
        
        if self.next_token:
            params['next-token'] = self.next_token

        try:
            self.state = 'IN_PROGRESS'
            datalake = 'datalake' if self.region == 'prod' else 'datalake-staging'
            api_url = f'https://{datalake}.content.aws.lexis.com/objects/v1'        
            response = requests.request('GET', api_url, headers=headers, params=params)  
            print('\nDatalake Request: {}'.format(response.request.url))
            if response.ok:
                dl_response = json.loads(response.text)
                self.next_token = dl_response.get('next-token')
                self.objects = [ d['object-key-url'] for d in dl_response['objects'] ]
                if not self.next_token:
                    self.state = 'COMPLETE' 
            else:
                error = json.loads(response.text)['error']
                print('Datalake error: {}'.format(error))
        except Exception as e:
                print('Datalake error: {}'.format(e))

    def object_keys(self):
        """
        Get keys for all the objects in collection. This is implemented as
        a generator for optimization. A client iterates over the keys using
        a for loop.

        :return: key generator
        """    
        while not self.state == 'DONE':
            if not self.state == 'COMPLETE' and not self.objects:
                self._get_object_batch()
            while self.objects:
                yield self.objects.pop()
            if self.state == 'COMPLETE':
                self.state = 'DONE'

    def object_content(self, key):
        """
        Get object content for given key.

        :type key: string
        :param key: key for data lake object
        :return: object content string
        """  
        response = requests.request('GET', key)
        try:
            if response.ok:
                return response.text, None
            else:
                return None, 'Unable to Get Resource {} STATUS {}'.format(key, response.status_code)
        except Exception as e:
            return None, str(e)