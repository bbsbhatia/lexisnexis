import re

def contains_mandatory_f_language(text):
    for r in mandatory_f_language:
        if r.search(text):
            return True
    return False

def contains_may_support_f_language(text):
    for r in may_support_f_language:
        if r.search(text):
            return True
    return False

mandatory_f_language = [
    r'\b(?:controlling|applicable|determinative|dispositive|apposite|apt)\b',
    r'\b(?:we .* (?:on the authority of|on the teaching of|for the reasons stated in|under the rationale of|consistent with|in harmony with))\b',
    r'\b(?:(?:here|thus), these (?:elements|factors|rules|criteria|prongs|requirements|steps|components|matrix) are met)\b',
    r'\b(?:in light of these (?:elements|factors|rules|criteria|prongs|requirements|steps|components|matrix))\b',
    r'\b(?:fails to (?:demonstrate|meet|satisfy) the (?:burden|hurdle|test))\b',
    r'\b(?:(?:does not|fails to) pass muster)\b',
    r'\b(?:it is the directive of CASECITEHERE that)\b',
    r'\b(?:(?:we|i) agree with)\b',
    r'\b(?:we adhere to CASECITEHERE)\b',
    r'\b(?:CASECITEHERE (?:reaffirms|teaches|instructs|directs|counsels|mandates|demands|dictates|commands|compels|forecloses|requires))\b',
    r'\b(?:(?:agrees with|is bound by|follows) CASECITEHERE)\b',
    r'\b(?:runs afoul of the holding in)\b',
    r'\b(?:it is the mandate of CASECITEHERE that)\b',
    r'\b(?:such a conclusion is (?:required|governed) by CASECITEHERE)\b',
    r'\b(?:the court did not have the benefit of our recent opinion in)\b',
    r'\b(?:we find CASECITEHERE helpful in reaching our decision)\b',
    r'\b(?:applying the (?:test|framework) in)\b',
    r'\b(?:was overruled on other grounds than those for which we are applying it today)\b',
    r'\b(?:meets the(?:se)? requirements of)\b',
    r'\b(?:fails to meet the requisite)\b',
    r'\b(?:meets the(?:se)? conditions of)\b',
    r'\b(?:factors clearly militate)\b',
    r'\b(?:(?:does not|cannot) meet the burden under)\b',
    r'\b(?:the facts of the present case are sufficiently similar to those in .* to compel a similar result)\b',
    r'\b(?:adopting the approach followed in)\b',
    r'\b(?:CASECITEHERE is binding on this court)\b',
    r'\b(?:heeding the warning in CASECITEHERE, we)\b',
    r'\b(?:is foreclosed by)\b',
    r'\b(?:CASECITEHERE bears directly on this case)\b',
    r'\b(?:CASECITEHERE is on all fours with this case)\b',
    r'\b(?:resolves this case)\b',
    r'\b(?:several of our sister courts have .* we follow suit here)\b',
    r'\b(?:falls within (?:this case|these cases|this line of cases))\b',
    r'\b(?:is the correct and governing legal standard)\b',
    r'\b(?:(?:we follow|following) CASECITEHERE)\b',
    r'\b(?:we find the reasoning of CASECITEHERE compelling)\b',
    r'\b(?:CASECITEHERE supplies the correct remedy here)\b',
    r'\b(?:we join (?:that|these) decisions? in holding)\b',
    r'\b(?:CASECITEHERE (?:does not permit|permits))\b',
    r'\b(?:no grounds to treat as exempt from CASECITEHERE)\b',
    r'\b(?:is cabined by CASECITEHERE)\b',
    r'\b(?:CASECITEHERE is conclusive on this point)\b',
    r'\b(?:is precluded by CASECITEHERE|CASECITEHERE precludes)\b',
    r'\b(?:CASECITEHERE effectively disposes of this (?:matter|claim))\b',
    r'\b(?:in violation of CASECITEHERE and other authorities)\b',
    r'\b(?:courts are split)\b', # An f/c split is mandatory f application.
]

may_support_f_language = [
    r'\b(?:persuasive|instructive|informs the court)\b',
    r'\b(?:this case is quite similar factually to .* where|identical|same with|substantially similar|remarkably similar issue|similar(?:ly)?)\b',
    r'\b(?:like the case at bar|likewise in this case)\b',
    r'\b(?:consistent with)\b',
    r'\b(?:in accordance with the case of)\b',
    r'\b(?:in light of|pursuant to|under)\b',
    r'\b(?:we reject|we have rejected|arguments that were rejected by)\b',
    r'\b(?:is analagous to|by analogy to)\b',
    r'\b(?:in the recent case of .* stated the applicable (?:standard|rule of law) to be)\b',
    r'\b(?:find the proper standard to be)\b',
    r'\b(?:as inadequately pleaded under the .* pleading standard)\b',
    r'\b(?:in accordance with|applying the above|accordingly|thus|with these principles in mind)\b',
    r'\b(?:as in(?: the case of)?|in the present case|here)\b',
    r'\b(?:is legally indistinguishable from)\b',
    r'\b(?:no reason to depart(?: from)?)\b',
    r'\b(?:prescribed in)\b',
    r'\b(?:as set forth in)\b',
    r'\b(?:subject to the considerations of)\b',
    r'\b(?:provides guidance)\b',
    r'\b(?:and so it is here, as well)\b',
    r'\b(?:the same thing is true here(?: as in CASECITEHERE)?)\b',
    r'\b(?:(?:occurs? when|requires?|defines?) .* n?or)\b', # Unenumerated tests are not the same as enumerated ones. They simply require express reliance language as any other non-test situation would.
    r'\b(?:CASECITEHERE confirms)\b',
    r'\b(?:comports with CASECITEHERE)\b',
    r'\b(?:aligns with CASECITEHERE)\b',
    r'\b(?:quoting CASECITEHERE)\b', # Commutative f rule still applies. When the IC follows follows a cited case and the cited case itself clearly followed an earlier case - f is also applied to the earlier case.
    r'\b(?:unlike CASECITEHERE|in contrast)\b', # f/d split is not mandatory f application; need additional language
    r'\b(?:CASECITEHERE\s*\(.*\))', # Cases with parenthetical language may get f's if there is strong surrounding language to indicate that this specific case is being followed.
    r'\b(?:(?:attempts? to|fails to|does not) distinguish|argues? that in CASECITEHERE|is not distinguishable|cannot be distinguished)\b', # Failed attempt to distinguish a cited case from the instant matter; not mandatory f - needs additional language.
]

mandatory_f_language = list(map(lambda pattern: re.compile(pattern, re.IGNORECASE), mandatory_f_language))
may_support_f_language = list(map(lambda pattern: re.compile(pattern, re.IGNORECASE), may_support_f_language))
