from lxml import etree as ElementTree
import re
import numpy as np
from f_operative_language import contains_may_support_f_language, contains_mandatory_f_language

editor_alias = '@'

editors = {}

def get_editor_alias(editor_id):
    if not editors.get(editor_id, ''):
        global editor_alias
        editor_alias = chr(ord(editor_alias) + 1)
        editors[editor_id] = editor_alias
    return editors[editor_id]

namespaces = {
    'laaa':'laaa',
    'lnci':'lnci',
    'lnvxe':'lnvxe',
    'lndocmeta':'lndocmeta',
    'lnv':'lnv',
    'lnsys':'lnsys',
    'docinfo':'docinfo',
    'lnvni':'lnvni'
}

def xml_to_tree(fragment):
    '''Return ElementTree for given xml text'''
    ns = ' '.join(('xmlns:{}="{}"'.format(*i) for i in namespaces.items()))
    xml = re.sub(r'^<\?xml .*<COURTCASE.*?>','<COURTCASE ' + ns + '>', fragment)
    tree = ElementTree.fromstring(xml)
    return tree

def find_paragraphs(tree):
    return tree.xpath('//p[not(ancestor::p)]', namespaces=namespaces)

def get_lni(tree):
    return tree.xpath('//lndocmeta:lnlni/@lnlni', namespaces=namespaces)[0]

def find_cite_references(tree):
    '''Return list of cite id referenced by treatment trigger in given xml text'''
    elements = tree.findall('./laaa:treatmentTriggerCitation/laaa:treatmentTriggerCiteReference', namespaces=namespaces)
    references = [e.attrib['ID'] for e in elements]
    return references

def find_cites(tree):
    '''Return list of tuples (cite_id, treatment_letter, is_anaphoric_ref) in given xml text'''
    def cite_letter(elements):
        letters = [e.attrib['letter'] for e in elements]
        return letters[0] if letters else None
    def anaphoric_ref(element):
        anaphref = element.attrib.get('anaphref')
        return anaphref
    elements = tree.xpath('.//lnci:cite', namespaces=namespaces)
    anapr = [anaphoric_ref(e) for e in elements]
    lettr = [cite_letter(e.findall('.//lnci:editletter', namespaces)) for e in elements]
    cites = [e.get('ID') for e in elements]
    descr = [''.join(e.itertext()) for e in elements]
    word_indices = [get_element_word_index(tree, e) for e in elements]
    return list(zip(cites, lettr, anapr, descr, word_indices))

def count_words(text):
    return len(text.split())

def get_element_word_index(tree, element):
    return count_words(''.join(itertext(tree, element)))

def itertext(tree, until_element=None, block_elems=['p'], replace_cites=False):
    '''
    Based on Element.itertext but with support for:
     - stopping when until_element is found
     - inserting newlines around nested block elements
     - replacing case cite content with CASECITEHERE placeholders
    '''
    class ElementFound(Exception):
        pass

    def impl(subtree):
        if subtree is until_element:
            raise ElementFound
        if tree is not subtree and subtree.tag in block_elems:
            yield "\n"
        if replace_cites and subtree.tag == '{lnci}cite' and subtree.find('{lnci}case') is not None:
            yield 'CASECITEHERE'
            return
        t = subtree.text
        if t:
            yield t
        for e in subtree:
            yield from impl(e)
            if tree is not e and e.tag in block_elems:
                yield "\n"
            t = e.tail
            if t:
                yield t

    try:
        yield from impl(tree)
    except ElementFound:
        return

def find_trigger_elements(tree):
    '''
    Return list of laaa:treatmentTrigger xml elements
    '''
    return tree.xpath('.//laaa:treatmentTrigger[@project="followedBy" or not(@project)]', namespaces=namespaces)

def find_trigger_language(tree):
    '''
    Return all the text embedded within laaa:treatmentTriggerText elements in given laaa:treatmentTrigger element
    '''
    language = ''.join(itertext(tree, replace_cites=True))
    group_id = tree.attrib['groupIdentifier']
    fragment_id = tree.attrib['fragmentNumber']
    editor_id = tree.attrib['editorId']
    return language, group_id, fragment_id, get_editor_alias(editor_id)

def get_text(tree):
    '''Return all the text within given xml element'''
    return ''.join(itertext(tree))

trigger_language_dict_metadata = ['para_text',
                                  'trgr_element',
                                  'trgr_language',
                                  'contains_mandatory_f_language',
                                  'contains_may_support_f_language',
                                  'trgr_language_word_index',
                                  'word_dist_to_cite',
                                  'para_dist_to_cite',
                                  'group_id',
                                  'fragment_id',
                                  'editor_id',
                                  'cites_in_para',
                                  'cite_referenced',
                                  'trgr_cite_is_closest',
                                  'cnt_cite_in_para',
                                  'cnt_cite_in_para_anaphref',
                                  'cnt_f_letter_cite_in_para',
                                  'cnt_nonf_letter_cite_in_para',
                                  'cnt_cite_referenced',
                                  'cnt_cite_referenced_in_para',
                                  'cnt_words']

def make_trigger_language_dict(filename, para, para_id, **kwargs):
    '''Utility function to make a dictionary object for trigger language'''
    trigger_language_dict = {'filename': filename, 'para_element': para, 'para_id': para_id}
    for key, value in kwargs.items():
        if key not in trigger_language_dict_metadata:
            raise ValueError('unknown key {}'.format(key))
        trigger_language_dict[key] = value
    return trigger_language_dict

followed_letters = ['#&ff', '#ff', '&ff', 'ff', '#&f', '#f', '&f', 'f']

def f_letter(text):
    return False if not text else text in followed_letters

def nonf_letter(text):
    return False if not text else text not in followed_letters

def process_f_percentage(document_language_list):
    f_letter_cnt = 0
    non_f_letter_cnt = 0
    for d in document_language_list:
        f_letter_cnt += d['cnt_f_letter_cite_in_para']
        non_f_letter_cnt += d['cnt_nonf_letter_cite_in_para']
    all_letter_cnt = f_letter_cnt + non_f_letter_cnt
    if all_letter_cnt:
        percent_f = '{:.2f}'.format(f_letter_cnt * 100 / all_letter_cnt)
    else:
        percent_f = np.nan
    for d in document_language_list:
        d['percent_f'] = percent_f

def process_dist_to_cites(document_language_list):
    def get_doc_word_index(para_id):
        words_in_doc = 0
        last_para = None
        for d in document_language_list:
            para_cur = d['para_id']
            if para_cur == para_id:
                break
            if para_cur != last_para:
                words_in_doc += d['cnt_words']
            last_para = para_cur
        return words_in_doc

    def get_cite_location(cite):
        words_in_doc = 0
        for d in document_language_list:
            for c in d['cites_in_para']:
                if c[0] == cite:
                    cite_para = d['para_id']
                    start_word = c[4] + get_doc_word_index(cite_para)
                    end_word = start_word + count_words(c[3])
                    return cite_para, start_word, end_word

    def get_word_distance(start_word_1, end_word_1, start_word_2, end_word_2):
        if end_word_1 <= start_word_2:
            # item 1 is before item 2
            return start_word_2 - end_word_1
        elif end_word_2 <= start_word_1:
            # item 1 is after item 2
            return start_word_1 - end_word_2
        else:
            # items are nested
            return 0

    def get_nearest_cites(start_index):
        last_para = None
        if start_index:
            for i in range(start_index - 1, -1, -1):
                para_id = document_language_list[i]['para_id']
                if para_id == last_para:
                    continue
                last_para = para_id
                cites = document_language_list[i]['cites_in_para']
                if cites:
                    yield cites[-1][0]
                    break
        cites = document_language_list[start_index]['cites_in_para']
        para_id = document_language_list[start_index]['para_id']
        if para_id != last_para:
            last_para = para_id
            for cite in cites:
                yield cite[0]
        if start_index <= len(document_language_list):
            for i in range(start_index + 1, len(document_language_list)):
                para_id = document_language_list[i]['para_id']
                if para_id == last_para:
                    continue
                last_para = para_id
                cites = document_language_list[i]['cites_in_para']
                if cites:
                    yield cites[0][0]
                    break

    for i, d in enumerate(document_language_list):
        word_dist_to_cite = ''
        para_dist_to_cite = ''
        trgr_cite_is_closest = ''
        if d['trgr_language']:
            trgr_para = d['para_id']
            cite = d['cite_referenced']
            cite_para, cite_start_word, cite_end_word = get_cite_location(cite)
            para_dist_to_cite = abs(trgr_para - cite_para)
            trgr_start_word = get_doc_word_index(trgr_para) + d['trgr_language_word_index']
            trgr_end_word = trgr_start_word + count_words(d['trgr_language'])
            word_dist_to_cite = get_word_distance(trgr_start_word, trgr_end_word, cite_start_word, cite_end_word)
            if word_dist_to_cite == 0:
                trgr_cite_is_closest = True
            else:
                nearest_cites = get_nearest_cites(i)
                nearest_cite_locations = map(get_cite_location, nearest_cites)
                nearest_cite_distances = map(
                    lambda loc: get_word_distance(trgr_start_word, trgr_end_word, loc[1], loc[2]),
                    nearest_cite_locations)
                trgr_cite_is_closest = word_dist_to_cite <= min(nearest_cite_distances)

        d['word_dist_to_cite'] = word_dist_to_cite
        d['trgr_cite_is_closest'] = trgr_cite_is_closest
        d['para_dist_to_cite'] = para_dist_to_cite

def process_trigger_sections(file, para, para_id, trgrs, lni):
    '''
    Create dictionary for each section that has trigger language
    and append to list
    '''
    section_language_list = []
    for trgr in trgrs:
        para_text = get_text(para)
        language, group_id, fragment_id, editor_id = find_trigger_language(trgr)
        cites_in_para = find_cites(para)
        cnt_cite_in_para = len(cites_in_para)
        cnt_cite_in_para_anaphref = len([e for e in cites_in_para if e[2]])
        cnt_f_letter_cite_in_para = len([e for e in cites_in_para if f_letter(e[1])])
        cnt_nonf_letter_cite_in_para = len([e for e in cites_in_para if nonf_letter(e[1])])
        cite_ids_in_para = [c[0] for c in cites_in_para]
        cite_references = find_cite_references(trgr)
        cite_referenced = ''.join(cite_references) if cite_references else ''
        cnt_cite_referenced = len(cite_references)
        cnt_cite_referenced_in_para = len([e for e in cite_references if e in cite_ids_in_para])
        cnt_words = count_words(para_text)
        trigger_language_dict = make_trigger_language_dict(file, para, para_id,
            para_text=para_text,
            trgr_element=trgr,
            trgr_language=language,
            contains_mandatory_f_language=contains_mandatory_f_language(language),
            contains_may_support_f_language=contains_may_support_f_language(language),
            trgr_language_word_index=get_element_word_index(para, trgr),
            group_id=group_id,
            fragment_id=fragment_id,
            editor_id=editor_id,
            cite_referenced=cite_referenced,
            cites_in_para=cites_in_para,
            cnt_cite_in_para=cnt_cite_in_para,
            cnt_cite_in_para_anaphref=cnt_cite_in_para_anaphref,
            cnt_f_letter_cite_in_para=cnt_f_letter_cite_in_para,
            cnt_nonf_letter_cite_in_para=cnt_nonf_letter_cite_in_para,
            cnt_cite_referenced_in_para=cnt_cite_referenced_in_para,
            cnt_cite_referenced=cnt_cite_referenced,
            cnt_words=cnt_words
        )
        trigger_language_dict['lni'] = lni
        section_language_list.append(trigger_language_dict)
    return section_language_list

def process_notrigger_para(file, para, para_id, lni):
    '''
    Create dictionary when no no trigger language is present
    and append to list
    '''
    section_language_list = []
    para_text = get_text(para)
    cites_in_para = find_cites(para)
    cnt_cite_in_para = len(cites_in_para)
    cnt_cite_in_para_anaphref = len([e for e in cites_in_para if e[2]])
    cnt_f_letter_cite_in_para = len([e for e in cites_in_para if f_letter(e[1]) ])
    cnt_nonf_letter_cite_in_para = len([e for e in cites_in_para if nonf_letter(e[1]) ])
    cnt_words = count_words(para_text)
    trigger_language_dict = make_trigger_language_dict(file, para, para_id,
        para_text=para_text,
        trgr_element='',
        trgr_language='',
        contains_may_support_f_language='',
        contains_mandatory_f_language='',
        trgr_language_word_index='',
        group_id='',
        fragment_id='',
        editor_id='',
        cite_referenced='',
        cites_in_para=cites_in_para,
        cnt_cite_in_para=cnt_cite_in_para,
        cnt_cite_in_para_anaphref=cnt_cite_in_para_anaphref,
        cnt_f_letter_cite_in_para=cnt_f_letter_cite_in_para,
        cnt_nonf_letter_cite_in_para=cnt_nonf_letter_cite_in_para,
        cnt_cite_referenced_in_para=0,
        cnt_cite_referenced=0,
        cnt_words=cnt_words
    )
    trigger_language_dict['lni'] = lni
    section_language_list.append(trigger_language_dict)
    return section_language_list

def extract_trigger_language(file, content):
    '''Find trigger language in given content and process it'''
    doc = xml_to_tree(content)
    lni = get_lni(doc)
    document_language_list = []
    for index, para in enumerate(find_paragraphs(doc)):
        trgrs = find_trigger_elements(para)
        if trgrs:
            section_language_list = process_trigger_sections(file, para, index, trgrs, lni)
            document_language_list.extend(section_language_list)
        else:
            section_language_list = process_notrigger_para(file, para, index, lni)
            document_language_list.extend(section_language_list)
    process_f_percentage(document_language_list)
    process_dist_to_cites(document_language_list)
    return document_language_list
