from trigger_language_analysis import *
import pytest
import os
from pathlib import Path


@pytest.fixture()
def xml():
    cwd = os.getcwd()
    test_doc_path = os.path.join(
        Path(cwd).parent, 'Tests', 'Data', 'BL18370175.XML')
    with open(test_doc_path, "r", encoding="utf-8") as f:
        return f.read()


def test_count_words():
    assert count_words('') == 0
    assert count_words('word') == 1
    assert count_words('some words') == 2
    assert count_words("WORDS!!! YAY!!!") == 2
    assert count_words("   \n\n\n   here's\nsome    words          ") == 3


def test_extract_trigger_language(xml):
    test_document_language_list = {
        0: {
            'filename': 'BL18370175.xml',
            'para_id': 0,
            'trgr_language': '',
            'contains_may_support_f_language': '',
            'contains_mandatory_f_language': '',
            'trgr_language_word_index': '',
            'group_id': '',
            'fragment_id': '',
            'cite_referenced': '',
            'cites_in_para': [],
            'cnt_cite_in_para': 0,
            'cnt_cite_in_para_anaphref': 0,
            'cnt_f_letter_cite_in_para': 0,
            'cnt_nonf_letter_cite_in_para': 0,
            'cnt_cite_referenced_in_para': 0,
            'cnt_cite_referenced': 0,
            'cnt_words': 3,
            'lni': '5PP9-KWS1-F04C-T3R4-00000-00',
            'percent_f': '42.86',
            'word_dist_to_cite': '',
            'trgr_cite_is_closest': '',
            'para_dist_to_cite': ''
        },
        4: {
            'para_id': 4,
            'trgr_language': 'The Ninth Circuit has set out seven factors for a court to consider when exercising its discretion:',
            'contains_mandatory_f_language': False,
            'contains_may_support_f_language': False,
            'trgr_language_word_index': 38,
            'group_id': 'I3f829839-a549-4bec-8383-30c9ccde14de',
            'fragment_id': '1',
            'cite_referenced': 'I5PPCMMK2N1R8300F0000400',
            'cites_in_para': [
                ('I5PPCMMK2N1R830070000400', None, None, 'Federal Rule of Civil Procedure 55', 0),
                ('I5PPCMMK2N1R830090000400', None, None, 'Lau Ah Yew v. Dulles, 236 F.2d 415, 416 (9th Cir. 1956)', 26),
                ('I5PPCMMK2N1R8300F0000400', 'f', None, 'Eitel v. McCool, 782 F.2d 1470, 1471-72 (9th Cir. 1986)', 121),
                ('I5PPCMMK2N1R8300K0000400', None, None, 'Geddes v. United Financial Group, 559 F.2d 557, 560 (9th Cir. 1977)', 161),
                ('I5PPCMMK2N1R8300R0000400', None, None, 'Pope v. United States, 323 U.S. 1, 12, 65 S. Ct. 16, 89 L. Ed. 3, 102 Ct. Cl. 846 (1944))', 174)
            ],
            'cnt_cite_in_para': 5,
            'cnt_cite_in_para_anaphref': 0,
            'cnt_f_letter_cite_in_para': 1,
            'cnt_nonf_letter_cite_in_para': 0,
            'cnt_cite_referenced_in_para': 1,
            'cnt_cite_referenced': 1,
            'cnt_words': 195,
            'word_dist_to_cite': 66,
            'trgr_cite_is_closest': False,
            'para_dist_to_cite': 0
        },
        5: {
            'para_id': 4,
            'trgr_language': "(1) the possibility of prejudice to the plaintiff, (2) the merits of plaintiff's substantive claim, (3) the sufficiency of the complaint, (4) the sum of money at stake in the action, (5) the possibility of a dispute concerning material facts, (6) whether the default was due to excusable neglect, and (7) the strong policy underlying the Federal Rules of Civil Procedure favoring decisions on the merits.",
            'contains_mandatory_f_language': False,
            'contains_may_support_f_language': False,
            'trgr_language_word_index': 55,
            'group_id': 'I3f829839-a549-4bec-8383-30c9ccde14de',
            'fragment_id': '2',
            'cite_referenced': 'I5PPCMMK2N1R8300F0000400',
            'cites_in_para': [
                ('I5PPCMMK2N1R830070000400', None, None, 'Federal Rule of Civil Procedure 55', 0),
                ('I5PPCMMK2N1R830090000400', None, None, 'Lau Ah Yew v. Dulles, 236 F.2d 415, 416 (9th Cir. 1956)', 26),
                ('I5PPCMMK2N1R8300F0000400', 'f', None, 'Eitel v. McCool, 782 F.2d 1470, 1471-72 (9th Cir. 1986)', 121),
                ('I5PPCMMK2N1R8300K0000400', None, None, 'Geddes v. United Financial Group, 559 F.2d 557, 560 (9th Cir. 1977)', 161),
                ('I5PPCMMK2N1R8300R0000400', None, None, 'Pope v. United States, 323 U.S. 1, 12, 65 S. Ct. 16, 89 L. Ed. 3, 102 Ct. Cl. 846 (1944))', 174)
            ],
            'cnt_cite_in_para': 5,
            'cnt_cite_in_para_anaphref': 0,
            'cnt_f_letter_cite_in_para': 1,
            'cnt_nonf_letter_cite_in_para': 0,
            'cnt_cite_referenced_in_para': 1,
            'cnt_cite_referenced': 1,
            'cnt_words': 195,
            'word_dist_to_cite': 0,
            'trgr_cite_is_closest': True,
            'para_dist_to_cite': 0
        },
        6: {
            'para_id': 5,
            'cites_in_para': [
                ('I5PPCMMK2N1R8300V0000400', None, None, '47 U.S.C. § 605', 19),
                ('I5PPCMMK2N1R8300X0000400', None, None, 'Phillip Morris USA v. Castword Prods., Inc., 219 F.R.D. 494, 499 (C.D. Cal. 2003)', 49)
            ],
            'cnt_cite_in_para': 2,
            'cnt_cite_in_para_anaphref': 0,
            'cnt_f_letter_cite_in_para': 0,
            'cnt_nonf_letter_cite_in_para': 0,
            'cnt_words': 72,
        },
        7: {
            'para_id': 6,
            'trgr_language': 'The next two Eitel factors require that ',
            'contains_mandatory_f_language': False,
            'contains_may_support_f_language': False,
            'trgr_language_word_index': 0,
            'group_id': 'I3f829839-a549-4bec-8383-30c9ccde14de',
            'fragment_id': '3',
            'cite_referenced': 'I5PPCMMK2N1R8300F0000400',
            'cites_in_para': [
                ('I5PWM0R81XJJ7D0020000400', None, None, "Kloepping v. Fireman's Fund, No. C 94-2684 TEH, 1996 U.S. Dist. LEXIS 1786, 1996 WL 75314, at *2 (N.D. Cal. Feb. 13 1996)", 18),
                ('I5PPCMMK2N1R830150000400', None, None, '47 U.S.C. § 605', 58),
                ('I5PPCMMK2N1R830160000400', None, None, '47 U.S.C. § 553', 74),
                ('I5PPCMMK2N1R830170000400', None, None, 'California Business and Professions Code § 17200', 82)
            ],
            'cnt_cite_in_para': 4,
            'cnt_cite_in_para_anaphref': 0,
            'cnt_f_letter_cite_in_para': 0,
            'cnt_nonf_letter_cite_in_para': 0,
            'cnt_cite_referenced_in_para': 0,
            'cnt_cite_referenced': 1,
            'cnt_words': 93,
            'word_dist_to_cite': 136,
            'trgr_cite_is_closest': False,
            'para_dist_to_cite': 2
        },
        22: {
            'para_id': 21,
            'trgr_language': 'Others reason that pirating broadcasts itself demonstrates willfulness, and that display for the purposes of entertainment by a commercial establishment such as a bar or restaurant is sufficient to establish commercial gain. ',
            'contains_mandatory_f_language': False,
            'contains_may_support_f_language': False,
            'trgr_language_word_index': 102,
            'group_id': 'I475a3277-9678-4052-bf8b-0bb19353b865',
            'fragment_id': '1',
            'cite_referenced': 'I5PWM3RY0K1NMG0010000400',
            'cites_in_para': [
                ('I5PWM3M00K1NJS0010000400', 'c', None, 'Kingvision Pay-Per-View, Ltd. v. Arias, C 99-3017 SI, 2000 U.S. Dist. LEXIS 162, 2000 WL 20973, at *2 (N.D. Cal. Jan. 7, 2000)', 50),
                ('I5PWM3N72202K90010000400', None, None, 'Don King Productions/Kingvision v. Carrera, C95-4545-TEH, 1997 U.S. Dist. LEXIS 9137, 1997 WL 362115, at *2 (N.D. Cal. Jun. 24, 1997)', 74),
                ('I5PPCMMK2N1R8308K0000400', 'c', 'I5PPCMMK2N1R8303G0000400', 'Backman, 102 F. Supp. 2d at 1199', 95),
                ('I5PWM3RY0K1NMG0010000400', 'f', None, "Joe Hand Promotions, Inc. v. Cat's Bar, Inc., No. 08-4049, 2009 U.S. Dist. LEXIS 20961, 2009 WL 700125, at *3 (C.D. Ill. Mar. 16, 2009)", 136)
            ],
            'cnt_cite_in_para': 4,
            'cnt_cite_in_para_anaphref': 1,
            'cnt_f_letter_cite_in_para': 1,
            'cnt_nonf_letter_cite_in_para': 2,
            'cnt_cite_referenced_in_para': 1,
            'cnt_cite_referenced': 1,
            'cnt_words': 161,
            'word_dist_to_cite': 2,
            'trgr_cite_is_closest': False,
            'para_dist_to_cite': 0
        },
        23: {
            'para_id': 22,
            'trgr_language': 'The Court finds persuasive the view that a pirated broadcast at a commercial establishment by itself recommends penalty enhancement. ',
            'contains_mandatory_f_language': False,
            'contains_may_support_f_language': True,
            'trgr_language_word_index': 0,
            'group_id': 'I475a3277-9678-4052-bf8b-0bb19353b865',
            'fragment_id': '2',
            'cite_referenced': 'I5PWM3RY0K1NMG0010000400',
            'cites_in_para': [
                ('I5PPCMMK2N1R8308Y0000400', None, None, "Entm't by J & J, Inc. v. Al-Waha Enterprises, Inc., 219 F. Supp. 2d 769, 776 (S.D. Tex. 2002)", 64),
                ('I5PPCMMK2N1R830930000400', None, 'I5PPCMMK2N1R8308Y0000400', 'Id. at 777', 253),
                ('I5PPCMMK2N1R830940000400', None, None, '§ 605', 259)
            ],
            'cnt_cite_in_para': 3,
            'cnt_cite_in_para_anaphref': 1,
            'cnt_f_letter_cite_in_para': 0,
            'cnt_nonf_letter_cite_in_para': 0,
            'cnt_cite_referenced_in_para': 0,
            'cnt_cite_referenced': 1,
            'cnt_words': 263,
            'word_dist_to_cite': 0,
            'trgr_cite_is_closest': True,
            'para_dist_to_cite': 1
        }
    }
    document_language_list = extract_trigger_language('BL18370175.xml', xml)

    for i, row in test_document_language_list.items():
        for key, value in row.items():
            assert value == document_language_list[i][key]
