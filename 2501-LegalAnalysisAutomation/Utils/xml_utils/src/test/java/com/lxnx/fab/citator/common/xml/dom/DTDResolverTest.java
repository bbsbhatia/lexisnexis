package com.lxnx.fab.citator.common.xml.dom;

import com.lxnx.fab.citator.common.xml.dom.DTDResolver;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class DTDResolverTest {

    @Test
    public void test()throws Exception{
        DTDResolver resolver = new DTDResolver();
        assertNull(resolver.resolveEntity("",""));
        assertNotNull(resolver.resolveEntity("//LN//COURTCASEv06-000//EN",""));
    }
}
