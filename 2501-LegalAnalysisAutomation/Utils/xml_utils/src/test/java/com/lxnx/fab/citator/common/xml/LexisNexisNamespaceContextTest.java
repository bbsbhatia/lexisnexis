package com.lxnx.fab.citator.common.xml;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.xml.XMLConstants;

import static org.junit.Assert.assertEquals;

public class LexisNexisNamespaceContextTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetPrefix(){
        LexisNexisNamespaceContext context = new LexisNexisNamespaceContext();
        thrown.expect(UnsupportedOperationException.class);
        context.getPrefix("asdf");
    }

    @Test
    public void testGetPrefixes(){
        LexisNexisNamespaceContext context = new LexisNexisNamespaceContext();
        thrown.expect(UnsupportedOperationException.class);
        context.getPrefixes("asdf");
    }

    @Test
    public void testGetNamespace(){
        LexisNexisNamespaceContext context = new LexisNexisNamespaceContext();
        assertEquals(XMLConstants.NULL_NS_URI, context.getNamespaceURI("asdf"));
        assertEquals("http://www.lexis-nexis.com/lnv",context.getNamespaceURI("lnv"));
    }
}
