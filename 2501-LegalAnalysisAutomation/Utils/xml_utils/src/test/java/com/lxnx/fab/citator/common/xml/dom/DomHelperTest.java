package com.lxnx.fab.citator.common.xml.dom;

import com.lxnx.fab.citator.common.xml.dom.DomHelper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import javax.print.Doc;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class DomHelperTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetChildNodesAsList()throws Exception{
        Document doc = readXmlFile("data/5RF2-MC51-F15C-B0NF-00000-00.xml");
        List<Node> nodes = DomHelper.getChildNodesAsList(doc.getDocumentElement());
        assertEquals(4, nodes.size());
    }

    @Test
    public void testgetListFromNodeListNull()throws Exception{
        List<Node> nodes = DomHelper.getListFromNodeList(null);
        assertEquals(0, nodes.size());
    }

    @Test
    public void testCreateTextNode() throws Exception{
        Document doc = readXmlFile("data/5RF2-MC51-F15C-B0NF-00000-00.xml");
        Node node = DomHelper.createTextNode(doc, "test");
        assertTrue(node instanceof Text);
        assertEquals("test", node.getTextContent());
    }

    @Test
    public void testIsParentElementName() throws Exception{
        Document doc = readXmlFile("data/5RF2-MC51-F15C-B0NF-00000-00.xml");
        NodeList nl = doc.getElementsByTagName("default:p");
        assertEquals(12, nl.getLength());
        assertFalse(DomHelper.isParentElementName(nl.item(0).getParentNode(), Arrays.asList("default:p")));
        assertTrue(DomHelper.isParentElementName(nl.item(0).getParentNode(), Arrays.asList("jcd:judicialCourtDecision")));
    }

    @Test
    public void testGetElementsByTagName() throws Exception{
        Document doc = readXmlFile("data/5RF2-MC51-F15C-B0NF-00000-00.xml");
        assertEquals(15, DomHelper.getElementsByTagName(doc.getDocumentElement(), Arrays.asList("default:p", "note:p")).size());
        assertEquals(12, DomHelper.getElementsByTagName(doc.getDocumentElement(), Arrays.asList("default:p")).size());
        assertEquals(3, DomHelper.getElementsByTagName(doc.getDocumentElement(), Arrays.asList("note:p")).size());
    }

    @Test
    public void testGetFirstInstanceOfNode() throws Exception{
        Document doc = readXmlFile("data/5RF2-MC51-F15C-B0NF-00000-00.xml");
        assertNotNull(DomHelper.getFirstInstanceOfNode(doc.getDocumentElement(), "note:p"));
        assertNull(DomHelper.getFirstInstanceOfNode(doc.getDocumentElement(), "note:psdfdsf"));
    }

    @Test
    public void testInsertAfterNull() throws Exception{
        thrown.expect(NullPointerException.class);
        DomHelper.insertAfter(null, null);
    }

    @Test
    public void testInsertAfter() throws Exception {
        Document doc = readXmlFile("data/5RF2-MC51-F15C-B0NF-00000-00.xml");
        Element child1 = doc.createElement("note:p");
        Node refNode = doc.getDocumentElement().getLastChild();
        DomHelper.insertAfter(child1, refNode);
        Element child2 = doc.createElement("note:p");
        DomHelper.insertAfter(child2, refNode);
        assertEquals(Node.DOCUMENT_POSITION_FOLLOWING, child2.compareDocumentPosition(child1));

    }

        private Document readXmlFile(String filePath) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        URL documentResource = classLoader.getResource(filePath);
        Path documentPath = new File(documentResource.getFile()).toPath();
        String documentXml = new String(Files.readAllBytes(documentPath));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(new InputSource(new StringReader(documentXml)));
    }
}
