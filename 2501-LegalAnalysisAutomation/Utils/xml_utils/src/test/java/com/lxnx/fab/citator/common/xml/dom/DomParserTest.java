package com.lxnx.fab.citator.common.xml.dom;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.common.xml.dom.DomParser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DomParserTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testReadDocument()throws Exception{
        Document doc = DomParser.readDocument(readXML("data/ccv6.xml"), true);
        assertNotNull(doc);
        assertEquals("COURTCASE", doc.getDocumentElement().getNodeName());
    }

    @Test
    public void testException() throws Exception{
        thrown.expect(ErrorChainException.class);
        DomParser.readDocument(new InputSource(new StringReader("asdfsd")), true);
    }

    private InputSource readXML(String filePath) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL documentResource = classLoader.getResource(filePath);

        Path documentPath = new File(documentResource.getFile()).toPath();
        String documentXml = new String(Files.readAllBytes(documentPath));

        return new InputSource(new StringReader(documentXml));
    }
}
