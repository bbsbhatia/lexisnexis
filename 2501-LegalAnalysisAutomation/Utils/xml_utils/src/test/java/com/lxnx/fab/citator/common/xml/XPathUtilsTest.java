package com.lxnx.fab.citator.common.xml;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;
import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;

public class XPathUtilsTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testGetFromXPath() throws Exception{
        Document doc = readXmlFile("data/5RF2-MC51-F15C-B0NF-00000-00.xml");
        String lni = (String)XPathUtils.getFromXPath("/jcd:judicialCourtDecision/doc:metadata/mncrdocmeta:chunkinfo/mncrdocmeta:lnlni/@lnlni", doc, XPathConstants.STRING);
        assertEquals("5RF2-MC51-F15C-B0NF-00000-00", lni);
    }

    @Test
    public void testGetFromXPathException() throws Exception{
        thrown.expect(ErrorChainException.class);
        thrown.expectMessage("javax.xml.transform.TransformerException: Empty expression!");
        Document doc = readXmlFile("data/5RF2-MC51-F15C-B0NF-00000-00.xml");
        XPathUtils.getFromXPath("", doc, XPathConstants.STRING);
    }

    private Document readXmlFile(String filePath) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        URL documentResource = classLoader.getResource(filePath);
        Path documentPath = new File(documentResource.getFile()).toPath();
        String documentXml = new String(Files.readAllBytes(documentPath));
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(new InputSource(new StringReader(documentXml)));
    }
}
