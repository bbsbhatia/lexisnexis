package com.lxnx.fab.citator.common.xml;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XMLUtilsTest {

    @Test
    public void test(){
        assertEquals("&lt;&amp;&apos;&quot;&#10;&#13;&#9;&gt; &#xa7; &#x2014; &#xb6;", XMLUtils.encodeXML("<&'\"\n\r\t> § — ¶"));
        assertEquals("&#xfffd;", XMLUtils.encodeXML(String.valueOf((char) 1)));
        assertEquals("&#xfffd;", XMLUtils.encodeXML(String.valueOf((char)0xfffe)));
    }
}
