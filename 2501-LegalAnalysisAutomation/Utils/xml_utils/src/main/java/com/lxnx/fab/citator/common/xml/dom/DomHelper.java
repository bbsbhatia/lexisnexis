package com.lxnx.fab.citator.common.xml.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public final class DomHelper {
    private DomHelper(){}
    public static List<Node> getChildNodesAsList(Element sourceElement) {
        return getListFromNodeList(sourceElement.getChildNodes());
    } // end getChildNodes(element) method

    public static List<Node> getListFromNodeList(NodeList nl){
        List<Node> list = new ArrayList<>();
        if (null != nl) {
            for (int node = 0; node < nl.getLength(); node++) {
                list.add(nl.item(node));
            }
        }
        return list;
    }

    public static Node createTextNode(Document doc, String inValue){
        return doc.createTextNode(inValue);
    }

    /**
     * Determines if a parent element has the name passed in.
     * @param node
     * @param elementNames
     * @return
     */
    public static boolean isParentElementName(Node node, List<String> elementNames){
        Node ref = node;
        while (ref != null){
            if (elementNames.contains(ref.getNodeName())){
                return true;
            }
            ref = ref.getParentNode();
        }
        return false;
    }

    /**
     * Gets elements by the element tags passed in.  These are returned in the order they appear in the document.
     * @param parentElement
     * @param elementTags
     * @return
     */
    public static List<Node> getElementsByTagName(Element parentElement, List<String> elementTags){
        List<Node> nodes = new ArrayList<>();
        for (Node node: getChildNodesAsList(parentElement)){
            if (elementTags.contains(node.getNodeName())){
                nodes.add(node);
            } else if (node instanceof Element){
                nodes.addAll(getElementsByTagName((Element)node, elementTags));
            }
        }
        return nodes;
    }
    /**
     * Using an in-order, find the first occurrence of sNodeName in nodeToSearch,
     * and return a reference to the node.
     * Creation date: (6/25/2002 4:46:28 PM) (Don Holliday)
     * @return org.w3c.dom.Node  The first occurrence of the node named in
     * sNodeName, using and in-order traversal, otherwise null.  Returns null if
     * nodeToSearch is null.
     * @param nodeToSearch - The node in which you want to find sNodeName.
     * @param sNodeName - The name of the node you want to find and return.
     */
    public final static Node getFirstInstanceOfNode( Node nodeToSearch, String sNodeName){
        Node node = null;
        if ( nodeToSearch != null ) {
            Stack<Node> stack = new Stack<>();
            stack.push( nodeToSearch );

            Node currentNode = null;
            Node childNode = null;
            while ( ( ! stack.empty() ) && ( node == null ) ) {
                currentNode = stack.pop();
                if ( currentNode.getNodeName().equals( sNodeName) ) {
                    node = currentNode;
                } else {
                    childNode = currentNode.getLastChild();
                    while ( childNode != null ) {
                        stack.push( childNode );
                        childNode = childNode.getPreviousSibling();
                    }
                }
            }
        }
        return node;
    } // getFirstInstanceOfNode(Node,String)

    /**
     * Inserts the node newChild after the existing child node refChild.
     * If refChild is null it throws a NullPointerException.
     * If newChild is a DocumentFragment object, all of its children are inserted,
     * in the same order, before refChild. If the newChild is already in the tree,
     * it is first removed.
     *
     * Creation date: (5/23/2001 5:00:42 PM)  (Don Holliday)
     * @since Release 1
     * version 1.0
     *
     * @return org.w3c.dom.Node
     * @param newChild org.w3c.dom.Node
     * @param refChild org.w3c.dom.Node
     * @exception NullPointerException The exception description.
     * @exception org.w3c.dom.DOMException The exception description.
     */
    public static Node insertAfter( Node newChild, Node refChild )
            throws NullPointerException, org.w3c.dom.DOMException{
        if ( refChild == null ) {
            throw new NullPointerException( "Parameter refChild cannot be null." );
        }

        Node parentNode = refChild.getParentNode();

        Node refChildNextSibling = refChild.getNextSibling();
        if ( refChildNextSibling == null ) {
            return parentNode.appendChild( newChild );
        } else {
            return parentNode.insertBefore( newChild, refChildNextSibling );
        }
    }
}
