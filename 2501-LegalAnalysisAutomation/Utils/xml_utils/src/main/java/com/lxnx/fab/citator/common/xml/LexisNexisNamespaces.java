package com.lxnx.fab.citator.common.xml;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public final class LexisNexisNamespaces {

    private LexisNexisNamespaces(){}

    public static final String ADMINDOC = "urn:x-lexisnexis:content:administrativedocument:mastering:1";
    public static final String CFI = "http://www.lexisnexis.com/xmlschemas/content/shared/cite-finding-information/1/";
    public static final String CITATION = "urn:x-lexisnexis:content:citation:global:1";
    public static final String CLASSIFY = "urn:x-lexisnexis:content:classify:mastering:1";
    public static final String DC = "http://purl.org/dc/elements/1.1/";
    public static final String DCTERMS = "http://purl.org/dc/terms/";
    public static final String DEFAULT = "urn:x-lexisnexis:content:default:mastering:1";
    public static final String DEFAULTLDC = "urn:x-lexisnexis:content:defaultldc:mastering:1";
    public static final String DOC = "urn:x-lexisnexis:content:documentlevelmetadata:mastering:1";
    public static final String GLOBALENTITY = "urn:x-lexisnexis:content:identified-entities:global:1";
    public static final String GUID = "urn:x-lexisnexis:content:guid:global:1";
    public static final String JCD = "urn:x-lexisnexis:content:judicialcourtdecision:mastering:1";
    public static final String JURISINFO = "urn:x-lexisnexis:content:jurisdiction-info:mastering:1";
    public static final String LEGACY = "urn:x-lexisnexis:content:legacy:mastering:1";
    public static final String LNCI = "http://www.lexisnexis.com/xmlschemas/content/shared/citations/1/";
    public static final String LNCR = "http://www.lexis-nexis.com/lncr";
    public static final String LNDOCMETA = "http://www.lexis-nexis.com/lndocmeta";
    public static final String LNLIT = "http://www.lexis-nexis.com/lnlit";
    public static final String LNV = "http://www.lexis-nexis.com/lnv";
    public static final String LNVX = "http://www.lexis-nexis.com/lnvx";
    public static final String LNVXE = "http://www.lexis-nexis.com/lnvxe";
    public static final String LOCATION = "urn:x-lexisnexis:content:location:mastering:1";
    public static final String M = "http://www.w3.org/1999/mathml";
    public static final String META = "urn:x-lexisnexis:content:metadata:mastering:1";
    public static final String MNCRDOCMETA = "urn:x-lexisnexis:content:mncrdocmeta:mastering:1";
    public static final String RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
    public static final String REF = "urn:x-lexisnexis:content:reference:mastering:1";
    public static final String TOPIC = "urn:x-lexisnexis:content:topic:global:1";
    public static final String XS = "http://www.w3.org/2001/XMLSchema";
    public static final String XSI = "http://www.w3.org/2001/XMLSchema-instance";

    private static final Map<String, String> namespaceMap = new HashMap<>();

    static {
        namespaceMap.put("admindoc", ADMINDOC);
        namespaceMap.put("cfi", CFI);
        namespaceMap.put("citation", CITATION);
        namespaceMap.put("classify", CLASSIFY);
        namespaceMap.put("dc", DC);
        namespaceMap.put("dcterms", DCTERMS);
        namespaceMap.put("default", DEFAULT);
        namespaceMap.put("defaultldc", DEFAULTLDC);
        namespaceMap.put("doc", DOC);
        namespaceMap.put("globalentity", GLOBALENTITY);
        namespaceMap.put("guid", GUID);
        namespaceMap.put("jcd", JCD);
        namespaceMap.put("jurisinfo", JURISINFO);
        namespaceMap.put("legacy", LEGACY);
        namespaceMap.put("lnci", LNCI);
        namespaceMap.put("lncr", LNCR);
        namespaceMap.put("lndocmeta", LNDOCMETA);
        namespaceMap.put("lnlit", LNLIT);
        namespaceMap.put("lnv", LNV);
        namespaceMap.put("lnvx", LNVX);
        namespaceMap.put("lnvxe", LNVXE);
        namespaceMap.put("location", LOCATION);
        namespaceMap.put("m", M);
        namespaceMap.put("meta", META);
        namespaceMap.put("mncrdocmeta", MNCRDOCMETA);
        namespaceMap.put("rdf", RDF);
        namespaceMap.put("ref", REF);
        namespaceMap.put("topic", TOPIC);
        namespaceMap.put("xs", XS);
        namespaceMap.put("xsi", XSI);
    }

    public static Map<String, String> getNamespaceMap(){
        return Collections.unmodifiableMap(namespaceMap);
    }
}
