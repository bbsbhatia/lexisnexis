package com.lxnx.fab.citator.common.xml;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import java.util.Iterator;
import java.util.Map;

/**
 * This class is to allow for XPathing to recognize namespaces
 */
public final class LexisNexisNamespaceContext implements NamespaceContext {
    @Override
    public String getNamespaceURI(String prefix) {
        Map<String, String> namespaceMap = LexisNexisNamespaces.getNamespaceMap();
        if (namespaceMap.containsKey(prefix)){
            return namespaceMap.get(prefix);
        }
        return XMLConstants.NULL_NS_URI;
    }

    @Override
    public String getPrefix(String namespaceURI) {
        //Not needed for Xpathing
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator getPrefixes(String namespaceURI) {
        //Not needed for Xpathing
        throw new UnsupportedOperationException();
    }
}
