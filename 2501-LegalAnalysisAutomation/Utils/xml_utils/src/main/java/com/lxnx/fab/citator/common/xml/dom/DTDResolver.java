package com.lxnx.fab.citator.common.xml.dom;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DTDResolver implements EntityResolver {

    private static final String CCV6 = "//LN//COURTCASEv06-000//EN";
    private static final Map<String, String> dtdMap = new HashMap<>();
    static {
        dtdMap.put(CCV6, "/courtcase-v6-norm.dtd");
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
        if (dtdMap.containsKey(publicId)){
            return new InputSource(DomParser.class.getResourceAsStream(dtdMap.get(publicId)));
        }
        return null;
    }
}
