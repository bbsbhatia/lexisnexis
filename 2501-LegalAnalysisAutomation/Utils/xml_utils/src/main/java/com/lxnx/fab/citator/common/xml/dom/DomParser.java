package com.lxnx.fab.citator.common.xml.dom;


import com.lexisnexis.wim.instrumentation.ErrorChainException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class DomParser {

    /**
     * Reads an XML document and returns a Document object.
     *
     * @param inputSource      the InputSourec object to read from
     * @param isNamespaceAware true if the parser should be namespace aware
     * @return a Document object parsed from the given file
     * @throws ErrorChainException error reading the document
     */
    public static Document readDocument(InputSource inputSource, boolean isNamespaceAware)
            throws ErrorChainException {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(isNamespaceAware);
            DocumentBuilder db = dbf.newDocumentBuilder();
            db.setEntityResolver(new DTDResolver());
            return db.parse(inputSource);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            ErrorChainException ece = ErrorChainException.get(e);
            ece.push("Error reading and parsing the " +
                    "document in readDocument() of CRDOMParser");
            throw ece;
        }
    }

}
