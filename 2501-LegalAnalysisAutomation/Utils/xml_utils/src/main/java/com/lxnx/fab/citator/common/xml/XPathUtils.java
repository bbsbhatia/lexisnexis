package com.lxnx.fab.citator.common.xml;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import org.w3c.dom.Node;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

public final class XPathUtils {

    private XPathUtils(){}
    private static final XPathFactory XPATH_FACTORY = XPathFactory.newInstance();

    /**
     * Gets a DOM Object from an XPath Expression
     * @param xpathExpression - The Xpath Expression.  e.g. /COURTCASE/lndocmeta:docinfo/lndocmeta:lnlni/@lnlni
     * @param referenceNode - The relative path from where the search should start
     * @param returnType - The expected object type we are expected to get back (e.g. XPathConstants.STRING or XPathConstants.NODESET)
     * @return The appropriate object based on the expression and returnType
     * @throws ErrorChainException - if there was an issue evaluating the XPath.
     */
    public static Object getFromXPath(String xpathExpression, Node referenceNode, QName returnType) throws ErrorChainException {
        XPath xpath = XPATH_FACTORY.newXPath();
        xpath.setNamespaceContext(new LexisNexisNamespaceContext());
        try {
            XPathExpression expr = xpath.compile(xpathExpression);
            return expr.evaluate(referenceNode, returnType);
        } catch(XPathExpressionException e){
            ErrorChainException ex = new ErrorChainException(e);
            ex.push("Failure using XPathExpression :" + xpathExpression);
            throw ex;
        }
    }
}
