package com.lxnx.fab.citator.caselaw.treatment;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.lexisnexis.wim.instrumentation.ErrorChainException;
import org.w3c.dom.Document;

public class FeatureExtractorCore {
    private static final Injector INJECTOR = Guice.createInjector(new FeatureExtractorModule());

    private final FeatureExtractorProcessor processor = INJECTOR.getInstance(FeatureExtractorProcessor.class);

    public FeatureExtractorResult process(Document xmlDoc) throws ErrorChainException {
        return processor.process(xmlDoc);
    }
}
