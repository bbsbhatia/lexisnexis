package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorCitationHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorLnciCiteCitationHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.FeatureExtractorPreProcessor;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.FeatureExtractorPreProcessorAddParagraphTags;
import org.w3c.dom.Document;

import java.util.*;

public class DigestDocumentHandler extends AbstractFeatureExtractorDocumentHandler{
    private static final Map<String, String[]> blockElements;

    static {
        Map<String, String[]> map = new HashMap<>();
        map.put("defaultldc:p", null);
        map.put("lnvxe:blockquote", null);
        blockElements = Collections.unmodifiableMap(map);
    }

    @Override
    public Map<String, String[]> getBlockElements() {
        return blockElements;
    }
    @Override
    protected String getLniXPath() {
        return "/admindoc:DIGESTDOC-LDC/doc:metadata/mncrdocmeta:chunkinfo/mncrdocmeta:lnlni/@lnlni";
    }

    @Override
    protected String getCourtLevelXPath() {
        return null;
    }

    @Override
    public String getCourtLevel(Document xmlDoc) throws ErrorChainException {
        return "0";
    }

    @Override
    protected String getElementsContainingParagraphsXPath() {
        return "//lnv:DIG-BODY-1 | //lnv:DIG-BODY-2 | //lnv:DIG-BODY-3 | //lnv:DIG-BODY-4 | //lnv:DIG-BODY-5 | //lnv:ATTACHMENT-1 | //lnv:ATTACHMENT-2 | //lnv:ATTACHMENT-3 | //lnv:ATTACHMENT-4 | //lnv:ATTACHMENT-5 | //lnv:FOOTNOTE-1 | //lnv:FOOTNOTE-2 | //lnv:FOOTNOTE-3 | //lnv:FOOTNOTE-4 | //lnv:FOOTNOTE-5";
    }

    @Override
    protected String getDateXPath() {
        return "/admindoc:DIGESTDOC-LDC/descendant::node()/lnvxe:date/@lnv:DATE-VALUE";
    }

    @Override
    public List<String> getParagraphTags() {
        return Arrays.asList("defaultldc:p");
    }

    @Override
    public String getHeadingTag() {
        return "";
    }

    @Override
    public FeatureExtractorCitationHandler getCitationHandler() {
        return new FeatureExtractorLnciCiteCitationHandler();
    }

    @Override
    public List<FeatureExtractorPreProcessor> getPreProcessors() {
        return Arrays.asList(new FeatureExtractorPreProcessorAddParagraphTags());
    }
}
