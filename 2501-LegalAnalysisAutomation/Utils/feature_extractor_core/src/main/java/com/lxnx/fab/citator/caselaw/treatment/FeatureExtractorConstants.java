package com.lxnx.fab.citator.caselaw.treatment;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.lxnx.fab.citator.caselaw.treatment.FeatureNameConstants.*;

public final class FeatureExtractorConstants {
    private FeatureExtractorConstants(){}

    public static final String CASECITEHERE = "CASECITEHERE";
    public static final String STATCITEHERE = "STATCITEHERE";
    public static final String COURTRULECITEHERE = "COURTRULECITEHERE";
    public static final String REGCITEHERE = "REGCITEHERE";
    public static final String AGENCYDECISIONCITEHERE = "AGENCYDECISIONCITEHERE";
    public static final String CONSTITUTIONCITEHERE = "CONSTITUTIONCITEHERE";
    public static final String JURYINSTRCITEHERE = "JURYINSTRCITEHERE";
    public static final String SESSLAWCITEHERE = "SESSLAWCITEHERE";
    public static final String PATENTCITEHERE = "PATENTCITEHERE";
    public static final String TMARKCITEHERE = "TMARKCITEHERE";
    public static final String COPYRIGHTCITEHERE = "COPYRIGHTCITEHERE";
    public static final String LAWREVCITEHERE = "LAWREVCITEHERE";
    public static final String PERIODICALCITEHERE = "PERIODICALCITEHERE";
    public static final String BOOKCITEHERE = "BOOKCITEHERE";
    public static final String OAGCITEHERE = "OAGCITEHERE";
    public static final String ANNOTCITEHERE = "ANNOTCITEHERE";
    public static final String EXECDOCCITEHERE = "EXECDOCCITEHERE";
    public static final String MISCCITEHERE = "MISCCITEHERE";
    public static final String FORMCITEHERE = "FORMCITEHERE";

    public static final String LNCI_CITE_ELEM = "lnci:cite";
    public static final String CITATION_ELEM = "citation:citation";
    public static final String RELATED_CITATION_ELEM = "citation:relatedCitation";

    public static final Set<String> NEG_TREATMENT_LETTERS = Collections.unmodifiableSet(
            Stream.of("#Grtd", "&#Grtd", "&A", "&A&R", "&A&Ren",
                    "&A&Rn", "&A&Rp", "&AcqP", "&AcqR", "&Ad", "&Ad&R", "&Ad&Rn", "&Add", "&Am", "&Amd", "&Corr", "&L",
                    "&Li", "&Lim", "&Lmt", "&Ln", "&LnVp", "&Lnp", "&Ltd", "&Nc", "&Noacq", "&Obsl", "&R", "&R&Ren",
                    "&R&Rn", "&RRRn", "&ReSp", "&Ren", "&RenRn", "&Rep", "&Rept", "&Resc", "&Rev", "&Revd", "&Rn",
                    "&Rnb", "&Rp", "&Rpr", "&Rrp", "&Rs", "&Rscp", "&Rv", "&Rv&Rn", "&Rvk", "&Rvkp", "&S&Rn", "&SP",
                    "&Sd", "&Sd&S", "&Sdp", "&Sg", "&Sgtg", "&Spl", "&Ssp", "&Sspp", "&Su", "&Sup", "&Supd", "&Supp",
                    "&Supt", "&Susd", "&Susp", "&U", "&Up", "&V", "&VaL", "&Vd", "&Vdpt", "&ViL", "&Vid", "&Vidp",
                    "&Vnd", "&Vndp", "&Void", "&Voidp", "&Vp", "&Vpdf", "&ab", "&abp", "&c", "&cf", "&cn", "&cr", "&mfd",
                    "&mod", "&modf", "&o", "&op", "&pex", "&pg", "&q", "&qab", "&qabp", "&qo", "&qop", "&qr", "&qv",
                    "&qw", "&rr", "A", "A&R", "A&Ren", "A&Rn", "A&Rp", "AcqP", "AcqR", "Ad", "Ad&R", "Ad&Rn", "Add",
                    "Am", "Amd", "Corr", "L", "Li", "Lim", "Lmt", "Ln", "LnVp", "Lnp", "Ltd", "Nc", "Noacq", "Obsl", "R",
                    "R&Ren", "R&Rn", "RRRn", "ReSp", "Ren", "RenRn", "Rep", "Rept", "Resc", "Rev", "Revd", "Rn", "Rnb",
                    "Rp", "Rpr", "Rrp", "Rs", "Rscp", "Rv", "Rv&Rn", "Rvk", "Rvkp", "S&Rn", "SP", "Sd", "Sd&S", "Sdp", "Sg",
                    "Sgtg", "Spl", "Ssp", "Sspp", "su", "Sup", "Supd", "Supp", "Supt", "Susd", "Susp", "U", "Up", "V", "VaL",
                    "Vd", "Vdpt", "ViL", "Vid", "Vidp", "Vnd", "Vndp", "Void", "Voidp", "Vp", "Vpdf", "ab", "abp", "c", "cf",
                    "cn", "cr", "mfd", "mod", "modf", "o", "op", "pex", "pg", "q", "qab", "qabp", "qo", "qop", "qr", "qv",
                    "qw", "rr").collect(Collectors.toSet()));

    public static final Set<String> F_TREATMENT_LETTERS = Collections.unmodifiableSet(
            Stream.of("f", "ff", "&f", "&ff", "#f", "#ff", "#&f", "#&ff").collect(Collectors.toSet()));

    public static final Set<String> NON_TREATMENT_LETTERS = Collections.unmodifiableSet(
            Stream.of("&", "#").collect(Collectors.toSet()));
    
    public static final Map<String, String> CITE_HERE_TO_FEATURE_COUNT_MAP = new HashMap<>();

    static {
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(CASECITEHERE, CASECCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(STATCITEHERE, STATCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(COURTRULECITEHERE, CRCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(REGCITEHERE, REGCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(AGENCYDECISIONCITEHERE, ADCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(CONSTITUTIONCITEHERE, CONSTCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(JURYINSTRCITEHERE, JURYINSTRCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(SESSLAWCITEHERE, SESSLAWCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(PATENTCITEHERE, PATENTCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(TMARKCITEHERE, TMARKCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(COPYRIGHTCITEHERE, COPYRIGHTCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(LAWREVCITEHERE, LAWREVCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(PERIODICALCITEHERE, PERIODICALCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(BOOKCITEHERE, BOOKCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(OAGCITEHERE, OAGCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(ANNOTCITEHERE, ANNOTCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(EXECDOCCITEHERE, EXECDOCCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(MISCCITEHERE, MISCCCT);
        CITE_HERE_TO_FEATURE_COUNT_MAP.put(FORMCITEHERE, FORMCCT);
    }

}
