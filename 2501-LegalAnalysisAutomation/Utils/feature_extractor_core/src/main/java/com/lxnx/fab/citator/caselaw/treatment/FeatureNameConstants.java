package com.lxnx.fab.citator.caselaw.treatment;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FeatureNameConstants {

    private FeatureNameConstants(){}

    public static final String LAST_HEADING = "last-head";
    public static final String TEXT = "text";
    public static final String CTEXT = "ctext";
    public static final String PNUM = "pnum";
    public static final String CCT = "cct";
    public static final String CASECCT = "casecct";
    public static final String STATCCT = "statcct";
    public static final String CRCCT = "crcct";
    public static final String REGCCT = "regcct";
    public static final String ADCCT = "adcct";
    public static final String CONSTCCT = "constcct";
    public static final String JURYINSTRCCT = "juryinstrcct";
    public static final String SESSLAWCCT = "sesslawcct";
    public static final String PATENTCCT = "patentcct";
    public static final String TMARKCCT = "tmarkcct";
    public static final String COPYRIGHTCCT = "copyrightcct";
    public static final String LAWREVCCT = "lawrevcct";
    public static final String PERIODICALCCT = "periodicalcct";
    public static final String BOOKCCT = "bookcct";
    public static final String OAGCCT = "oagcct";
    public static final String ANNOTCCT = "annotcct";
    public static final String EXECDOCCCT = "execdoccct";
    public static final String MISCCCT = "misccct";
    public static final String FORMCCT = "formcct";
    public static final String TEXTLEN = "len";
    public static final String SRC = "src";
    public static final String ENCPARTY_1 = "encparty1";
    public static final String ENCPARTY_2 = "encparty2";
    public static final String LETTERS = "letters";
    public static final String TRTS = "trts";
    public static final String TRTCT = "trtct";
    public static final String TRTNOFCT = "trtnofct";
    public static final String NEGCT = "negct";
    public static final String MANANAPHCASECT = "mananaphcasect";
    public static final String ALLANAPHCASECT = "allanaphcasect";
    public static final String RETROCASECCT = "retrocasecct";
    public static final String RETROCCT = "retrocct";
    public static final String FOLLOWEDBYCT = "followedbyct";
    public static final String ADJ_CASECCT = "adj_casecct";
    public static final String TREATED_NOCASE = "treated_nocase";
    public static final String YEAR = "year";

    //Feature Sets used to iterate over the features in the map

    //All Features is a comprehensive set of all features
    public static final Set<String> ALL_FEATURES = Collections.unmodifiableSet(
            Stream.of(LAST_HEADING, TEXT, CTEXT, PNUM, CCT, CASECCT, STATCCT, CRCCT, REGCCT,
                    ADCCT, CONSTCCT, JURYINSTRCCT, SESSLAWCCT, PATENTCCT, TMARKCCT, COPYRIGHTCCT,
                    LAWREVCCT, PERIODICALCCT, BOOKCCT, OAGCCT, ANNOTCCT, EXECDOCCCT, MISCCCT,
                    FORMCCT, TEXTLEN, SRC, ENCPARTY_1, ENCPARTY_2, LETTERS, TRTS, TRTCT, TRTNOFCT, NEGCT,
                    MANANAPHCASECT, ALLANAPHCASECT, RETROCASECCT, RETROCCT, FOLLOWEDBYCT, ADJ_CASECCT,
                    TREATED_NOCASE, YEAR).collect(Collectors.toSet()));

    //This is a set that is comprehensive of the features used by the Phase 1 classifier
    public static final Set<String> TREATMENT_FEATURES = Collections.unmodifiableSet(
            Stream.of(LAST_HEADING, TEXT, CTEXT, PNUM, CCT, CASECCT, STATCCT, CRCCT, REGCCT,
                    ADCCT, CONSTCCT, JURYINSTRCCT, SESSLAWCCT, PATENTCCT, TMARKCCT, COPYRIGHTCCT,
                    LAWREVCCT, PERIODICALCCT, BOOKCCT, OAGCCT, ANNOTCCT, EXECDOCCCT, MISCCCT,
                    FORMCCT, TEXTLEN, SRC, ENCPARTY_1, ENCPARTY_2, LETTERS, TRTS, TRTCT,
                    MANANAPHCASECT, ALLANAPHCASECT, RETROCASECCT, RETROCCT, FOLLOWEDBYCT, ADJ_CASECCT,
                    TREATED_NOCASE, YEAR).collect(Collectors.toSet()));

    //This is a set that contains all features that are counts or otherwise can be represented as integers.
    //This set is used in the processor to create a smaller map to initially create all the counts.
    public static final Set<String> COUNT_FEATURES = Collections.unmodifiableSet(
            Stream.of(CCT, CASECCT, STATCCT, CRCCT, REGCCT, ADCCT, CONSTCCT, JURYINSTRCCT, SESSLAWCCT,
                    PATENTCCT, TMARKCCT, COPYRIGHTCCT, LAWREVCCT, PERIODICALCCT, BOOKCCT, OAGCCT, ANNOTCCT,
                    EXECDOCCCT, MISCCCT, FORMCCT, TRTCT, TRTNOFCT, NEGCT, MANANAPHCASECT, ALLANAPHCASECT, RETROCASECCT,
                    RETROCCT, FOLLOWEDBYCT, ADJ_CASECCT, TREATED_NOCASE).collect(Collectors.toSet()));
}
