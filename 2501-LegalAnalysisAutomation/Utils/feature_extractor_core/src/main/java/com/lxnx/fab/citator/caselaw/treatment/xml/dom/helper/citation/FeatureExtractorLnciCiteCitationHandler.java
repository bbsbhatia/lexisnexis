package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation;

import com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorConstants;
import com.lxnx.fab.citator.common.xml.dom.DomHelper;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FeatureExtractorLnciCiteCitationHandler implements FeatureExtractorCitationHandler {
    private static final String NAME_ATTRIB = "name";
    private static Map<String, String> citationTypeMap = new HashMap<>();
    private static final String PARTY_1_ELEM = "lnci:party1";
    private static final String PARTY_2_ELEM = "lnci:party2";

    static {
        citationTypeMap.put("lnci:case", FeatureExtractorConstants.CASECITEHERE);
        citationTypeMap.put("lnci:stat", FeatureExtractorConstants.STATCITEHERE);
        citationTypeMap.put("lnci:courtrule", FeatureExtractorConstants.COURTRULECITEHERE);
        citationTypeMap.put("lnci:reg", FeatureExtractorConstants.REGCITEHERE);
        citationTypeMap.put("lnci:agencydecision", FeatureExtractorConstants.AGENCYDECISIONCITEHERE);
        citationTypeMap.put("lnci:constitution", FeatureExtractorConstants.CONSTITUTIONCITEHERE);
        citationTypeMap.put("lnci:juryinstr", FeatureExtractorConstants.JURYINSTRCITEHERE);
        citationTypeMap.put("lnci:sesslaw", FeatureExtractorConstants.SESSLAWCITEHERE);
        citationTypeMap.put("lnci:patent", FeatureExtractorConstants.PATENTCITEHERE);
        citationTypeMap.put("lnci:tmark", FeatureExtractorConstants.TMARKCITEHERE);
        citationTypeMap.put("lnci:copyright", FeatureExtractorConstants.COPYRIGHTCITEHERE);
        citationTypeMap.put("lnci:lawrev", FeatureExtractorConstants.LAWREVCITEHERE);
        citationTypeMap.put("lnci:periodical", FeatureExtractorConstants.PERIODICALCITEHERE);
        citationTypeMap.put("lnci:book", FeatureExtractorConstants.BOOKCITEHERE);
        citationTypeMap.put("lnci:oag", FeatureExtractorConstants.OAGCITEHERE);
        citationTypeMap.put("lnci:annot", FeatureExtractorConstants.ANNOTCITEHERE);
        citationTypeMap.put("lnci:execdoc", FeatureExtractorConstants.EXECDOCCITEHERE);
        citationTypeMap.put("lnci:admininterp", FeatureExtractorConstants.MISCCITEHERE);
        citationTypeMap.put("lnci:misc", FeatureExtractorConstants.MISCCITEHERE);
        citationTypeMap.put("lnci:courtorder", FeatureExtractorConstants.MISCCITEHERE);
        citationTypeMap.put("lnci:bill", FeatureExtractorConstants.MISCCITEHERE);
        citationTypeMap.put("lnci:form", FeatureExtractorConstants.FORMCITEHERE);
    }

    @Override
    public String[] getRootCitationElements() {
        return new String[]{FeatureExtractorConstants.LNCI_CITE_ELEM};
    }

    @Override
    public Node getReplacementCitationElement(Element citationElement) {
        NodeList childNodes = citationElement.getChildNodes();
        for (int i=0; i < childNodes.getLength(); i++){
            Node childNode = childNodes.item(i);
            if (citationTypeMap.containsKey(childNode.getNodeName())){
                return DomHelper.createTextNode(citationElement.getOwnerDocument(), citationTypeMap.get(childNode.getNodeName()));
            }
        }
        //The Citation type was not determined, return misc cite.
        return DomHelper.createTextNode(citationElement.getOwnerDocument(), FeatureExtractorConstants.MISCCITEHERE);
    }

    @Override
    public String[] getPartyElements(Element citationElement) {
        Element partyOneElement = (Element) DomHelper.getFirstInstanceOfNode(citationElement, PARTY_1_ELEM);
        Element partyTwoElement = (Element) DomHelper.getFirstInstanceOfNode(citationElement, PARTY_2_ELEM);
        String[] parties = new String[2];

        if (partyOneElement != null) {
            parties[0] = partyOneElement.getAttribute(NAME_ATTRIB);
        }

        if (partyTwoElement != null) {
            parties[1] = partyTwoElement.getAttribute(NAME_ATTRIB);
        }

        return parties;
    }

    @Override
    public String[][] getCitationTreatments(Element citationElement){
        List<String> treatmentTexts = new ArrayList<>();
        List<String> treatmentLetters = new ArrayList<>();
        NodeList editLetters = citationElement.getElementsByTagName("lnci:editletter");
        for (int i = 0; i < editLetters.getLength(); i++) {
            Element editletter = (Element) editLetters.item(i);
            if (editletter != null) {
                String treatmentLetter = editletter.getAttribute("letter");
                if (treatmentLetter != null && !treatmentLetter.isEmpty()) {
                    treatmentTexts.add(editletter.getAttribute("comment"));
                    treatmentLetters.add(treatmentLetter);
                }
            }
        }

        return new String[][]{treatmentTexts.toArray(new String[0]), treatmentLetters.toArray(new String[0])};
    }

    @Override
    public boolean isAnaphoric(Element citationElement) {
        NodeList caseRefElements = citationElement.getElementsByTagName("lnci:caseref");
        return caseRefElements.getLength() > 0;
    }

    @Override
    public boolean isManualAnaphoric(Element citationElement) {
        NodeList caseRefElements = citationElement.getElementsByTagName("lnci:caseref");
        for (int node = 0; node < caseRefElements.getLength(); node++) {
            Node caseRefElement = caseRefElements.item(node);
            Node pinpoint = DomHelper.getFirstInstanceOfNode(caseRefElement, "lnci:pinpoint");
            if (pinpoint != null) {
                return false;
            }
        }
        return caseRefElements.getLength() > 0;
    }
}
