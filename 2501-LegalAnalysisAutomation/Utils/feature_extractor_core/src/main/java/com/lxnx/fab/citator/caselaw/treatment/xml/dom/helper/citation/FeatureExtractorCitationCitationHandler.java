package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation;

import com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorConstants;
import com.lxnx.fab.citator.common.xml.dom.DomHelper;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeatureExtractorCitationCitationHandler implements FeatureExtractorCitationHandler {
    private static Map<String, String> citationTypeMap = new HashMap<>();

    static {
        citationTypeMap.put("judicialcourtdecision", FeatureExtractorConstants.CASECITEHERE);
        citationTypeMap.put("judicialCourtDecision", FeatureExtractorConstants.CASECITEHERE);
        citationTypeMap.put("casereport", FeatureExtractorConstants.CASECITEHERE);
        citationTypeMap.put("caseReport", FeatureExtractorConstants.CASECITEHERE);
        citationTypeMap.put("statutorycode", FeatureExtractorConstants.STATCITEHERE);
        citationTypeMap.put("statutoryCode", FeatureExtractorConstants.STATCITEHERE);
        citationTypeMap.put("treatise", FeatureExtractorConstants.STATCITEHERE);
        citationTypeMap.put("publiclaw", FeatureExtractorConstants.STATCITEHERE);
        citationTypeMap.put("publicLaw", FeatureExtractorConstants.STATCITEHERE);
        citationTypeMap.put("courtrule", FeatureExtractorConstants.COURTRULECITEHERE);
        citationTypeMap.put("restatement", FeatureExtractorConstants.STATCITEHERE);
        citationTypeMap.put("courtRule", FeatureExtractorConstants.COURTRULECITEHERE);
        citationTypeMap.put("administrativedecision", FeatureExtractorConstants.AGENCYDECISIONCITEHERE);
        citationTypeMap.put("administrativeDecision", FeatureExtractorConstants.AGENCYDECISIONCITEHERE);
        citationTypeMap.put("administrativeCode", FeatureExtractorConstants.REGCITEHERE);
        citationTypeMap.put("administrativecode", FeatureExtractorConstants.REGCITEHERE);
        citationTypeMap.put("constitution", FeatureExtractorConstants.CONSTITUTIONCITEHERE);
        citationTypeMap.put("juryinstruction", FeatureExtractorConstants.JURYINSTRCITEHERE);
        citationTypeMap.put("juryInstruction", FeatureExtractorConstants.JURYINSTRCITEHERE);
        citationTypeMap.put("sesslaw", FeatureExtractorConstants.SESSLAWCITEHERE);
        citationTypeMap.put("patent", FeatureExtractorConstants.PATENTCITEHERE);
        citationTypeMap.put("trademark", FeatureExtractorConstants.TMARKCITEHERE);
        citationTypeMap.put("copyright", FeatureExtractorConstants.COPYRIGHTCITEHERE);
        citationTypeMap.put("lawReviewAndJournal", FeatureExtractorConstants.LAWREVCITEHERE);
        citationTypeMap.put("periodical", FeatureExtractorConstants.PERIODICALCITEHERE);
        citationTypeMap.put("book", FeatureExtractorConstants.BOOKCITEHERE);
        citationTypeMap.put("attorneyGeneralOpinion", FeatureExtractorConstants.OAGCITEHERE);
        citationTypeMap.put("attorneygeneral", FeatureExtractorConstants.OAGCITEHERE);
        citationTypeMap.put("annotation", FeatureExtractorConstants.ANNOTCITEHERE);
        citationTypeMap.put("executiveorder", FeatureExtractorConstants.EXECDOCCITEHERE);
        citationTypeMap.put("executiveOrder", FeatureExtractorConstants.EXECDOCCITEHERE);
        citationTypeMap.put("register", FeatureExtractorConstants.MISCCITEHERE);
        citationTypeMap.put("", FeatureExtractorConstants.MISCCITEHERE);
    }

    @Override
    public String[] getRootCitationElements() {
        return new String[]{
                FeatureExtractorConstants.CITATION_ELEM,
                FeatureExtractorConstants.RELATED_CITATION_ELEM
        };
    }

    @Override
    public Node getReplacementCitationElement(Element citationElement) {
        String citedResourceType = citationElement.getAttribute("citation:citedResourceType");
        if (citedResourceType != null){
            String citeHereValue = citationTypeMap.get(citedResourceType);
            if (citeHereValue != null ){
                return DomHelper.createTextNode(citationElement.getOwnerDocument(), citeHereValue);
            }
        }
        return DomHelper.createTextNode(citationElement.getOwnerDocument(), FeatureExtractorConstants.MISCCITEHERE);
    }

    @Override
    public String[] getPartyElements(Element citationElement){
        String[] parties = new String[2];
        Element citedResource = (Element) DomHelper.getFirstInstanceOfNode(citationElement,"citation:citedResource");
        if (null != citedResource){
            NodeList legacyElements = citedResource.getElementsByTagName("legacy:element");
            for (int node = 0; node < legacyElements.getLength(); node++) {
                Element legacyElement = (Element) legacyElements.item(node);
                String qName = legacyElement.getAttribute("qName");
                Element legacyAttribute = (Element) DomHelper.getFirstInstanceOfNode(legacyElement, "legacy:attribute");
                if (null != legacyAttribute){
                    String partyName = legacyAttribute.getAttribute("value");
                    if ("lnci:party1".equals(qName)){
                        parties[0] = partyName;
                    } else if ("lnci:party2".equals(qName)) {
                        parties[1] = partyName;
                    }

                }
            }
        }
        return parties;
    }

    /**
     * @param citationElement The element that is being searched
     * @return String Array that can index the treatmentText and treatmentLetter
     * @throws Exception
     */
    public String[][] getCitationTreatments(Element citationElement){
        List<String> treatmentTexts = new ArrayList<>();
        List<String> treatmentLetters = new ArrayList<>();

        NodeList relatedResources = citationElement.getElementsByTagName("citation:relatedResource");
        for (int i = 0; i < relatedResources.getLength(); i++) {
            Element relatedResource = (Element) relatedResources.item(i);
            if (relatedResource != null) {
                String treatmentLetter = relatedResource.getAttribute("citation:treatmentClassifier");
                if (treatmentLetter != null && !treatmentLetter.isEmpty()) {
                    treatmentLetters.add(treatmentLetter);
                    treatmentTexts.add(relatedResource.getAttribute("treatmentText"));
                }
            }
        }
        return new String[][]{treatmentTexts.toArray(new String[0]), treatmentLetters.toArray(new String[0])};
    }

    @Override
    public boolean isAnaphoric(Element citationElement){
        NodeList anaphoricReferenceElements = citationElement.getElementsByTagName("citation:anaphoricReference");
        return anaphoricReferenceElements.getLength() > 0;
    }

    @Override
    public boolean isManualAnaphoric(Element citationElement){
        NodeList legacyElements = citationElement.getElementsByTagName("legacy:element");
        for (int node = 0; node < legacyElements.getLength(); node++) {
            Element legacyElement = (Element)legacyElements.item(node);
            String qName = legacyElement.getAttribute("qName");
            if ("lnci:pinpoint".equals(qName)) {
                return false;
            }
        }
        return isAnaphoric(citationElement);
    }
}
