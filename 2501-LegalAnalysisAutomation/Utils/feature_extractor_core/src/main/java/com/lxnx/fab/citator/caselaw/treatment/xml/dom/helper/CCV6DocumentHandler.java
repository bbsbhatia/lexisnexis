package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper;

import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorCitationHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorLnciCiteCitationHandler;
import org.w3c.dom.Element;

import java.util.*;

public class CCV6DocumentHandler extends AbstractFeatureExtractorDocumentHandler {
    private static final Map<String, String[]> blockElements;

    static {
        Map<String, String[]> map = new HashMap<>();
        map.put("p", null);
        map.put("lnvxe:blockquote", null);
        blockElements = Collections.unmodifiableMap(map);
    }

    @Override
    public Map<String, String[]> getBlockElements() {
        return blockElements;
    }

    @Override
    protected String getLniXPath() {
        return "/COURTCASE/lndocmeta:docinfo/lndocmeta:lnlni/@lnlni";
    }

    @Override
    protected String getCourtLevelXPath() {
        return "/COURTCASE/roundTrippingMetadata/source/elementMetadata/element[@qName='jurisinfo:courtType']/attribute[@name='level']/@value";
    }

    @Override
    protected String getElementsContainingParagraphsXPath() {
        return "/COURTCASE/lnv:OPINION | /COURTCASE/lnv:CONCURS";
    }

    @Override
    protected String getDateXPath() {
        return "/COURTCASE/lnv:DECIDEDDATE/lnvxe:date/text() | /COURTCASE/lnv:FILEDDATE/lnvxe:date/text()";
    }

    @Override
    public List<String> getParagraphTags() {
        return Arrays.asList("p");
    }

    @Override
    public String getHeadingTag() {
        return "lnvxe:h";
    }

    @Override
    public String getOpinionType(Element parentElement){
        switch (parentElement.getNodeName()) {
            case "lnv:CONCURS":
                return CONCUR;
            case "lnv:OPINION":
                return MAJORITY;
            default:
                return parentElement.getNodeName();
        }
    }

    @Override
    public FeatureExtractorCitationHandler getCitationHandler(){
        return new FeatureExtractorLnciCiteCitationHandler();
    }

}
