package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.common.xml.XPathUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import java.util.Collections;
import java.util.Set;

public abstract class AbstractFeatureExtractorDocumentHandler implements FeatureExtractorDocumentHandler{

    protected abstract String getLniXPath();
    protected abstract String getCourtLevelXPath();
    protected abstract String getElementsContainingParagraphsXPath();
    protected abstract String getDateXPath();

    @Override
    public String getLni(Document xmlDoc) throws ErrorChainException {
        return (String) XPathUtils.getFromXPath(getLniXPath(), xmlDoc, XPathConstants.STRING);
    }

    @Override
    public String getCourtLevel(Document xmlDoc) throws ErrorChainException{
        return (String) XPathUtils.getFromXPath(getCourtLevelXPath(), xmlDoc, XPathConstants.STRING);
    }

    @Override
    public String getDate(Document xmlDoc) throws ErrorChainException{
        return (String) XPathUtils.getFromXPath(getDateXPath(), xmlDoc, XPathConstants.STRING);
    }

    @Override
    public NodeList getElementsContainingParagraphs(Document xmlDoc) throws ErrorChainException{
        return (NodeList) XPathUtils.getFromXPath(getElementsContainingParagraphsXPath(), xmlDoc, XPathConstants.NODESET);
    }

    @Override
    public Set<String> getIgnoreElements() {
        return Collections.emptySet();
    }

    @Override
    public String getTextContent(Element element) {
        StringBuilder builder = new StringBuilder();

        getTextContentImpl(element, builder);

        return builder.toString().replaceAll("\\s+", " ");
    }

    private void getTextContentImpl(Element element, StringBuilder builder) {
        if (getIgnoreElements().contains(element.getNodeName())) {
            return;
        }

        boolean isBlock = false;
        if (getBlockElements().containsKey(element.getNodeName())) {
            String[] attr = getBlockElements().get(element.getNodeName());

            // if attributes are specified, they need to match or the element is not considered a block
            isBlock = (attr == null || element.getAttribute(attr[0]).equals(attr[1]));
        }

        if (isBlock)
            builder.append('\n');

        NodeList nodes = element.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            switch (node.getNodeType()) {
                case Node.ELEMENT_NODE:
                    getTextContentImpl((Element) node, builder);
                    break;

                case Node.TEXT_NODE:
                    builder.append(node.getNodeValue());
                    break;

                default:
                    break;
            }
        }

        if (isBlock)
            builder.append('\n');
    }
}
