package com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.FeatureExtractorDocumentHandler;
import com.lxnx.fab.citator.common.xml.dom.DomHelper;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FeatureExtractorPreProcessorAddParagraphTags implements FeatureExtractorPreProcessor {
    private static final String DEFAULT_LDC_NL_ELEMENT = "defaultldc:nl";
    private static final String DEFAULT_LDC_P_ELEMENT = "defaultldc:p";
    private static final String LNVXE_FOOTNOTE = "lnvxe:footnote";
    private static final String LNVXE_FNBODY = "lnvxe:fnbody";
    private static final String IS_SYSTEM_GENERATED_ATTRIBUTE = "isSystemGenerated";

    private static final Set<String> STOP_ELEMENTS = new HashSet<>();
    private static final Set<String> FOOTNOTE_ELEMENTS = new HashSet<>();

    static {
        STOP_ELEMENTS.add(DEFAULT_LDC_NL_ELEMENT);
        STOP_ELEMENTS.add(LNVXE_FOOTNOTE);
        STOP_ELEMENTS.add(DEFAULT_LDC_P_ELEMENT);
        FOOTNOTE_ELEMENTS.add(LNVXE_FOOTNOTE);
        FOOTNOTE_ELEMENTS.add(LNVXE_FNBODY);
    }

    public void preProcess(Document xmlDoc, FeatureExtractorDocumentHandler handler) throws ErrorChainException{
        for (Node parentSection : DomHelper.getListFromNodeList(
                handler.getElementsContainingParagraphs(xmlDoc))){
            addParagraphsForNewLineElements(xmlDoc, (Element) parentSection, handler);
        }
    }


    private void addParagraphsForNewLineElements(Document xmlDom, Element parentElement, FeatureExtractorDocumentHandler handler) throws ErrorChainException{
        boolean newLineFound = false;
        List<Node> elementsAfterNewLine = new ArrayList<>();
        Node newLineReference = null;
        List<Node> childElements = new ArrayList<>();
        for (Node childNode : DomHelper.getChildNodesAsList(parentElement)){
            if (STOP_ELEMENTS.contains(childNode.getNodeName())) {
                if (newLineFound) {
                    addParagraph(xmlDom, parentElement, elementsAfterNewLine, newLineReference, handler);
                    elementsAfterNewLine.clear();
                } else if (DEFAULT_LDC_NL_ELEMENT.equals(childNode.getNodeName())) {
                    newLineFound = true;
                }
                if (newLineFound) {
                    newLineReference = childNode;
                }
            }

            if (!DEFAULT_LDC_NL_ELEMENT.equals(childNode.getNodeName())) {
                if (newLineFound && !STOP_ELEMENTS.contains(childNode.getNodeName())) {
                    elementsAfterNewLine.add(childNode);
                }
                if (childNode instanceof Element){
                    childElements.add(childNode);
                }
            }
        }
        if (!elementsAfterNewLine.isEmpty()){
            addParagraph(xmlDom, parentElement, elementsAfterNewLine, newLineReference, handler);
        }
        //Process child objects after everything else so that the logic to see if a parent is a element is a paragraph works propoerly.
        for (Node childNode : childElements){
            if (FOOTNOTE_ELEMENTS.contains(childNode.getNodeName())) {
                // Only recurse into footnote elements to avoid adding paragraph tags to elements which may contain
                // defauldldc:nl but not allow defaultldc:p, such as lnvxe:h, lnvxe:title, and defaultldc:emph
                // (see D-17179)
                addParagraphsForNewLineElements(xmlDom, (Element) childNode, handler);
            }
        }
    }

    private void addParagraph(Document xmlDom, Element parentElement, List<Node> elementsToAdd, Node newLineReference, FeatureExtractorDocumentHandler handler){
        if (elementsToAdd.size() == 0){
            //No child elements to add, do nothing.
            return;
        } else {
            boolean textExists = false;
            for (Node element : elementsToAdd){
                if (StringUtils.isNotBlank(element.getTextContent())){
                    textExists = true;
                    break;
                }
            }
            if (!textExists){
                //No text found in the elements to add.  So don't add a paragraph.
                return;
            }
        }
        if (DomHelper.isParentElementName(parentElement, handler.getParagraphTags())){
            //Don't add new paragraph tags as we don't need to add a paragraph inside a paragraph
            return;
        }
        Element paragraphElement = xmlDom.createElement(handler.getParagraphTags().get(0));
        paragraphElement.setAttribute(IS_SYSTEM_GENERATED_ATTRIBUTE, "true");
        for (Node elementToAdd: elementsToAdd) {
            parentElement.removeChild(elementToAdd);
            paragraphElement.appendChild(elementToAdd);
        }
        DomHelper.insertAfter(paragraphElement, newLineReference);
    }
}
