package com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.FeatureExtractorDocumentHandler;
import org.w3c.dom.Document;

public interface FeatureExtractorPreProcessor {
    public void preProcess(Document xmlDoc, FeatureExtractorDocumentHandler handler) throws ErrorChainException;
}
