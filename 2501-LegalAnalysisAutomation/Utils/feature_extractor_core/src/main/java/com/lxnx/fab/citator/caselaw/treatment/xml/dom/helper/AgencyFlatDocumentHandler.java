package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorCitationHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorLnciCiteCitationHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.FeatureExtractorPreProcessor;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.FeatureExtractorPreProcessorAddParagraphTags;
import org.w3c.dom.Document;

import java.util.*;

public class AgencyFlatDocumentHandler extends AbstractFeatureExtractorDocumentHandler{
    private static final Map<String, String[]> blockElements;

    static {
        Map<String, String[]> map = new HashMap<>();
        map.put("defaultldc:p", null);
        map.put("lnvxe:blockquote", null);
        blockElements = Collections.unmodifiableMap(map);
    }

    @Override
    public Map<String, String[]> getBlockElements() {
        return blockElements;
    }
    @Override
    protected String getLniXPath() {
        return "/admindoc:AGENCYDEC-FLAT/doc:metadata/mncrdocmeta:chunkinfo/mncrdocmeta:lnlni/@lnlni";
    }

    @Override
    protected String getCourtLevelXPath() {
        return null;
    }

    @Override
    public String getCourtLevel(Document xmlDoc) throws ErrorChainException {
        return "0";
    }

    @Override
    protected String getElementsContainingParagraphsXPath() {
        return "//lnv:OPINION-1 | //lnv:OPINION-2 | //lnv:OPINION-3 | //lnv:OPINION-4 | //lnv:OPINION-5 | //lnv:OPINION-6 | //lnv:OPINION-7 | //lnv:OPINION-8 | //lnv:OPINION-9 | //lnv:CONCUR-1 | //lnv:CONCUR-2 | //lnv:CONCUR-3 | //lnv:CONCUR-4 | //lnv:CONCUR-5 | //lnv:CONCUR-6 | //lnv:CONCUR-7 | //lnv:CONCUR-8 | //lnv:CONCUR-9 | //lnv:ALJ-DECISION | //lnv:ALJ-PROC-ORDER | //lnv:COMPLAINT | //lnv:FOOTNOTE-1 | //lnv:FOOTNOTE-2 | //lnv:FOOTNOTE-3 | //lnv:FOOTNOTE-4 | //lnv:FOOTNOTE-5 | //lnv:INITIAL-DEC-1 | //lnv:ORDER | //lnv:APPENDIX-1 | //lnv:APPENDIX-10 | //lnv:APPENDIX-11 | //lnv:APPENDIX-12 | //lnv:APPENDIX-13 | //lnv:APPENDIX-14 | //lnv:APPENDIX-15 | //lnv:APPENDIX-16 | //lnv:APPENDIX-17 | //lnv:APPENDIX-18 | //lnv:APPENDIX-19 | //lnv:APPENDIX-2 | //lnv:APPENDIX-20 | //lnv:APPENDIX-3 | //lnv:APPENDIX-4 | //lnv:APPENDIX-5 | //lnv:APPENDIX-6 | //lnv:APPENDIX-7 | //lnv:APPENDIX-8 | //lnv:APPENDIX-9 | //lnv:ATTACHMENT-1";
    }

    @Override
    protected String getDateXPath() {
        return "/admindoc:AGENCYDEC-FLAT/lnv:FILED-DATE/text() | /admindoc:AGENCYDEC-FLAT/descendant::node()/dc:date/text() | /admindoc:AGENCYDEC-FLAT/descendant::node()/lnvxe:date/@lnv:DATE-VALUE";
    }

    @Override
    public List<String> getParagraphTags() {
        return Arrays.asList("defaultldc:p");
    }

    @Override
    public String getHeadingTag() {
        return "";
    }

    @Override
    public FeatureExtractorCitationHandler getCitationHandler() {
        return new FeatureExtractorLnciCiteCitationHandler();
    }

    @Override
    public List<FeatureExtractorPreProcessor> getPreProcessors() {
        return Arrays.asList(new FeatureExtractorPreProcessorAddParagraphTags());
    }
}
