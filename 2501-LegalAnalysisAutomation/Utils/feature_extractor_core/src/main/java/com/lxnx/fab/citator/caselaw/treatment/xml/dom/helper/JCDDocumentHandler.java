package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper;

import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorCitationCitationHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorCitationHandler;
import org.w3c.dom.Element;

import java.util.*;

public class JCDDocumentHandler extends AbstractFeatureExtractorDocumentHandler {
    private static final Map<String, String[]> blockElements;
    private static final Set<String> ignoreElements;

    static {
        Map<String, String[]> blockMap = new HashMap<>();
        blockMap.put("default:p", null);
        blockMap.put("note:p", null);
        blockMap.put("default:quote", new String[]{"style", "block"});
        blockElements = Collections.unmodifiableMap(blockMap);

        Set<String> ignoreSet = new HashSet<>();
        ignoreSet.add("dc:date");
        ignoreElements = Collections.unmodifiableSet(ignoreSet);
    }

    @Override
    public Map<String, String[]> getBlockElements() {
        return blockElements;
    }

    @Override
    public Set<String> getIgnoreElements() {
        return ignoreElements;
    }

    @Override
    protected String getLniXPath() {
        return "/jcd:judicialCourtDecision/doc:metadata/mncrdocmeta:chunkinfo/mncrdocmeta:lnlni/@lnlni";
    }

    @Override
    protected String getCourtLevelXPath() {
        return "/jcd:judicialCourtDecision/jcd:information/jcd:decidedBy/jurisinfo:courtInformation/jurisinfo:courtType/@level";
    }

    @Override
    protected String getElementsContainingParagraphsXPath() {
        return "//jcd:majorityOpinion | //jcd:concurringOpinion";
    }

    @Override
    protected String getDateXPath() {
        return "/jcd:judicialCourtDecision/jcd:information/jcd:dates/descendant::node()/@normalizedDate";
    }

    @Override
    public List<String> getParagraphTags() {
        return Arrays.asList("default:p", "note:p");
    }

    @Override
    public String getHeadingTag() {
        return "default:genericHeading";
    }

    @Override
    public FeatureExtractorCitationHandler getCitationHandler() {
        return new FeatureExtractorCitationCitationHandler();
    }

    @Override
    public String getOpinionType(Element parentElement) {
        switch (parentElement.getNodeName()) {
            case "jcd:concurringOpinion":
                return CONCUR;
            case "jcd:majorityOpinion":
                return MAJORITY;
            default:
                return parentElement.getNodeName();
        }
    }
}
