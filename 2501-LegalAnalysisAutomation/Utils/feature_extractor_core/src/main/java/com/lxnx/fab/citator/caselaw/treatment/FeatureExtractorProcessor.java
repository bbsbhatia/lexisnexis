
package com.lxnx.fab.citator.caselaw.treatment;

/*
 * ================================================================
 * Licensed Materials - Property of LexisNexis.
 * (C) Copyright 2019 LexisNexis, a division of Reed Elsevier Inc.
 * All rights reserved.
 * ================================================================
 */

import com.google.inject.Inject;
import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.FeatureExtractorDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorCitationHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.FeatureExtractorPreProcessor;
import com.lxnx.fab.citator.common.xml.dom.DomHelper;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorConstants.*;
import static com.lxnx.fab.citator.caselaw.treatment.FeatureNameConstants.*;


/**
 * <b>Overview:</b>
 * com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorProcessor is responsible for extracting information from a document,
 * building input for a call to the Classifier service, making a call to the Classifier
 * service, and marking up the document based on the results of the Classifier
 * call.
 * <p>
 * Creation date: (03/01/2019)
 *
 * @version 1.0
 * </pre>
 */

public class FeatureExtractorProcessor {

    private List<String> party1s = new ArrayList<>();
    private List<String> party2s = new ArrayList<>();
    private int paraNum = 0;
    private String lastHead = "";
    private ArrayList<String> treatments = new ArrayList<>();
    private ArrayList<String> letters = new ArrayList<>();
    private Document xmlDoc = null;

    @Inject
    private Map<String, FeatureExtractorDocumentHandler> handlerMap;
    private FeatureExtractorDocumentHandler handler;


    /**
     * Controls the calling of methods related to extracting,
     * calling the classifier, building mark up.
     *
     * @param xmlDoc Document dom object of the document to process
     * @throws Exception
     */
    public FeatureExtractorResult process(Document xmlDoc) throws ErrorChainException {
        this.xmlDoc = xmlDoc;
        this.handler = handlerMap.get(xmlDoc.getDocumentElement().getNodeName());
        FeatureExtractorResult result = new FeatureExtractorResult();
        result.setLni(handler.getLni(xmlDoc));
        result.setCourtLevel(handler.getCourtLevel(xmlDoc));

        for (FeatureExtractorPreProcessor preProcessor : handler.getPreProcessors()) {
            preProcessor.preProcess(xmlDoc, handler);
        }

        for (Node parentSection : DomHelper.getListFromNodeList(handler.getElementsContainingParagraphs(xmlDoc))) {
            Element parent = (Element) parentSection;
            List<Node> paraList = DomHelper.getElementsByTagName(parent, handler.getParagraphTags());
            List<Node> headingList = DomHelper.getListFromNodeList(parent.getElementsByTagName(handler.getHeadingTag()));
            extractParagraphs(paraList, headingList, parent, result);
        }
        return result;
    }// end process(Document) method


    /**
     * Extracts information from paragraph nodes in the document
     * to be used to build features for the
     * Classifier.
     *
     * @param paragraphList NodeList  paragraphs
     * @param headingList   NodeList  headings
     * @param parent   Element opinionType
     * @throws Exception
     */
    private void extractParagraphs(List<Node> paragraphList, List<Node> headingList, Element parent,
                                   FeatureExtractorResult result)throws ErrorChainException {
        result.setYear(determineYear(result.getLni()));
        for (Node origParaElement : paragraphList) {
            if (DomHelper.isParentElementName(origParaElement.getParentNode(), handler.getParagraphTags())){
                //The parent is already a Paragraph.  We can bypass this paragraph.
                continue;
            }
            FeatureExtractorRecord featureExtractorRecord = new FeatureExtractorRecord();
            Element paraElement = (Element) origParaElement.cloneNode(true);
            featureExtractorRecord.setOriginalParagraphNode(origParaElement);
            Map<String, Integer> countMap = new HashMap<>();
            for (String countKey: COUNT_FEATURES){
                countMap.put(countKey, 0);//Initialize all counts to zero.
            }
            treatments.clear();
            letters.clear();
            paraNum++;
            featureExtractorRecord.addFeature(PNUM, String.valueOf(paraNum));

            lastHead = getCurrentHeading(headingList, origParaElement, lastHead);
            featureExtractorRecord.addFeature(LAST_HEADING, lastHead);

            String paragraphText = handler.getTextContent(paraElement).trim();

            featureExtractorRecord.addFeature(TEXT, paragraphText);
            featureExtractorRecord.addFeature(TEXTLEN, String.valueOf(paragraphText.length()));
            featureExtractorRecord.addFeature(SRC, handler.getOpinionType(parent));
            processParaChildren(paraElement, countMap);

            //Mark calculates these by all the case cites and citation counts with a retro variable.
            countMap.put(RETROCASECCT, countMap.get(CASECCT));
            countMap.put(RETROCCT, countMap.get(CCT));
            //These are recalculated to make sure that anaphorics are taken into account.
            countMap.replace(CCT, countMap.get(CCT) - countMap.get(MANANAPHCASECT));
            countMap.replace(CASECCT, countMap.get(CASECCT) - countMap.get(MANANAPHCASECT));
            countMap.put(TREATED_NOCASE, countMap.get(RETROCASECCT) == 0 && countMap.get(TRTCT) > 0 ? 1 : 0);

            for (Map.Entry<String, Integer> entry : countMap.entrySet()){
                //Add features for all the counts.
                featureExtractorRecord.addFeature(entry.getKey(), String.valueOf(entry.getValue()));
            }
            String cParagraphText = handler.getTextContent(paraElement);

            featureExtractorRecord.addFeature(CTEXT, cParagraphText.trim());
            featureExtractorRecord.addFeature(LETTERS, String.join("|", letters));
            featureExtractorRecord.addFeature(TRTS, String.join("|", treatments));
            featureExtractorRecord.addFeature(ENCPARTY_1, String.join("|", party1s));
            featureExtractorRecord.addFeature(ENCPARTY_2, String.join("|", party2s));

            result.addFeatureExtractorRecord(featureExtractorRecord);

        }// end i for loop
    }// end extractParagraphs(NodeList) method


    /**
     * @return The year of the document was filed or decided.
     */
    private int determineYear(String lni) throws ErrorChainException {
        int docYear = 0;
        String date = handler.getDate(xmlDoc);
        if (StringUtils.isNotBlank(date)) {
            DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder()
                    .parseCaseInsensitive().parseLenient()
                    .appendPattern("[yyyy-MM-dd]")
                    .appendPattern("[yyyy-MM-dd]")
                    .appendPattern("[M/dd/yyyy]")
                    .appendPattern("[M/d/yyyy]")
                    .appendPattern("[MM/dd/yyyy]")
                    .appendPattern("[MMMM dd yyyy]")
                    .appendPattern("[MMMM dd, yyyy]");
            DateTimeFormatter formatter = builder.toFormatter(Locale.ENGLISH);
            try {
                LocalDate localDate = LocalDate.parse(date.trim(), formatter);
                docYear = localDate.getYear();
            } catch (DateTimeParseException e) {
                ErrorChainException ece = new ErrorChainException(e);
                String msg = String.format("Unable to parse date string. String is not valid date format. " +
                        "Document name: %s, Error: %s", lni, e);
                ece.push(msg);
                throw ece;
            }
        }
        return docYear;
    }// end determineYear() method

    /**
     * Process children nodes of paragraph.
     *
     * @param paragraphElement   Element  paragraphElement
     */
    private void processParaChildren(Element paragraphElement, Map<String, Integer> countMap) {
        NodeList paragraphChildList = paragraphElement.getChildNodes();
        for (int j = 0; j < paragraphChildList.getLength(); j++) {
            Node childNode = paragraphChildList.item(j);

            if (childNode instanceof Element) {
                for (Element citationElement : getCitationNodes((Element) childNode)) {
                    FeatureExtractorCitationHandler citationHandler = handler.getCitationHandler();
                    Node citeHere = citationHandler.getReplacementCitationElement(citationElement);

                    incrCitationCnt(citeHere.getTextContent(), countMap);
                    determinePartyNames(citationElement);
                    determineCitations(citationElement, countMap);
                    determineAnaphoricReferences(citationElement, countMap);

                    if (!citationHandler.isManualAnaphoric(citationElement)) {
                        Element parentElement = (Element) citationElement.getParentNode();
                        parentElement.replaceChild(citeHere, citationElement);
                    }
                }
            }
        }//end j for loop

    }// end processParaChildren() method.

    private List<Element> getCitationNodes(Element refNode){
        List<Element> elements = new ArrayList<>();
        for(String rootCitationElement : handler.getCitationHandler().getRootCitationElements()){
            NodeList citationElements = refNode.getElementsByTagName(rootCitationElement);
            for (int cite = 0; cite < citationElements.getLength(); cite++) {
                elements.add((Element) citationElements.item(cite));
            }
            if (rootCitationElement.equals(refNode.getNodeName())){
                elements.add(refNode);
            }
        }
        return elements;
    }

    /**
     * Increment citation counts.
     *
     * @param citationHereType String citation here value
     * @return void
     */
    private void incrCitationCnt(String citationHereType, Map<String, Integer> countMap){
        countMap.merge(CCT, 1, Integer::sum);
        String featureKey = CITE_HERE_TO_FEATURE_COUNT_MAP.get(citationHereType);
        countMap.merge(featureKey, 1, Integer::sum);
    } // end incrCitaitonCnt() method

    /**
     * Determine the party names for existing citations, and add them to
     * the party name string "lists".
     *
     * @param inElement Element
     * @return Node text
     */
    private void determinePartyNames(Element inElement){
        String[] partyNames = handler.getCitationHandler().getPartyElements(inElement);
        if (null != partyNames[0] && !party1s.contains(partyNames[0])) {
            party1s.add(partyNames[0]);
        }

        if (null != partyNames[1] && !party2s.contains(partyNames[1])) {
            party2s.add(partyNames[1]);
        }

    } // end determinePartyNames() method

    /**
     * This method checks to see if there are followed by letters in the paragraph element that it is searching.
     * If one is found, it increments the instance variable trtCNT and adds the follow by attributes to their lists.
     *
     * @param inElement The element that you are wanting to find a followed by citation
     */
    private void determineCitations(Element inElement, Map<String, Integer> countMap){
        FeatureExtractorCitationHandler citationHandler = handler.getCitationHandler();
        String[][] citationArrayElements = citationHandler.getCitationTreatments(inElement);
        String[] treatmentTexts = citationArrayElements[0];
        String[] treatmentLetters = citationArrayElements[1];

        for (int i = 0; i < treatmentLetters.length; i++) {
            String treatmentText = treatmentTexts[i];
            String treatmentLetter = treatmentLetters[i];

            letters.add(treatmentLetter);
            treatments.add(treatmentText);
            if (!NON_TREATMENT_LETTERS.contains(treatmentLetter)) {
                countMap.merge(TRTCT, 1, Integer::sum);//Increase Treatment Count

                if (F_TREATMENT_LETTERS.contains(treatmentLetter)) {
                    countMap.merge(FOLLOWEDBYCT, 1, Integer::sum);//Increase Followedby Count
                } else {
                    countMap.merge(TRTNOFCT, 1, Integer::sum);
                }

                if (NEG_TREATMENT_LETTERS.contains(treatmentLetter)) {
                    countMap.merge(NEGCT, 1, Integer::sum);//Increase Negative Treatment Count
                }
            }
        }

        String citeHere = citationHandler.getReplacementCitationElement(inElement).getTextContent();
        if (treatmentLetters.length == 0 && CASECITEHERE.equals(citeHere)) {
            countMap.merge(ADJ_CASECCT, 1, Integer::sum);
        }

    }// end determineCitations() method

    /**
     * @param inElement The citation element that we are searching
     */
    private void determineAnaphoricReferences(Element inElement, Map<String, Integer> countMap) {
        FeatureExtractorCitationHandler citationHandler = handler.getCitationHandler();
        if(citationHandler.getReplacementCitationElement(inElement).getTextContent().equals(CASECITEHERE)) {
            if (citationHandler.isManualAnaphoric(inElement)) {
                countMap.merge(MANANAPHCASECT, 1, Integer::sum);
                countMap.merge(ALLANAPHCASECT, 1, Integer::sum);
            } else if (citationHandler.isAnaphoric(inElement)) {
                countMap.merge(ALLANAPHCASECT, 1, Integer::sum);
            }
        }
    }// end determineAnaphoricReferences() method

    /**
     * Set current heading value by determining
     * if there is a change since it was last set.
     *
     * @param headingsList     List    headings
     * @param paragraphElement Element paragraph
     * @param currentHeading   String  heading
     * @return String
     */
    private String getCurrentHeading(List<Node> headingsList, Node paragraphElement, String currentHeading) {
        for (Node headingNode : headingsList){
            if (headingNode.compareDocumentPosition(paragraphElement) == Node.DOCUMENT_POSITION_FOLLOWING) {
                currentHeading =  headingNode.getTextContent();
            } else {
                break;
            }
        }// end for loop

        return currentHeading;

    } // end getCurrentHeading() method

}// end class
