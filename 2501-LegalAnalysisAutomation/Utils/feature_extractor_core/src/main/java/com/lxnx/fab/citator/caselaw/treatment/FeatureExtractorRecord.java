package com.lxnx.fab.citator.caselaw.treatment;

import org.w3c.dom.Node;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FeatureExtractorRecord {

    private Node originalParagraphNode;
    private Map<String, String> features = new HashMap<>();

    public Node getOriginalParagraphNode() {
        return originalParagraphNode;
    }

    public void setOriginalParagraphNode(Node originalParagraphNode) {
        this.originalParagraphNode = originalParagraphNode;
    }

    public void addFeature(String key, String value){
        features.put(key, value);
    }

    public String getFeature(String key){
        return features.get(key);
    }

    /**
     * Returns a map of all the features
     * @return
     */
    public Map<String, String> getAllFeatures(){
        return new HashMap<>(features);
    }

    /**
     * Returns a map of the features with only the keys specified in featureSet.  The sets provided in this
     * class may be used, or the caller may specify their own list.
     * @param featureSet
     * @return
     */
    public Map<String, String> getFeatures(Set<String> featureSet){
        return featureSet.stream()
                .filter(features::containsKey)
                .collect(Collectors.toMap(Function.identity(), features::get));
    }
}
