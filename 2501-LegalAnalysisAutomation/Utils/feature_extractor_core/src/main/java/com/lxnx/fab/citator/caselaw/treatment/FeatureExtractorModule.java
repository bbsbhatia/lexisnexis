package com.lxnx.fab.citator.caselaw.treatment;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.MapBinder;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.AdminInterpretationDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.AgencyFlatDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.CCV6DocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.DigestDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.FeatureExtractorDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.JCDDocumentHandler;

public class FeatureExtractorModule extends AbstractModule {

    @Override
    protected void configure() {
        MapBinder<String, FeatureExtractorDocumentHandler> mapBinder =
                MapBinder.newMapBinder(binder(), String.class, FeatureExtractorDocumentHandler.class);
        mapBinder.addBinding("COURTCASE").to(CCV6DocumentHandler.class);
        mapBinder.addBinding("admindoc:AGENCYDEC-FLAT").to(AgencyFlatDocumentHandler.class);
        mapBinder.addBinding("admindoc:ADMIN-INTERPDOC-LDC").to(AdminInterpretationDocumentHandler.class);
        mapBinder.addBinding("admindoc:DIGESTDOC-LDC").to(DigestDocumentHandler.class);
        mapBinder.addBinding("jcd:judicialCourtDecision").to(JCDDocumentHandler.class);
    }

}