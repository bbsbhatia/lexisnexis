package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation;

import org.w3c.dom.Node;
import org.w3c.dom.Element;

public interface FeatureExtractorCitationHandler {
    public String[] getRootCitationElements();
    public Node getReplacementCitationElement(Element citationElement);
    public String[] getPartyElements(Element citationElement);
    public String[][] getCitationTreatments(Element citationElement);
    public boolean isAnaphoric(Element citationElement);
    public boolean isManualAnaphoric(Element citationElement);
}
