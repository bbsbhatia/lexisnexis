package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.FeatureExtractorPreProcessor;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.citation.FeatureExtractorCitationHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface FeatureExtractorDocumentHandler {

    public static final String MAJORITY = "majority";
    public static final String CONCUR = "concur";

    public String getLni(Document xmlDoc) throws ErrorChainException;
    public String getCourtLevel(Document xmlDoc) throws ErrorChainException;
    public String getDate(Document xmlDoc) throws ErrorChainException;
    public NodeList getElementsContainingParagraphs(Document xmlDoc) throws ErrorChainException;
    public List<String> getParagraphTags();
    public String getHeadingTag();
    public FeatureExtractorCitationHandler getCitationHandler();
    public String getTextContent(Element element);
    public Map<String, String[]> getBlockElements();
    public Set<String> getIgnoreElements();

    public default String getOpinionType(Element parentElement){
        if (null != parentElement) {
            return parentElement.getNodeName();
        } else {
            return "";
        }
    }

    public default List<FeatureExtractorPreProcessor> getPreProcessors(){
        return Collections.emptyList();
    }
}
