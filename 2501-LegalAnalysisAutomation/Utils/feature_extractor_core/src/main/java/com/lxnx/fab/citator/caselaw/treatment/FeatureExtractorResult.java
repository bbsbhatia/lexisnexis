package com.lxnx.fab.citator.caselaw.treatment;

import java.util.ArrayList;
import java.util.List;

public class FeatureExtractorResult {

    private String lni;
    private String courtLevel;
    private int year;
    private List<FeatureExtractorRecord> featureExtractorRecords = new ArrayList<>();

    public void addFeatureExtractorRecord(FeatureExtractorRecord record){
        featureExtractorRecords.add(record);
    }

    public List<FeatureExtractorRecord> getFeatureExtractorRecords(){
        return featureExtractorRecords;
    }

    public String getLni() {
        return lni;
    }

    public void setLni(String lni) {
        this.lni = lni;
    }

    public String getCourtLevel() {
        return courtLevel;
    }

    public void setCourtLevel(String courtLevel) {
        this.courtLevel = courtLevel;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
