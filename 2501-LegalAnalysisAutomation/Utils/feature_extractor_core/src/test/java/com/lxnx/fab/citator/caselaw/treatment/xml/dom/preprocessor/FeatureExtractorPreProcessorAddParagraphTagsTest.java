package com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor;

import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.AdminInterpretationDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.AgencyFlatDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.DigestDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.FeatureExtractorDocumentHandler;
import com.lxnx.fab.citator.common.xml.dom.DomParser;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class FeatureExtractorPreProcessorAddParagraphTagsTest {
    private Map<String, FeatureExtractorDocumentHandler> handlers = new HashMap<>();
    private FeatureExtractorPreProcessor preprocessor = new FeatureExtractorPreProcessorAddParagraphTags();
    private Transformer transformer;

    @Before
    public void setUp() throws Exception {
        handlers.put("admindoc:AGENCYDEC-FLAT", new AgencyFlatDocumentHandler());
        handlers.put("admindoc:ADMIN-INTERPDOC-LDC", new AdminInterpretationDocumentHandler());
        handlers.put("admindoc:DIGESTDOC-LDC", new DigestDocumentHandler());

        TransformerFactory factory = TransformerFactory.newInstance();
        transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
    }

    @Test
    public void testParagraphsAddedToBody() throws Exception {
        testDocument("ad_test_doc_a_1293.xml");
    }

    @Test
    public void testParagraphsAddedToFootnote() throws Exception {
        testDocument("ad_test_doc_a_115.xml");
    }

    @Test
    public void testParagraphsNotAddedToLnvxeTitleInAdminInterpDoc() throws Exception {
        // D-17179
        testDocument("ID25933286.BT25933288.xml");
    }

    @Test
    public void testParagraphsNotAddedToLnvxeTitleInDigestDoc() throws Exception {
        // D-17179
        testDocument("ID25928130.BT25928777.xml");
    }

    @Test
    public void testParagraphsNotAddedToLnvxeHInAdminInterpDoc() throws Exception {
        // D-17179
        testDocument("ID25933286.BT25933929.xml");
    }

    @Test
    public void testParagraphsNotAddedToLnvxeHInDigestDoc() throws Exception {
        // D-17179
        testDocument("ID25928130.BT25928201.xml");
    }

    @Test
    public void testParagraphsNotAddedToDefaultLdcEmph() throws Exception {
        // D-17179
        testDocument("ID25933286.BT25933530.xml");
    }

    private void testDocument(String filename) throws Exception {
        Document doc = getDom(getDocumentXml(String.format("data/input/%s", filename)));
        FeatureExtractorDocumentHandler handler = getHandler(doc);
        preprocessor.preProcess(doc, handler);
        String actualXml = DomToString(doc);
        String expectedXml = getDocumentXml(String.format("data/output/%s", filename));
        assertEquals(expectedXml, actualXml);
    }

    private String DomToString(Document doc) throws Exception {
        DOMSource source = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        transformer.transform(source, new StreamResult(writer));
        return writer.toString();
    }

    private String getDocumentXml(String filePath) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        URL documentResource = classLoader.getResource(filePath);

        Path documentPath = new File(documentResource.getFile()).toPath();
        return new String(Files.readAllBytes(documentPath));
    }

    private Document getDom(String documentXml) throws Exception {
        return DomParser.readDocument(new InputSource(new StringReader(documentXml)), true);
    }

    private FeatureExtractorDocumentHandler getHandler(Document doc) {
        String docElemTagName = doc.getDocumentElement().getTagName();
        return handlers.get(docElemTagName);
    }
}