package com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper;

import org.junit.Test;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/*
Primarily for code coverage
 */
public class FeatureExtractorHelperTest {
    @Test
    public void testAdminInterp(){
        AdminInterpretationDocumentHandler handler = new AdminInterpretationDocumentHandler();
        assertNull(handler.getCourtLevelXPath());
    }

    @Test
    public void testAgencyFlat(){
        AgencyFlatDocumentHandler handler = new AgencyFlatDocumentHandler();
        assertNull(handler.getCourtLevelXPath());
    }

    @Test
    public void testCCV6() throws Exception{
        CCV6DocumentHandler handler = new CCV6DocumentHandler();
        Document doc = getDocument();
        assertEquals("concur", handler.getOpinionType(doc.createElement("lnv:CONCURS")));
        assertEquals("majority", handler.getOpinionType(doc.createElement("lnv:OPINION")));
        assertEquals("test", handler.getOpinionType(doc.createElement("test")));
    }


    @Test
    public void testJCD() throws Exception{
        JCDDocumentHandler handler = new JCDDocumentHandler();
        Document doc = getDocument();
        assertEquals("concur", handler.getOpinionType(doc.createElement("jcd:concurringOpinion")));
        assertEquals("majority", handler.getOpinionType(doc.createElement("jcd:majorityOpinion")));
        assertEquals("test", handler.getOpinionType(doc.createElement("test")));
    }

    @Test
    public void testDigest(){
        DigestDocumentHandler handler = new DigestDocumentHandler();
        assertNull(handler.getCourtLevelXPath());
    }

    @Test
    public void testDefault(){
        DigestDocumentHandler handler = new DigestDocumentHandler();
        assertEquals("", handler.getOpinionType(null));
    }

    private Document getDocument()throws Exception{
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        return builder.newDocument();
    }
}
