package com.lxnx.fab.citator.caselaw;

import com.lxnx.fab.citator.common.xml.dom.DomParser;
import com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorCore;
import com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorRecord;
import com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorResult;
import com.lxnx.fab.citator.caselaw.treatment.FeatureNameConstants;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.*;

public class FeatureExtractorProcessorTest {
    @Before
    public void before(){
        System.setProperty("XML_DTD_DIR_PATH", getClass().getClassLoader().getResource("xml/dtd").getFile());
        System.setProperty("XML_CATALOG", "catalog");
    }

    @Test
    public void testProcess() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/55PV-T3B1-F04F-11W7-00000-00.xml");

        assertNotNull(result);
        assertEquals("55PV-T3B1-F04F-11W7-00000-00", result.getLni());
        assertEquals("3", result.getCourtLevel());
        assertEquals(2012, result.getYear());
        assertEquals(10, result.getFeatureExtractorRecords().size());
        FeatureExtractorRecord record = result.getFeatureExtractorRecords().get(0);
        assertNotNull(record.getOriginalParagraphNode());
        assertEquals("default:p", record.getOriginalParagraphNode().getNodeName());
        assertEquals(40, record.getAllFeatures().keySet().size());
        assertEquals(38, record.getFeatures(FeatureNameConstants.TREATMENT_FEATURES).keySet().size());
    }

    @Test
    public void testAdminInterp() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/admin_interp.xml");

        assertNotNull(result);
        assertEquals("42V6-M810-002H-C2V6-00000-00", result.getLni());
        assertEquals("0", result.getCourtLevel());
        assertEquals(2019, result.getYear());
        assertEquals(127, result.getFeatureExtractorRecords().size());
        FeatureExtractorRecord record = result.getFeatureExtractorRecords().get(0);
        assertNotNull(record.getOriginalParagraphNode());
        assertEquals("defaultldc:p", record.getOriginalParagraphNode().getNodeName());
    }

    @Test
    public void testAgencyFlat() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/agency_flat.xml");

        assertNotNull(result);
        assertEquals("56V7-KFF0-01KR-B0MC-00000-00", result.getLni());
        assertEquals("0", result.getCourtLevel());
        assertEquals(2019, result.getYear());
        assertEquals(32, result.getFeatureExtractorRecords().size());
        FeatureExtractorRecord record = result.getFeatureExtractorRecords().get(0);
        assertNotNull(record.getOriginalParagraphNode());
        assertEquals("defaultldc:p", record.getOriginalParagraphNode().getNodeName());
    }

    @Test
    public void testDigestDoc() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/digest_doc.xml");

        assertNotNull(result);
        assertEquals("4N9M-Y2W0-TX02-W26K-00000-00", result.getLni());
        assertEquals("0", result.getCourtLevel());
        assertEquals(2000, result.getYear());
        assertEquals(23, result.getFeatureExtractorRecords().size());
        FeatureExtractorRecord record = result.getFeatureExtractorRecords().get(0);
        assertNotNull(record.getOriginalParagraphNode());
        assertEquals("defaultldc:p", record.getOriginalParagraphNode().getNodeName());
    }

    @Test
    public void testCCV6() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/ccv6.xml");

        assertNotNull(result);
        assertEquals("5TGW-4821-DXHD-G2KC-00000-00", result.getLni());
        assertEquals("3", result.getCourtLevel());
        assertEquals(2018, result.getYear());
        assertEquals(5, result.getFeatureExtractorRecords().size());
        FeatureExtractorRecord record = result.getFeatureExtractorRecords().get(0);
        assertNotNull(record.getOriginalParagraphNode());
        assertEquals("p", record.getOriginalParagraphNode().getNodeName());
    }

    @Test
    public void testJuryInstCct() throws Exception {

        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String juryinstrcct = getFeature(result, "juryinstrcct", 0);
        assertEquals("0", juryinstrcct);
    }

    @Test
    public void testCct() throws Exception {

        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5704-2VV1-F04J-X0F1-00000-00.xml");

        String cct = getFeature(result, "cct", 0);
        assertEquals("1", cct);

    }

    @Test
    public void testRegCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/594P-G5H1-F04F-01BB-00000-00.xml");

        String regcct = getFeature(result, "regcct", 17);
        assertEquals("1", regcct);
    }

    @Test
    public void testAnnotCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String annotcct = getFeature(result, "annotcct", 1);
        assertEquals("0", annotcct);
    }

    @Test
    public void testLastHead() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String lastHead = getFeature(result, "last-head", 1);
        assertEquals("MEMORANDUM AND ORDER PURSUANT TO RULE 1:28", lastHead);
    }

    @Test
    public void testSessLawCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String sesslawcct = getFeature(result, "sesslawcct", 1);
        assertEquals("0", sesslawcct);
    }

    @Test
    public void testFormCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String formcct = getFeature(result, "formcct", 1);
        assertEquals("0", formcct);
    }

    @Test
    public void testCopyRightCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String copyrightcct = getFeature(result, "copyrightcct", 1);
        assertEquals("0", copyrightcct);
    }

    @Test
    public void testEncParty1() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/55PV-T3B1-F04F-11W7-00000-00.xml");

        String encparty1 = getFeature(result, "encparty1", 9);
        assertNotNull(encparty1);
    }

    @Test
    public void testEncParty2() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/55PV-T3B1-F04F-11W7-00000-00.xml");

        String encparty2 = getFeature(result, "encparty2", 9);
        assertNotNull(encparty2);
    }

    @Test
    public void testBookCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String bookcct = getFeature(result, "bookcct", 1);
        assertEquals("0", bookcct);
    }

    @Test
    public void testAdministrativeCodeIsNotAdCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5HFG-D6D1-F04F-02FJ-00000-00.xml");

        String adcct = getFeature(result, "adcct", 4);
        assertEquals("0", adcct);
    }

    @Test
    public void testAdCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/594P-G5H1-F04F-01BB-00000-00.xml");

        String adcct = getFeature(result, "adcct", 9);
        assertEquals("1", adcct);
    }

    @Test
    public void testPatentCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5FH2-X9P1-F04D-71J3-00000-00.xml");

        String parentCct = getFeature(result, "patentcct", 1);
        assertEquals("0", parentCct);
    }

    @Test
    public void testMisCiteCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String misccct = getFeature(result, "misccct", 1);
        assertEquals("0", misccct);
    }


    @Test
    public void testPnum() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String pnum = getFeature(result, "pnum", 0);
        assertNotEquals("0", pnum);
    }

    @Test
    public void testPeriodicalCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String periodicalcct = getFeature(result, "periodicalcct", 1);
        assertEquals("0", periodicalcct);
    }

    @Test
    public void testStatCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/58RT-2M61-F04J-X00S-00000-00.xml");

        String statcct = getFeature(result, "statcct", 34);
        assertEquals("1", statcct);
    }

    @Test
    public void testRestatementStatCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5W6V-FGH1-JNJT-B4P7-00000-00.xml");

        String statcct = getFeature(result, "statcct", 28);
        assertEquals("1", statcct);
    }

    @Test
    public void testCrCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5FGT-6W61-F04C-T185-00000-00.xml");

        String crcct = getFeature(result, "crcct", 0);
        assertEquals("1", crcct);
    }

    @Test
    public void testOagCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String oagcct = getFeature(result, "oagcct", 1);
        assertEquals("0", oagcct);
    }

    @Test
    public void testExecDocCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String execdoccct = getFeature(result, "execdoccct", 1);
        assertEquals("0", execdoccct);
    }

    @Test
    public void testTextLength() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String textlen = getFeature(result, "len", 0);
        assertNotNull(textlen);
    }

    @Test
    public void testCaseCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5HM8-VCP1-F04D-R0GK-00000-00.xml");

        String casecct = getFeature(result, "casecct", 7);
        assertEquals("1", casecct);
    }

    @Test
    public void testConstCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5HM8-VCP1-F04D-R0GK-00000-00.xml");

        String constcct = getFeature(result, "constcct", 8);
        assertEquals("3", constcct);
    }

    @Test
    public void testLawrevCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/58RT-2M61-F04J-X00S-00000-00.xml");

        String lawrevcct = getFeature(result, "lawrevcct", 1);
        assertEquals("0", lawrevcct);
    }

    @Test
    public void testFollowedByCt() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/589S-3CP1-F0K0-S00K-00000-00.xml");

        int para = 37;
        assertEquals("&|&f|&|&|&|&", getFeature(result, "letters", para));
        assertEquals("1", getFeature(result, "followedbyct", para));
    }

    @Test
    public void testTreatedNoCase() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/589S-3CP1-F0K0-S00K-00000-00.xml");

        int para = 26;
        assertEquals("1", getFeature(result, "retrocasecct", para));
        assertEquals("0", getFeature(result, "casecct", para));
        assertEquals("1", getFeature(result, "trtct", para));
        assertEquals("0", getFeature(result, "treated_nocase", para));

        result = getFeatureExtractorResult("data/input/5HM8-VCP1-F04D-R0GK-00000-00.xml");

        para = 1;
        assertEquals("5", getFeature(result, "retrocasecct", para));
        assertEquals("5", getFeature(result, "casecct", para));
        assertEquals("0", getFeature(result, "trtct", para));
        assertEquals("0", getFeature(result, "treated_nocase", para));

        result = getFeatureExtractorResult("data/input/589S-3CP1-F0K0-S00K-00000-00.xml");

        para = 17;
        assertEquals("0", getFeature(result, "retrocasecct", para));
        assertEquals("0", getFeature(result, "casecct", para));
        assertEquals("1", getFeature(result, "trtct", para));
        assertEquals("1", getFeature(result, "treated_nocase", para));
    }

    @Test
    public void testCtext() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/56WB-VWJ1-DY0T-J208-00000-00.xml");

        int para = 10;
        assertEquals("5", getFeature(result, "casecct", para));
        assertEquals("0", getFeature(result, "mananaphcasect", para));
        assertTrue(getFeature(result, "ctext", para).contains("CASECITEHERE"));

        result = getFeatureExtractorResult("data/input/3RRM-Y840-003C-60YW-00000-00.xml");

        para = 20;
        assertEquals("0", getFeature(result, "casecct", para));
        assertEquals("1", getFeature(result, "mananaphcasect", para));
        assertFalse(getFeature(result, "ctext", para).contains("CASECITEHERE"));
    }

    @Test
    public void testExcessWhitespaceTrimmed() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5NX6-Y1S1-F048-C0FS-00000-00.xml");
        String text = getFeature(result, "text", 2);
        assertTrue(text.contains(
                "in pertinent part: \"Arbitration disclosures: \"Arbitration is final and binding on the parties."
        ));
        String ctext = getFeature(result, "ctext", 2);
        assertTrue(ctext.contains(
                "in pertinent part: \"Arbitration disclosures: \"Arbitration is final and binding on the parties."
        ));
    }

    @Test
    public void testWhitespaceBetweenBlockElements() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5NX6-Y1S1-F048-C0FS-00000-00-oneline-partial.xml");
        String text = getFeature(result, "text", 2);
        assertFalse(text.contains(
                "in pertinent part:\"Arbitration disclosures:\"Arbitration is final and binding on the parties."
        ));
        assertTrue(text.contains(
                "in pertinent part: \"Arbitration disclosures: \"Arbitration is final and binding on the parties."
        ));
        String ctext = getFeature(result, "ctext", 2);
        assertFalse(ctext.contains(
                "in pertinent part:\"Arbitration disclosures:\"Arbitration is final and binding on the parties."
        ));
        assertTrue(ctext.contains(
                "in pertinent part: \"Arbitration disclosures: \"Arbitration is final and binding on the parties."
        ));
    }

    @Test
    public void testIgnoreElements() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5XKV-YJ51-F528-G04S-00000-00.xml");
        String text = getFeature(result, "text", 41);
        assertFalse(text.contains("12 C.F.R. § 1002.9(c)(2) (2011) 2011-01-01 ."));
        assertTrue(text.contains("12 C.F.R. § 1002.9(c)(2) (2011) ."));
    }

    @Test
    public void testAgencyAnaphoricsNotCountedAsCases() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/57GC-XXF1-F04B-M3V7-00000-00.xml");

        int para = 7;
        assertEquals("0", getFeature(result, "allanaphcasect", para));
        assertEquals("0", getFeature(result, "mananaphcasect", para));
    }

    @Test
    public void testPinpointNotInFirstCitedResource() throws Exception {
        // D-21233 Tests that anaphorics are not marked as manual if the pinpoint is not in the first citedResource element
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5B0K-2J81-F048-C12P-00000-00.xml");

        int para = 1;
        assertEquals("1", getFeature(result, "allanaphcasect", para));
        assertEquals("0", getFeature(result, "mananaphcasect", para));
    }

    @Test
    public void testJCDAnaphorics() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/56WB-VWJ1-DY0T-J208-00000-00.xml");

        int para = 10;
        assertEquals("5", getFeature(result, "retrocct", para));
        assertEquals("5", getFeature(result, "cct", para));
        assertEquals("5", getFeature(result, "retrocasecct", para));
        assertEquals("5", getFeature(result, "casecct", para));
        assertEquals("2", getFeature(result, "allanaphcasect", para));
        assertEquals("0", getFeature(result, "mananaphcasect", para));
    }

    @Test
    public void testCCV6Anaphorics() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/ccv6.xml");

        int para = 3;
        assertEquals("2", getFeature(result, "retrocct", para));
        assertEquals("2", getFeature(result, "cct", para));
        assertEquals("1", getFeature(result, "retrocasecct", para));
        assertEquals("1", getFeature(result, "casecct", para));
        assertEquals("1", getFeature(result, "allanaphcasect", para));
        assertEquals("0", getFeature(result, "mananaphcasect", para));
    }

    @Test
    public void testTmarkCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String tmarkcct = getFeature(result, "tmarkcct", 1);
        assertEquals("0", tmarkcct);
    }

    @Test
    public void testAdjCaseCct() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/589S-3CP1-F0K0-S00K-00000-00.xml");

        String adj_casecct = getFeature(result, "adj_casecct", 18);
        assertEquals("2", adj_casecct);
    }

    @Test
    public void testRelatedCitation() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/589S-3CP1-F0K0-S00K-00000-00.xml");

        assertEquals("4", getFeature(result, "retrocct", 16));
    }

    @Test
    public void testNonTreatmentLetters() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/5NX6-Y1S1-F048-C0FS-00000-00.xml");
        int para = 70;

        assertEquals("&|#", getFeature(result, "letters", para));
        assertEquals("0", getFeature(result, "trtct", para));
    }

    @Test
    public void testLetterNotFirstCitedResource() throws Exception {
        FeatureExtractorResult result = getFeatureExtractorResult("data/input/57MN-6RT1-F04K-J02F-00000-00.xml");
        int para = 4;

        assertEquals("s|s", getFeature(result, "letters", para));
        assertEquals("Same case at|Same case at", getFeature(result, "trts", para));
        assertEquals("2", getFeature(result, "trtct", para));
    }

    private String getFeature(FeatureExtractorResult result, String featureName, int paragraphIndex) throws Exception {
        FeatureExtractorRecord record = result.getFeatureExtractorRecords().get(paragraphIndex);
        
        assertNotNull(record);
        return record.getFeature(featureName);
    }



    private FeatureExtractorResult getFeatureExtractorResult(String filePath) throws Exception{
        ClassLoader classLoader = getClass().getClassLoader();
        URL documentResource = classLoader.getResource(filePath);

        Path documentPath = new File(documentResource.getFile()).toPath();
        String documentXml = new String(Files.readAllBytes(documentPath));

        Document doc = DomParser.readDocument(new InputSource(new StringReader(documentXml)), true);

        FeatureExtractorCore core = new FeatureExtractorCore();
        return core.process(doc);
    }
}