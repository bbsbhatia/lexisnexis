import glob
import argparse
import os
from pathlib import Path

from tinydb import TinyDB, Query
from datalake import DataLakeObject

regions = ['staging', 'prod']
modes = ['init', 'run', 'recover', 'stats']


def create_object(region, **kwargs):
    collection = kwargs['collection']
    file = kwargs['file']
    mime = kwargs['mime_type']
    api_key = kwargs['api_key']
    oid, url, error = DataLakeObject(api_key, region).create(file, mime, collection)
    if not error:
        print('Created Object (id: {}, url: {})'.format(oid, url))
    return oid, error


def uploader_init(db, args):
    db.purge()


def uploader_run(db, args):
    files = glob.glob(args.files)
    print("Processing {} files from listing {}".format(len(files), args.files))
    for f in files:
        db.insert({'region': args.region, 'api_key': args.key, 'collection': args.collection,
                   'file': f, 'mime_type': args.type, 'status': 'pending'})
    for f in files:
        oid, error = create_object(args.region, collection=args.collection, api_key=args.key, mime_type=args.type, file=f)
        patch = {'status': 'uploaded', 'oid': oid} if not error else {'status': 'failed', 'error': error}
        query = Query()
        db.update(patch, query.file == f)


def uploader_stats(db, args):
    query = Query()
    u_docs = db.search(query.status == 'uploaded')
    p_docs = db.search(query.status == 'pending')
    f_docs = db.search(query.status == 'failed')
    print('uploaded:{}, pending:{}, failed:{}'.format(len(u_docs), len(p_docs), len(f_docs)))
    for d in f_docs:
        print(d)


def uploader_recover(db, args):
    query = Query()
    items = db.search(query.status != 'uploaded')
    for i in items:
        f = i['file']
        print(f)
        oid, error = create_object(args.region, collection=args.collection, api_key=args.key, mime_type=args.type, file=f)
        patch = {'status': 'uploaded', 'oid': oid} if not error else {'status': 'failed', 'error': error}
        query = Query()
        db.update(patch, query.file == f)


uploader_function = {
    'init': uploader_init,
    'run': uploader_run,
    'stats': uploader_stats,
    'recover': uploader_recover
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Upload Documents.')
    parser.add_argument("-m", "--mode", type=str, choices=modes,
                        help="script execution mode", required=True)
    parser.add_argument("-f", "--files", type=str,
                        help="source files", required=False)
    parser.add_argument("-r", "--region", type=str, choices=regions,
                        help="data lake region", default="prod")
    parser.add_argument("-c", "--collection", type=str,
                        help="data lake collection name", required=False)
    parser.add_argument("-t", "--type", type=str,
                        help="file mime type", required=False)
    parser.add_argument("-k", "--key", type=str,
                        help="data lake api key", required=False)
    args = parser.parse_args()

    db_file = os.path.join(Path(__file__).parent.parent, 'datalake_uploader.json')

    db = TinyDB(db_file)

    uploader_function[args.mode](db, args)
