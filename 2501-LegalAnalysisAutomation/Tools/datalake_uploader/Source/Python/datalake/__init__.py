# Copyright 2019 Reed Elsevier Inc, or its affiliates. All Rights Reserved.
#
# This package provides an API to interact with Data Lake.
#
# For more information refer to https://wiki.regn.net/wiki/DataLake_Hello_World

from . import owner
from . import collection
from . import object

__all__ = ['DataLakeOwner', 'DataLakeCollection', 'DataLakeObject']

DataLakeOwner = owner.DataLakeOwner
DataLakeCollection = collection.DataLakeCollection
DataLakeObject = object.DataLakeObject
