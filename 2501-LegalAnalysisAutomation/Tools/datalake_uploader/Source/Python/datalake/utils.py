import logging


def logger(name):
    lgr = logging.getLogger(name)
    if lgr.hasHandlers():
        return lgr
    lgr.setLevel(logging.INFO)
    sh = logging.StreamHandler()
    sh.setLevel(logging.INFO)
    lgr.addHandler(sh)
    return lgr
