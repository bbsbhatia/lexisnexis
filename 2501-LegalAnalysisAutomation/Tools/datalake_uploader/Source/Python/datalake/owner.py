from requests import request
from requests.exceptions import RequestException
import json
from . import constants
from . import utils


class DataLakeOwner:

    def __init__(self, region='prod', version='v1'):
        base_url = constants.dl_prod_url if region == 'prod' else constants.dl_staging_url
        self.api_url = f'{base_url}/administration/{version}/owners'
        self.logger = utils.logger('DataLakeOwner')

    def create(self, name, email, collection_prefix):

        self.logger.info('create owner: {}, {}'.format(name, email))

        headers = {
            'Accept': 'application/json',
            'Content-type': 'application/json',
        }

        params = {
            'owner-name': name,
            'email-distribution': [email],
            "collection-prefixes": [collection_prefix]
        }

        try:
            response = request('POST', self.api_url, headers=headers, data=json.dumps(params))
            self.logger.debug('url: {}'.format(self.api_url))
            self.logger.debug('request: {}'.format(response.request.headers))
            self.logger.debug('request: {}'.format(response.request.body))
            self.logger.debug('request: {}'.format(response.request))
            self.logger.debug('response: {}\n{}'.format(response.status_code, response.text))
            if response.ok:
                owner = json.loads(response.text)['owner']
                return owner['owner-id'], owner['api-key'], None
            else:
                error = json.loads(response.text)['error']
                self.logger.error('error: %s', error)
                return None, None, error
        except RequestException as e:
            self.logger.error('error: {}'.format(e))
            return None, None, str(e)

    def delete(self, owner_id, api_key):

        self.logger.info('delete owner: {}'.format(owner_id))

        headers = {
            'Accept': 'application/json',
            'x-api-key':  api_key

        }

        api_url = f'{self.api_url}/{owner_id}'

        try:
            response = request('DELETE', api_url, headers=headers)
            self.logger.debug('url: {}'.format(api_url))
            self.logger.debug('request headers: {}'.format(response.request.headers))
            self.logger.debug('request body: {}'.format(response.request.body))
            self.logger.debug('response: {}\n{}'.format(response.status_code, response.text))
            if response.ok:
                owner = json.loads(response.text)['owner']
                return owner['owner-id'], None
            else:
                error = json.loads(response.text)['error']
                self.logger.error('error: %s', error)
                return None, error
        except RequestException as e:
            self.logger.error('error: {}'.format(e))
            return None, str(e)

