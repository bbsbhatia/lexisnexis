from requests import request
from requests.exceptions import RequestException
import json
from . import constants
from . import utils


class DataLakeCollection:

    def __init__(self, api_key, region='prod', version='v1'):
        base_url = constants.dl_prod_url if region == 'prod' else constants.dl_staging_url
        version = version
        self.api_key = api_key
        self.api_url = f'{base_url}/collections/{version}'
        self.logger = utils.logger('DataLakeCollection')

    def create(self, name, description, asset_id, retention=10):

        self.logger.info('create collection: {}'.format(name))

        headers = {
            'Accept': 'application/json',
            'Content-type': 'application/json',
            'x-api-key': self.api_key
        }

        params = {
            "object-expiration-hours": retention * 365 * 24,
            "asset-id": asset_id,
            "classification-type": "Content",
            "description": description,
            "old-object-versions-to-keep": 0
        }

        url = f'{self.api_url}/{name}'

        try:
            response = request('POST', url, headers=headers, data=json.dumps(params))
            self.logger.debug('url: {}'.format(url))
            self.logger.debug('request: {}'.format(response.request.headers))
            self.logger.debug('request: {}'.format(response.request.body))
            self.logger.debug('response: {}\n{}'.format(response.status_code, response.text))
            if response.ok:
                collection = json.loads(response.text)['collection']
                return collection['collection-id'], collection['collection-url'], None
            else:
                error = json.loads(response.text)['error']
                self.logger.error('error: %s', error)
                return None, None, error
        except RequestException as e:
            self.logger.error('error: {}'.format(e))
            return None, None, str(e)

    def suspend(self, name):

        self.logger.info('suspend collection: {}'.format(name))

        headers = {
            'Accept': 'application/json',
            'x-api-key':  self.api_key

        }

        params = {
            "patch-operations": [
                {
                    "op": "replace",
                    "path": "/suspended",
                    "value": True
                }
            ]
        }

        url = f'{self.api_url}/{name}'

        try:
            response = request('PATCH', url, headers=headers, data=json.dumps(params))
            self.logger.debug('url: {}'.format(url))
            self.logger.debug('request: {}'.format(response.request.headers))
            self.logger.debug('request: {}'.format(response.request.body))
            self.logger.debug('response: {}\n{}'.format(response.status_code, response.text))
            if response.ok:
                collection = json.loads(response.text)['collection']
                return collection['collection-id'], None
            else:
                error = json.loads(response.text)['error']
                self.logger.error('error: %s', error)
                return None, error
        except RequestException as e:
            self.logger.error('error: {}'.format(e))
            return None, str(e)

    def delete(self, name):

        self.logger.info('delete collection: {}'.format(name))

        headers = {
            'Accept': 'application/json',
            'x-api-key':  self.api_key

        }

        url = f'{self.api_url}/{name}'

        try:
            response = request('DELETE', url, headers=headers)
            self.logger.debug('url: {}'.format(url))
            self.logger.debug('request: {}'.format(response.request.headers))
            self.logger.debug('request: {}'.format(response.request.body))
            self.logger.debug('response: {}\n{}'.format(response.status_code, response.text))
            if response.ok:
                collection = json.loads(response.text)['collection']
                return collection['collection-id'], None
            else:
                error = json.loads(response.text)['error']
                self.logger.error('error: %s', error)
                return None, error
        except RequestException as e:
            self.logger.error('error: {}'.format(e))
            return None, str(e)
