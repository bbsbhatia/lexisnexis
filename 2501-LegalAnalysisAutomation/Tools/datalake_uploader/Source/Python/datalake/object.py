from requests import request
from requests.exceptions import RequestException
import json
import os
from . import constants
from . import utils


class DataLakeObject:

    def __init__(self, api_key, region='prod', version='v1'):
        base_url = constants.dl_prod_url if region == 'prod' else constants.dl_staging_url
        self.api_key = api_key
        self.api_url = f'{base_url}/objects/{version}'
        self.logger = utils.logger('DataLakeObject')

    def create(self, file, mime_type, collection_id):

        self.logger.info('create object: {}'.format(file))

        headers = {
            'Accept': 'application/json',
            'Content-type': mime_type,
            'x-api-key': self.api_key
        }

        with open(file, "r", encoding="utf-8") as f:
            try:
                filename = os.path.basename(f.name)
                content = f.read()
                url = f'{self.api_url}/{filename}?collection-id={collection_id}'
                response = request('POST', url, headers=headers, data=content)
                self.logger.debug('url: {}'.format(url))
                self.logger.debug('request: {}'.format(response.request.headers))
                self.logger.debug('request: {}'.format(response.request.body))
                self.logger.debug('response: {}\n{}'.format(response.status_code, response.text))
                if response.ok:
                    dl_object = json.loads(response.text)['object']
                    return dl_object['object-id'], dl_object['object-url'], None
                else:
                    error = json.loads(response.text)['error']
                    self.logger.error('error: %s', error)
                    return None, None, error
            except Exception as e:
                self.logger.error('error: {}'.format(e))
                return None, None, str(e)

    def delete(self, object_id, collection_id):

        self.logger.info('delete object: {}'.format(object_id))

        headers = {
            'Accept': 'application/json',
            'x-api-key':  self.api_key

        }

        url = f'{self.api_url}/{object_id}?collection-id={collection_id}'

        try:
            response = request('DELETE', url, headers=headers)
            self.logger.debug('url: {}'.format(url))
            self.logger.debug('request: {}'.format(response.request.headers))
            self.logger.debug('request: {}'.format(response.request.body))
            self.logger.debug('response: {}\n{}'.format(response.status_code, response.text))
            if response.ok:
                dl_object = json.loads(response.text)['object']
                return dl_object['object-id'], None
            else:
                error = json.loads(response.text)['error']
                self.logger.error('error: %s', error)
                return None, error
        except RequestException as e:
            self.logger.error('error: {}'.format(e))
            return None, str(e)
