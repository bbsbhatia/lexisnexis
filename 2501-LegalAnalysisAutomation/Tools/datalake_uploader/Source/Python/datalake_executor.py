import argparse
from datalake import DataLakeCollection, DataLakeOwner, DataLakeObject

dl_regions = ['staging', 'prod']
dl_entities = ['owner', 'collection', 'object']
dl_operations = ['create', 'delete', 'suspend']


def create_owner(region, **kwargs):
    name = kwargs['name']
    email = kwargs['email']
    prefix = kwargs['prefix']
    owner = DataLakeOwner(region)
    owner_id, api_key, error = owner.create(name, email, prefix)
    if not error:
        print('Created Owner (owner_id: {}, api_key: {})'.format(owner_id, api_key))


def delete_owner(region, **kwargs):
    owner_id = kwargs['id']
    api_key = kwargs['api_key']
    owner = DataLakeOwner(region)
    oid, error = owner.delete(owner_id, api_key)
    if not error:
        print('Deleted Owner (owner_id: {})'.format(oid))


def create_collection(region, **kwargs):
    name = kwargs['collection']
    desc = kwargs['desc']
    asset_id = kwargs['asset_id']
    api_key = kwargs['api_key']
    cid, url, error = DataLakeCollection(api_key, region).create(name, desc, asset_id)
    if not error:
        print('Created Collection (id: {}, url: {})'.format(cid, url))


def delete_collection(region, **kwargs):
    name = kwargs['collection']
    api_key = kwargs['api_key']
    dl_collection = DataLakeCollection(api_key, region)
    cid, error = dl_collection.suspend(name)
    if not error:
        print('Suspended Collection (id: {})'.format(cid))
    cid, error = dl_collection.delete(name)
    if not error:
        print('Deleted Collection (id: {})'.format(cid))


def create_object(region, **kwargs):
    collection = kwargs['collection']
    file = kwargs['file']
    mime = kwargs['mime_type']
    api_key = kwargs['api_key']
    oid, url, error = DataLakeObject(api_key, region).create(file, mime, collection)
    if not error:
        print('Created Object (id: {}, url: {})'.format(oid, url))


def delete_object(region, **kwargs):
    collection = kwargs['collection']
    oid = kwargs['id']
    api_key = kwargs['api_key']
    oid, error = DataLakeObject(api_key, region).delete(oid, collection)
    if not error:
        print('Deleted Object (id: {})'.format(oid))


entity_function = {
    'owner:create': create_owner,
    'owner:delete': delete_owner,
    'collection:create': create_collection,
    'collection:delete': delete_collection,
    'object:create': create_object,
    'object:delete': delete_object
}


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Invoke Data Lake API.')
    parser.add_argument("-e", "--entity", type=str, choices=dl_entities,
                        help="data lake entity", required=True)
    parser.add_argument("-o", "--operation", type=str, choices=dl_operations,
                        help="data lake operation", required=True)
    parser.add_argument("-r", "--region", type=str, choices=dl_regions,
                        help="data lake region", default="prod")
    parser.add_argument("-n", "--name", type=str,
                        help="data lake entity name", required=False)
    parser.add_argument("-c", "--collection", type=str,
                        help="data lake collection name", required=False)
    parser.add_argument("-m", "--mail", type=str,
                        help="data lake owner email", required=False)
    parser.add_argument("-p", "--prefix", type=str,
                        help="data lake collection prefix", required=False)
    parser.add_argument("-i", "--id", type=str,
                        help="data lake entity id", required=False)
    parser.add_argument("-k", "--key", type=str,
                        help="data lake api key", required=False)
    parser.add_argument("-a", "--asset_id", type=int,
                        help="data lake collection asset id", required=False)
    parser.add_argument("-d", "--desc", type=str,
                        help="data lake collection description", required=False)
    parser.add_argument("-f", "--file", type=str,
                        help="file to be uploaded as data lake object", required=False)
    parser.add_argument("-t", "--type", type=str,
                        help="file mime type", required=False)

    args = parser.parse_args()
    dl_entity, dl_operation = args.entity, args.operation

    function_args = {
        'region': args.region,
        'api_key': args.key,
        'id': args.id,
        'asset_id': args.asset_id,
        'prefix': args.prefix,
        'email': args.mail,
        'name': args.name,
        'desc': args.desc,
        'collection': args.collection,
        'file': args.file,
        'mime_type': args.type
    }

    function_key = dl_entity+':'+dl_operation
    entity_function[function_key](**function_args)

