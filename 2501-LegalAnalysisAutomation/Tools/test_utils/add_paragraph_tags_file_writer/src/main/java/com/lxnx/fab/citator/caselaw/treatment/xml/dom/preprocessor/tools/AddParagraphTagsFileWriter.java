package com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.tools;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.AdminInterpretationDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.AgencyFlatDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.DigestDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.helper.FeatureExtractorDocumentHandler;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.FeatureExtractorPreProcessor;
import com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.FeatureExtractorPreProcessorAddParagraphTags;
import com.lxnx.fab.citator.common.xml.dom.DomParser;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.nio.file.*;
import java.util.HashMap;

/**
 * Tool for running the FeatureExtractorPreProcessorAddParagraphTags to add system-generated paragraph tags to Agency/Tax
 * Documents which sometimes use standalone defaultldc:nl to break up paragraphs instead of wrapping them with defaultldc:p
 *
 * The preprocessor will add defaultldc:p tags with @isSystemGenerated="true" attribute.
 *
 * This tool writes the results to files for later testing of removal of the system-generated tags by the workflow team
 */
public class AddParagraphTagsFileWriter {
    private static HashMap<String, FeatureExtractorDocumentHandler> handlers = new HashMap<>();
    private static FeatureExtractorPreProcessor preprocessor = new FeatureExtractorPreProcessorAddParagraphTags();

    public static void main(String[] args) throws IOException {
        handlers.put("admindoc:AGENCYDEC-FLAT", new AgencyFlatDocumentHandler());
        handlers.put("admindoc:ADMIN-INTERPDOC-LDC", new AdminInterpretationDocumentHandler());
        handlers.put("admindoc:DIGESTDOC-LDC", new DigestDocumentHandler());

        if (args.length == 0)
            throw new IllegalArgumentException("Specify a source folder");

        Path srcFolder = Paths.get(args[0]);
        Path outFolder = srcFolder.resolve("out");
        try {
            Files.createDirectory(outFolder);
        } catch (FileAlreadyExistsException ignored) {
            // do nothing
        }

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(srcFolder)) {
            for (Path file : stream) {
                if (file.toFile().isDirectory()) continue;
                try {
                    processFile(outFolder, file);
                } catch (IOException | ErrorChainException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void processFile(Path outFolder, Path file) throws ErrorChainException, IOException {
        System.out.println("processing " + file);
        String documentXml = new String(Files.readAllBytes(file));
        Document doc = DomParser.readDocument(new InputSource(new StringReader(documentXml)), true);

        String docElemTagName = doc.getDocumentElement().getTagName();
        FeatureExtractorDocumentHandler handler = handlers.get(docElemTagName);
        preprocessor.preProcess(doc, handler);

        Path outfile = outFolder.resolve(file.getFileName());
        try (OutputStream outStream = Files.newOutputStream(outfile)) {
            DomWriter domWriter = new DomWriter("UTF-8", false, new PrintStream(outStream), DomWriter.DEFAULT_ENTITIES);
            domWriter.print(doc);
        }
    }
}
