/* ************************************************************************
 * This document is the property of LexisNexis and its contents are
 * proprietary to LexisNexis.  Reproduction in any form by anyone of the
 * materials contained herein without the permission of LexisNexis is
 * prohibited. Finders are asked to return this document to
 * LexisNexis, 9443 Springboro Pike, P.O.Box 933, Miamisburg, Ohio 45401-0933
 *
 *-----------------
 * File Name: DomWriter.java
 *
 * Description:
 *   This class converts an XML tree (or sub-tree) to a text representation
 *   and places the result on the PrintStream passed in.
 *
 *
 * Created by:  Jim Janko
 *
 *-----------------
 * Revision History:
 *  07/02/2001 - Initial creation.
 *
 *  06/19/2003 - J. Broering
 *    Added the private variable _entityRestorer, the setEntityRestorer()
 *      method and added functionality to the print() method to use
 *      _entityRestorer.restore().
 *
 *  Version 2.01
 *  08/06/2005 - J. Slusher
 *    Added private variables _writeHexEntities and _writeHexEntitiesAbove.
 *    Added a constructor that accepts an integer indicating the character
 *      number to write out hex entities above.  If it is set to anything
 *      greater than zero _writeHexEntities will be set to true.
 *    Added code to normalize() to write characters > than the number in
 *      _writeHexEntitiesAbove out as hex entities if _writeHexEntities
 *      is true.
 *
 *  Version 2.02
 *  09/13/2005 - J. Slusher
 *    Changed print() to stop calling normalize() on comment nodes.
 *    Added a comment to indicate that writeHexEntitiesAbove only
 *      affects text and attribute nodes.
 *
 *  Version 2.03
 *  10/05/2005 - J. Slusher
 *    Changed print() to use "print" and not "println" when writing out
 *      comments.  There should not be a newline following comments.
 *
 *  Version 2.04
 *  11/08/2005 - J. Slusher
 *    Cleaned up some spacing and organized imports.
 *
 *  Version 2.05
 *  12/9/2005 - Dustin Skaggs
 *    Added a constructor that take a noEntities argument.  This will cause
 *      entities to be interpreted as the characters they represent.
 *
 *  Version 2.06
 *  04/21/2006 - J. Slusher
 *    Added the concept of entity mode which takes the place of constructor
 *      parameters for writeHexEntitiesAbove and noEntities.
 *    Updated the constructor that accepts all possible options to accept
 *      the parameters entityMode and useHexForEscape.
 *    Updated to reset the escape values for the 5 special XML characters to
 *      their hexadecimal entities if useHexForEscape is true.
 *    Updated to set up _writeHexEntitiesAbove, _writeHexEntities, and
 *      _noEntities based on the entity mode.
 *    The constructors now throw ErrorChainException.
 *    Updated constructors to call the main constructor rather than duplicate
 *      functionality.
 *    Updated print() to stop printing a newline after DOCTYPE nodes, the
 *      XML declaration, and ENTITY nodes.
 *    Updated normalize() to accept a new parameter that indicates whether or
 *      not to escape the 5 special XML characters.
 *    Updated print() to call normalize with escSpecXMLChars set to false on
 *      comments and CDATA sections.
 *    When _noEntities is true, set escSpecXMLChars to false automatically
 *      rather than skipping normalize() entirely.
 *
 *  08/08/2006 - J. Slusher
 *    Updated constructor to uppercase the encoding when comparing it to the
 *      encoding constants.
 *
 *  Version 2.07
 *  09/18/2006 - J. Slusher
 *    Added a new entity mode: FORCE_HEX.  This entity mode will expand all
 *      named entities to their actual character and then normalize that
 *      character to a hexadecimal entity.  The result is that all named
 *      entities are written out as their hexadecimal equivalent.  FORCE_HEX
 *      also writes all characters greater than ASCII out as hexadecimal
 *      entities.
 *    Changed print() to write the normalized value of entity nodes when
 *      expand entities is true.
 *    Removed all processing and parameters for useHexForEscape.
 *
 *  Version 2.08
 *  10/17/08 - JainAS
 *  Related Webstar : 2281715
 *     Added an extra if condition in print() function-> case Node.ENTITY_REFERENCE_NODE:
 *       This condition will check the current node that is being processed
 *       is within the specialEntitiesList or not. If present ,it will retain the
 *       entity as its original name.This will avoid deleting of entities like
 *       cpright and fair-use which have multiple nodes to be processed, Hence
 *       such entities are to be included under specialEntitesToLook in
 *       domparser.properties file.
 *
 */
package com.lxnx.fab.citator.caselaw.treatment.xml.dom.preprocessor.tools;

import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import org.w3c.dom.Attr;
import org.w3c.dom.DocumentType;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.lexisnexis.wim.instrumentation.ErrorChainException;

/**
 * This class converts an XML tree (or sub-tree) to a text representation
 * and places the result on the PrintStream passed in.
 *
 * @author Jim Janko
 *
 */
public class DomWriter {

    private PrintWriter _output = null;

    private boolean _writeDefaults = false;

    private boolean _writeHexEntities = false;
    private int _writeHexEntitiesAbove = 0;
    private boolean _expandEntities = false;
    private boolean _noEntities = false;


    private String _encoding = "UTF-8";

    private static final String IBM1047_ENC = "IBM1047";
    private static final String LATIN1_ENC = "ISO-8859-1";

    // The default values to use when escaping the 5 special XML characters
    private String ESC_LT = "&lt;";
    private String ESC_GT = "&gt;";
    private String ESC_QUOT = "&quot;";
    private String ESC_APOS = "&apos;";
    private String ESC_AMP = "&amp;";

    /**
     * Entity Mode: FORCE_US_ASCII
     * All characters > 127 will be written out as their hexadecimal entity
     * rather than the actual character.
     */
    public static final String FORCE_US_ASCII = "FORCE_US_ASCII";
    /**
     * Entity Mode: FORCE_HEX
     * All characters > 127 will be written out as their hexadecimal entity
     * rather than the actual character.  All named entities will be expanded
     * and written out as their hexadecimal entity.
     */
    public static final String FORCE_HEX = "FORCE_HEX";
    /**
     * Entity Mode: NO_ENTITIES
     * All characters will be written out as their actual character instead of
     * an entity representation.  WARNING: This option should
     * only be used when writing out to a String because it will not create
     * well-formed XML (<,>,',",& will not be written out as entities).
     */
    public static final String NO_ENTITIES = "NO_ENTITIES";
    /**
     * Enitity Mode: DEFAULT
     * The encoding will determine how to write out characters > 127.
     * If the encoding is UTF-8, all characters will be written as
     * their actual character.
     * If the encoding is IBM1047, all characters > 127 will be written out as
     * their hexadecimal entity.
     * If the encoding is ISO-8859-1, all characters > 255 will be written out
     * as their hexadecimal entity.
     */
    public static final String DEFAULT_ENTITIES = "DEFAULT";

    /**
     * Create a new DomWriter object.
     *
     * @param encoding       the encoding to use when writing the document;
     *                       if null is passed in, then UTF-8 is assumed
     * @param writeDefaults  whether or not to write out default attributes
     * @param output         the stream to print the XML to
     *
     * @throws UnsupportedEncodingException
     */
    public DomWriter(
            String encoding,
            boolean writeDefaults,
            PrintStream output)
            throws UnsupportedEncodingException, ErrorChainException {

        this(encoding, writeDefaults, output, DEFAULT_ENTITIES);
    }

    /**
     * Create a new DomWriter object.
     *
     * @param  encoding         the encoding to use when writing the document;
     *                          if null is passed in, then UTF-8 is assumed
     * @param  writeDefaults    whether or not to write out default attributes
     * @param  output           the stream to print the XML to
     * @param  entityMode       a mode that indicates how to determine which
     *                          characters should be written as hexadecimal
     *                          entities versus which should be written as
     *                          actual characters; choices are FORCE_US_ASCII,
     *                          NO_ENTITIES, and DEFAULT.
     * @param  useHexForEscape  boolean indicating whether or not to use
     *                          hexadecimal entities when escaping the 5 special
     *                          XML characters; default is to use named
     *                          entities
     *
     * @throws UnsupportedEncodingException
     */
    public DomWriter(
            String encoding,
            boolean writeDefaults,
            PrintStream output,
            String entityMode)
            throws UnsupportedEncodingException, ErrorChainException {

        if (encoding != null) {
            _encoding = encoding;
        }
        _writeDefaults = writeDefaults;
        _output = new PrintWriter(new OutputStreamWriter(output, _encoding));

        if (entityMode.equals(FORCE_US_ASCII)) {
            _writeHexEntities = true;
            _writeHexEntitiesAbove = 127;
        } else if (entityMode.equals(FORCE_HEX)) {
            _writeHexEntities = true;
            _writeHexEntitiesAbove = 127;
            _expandEntities = true;
        } else if (entityMode.equals(NO_ENTITIES)) {
            _noEntities = true;
        } else if (entityMode.equals(DEFAULT_ENTITIES)) {
            if (_encoding.toUpperCase().equals(IBM1047_ENC)) {
                _writeHexEntities = true;
                _writeHexEntitiesAbove = 127;
            } else if (_encoding.toUpperCase().equals(LATIN1_ENC)) {
                _writeHexEntities = true;
                _writeHexEntitiesAbove = 255;
            }
        } else {
            throw new ErrorChainException(
                    "Unsupported entity mode: "
                            + entityMode
                            + ", entity mode must be one of the following: "
                            + FORCE_US_ASCII
                            + ", "
                            + NO_ENTITIES
                            + ", or "
                            + DEFAULT_ENTITIES);
        }
    }

    /**
     * Print the DOM tree to the output stream specified in the constructor.
     *
     * @param  currentNode  the node to print
     */
    public void print(Node currentNode) {

        if (currentNode == null) {
            return;
        }

        boolean hasChildren = false;

        int nodeType = currentNode.getNodeType();

        switch (nodeType) {
            case Node.DOCUMENT_NODE :
            {
                _output.print("<?xml version=\"1.0\" encoding=\"");
                _output.print(_encoding + "\"?>");

                NodeList children = currentNode.getChildNodes();
                for (int iChild = 0;
                     iChild < children.getLength();
                     iChild++) {
                    print(children.item(iChild));
                }
                _output.flush();
                break;
            }
            case Node.ELEMENT_NODE :
            {
                _output.print("<" + currentNode.getNodeName());
                NamedNodeMap attrs = currentNode.getAttributes();
                for (int i = 0; i < attrs.getLength(); i++) {
                    // If the attribute was specified in the document
                    // or the flag to explicitly write out default attributes
                    // is set, write out the attribute.
                    Attr attr = (Attr) attrs.item(i);
                    if ((attr.getSpecified()) || _writeDefaults) {
                        _output.print(" ");
                        _output.print(attr.getNodeName());
                        _output.print("=\"");
                        _output.print(
                                normalize(attr.getNodeValue(), true));
                        _output.print("\"");
                    }
                }

                NodeList children = currentNode.getChildNodes();
                if (children != null) {
                    int len = children.getLength();
                    if (len > 0) {
                        _output.print(">");
                        hasChildren = true;
                    } else {
                        _output.print("/>");
                    }
                    for (int i = 0; i < len; i++) {
                        print(children.item(i));
                    }
                } else {
                    _output.print("/>");
                }
                break;
            }
            case Node.ENTITY_REFERENCE_NODE :
            {
                if (_noEntities) {
                    Node child = currentNode.getFirstChild();
                    if (child != null) {
                        _output.print(child.getNodeValue());
                    }
                } else if (_expandEntities) {
                    Node child = currentNode.getFirstChild();
                    if (child != null) {
                        /* MODIFIED BY Infosys:JainAS on 10/17/2008 FOR WEBSTAR: 2281715 START*/
                        /* EXPLANATION : The if condition checks for _specialEntityList Array list for special entities.
                         * _specialEntityList is updated by SpecialEntitesToLook list in domparser.properties file, this
                         * file is read by readSpecialEntities() in EntityPreserver.java.
                         * Whenever the the condition finds the currentNode within special entity list it will retain the
                         * entity as its original name.
                         * Example : cpright if encountered will be retained as &cpright; in the processed output document.
                         */
                        /* <ORIGINAL CODE> This is an  new added functionality,there was no code previously */



                        _output.print(
                                normalize(child.getNodeValue(), true));


                        /* CREATED BY Infosys:JainAS on 10/17/2008 FOR WEBSTAR: 2281715 END*/

                    }
                } else {
                    _output.print('&');
                    _output.print(currentNode.getNodeName());
                    _output.print(';');
                }
                break;
            }
            case Node.CDATA_SECTION_NODE :
            {
                _output.print("<![CDATA[");
                _output.print(normalize(currentNode.getNodeValue(), false));
                _output.print("]]>");
                break;
            }
            case Node.TEXT_NODE :
            {
                _output.print(normalize(currentNode.getNodeValue(), true));
                break;
            }
            case Node.COMMENT_NODE :
            {
                _output.print(
                        "<!--"
                                + normalize(currentNode.getNodeValue(), false)
                                + "-->");
                break;
            }
            case Node.PROCESSING_INSTRUCTION_NODE :
            {
                _output.print("<?");
                _output.print(currentNode.getNodeName());
                String data = currentNode.getNodeValue();
                if (data != null && data.length() > 0) {
                    _output.print(' ');
                    _output.print(data);
                }
                _output.print("?>");
                break;
            }
            case Node.DOCUMENT_FRAGMENT_NODE :
                // I don't know how to handle this type.
                break;
            case Node.DOCUMENT_TYPE_NODE :
                _output.print("<!DOCTYPE ");
                _output.print(((DocumentType) currentNode).getName());
                _output.print(" PUBLIC \"");
                _output.print(((DocumentType) currentNode).getPublicId());
                _output.print("\"");
                if ((((DocumentType) currentNode).getSystemId()) != null) {
                    _output.print(" \"");
                    _output.print(((DocumentType) currentNode).getSystemId());
                    _output.print("\"");
                }
                if ((((DocumentType) currentNode).getInternalSubset())
                        != null) {
                    _output.print(" [");
                    _output.print(
                            ((DocumentType) currentNode).getInternalSubset());
                    _output.print("]");
                }
                _output.print(">");
                break;
            case Node.ENTITY_NODE :
                _output.print("<!ENTITY ");
                _output.print(currentNode.getNodeName());
                _output.print(" " + currentNode.getNodeValue());
                _output.print(">");
                break;
            case Node.NOTATION_NODE :
                // I don't know how to handle this type.
                break;
            default :
                break;
        } // switch

        if ((nodeType == Node.ELEMENT_NODE) && hasChildren) {
            _output.print("</");
            _output.print(currentNode.getNodeName());
            _output.print('>');
        }

        _output.flush();
    } /* print() */

    /**
     * Loop through each character and perform the following normalization:
     *   -Translate the 5 special characters to their escaped values if
     *    expandDefaultXMLChars is true.
     *   -If _writeHexEntities is true and the character is above the value
     *    of _writeHexEntitiesAbove, write the character out as a hexadecimal
     *    entity.
     *   -If the character is a newline or carriage return, do nothing so that
     *    the output will have no newlines or carriage returns.
     *
     * @param  s                the String to normalize
     * @param  escSpecXMLChars  boolean to indicate whether or not the
     *                          default XML characters (<,>,',",&) should
     *                          be translated; should be true except in
     *                          CDATA sections and comments
     *
     * @return the normalized String
     */
    protected String normalize(String s, boolean escSpecXMLChars) {



        StringBuffer str = new StringBuffer();
        int len = (s != null) ? s.length() : 0;

        for (int i = 0; i < len; i++) {
            char ch = s.charAt(i);

            if (_writeHexEntities && (ch > _writeHexEntitiesAbove)) {
                str.append("&#x" + Integer.toString((int) ch, 16) + ";");

            } else {
                if (escSpecXMLChars) {
                    switch (ch) {
                        case '<' :
                            str.append(ESC_LT);
                            continue;

                        case '>' :
                            str.append(ESC_GT);
                            continue;

                        case '"' :
                            str.append(ESC_QUOT);
                            continue;

                        case '\'' :
                            str.append(ESC_APOS);
                            continue;

                        case '&' :
                            str.append(ESC_AMP);
                            continue;
                    }
                    if (ch == '&') {
                        str.append(ESC_AMP);
                        continue;
                    }
                }

                str.append(ch);
            }
        }
        return (str.toString());
    } // normalize(String):String



} /*  class DomWriter */
