import sys
import csv
import json
import random
import re


def main(num_files, num_paras):
    lines = load_data('test_data.json', num_files)
    write_files(lines, num_paras)


def load_data(filename, num_files):
    with open(filename, 'r', encoding='utf-8') as all_data:
        lines = all_data.readlines()
    random.shuffle(lines)
    return lines[:num_files]


def write_files(lines, num_paras):
    with open('files.csv', 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file, dialect='excel')
        for i in range(len(lines)):
            request = json.loads(lines[i])
            filename = f'{i}.json'
            write_request(filename, request, num_paras)
            csv_writer.writerow([filename])


def write_request(filename, request, num_paras):
    paras = request['paragraphs']
    random.shuffle(paras)

    lni = re.findall(r'para_\d+_(.*)', paras[0]['para-id'])[0]

    # repeat paragraphs if request contains fewer than requested
    request['paragraphs'] = [paras[n % len(paras)]
                             for n in range(num_paras)]
    request['ptot'] = num_paras
    request['lni'] = lni
    with open(filename, 'w', encoding='utf-8') as json_file:
        json.dump(request, json_file)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('usage: python test_gen.py number_of_files number_of_paragraphs')
    else:
        main(num_files=int(sys.argv[1]), num_paras=int(sys.argv[2]))
