package com.lxnx.fab.citator.caselaw;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import com.amazonaws.serverless.proxy.model.AwsProxyRequest;
import com.amazonaws.serverless.proxy.model.AwsProxyResponse;
import com.amazonaws.services.lambda.runtime.Context;

public class XmlStreamHandler implements RequestStreamHandler{

    private static final int MAX_WIDTH = 256;

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

        String input = IOUtils.toString(inputStream, Charsets.UTF_8);

        ObjectMapper mapper = new ObjectMapper();
        AwsProxyRequest request = mapper.readValue(input, AwsProxyRequest.class);

        String message = request.getBody();
        String abbreviatedMessage = StringUtils.abbreviate(message, MAX_WIDTH).replaceAll("(\\r|\\n)", "");
        System.out.println(String.format("Message: %s", abbreviatedMessage));

        String result = callFeatureExtractor(message);

        String abbreviatedResult = StringUtils.abbreviate(result, MAX_WIDTH).replaceAll("(\\r|\\n)", "");
        System.out.println(String.format("Result: %s", abbreviatedResult));

        AwsProxyResponse response = new AwsProxyResponse();
        response.setBody(result);
        response.addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML_UTF_8.toString());
        response.setStatusCode(HttpStatus.SC_OK);

        String output = mapper.writeValueAsString(response);

        outputStream.write(output.getBytes(Charsets.UTF_8));
    }

    private String callFeatureExtractor(String message){
        try {
            FeatureExtractor featureExtractor = new FeatureExtractor();
            return featureExtractor.process(message);
        } catch (Exception e) {
            System.out.println("Exception during feature extraction: "+e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}
