package com.lxnx.fab.citator.caselaw;

import com.lexisnexis.wim.instrumentation.ErrorChainException;
import com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorCore;
import com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorRecord;
import com.lxnx.fab.citator.caselaw.treatment.FeatureExtractorResult;
import com.lxnx.fab.citator.common.xml.XMLUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Map;

public class FeatureExtractor {

    public String process(String documentXML) throws ParserConfigurationException, IOException, SAXException, ErrorChainException {
        InputSource input = new InputSource(new StringReader(documentXML));

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(input);

        FeatureExtractorCore core = new FeatureExtractorCore();

        FeatureExtractorResult result = core.process(doc);

        return buildXMLOutput(result);
    }


    private String buildXMLOutput(FeatureExtractorResult result)  {
            StringBuilder paragraphs = new StringBuilder();
            for (FeatureExtractorRecord featureRecord : result.getFeatureExtractorRecords()) {
                featureRecord.addFeature("year", String.valueOf(result.getYear()));
                StringBuilder features = new StringBuilder();
                for (Map.Entry<String, String> entry : featureRecord.getAllFeatures().entrySet()) {
                    features.append(String.format("<feature %s=\"%s\"/>", entry.getKey(), XMLUtils.encodeXML(entry.getValue())));
                }
                paragraphs.append(String.format("<paragraph>%s</paragraph>", features.toString()));
            }
            return String.format("<document lni=\"%s\" courtlevel=\"%s\" ptot=\"%s\">%s</document>",
                    result.getLni(), result.getCourtLevel(), result.getFeatureExtractorRecords().size(), paragraphs);
    }
}
