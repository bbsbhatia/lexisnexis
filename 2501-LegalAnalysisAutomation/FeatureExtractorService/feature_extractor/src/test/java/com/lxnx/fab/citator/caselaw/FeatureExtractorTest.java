package com.lxnx.fab.citator.caselaw;

import org.junit.Test;
import org.junit.Before;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import java.util.Properties;

import static org.junit.Assert.*;

public class FeatureExtractorTest {

    @Test
    public void testProcess() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/55PV-T3B1-F04F-11W7-00000-00.xml");
        assertNotNull(featureXml);
    }

    @Test
    public void testJuryInstCct() throws Exception {

        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String juryinstrcct = getFeature(featureXml, "juryinstrcct", 1, true);
        assertEquals("0", juryinstrcct);
    }

    @Test
    public void testCct() throws Exception {

        String featureXml = getFeatureXml("data/input/jcd/5704-2VV1-F04J-X0F1-00000-00.xml");

        String cct = getFeature(featureXml, "cct", 1, true);
        assertEquals("1", cct);

    }

    @Test
    public void testRegCct() throws Exception {
        String featureXml = getFeatureXml("data/input/agency/55CH-69W0-01KR-B08W-00000-00.xml");

        String regcct = getFeature(featureXml, "regcct", 7, true);
        assertEquals("1", regcct);
    }

    @Test
    public void testAnnotCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String annotcct = getFeature(featureXml, "annotcct", 2, true);
        assertEquals("0", annotcct);
    }

    @Test
    public void testLastHead() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String lastHead = getFeature(featureXml, "last-head", 2, true);
        assertEquals("MEMORANDUM AND ORDER PURSUANT TO RULE 1:28",lastHead);
    }

    @Test
    public void testSessLawCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String sesslawcct = getFeature(featureXml, "sesslawcct", 2, true);
        assertEquals("0", sesslawcct);
    }

    @Test
    public void testFormCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String formcct = getFeature(featureXml, "formcct", 2, true);
        assertEquals("0", formcct);
    }

    @Test
    public void testCopyRightCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String copyrightcct = getFeature(featureXml, "copyrightcct", 2, true);
        assertEquals("0", copyrightcct);
    }

    @Test
    public void testEncParty1() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/55PV-T3B1-F04F-11W7-00000-00.xml");

        String encparty1 = getFeature(featureXml, "encparty1", 10, true);
        assertNotNull(encparty1);
    }

    @Test
    public void testEncParty2() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/55PV-T3B1-F04F-11W7-00000-00.xml");

        String encparty2 = getFeature(featureXml, "encparty2", 10, true);
        assertNotNull(encparty2);
    }

    @Test
    public void testBookCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String bookcct = getFeature(featureXml, "bookcct", 2, true);
        assertEquals("0", bookcct);
    }

    @Test
    public void testAdCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/594P-G5H1-F04F-01BB-00000-00.xml");

        String adcct = getFeature(featureXml, "adcct", 10, true);
        assertEquals("1", adcct);
    }

    @Test
    public void testPatentCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5FH2-X9P1-F04D-71J3-00000-00.xml");

        String parentCct = getFeature(featureXml, "patentcct", 2, true);
        assertEquals("0", parentCct);
    }

    @Test
    public void testMisCiteCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5HM8-VCP1-F04D-R0GK-00000-00.xml");

        String misccct = getFeature(featureXml, "misccct", 9, true);
        assertEquals("2", misccct);
    }


    @Test
    public void testPnum() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String pnum = getFeature(featureXml, "pnum", 1, true);
        assertNotEquals("0", pnum);
    }

    @Test
    public void testPeriodicalCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String periodicalcct = getFeature(featureXml, "periodicalcct", 2, true);
        assertEquals("0", periodicalcct);
    }

    @Test
    public void testStatCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/58RT-2M61-F04J-X00S-00000-00.xml");

        String statcct = getFeature(featureXml, "statcct", 35, true);
        assertEquals("1", statcct);
    }

    @Test
    public void testCrCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5FGT-6W61-F04C-T185-00000-00.xml");

        String crcct = getFeature(featureXml, "crcct", 1, true);
        assertEquals("1", crcct);
    }

    @Test
    public void testOagCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String oagcct = getFeature(featureXml, "oagcct", 2, true);
        assertEquals("0", oagcct);
    }

    @Test
    public void testExecDocCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String execdoccct = getFeature(featureXml, "execdoccct", 2, true);
        assertEquals("0", execdoccct);
    }

    @Test
    public void testTextLength() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String textlen = getFeature(featureXml, "textlen", 1, true);
        assertNotNull(textlen);
    }

    @Test
    public void testCaseCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5HM8-VCP1-F04D-R0GK-00000-00.xml");

        String casecct = getFeature(featureXml, "casecct", 8, true);
        assertEquals("1", casecct);
    }

    @Test
    public void testConstCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5HM8-VCP1-F04D-R0GK-00000-00.xml");

        String constcct = getFeature(featureXml, "constcct", 9, true);
        assertEquals("3", constcct);
    }

    @Test
    public void testLawrevCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/58RT-2M61-F04J-X00S-00000-00.xml");

        String lawrevcct = getFeature(featureXml, "lawrevcct", 43, true);
        assertEquals("1", lawrevcct);
    }

    @Test
    public void testCtext() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String ctext = getFeature(featureXml, "ctext", 2, true);
        assertNotNull(ctext);
    }

    @Test
    public void testText() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String text = getFeature(featureXml, "text", 19, true);
        assertNotNull(text);
    }

    @Test
    public void testMarkCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/5RF2-MC51-F15C-B0NF-00000-00.xml");

        String tmarkcct = getFeature(featureXml, "tmarkcct", 2, true);
        assertEquals("0", tmarkcct);
    }

    @Test
    public void testAllAnaphCaseCt() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/4SM7-VBY0-TXFN-F3C0-00000-00.xml");

        String allanaphcasect = getFeature(featureXml, "allanaphcasect", 59, true);
        assertEquals("3", allanaphcasect);
    }

    @Test
    public void testMananaphcaseCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String mananaphcasect = getFeature(featureXml, "mananaphcasect", 19, true);
        assertEquals("1", mananaphcasect);
    }

    @Test
    public void testTrts() throws Exception{
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String trts = getFeature(featureXml, "trts", 19, true);
        assertEquals("Followed by", trts);
    }

    @Test
    public void testLetters() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String letters = getFeature(featureXml, "letters", 19, true);
        assertEquals("f", letters);
    }

    @Test
    public void testFollowedByCt() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String followedbyct = getFeature(featureXml, "followedbyct", 19, true);
        assertEquals("1", followedbyct);
    }

    @Test
    public void testAdjCaseCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String adj_casecct = getFeature(featureXml, "adj_casecct", 19, true);
        assertEquals("2", adj_casecct);
    }

    @Test
    public void testRetroCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String retrocct = getFeature(featureXml, "retrocct", 19, true);
        assertEquals("3", retrocct);
    }

    @Test
    public void testNegCct() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String negct = getFeature(featureXml, "negct", 42, true);
        assertEquals("2", negct);
    }

    @Test
    public void testTrtCt() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String trtct = getFeature(featureXml, "trtct", 19, true);
        assertEquals("1", trtct);
    }

    @Test
    public void testYear() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String year = getFeature(featureXml, "year", 19, true);
        assertEquals("2013", year);
    }

    @Test
    public void testSrc() throws Exception {
        String featureXml = getFeatureXml("data/input/jcd/589S-3CP1-F0K0-S00K-00000-00.xml");

        String src = getFeature(featureXml, "src", 19, true);
        assertEquals("majority", src);
    }

    private String getFeature(String featureXml, String featureName, int paragraphIndex, boolean useParagraphIndex) throws Exception {
        String expectedValue = null;

        InputSource input = new InputSource(new StringReader(featureXml));

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(input);

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();

        if(useParagraphIndex){
            expectedValue = getXPathExpression(doc, xpath, featureName, paragraphIndex);
        }
        else {
            expectedValue = getXPathExpression(doc, xpath, featureName);
        }
        return expectedValue;
    }

    private String getXPathExpression(Document document, XPath xPath, String featureName)
            throws XPathExpressionException {
        String featureValue;
        try {
            String xpath = String.format("//paragraph/feature/@%s!='0'", featureName);
            XPathExpression expression = xPath.compile(xpath);
            featureValue = (String) expression.evaluate(document, XPathConstants.STRING);
        } catch (XPathExpressionException e){
            String msg = "Unable to create xml string output: " + e;
            throw new XPathExpressionException(msg);
        }

        return featureValue;
    }

    private String getXPathExpression(Document document, XPath xPath, String featureName, int paragraphIndex)
            throws XPathExpressionException {
        String featureValue;

        try {
            String xpath = String.format("//paragraph[%d]/feature/@%s", paragraphIndex, featureName);
            XPathExpression expression = xPath.compile(xpath);
            featureValue = (String) expression.evaluate(document, XPathConstants.STRING);
        } catch (XPathExpressionException e){
            String msg = "Unable to create xml string output: " + e;
            throw new XPathExpressionException(msg);
        }

        return featureValue;
    }


    private String getFeatureXml(String filePath) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();

        URL documentResource = classLoader.getResource(filePath);


        Path documentPath = new File(documentResource.getFile()).toPath();
        String documentXml = new String(Files.readAllBytes(documentPath));
        if (!documentXml.isEmpty()){
            FeatureExtractor featureExtractor = new FeatureExtractor();
            return featureExtractor.process(documentXml);
        }
        throw new Exception("Empty document " + filePath);
    }

    private File getRootFolder(String rootFolder) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        URL documentResource = classLoader.getResource(rootFolder);
        return new File(documentResource.getFile());
    }
    private File[] getResourceFolderFiles(String folder) throws Exception{
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        URL url = classLoader.getResource(folder);
        String path = url.getPath();
        return new File(path).listFiles();
    }

    private String getRelativePath(File file, File folder, String appendString){
        String filepath = file.getAbsolutePath();
        String folderPath = folder.getAbsolutePath();
        if(filepath.startsWith(folderPath)) {
            String relativePath = filepath.substring(folderPath.length() + 1);
            return (appendString + "/" + relativePath);
        }
        return null;
    }
}