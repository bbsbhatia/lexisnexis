# DevOpsShared Library  <div id="jenkins-library" />
##  Overview

The [BCS-Cloud DevOps](mailto:BCS-CloudDevOps@reedelsevier.com) Team maintains this [Shared Library](https://jenkins.io/doc/book/pipeline/shared-libraries/) for Jenkins.  By default, this library is available in all pipeline jobs run within the BCS Jenkins environment.  The library is maintained and sourced from the [1939-JenkinsLibraries](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries) repo in the Content Project within VSTS.

The intent is for this shared library to be a common location for all users to contribute and evolve best practices and common code for use in Jenkins Pipelines.  

Additionally, this library serves as the control point for securely accessing AWS from Jenkins.  

Collectively this Library will be treated similar to an SDK.

## Contributing
If you would like to contribute, please perform your development in a feature branch.  You can reference your feature branch in your pipelines using the following code at the top of your file.

>`@Library('DevOpsShared@branchName') _`

Once development is complete, please create a Pull Request to the current version branch, and personally reach out to the Required Reviewer(s) to schedule a code review.

## Versions
- [Version Log](./VERSIONS.md)

## Step Documentation
- [apigateway()](/vars/apigateway.md)
- [boto3()](/vars/boto3.md)
- [buildAmi()](/vars/buildAmi.md)
- [cloudformation()](/vars/cloudformation.md)
- [s3()](/vars/s3.md)

## As-of-yet-Undocumented Steps
- commons
- buildReleaseUnit()
- checkQuality()
- publishReleaseUnit()
- stageReleaseUnit()
- withAwsAccount()