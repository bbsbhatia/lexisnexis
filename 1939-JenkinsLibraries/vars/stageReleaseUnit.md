stageReleaseUnit(**params)
========================

The standard behavior for stageReleaseUnit() performs the following 3 steps:
1. deletes the `artifacts` folder from the local workspace
2. retrieves a release unit from Artifactory
3. uploads a copy of all files to S3 for deployment.

Locally, files are downloaded into the `artifacts` folder.  

Files retrieved from `source` are stored under the following path within the jenkins workspace: 
>`./artifacts/assetID/assetName/assetAreaName/releaseUnit`

Files staged to `destination` are copied from the release unit folder defined above, to the target, as defined by the destination type.

Usable from both declarative and scripted pipelines.

### REQUEST SYNTAX
```groovy
response = stageReleaseUnit(
    assetAreaName: 'String',
    assetID: 'String',
    assetName: 'String',
    clean: 'Boolean',
    destination: 'Map',
    releaseUnit: 'String',
    source: 'Map'
)
```

### PARAMETERS
##### REQUIRED

> **assetID** (_String_) **[REQUIRED]** -- Defines the Asset ID of the release unit to stage.  If not present, will use the value of the environment variable `ASSET_ID`.

> **assetName** (_String_) **[REQUIRED]** -- Defines the Asset Name of the release unit to stage.  If not present, will use the value of the environment variable `ASSET_NAME`.

> **assetAreaName** (_String_) **[REQUIRED]** -- Defines the Asset Area Name of the release unit to stage.  If not present, will use the value of the environment variable `ASSET_AREA_NAME`.

> **releaseUnit** (_String_) **[REQUIRED]** -- Specifies the release unit to stage.  If not present, will use the value of the environment variable `RELEASE_UNIT`.  

##### OPTIONAL

> **clean** (_Boolean_) -- If value is not true, the artifacts folder will not be purged before the download.  This value is not case sensitive. *Default*: `true`

> **destination** (_Map_) -- This parameter should be a Map of ArrayLists of Maps.  Supported `key` values for this map are [`s3`, `smb`].  The `value` for a given `key` must provide the parameters required for the service `key`.  If destination is `false`, then no destinations are staged.

>> `s3` - The S3 destination map supports the following parameters:  
>> * **bucket** (_String_) -- This is the S3 bucket name that the release unit will be copied to.  
>> * **prefix** (_String_) -- This prefix will be prepended to the release unit before upload.  

>> `smb` - The sbm destination map supports the following parameters:  
>> * **share**  (_String_) -- This is the host and share.  Should be in the format `//servername/sharename`.  The host name should be fully qualified so that DNS can resolve from the jenkins worker node.
>> * **directory**  (_String_) -- This is the folder in which the release unit will be staged.  This directory structure must already exist.

> **source** (_Map_) -- This parameter should be a Map of ArrayLists of Maps.  Supported `key` values for this map are [`artifactory`].  The `value` for a given `key` must provide the parameters required for the service `key`.  If source is `false`, then no release unit is downloaded.

>> `artifactory` - The S3 destination map supports the following parameters:  
>> * **server** (_String_) -- This is the name of the Artifactory server, as configured within Jenkins.  
>> * **pattern** (_String_) -- This is the pattern that will be used within the filespec for downloading from Artifactory.  

### EXAMPLE USAGE

#### Example 1
```groovy
stageReleaseUnit()
```
```groovy
stageReleaseUnit(
    source: [
        artifactory: [
            [
                server: 'Artifactory',
                pattern: 'lng/assetID/assetName/assetAreaName/releaseUnit/*'
            ]
        ]
    ]
    destination: [
        s3: [
            [
                bucket: STAGING_BUCKET,
                prefix: 'assetID/assetName/assetAreaName/releaseUnit'
            ]
        ]
    ]
)
```
When no parameters are passed to the `stageReleaseUnit()` step, the above source and destination values are injected by default.  This enables the standard behavior for this step.

#### Example 2

```groovy
stageReleaseUnit(
    destination: [
        smb: [
            share: '//lngdaydatp001.legal.regn.net/Staging',
            directory: 'NeptuneDeploy/G2/Citrix/All'
        ]
    ]
)
```

This example will download the release unit from Artifactory following the default source spec, as shown in Example 1.  Then, instead of staging the files to S3, it will stage the files to the specified share, in the specified directory.

#### Example 3

```groovy
stageReleaseUnit(
    destination: false
)
```

This example will download the release unit from Artifactory following the default source spec, as shown in Example 1.  Then end without staging it to any external locations.


#### Example 4

```groovy
stageReleaseUnit(
    destination: false
)
dir('artifacts/assetID/assetName/assetAreaName/releaseUnit'){
    sh('mv special_config.conf master.conf')
}
stageReleaseUnit(
    source: false,
    clean: false
)
```

This example will download the release unit from Artifactory following the default source spec, as shown in Example 1.  The user then renames a file within the release unit.  Then stageReleaseUnit() will upload the modified release unit to the default S3 staging location.