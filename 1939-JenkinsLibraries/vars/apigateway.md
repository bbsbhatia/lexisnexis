apigateway(**params)
========================

Usable from both declarative and scripted pipelines.  Uses command parameter to call one of the sub-commands, passing parameters as is.

### REQUEST SYNTAX
```
apigateway(
    command: 'string',
    **params
)
```
### PARAMETERS
> **command** (_string_) -- **[REQUIRED]** Used to control which command is executed.  
>>Valid Values:
>>  - createDeployment
_____

apigateway.createDeployment(**params)
========================================
Usable only from scripted pipeline.  Called by apigateway if command parameter is set to 'createDeployment'

### DEPRECATION WARNING
This function has been deprecated.  Please refactor to use the [boto3()](boto3.md) step.

### REQUEST SYNTAX
```
apigateway.createDeployment(
    account: 'string',
    region: 'string',
    restApiId: 'string',
    stageDescription: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **account** (_string_) **[REQUIRED]** -- Defines which AWS account the command is executed in.  Must be a 12 digit string.  Account must be one serviced by the executing Jenkins server.

> **restApiId** (_string_) **[REQUIRED]** -- The string identifier of the associated RestApi.

##### OPTIONAL

> **region** (_string_) -- Defines which AWS region the command is executed in.  Must be a valid AWS Region. *Default*: 'us-east-1'

> **stageDescription** (_string_) -- The description of the Stage resource for the Deployment resource to create.

_____
