import groovy.time.*
import groovy.json.JsonOutput

// Getting commit ID from build
def _findCommitId(build_job, build_number, repo_url){
// build_job = "Wormhole/62-DataLake/Core/Build/master"
// build_number = 23
// repo_url = "https://tfs-glo-lexisadvance.visualstudio.com/DefaultCollection/Content/_git/62-DataLake"

    def jenkins = Jenkins.getInstance()
    def job = jenkins.getItemByFullName(build_job)
    def bld = job.getBuildByNumber(build_number)
    startTime = bld.getStartTimeInMillis()
    logText = bld.getLog()

    flag = false
    commit_log = ""
    for(line in logText.split('\n')){
        if(flag){
            if(line.contains("git checkout -f")){
                commit_log = line
                break
            }
        }else{  
            if(line.contains("git fetch") && line.contains(repo_url)){
                flag = true
            }
        }
    }
    if (commit_log != ""){
        return commit_log.split(' ')[5]
    }else{
        error "Unable to find checkout information in $build_job #$build_number for $repo_url"
    }
}

def _getCommitTimestamp(commitId){
    script = "git show --no-patch --no-notes --pretty='%cd' --date=iso-strict $commitId"
    git_time = sh(script: script, returnStdout: true).trim()
    return Date.parse("yyy-MM-dd'T'HH:mm:ssX", git_time).format("yyy-MM-dd'T'HH:mm:ssX")
}


def call(Map params = [:]){
    println("The collectMetrics step does not currently do anything.")
}

def reportRelease(Map params = [:]){
    // required params:
    // build_number
    // build_job
    build_job = params.get('assetGroup', JOB_NAME.replace('/Deploy/', '/Build/'))
    response = [:]
    response['result'] = currentBuild.currentResult
    response['deployEndTimestamp'] = new Date().format("yyy-MM-dd'T'HH:mm:ssX")
    response['assetName'] = ASSET_NAME
    response['assetAreaName'] = ASSET_AREA_NAME
    response['assetGroup'] = params.get('assetGroup', ASSET_GROUP)
    response['assetId'] = ASSET_ID
    response['repoUrl'] = scm.getUserRemoteConfigs()[0].getUrl()
    response['commitId'] = _findCommitId(build_job, params['build_number'] as Integer, response['repoUrl'])
    response['commitTimestamp'] = _getCommitTimestamp(response['commitId'])
    // We can't calculate the time delta with any simplicity, so we'll leave that to the consumer.
    
    commons.http(
        url: "https://aoe93b0qvk.execute-api.us-east-1.amazonaws.com/v1/wormhole",
        method: "POST",
        body: JsonOutput.toJson(response),
        "content-type": "application/json"
    )
}
