def call(Map params = [:]){
    String command = params.get('command', null)

    switch (command) {
        case 'createDeployment':
            executeChangeSet(params)
            break
        default:
            echo "You must specify a command, or specified command not yet implimented"
            break
    }
}

def createDeployment(Map params = [:]){
    message = """
    +========================== WARNING ==========================+
    |  apigateway.createDeployment() has been deprecated          |
    |  Please refactor to use the boto3() step.                   |
    +========================== WARNING ==========================+
    """
    println(message)
    params.put('client', 'apigateway')
    params.put('method', 'create_deployment')
    params.put('arguments',['restApiId':params.get('restApiId'),
                            'stageDescription': params.get('stageDescription')])
    return boto3(params)
}