
def call(Map params = [:]){
    def encoded = params.inspect().bytes.encodeBase64().toString()
    def gitBranch = env.BRANCH_NAME
    def gitPrevCommit = env.GIT_PREVIOUS_COMMIT
    def gitCommit = env.GIT_COMMIT
    def gitLastSuccessCommit = env.GIT_PREVIOUS_SUCCESSFUL_COMMIT
    def buildNumber = env.BUILD_ID
    String projectPath = params.get('projectPath', env.PROJECT_PATH ?: '.')
    def imageName = projectPath.toLowerCase().replace('/', '-')
    imageName = imageName == "." ? "image": imageName
    def targetAccount = "807841377931"
    def region = "us-east-1"
    def repoEndpoint = "${targetAccount}.dkr.ecr.${region}.amazonaws.com"
    env.DOCKER_HOME = tool("Docker-18")

    dir(projectPath){
        // ensure artifacts exists
        sh "mkdir artifacts -p"
        def dockerFolder = null
        if (fileExists(['DevOps', 'Docker'].join(File.separator))) {
            dockerFolder = ['DevOps', 'Docker'].join(File.separator)
        }

        if (fileExists('Docker')) {
            dockerFolder = 'Docker'
        }        

        if (dockerFolder != null) {
            echo "Found DevOps/Docker folder - Collecting Artifacts"

            def repoName = "${env.ASSET_ID}/${env.ASSET_AREA_NAME}".toLowerCase()
            def remoteImageName = "${repoEndpoint}/${repoName}"            
            def buildTag = "${env.GIT_COMMIT}.${commons.getBranchName().replace('/', '-')}.build.${env.BUILD_NUMBER}"
            collectDocker(dockerFolder)
            dockerLoginToECR(targetAccount, repoEndpoint)
            createEcrRepo(targetAccount, repoEndpoint, repoName)
            buildDocker(dockerFolder, imageName, buildTag)
            pushToECR(imageName, repoName, remoteImageName, buildTag)
            writeManifest(targetAccount, repoName)
            return "${remoteImageName}:${buildTag}"
        }
    }
}

void collectDocker(dockerFolder){
    sh "cp $dockerFolder ./artifacts/ -R -v"
}

void buildDocker(dockerFolder, imageName, buildTag){
    sh 'echo "WORKSPACE=${WORKSPACE}" > .env'
    def dockerFilePath = [dockerFolder, 'Dockerfile'].join(File.separator)
    if (!fileExists(dockerFilePath)){
        error "$dockerFilePath does not exist"
    }
    docker "system prune -a --force"
    docker "build -f ${dockerFilePath} -t ${imageName}:cert ."
}

def writeManifest(targetAccount, repoName){
    def repoNameAsFile = repoName.replace("/","-")
    def getManifest = "${describeEcrRepo(targetAccount, repoName)}"
    echo "Writing docker manifest to file: artifacts/${repoNameAsFile}.dockerManifest"
    echo getManifest
    writeFile file: "artifacts/${repoNameAsFile}.dockerManifest", text: getManifest    
}

def pushToECR(imageName, repoName, remoteImageName, buildTag){    
    def tags = ["latest", buildTag]
    if(commons.getBranchName() == "master"){tags.add("stable")}
    
    for (tag in tags) {
        docker "tag ${imageName}:cert ${remoteImageName}:${tag}"        
        docker "push ${remoteImageName}:${tag}"
    }
}

def createEcrRepo(targetAccount, repoEndpoint, repoName) {
    def exists = doesRepoExist(repoName, targetAccount)
    if ( exists == false || exists == "false" ) {
        echo "Repo $repoName does not exist. Kicking of build pipeline"

        echo "Checking if $repoName exists"
        def encodedRepoName = repoName.replace("/", "%2f")
        def repoExists = describeRepositories([
                account : targetAccount,
                repoName: encodedRepoName
        ])
        def repoExistsResult = repoExists.get("result", null)
        echo "$repoName exists = $repoExistsResult"
        createRepository([
                account : targetAccount,
                repoName: encodedRepoName
        ])
    } else {
        echo "Repo $repoName already exists, skip making"
    }
}

def doesRepoExist(repoName, targetAccount) {
    echo "Checking if $repoName exists"
    def encodedRepoName = repoName.replace("/", "%2f")
    def repoExists = describeRepositories([
            account : targetAccount,
            repoName: encodedRepoName
    ])
    def repoExistsResult = repoExists.get("result", null)
    echo "$repoName exists = $repoExistsResult"
    return repoExistsResult
}

def describeEcrRepo(targetAccount, repoName){
    def repoNameEscaped = repoName.replace("/","%2f")
    def result = batchGetImage(
            account: targetAccount,
            repoName: repoNameEscaped
    )
    return result
}

def dockerLoginToECR(targetAccount, repoEndpoint) {
    def authString = getAuthString(targetAccount)
    def authStringLoginArray = authString.split(":")
    def user = authStringLoginArray[0]
    def pass = authStringLoginArray[1]
    docker "login -u $user -p $pass https://$repoEndpoint"
}

def getAuthString(targetAccount) {
    def result = getAuthorizationToken([account: targetAccount])
    def authStringBase64 = result.get("result", null)
    def authStringByterray = authStringBase64.decodeBase64()
    def authString = new String(authStringByterray)
    return authString
}

def docker(args) {
    def shellScript = "sudo docker ${args}"
    def exitCode = sh (
        script: shellScript,
        returnStatus: true
    )
    if (exitCode != 0) {
        error("Command \"$shellScript\" failed with exit code $exitCode.")
    }
}


// ECR helpers

def describeRepositories(Map params = [:]){
    echo "calling describeRepositories"
    params.command = "describeRepositories"
    return awsService(params, "ecr", ["describeRepositories"])
}

def batchGetImage(Map params = [:]){
    echo "calling batchGetImage"
    params.command = "batchGetImage"
    return awsService(params, "ecr", ["batchGetImage"])
}

def getAuthorizationToken(Map params = [:]){
    echo "calling getAuthorizationToken"
    params.command = "getAuthorizationToken"
    return awsService(params, "ecr", ["getAuthorizationToken"])
}

def createRepository(Map params = [:]){
    echo "calling createRepository"
    params.command = "createRepository"
    return awsService(params, "ecr", ["createRepository"])
}