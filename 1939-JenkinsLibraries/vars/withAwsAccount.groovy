def call(Map params = [:], Closure body){
    String region = params.get('region', 'us-east-1')
    String account = params.get('account', null)

    def script = libraryResource "aws/sts/assumeRole.py"
    writeFile file: "temp/assumeRole.py", text: script

    switch (account) {
        case [  '069379813652', //product-content-sandbox
            // Sandbox accounts do not require any approval to access
                '549221528994', //operations-bcs-dev
                '284211348336', //product-content-dev
                '774060640982', //product-lexisai-dev
                '288044017584', //product-datalake-dev
                '847642044734', //product-content-devold
                '153576756524']://product-business-dev
            // Dev accounts do not require any approval to access
            break
        default: 
            //By default, access to credentials requires DevOps approval
            input(message: "Access to AWS credentials for this account [$account] requires DevOps approval.", submitter: "Business and Content - Cloud DevOps")
            break
    }
    def shCommand = "python3 -u temp/assumeRole.py -account $account -region $region"
    withEnv(["BUILD_USER_ID=${commons.getBuildUserId()}"]) {
        withEnv(sh(script: shCommand, returnStdout: true).trim().tokenize(',')){
            body()
        }
    }
}

//convenience overload
def call(String account, Closure body){
    return call(account: account){body()}
}