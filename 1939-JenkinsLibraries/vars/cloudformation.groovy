import groovy.json.JsonSlurper
import groovy.json.JsonOutput

def call(Map params = [:]){
    String command = params.get('command', null)

    switch (command) {
        case 'executeChangeSet':
            executeChangeSet(params)
            break
        case 'createChangeSet':
            createChangeSet(params)
            break
        case 'describeStacks':
            describeStacks(params)
            break
        case 'describeStackResource':
            describeStackResource(params)
            break
        case 'createStack':
            createStack(params)
            break
        default:
            echo "You must specify a command, or specified command not yet implimented"
            break
    }
}

def executeChangeSet(Map params = [:]){
    // Process Parameters
    String region = params.get('region', 'us-east-1')
    String account = params.get('account')

    // Inject EnvVars into param map to pass to python
    params['templateName'] = params.get('templateName')
    params['stackName'] = params.get('stackName', generateStackName(params['templateName']))
    params['assetGroup'] = params.get('assetGroup', ASSET_GROUP)
    params['jenkinsJobNumber'] = params.get('jenkinsJobNumber', BUILD_NUMBER)
    params['assetId'] = params.get('assetId', ASSET_ID)
    params['assetName'] = params.get('assetName', ASSET_NAME)
    params['assetAreaName'] = params.get('assetAreaName', ASSET_AREA_NAME)
    params['releaseUnit'] = params.get('releaseUnit', RELEASE_UNIT)

    // Obtain Library Resources
    def script = libraryResource "aws/sts/assumeRole.py"
    writeFile file: "temp/assumeRole.py", text: script

    def script2 = libraryResource "aws/cloudformation/executeChangeSet.py"
    writeFile file: "temp/executeChangeSet.py", text: script2

    // Get AWS Credentials for desired account/region
    def shCommand = "python3 -u temp/assumeRole.py -account $account -region $region"
    wrap([$class: 'BuildUser']) {
        // Inject AWS Tokens into Environment
        withEnv(sh(script: shCommand, returnStdout: true).trim().tokenize(',')){
            params['buildUserId'] = env.BUILD_USER_ID ?: 'unknown'
            // Execute python script
            shCommand2 = "python3 -u temp/executeChangeSet.py '${JsonOutput.toJson(params)}'"
            sh(script: shCommand2, returnStdout: false)
        }
    }

}

def createStack(Map params = [:]){
    createChangeSet(params)
    executeChangeSet(params)
}

def createChangeSet(Map params = [:]){
    // Process Parameters for use in Groovy code
    String region = params.get('region', 'us-east-1')
    String account = params.get('account')

    // Inject EnvVars into param map to pass to python
    params['stagingBucket'] = params.get('stagingBucket', STAGING_BUCKET)
    params['templateName'] = params.get('templateName')
    params['stackName'] = params.get('stackName', generateStackName(params['templateName']))
    params['assetGroup'] = params.get('assetGroup', ASSET_GROUP)
    params['jenkinsJobNumber'] = params.get('jenkinsJobNumber', BUILD_NUMBER)
    params['assetId'] = params.get('assetId', ASSET_ID)
    params['assetName'] = params.get('assetName', ASSET_NAME)
    params['assetAreaName'] = params.get('assetAreaName', ASSET_AREA_NAME)
    params['releaseUnit'] = params.get('releaseUnit', RELEASE_UNIT)
    params['jenkinsJobName'] = JOB_NAME

    // Obtain Library Resources
    def script = libraryResource "aws/sts/assumeRole.py"
    writeFile file: "temp/assumeRole.py", text: script

    def script2 = libraryResource "aws/cloudformation/createChangeSet.py"
    writeFile file: "temp/createChangeSet.py", text: script2

    // Get AWS Credentials for desired account/region
    def shCommand = "python3 -u temp/assumeRole.py -account $account -region $region"
    wrap([$class: 'BuildUser']) {
        // Inject AWS Tokens into Environment
        withEnv(sh(script: shCommand, returnStdout: true).trim().tokenize(',')){
            params['buildUserId'] = env.BUILD_USER_ID ?: 'unknown'
            // Execute python script
            shCommand2 = "python3 -u temp/createChangeSet.py '${JsonOutput.toJson(params)}'"
            sh(script: shCommand2, returnStdout: false)
        }
    }
}

def describeStacks(Map params = [:]){
    message = """
    +========================== WARNING ==========================+
    |  cloudformation.describeStacks() has been deprecated        |
    |  Please refactor to use the boto3() step.                   |
    +========================== WARNING ==========================+
    """
    println(message)
    // Add Client and Method Parameters for backwards compatability
    params.put('client', 'cloudformation')
    params.put('method', 'describe_stacks')
    params.put('arguments', ['StackName': params.get('stackName')])
    return boto3(params)
}

def describeStackResource(Map params = [:]){
    message = """
    +========================== WARNING ==========================+
    |  cloudformation.describeStackResource() has been deprecated |
    |  Please refactor to use the boto3() step.                   |
    +========================== WARNING ==========================+
    """
    println(message)
    // Add Client and Method Parameters for backwards compatability
    params.put('client', 'cloudformation')
    params.put('method', 'describe_stack_resource')
    params.put('arguments', ['StackName': params.get('stackName'),
                             'LogicalResourceId': params.get('logicalResourceId')])
    return boto3(params)
}

def generateStackName(templateName){
    if(templateName == null){
        error "generateStackName requires templateName as a parameter"
    }
    else{
        return [ASSET_GROUP, ASSET_NAME, templateName].join('-').replace("_","-").replace("/", "-")
    }
}