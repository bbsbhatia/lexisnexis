import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput

def call(Map params = [:]){
    // Process Parameters
    String region = params.get('region', 'us-east-1')
    String account = params.get('account')

    // Obtain Library Resources
    def script = libraryResource "aws/sts/assumeRole.py"
    writeFile file: "temp/assumeRole.py", text: script

    def script2 = libraryResource "aws/boto3wrapper.py"
    writeFile file: "temp/boto3wrapper.py", text: script2

    // Get AWS Credentials for desired account/region
    def shCommand = "python3 -u temp/assumeRole.py -account $account -region $region"
    wrap([$class: 'BuildUser']) {
        // Inject AWS Tokens into Environment
        withEnv(sh(script: shCommand, returnStdout: true).trim().tokenize(',')){
            // Execute python script
            shCommand2 = "python3 -u temp/boto3wrapper.py ${commons.shellString(JsonOutput.toJson(params).toString())}"
            results = sh(script: shCommand2, returnStdout: true).trim()
            //println results
            // Process response Json into a json object
            if(params.get('streamingBody')){
                return results
            }else{
                def slurper = new JsonSlurperClassic()
                println JsonOutput.prettyPrint(results)
                // handle null response
                if(results == 'null'){
                    return null
                // handle if response is a valid JSON Object
                }else if((results.startsWith('{') && results.endsWith('}')) || (results.startsWith('[') && results.endsWith(']'))){
                    return slurper.parseText(results)
                // handle if response is a string
                }else if((results.startsWith('"') && results.endsWith('"'))){
                    return results.substring(1,results.length()-1)
                // catchall
                }else{
                    return output
                }
            }
        }
    }
}
