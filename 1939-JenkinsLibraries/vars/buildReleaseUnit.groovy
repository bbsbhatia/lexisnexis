def call(Map params = [:]){
    String projectPath = params.get('projectPath', env.PROJECT_PATH ?: '.')
    String encoded = params.inspect().bytes.encodeBase64().toString()

    dir(projectPath){
        // Delete the artifacts folder and all artifacts from prior builds
        echo "Cleaning up the artifacts folder."
        sh "rm artifacts --force --recursive"
        // Recreate the artifacts folder
        sh "mkdir artifacts -p"

        // Infrastructure
        if(fileExists('Infrastructure')){
            echo "Found Infrastructure folder - Collecting Artifacts"
            collectCloudFormation()
        }
        // Resources
        if(fileExists('Resources')){
            echo "Found Resources folder - Collecting Artifacts"
            collectResources()
        }
        // Packer - Must use stand alone step, not yet integrated
        //if(fileExists(['DevOps','Packer'].join(File.separator))){
        //    echo "Found DevOps/Packer folder"
        //    //buildAmi()
        //}
        // Source
        if(fileExists('Source')){
            // Python Package 
            if(fileExists(['Source','PyPI','setup.py'].join(File.separator))){
                echo "Found Source/PyPI/setup.py - Building Python Package"
                buildPythonPackage() 
            }
            if(fileExists(['Source','Python','setup.py'].join(File.separator))){
                echo "Found Source/Python/setup.py - Building Python Package"
                buildPythonPackage() 
            // Python Zip
            }else if(fileExists(['Source','Python'].join(File.separator))){
                echo "Found Source/Python, Missing setup.py - Building Python Zip"
                buildPythonZip() 
            }
            // Lambda
            if(fileExists(['Source','Lambda'].join(File.separator))){
                echo "Found Source/Lambda folder - Building Python Lambda Zip"
                // TODO - Detect Language other than Python
                buildPythonLambda()
            }
            // VelocityTemplates
            if(fileExists(['Source','VelocityTemplates'].join(File.separator))){
                echo "Found Source/VelocityTemplates folder - Transforming and Collecting"
                runTransforms('escape2fnsub', ['Source','VelocityTemplates'].join(File.separator))
            }
            // DocumentParts
            if(fileExists(['Source','DocumentParts'].join(File.separator))){
                echo "Found Source/DocumentParts folder - Transforming and Collecting"
                runTransforms('escape2fnsub', ['Source','DocumentParts'].join(File.separator))
            }
            // StepFunctions
            if(fileExists(['Source','StepFunctions'].join(File.separator))){
                echo "Found Source/StepFunctions folder - Transforming and Collecting"
                runTransforms('escape2fnsub', ['Source','StepFunctions'].join(File.separator))
            }
            // runCommands
            if(fileExists(['Source','RunCommands'].join(File.separator))){
                echo "Found Source/RunCommands folder - Transforming and Collecting"
                runTransforms('script2runCommand', ['Source','RunCommands'].join(File.separator))
            }
        }
        // Maven
        // if(fileExists('pom.xml')){
        //     echo "Found POM.xml file - Running Maven Build"
        //     buildMaven()
        // }
    }
}

def collectCloudFormation(){
    sh "cp ./Infrastructure/* ./artifacts/ -R -v"
}

def collectResources(){
    sh "cp ./Resources ./artifacts/ -R -v"
}

def buildMaven(){
    sh "mvn clean package -Dmaven.test.skip=true"
    sh "cp ./target/*.jar ./artifacts/ -v"
}

def buildPythonPackage(){
    // Cleanup build and dist folders if they exist, to ensure no cross contamination
    // Create Universal Built Distribution Wheel based on setup.py
    dir('Source/Python'){
        sh "rm dist -r -f; rm build -r -f; python3 setup.py bdist_wheel --universal"
    }
    // Move built artifacts to the artifacts directory
    sh "cp Source/Python/dist/* ./artifacts/ -R -v"
}

def buildPythonZip(){
    echo "Not yet implemented"
}

def buildPythonLambda(){
    script = libraryResource "python/buildLambda.py"
    writeFile file: "temp/buildLambda.py", text: script
    
    shCommand =  """virtualenv -p python3.6 --system-site-packages venv --clear
                    . venv/bin/activate
                    
                    python3 ./temp/buildLambda.py ${env.ASSET_AREA_NAME.replace("/","-")}

                    deactivate
                 """
    sh shCommand
}

def runTransforms(String type, String path){
    if (type in ['escape2fnsub','script2runCommand']) {
        // Obtain and execute python script
        script = libraryResource "transforms/${type}.py"
        writeFile file: "temp/${type}.py", text: script
        shCommand = "python3 ./temp/${type}.py $path"
        sh shCommand
    }else{
        echo "Requested type is not yet implemented."
    }
}