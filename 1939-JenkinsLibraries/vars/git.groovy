//This is an overload to the normal git step in order to inject the VSTS credentials automatically

def call(Map params = [:]) {
    String branch = params.get('branch', null)
    String url = params.get('url', null)

    return steps.git(   branch: branch, 
                        credentialsId: 'f9d5051e-bc30-4a4b-aab6-e5cc3693d9cf',
                        url: url)
}
/* Convenience overload */
def call(String branch, String url) {
    return call(branch: branch, url: url)
}