import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput

def call(){
    error "The commons step cannot be called from declarative pipeline."
}

def getBranchName(String bn){
    getBranchName(branchName: bn)
}

def getBranchName(Map params = [:]){
    if(params){
        branch = params.get('branchName')
    }else{
        branch = null
    }
    if(!branch){
        branch = scm.branches[0].name
    }
    
    if (branch.contains("/pull/")){
        // If this is a pull request merge branch from VSTS, we want to log everything as the parent branch
        // Query the git log to find the parent commit, and pull it's name.
        // If multiple branches share this revision id, this could return the wrong branch name
        // As best effort, we are going to return the LAST branch name
        git_log = sh(script: "git log --max-count=2 --decorate=short | grep commit", returnStdout: true).split('\n')[1]

        // response will be of the following format:
        // commit 98a406b801fc0cb28cdeb6730d8e1da021be0c78 (HEAD, origin/master, origin/integration/create_subscription_command, origin/feature/pipelineUpdate)
        // [49..-2] grabs the substring that is inside of the ()
        // then we split it on ", "
        // finally return item -1
        branch = git_log[49..-2].split(", ")[-1]
    }

    branch = branch.replace("origin/","")
    branch = branch.replace("refs/","")
    branch = branch.replace("heads/","")
    return branch
}

@NonCPS
def getBuildUserId() {
    //https://javadoc.jenkins-ci.org/hudson/model/Run.html
    try{
        return currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
    }catch(e){
        return 'unknown'
    }
}

def getCommitId(){
    try{
        git_commit = sh(script: "git rev-parse HEAD", returnStdout: true).trim()
    }
    catch(e){
        git_commit = '0000000000000000000000000000000000000000'
    }

    return git_commit
}

def getRepoName(String repoUrl){
    getRepoName(repoUrl: "$repoUrl")
}

def getRepoName(Map params = [:]){
    if(params){
        repo_url = params.get('repoUrl', null)
    }else{
        repo_url = null
    }
    if(!repo_url){
        repo_url = scm.getUserRemoteConfigs()[0].getUrl()
    } 

    repo_name = repo_url.tokenize('/').last().split("\\.")[0]
    return repo_name
}

def httpGet(String url){
    Map response = [:]

    URL url_object = new URL(url)
    def connection = url_object.openConnection()
    connection.setRequestMethod("GET")

    response.put('status', connection.getResponseCode())
    if (response['status'] == 200){
        response.put('body', connection.inputStream.getText())
        response.put('json', slurpJson(response.body))
        response.put('message', connection.getResponseMessage())
    }
    return response
}


def http(Map params = [:]){
    // Valid params
    // url
    // method
    // body
    // content-type

    Map response = [:]

    URL url_object = new URL(params['url'])
    def connection = url_object.openConnection()
    connection.setRequestMethod(params['method'].toUpperCase())

    if(params['method'].toUpperCase() == "POST") {
        connection.setRequestProperty('Content-Type', params.get('content-type', 'text/plain'))
        connection.setDoOutput(true)
        connection.outputStream.write(params['body'].getBytes('UTF-8'))
    }

    connection.connect()
    response.put('status', connection.responseCode)
    response.put('message', connection.getResponseMessage())
    if (connection.inputStream != null){
        response.put('body', connection.inputStream.getText())
        response.put('json', slurpJson(response.body))
    }
    if (connection.errorStream != null){
        response.put('error', connection.errorStream.getText())
    }
}


def httpGet(String url, String auth){
    Map response = [:]
    String basicAuth = "Basic " + auth.getBytes().encodeBase64().toString()

    URL url_object = new URL(url)
    def connection = url_object.openConnection()

    connection.setRequestProperty ("Authorization", basicAuth);
    connection.setRequestMethod("GET")

    response.put('status', connection.getResponseCode())
    if (response['status'] == 200){
        response.put('body', connection.inputStream.getText())
        response.put('json', slurpJson(response.body))
        response.put('message', connection.getResponseMessage())
    }
    return response
}


HashMap mergeMap(HashMap source, HashMap destination){
    return destination.plus(source)
}


HashMap slurpJson(String input){
    def slurper = new JsonSlurperClassic()
    return slurper.parseText(input)
}


def printMap(Map map){
    echo JsonOutput.prettyPrint(JsonOutput.toJson(map))
}


def printParameterDeprecationMessage(String parameterName){
    println("""
    +========================== WARNING ==========================+
       The parameter [${parameterName}] has been deprecated.
       Please refer to the documentation, and refactor your code.
    +========================== WARNING ==========================+
    """)
}


// Given arbitrary string returns a strongly escaped shell string literal.
// I.e. it will be in single quotes which turns off interpolation of $(...), etc.
// E.g.: 1'2\3\'4 5"6 (groovy string) -> '1'\''2\3\'\''4 5"6' (groovy string which can be safely pasted into shell command).
def shellString(s) {
    // Replace ' with '\'' (https://unix.stackexchange.com/a/187654/260156). Then enclose with '...'.
    // 1) Why not replace \ with \\? Because '...' does not treat backslashes in a special way.
    // 2) And why not use ANSI-C quoting? I.e. we could replace ' with \'
    // and enclose using $'...' (https://stackoverflow.com/a/8254156/4839573).
    // Because ANSI-C quoting is not yet supported by Dash (default shell in Ubuntu & Debian) (https://unix.stackexchange.com/a/371873).
    '\'' + s.replace('\'', '\'\\\'\'') + '\''
}