cloudformation(**params)
========================

Usable from both declarative and scripted pipelines.  Uses command parameter to call one of the sub-commands, passing parameters as is.

### REQUEST SYNTAX
```
cloudformation(
    command: 'string',
    **params
)
```
### PARAMETERS
> **command** (_string_) -- **[REQUIRED]** Used to control which command is executed.  
>>Valid Values:
>>  - createChangeSet
>>  - createStack
>>  - describeStackResources
>>  - describeStacks
>>  - executeChangeSet
_____

cloudformation.createChangeSet(**params)
========================================
Usable only from scripted pipeline.  Called by cloud formation if command parameter is set to 'createChangeSet'

### REQUEST SYNTAX
```
cloudformation.createChangeSet(
    account: 'string',
    capabilities: [
        'CAPABILITY_IAM'|'CAPABILITY_NAMED_IAM',
    ],
    notificationARNs: ['string'],
    region: 'string',
    stagingBucket: 'string',
    stackName: 'string',
    stackParameters: [
        'string': 'string'
    ]
    templateName: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **account** (_string_) **[REQUIRED]** -- Defines which AWS account the command is executed in.  Must be a 12 digit string.  Account must be one serviced by the executing Jenkins server.

> **templateName** (_string_) **[REQUIRED]** -- This value should be equal to the file name of the cloud formation template, _not_ including the file extension.  
>> Example: If your template is _jenkins.template_, your parameters file should be named _jenkins.parameters_, and the value of **templateName** should be _jenkins_.

##### OPTIONAL

> **capabilities** (_list_) -- If you have IAM resources with custom names, you must specify CAPABILITY_NAMED_IAM. 
For more information, see [Acknowledging IAM Resources in AWS CloudFormation Templates](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-iam-template.html#capabilities).  *Default*: ['CAPABILITY_IAM']

> **notificationARNs** (_list_) -- The Amazon Resource Names (ARNs) of Amazon Simple Notification Service (Amazon SNS) topics that AWS CloudFormation associates with the stack. To remove all associated notification topics, specify an empty list.

> **region** (_string_) -- Defines which AWS region the command is executed in.  Must be a valid AWS Region. *Default*: 'us-east-1'

> **stagingBucket** (_string_) -- Defines the staging bucket used to source template and parameter files.  Defaults to the staging bucket dedicated to the executing Jenkins instance.  Defined in global variable STAGING_BUCKET.  The bucket is created as part of the Jenkins stack Asset Group.

> **stackName** (_string_) -- The name or the unique ID of the stack for which you are creating a change set. If left undefined, Jenkins will generate a stack name using the following logic: [ASSET_GROUP, ASSET_NAME, templateName].join('-').replace("_","-").replace("/", "-")

> **stackParameters** (_list_) -- A list of Key:Value pairs that are passed as stack parameters.  Any Key:Value pairs passed in here will supersede any other parameter sources.
>> Stack Parameters can be defined in three different locations, with the later always superseding the former.
>> 1. Default values from the stack template. Only used for new stacks.  Stack updates will _not_ pick up changes to defaults.
>> 2. Parameters provided as part of the Release Unit in a templateName.parameters file.  This is the preferred method of changing non-assetgroup related parameters.
>> 3. stackParameters passed in through the pipeline.  This is the only method for adjusting AssetGroup, or runtime level parameters.
_____

cloudformation.createStack(**params)
========================================
Usable only from scripted pipeline.  Called by cloud formation if command parameter is set to 'createStack'.  Performs a createChangeSet, then executes it

### REQUEST SYNTAX
```
cloudformation.createStack(
    account: 'string',
    capabilities: [
        'CAPABILITY_IAM'|'CAPABILITY_NAMED_IAM',
    ],
    deleteOnCreateFail: 'boolean',
    notificationARNs: ['string'],
    region: 'string',
    stagingBucket: 'string',
    stackName: 'string',
    stackParameters: [
        'string': 'string'
    ]
    templateName: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **account** (_string_) **[REQUIRED]** -- Defines which AWS account the command is executed in.  Must be a 12 digit string.  Account must be one serviced by the executing Jenkins server.

> **templateName** (_string_) **[REQUIRED]** -- This value should be equal to the file name of the cloud formation template, _not_ including the file extension.  
>> Example: If your template is _jenkins.template_, your parameters file should be named _jenkins.parameters_, and the value of **templateName** should be _jenkins_.

##### OPTIONAL

> **capabilities** (_list_) -- If you have IAM resources with custom names, you must specify CAPABILITY_NAMED_IAM. 
For more information, see [Acknowledging IAM Resources in AWS CloudFormation Templates](http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/using-iam-template.html#capabilities).  *Default*: ['CAPABILITY_IAM']

> **deleteOnCreateFail** (_boolean_) -- If the change set being executed is creating a new stack, and the stack fails, this function will wait for the stack rollback to complete, then delete the stack.  The failed stack can still be found in the AWS Console by changing the filter to "Deleted" *Default*: 'True'

> **notificationARNs** (_list_) -- The Amazon Resource Names (ARNs) of Amazon Simple Notification Service (Amazon SNS) topics that AWS CloudFormation associates with the stack. To remove all associated notification topics, specify an empty list.

> **region** (_string_) -- Defines which AWS region the command is executed in.  Must be a valid AWS Region. *Default*: 'us-east-1'

> **stagingBucket** (_string_) -- Defines the staging bucket used to source template and parameter files.  Defaults to the staging bucket dedicated to the executing Jenkins instance.  Defined in global variable STAGING_BUCKET.  The bucket is created as part of the Jenkins stack Asset Group.

> **stackName** (_string_) -- The name or the unique ID of the stack for which you are creating a change set. If left undefined, Jenkins will generate a stack name using the following logic: [ASSET_GROUP, ASSET_NAME, templateName].join('-').replace("_","-").replace("/", "-")

> **stackParameters** (_list_) -- A list of Key:Value pairs that are passed as stack parameters.  Any Key:Value pairs passed in here will supersede any other parameter sources.
>> Stack Parameters can be defined in three different locations, with the later always superseding the former.
>> 1. Default values from the stack template. Only used for new stacks.  Stack updates will _not_ pick up changes to defaults.
>> 2. Parameters provided as part of the Release Unit in a templateName.parameters file.  This is the preferred method of changing non-assetgroup related parameters.  Please see [BCS CICD Documentation](https://tfs-glo-lexisadvance.visualstudio.com/Content/Content%20Team/_git/BCS-CloudDevOps-Documentation?path=%2Fother%2Fcf-param-file.md&version=GBmaster&fullScreen=true&createIfNew=true) for details
>> 3. stackParameters passed in through the pipeline.  This is the only method for adjusting AssetGroup, or runtime level parameters.
_____

cloudformation.describeStackResource(**params)
=========================================
Usable only from scripted pipeline.  Called by cloudformation if command parameter is set to 'describeStackResource'.  Executes the AWS describe-stack-resource command.

### DEPRECATION WARNING
This function has been deprecated.  Please refactor to use the [boto3()](boto3.md) step.

### REQUEST SYNTAX
```
cloudformation.describeStackResource(
    account: 'string',
    region: 'string',
    stackName: 'string',
    logicalResourceId: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **account** (_string_) **[REQUIRED]** -- Defines which AWS account the command is executed in.  Must be a 12 digit string.  Account must be one serviced by the executing Jenkins server.

> **stackName** (_string_) **[REQUIRED]** -- The name or the unique stack ID that is associated with the stack, which are not always interchangeable.

> **logicalResourceId** (_string_) **[REQUIRED]** -- The logical name of the resource as specified in the template. 

##### OPTIONAL

> **query** (_string_) -- If present, will be used as the [jmespath](http://jmespath.org/) expression used to search the response data.  The result of the jmespath.search() command will be returned. 

> **region** (_string_) -- Defines which AWS region the command is executed in.  Must be a valid AWS Region. *Default*: 'us-east-1'

_____

cloudformation.describeStacks(**params)
=========================================
Usable only from scripted pipeline.  Called by cloudformation if command parameter is set to 'describeStacks'.  Executes the AWS describe-stacks command.

### DEPRECATION WARNING
This function has been deprecated.  Please refactor to use the [boto3()](boto3.md) step.

### REQUEST SYNTAX
```
cloudformation.describeStacks(
    account: 'string',
    region: 'string',
    stackName: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **account** (_string_) **[REQUIRED]** -- Defines which AWS account the command is executed in.  Must be a 12 digit string.  Account must be one serviced by the executing Jenkins server.

> **stackName** (_string_) **[REQUIRED]** -- The name or the unique ID of the stack for which you are querying 

##### OPTIONAL

> **query** (_string_) -- If present, will be used as the [jmespath](http://jmespath.org/) expression used to search the response data.  The result of the jmespath.search() command will be returned. 

> **region** (_string_) -- Defines which AWS region the command is executed in.  Must be a valid AWS Region. *Default*: 'us-east-1'

_____

cloudformation.executeChangeSet(**params)
=========================================
Usable only from scripted pipeline.  Called by cloud formation if command     parameter is set to 'executeChangeSet'.  Executes the most recent change set for the stack.

### REQUEST SYNTAX
```
cloudformation.executeChangeSet(
    account: 'string',
    templateName: 'string',
    deleteOnCreateFail: 'boolean',
    region: 'string',
    stackName: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **account** (_string_) **[REQUIRED]** -- Defines which AWS account the command is executed in.  Must be a 12 digit string.  Account must be one serviced by the executing Jenkins server.

> **templateName** (_string_) **[REQUIRED]** -- This value should be equal to the file name of the cloud formation template, _not_ including the file extension.  
>> Example: If your template is _jenkins.template_, your parameters file should be named _jenkins.parameters_, and the value of **templateName** should be _jenkins_.

##### OPTIONAL

> **deleteOnCreateFail** (_boolean_) -- If the change set being executed is creating a new stack, and the stack fails, this function will wait for the stack rollback to complete, then delete the stack.  The failed stack can still be found in the AWS Console by changing the filter to "Deleted" *Default*: 'True'

> **region** (_string_) -- Defines which AWS region the command is executed in.  Must be a valid AWS Region. *Default*: 'us-east-1'

> **stackName** (_string_) -- The name or the unique ID of the stack for which you are executing a change set. If left undefined, Jenkins will generate a stack name using the following logic: [ASSET_GROUP, ASSET_NAME, templateName].join('-').replace("_","-").replace("/", "-")