// define parameter object at a higher scope so that it's availble within all methods.
def params = [:]

def call(Map parameters = [:]) {
    params = validateParameters(parameters)
    
    if (params['clean']){
        cleanArtifacts()
    }

    if (params['source'] != false){
        get(params['source'])
    }
    
    if (params['destination'] != false){
        put(params['destination'])
    }
}


def cleanArtifacts(){
    println("Cleaning the artifacts folder.")
    sh "rm -rf artifacts/*"
}


def fromArtifactory(ArrayList sources){
    for (source in sources){
        fromArtifactory(source)
    }
}


def fromArtifactory(Map source = [:]) {
    def fileSpec = """{
        "files": [
            {
                "pattern": "${source['pattern']}",
                "target": "artifacts/"
            }
        ]
    }"""
    def stagingInfo = rtDownload(
        serverId: source['server'],
        spec: fileSpec,
        failNoOp: true
    )
    return stagingInfo
}


def get(Map source_spec = [:]) {
    source_spec.each{
        key, value ->
        switch (key.toLowerCase()) {
            case "artifactory":
                fromArtifactory(value)
                break
            default:
                error "Error in stageReleaseUnit.get() - Unsupported source type provided in source spec: ${key}"
        }
    }
    }


def put(Map dest_spec = [:]) {
    dest_spec.each{
        key, value ->
        switch (key.toLowerCase()){
            case 's3':
                toS3(value)
                break
            case 'smb':
                toSmb(value)
                break
            default:
                error "Error in stageReleaseUnit.put() - Unsupported destination type provided in dest spec: ${key}"
        }
    }
}


def toS3(ArrayList destinations){
    for (destination in destinations){
        toS3(destination)
    }
}


def toS3(Map dest = [:]) {
    /*
    dest = [
        'bucket': 'string',
        'prefix': 'string'
    ]
    */
    writeFile file: "temp/stageReleaseUnit.py", text: libraryResource("devops/stageReleaseUnit.py")
    
    def shCommand = "python3 -u ./temp/stageReleaseUnit.py ${dest['bucket']} ${dest['prefix']}"
    withEnv(["BUILD_USER_ID=${commons.getBuildUserId()}"]) {
        sh(script: shCommand)
    }
}


def toSmb(ArrayList destinations){
    for (destination in destinations){
        toSmb(destination)
    }
}


def toSmb(Map dest = [:]) {
    /*
    dest = [
        'share': 'string',
        'directory': 'string'
    ]
    */

    dest['share']

    // Step into the release unit folder
    dir("artifacts/${params['assetID']}/${params['assetName']}/${params['assetAreaName']}/${params['releaseUnit']}"){
        withCredentials([usernamePassword(credentialsId: 'svc-awscloud002', passwordVariable: 'pw', usernameVariable: 'un')]) {
            // Use SMBClient to connect to dest['share'], starting in the directory dest['directory'].
            // Connect using credentials, and copy all files from the current directory (./artifacts) to dest['directory']
            def shCommand = "smbclient ${dest['share']} -U ${un}%${pw} --directory ${dest['directory']} -c 'recurse; prompt; mput *'"
            sh(script: shCommand)
        }
    }
    }


def validateParameters(Map params = [:]) {
    // Globally Used Parameters
    params['assetID'] = params.get('assetID', ASSET_ID)
    params['assetName'] = params.get('assetName', ASSET_NAME)
    params['assetAreaName'] = params.get('assetAreaName', ASSET_AREA_NAME)
    params['releaseUnit'] = params.get('releaseUnit', env.RELEASE_UNIT ?: 'unknown')

    params['clean'] = params.get('clean', 'TRUE').toBoolean()

    // Handle Deprecated Parameters
    if (! params.get('stagingBucket')){
        params['stagingBucket'] = STAGING_BUCKET
    }else{
        commons.printParameterDeprecationMessage("stagingBucket")
    }
    if (! params.get('upload')){
        params['upload'] = "TRUE"
    }else{
        params['upload'] = params['upload'].toUpperCase()
        commons.printParameterDeprecationMessage("upload")
    }
    if (! params.get('toS3')){
        params['toS3'] = params['upload']
    }else{
        params['toS3'] = params['toS3'].toUpperCase()
        commons.printParameterDeprecationMessage("toS3")
    }
    
    if (params['releaseUnit'] == "unknown"){
        error "No releaseUnit was supplied to stage."
    }
    // If the last component of the release unit is not an integer;
    //    it is not valid, and we should error
    if (!params['releaseUnit'].split('/')[-1].isInteger()){
        error "releaseUnit is missing a build number"
    }

    // Destination spec
    // If it wasn't specified, build the default_destination_spec, factoring in the above deprecated parameters 
    if(!params.containsKey('destination')){
        default_destination_spec = [:]
        default_s3_destination = [
            [
                bucket: params['stagingBucket'],
                prefix: "${params['assetID']}/${params['assetName']}/${params['assetAreaName']}/${params['releaseUnit']}"
            ]
        ]

        if (params['toS3'] == "TRUE"){
            default_destination_spec.put('s3', default_s3_destination)
        }

        // New destination spec parameter
        params.put('destination', default_destination_spec)
    }

    /*  Example spec 
    destination = [ 
        "s3": [
            [
                "bucket": "value",
                "prefix": "value"
            ],
            [
                "bucket": "value2",
                "prefix": "value2"
            ]
        ],
        "Smb": [
            [
                "share": "//server/share",
                "directory": "/path/to/dir"
            ],
            [
                "share": "//server/share",
                "directory": "/path/to/dir"
            ]
        ]
    ]
    */

    // Source spec
    // If it wasn't specified, build the default_source_spec
    if(!params.containsKey('source')){
        default_source_spec = [
            "artifactory": [
                [
                    server: "Artifactory",
                    pattern: "lng/${params['assetID']}/${params['assetName']}/${params['assetAreaName']}/${params['releaseUnit']}/*"
                ]
            ]
        ]
        // New source spec parameter
        params.put('source', default_source_spec)
    }

    /*  Example spec 
    source = [ 
        "artifactory": [
            [
                "server": "value",
                "pattern": "value"
            ],
            [
                "server": "value2",
                "pattern": "value2"
            ]
        ]
    ]
    */
    return params
}