buildAmi(**params)
========================

buildAmi() is built around the use of [Packer](https://packer.io).  Users should provide a partial [packer template](https://packer.io/docs/templates/index.html).  Users are only required to provide the `provisioners` section of the template, but may optionally provide any other section as required.  If `builders` is present, only the following keys will be honored: `ami_block_device_mappings`, `launch_block_device_mappings`, `ami_description`, `instance_type`.  The template stub provided will be merged with other template stubs located in [287-Bakery](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/287-Bakery) to generate a complete packer template which will be used to build the AMI.  The merged template will be included in the Jenkins artifacts.

Usable from both declarative and scripted pipelines.

### REQUEST SYNTAX
```
response = buildAmi(
    amiName: 'string',
    autoShare: 'boolean',
    bakeryBranch: 'string',
    bakeryRepoUrl: 'string',
    base: 'string',
    debug: 'boolean',
    projectPath: 'string',
    templateName: 'string',
    sourceAmi: 'string'
)
```

### PARAMETERS
##### REQUIRED

> **templateName** (_string_) **[REQUIRED]** -- The name of a packer template stub.  File extension must be .packer and should should not be included.  File must be located within the Packer Folder, as defined in [Project Structure](https://tfs-glo-lexisadvance.visualstudio.com/Content/Content%20Team/_git/BCS-CloudDevOps-Documentation?path=%2Fusage%2Fproject.md&version=GBmaster&fullScreen=true&createIfNew=true&anchor=structure).


##### OPTIONAL

> **amiName** (_string_) -- If you wish to use a custom AMI name, enter it here.  Otherwise the AMI will generated based on the following logic: JOB_NAME.replace("/","-").replace("%2F", "-") + "-" + BUILD_NUMBER + "-" + templateName

> **autoShare** (_string_) -- If set to false, the built AMI is not shared out to the BCS Family of Accounts.  buildAmi.shareAmi() must be called independently before it will be available in other accounts.  Primarily used for testing BCS Base AMIs before publishing.  *Default*: 'true'

> **base** (_string_) -- Defines which OS build process to follow.  
>> Valid values:
>>    - *amazon-linux (Default)*
>>    - amazon-linux-2
>>    - emr
>>    - windows-2012r2
>>    - windows-2016

> **bakeryBranch** (_string_) -- Defines which branch of [287-Bakery](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/287-Bakery) will be used to build the AMI.  Used for testing changes to the Bakery before pressing into production.  A non-default value requires DevOps approval.  *Default*: 'v1'

> **bakeryRepoUrl** (_string_) -- Defines repo will be used to build the AMI.  Used for testing changes to the Bakery before pressing into production.  A non-default value requires DevOps approval.  *Default*: 'https://tfs-glo-lexisadvance.visualstudio.com/DefaultCollection/Content/_git/287-Bakery'

> **debug** (_boolean_) -- If `true` will pass the `-debug` flag into Packer for more verbose output.  *Default*: 'false'

> **projectPath** (_string_) -- As defined in the [Glossary](https://tfs-glo-lexisadvance.visualstudio.com/Content/Content%20Team/_git/BCS-CloudDevOps-Documentation?path=%2Fother%2Fglossary.md&version=GBmaster&fullScreen=true&createIfNew=true).  *Default*: '.'

> **sourceAmi** (_string_) -- The AMI to use as the base upon which the Packer provisioners will execute.  If not provided, the latest stable release of the BCS Base AMIs will be used.  *Default*: `null`

### RESPONSE
> The function will return a `string` containing the AMI ID of the resultant AMI.

_____

buildAmi.calculateAmiAge(Map imageData))
=========================================
Usable only from scripted pipeline.  Extracts the CreationDate from the provided imageData Map.  Returns an int with a value equal to number of days betweeen`now` and the `creationDate`.

### REQUEST SYNTAX
```
buildAmi.calculateAmiAge(
    imageData: 'Map'
)
```
### PARAMETERS

##### REQUIRED

> **imageData** (_Map_) **[REQUIRED]** -- Any map with a root key of CreationDate will succeed.  Designed to consume the response from buildAmi.getImageData().

### RESPONSE
> The function will return an `int` containing the number of days elapsed from CreationDate.

_____

buildAmi.generateAmiName(String templateName)
=========================================
Usable only from scripted pipeline.  Combines the Jenkins Job name, Build Number, and templateName to construct a standard AMI Name.

### REQUEST SYNTAX
```
buildAmi.generateAmiName(
    templateName: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **templateName** (_string_) **[REQUIRED]** -- Name of the template used to generate the AMI.

### RESPONSE
> The function will return a `string` containing the generated AMI name.

_____

buildAmi.getAmiIdFromManifest(String filePath)
=========================================
Usable only from scripted pipeline.  Returns the AMI ID from a packer manifest file located at `filePath`.  Primarily used for obtaining an AMI ID from a Release Unit.

### REQUEST SYNTAX
```
buildAmi.getAmiIdFromManifest(
    filePath: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **filePath** (_string_) **[REQUIRED]** -- Path to a Jenkins Manifest file.  This file is generated as part of the output of the Packer AMI generation process that is executed by the buildAmi() step.

### RESPONSE
> The function will return a `string` containing the AMI ID.

_____

buildAmi.getBaseAmiId(String os)
=========================================
Usable only from scripted pipeline.  Returns the AMI ID of the current BCS Base AMI for the provided OS family.  Primarily used for using Base AMIs in Cloud Formation Templates.

### REQUEST SYNTAX
```
buildAmi.getBaseAmiId(
    os: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **os** (_string_) **[REQUIRED]** -- Base OS name.  Valid values are: ['amazon-linux', 'amazon-linux-2', 'emr', 'windows-2012r2', 'windows-2016']

### RESPONSE
> The function will return a `string` containing the AMI ID.

_____

buildAmi.getImageData(String amiId)
=========================================
Usable only from scripted pipeline.  Obtains a subset of data regarding the input amiId.  Only returns the data used by the buildAmi step and related methods.

### REQUEST SYNTAX
```
buildAmi.getImageData(
    amiId: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **amiId** (_string_) **[REQUIRED]** -- The AMI ID to return data for.

### RESPONSE

Map response = {
    ImageLocation: 'string',
    ImageId: 'string',
    OwnerId: 'string',
    CreationDate: 'string',
    Ancestry: 'string'
}

_____

buildAmi.shareAmi(String amiId)
=========================================
Usable only from scripted pipeline.  Grants the BCS Family of accounts permission to the input amiId.  Only required if the buildAmi() parameter `autoShare` is set to `false`.  Only AMI IDs owned by operations-content-prod are valid inputs.

### REQUEST SYNTAX
```
buildAmi.getImageData(
    amiId: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **amiId** (_string_) **[REQUIRED]** -- The AMI ID to share.

_____

buildAmi.shareAmi(String amiId, String accountId)
=========================================
Usable only from scripted pipeline.  Grants the provided accountId permissions to the input amiId.  Only required if the buildAmi() parameter `autoShare` is set to `false`.  Only AMI IDs owned by operations-content-prod are valid inputs.

### REQUEST SYNTAX
```
buildAmi.getImageData(
    amiId: 'string'
)
```
### PARAMETERS

##### REQUIRED

> **amiId** (_string_) **[REQUIRED]** -- The AMI ID to share.
> **accountId** (_string_) **[REQUIRED]** -- ID of the AWS Account to share to.

_____

