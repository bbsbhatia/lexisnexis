boto3(**params)
========================

boto3() is a wrapper for the [Python Boto3 API](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html).  By specifying the client type, method, and arguments you can make boto3 calls into the specified account using the Jenkins Execution Role.

The function should support all client methods, however the request is checked against a whitelist of approved clients and methods before executing.  If you require a client or method that is not currently available, please reach out to [BCS Cloud DevOps](mailto:BCS-CloudDevOps@reedelsevier.com) for approval.

Usable from both declarative and scripted pipelines.  Uses command parameter to call one of the sub-commands, passing parameters as is.

### REQUEST SYNTAX
```
response = boto3(
    account: 'string',
    arguments: 'map',
    client: 'string',
    method: 'string',
    query: 'string',
    region: 'string',
    streamingBody: 'string'
)
```

### PARAMETERS
##### REQUIRED

> **account** (_string_) **[REQUIRED]** -- Defines which AWS account the command is executed in.  Must be a 12 digit string.  Account must be one serviced by the executing Jenkins server.

> **client** (_string_) **[REQUIRED]** -- The name of a service client, e.g. 's3' or 'ec2'.  This usually matches the list of [Available Services](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/index.html).

> **method** (_string_) **[REQUIRED]** -- The name of an available method for the specified service client.  Method names can be found in the [Boto3 Documentation](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/index.html) by selecting your desired service's "Client" page.

##### OPTIONAL

> **arguments** (_map_) -- This should be a [Groovy Map](http://groovy-lang.org/groovy-dev-kit.html#Collections-Maps) of the _kwargs_ required by the specified boto3 method. 

> **query** (_string_) -- If present, will be used as the [jmespath](http://jmespath.org/) expression used to search the response data.  The result of the jmespath.search() command will be returned.  streamingBody overrides this option.

> **region** (_string_) -- Defines which AWS region the command is executed in.  Must be a valid AWS Region. *Default*: 'us-east-1'

> **streamingBody** (_string_) -- If present, the StreamingBody at this JSON location will be read and it's contents will be the returned as a string.  The value must be a valid [jmespath](http://jmespath.org/) expression, and must resolve to a single StreamingBody.  If this parameter is provided, it supercedes the query parameter.

### RESPONSE
> The function will return either a string or [Groovy Map](http://groovy-lang.org/groovy-dev-kit.html#Collections-Maps) depending on which **method** was called, as well as the value of the **query** parameter.  Boto3 calls always return a [Python Dictionary](https://www.w3schools.com/python/python_dictionaries.asp).  This dictionary is then converted to json, against which the **query** is executed using [jmespath](http://jmespath.org/).  The resulting json is converted into a groovy object by the [JsonSlurper](http://docs.groovy-lang.org/latest/html/gapi/groovy/json/JsonSlurper.html) library.

### EXAMPLE USAGE
[Jenkins Example](http://jenkins.content.aws.lexis.com/job/Examples/job/Boto3/)

#### Example 1 - Wait for instance to be `Running`
```
script{
    timeout(time: 300){
        waitUntil {
            state = boto3(  account:    "807841377931",
                        client:     "ec2",
                        method:     "describe_instances",
                        arguments:  ["InstanceIds":["i-086437dd927ddff87"]],
                        query:      "Reservations[0].Instances[0].State.Name")
            return (state == "Running")
        }
    }
}
```

#### Example 2 - Look up Status for a specific CloudFront Distribution
```
boto3(  account:    "288044017584",
        client:     "cloudfront",
        method:     "get_distribution",
        arguments:  ["Id":"E85KVYSBVEFZJ"],
        query:      "Distribution.Status")
```

#### Example 3 - List all S3 Buckets in account
```
boto3(  account:    "288044017584",
        client:     "s3",
        method:     "list_buckets",
        query:      "Buckets[].Name")
```

#### Example 4 - Look up a specific CloudFormation Resource property
```
boto3(  account:    "873434867576",
        client:     "cloudformation",
        method:     "describe_stack_resource",
        arguments:  ["StackName":"staging-DataLake-ObjectApi",
                     "LogicalResourceId":"DataLakeObjectApi"],
        query:      "StackResourceDetail.PhysicalResourceId")
```

#### Example 5 - Retrieve a StreamingBody and write to file
```
response = boto3(
        account: params.targetAccount,
        arguments: [
            restApiId: api_id, 
            stageName: "v1", 
            exportType: "swagger"
        ],
        client: 'apigateway',
        method: 'get_export',
        streamingBody: 'body'
)
writeFile(file: "SwaggerDocumentation.json", text: response)
```

### APPROVED METHODS
```
whitelist = {
    'athena': [
        'start_query_execution'
    ],
    'apigateway': [
        'create_deployment',
        'get_*'
    ],
    'autoscaling': [
        'describe_*'
    ],
    'cloudformation': [
        'describe_*',
        'get_*',
        'list_*',
        'validate_template',
        'set_stack_policy'
    ],
    'cloudfront': [
        'get_*',
        'list_*',
        'create_invalidation'
    ],
    'dynamodb': [
        'create_backup',
        'list_global_tables',
        'create_global_table'
    ],
    'ec2': [
        'describe_*',
        'get_*',
        'modify_image_attribute'
    ],
    'elb': [
        'describe_*'
    ],
    'elbv2': [
        'describe_*'
    ],
    'rds': [
        'describe_*',
        'list_*'
    ],
    's3': [
        'copy',
        'get_*',
        'list_*',
        'upload_file',
        'put_object',
        'download_file'
    ]
}
```