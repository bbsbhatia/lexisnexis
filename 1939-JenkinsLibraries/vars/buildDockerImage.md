buildDockerImage()
========================

buildDockerImage() provides a way to build an image from a Dockerfile and deploy it to ECR in the **operations-content-prod** account. Image repos will be created in the following format:

```
807841377931.dkr.ecr.us-east-1.amazonaws.com/<ASSET ID>/<ASSET AREA NAME>
```

All image builds are tagged in the following way:
```
807841377931.dkr.ecr.us-east-1.amazonaws.com/<ASSET ID>/<ASSET AREA NAME>:<COMMIT ID>.<BRANCH NAME>.build.<BUILD NUMBER>
```

The **latest** tag is always placed on the latest build, regardless of branch, and the **stable** tag is placed on the latest build from master.

Usable from both declarative and scripted pipelines.

### Valid Project Structures
```bash
Project/
    DevOps/
        Docker/
            Dockerfile
        build.pipeline
```

```bash
Project/
    DevOps/
        build.pipeline
    Docker/
        Dockerfile        
```

### Dependencies

Build pipelines leveraging buildDockerImage() need to supply the following environment variables (similar to most build pipelines):
- ASSET_ID
- ASSET_NAME
- PROJECT_PATH
- ASSET_AREA_NAME

### USAGE
#### Method call
```groovy
taggedRemoteImageURI = buildDockerImage()
```
#### Within a pipeline
```groovy
@Library('DevOpsShared@v1') _
pipeline {
    agent any
    environment {
        def BUSINESS_UNIT = "Content"
        def ASSET_ID = "<asset id>"
        def ASSET_NAME = "<asset name>"
        def PROJECT_PATH = "<project path>"
        def ASSET_AREA_NAME = "${ASSET_NAME}/${PROJECT_PATH}"
    }
    stages {
        stage('Quality Check') {
            steps {
                checkQuality()   
            }
        }      
        stage('Build release unit'){
            steps{
                buildReleaseUnit()
            }
        }
        stage('Build docker image'){
            steps{
                script {
                    buildDockerImage()                    
                }
            }            
        }
        stage('Publish release unit'){
            steps{
                publishReleaseUnit()
            }
        }
    }
}
```


### PARAMETERS
There are no parameters currently supported for buildDockerImage().

### RETURN VALUE
The function will return a `string` containing the tagged remote image URI that corresponds to this build.

_____