import hudson.console.HyperlinkNote

def call(Map params = [:]){
    String projectPath =    params.get('projectPath', env.PROJECT_PATH ?: '.')
    String assetId =        params.get('assetId', env.ASSET_ID)
    String assetName =      params.get('assetName', env.ASSET_NAME)
    String assetAreaName =  params.get('assetAreaName', env.ASSET_AREA_NAME)

    dir(projectPath){
        // Validate that the SonarQube Quality Gate for this Release Unit is passing
        if(!checkQuality.didItRun()){
            checkQualityDocLink = HyperlinkNote.encodeTo("https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?path=%2FREADME.md&version=GBv1&fullScreen=true", "checkQuality()")
            emailLink = HyperlinkNote.encodeTo("mailto:BCS-CloudDevOps@lexisnexis.com", "BCS-CloudDevOps@lexisnexis.com")
            println("""**** ERRROR ****
** Unable to find evidence that the ${checkQualityDocLink} step was run.
** The ${checkQualityDocLink} step must be run during all builds in order to comply with InfoSec policy.
**
** Please see Jenkins Shared Library Documentation for guidance on using ${checkQualityDocLink}.
**
** If you have any questions, please reach out to ${emailLink}
**** ERROR ****""")
            error "Unable to publish the results of this build unless the ${checkQualityDocLink} step is run."
        }
        
        dir('artifacts'){
            // Ensure there are files in the artifacts folder
            artifact_count = sh(script: 'find .//. ! -name . -print | wc -l', returnStdout: true).replace("\n", "")
            if (artifact_count == "0"){
                error "No artifacts found in release unit, nothing to publish."
            }
            else{
                echo "Found ${artifact_count} artifacts in the release unit."
            }
            
            // Define filespec for artifact upload
            String branch = commons.getBranchName()
            String commit_id = commons.getCommitId()
            String buildName = params.get('buildName', null)
                    
            def fileSpec = """{
                "files": [
                    {
                        "pattern": "*",
                        "target": "lng-dev/${assetId}/${assetName}/${assetAreaName}/${branch}/${BUILD_NUMBER}/",
                        "recursive": "true",
                        "flat" : "false",
                        "props": "AssetID=${assetId};AssetName=${assetName};AssetAreaName=${assetAreaName};Branch=${branch};Build=${BUILD_NUMBER},Commit=${commit_id},CommitTimestamp=${collectMetrics._getCommitTimestamp(commit_id)}",
                        "excludePatterns": ["*.whl"]
                    },
                    {
                        "pattern": "(.*?)-([\\+\\.a-zA-Z0-9]*)-(.*)(\\.whl)",
                        "target": "pypi-test/{1}/{2}/",
                        "recursive": "false",
                        "flat" : "true",
                        "regexp" : "true",
                        "props": "AssetID=${assetId};AssetName=${assetName};AssetAreaName=${assetAreaName};Branch=${branch};Build=${BUILD_NUMBER},Commit=${commit_id},CommitTimestamp=${collectMetrics._getCommitTimestamp(commit_id)}"
                    }
                ]
            }"""
            
            def artifactoryServer = Artifactory.server("Artifactory")

            def buildInfo = Artifactory.newBuildInfo()

            // Collect Environment Variables from build
            buildInfo.env.capture = true
            buildInfo.env.filter.addExclude("OPERATIONS_*")
            buildInfo.env.filter.addExclude("PRODUCT_*")
            buildInfo.env.collect()

            // Specify Build Name
            if(buildName == null) { buildName = "$assetId:$assetName:$assetAreaName:$branch".replace("/",":") }
            buildInfo.name = buildName

            // Collect V1 tickets from the commit log
            issue_spec = """{
                "version": 1,
                "issues": {
                    "trackerName": "V1",
                    "regexp": "([SD]-\\d+)",
                    "keyGroupIndex": 1,
                    "summaryGroupIndex": 1,
                    "trackerUrl": "https://www4.v1host.com/LexisNexis/",
                    "aggregate": "false",
                    "aggregationStatus": "RELEASED"
                }
            }"""
            try{
                buildInfo.issues.collect(artifactoryServer, issue_spec)
            } catch(Exception e){
                println("Artifactory buildInfo.issues.collect() failed with message:")
                println(e as String)
            }

            buildInfo = artifactoryServer.upload(
                spec: fileSpec,
                buildInfo: buildInfo,
                failNoOp: true
            )

            // Publish Build Info to Artifactory
            artifactoryServer.publishBuildInfo(buildInfo)

            // Scan with X-Ray
            def scanConfig = [
                'buildName': buildInfo.name,
                'buildNumber': buildInfo.number
            ]
            scanResult = artifactoryServer.xrayScan(scanConfig)

            // Add Interactive Promotion
            if(branch == "master") {
                def promotionConfig = [
                    // Mandatory parameters
                    'buildName'          : buildInfo.name,
                    'buildNumber'        : buildInfo.number,
                    'targetRepo'         : 'lng-cert',
                
                    // Optional parameters
                    'comment'            : 'Promoted to Prod',
                    'sourceRepo'         : 'lng-dev',
                    'status'             : 'Released',
                    'includeDependencies': true,
                    'copy'               : false,
                    // 'failFast' is true by default.
                    // Set it to false, if you don't want the promotion to abort upon receiving the first error.
                    'failFast'           : true
                ]

                def pypiPromotionConfig = [
                    // Mandatory parameters
                    'buildName'          : buildInfo.name,
                    'buildNumber'        : buildInfo.number,
                    'targetRepo'         : 'pypi-release',
                
                    // Optional parameters
                    'comment'            : 'Released to Pypi',
                    'sourceRepo'         : 'pypi-test',
                    'status'             : 'Released',
                    'includeDependencies': true,
                    'copy'               : false,
                    // 'failFast' is true by default.
                    // Set it to false, if you don't want the promotion to abort upon receiving the first error.
                    'failFast'           : true
                ]

                Artifactory.addInteractivePromotion server: artifactoryServer, promotionConfig: pypiPromotionConfig, displayName: "Promote PyPi Packages"
                Artifactory.addInteractivePromotion server: artifactoryServer, promotionConfig: promotionConfig, displayName: "Promote Build Artifacts"
            }
        }
    }
}