import groovy.json.JsonSlurperClassic
import groovy.json.JsonOutput
import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException

def call(Map params = [:]){
    String projectPath = params.get('projectPath', env.PROJECT_PATH ?: '.')
    String encoded = params.inspect().bytes.encodeBase64().toString()
    boolean clean = params.get('clean', true)
    String sourceCode = params.get('sourceCode')
    
    withCredentials([string(credentialsId: 'devtools-sonar-api', variable: 'token')]) {
        params['sonarApiToken'] = token + ":"
    }

    // Change the current working directory to the project root, as defined by projectPath
    dir(projectPath){
        // Clean the git checkout to ensure we have a pure checkout, unless disabled
        if(clean){
            gitClean()
        }
        // if sourceCode folder wasn't specified, find source code folder among supported folder names
        // In case of multiples (which is not allowed), will only process the last one found
        if(sourceCode == null){
            ['Source','src','source'].each {
                if(fileExists(it)){
                    sourceCode = it
                    params.put('sourceCode', it)
                }
            }
        }
        // Source Code
        // If there is a sourceCode folder
        //   1) run unit tests
        //   2) run a SonarQube scan
        if(sourceCode != null){
            echo "Running Unit Tests - Found source code at: ${sourceCode}"
            runUnitTests(params)
            echo "Running SonarQube Scan - Found source code at: ${sourceCode}"
            runSonarQube(params)
        }
        // Packer
        // Run packer validate on MERGED .packer files
        if(fileExists(['DevOps','Packer'].join(File.separator))){
            echo "Found DevOps/Packer folder - Validating Packer Templates"
            validatePacker(encoded)
        }
        // Infrastructure
        // Run aws validate-template against all ./Infrastructure/*.template files
        if(fileExists('Infrastructure')){
            echo "Found Infrastructure folder - Validating AWS CloudFormation Templates"
            validateCloudFormation(encoded)
        }
        // If we ran a sonar scan, process results
        if(fileExists('.scannerwork/report-task.txt')){
            processSonarResults(params)
        }
        // Save reports 
        saveReports(params)
        tempDir = pwd(tmp: true)
        // write a file into the tmp dir, the flag that the checkQuality() step ran
        writeFile(
            file: [tempDir,'checkQuality.txt'].join(File.separator),
            text: "${currentBuild.startTimeInMillis}"
        )
        // If we ran a sonar scan, validate the gate was good
        // we do this after saving reports, because this can error the pipeline
        if(fileExists('.scannerwork/report-task.txt')){
            validateSonar()
        }
    }
}


def didItRun(){
    tempDir = pwd(tmp: true)
    filename = [tempDir,'checkQuality.txt'].join(File.separator)
    // write a file into the tmp dir, the flag that the checkQuality() step ran
    if(fileExists(filename)){
        fileBody = readFile(
            file: filename
        )
        return (fileBody == "${currentBuild.startTimeInMillis}")
    }
    return false
}


def processSonarResults(Map params = [:]){
    // read the report-task.txt file
    HashMap sonarResults = readProperties(file: ['.scannerwork','report-task.txt'].join(File.separator))
    HashMap taskResults = null
    HashMap gateResults = null
    // Sometimes the webhook never gets sent, resulting in a failed job.
    // Retrying 6 times over the course of 30m, in an attempt to handle that scenario
    // Have to use try/catch block on timeout step, see JENKINS-51454
    // https://issues.jenkins-ci.org/browse/JENKINS-51454
    retry(6){
        try{
            timeout(time: 5, unit: 'MINUTES') {
                // Wait for Quality Gate
                def qg = waitForQualityGate()
                println("SonarQube Analysis Complete")
                // Sometimes the webhook reports back, but the API is still behind
                // Looping here to account for that - 1 second is usually enough
                for(i = 0; i < 5; i++){
                    sleep(1)
                    taskResults = commons.httpGet("${sonarResults['serverUrl']}/api/ce/task?id=${sonarResults['ceTaskId']}", params['sonarApiToken'])
                    if(taskResults['status'] == 200){
                        break
                    }
                }
                // Merge the task results into the sonar results 
                sonarResults = commons.mergeMap(taskResults['json'], sonarResults)
                // Assuming the REST call was successful, we can use the response to get the gate status
                if (taskResults['status'] == 200){
                    // Using the analysis ID, query the results of the scan
                    gateResults = commons.httpGet("${sonarResults['serverUrl']}/api/qualitygates/project_status?analysisId=${taskResults['json']['task']['analysisId']}", params['sonarApiToken'])
                    sonarResults = commons.mergeMap(gateResults.get('json'), sonarResults)
                }
            }
        } catch (FlowInterruptedException e) {
            error 'Timeout has been exceeded for response from SonarQube server.'
        }

    }
    // Wite out the resultant file to the reports directory for later retrieval.
    reportFileName = ['reports','sonar-results.txt'].join(File.separator)
    writeFile(
        file: reportFileName,
        text: JsonOutput.prettyPrint(JsonOutput.toJson(sonarResults))
    )
    println("Analysis Report created: $reportFileName")
    // commons.printMap(sonarResults)
}


def saveReports(Map params = [:]){
    // Log Unit Test and Coverage results with Jenkins
    // Unit Test must be JUnit compatible
    junit allowEmptyResults: true, testResults: 'reports/xunit-result-*.xml'
    checkCoverage(params)
    // Archive Reports as Artifacts in Jenkins, primarily for use in VSTS
    archiveArtifacts allowEmptyArchive: true, artifacts: 'reports/*'
}


def runUnitTests(Map params = [:]){
    String encoded = params.inspect().bytes.encodeBase64().toString()
    String sourceCode = params.get('sourceCode')
    //     Python
    if(fileExists([sourceCode,'Python'].join(File.separator))){
        echo "Found Source/Python folder - Running Python Tester"
        testPython(encoded) 
    }
    //     Python
    if(fileExists([sourceCode,'PyPI'].join(File.separator))){
        echo "Found Source/PyPI folder - Running Python Tester"
        testPython(encoded) 
    }
    //     Lambda
    if(fileExists([sourceCode,'Lambda'].join(File.separator))){
        // TODO - Detect Language other than Python
        echo "Found Source/Lambda folder containing .py files - Running Python Tester"
        testPython(encoded)
    }
    // if(fileExists('pom.xml')){
    //     echo "Found POM.xml file - Running Maven Test"
    //     testMaven(params)
    // }
}


def checkCoverage(Map params = [:]){
    String coverageTarget = params.get('coverageTarget', '0')
    String conditionalCoverage = params.get('conditionalCoverage', coverageTarget)
    String fileCoverage = params.get('fileCoverage', coverageTarget)
    String lineCoverage = params.get('lineCoverage', coverageTarget)
    String methodCoverage = params.get('methodCoverage', coverageTarget)
    String packageCoverage = params.get('packageCoverage', coverageTarget)
    boolean failNoReports = params.get('failNoCoverage') ? true : false

    // println("""Coverage Targets:
    // Conditionals Coverage: $conditionalCoverage%
    // File  Coverage: $fileCoverage%
    // Line Coverage: $lineCoverage%
    // Method Coverage: $methodCoverage%
    // Package Coverage: $packageCoverage%
    // Fail if no coverage reports: $failNoReports
    // """)

    if(fileExists('reports/jacoco.xml')){
        jacoco()
    }else if(fileExists('reports/cobertura.xml')){
        cobertura( 
            coberturaReportFile: 'reports/cobertura.xml',
            autoUpdateHealth: false,
            autoUpdateStability: false,
            failNoReports: failNoReports, 
            failUnhealthy: true, 
            failUnstable: true, 
            maxNumberOfBuilds: 15, 
            onlyStable: false, 
            sourceEncoding: 'ASCII', 
            zoomCoverageChart: false,
            conditionalCoverageTargets: "$conditionalCoverage, $conditionalCoverage, 0",
            fileCoverageTargets: "$fileCoverage, $fileCoverage, 0",
            lineCoverageTargets: "$lineCoverage, $lineCoverage, 0",
            methodCoverageTargets: "$methodCoverage, $methodCoverage, 0",
            packageCoverageTargets: "$packageCoverage, $packageCoverage, 0"
        )
    }else{
        cobertura( 
            coberturaReportFile: 'reports/coverage*.xml',
            autoUpdateHealth: false,
            autoUpdateStability: false,
            failNoReports: failNoReports, 
            failUnhealthy: true, 
            failUnstable: true, 
            maxNumberOfBuilds: 15, 
            onlyStable: false, 
            sourceEncoding: 'ASCII', 
            zoomCoverageChart: false,
            conditionalCoverageTargets: "$conditionalCoverage, $conditionalCoverage, 0",
            fileCoverageTargets: "$fileCoverage, $fileCoverage, 0",
            lineCoverageTargets: "$lineCoverage, $lineCoverage, 0",
            methodCoverageTargets: "$methodCoverage, $methodCoverage, 0",
            packageCoverageTargets: "$packageCoverage, $packageCoverage, 0"
        )
    }
}


def gitClean(){
    sh "git clean -dfx"
}


def testPython(String encoded){
    // This section of code is used for performing unit tests with coverage
    // against the following code types:
    //     Python Packages
    //     Python based Lambda functions
    //     Other Python based packages
    script = libraryResource "python/pythonTester.py"
    writeFile file: "pythonTester.py", text: script
    
    shCommand =  """virtualenv -p python3.6 venv --clear
                    . venv/bin/activate
                    python -m pip install pytest coverage --target=venv/lib/python3.6/site-packages/

                    python -u pythonTester.py $encoded

                    deactivate
                """
    sh shCommand
}


def validateCloudFormation(String encoded){
    script = libraryResource "devops/validateTemplate.py"
    writeFile file: "validateTemplate.py", text: script
    
    wrap([$class: 'BuildUser']) {
        shCommand = "python3 -u validateTemplate.py ${STAGING_BUCKET}"
        sh shCommand
    }
}


def generateProjectKey(Map params = [:]){
    sonarQubeEnv = params.get('sonarQubeEnv', "Default")
    // branchName = params.get('branchName')
    branchName = commons.getBranchName(params)
    branchFamily = branchName.split('/')[0]

    repoUrl = params.get('repoUrl')
    repoName = commons.getRepoName(repoUrl)

    projectPath = params.get('projectPath', env.PROJECT_PATH ?: '.').replace("/","-") 
    
    if (projectPath == '.'){
        keyBase = "BCS:${repoName}"
    }else{
        keyBase = ["BCS",repoName,projectPath].join(':')
    }

    if(checkBranchingSupport(sonarQubeEnv)){
        projectKey = keyBase
    }else{
        // These SonarQube environments use 'hacky' branch support by
        // appending branchFamily to the project Key
        projectKey = [keyBase,branchFamily].join(':')
    }

    return projectKey
}


def checkBranchingSupport(String envName){
    switch(envName){
        case ["DevTools"]: 
        // These SonarQube environments natively support branching
        // because they are licensed with Dev or greater
            return true
        default: 
            return false
    }
}


def checkSonarProjectSeeded(projectName, projectKey){
    // We make an API call to the sonar server to see if there are any analyses for the project
    // If the response is 200 (Project Exists) AND the count of analyses in the system is not 0
    // Then the project is to be considered as 'seeded'    
    try{
        def connection = new URL("$SONAR_HOST_URL/api/project_analyses/search?project=$projectKey&ps=1").openConnection() as HttpURLConnection
        connection.setRequestMethod("POST")
        status = connection.getResponseCode()
        if (status == 200){
            println "Successfully discovered SonarQube project $projectName"
        }
        else{
            println "Failed to discover SonarQube project $projectName ($status)"
        }
        
        // Status of 200 means the project already exists
        if (status == 200){
            def slurper = new JsonSlurperClassic()
            Map response = slurper.parseText(connection.inputStream.text)
            if (response['paging']['total'] != 0){
                return true
            }
        }
        // Status of 404 means the project does NOT already exist
        else if (status == 404){
            // pre-create the project
            connection = new URL("$SONAR_HOST_URL/api/projects/create?project=$projectKey&name=$projectName").openConnection() as HttpURLConnection
            connection.setRequestMethod("POST")
            status = connection.getResponseCode()
            if (status == 200){
                println "Successfully precreated SonarQube project $projectName"
            }
            else{
                println "Failed to precreate SonarQube project $projectName ($status)"
            }
            // TODO Handle different sonar servers, as this only works with DevTools
            //and assign the "BCS - Grade F" quality gate which is gateId "15"
            connection = new URL("$SONAR_HOST_URL/api/qualitygates/select?projectKey=$projectKey&gateId=15").openConnection() as HttpURLConnection
            connection.setRequestMethod("POST")
            connection.setRequestProperty("Authorization", "Basic "+"$SONAR_AUTH_TOKEN:".bytes.encodeBase64().toString())

            status = connection.getResponseCode()
        
            if (status == 204){
                println "Successfully assigned Quality Gate [BCS - Grade F] to project $projectName"
            }
            else{
                println "Failed to assign Quality Gate [BCS - Grade F] to project $projectName ($status)"
            }
        }
        return false
    }catch(Exception e){
        println "ERROR: $e"
        return false
    }
}


def testMaven(Map params = [:]){
    mvnHome = tool name: 'Maven-3.5', type: 'maven'
    sh "mvn clean test"
    sh "mkdir reports -p"
    sh "cp target/surefire-reports/TEST-*.xml reports/"
    sh "cp target/site/jacoco/jacoco.xml reports/jacoco.xml"
}


def runSonarQube(Map params = [:]){
    sonarQubeEnv = params.get('sonarQubeEnv', 'DevTools')
    branchName = params.get('branchName')
    scannerHome = tool name: 'SonarQube', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
    projectKey = params.get('projectKey', generateProjectKey(params))
    projectName = params.get('projectName', projectKey)

    branchName = commons.getBranchName(branchName)

    withSonarQubeEnv(sonarQubeEnv){
        def shCommand =  """
            $scannerHome/bin/sonar-scanner \
            -Dsonar.projectKey=$projectKey \
            -Dsonar.projectName=$projectName \
            -Dsonar.projectVersion=$BUILD_NUMBER \
            -Dsonar.scm.disabled=true \
        """
        
        // If there is a sonar-project.properties file defined, we need to use it
        if(fileExists(['DevOps','sonar-project.properties'].join(File.separator))){
            println("Found prop file")
            shCommand += "-Dproject.settings=DevOps${File.separator}sonar-project.properties "
        }
        // If not, we'll set some defaults
        else{
            println("Did not find prop file")
            shCommand += "-Dsonar.projectBaseDir=. "
            shCommand += "-Dsonar.exclusions=**/*Test*/**,**/*test*/** "
            shCommand += "-Dsonar.coverage.exclusions=**/Tests/**,Source/Python/setup.py,Source/Layer/layer.py "
            shCommand += "-Dsonar.python.coverage.reportPath=reports/*coverage*.xml "
            shCommand += "-Dsonar.python.xunit.reportPath=reports/xunit-result-*.xml "

            // Educated guesses that if a target or bin folder exists, these might be java
            // If it is java, this is where we will find the classfiles
            if(fileExists('target')){
                shCommand += "-Dsonar.java.binaries=target "
            }else if(fileExists('bin')){
                shCommand += "-Dsonar.java.binaries=bin "
            }   

            // Educated guesses that if a lib folder exists
            if(fileExists('lib')){
                shCommand += "-Dsonar.java.libraries=lib "
            }
            
            // Educated guess on where to find tests
            if(fileExists(['src','test'].join(File.separator))){
                shCommand += "-Dsonar.tests=src${File.separator}test "
            }else if(fileExists('Tests')){
                shCommand += "-Dsonar.tests=Tests "
            }else if(fileExists('tests')){
                shCommand += "-Dsonar.tests=tests "
            }

            // Educated guess on where to find the source code
            if(fileExists('src')){
                shCommand += "-Dsonar.sources=src "
            }else if(fileExists('source')){
                shCommand += "-Dsonar.sources=source "
            }else{
                shCommand += "-Dsonar.sources=Source "
            }
        }

        // If the environment supports branching, we should use it
        if(checkBranchingSupport(sonarQubeEnv)){
            // If the project has never had an initial run, branching won't work
            // So we check to see if the project has been seeded, if it has, we can
            // use the branch params
            println("$sonarQubeEnv supports branching.")
            if(checkSonarProjectSeeded(projectName, projectKey)){
                if (branchName.startsWith('pr/') || branchName.startsWith('pull/') || branchName.startsWith('origin/pr/') || branchName.startsWith('refs/pull/')) {
                    git_comment = sh(script: "git log -1 --pretty=%B", returnStdout: true).trim()
                    // Assuming standard PR Commit Message of: 
                    //       Merge pull request #### from feature/branch into master
                    //       0     1    2       3    4    5              6    7
                    fragments = git_comment.split(' ')
                    pr_key = fragments[3]
                    pr_source = fragments[5]
                    pr_target = fragments[7]

                    shCommand += "-Dsonar.pullrequest.branch=$pr_source "
                    shCommand += "-Dsonar.pullrequest.key=$pr_key "
                    shCommand += "-Dsonar.pullrequest.base=$pr_target "
                }else{
                    shCommand += "-Dsonar.branch.name=$branchName "
                }
            }
        }

        // If logLevel was passed into the step and it's value is DEBUG, then pass along the -X flag to sonar
        if (params.get('logLevel','WARNING').toUpperCase() == "DEBUG"){
            shCommand += " -X "
        }

        // Run SonarScan
        sh shCommand
    }

}


def validatePacker(encoded){
    //TODO - Packer template validation requires the merge to occur first.
    echo "Packer template validation not yet implemented."
}


def validateSonar(){
    reportFileName = ['reports','sonar-results.txt'].join(File.separator)
    if (fileExists(reportFileName)){
        println("Reading SonarQube Analysis Report from $reportFileName")
        sonarResults = readFile(reportFileName)
        sonarJson = commons.slurpJson(sonarResults)
        println("Analysis Quality Gate Status: ${sonarJson['projectStatus']['status']}")
        switch (sonarJson['projectStatus']['status']) {
            case "OK":
                return true
            case "WARN":
                currentBuild.result = "UNSTABLE"
                return true
            default:
                error "Pipeline aborted due to quality gate failure: ${sonarJson['projectStatus']['status']}"
        }
    }
    else{
        println("Unable to find $reportFileName.  Did the checkQuality() step run?")
        return false
    }
}