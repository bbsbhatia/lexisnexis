def call(Map params = [:], String service, List<String> commands){
    String command = "Not set"
    try {
        command = params.get('command', null)
        if (!command in commands) {
            echo "$service does not yet implement $command"
            throw new Exception("$command not found")
        }
        def envvars = getEnvVars(params)
        return invokeCommand(service, command, params, envvars)
    }
    catch (Exception e){
        echo "Call to AWS ${service}.${command} failed"
        printStackFormatted(e)
        throw e
    }
}

def invokeCommand(String service, String command, Map params, envvars) {
    def script2 = libraryResource "aws/$service/${command}.py"
    writeFile file: "temp/${command}.py", text: script2
    // Encode parameters to pass to python
    String encoded = params.inspect().bytes.encodeBase64().toString()
    // Inject AWS Tokens into Environment
    return invokeAndReturnJson(envvars, command, encoded)
}

def invokeAndReturnJson(envvars, String command, String encoded) {
    def results = null
    withEnv(envvars) {
        wrap([$class: 'BuildUser']) {
            // Execute python script
            shCommand2 = "python36 -u temp/${command}.py $encoded"
            results = sh(script: shCommand2, returnStdout: true)
        }
    }
    // Process response Json into a json object
    try {
        print(results)
        if (results != null) {
            def slurper = new groovy.json.JsonSlurperClassic()
            return slurper.parseText(results)
        } else {
            return [result: "No output collected from $command"]
        }
    }
    catch(Exception e){
        //printStackFormatted(e)
        return [result: "No output collected from $command"]
    }
}

def getEnvVars(Map params) {
// Process Parameters
    String region = params.get('region', 'us-east-1')
    String account = params.get('account', null)
    String templateName = params.get('templateName', null)
    String stagingBucket = params.get('stagingBucket', '1939-jenkins-staging-p2')
    def envvars = null
    // Obtain Library Resources
    def script = libraryResource "aws/sts/assumeRole.py"
    writeFile file: "temp/assumeRole.py", text: script
    // Get AWS Credentials for desired account/region
    def shCommand = "python36 -u temp/assumeRole.py -account $account -region $region"
    echo "$shCommand"
    wrap([$class: 'BuildUser']) {
        envvars = sh(script: shCommand, returnStdout: true).trim().tokenize(',')
    }
    return envvars
}

void printStackFormatted(Exception e) {
    def sw = new StringWriter()
    def pw = new PrintWriter(sw)
    e.printStackTrace(pw)
    echo sw.toString()
}
