import net.sf.json.JSONObject

def call(Map params = [:]){
    String templateName = params.get('templateName', null)
    String amiName = params.get('amiName', generateAmiName(templateName))
    boolean autoShare = params.get('autoShare', true)
    String bakeryBranch = params.get('bakeryBranch', 'v1')
    String bakeryRepoUrl = params.get('bakeryRepoUrl', 'https://tfs-glo-lexisadvance.visualstudio.com/DefaultCollection/Content/_git/287-Bakery')
    String base = params.get('base', 'amazon-linux-2')
    boolean debug = params.get('debug', false)
    String projectPath = params.get('projectPath', env.PROJECT_PATH ?: '.')
    String sourceAmi = params.get('sourceAmi')

    dir(projectPath){
        if(bakeryBranch != 'v1'){
            timeout(time: 30){
                input(message: "Using a non default bakeryBranch requires DevOps approval.", submitter: "Business and Content - Cloud DevOps")
            }
        }
        if(bakeryRepoUrl != 'https://tfs-glo-lexisadvance.visualstudio.com/DefaultCollection/Content/_git/287-Bakery'){
            timeout(time: 30){
                input(message: "Using a non default bakeryRepoUrl requires DevOps approval.", submitter: "Business and Content - Cloud DevOps")
            }
        }
        // Grab the Bakery Repo in order to have access to our tooling
        checkout    changelog: false, 
                    poll: false, 
                    scm: [  $class: 'GitSCM', 
                            branches: [[name: bakeryBranch]], 
                            extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: '287-Bakery']], 
                            userRemoteConfigs: [[   credentialsId: 'f9d5051e-bc30-4a4b-aab6-e5cc3693d9cf', 
                                                    url: bakeryRepoUrl]]]

        // Ensure that packer is installed and available on this Jenkins Worker Node
        packerHome = tool name: 'packer-1.3', type: 'com.cloudbees.jenkins.plugins.customtools.CustomTool'

        // Ensure artifacts directory exists for the manifest file to land in
        sh "mkdir -p artifacts"

        def newAncestry = null
        Map imageData = null
        boolean abbreviate = null
        // If a sourceAmi was provided
        if(sourceAmi){
            // Get information for the provided AMI
            imageData = getImageData(sourceAmi)
            newAncestry = "$sourceAmi,${imageData.Ancestry}"
            int maxAmiAge = 60
            // If the sourceAMI is owned by BCS
            if (imageData.OwnerId == "807841377931") {
                // Verify it has not exceeded the maximum age
                if (calculateAmiAge(imageData) <= maxAmiAge){
                    // Approve for an abbreviated run
                    abbreviate = true
                }else{
                    // Error, requesting younger AMI
                println("""
            +======================== ERROR ========================+
            |  The requested source AMI is too old.                 |
            |  Provide a sourceAmi younger than $maxAmiAge.                 |
            +======================== ERROR ========================+
                """)
                error "Source AMI does not meet requirements.  Please provide a different sourceAmi."
                }
            // If the sourceAMI is owned by RETS
            }else if (imageData.OwnerId == "752576941788"){
                // It still needs the full bake to receive BCS Requirements
                abbreviate = false
            // EMR has an exception to excuse it from hardening
            // any EMR AMIs must be baked from the base Amazon Linux AMI.  
            // This logic allows us to make the BCS EMR Base AMI
            }else if (imageData.OwnerId == "137112412989" && imageData.ImageLocation.startsWith("amazon/amzn-ami-hvm-20") && base == 'emr'){
                abbreviate = false
            }
            // If it is owned by any other account, error out
            else{
                println("""
            +======================== ERROR ========================+
            |  The requested source AMI is not trusted              |
            |  Please provide a sourceAmi owned by Account          |
            |  807841377931 (BCS) or 752576941788 (RETS)            |
            +======================== ERROR ========================+
                """)
                error "Source AMI does not meet requirements.  Please provide a different sourceAmi."
            }
        }
        // If a sourceAmi was not provided
        else{
            // The templates in the bakery use BCS AMIs as their base
            // which are approved for an abbreviated run
            abbreviate = true
            // If no sourceAMI was provided, we'll use the most recently published BCS Base AMI for the OS chosen
            sourceAmi = getBaseAmiId(base) 
            // Look up AMI metadata
            imageData = getImageData(sourceAmi)
            ancestry = imageData.Ancestry
            if (ancestry){
                newAncestry = "$sourceAmi,${imageData.Ancestry}"
            } else {
                newAncestry = "$sourceAmi,${imageData.OwnerId}"
            }
        }
        println("""    Found AMI:
        ImageId:        ${imageData.ImageId}
        OwnerId:        ${imageData.OwnerId}
        CreationDate:   ${imageData.CreationDate}
        ImageAge:       ${calculateAmiAge(imageData)}
        Ancestry:       ${imageData.Ancestry}""")
        // Merge the template fragments into a complete packer template
        buildFullTemplate(projectPath, base, templateName, debug, abbreviate)
        

        // Build packer command
        def shCommand = "$packerHome/packer build"
        shCommand += " -var-file=./287-Bakery/default.vars"
        shCommand += " -var \'amiName=$amiName\'"
        shCommand += " -var \'templateName=$templateName\'"
        shCommand += " -var \'Ancestry=$newAncestry\'"
        shCommand += " -var \'sourceAmi=$sourceAmi\'"
        if(debug){shCommand += " -debug"}
        shCommand += " \'Packer/merged-$templateName" + ".packer\'"

        archiveArtifacts allowEmptyArchive: true, artifacts: "Packer/merged-${templateName}.packer"
        // Run packer command
        timestamps {
             sh shCommand
        }

        // Read Manifest file to get AMI-ID
        String amiId = getAmiIdFromManifest("artifacts/${templateName}_packerManifest.json")
        echo amiId

        // Share the generated AMI with all accounts
        if (autoShare){
            shareAmi(amiId)
        }

        // Add Manifest File to Artifacts
        archiveArtifacts allowEmptyArchive: true, artifacts: '**/*_packerManifest.json'
        return amiId
    }
}


Map buildersMerge(List builders){
    Map result

    if (builders.size() == 0) {
        result = [:]
    } else if (builders.size() == 1) {
        result = builders[0]
    } else {
        result = [:]
        builders.each { 
            def builder = new LinkedHashMap(it)
            builder.each { k, v ->
                if (v instanceof Map) {
                    result[k] = templateMerge(result[k], v)
                } else if (result[k]) {
                    result[k] += v
                } else {
                    result[k] = v
                }
            }
        }
    }

    return result
}


Map buildFullTemplate(String projectPath, String base, String templateName, Boolean debug, Boolean abbreviate){
    def global_json = readJSON file:"./287-Bakery/global.packer"
    def base_json = readJSON file:"./287-Bakery/${base}/base.packer"
    def prefix_json = readJSON file:"./287-Bakery/${base}/prefix.packer"
    def user_json = readJSON file:"./Packer/${templateName}.packer"
    def suffix_json = readJSON file:"./287-Bakery/${base}/suffix.packer"
    
    user_json = sanitizeUserTemplate(user_json)

    // Merge all of the templates
    if(abbreviate){
        final_map = templateMerge(global_json, base_json, user_json, suffix_json)
    }else{
        final_map = templateMerge(global_json, base_json, prefix_json, user_json, suffix_json)
    }
    
    // Merge individual JSONObjects from the builders JSONArray into a single JSONObject
    // TODO: There has got to be a better way, but this is working, and I've spent 
    //       WAY too much time on this.
    def builders = new java.util.ArrayList()
    builders = final_map.get('builders')
    final_map.remove('builders')
    final_builders = buildersMerge(builders)
    final_map.put('builders', [final_builders])

    // Since our merge concats the values of variables, 
    // if the user provides a value for instanceType, we'll have an invalid value.
    // So we need to handle allowing the user to overwrite our default value.  
    // This is the only value that should have a collision like this.
    if(user_json.get('builders')){
        if(user_json['builders'].size() > 0){
            if(user_json['builders'][0].get('instance_type')){
                final_map['builders'][0]['instance_type'] = user_json['builders'][0]['instance_type']
            }
        }
    }
    if(user_json.get('variables')){
        if(user_json['variables'].get('instanceType')){
            final_map['variables']['instanceType'] = user_json['variables']['instanceType']
        }
    }
    
    def output = new net.sf.json.JSONObject().fromObject(final_map)

    if(debug){
        println(output.toString(4))
    }
    writeJSON(file: "./Packer/merged-${templateName}.packer", json: output)
    
    return final_map
}


int calculateAmiAge(Map imageData){
    def awsTimeStampPattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    def date = new Date().parse(awsTimeStampPattern, imageData.CreationDate)
    def now = new Date()
    def amiAge = now - date
    return amiAge
}


String generateAmiName(String templateName){
    if(templateName == null){
        error "generateStackName requires templateName as a parameter"
    }
    else{
        return JOB_NAME.replace("/","-").replace("%2F", "-") + "-" + BUILD_NUMBER + "-" + templateName
    }
}


String getAmiIdFromManifest(String filePath){
    manifestJson = readJSON(file: filePath)
    String amiId = null
    // We have seen this file persist from build to build, in which case Packer appends the file.  
    // The proper AMI for the build will always be the last run, so we'll use the UUID to find it in the file.
    manifestJson['builds'].find{ build ->
        if (build['packer_run_uuid'] == manifestJson['last_run_uuid']){
            amiId = build['artifact_id'].tokenize(':')[1]
            println("Found AMI ID ($amiId) in $filePath")
            return amiId
        }
    }
    return amiId

}


String getBaseAmiId(String os){
    switch(os) {
        case 'amazon-linux':
            return BCS_AMI_AMAZON_LINUX
        case 'amazon-linux-2':
            return BCS_AMI_AMAZON_LINUX_2
        case 'emr':
            return BCS_AMI_EMR
        case 'windows-2012r2':
            return BCS_AMI_WINDOWS_2012R2
        case 'windows-2016':
            return BCS_AMI_WINDOWS_2016
        case 'amazon-linux-2-ecs':
            return BCS_AMI_AMAZON_LINUX_2_ECS
        default:
            error "Invalid base os provided.  Valid values are: ['amazon-linux', 'amazon-linux-2', 'amazon-linux-2-ecs', 'emr', 'windows-2012r2', 'windows-2016']"
    }
}


Map getImageData(String amiId){
    writeFile(file: "temp/boto3wrapper.py", text: libraryResource("aws/boto3wrapper.py"))
    scriptParams = [client: 'ec2',
                    method: 'describe_images',
                    arguments: [ImageIds: [amiId]],
                    query: 'Images[0].{ImageLocation: ImageLocation, ImageId: ImageId, OwnerId: OwnerId, CreationDate: CreationDate, Ancestry: (Tags[?Key == `Ancestry`].Value)[0]}']
    def paramsJson = new net.sf.json.JSONObject(scriptParams)
    def results = new net.sf.json.JSONObject()
    withEnv(["AWS_DEFAULT_REGION=us-east-1"]){
        def shCommand = "python3 -u temp/boto3wrapper.py '${paramsJson.toString()}'"
        results = net.sf.json.JSONObject.fromObject(sh(script: shCommand, returnStdout: true).trim())
    }
    println(results)
    return new LinkedHashMap(results)
}


JSONObject sanitizeUserTemplate(JSONObject sanitized){
    // If users defined a protected builder, remove it
    if(sanitized.get('builders')){
        def allowedBuilders = [
            "ami_block_device_mappings",
            "launch_block_device_mappings",
            "ami_description",
            "instance_type"
        ]
        for(i=sanitized['builders'].size()-1; i>=0; i--){
            sanitized['builders'][i].each { k, v ->
                if (!allowedBuilders.contains(k)){
                    sanitized['builders'][i].remove(k)
                }
            }
        }
    }
    // If users defined a protected variable, remove it
    if(sanitized.get('variables')){
        sanitized['variables'].remove('vpcId')
        sanitized['variables'].remove('subnetId')
        sanitized['variables'].remove('sgIds')
        sanitized['variables'].remove('amiName')
        sanitized['variables'].remove('sourceAmi')
        sanitized['variables'].remove('Ancestry')
        sanitized['variables'].remove('regionName')
        sanitized['variables'].remove('userDataFile')
        sanitized['variables'].remove('regionName')
    }
    return sanitized
}


def shareAmi(String amiId, String accountId){
    println("Sharing AMI ($amiId) with account $accountId.")
    // Run python script to share AMI out to target account
    // Operations-content-prod and opersations-bcs-dev are shared by packer.
    writeFile(file: "temp/boto3wrapper.py", text: libraryResource("aws/boto3wrapper.py"))
    def scriptParams = [
        client: 'ec2',
        method: 'modify_image_attribute',
        arguments: [
            ImageId: amiId,
            LaunchPermission: ['Add': [['UserId': accountId]]]
        ]
    ]
    def paramsJson = new net.sf.json.JSONObject(scriptParams)
    def results = new net.sf.json.JSONObject()
    withEnv(["AWS_DEFAULT_REGION=us-east-1"]){
        def shCommand = "python3 -u temp/boto3wrapper.py '${paramsJson.toString()}'"
        results = net.sf.json.JSONObject.fromObject(sh(script: shCommand, returnStdout: true).trim())
    }
    println(results)
}


def shareAmi(String amiId){
    println("Sharing AMI ($amiId) with BCS Accounts")
    // Run python script to share AMI out to all BCS accounts
    // Operations-content-prod and opersations-bcs-dev are shared by packer.
    writeFile(file: "temp/boto3wrapper.py", text: libraryResource("aws/boto3wrapper.py"))
    def scriptParams = [
        client: 'ec2',
        method: 'modify_image_attribute',
        arguments: [
            ImageId: amiId,
            LaunchPermission: ['Add': [
                ['UserId': '847642044734'], //Product-lexisai-dev
                ['UserId': '066018718749'], //Product-lexisai-cert
                ['UserId': '910689975622'], //Product-lexisai-prod
                ['UserId': '774060640982'], //Product-warehouse-dev
                ['UserId': '347954756689'], //product-warehouse-cert
                ['UserId': '638746469603'], //Product-warehouse-prod
                ['UserId': '288044017584'], //Product-datalake-dev
                ['UserId': '873434867576'], //Product-datalake-cert
                ['UserId': '195052678233'], //Product-datalake-prod
                ['UserId': '069379813652'], //Product-content-sandbox
                ['UserId': '284211348336'], //product-content-dev
                ['UserId': '217306840436'], //product-content-cert
                ['UserId': '533833414464'], //product-content-prod
                ['UserId': '153576756524'], //Product-business-dev
                ['UserId': '961609659473'], //Product-business-cert
                ['UserId': '173462424778'], //Product-business-prod
                ['UserId': '060958554326'], //Operations-business-prod
                ['UserId': '083190835473']  //Product-flywheel-dev
            ]]
        ]
    ]
    def paramsJson = new net.sf.json.JSONObject(scriptParams)
    def results = new net.sf.json.JSONObject()
    withEnv(["AWS_DEFAULT_REGION=us-east-1"]){
        def shCommand = "python3 -u temp/boto3wrapper.py '${paramsJson.toString()}'"
        results = net.sf.json.JSONObject.fromObject(sh(script: shCommand, returnStdout: true).trim())
    }
    println(results)
}


Map templateMerge(Map... maps) {
    Map result

    if (maps.length == 0) {
        result = [:]
    } else if (maps.length == 1) {
        result = maps[0]
    } else {
        result = [:]
        maps.each { map ->
            map.each { k, v ->
                if (result[k] instanceof Map) {
                    result[k] = templateMerge(result[k], v)
                } else if (result[k]) {
                    result[k] += v
                } else {
                    result[k] = v
                }
            }
        }
    }
    return result
}