import groovy.json.JsonSlurper
import groovy.json.JsonOutput

def call(Map params = [:]){
    String command = params.get('command', null)

    switch (command) {
        case 'cp':
            cp(params)
            break
        default:
            echo "You must specify a command, or specified command not yet implimented"
            break
    }
}

def cp(Map params = [:]){
    // Process Parameters
    String region = params.get('region', 'us-east-1')
    String account = params.get('account')

    // Inject EnvVars into param map to pass to python
    params['source'] = params.get('source')
    params['destination'] = params.get('destination')
    params['recursive'] = params.get('recursive') ? '--recursive' : ''
    
    // Obtain Library Resources
    def script = libraryResource "aws/sts/assumeRole.py"
    writeFile file: "temp/assumeRole.py", text: script

    // Get AWS Credentials for desired account/region
    def shCommand = "python3 -u temp/assumeRole.py -account $account -region $region"
    wrap([$class: 'BuildUser']) {
        // Inject AWS Tokens into Environment
        withEnv(sh(script: shCommand, returnStdout: true).trim().tokenize(',')){
            params['buildUserId'] = env.BUILD_USER_ID ?: 'unknown'
            // Execute python script
            shCommand2 = "aws s3 cp ${params['source']} ${params['destination']} ${params['recursive']}"
            sh(script: shCommand2, returnStdout: false)
        }
    }

}