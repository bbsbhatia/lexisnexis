[Readme](/README.md)
# Branching Strategy

## Major Versions (v#)

Each major version will be maintained as an individual branch.  The default library in Jenkins will always be the latest version's mainline branch.  When using a major version branch, you will always have the latest release of _that_ version.

It is our development goal that there will be no breaking changes within a Major Version.  It is possible that due to Security Requirements, we will not be able to adhere to this vision, but we will try our best.

## Minor Versions (v#.#)

Minor Versions will be developed as feature branches and commonly available as pre-release under their specific version number.  Once merged back into the Major version branch, a tag will be assigned to the commit allowing use of point-in-time versions.  
> _If you are using a minor version tag, you will never benefit from future features or fixes._

# Change Log
- [**v1**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1) - _Current release branch_
    - [**v1.36**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.36)
        - _Update Step_: [publishReleaseUnit()](/vars/publishReleaseUnit.md)
            - Build publication now triggers an X-Ray scan, checking for dependencies with known security vulnerabilities.
            - If the X-Ray scan discovers any violations, the build will be flagged, and the Jenkins job will fail.
            - Wrapped Artifactory Build Issue Collection in a try block.
            - v1.36.2 - Modified Artifactory Build name to handle weird URL patterns in Artifactory
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Retry [Cerberus](https://cerberus.content.aws.lexis.com) calls if API Gateway returns 5xx errors. (Limit 3)
    - [**v1.35**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.35)
        - _New Method_: [buildReleaseUnit()](/vars/buildReleaseUnit.md)
            - Slight modification to how python virtual environments are created, to support updating workers to Amazon Linux 2
    - [**v1.34**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.34)
        - _New Method_: [checkQuality()](/vars/checkQuality.md)
            - Slight modification to how python virtual environments are created, to support updating workers to Amazon Linux 2
    - [**v1.33**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.33)
        - _New Method_: [collectMetrics.reportRelease()](/vars/collectMetrics.md)
            - This is a beta release, not for broad use yet.  If interested, please reach out.
            - Documentation will be added when this feature is ready for public consumption.
    - [**v1.32**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.32)
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Update Cerberus API calls to reflect module name changes.
    - [**v1.31**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.31)
        - _Update Step_: [buildDockerImage()](/vars/buildDockerImage.md)
            - Corrected faulty handling of Docker exit codes
        - _Update Method_: [buildDockerImage.docker()](/vars/buildDockerImage.md)
            - Added condition to throw an error when non-zero docker exit codes are encountered.
        - _Update Method_: [buildDockerImage.buildDocker()](/vars/buildDockerImage.md)
            - Removed incorrect exit code handling.
    - [**v1.30.1**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.30.1)
        - _Update Step_: [stageReleaseUnit()](/vars/stageReleaseUnit.md)
            - Corrected bug where when staging to S3, the file structure was flattened
        - _Update Method_: [commons.getBuildUserId()](/vars/commons.md)
            - Added workaround for problems obtaining the build user when the job was triggered from another job
    - [**v1.30**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.30)
        - _Update Step_: [stageReleaseUnit()](/vars/stageReleaseUnit.md)
            - **BREAK RISK** 
                - There is a small chance that non-standard usage of `stageReleaseUnit()` will have issues with this change.
                - Please reach out immediately and we will help remediate the issue.
            - Parameter and behavior change for following methods added in v1.25. No one appears to be using them yet, so it was deemed an acceptable risk.
                - `fromArtifactory()`
                - `toS3()`
            - Complete rework of step logic.
            - Transitioned to a `source` and `destination` parameter specification
            - Add support for `smb` destination type
                - _Note: destination share must grant read/write permissions to `app-leglngdayapp245@legal.regn.net`_
    - [**v1.29**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.29)
        - _Update Method_: [buildReleaseUnit.runTransformations()](/vars/buildReleaseUnit.md)
            - Fix bug in escape2fnsub transform.
    - [**v1.28**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.28)
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Update SonarQube API calls to include an API key as an authorization token.
    - [**v1.27**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.27)
        - _Update Method_: [buildAmi.shareAmi()](/vars/buildami.md)
            - Add new LexisAI accounts to share list for AMIs.
            - Add overload of shareAmi that allows specification of target account.
    - [**v1.26**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.26)
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Modified processSonarResults() method to retry, incase Sonarqube fails to send webhook notification.
    - [**v1.25**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.25)
        - _Update Step_: [stageReleaseUnit()](/vars/stageReleaseUnit.md)
            - Added initial documentation
            - Break out two new methods `fromArtifactory()` and `toS3()` to enable more granular workflows, including modification of release unit before upload.  For example, file renames.
    - [**v1.24.2**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.24)
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Fix to account for eventually-consistent responses from [Cerberus](https://cerberus.content.aws.lexis.com)
    - [**v1.24.1**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.24)
        - _Updated Step: [boto3()](/vars/boto3.md)
            - Properly handle non-json responses from boto3, problem created due to implementation of JsonSlurperClassic
    - [**v1.24**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.24)
        - _Updated Step: [boto3()](/vars/boto3.md)
            - Properly handle a null response from boto3, problem created due to implementation of JsonSlurperClassic
    - [**v1.23**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.23)
        - _Updated Step: [boto3()](/vars/boto3.md)
            - Changed from using groovy.json.JsonSlurper, to JsonSlurperClassic to avoid a Jenkins CPS error.
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Improve handling of empty responses from sonarqube.
            - Convert [Cerberus](https://cerberus.content.aws.lexis.com) Scans to blocking.
    - [**v1.22**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.22)
        - _Updated Method_: [buildAmi.getBaseAmiId()](/vars/buildAmi.md)
            - Added 'amazon-linux-2-ecs' as a valid option.
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - SCM will always be disabled as a workaround for the Git Blame issue until it has been resolved.
    - [**v1.21**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.21)
        - _Update Step_: [boto3()](/vars/boto3.md)
            - Whitelist Changes:
                - Updated client: `cloudfront`
                    - Added method `get_distribution_config`
                    - Added method `update_distribution`
            - Improved escaping of string parameters that include single-quotes
    - [**v1.20**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.20)
        - _New Step_: [buildDockerImage()](/vars/buildDockerImage.md)
            - Provides a way to build an image from a Dockerfile and deploy it to ECR in the **operations-content-prod** account. 
    - [**v1.19**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.19)
        - _Update Step_: [boto3()](/vars/boto3.md)
            - Whitelist Changes:
                - New client: `athena`
                    - `start_query_execution`
    - [**v1.18**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.18)
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Results of a SonarQube scan are now copied into the reports folder as a json file.
            - pythonTester no longer blows up if it is unable to generate a coverage report.
            - pythonTester now accurately lists file that have 0% coverage.
        - _Update Step_: [publishReleaseUnit()](/vars/publishReleaseUnit.md)
            - Prior to execution, this step will check the workspace to verify that the checkQuality step was run.
        - _New Method_: [buildAmi.getBaseAmiId()](/vars/buildAmi.md)
            - Will return the current Base BCS AMI for a provided OS.
            - AMI ID is sourced from outside this codebase, so further AMI ID changes will no longer be listed in these version notes.
        - _Update Method_: [checkQuality.testPython()](/vars/checkQuality.md)
            - Added `*/setup.py` to the list of file to omit from coverage
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Moved validateSonar to after saveReports so that quality report artifacts will always be saved in Jenkins
    - [**v1.17**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.17)
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Change default sonarqube environment back to `DevTools` (https://opsdev-app-sonarqube.route53.lexis.com)
            - Cerberus will now send email notification on failures.
    - [**v1.16**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.16)
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Improve handling of default arguments if DevOps/sonar-project.properties exists
            - Added defaults for java projects (sonar.java.binaries and sonar.java.libraries)
    - [**v1.15**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.15)
        - _Update AMIs_: Updated default BCS AMIs to March release
            - Amazon Linux 1
            - Amazon Linus 2
            - EMR
            - Windows 2012-R2
            - Windows 2016
    - [**v1.14**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.14)
        - _Update Method_: [cloudformation.createChangeSet()](/vars/cloudformation.md)
            - Added helpful text incase createChangeSet fails due to an S3 Access Denied error
        - _Update Method_: [checkQuality.validateCloudFormation()](/vars/checkQuality.md)
            - Print the reason response if AWS's ValidateTemplate call fails
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - SonarQube Quality Gate status of Warning will not block Jenkins job, but will set the Job Status as "UNSTABLE"
    - [**v1.13**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.13)
        - _Bug Fix_: Disabled output buffering for python calls that were missing it.
        - _Bug Fix_: Improve handling when BUILD_USER_ID is missing for AWS calls.
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Change default sonarqube environment to `DevTools` (https://opsdev-app-sonarqube.route53.lexis.com)
            - SonarQube scan will now properly run against `src` folders in addition to `Source`.
            - SonarQube project keys and names are now prefixed with `BCS:` to allow for inheriting management permissions in Sonar.
            - Will now pre-create SonarQube projects and assign default BCS Quality gate if project is new.
            - Add support for Pull-Request branches in SonarQube.  Will auto-detect based on standard Azure DevOps git comment for PRs.
        - _New Method_: [checkQuality.checkCoverage()](/vars/checkQuality.md)
            - Allow coverage targets to be set within jenkins pipeline.  This is to compensate for the loss of Coverage blocking in SonarQube for short-lived branches.
    - [**v1.12**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.12)
        - _Update Method_: [checkQuality.validateCloudFormation()](/vars/checkQuality.md)
            - Internal Cerberus Webhook notify setting change from `always` to `fail`.
        - _Update Step_: [boto3()](/vars/boto3.md)
            - Add exception handling to provide automatic retries for `Throtting*` and `TooManyRequestsExceptions`
    - [**v1.11**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.11)
        - _Update Step_: [checkQuality()](/vars/checkQuality.md)
            - Added Cerberus (Static Code Analysis) as part of CloudFormation Template scanning.
    - [**v1.10**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.10)
        - _Update AMIs_: Updated default BCS AMIs to February release
            - Amazon Linux 1
            - Amazon Linus 2
            - EMR
            - Windows 2012-R2
            - Windows 2016
    - [**v1.9**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.9)
        - _Update Step_: [boto3()](/vars/boto3.md)
            - Whitelist Changes:
                - Updated client: `cloudfront`
                    - Added `create_invalidation`
        - _Bug Fix_: cloudformation.executeChangeSet()
            - Fixed a malformed JSON object that prevented proper handling of throttling exceptions.
        - _Bug Fix_: checkQuality.validateTemplate()
            - Added import for a missing library that prevented proper handling of throttling exceptions.
        - _Bug Fix_: checkQuality.testPython()
            - Modified library import search pattern to correct improper lambda construction.  Should either solve the issue or allow for faster fails.
        - _Bug Fix_: buildReleaseUnit.buildPythonLambda()
            - Modified library import search pattern to correct improper lambda construction.  Should either solve the issue or allow for faster fails.
    - [**v1.8**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.8)
        - _New Step_: [s3()](/vars/s3.md) - This method is a wrapper for the AWS CLI S3 commands.
            - _New Method_: [s3.cp()](/vars/s3.md)
        - _Update Step_: [boto3()](/vars/boto3.md)
            - New parameter `streamingBody`
                - If present, the StreamingBody at this JSON location will be read and it's contents will be the returned as a UTF-8 string.  The value must be a valid [jmespath](http://jmespath.org/) expression, and must resolve to a single StreamingBody type.  If this parameter is provided, it supersedes the query parameter.
            - Whitelist Changes:
                - Updated client: `cloudformation`
                    - Added `set_stack_policy`
                - Updated client: `cloudfront`
                    - Added `create_invalidation`
                - Updated client: `s3`
                    - Added `copy`
                - New client: `dynamodb`
                    - `create_backup`
                    - `list_global_tables`
                    - `create_global_table`
        - _Bug Fix_: withAwsAccount()
            - AWS Assume role was performed before the input step, resulting in expired credentials if approval took more than an hour to obtain.  Assume Role was moved to after the input step.
    - [**v1.7**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.7)
        - _Update Step_: [buildAmi()](/vars/buildAmi.md)
            - Update template merge logic to allow users to submit the following values within the builders block: `ami_block_device_mappings`, `launch_block_device_mappings`, `ami_description`, `instance_type`.
    - [**v1.6**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.6)
        - _Update Step_: [buildAmi()](/vars/buildAmi.md)
            - Complete rewrite to support [Bakery v1](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/287-Bakery?path=%2FVERSION.md&version=GBv1&_a=preview)
            - Now supports multi-layered bakes
        - *New Method*: [buildAmi.calculateAmiAge()](/vars/buildAmi.md)
        - *New Method*: [buildAmi.generateAmiName()](/vars/buildAmi.md)
        - *New Method*: [buildAmi.getAncestry()](/vars/buildAmi.md)
        - _New Method_: [buildAmi.getAmiIdFromManifest()](/vars/buildAmi.md)
        - *New Method*: [buildAmi.getImageData()](/vars/buildAmi.md)
        - *New Method*: [buildAmi.shareAmi()](/vars/buildAmi.md)
    - [**v1.5**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.5)
        - _Update Method_: buildReleaseUnit.buildPythonLambda()
            - Updated build script to support collection of single-file python packages.
    - [**v1.4**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.4)
        - _Update Step_: [boto3()](/vars/boto3.md)
            - Added new clients: apigateway, autoscaling, elb, elbv2, and rds
            - Update Whitelist: Add describe_* , list_* , and get_* to applicable services.
            - Update Whitelist: s3:upload_file, s3:put_object, and s3:download_file
        - _Update Step_: stageReleaseUnit()
            - Error the job if releaseUnit is missing the build number
            - Bug Fix: When calling stageReleaseUnit() the artifacts folder is now properly emptied first.  You can optionally skip cleaning, by passing the parameter `clean` with a value of `false`.  This will prevent leakage of artifacts between releases when run in subsequent jobs on the same Jenkins worker node.
        - _Update Method_: checkQuality.validateCloudFormation()
            - Upload templates to S3 and pass TemplateURL instead of TemplateBody into API call in order to eliminate size restrictions on templates.
            - Added handling for Throttling exceptions thrown by validateTemplate API call.
        - _Deprecate Methods_: The following step methods have been deprecated, and rewritten to leverage the [boto3()](/vars/boto3.md) step behind the scenes.
            - [apigateway.createDeployment()](/vars/apigateway.md)
            - [cloudformation.describeStackResource()](/vars/cloudformation.md)
            - [cloudformation.describeStacks()](/vars/cloudformation.md)
        - _Testing_: Added simple regression tests to validate functionality of the following calls:
            - [boto3()](/vars/boto3.md)
            - [cloudformation.describeStackResource()](/vars/cloudformation.md)
            - [cloudformation.describeStacks()](/vars/cloudformation.md)
    - [**v1.3**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.3)
        - _**New Step**_: [boto3()](/vars/boto3.md)
        - Started a change log (yay!)
    - [**v1.2**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.2)
        - _**New Transformation**_: Source/DocumentParts will have escape2fnsub applied
    - [**v1.1**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.1) 
        - Fix: Infinite loop when deleting a rolled back stack
        - Tool: Updated Packer to Version 1.3
        - Documentation Update
    - [**v1.0**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv1.0)
        - Initial Release
- [**v0**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv0) - _No longer supported_
    - [**v0.8**](https://tfs-glo-lexisadvance.visualstudio.com/Content/_git/1939-JenkinsLibraries?version=GTv0.8) - Snapshot preceding major pre-v1 changes