#!/usr/bin/env python3.6

###############################################################################
# Imports
###############################################################################

import os
import shutil
import logging
import sys
import subprocess
import site
from distutils.dir_util import copy_tree
import re

log = logging.getLogger()
log.setLevel(logging.INFO)

###############################################################################
# Import and Define Variables
###############################################################################

if len(sys.argv) > 0:
    zip_name = sys.argv[1]
else:
    zip_name = 'Lambda'

BUILD_NUMBER = os.getenv('BUILD_NUMBER')

###############################################################################
# Define Sub-processes
###############################################################################


def install_required_packages(my_location):
    if os.path.isfile(os.path.join(my_location, 'requirements.txt')):
        results = subprocess.check_output([
            sys.executable,
            '-m',
            'pip',
            'install',
            '-r',
            os.path.join(my_location, 'requirements.txt'),
            '--target=venv/lib/python3.6/site-packages/'
            ])
        print('{0}'.format(results.decode("utf-8")))
    else:
        print("No requirements.txt found")
    site.main()


def copy_virtual_artifacts(working_dir):
    artifact_dir = os.path.join(working_dir, 'Source', 'Lambda')
    venv_dir = os.path.join(working_dir, 'venv')
    package_dirs = [
        os.path.join(venv_dir,
                     os.path.join('lib', 'python3.6', 'site-packages')),
        os.path.join(venv_dir,
                     os.path.join('lib64', 'python3.6', 'site-packages')),
        os.path.join(venv_dir,
                     os.path.join('lib', 'python3.6', 'dist-packages')),
        os.path.join(venv_dir,
                     os.path.join('lib64', 'python3.6', 'dist-packages'))
        ]

    ignore_list = ['pip',
                   'wheel',
                   '__pycache__',
                   'pkg_resources',
                   'setuptools',
                   'easy_install.py']

    for package_dir in package_dirs:
        if os.path.exists(package_dir):
            for i in os.listdir(package_dir):
                full_path = os.path.join(package_dir, i)
                if (i not in ignore_list) and (not i.endswith('.dist-info')):
                    if os.path.isdir(full_path):
                        print('Copying Item: {0}'.format(i))
                        copy_tree(full_path, os.path.join(artifact_dir, i))
                    else:
                        shutil.copyfile(full_path, os.path.join(artifact_dir, i))

###############################################################################
# Main Function
###############################################################################

print("Executing Lambda Build for [{0}].".format(zip_name))

# Get current working directory
working_dir = os.getcwd()
print("####  Working Directory: {0}".format(working_dir))

# Ensure user libraries are NOT included in sys.path
re_user_libs = re.compile('\/usr\/.*')
sys.path = [x for x in sys.path if not re_user_libs.match(x)]

# Create Python Virtual environment if we can find a requirements.txt file
install_required_packages(working_dir)

# Copy required packages from the virtual environment
# to the Source/Lambda folder
copy_virtual_artifacts(working_dir)

# Zip up the Source/Lambda folder and place the
# resulting zip file into the artifacts folder
print("Creating Artifact: ./artifacts/{0}.zip".format(zip_name))
shutil.make_archive(os.path.join(working_dir, 'artifacts', zip_name),
                    "zip",
                    os.path.join(working_dir, 'Source', 'Lambda'))
