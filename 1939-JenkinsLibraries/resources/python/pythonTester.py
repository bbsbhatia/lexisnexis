#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import os
import sys
import pytest
import subprocess
import coverage
import site
import re

__author__ = "Mark Seitter, Brian Besl"
__copyright__ = "Copyright 2017, LexisNexis"
__version__ = "1.0"

################################################################################
# Sub Functions
################################################################################


def callPyTest(p):
    return pytest.main(p)


def getModuleName(my_location):
    return os.path.split(my_location)[-1]


def installRequiredPackages(my_location):
    if os.path.isfile(os.path.join(my_location, 'Source', 'Python', 'setup.py')):
        results = subprocess.check_output([
            sys.executable,
            '-m',
            'pip',
            'install',
            '-e',
            os.path.join(my_location, 'Source', 'Python')
        ])
        print('{0}'.format(results.decode("utf-8")))
    elif os.path.isfile(os.path.join(my_location, 'requirements.txt')):
        results = subprocess.check_output([
            sys.executable,
            '-m',
            'pip',
            'install',
            '-r',
            os.path.join(my_location, 'requirements.txt'),
            '--target=venv/lib/python3.6/site-packages/'
        ])
        print('{0}'.format(results.decode("utf-8")))
    else:
        print("No setup.py or requirements.txt found.")
    site.main()


def printExitCode(exitCode):
    if exitCode == 0:
        print("0 - All tests were collected and passed successfully")
    if exitCode == 1:
        print("1 - Tests were collected and run but some of the tests failed")
    if exitCode == 2:
        print("2 - Test execution was interrupted by the user")
    if exitCode == 3:
        print("3 - Internal error happened while executing tests")
    if exitCode == 4:
        print("4 - Unit tests provided zero coverage, or other pytest command error")
    if exitCode == 5:
        print("5 - No tests were collected")


def get_directories(working_dir='.'):
    all_directories = []
    for root, dirs, _ in os.walk(working_dir):
        all_directories.extend([os.path.join(root, dir_) for dir_ in dirs])
    return all_directories


################################################################################
# Main Function
################################################################################

# Get current working directory
working_dir = os.getcwd()
print("####  Working Directory: {0}".format(working_dir))

# Calculate Module Name
moduleName = getModuleName(working_dir)
print("####  Beginning test run for: {0}".format(moduleName))

# Add key locations to the system path
# Needed so test runner knows the how to import the test case
tests_root = os.path.join(working_dir, 'Tests')
sys.path.append(tests_root)
# Add ./Source/Lambda to sys path to handle Lambda testing
lambda_root = os.path.join(working_dir, 'Source', 'Lambda')
sys.path.append(lambda_root)
# Add ./Source/Python to sys path to handle non-package Python testing
python_root = os.path.join(working_dir, 'Source', 'Python')
sys.path.append(python_root)

# Ensure all package locations are included in sys.path
venv_dir = os.path.join(working_dir, 'venv')
sys.path.append(os.path.join(venv_dir, os.path.join('lib', 'python3.6', 'site-packages')))
sys.path.append(os.path.join(venv_dir, os.path.join('lib64', 'python3.6', 'site-packages')))
sys.path.append(os.path.join(venv_dir, os.path.join('lib', 'python3.6', 'dist-packages')))
sys.path.append(os.path.join(venv_dir, os.path.join('lib64', 'python3.6', 'dist-packages')))

# Ensure user libraries are NOT included in sys.path
re_user_libs = re.compile('\/usr\/.*')
sys.path = [x for x in sys.path if not re_user_libs.match(x)]

# Install required packages into our current env based on either requirements.txt or setup.py
installRequiredPackages(working_dir)

# Execute pytest against the current module
pytest_params = [
    tests_root,
    "--junitxml={0}".format(os.path.join(working_dir, 'reports', "xunit-result-{0}.xml".format(moduleName)))
]

# Check for a .coveragerc config filein DevOps so we can process it
# If it doesn't exist, set configPath to true to allow coverage to do it's normal thing
configPath = os.path.join(working_dir, 'DevOps', '.coveragerc')
if not os.path.isfile(configPath):
    configPath = True

# Instantiate coverage object
cov = coverage.Coverage(
    cover_pylib=False,
    branch=False,
    data_suffix=moduleName,
    omit=['*/Tests/*', '*/venv/*', 'pythonTester.py', '*/setup.py'],
    debug="*",
    config_file=configPath,
    source=get_directories('Source')
)
cov.start()
# Execute PyTest - must be done in a function call in order to trigger coverage
exitCode = callPyTest(pytest_params)
cov.stop()
cov.save()

# Generate Coverage Reports
print("######## COVERAGE RESULTS ########")
try:
    cov.report(omit=['*/Tests/*', '*/venv/*', 'pythonTester.py'])
    cov.xml_report(
        outfile="{0}".format(os.path.join(working_dir, 'reports', "coverage-{0}.xml".format(moduleName))),
        omit=['*/Tests/*', '*/venv/*', 'pythonTester.py']
    )
except coverage.misc.CoverageException as e:
    print(e)


printExitCode(exitCode)
if exitCode in [1, 2, 3]:
    sys.exit(exitCode)
