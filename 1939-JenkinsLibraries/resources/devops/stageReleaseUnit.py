#!/usr/bin/env python3.6

__author__ = "Brian Besl"
__copyright__ = "Copyright 2018, LexisNexis"
__version__ = "1.0"

## This function uploads everything from the 'artifacts' folder to S3

################################################################################
# Imports
################################################################################

import boto3
import os
import sys

#######################################
### Variables
staging_bucket = sys.argv[1]
prefix = sys.argv[2].strip('/')

################################################################################
# Main Function
################################################################################

print("Staging Release Unit to S3:")

# Create an S3 Client
s3 = boto3.client('s3', "us-east-1")

for root, dirs, files in os.walk('artifacts'):
    for name in files:
        #  root = "artifacts/62/DataLake/Core/master/15/Resources"
        #  prefix = "62/DataLake/Core/master/15"
        #  name = "name.file"
        trimlength = 11 + len(prefix)
        suffix = os.path.join(root, name)[trimlength:]
        target = os.path.join(prefix, suffix)
        print(target)
        s3.upload_file(os.path.join(root, name), staging_bucket, target)
