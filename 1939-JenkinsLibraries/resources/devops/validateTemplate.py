#!/usr/bin/env python3.6

##############################################################################
# Imports
##############################################################################

import boto3
import os
import re
import sys
import uuid
import time
import random
import requests
from botocore.exceptions import ClientError
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

CERBERUS_URL = 'https://cerberus.content.aws.lexis.com/STABLE'
BUILD_USER_ID = os.getenv('BUILD_USER_ID')


###################################
# Retry session
###################################
# Stolen from https://www.peterbe.com/plog/best-practice-with-retries-with-requests
def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


###################################
# Validate Cloudformation Function
###################################
# cf.validate_template()
# http://boto3.readthedocs.io/en/latest/reference/services/cloudformation.html#CloudFormation.Client.validate_template
def is_template_valid(template_url) -> str:
    while True:
        try:
            cf.validate_template(TemplateURL=template_url)
            return "PASS"
        # Fail
        except ClientError as e:
            if e.response["Error"]["Code"].startswith('Throttling'):
                print(
                    "        Received Throttling Exception on validate_template sleeping then trying again.")
                time.sleep(random.randint(15, 30))
            elif e.response["Error"]["Code"] == "ValidationError":
                return "FAIL - {}".format(e.response["Error"]["Message"])
            else:
                return "ERROR - {}".format(e)


###################################
# Cerberus Scan
###################################
def cerberus_scan(path, file) -> dict:
    filepath = os.path.join(path, file)
    try:
        with open(filepath, "r") as template_file:
            template_body = template_file.read()
        # Submit Template to Cerberus
        put_resp = requests_retry_session().put(
            "{}/upload".format(CERBERUS_URL),
            data=template_body,
            headers={
                "content-type": 'application/json'
            }
        )
        # Initiate Scan
        post_body = {
            "template": [
                {
                    "key": put_resp.json()['template-key'],
                    "name": file
                }
            ],
            "notify": "fail",
            "webhook": "https://outlook.office.com/webhook/c21b6cf6-5b87-4c7a-b2a3-0aff295bd945@9274ee3f-9425-4109-a27f-9fb15c10675d/IncomingWebhook/71b6cdaedaab485d8520a9d6f52c8e07/0c9f8d64-d86e-4e12-bb84-4e08678a887a",
            "scan-modules": [
                "BCS_DevOps",
                "Security"
            ]
        }
        # If we were able to retrieve a user ID, set it as the notification email recipient
        if BUILD_USER_ID is not None:
            post_body['email'] = BUILD_USER_ID

        post_resp = requests_retry_session().post(
            "{}/".format(CERBERUS_URL),
            json=post_body
        )
        exec_id = post_resp.json()['execution-id']
        # Get Results of scan
        i = .2
        while (True):
            time.sleep(i)
            results = requests_retry_session().get(
                "{}/{}".format(CERBERUS_URL, exec_id)
            ).json()
            if (results['execution-status'] == "RUNNING" or
                    (results['execution-status'] == "SUCCEEDED" and not results.get('scan-status'))):
                i = i * 2
            else:
                break
        if results.get('execution-status') == "SUCCEEDED":
            return results
        else:
            return {"scan-status": "ERROR"}
    except:
        return {"scan-status": "ERROR"}


##############################################################################
# Main Function
##############################################################################
# This will have been run from the project root dir.
# Templates should be found in the ./Infrastructure folder
results = True
template_file_pattern = re.compile(r"^.*\.template$")
cf = boto3.client('cloudformation', 'us-east-1')
s3 = boto3.client('s3', 'us-east-1')
staging_bucket = sys.argv[1]

# Get current working directory
working_dir = os.getcwd()
infra_root = os.path.join(working_dir, "Infrastructure")

print("Looking for cloudformation templates to validate in:")
print("  {0}".format(infra_root))

for subdir, dirs, files in os.walk(infra_root):
    for file in files:
        # Check if file is a template
        if template_file_pattern.match(file):
            filepath = os.path.join(subdir, file)
            s3_key = 'TemplateValidation/{}'.format(str(uuid.uuid4()))
            s3.upload_file(
                filepath,
                staging_bucket,
                s3_key
            )
            templateUrl = "https://{0}.s3.amazonaws.com/{1}".format(
                staging_bucket,
                s3_key
            )
            valid = is_template_valid(templateUrl)
            print("    {}".format(file))
            print("      AWS Validation: {}".format(valid))
            if valid == "PASS":
                try:
                    cerberus = cerberus_scan(subdir, file)
                    print("      Cerberus Scan:  {}".format(
                        cerberus.get('scan-status')))
                    if cerberus.get('scan-status-url'):
                        print("      Cerberus Report: {url}".format(
                            url=cerberus['scan-status-url']
                        ))
                except:
                    print("      Cerberus Scan:  ERRORED")
            else:
                print("      Cerberus Scan:  SKIPPED")

            results = results and (valid == "PASS") and (
                cerberus['scan-status'] == 'PASS')

if results:
    print("All cloudformation template(s) valid.")
else:
    print("Invalid cloudformation template(s) found.")
    sys.exit(1)
