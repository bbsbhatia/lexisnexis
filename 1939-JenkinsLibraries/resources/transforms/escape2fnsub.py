#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import logging
import os
import re
import sys

log = logging.getLogger()
log.setLevel(logging.INFO)

################################################################################
# Process params into a dictionary 
################################################################################

try:
    path = sys.argv[1]
except NameError:
    print("path is a required parameter for this transform type")

working_dir = os.getcwd()

transform_root = os.path.normpath(os.path.join(working_dir, path))


################################################################################
# Sub Functions
################################################################################

def transform(filepath):
    # Get the relative path of the file to transform
    relative_path = filepath[len(working_dir + '/Source/'):]
    print("        {0}".format(relative_path))
    # Read in the file
    with open(filepath) as inFile:
        data = inFile.read()
    # Perform transformation(s), build output string
    transformed = re.sub(r'\n+', r'\\n', data.replace('\\', '\\\\')).replace('"', '\\"')
    out_string = "{\"Fn::Sub\":\"" + transformed + "\"}"
    # Build our output path
    out_path = os.path.join(working_dir, "artifacts", relative_path)
    # Ensure folders exist
    (out_dir, _) = os.path.split(out_path)
    os.makedirs(out_dir, exist_ok=True)
    # Write output file
    with open(out_path, 'w+', encoding='UTF-8') as outFile:
        outFile.write(out_string)


def recursion_is_fun(transform_root):
    if os.path.isfile(transform_root):
        # Pipeline Step provided a specific file as the path
        transform(transform_root)
    elif os.path.isdir(transform_root):
        # Pipeline Step provided a folder, so we'll do everything in the folder
        for f in os.listdir(transform_root):
            recursion_is_fun(os.path.join(transform_root, f))


################################################################################
# Main Function 
################################################################################

print("Executing escape2fnsub Transform on:")
print("    {0}".format(transform_root))

recursion_is_fun(transform_root)
