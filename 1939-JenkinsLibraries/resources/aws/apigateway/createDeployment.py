#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import boto3
import sys
import jmespath
import json

################################################################################
# Process params into a dictionary 
################################################################################

try:
    params = json.loads(sys.argv[1])
except:
    print("No parameters passed from command line")

##############################################
### Check for Required Variables, set defaults
try: params['restApiId']
except NameError: print("stackName is a required parameter")

try:    params['stageDescription']
except: params['stageDescription'] = ""

for param in ['stageName','description','cacheClusterEnabled','cacheClusterSize','variables','canarySettings']:
    if params.get(param):
        print("{} is not yet supported.".format(param))

try:    params['query']
except: params['query'] = None
# TODO This is a really bad way to handle this.  Figure out a better way.
# TODO As is, when re decode the params, we loose any [], with this fix, we loose all {}.
#else:   params['query'] = params['query'].replace('{', '[').replace('}', ']')

################################################################################
# Main Function 
################################################################################

# Request Syntax

# response = client.create_deployment(
#     restApiId='string',
#     stageName='string',
#     stageDescription='string',
#     description='string',
#     cacheClusterEnabled=True|False,
#     cacheClusterSize='0.5'|'1.6'|'6.1'|'13.5'|'28.4'|'58.2'|'118'|'237',
#     variables={
#         'string': 'string'
#     },
#     canarySettings={
#         'percentTraffic': 123.0,
#         'stageVariableOverrides': {
#             'string': 'string'
#         },
#         'useStageCache': True|False
#     }
# )

# Debug logging
try: params['debug']
except:
    pass
else:
    boto3.set_stream_logger('botocore', level='DEBUG')

client = boto3.client('apigateway')

try: 
    response = client.create_deployment(
        restApiId=params['restApiId'],
        stageDescription=params['stageDescription']
    )
except Exception as e:
    print("create_deployment call returned an error: {0}".format(e))
    raise Exception(e)

if params['query']:
    print(json.dumps(jmespath.search(params['query'], response),default=str))
else:
    print(json.dumps(response,default=str))