#!/usr/bin/env python3.6
###############################################################################
# Imports
###############################################################################
import boto3
import sys
import jmespath
import json
import re
from botocore.exceptions import ClientError
import time
import random

###############################################################################
# Allowed Clients and Methods
###############################################################################
# Kosater: s3: cp, sync; ssm?

whitelist = {
    'athena': [
        'start_query_execution'
    ],
    'apigateway': [
        'create_deployment',
        'get_*'
    ],
    'autoscaling': [
        'describe_*'
    ],
    'cloudformation': [
        'describe_*',
        'get_*',
        'list_*',
        'validate_template',
        'set_stack_policy'
    ],
    'cloudfront': [
        'get_*',
        'list_*',
        'create_invalidation',
        'get_distribution_config',
        'update_distribution'
    ],
    'dynamodb': [
        'create_backup',
        'list_global_tables',
        'create_global_table'
    ],
    'ec2': [
        'describe_*',
        'get_*',
        'modify_image_attribute'
    ],
    'elb': [
        'describe_*'
    ],
    'elbv2': [
        'describe_*'
    ],
    'rds': [
        'describe_*',
        'list_*'
    ],
    's3': [
        'copy',
        'get_*',
        'list_*',
        'upload_file',
        'put_object',
        'download_file'
    ]
}


###############################################################################
# Process params
###############################################################################
try:
    params = json.loads(sys.argv[1])
except:
    print("No parameters passed from command line")
    exit(1)

# Check for Required Variables, set defaults
# Validate that the requested client is approved
svc_client = params.get('client', None)
if whitelist.get(svc_client, None):
    # Verify that the requested method is approved
    method_approval = False
    method = params.get('method', None)
    for allowed_method in whitelist[svc_client]:
        if re.search(allowed_method, method):
            method_approval = True
    if not method_approval:
        print("Requested method is either not approved or does not exist.")
        exit(1)
else:
    print("Requested client is either not approved or does not exist.")
    exit(1)

args = params.get('arguments', {})
streaming_body = params.get('streamingBody', None)
response_query = params.get('query', None)

###############################################################################
# Main Function
###############################################################################

# Create an CloudFormation Client

# Debug logging
if params.get('debug', None):
    boto3.set_stream_logger('botocore', level='DEBUG')

client = boto3.client(svc_client)


while True:
    try:
        response = eval(
            "client.{method}(**args)".format(method=method, args=args))
        break
    # Fail
    except ClientError as e:
        if e.response["Error"]["Code"].startswith('Throttling'):
            time.sleep(random.randint(15, 30))
            print("        Received Throttling Exception, sleeping then trying again.")
        elif e.response["Error"]["Code"] == "TooManyRequestsException":
            time.sleep(random.randint(15, 30))
            print(
                "        Received TooManyRequestsException, sleeping then trying again.")
        else:
            print("{method} call returned an error: {e}".format(
                method=method, e=e))
            raise Exception(e)

if streaming_body:
    body = jmespath.search(streaming_body, response).read()
    print(body.decode("utf-8"))
elif response_query:
    print(json.dumps(jmespath.search(response_query, response), default=str))
else:
    print(json.dumps(response, default=str))
