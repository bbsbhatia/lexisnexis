
#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import sys
import boto3
import os
import json

def getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0][1:]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

myargs = getopts(sys.argv)

ec2 = boto3.client('ec2', region_name='us-east-1')

permissions = json.loads(myargs['launch-permission'])

results = ec2.modify_image_attribute(   ImageId=myargs['image-id'],
                                        LaunchPermission=permissions)

print(results)
