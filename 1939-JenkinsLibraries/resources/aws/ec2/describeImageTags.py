
#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import sys
import boto3
import os
import json

def getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0][1:]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

myargs = getopts(sys.argv)

# Local testing
#session = boto3.session.Session(profile_name='c-ops')
#ec2 = session.client('ec2', region_name='us-east-1')
# myargs['image-id'] = 'ami-636f901e'

ec2 = boto3.client('ec2', region_name='us-east-1')

results = ec2.describe_images(  Filters=[{'Name' : 'image-id', 'Values': [myargs['image-id']]}],
                                ImageIds=[myargs['image-id']])

tags = {}
for tag in results['Images'][0]['Tags']:
    tags[tag['Key']] = tag['Value']

print(tags)
