#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import sys
import boto3
import os

def getopts(argv):
    opts = {}  # Empty dictionary to store key-value pairs.
    while argv:  # While there are arguments left to parse...
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0][1:]] = argv[1]  # Add key and value to the dictionary.
        argv = argv[1:]  # Reduce the argument list by copying it starting from index 1.
    return opts

myargs = getopts(sys.argv)
BUILD_USER_ID = os.getenv('BUILD_USER_ID', 'unknown') 

sts = boto3.client('sts', region_name=myargs['region'])

results = sts.assume_role(  DurationSeconds=43200,
                            RoleArn='arn:aws:iam::{0}:role/AssetApplication_1939/JenkinsExecutionRole'.format(myargs['account']),
                            RoleSessionName=BUILD_USER_ID)

print("AWS_ACCESS_KEY_ID={0},AWS_SECRET_ACCESS_KEY={1},AWS_SESSION_TOKEN={2},AWS_DEFAULT_REGION={3}".format(results['Credentials']['AccessKeyId'],results['Credentials']['SecretAccessKey'],results['Credentials']['SessionToken'],myargs['region']))
