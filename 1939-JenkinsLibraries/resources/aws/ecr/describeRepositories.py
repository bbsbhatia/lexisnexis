#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import boto3
from botocore.exceptions import ClientError
import os
import sys
import base64
import time
import urllib

################################################################################
# Process params into a dictionary 
################################################################################

try:
    encoded = sys.argv[1]
except:
    print("No parameters passed from command line")
else:
    params = eval(str(base64.b64decode(encoded), 'utf-8')
                  .replace('[', '{')
                  .replace(']', '}')
                  .replace('null', 'None'))
##############################################
### Check for Required Variables, set defaults
BRANCH = os.getenv('GIT_BRANCH').replace('origin/', '', 1)
BUSINESS_UNIT = os.getenv('BUSINESS_UNIT')
ASSET_ID = os.getenv('ASSET_ID')
ASSET_NAME = os.getenv('ASSET_NAME')
MODULE_NAME = os.getenv('MODULE_NAME')
BUILD = os.getenv('BUILD', params.get('build', "NONE"))
BUILD_NUMBER = os.getenv('BUILD_NUMBER', str(int(round(time.time() * 1000))))
BUILD_USER_ID = os.getenv('BUILD_USER_ID','Unknown') 
WORKSPACE = os.getenv('WORKSPACE') 
BUILD_URL = os.getenv('BUILD_URL') 
JOB_NAME = os.getenv('JOB_NAME') 


################################################################################
# Main Function 
################################################################################

# Create a ECR Client

# Debug logging
try: params['debug']
except:
    pass
else:
    boto3.set_stream_logger('botocore', level='DEBUG')

ecr = boto3.client('ecr')

response = None
try:
    repoName = params.get("repoName", "need repo name please")
    repoName = urllib.parse.unquote(repoName)
    response = ecr.describe_repositories(repositoryNames=[ repoName ])
    if type(response) is dict :
        repos = response.get("repositories", None)
        if repos and len(repos) > 0:
            print('{"result": "true"}')
    else:
        print('{"result": "false"}')
except ClientError as e:
    if e.response['Error']['Code'] == 'RepositoryNotFoundException':
        print('{"result": "false"}')
    else:
        print(str(e))
        print('{"result": "unkown exception occurred"}')

except Exception as e:
    print(str(e))
    print('{"result": "unkown exception occurred"}')
