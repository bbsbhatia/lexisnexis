#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import boto3
import os
import sys
import json
import time
from botocore.exceptions import ClientError
import random

################################################################################
# Process params into a dictionary 
################################################################################

try:
    params = json.loads(sys.argv[1])
except:
    print("No parameters passed from command line")

##############################################
# LOCAL TESTING 
# os.environ["AWS_PROFILE"] = "product-datalake-prod-admin"
# params = json.loads('{"command":"createStack","account":"195052678233","templateName":"CreateSubscriptionEventHandlerLambda","stackParameters":{"LambdaFunctionVersionV1":"1"},"region":"us-east-1","stagingBucket":"1939-jenkins-staging-p2","stackName":"release-DataLake-CreateSubscriptionEventHandlerLambda","assetGroup":"release","jenkinsJobNumber":"57","assetId":"62","assetName":"DataLake","assetAreaName":"Subscription","releaseUnit":"master/42","jenkinsJobName":"Wormhole/62-DataLake-Subscription/Deploy/master","buildUserId":"beslbx"}')


##############################################
### Check for Required Parameters, set defaults
try:    params['stagingBucket']
except NameError: print("stagingBucket is a required parameter")

try:    params['stackName']
except NameError: print("stackName is a required parameter")

try:    params['templateName']
except NameError: print("templateName is a required parameter")

try:    params['assetGroup']
except NameError: print("assetGroup is a required parameter")

try:    params['stackParameters']
except: params['stackParameters'] = []

try:    params['capabilities']
except: params['capabilities'] = 'CAPABILITY_IAM'.split(',')
else:   params['capabilities'] = params['capabilities'].split(',')

try:    params['notificationARNs']
except: params['notificationARNs'] = []
else: params['notificationARNs'] = params['notificationARNs'].split(',')

try:    params['buildUserId']
except NameError: print("buildUserId is a required parameter")

try:    params['jenkinsJobNumber']
except NameError: print("jenkinsJobNumber is a required parameter")

try:    params['assetId']
except NameError: print("assetId is a required parameter")

try:    params['assetName']
except NameError: print("assetName is a required parameter")

try:    params['assetAreaName']
except NameError: print("assetAreaName is a required parameter")

try:    params['releaseUnit']
except NameError: print("releaseUnit is a required parameter")

################################################################################
# Main Function 
################################################################################

# Create a CloudFormation Client, S3 Client

# Debug logging
try: params['debug']
except:
    pass
else:
    boto3.set_stream_logger('botocore', level='DEBUG')

# Create Boto Clients
cf = boto3.client('cloudformation')
s3 = boto3.resource('s3')
bucket = s3.Bucket(params['stagingBucket'])

# Look for existing stack
while True:
# If Stack already exists, we are doing an UPDATE, else we are doing a CREATE
    try: 
        response = cf.describe_stacks(StackName=params['stackName'])
    except ClientError as e:
        if e.response["Error"]["Code"].startswith('Throttling'):
            time.sleep(random.randint(15,30))
            print("Received Throttling Exception on cloudformation.describestacks sleeping then trying again.")
        else:
            changeSetType = "CREATE"
            print("Stack [{0}] does not exist yet, issuing CREATE change set.".format(params['stackName']))
            break
    else: 
        for stack in response['Stacks']:
            # Find our stack, it should be the only result
            if stack['StackName'] == params['stackName']:
                # If the status is REVIEW_IN_PROGRESS, then the stack is pending creation, so the type should be CREATE
                if stack['StackStatus'] == "REVIEW_IN_PROGRESS":
                    changeSetType = "CREATE"
                    print("Stack [{0}] under review, issuing CREATE change set.".format(params['stackName']))
                    retry_count = 0 
                else:
                    changeSetType = "UPDATE"
                    print("Stack [{0}] already exists, issuing UPDATE change set.".format(params['stackName']))
                    previousParameters = stack['Parameters']
                    retry_count = 0
        break


# Build Strings
templateUrl = "https://{0}.s3.amazonaws.com/{1}/{2}/{3}/{4}/{5}.template".format(params['stagingBucket'],params['assetId'],params['assetName'],params['assetAreaName'],params['releaseUnit'],params['templateName'])
changeSetName = "Jenkins-Deploy-Job-{0}".format(params['jenkinsJobNumber'])
description = "-".join([params['assetId'],params['assetName'],params['assetAreaName'],params['releaseUnit'].replace('/', '-')])

## Build Parameters
stackParameters = []
# Try to read ReleaseUnit's parameters file from S3
try:
    fileParametersObj = bucket.Object("/".join([params['assetId'],params['assetName'],params['assetAreaName'],params['releaseUnit'],".".join([params['templateName'],'parameters'])]))
    fileParameters = eval(str(fileParametersObj.get().get('Body').read(), 'utf-8'))
    print("Found {0}.parameters file in S3.".format(params['templateName']))
except:
    fileParameters = []

# Build Stack Parameters Array
# For each parameter from the ReleaseUnit parameters file...
for fileParameter in fileParameters:
    # If a value for the parameter was provided from the Jenkins Pipeline, it takes priority
    if fileParameter['ParameterKey'] in params['stackParameters']:
        # Pop Key:Value pair from the Jenkins Pipeline dict
        value = params['stackParameters'].pop(fileParameter['ParameterKey'])
        # Push it to our Stack Parameters Array
        stackParameters.append({'ParameterKey': fileParameter['ParameterKey'], "ParameterValue": value})
    else:
        # Add the Key:Value parameter from the ReleaseUnit to our Stack Parameters Array
        stackParameters.append(fileParameter)

# Any parameters that were passed in from the Jenkins pipeline, and not yet handled, should be added to the Stack Parameters Array
for key in params['stackParameters']:
    value = params['stackParameters'][key]
    stackParameters.append({'ParameterKey': key, "ParameterValue": value})

# If this is an update, then we need to define UsePreviousValue:True for any unspecified parameters
# that exist in both the deployed stack AND the new template
if changeSetType == "UPDATE":
    # Try to read ReleaseUnit parameters file from S3
    try:
        templateObj = bucket.Object("/".join([params['assetId'],params['assetName'],params['assetAreaName'],params['releaseUnit'],".".join([params['templateName'],'template'])]))
        templateBody = str(templateObj.get().get('Body').read(), 'utf-8')
        templateJson = json.loads(templateBody)
        templateParameters = templateJson['Parameters']
    except:
        templateParameters = {}

    # Build a list of ParameterKeys we already added
    stackParameterKeys = [key['ParameterKey'] for key in stackParameters]
    # For each parameter that was in the deployed stack
    for previousParameter in previousParameters:
        # If it still exists in the template and is not already in stackParameterKeys
        if  (previousParameter['ParameterKey'] in templateParameters.keys()) \
        and (previousParameter['ParameterKey'] not in stackParameterKeys):
            # Then add it to stackParameters with UsePreviousValue:True
            stackParameters.append({'ParameterKey': previousParameter['ParameterKey'], "UsePreviousValue": True})

#####
# Debug logging
try:
    params['debug']
except:
    pass
else:
    for param in params:
        print("params['{0}']: {1}".format(param, params[param]))
    print('stackParameters: {0}'.format(stackParameters))

# Create Stack Set
while True:
    try:
        response = cf.create_change_set(
            StackName=params['stackName'],
            TemplateURL=templateUrl,
            Parameters=stackParameters,
            Capabilities=params['capabilities'],
            NotificationARNs=params['notificationARNs'],
            Tags=[  {'Key': 'AssetID',      'Value': params['assetId']},
                    {'Key': 'AssetName',    'Value': params['assetName']},
                    {'Key': 'AssetAreaName','Value': params['assetAreaName']},
                    {'Key': 'AssetGroup',   'Value': params['assetGroup']},
                    {'Key': 'ReleaseUnit',  'Value': params['releaseUnit']},
                    {'Key': 'DeployedBy',   'Value': params['buildUserId']},
                    {'Key': 'JenkinsJob',   'Value': "{0}-{1}".format(params['jenkinsJobName'].replace("/","-").replace("%2F","-"), params['jenkinsJobNumber'])}
                ],
            ChangeSetName=changeSetName,
            Description=description,
            ChangeSetType=changeSetType
        )
    except ClientError as e:
        if e.response["Error"]["Code"].startswith('Throttling'):
            time.sleep(random.randint(15,30))
            print("Received Throttling Exception on cloudformation.create_change_set sleeping then trying again.")
        elif e.response["Error"]["Code"] == "ValidationError":
            print("If the ValidationError was and S3:AccessDenied Error check the following:")
            print("    - Does the spelling of the filename match the spelling in the pipeline file?")
            print("    - Did the file show up in the Staging Release Unit to S3 list?")
            print("    - Did the file show up in the Build Job's Published to Artifactory list?")
            raise e
        else:
            raise Exception("AWS create_change_set Failed.")
    else:
        break

print("Create Change Set: {0}".format(response))

# Wait for Change Set to "CREATE_COMPLETE"
while True:
    try:
        response = cf.describe_change_set(ChangeSetName=changeSetName,StackName=params['stackName'])
        if response['Status'] == 'CREATE_COMPLETE':
            break
        elif response['Status'] == 'FAILED':
            if response['StatusReason'] == "The submitted information didn't contain changes. Submit different information to create a change set.":
                print(response['StatusReason'])
                break
            else:
                raise Exception("change-set creation failed after API call: {}".format(response['StatusReason']))
        else: #'CREATE_PENDING'|'CREATE_IN_PROGRESS'|'DELETE_COMPLETE'
            time.sleep(5)
    except ClientError as e:
        if e.response["Error"]["Code"].startswith('Throttling'):
            time.sleep(random.randint(15,30))
            print("Received Throttling Exception on cloudformation.describe_change_set sleeping then trying again.")
        else:
            raise Exception("AWS describe_change_set failed:")
            

results = {"StackName": response["StackName"], "ChangeSetName": response["ChangeSetName"],"Status": response["Status"]}
print(results)