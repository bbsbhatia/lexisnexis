#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import boto3
import os
import sys
import json
import time
from botocore.exceptions import ClientError
import random

################################################################################
# Process params into a dictionary 
################################################################################

try:
    params = json.loads(sys.argv[1])
except:
    print("No parameters passed from command line")

##############################################
### Check for Required Parameters, set defaults
try:    params['stackName']
except NameError: print("stackName is a required parameter")

try:    params['templateName']
except NameError: print("templateName is a required parameter")

try:    params['assetGroup']
except NameError: print("assetGroup is a required parameter")

try:    params['jenkinsJobNumber']
except NameError: print("jenkinsJobNumber is a required parameter")

try:    params['assetId']
except NameError: print("assetId is a required parameter")

try:    params['assetName']
except NameError: print("assetName is a required parameter")

try:    params['assetAreaName']
except NameError: print("assetAreaName is a required parameter")

try:    params['releaseUnit']
except NameError: print("releaseUnit is a required parameter")

try:    params['deleteOnCreateFail']
except: params['deleteOnCreateFail'] = True

################################################################################
# Sub Function 
################################################################################

# monitor_stack_events performs a describe stack every 10s until the stack reaches a state of *_COMPLETE or *_FAILED
# Both parameters are required.  #1 is a boto3 CloudFormation client, #2 is the stack_id as a string
def monitor_stack_events(cf, stack_id):
    waiting = True
    found_begining = False
    last_posted = None
    next_token = None

    events = []
    while waiting:
        time.sleep(10)
        while not found_begining:
            # Call AWS and get a page of events
            try: 
                if next_token:
                    response = cf.describe_stack_events(StackName=stack_id,NextToken=next_token)
                else:
                    response = cf.describe_stack_events(StackName=stack_id)
            except ClientError as e:
                if e.response['Error']['Code'].startswith("Throttling"):
                    response = {'StackEvents': []}
                else:
                    raise Exception("AWS describe_stack_events failed:")

            # Walk through the events looking for the last_posted event, or the first event of this activity
            try:
                for event in response['StackEvents']:
                    # If the event is the first of this activity, print it and set it as 'last_posted'
                    if (event['ResourceType'] == 'AWS::CloudFormation::Stack') and ('ResourceStatusReason' in event) and (event['ResourceStatusReason'] == "User Initiated") and (last_posted is None):
                        last_posted = event['EventId']
                        print("{0:35} {1:35} {2:45}".format(event['ResourceStatus'],event['ResourceType'],event['LogicalResourceId']))
                        print(". . . . . {0}".format(event['ResourceStatusReason']))
                    # If we find the last_posted event, we can stop digging
                    if event['EventId'] == last_posted:
                        found_begining = True
                        break
                    # Otherwise, we need to put this event on our list, and keep digging
                    else:
                        events.append(event)
            except Exception as e:
                print(response)
                raise e

            if 'NextToken' in response:
                next_token = response['NextToken']
        # The loop continues if we haven't found the last_posted event yet because we need to get the next page of events, and keep digging
        # If we found the last_posted event the loop exits
        # Reset found_begining 
        found_begining = False
        # Set the next_token to none, so that we get the newest events again
        next_token = None
        # While we have events in our list and we are still waiting
        while len(events) and waiting:
            # Get the event from the end of the list
            event = events.pop(-1)
            # Print it
            print("{0:35} {1:35} {2:45}".format(event['ResourceStatus'],event['ResourceType'],event['LogicalResourceId']))
            if 'ResourceStatusReason' in event:
                print(". . . . . {0}".format(event['ResourceStatusReason']))
            # Set it as the last_posted event
            last_posted = event['EventId']
            # Inspect the event to find out if is a Stack *_COMPLETE event
            if ((event['ResourceType'] == 'AWS::CloudFormation::Stack') and (event['ResourceStatus'] in ["CREATE_COMPLETE","UPDATE_COMPLETE","DELETE_COMPLETE","ROLLBACK_COMPLETE","UPDATE_ROLLBACK_COMPLETE"])):
                # We have found proof that this stack activity is complete, and we are no longer waiting for the activity to complete
                waiting = False
                return event['ResourceStatus']
    # If we are still waiting for the activity to conclude, loop back to the start and get the latest events

# Debug logging
try: params['debug']
except:
    pass
else:
    boto3.set_stream_logger('botocore', level='DEBUG')

################################################################################
# Main Function 
################################################################################

# Create a CloudFormation Client
#boto3.set_stream_logger('botocore', level='DEBUG')
cf = boto3.client('cloudformation')

# Build Strings
changeSetName = "Jenkins-Deploy-Job-{0}".format(params['jenkinsJobNumber'])
# Describe Stack to discover if this is a CREATE or UPDATE changeSetType
while True:
    try:
        response = cf.describe_stacks(StackName=params['stackName'])
    except ClientError as e:
        if e.response["Error"]["Code"].startswith('Throttling'):
            time.sleep(random.randint(15,30))
            print("Received Throttling Exception on cloudformation.describe_stacks sleeping then trying again.")
        else:
            raise Exception("AWS Call Failed")
    else:
        break

for stack in response['Stacks']:
    if stack['StackName'] == params['stackName']:
        stack_id = stack['StackId']
        if stack['StackStatus'] == 'REVIEW_IN_PROGRESS':
            changeSetType = 'CREATE'
            break
        else:
            changeSetType = 'UPDATE'
            break

#####
# Debug logging
try: params['debug']
except:
    pass
else:
    print("changeSetType: {0}".format(changeSetType))
    print("changeSetName: {0}".format(changeSetName))
    print("stack_id: {0}".format(stack_id))
    for param in params:
        print("params['{0}']: {1}".format(param, params[param]))

# Execute Change Set
while True:
    try:
        response = cf.execute_change_set(
            ChangeSetName=changeSetName,
            StackName=stack_id
        )
        print("Executed Change Set [{0}] to {1} stack [{2}]".format(changeSetName,changeSetType,params['stackName']))
        stackActivityResult = monitor_stack_events(cf, stack_id)
    except ClientError as e:
        if e.response["Error"]["Code"].startswith('Throttling'):
            time.sleep(random.randint(15,30))
            print("Received Throttling Exception on cloudformation.execute_change_set sleeping then trying again.")
        else:
            raise Exception("AWS execute_change_set Failed.")
    else:
        break

print(stack)

# If we were creating a new stack, and we had a rollback, we should cleanup the bad stack
if changeSetType == 'CREATE' and params['deleteOnCreateFail'] and stackActivityResult in ['ROLLBACK_COMPLETE']:
    print("Stack creation failed, and rollback is complete.")
    while True:
        try:
            response = cf.delete_stack(StackName=stack_id)
            print("Deleting stack [{0}]".format(params['stackName']))
            stackActivityResult = monitor_stack_events(cf, stack_id)
        except ClientError as e:
            if e.response["Error"]["Code"].startswith('Throttling'):
                time.sleep(random.randint(15,30))
                print("Received Throttling Exception on cloudformation.delete_stack sleeping then trying again.")
            else:
                raise Exception("AWS delete_stack failed:")
        else:
            break

# If this changeset executed properly, the end result should be either CREATE_COMPLETE or "UPDATE_COMPLETE"
if stackActivityResult in ['CREATE_COMPLETE','UPDATE_COMPLETE']:
    sys.exit(0)
else:
    sys.exit(1)