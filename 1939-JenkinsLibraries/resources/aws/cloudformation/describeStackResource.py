#!/usr/bin/env python3.6

################################################################################
# Imports
################################################################################

import boto3
import sys
import jmespath
import json

################################################################################
# Process params into a dictionary 
################################################################################

try:
    params = json.loads(sys.argv[1])
except:
    print("No parameters passed from command line")

##############################################
### Check for Required Variables, set defaults
try: params['stackName']
except NameError: print("stackName is a required parameter")

try: params['logicalResourceId']
except NameError: print("logicalResourceId is a required parameter")

try:    params['query']
except: params['query'] = None
# TODO This is a really bad way to handle this.  Figure out a better way.
# TODO As is, when re decode the params, we loose any [], with this fix, we loose all {}.
#else:   params['query'] = params['query'].replace('{', '[').replace('}', ']')

################################################################################
# Main Function 
################################################################################

# Create an CloudFormation Client

# Debug logging
try: params['debug']
except:
    pass
else:
    boto3.set_stream_logger('botocore', level='DEBUG')

cf = boto3.client('cloudformation')

try: 
    response = cf.describe_stack_resource(StackName=params['stackName'],LogicalResourceId=params['logicalResourceId'])
except Exception as e:
    print("describe_stack_resource call returned an error: {0}".format(e))
    raise Exception(e)
    
if params['query']:
    print(json.dumps(jmespath.search(params['query'], response),default=str))
else:
    print(json.dumps(response,default=str))