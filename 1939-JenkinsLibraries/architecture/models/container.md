


```plantuml
@startuml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Container.puml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Component.puml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Context.puml
title Wormhole CICD Jenkins Container Diagram

Person(developer, "Devloper", "Human User")

System_Boundary(jenkins, "Jenkins"){
    Container(webserver, "Jenkins", "Web Server", "Core orchestration platform for integration and deployment")
    Container(packer, "AMI Bakery", "Packer", "Provisioning of AMIs")
    Container(docker, "Container Bakery", "Docker", "Provisioning of Docker Images")
    Container_Boundary(plugins, "Jenkins Plugins"){
        Component(plug_ad, "Active Directory plugin", "User Authentication", "Allows jenkins to use Legal AD for Authentication")
        Component(plug_stat_gather, "Statistics Gatherer Plugin", "Build/Deploy Metrics", "Captures Statistics related to Jenkins Builds, Build Step, SCM checkouts, Jobs and Queue and sends them where you want.")
        Component(plug_tfs, "Team Foundation Server Plug-in", "VSTS Integration", " ")
        Component(plug_ec2, "Amazon EC2 plugin", "Ec2 Management", "Used for on-demand management of Jenkins worker nodes.")
    }
    Container_Boundary(lib, "Jenkins Libraries"){
         Component(library, "DevOpsShared", "1939-JenkinsLibraries", "Provides Jenkins pipeline steps supporting the Wormhole CICD process.")
    }

}

System(metrics, "Build/Deploy Metrics", "Collection of emitted Build/Deploy events.")
System_Ext(artifactory, "Artifactory", "Universal binary repository, SaaS provided by jFrog on AWS")
System_Ext(legal, "legal.regn.net", "User Directory")

System_Ext(aws, "AWS", "Cloud Service Provider")
System_Ext(vsts, "VSTS Git", "Source Code Management")
System(cerberus, "Cerberus", "Static code analysis of CloudFormation templates")
System(sonarqube, "SonarQube", "Static code analysis of source code")

Rel(developer, webserver, "Uses", "HTTPS")
Rel_Down(webserver, plug_ad, "Implements")
Rel_Down(webserver, plug_ec2, "Implements")
Rel_Down(webserver, plug_stat_gather, "Implements")
Rel_Down(webserver, plug_tfs, "Implements")
Rel_Right(webserver, library, "Executes")

Rel(library, packer, "Executes", "CLI")
Rel(library, docker, "Executes", "CLI")
Rel_Left(library, artifactory, "Gets/Puts Files", "API")
Rel_Left(library, cerberus, "Validates", "API")
Rel_Left(library, sonarqube, "Validates", "API")
Rel(library, aws, "Configures", "API")


Rel(plug_stat_gather, metrics, "Sends Events", "API")
Rel(plug_ad, legal, "Authenticates")
Rel(plug_tfs, vsts, "Gets Source Code", "API")
Rel(docker, aws, "Stores Containers in ECR", "API")
Rel(packer, aws, "Creates AMIs in EC2", "API")
Rel(plug_ec2, aws, "Deploys", "API")

@enduml
```