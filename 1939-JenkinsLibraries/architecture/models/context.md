# Wormhole CICD 
## System Context Diagram

Jenkins is the core orchestration platform for the Wormhole CICD Process.  Users interact with it through an authenticated web application.  Other services, such as Azure DevOps Pipelines can call into Jenkins via API.

Jenkins uses a number of other systems on behalf of the Developer.

```plantuml
@startuml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Context.puml

LAYOUT_LEFT_RIGHT
title Wormhole CICD Context Diagram

Person(developer, "Devloper", "Human User")
System_Ext(azure_devops, "Azure DevOps Pipelines", "Event driven git-based triggers")

System(jenkins, "Jenkins", "Core orchestration platform for integration and deployment")
System(metrics, "Jenkins Metrics", "Collection of jenkins build metrics for data processing")
System_Ext(artifactory, "Artifactory", "Release Unit Storage")
System_Ext(vsts, "VSTS Git", "Source Code Management")
System(cerberus, "Cerberus", "Static code analysis of CloudFormation templates")
System(sonarqube, "SonarQube", "Static code analysis of source code")
System_Ext(aws, "AWS", "Cloud Service Provider")

Rel(azure_devops, jenkins, "Uses", "API")
Rel(developer, jenkins, "Uses", "HTTPS")

Rel(jenkins, artifactory, "Gets/Puts Files", "API")
Rel(jenkins, vsts, "Gets Code", "API")
Rel(jenkins, metrics, "Sends Events", "API")
Rel(jenkins, cerberus, "Checks", "API")
Rel(jenkins, sonarqube, "Checks", "API")
Rel(jenkins, aws, "Deploys", "API")

@enduml
```


