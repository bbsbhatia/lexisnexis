from flask import Flask, Response, request, make_response
from prometheus_client import Summary, Counter, Gauge, Histogram, make_wsgi_app

import time
import json
import random
import itertools


app = Flask(__name__)

metrics_server = make_wsgi_app()

counter = {'request': 0, 'failure': 0}

requests_serviced_summary = Summary('requests_serviced', 'Time to service request')
requests_failure_counter = Counter('requests_failed', 'Number of requests failed')
requests_inprogress_guage = Gauge('requests_in_progress', 'Number of currently executing requests')
requests_latency_histogram = Histogram('requests_latency', 'Request Latency')


@app.before_request
def inc_request_counter():
    counter['request'] += 1
    return None


@app.before_first_request
def load_model():
    print("Model Loaded!")


@app.route('/')
@requests_serviced_summary.time()
@requests_inprogress_guage.track_inprogress()
@requests_latency_histogram.time()
def index():
    try:
        print("request received!")
        wait = random.randint(1, 5)
        if wait == 3:
            raise Exception("This was destined to fail")
        time.sleep(wait)
        return 'Welcome to Flask!'
    except Exception as err:
        print('Service Exception: {0}'.format(err))
        requests_failure_counter.inc()
        return make_response('Service Exception: {0}'.format(err), 501)


def make_start_response():
    data = {}

    def start_response(status, headers):
        data['status'] = status
        data['headers'] = headers

    return start_response, data


@app.route('/metrics')
def metrics():
    """
    http://localhost:8000/metrics?name[]=requests_serviced_count&name[]=requests_serviced_sum
    &name[]=requests_failed_total&name[]=requests_in_progress
    """
    start_response_fn, start_response_data = make_start_response()
    metrics_data = metrics_server(request.environ, start_response_fn)
    return make_response(metrics_data[0], start_response_data['status'], start_response_data['headers'])


@app.route('/counters')
def counters():
    metrics = [requests_serviced_summary, requests_failure_counter, requests_inprogress_guage,
               requests_latency_histogram]
    samples = list(map(lambda m: m.collect()[0].samples, metrics))
    samples = list(itertools.chain.from_iterable(samples))

    return Response(response=json.dumps(list(map(lambda s: s._asdict(), samples))),
                    status=200, mimetype='application/json')


@app.route('/health')
def health():
    metrics = [requests_serviced_summary, requests_failure_counter, requests_inprogress_guage,
               requests_latency_histogram]
    samples = list(map(lambda m: m.collect()[0].samples, metrics))
    samples = list(itertools.chain.from_iterable(samples))

    mapper = lambda s: {'name': s.name, 'value': s.value, 'labels': s.labels}
    return Response(response=json.dumps(list(map(mapper, samples))),
                    status=200, mimetype='application/json')


@app.route('/dispatch', methods=['POST'])
def dispatch():
    content = request.json
    print('request:{}'.format(content))
    print(content['message'].encode())
    return Response(response=json.dumps({'message': content['message'], 'status': 'accepted'}),
                    status=200, mimetype='application/json')


@app.route('/version')
def version():
    return app.send_static_file('version.txt')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8000', debug=False, threaded=True)
