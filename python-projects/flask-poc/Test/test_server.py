import unittest
from server import app


class ServerTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.app = app.test_client()

    def test_index(self):
        result = self.app.get('/')
        self.assertTrue(result.status_code in [200, 501])
